import { BrowserModule } from '@angular/platform-browser';
import { NgModule, LOCALE_ID } from '@angular/core';
import { AppComponent } from './parent-component/app.component';
import { SidebarComponent } from './template/sidebar/sidebar.component';
import { registerLocaleData } from '@angular/common';
import localeId from '@angular/common/locales/id';
import { RoutersModule } from './router.module';
import { HttpClientModule } from '@angular/common/http';
import { DragulaModule, DragulaService } from 'ng2-dragula';
import { MaterialModule } from './material.module';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';

import { PipesModule } from './pipes/pipes.module';
import { DialogComponent } from './shared/dialog/dialog.component';
import { PlanningValidCellComponent } from './shared/cell/planning-valid-cell/planning-valid-cell.component';
import { NormalTextPipe } from './shared/normal-text.pipe';
import { LoginV2Component } from './login-v2/login-v2.component';
import { HomeComponent } from './home/home.component';
import { SelectSearchPipe } from './shared/select-search.pipe';
import { AngularFontAwesomeModule } from 'angular-font-awesome';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { FormlyModule } from '@ngx-formly/core';
import { ProjectTableComponent } from './components/project-table/project-table.component';
import { ToastrModule } from 'ngx-toastr';

registerLocaleData(localeId, 'id');

@NgModule({
  declarations: [
    AppComponent,
    SidebarComponent,
    DialogComponent,
    // PlanningValidCellComponent,
    NormalTextPipe,

    // update componnet version new
    LoginV2Component,
    HomeComponent,
    SelectSearchPipe,
    // ProjectTableComponent,
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    RoutersModule,
    HttpClientModule,
    ReactiveFormsModule,
    FormsModule,
    PipesModule,
    DragulaModule.forRoot(),
    AngularFontAwesomeModule,
    MaterialModule,
    MaterialModule,
    FormlyModule,
  ],
  entryComponents: [
    DialogComponent,
  ],
  providers: [
    { provide: LOCALE_ID, useValue: 'id'},
    DragulaService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
