import { Component, OnInit, Input, ChangeDetectionStrategy, OnChanges } from '@angular/core';
import { MatTableDataSource } from '@angular/material';

export interface DescriptionData {
  title: string;
  description: string;
}

@Component({
  selector: 'app-card-description',
  templateUrl: './card-description.component.html',
  styleUrls: ['./card-description.component.css'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class CardDescriptionComponent implements OnInit, OnChanges {
  displayedColumns: string[] = ['title', 'description'];
  @Input() title: string;
  @Input() data: DescriptionData[] = [];
  datasource: MatTableDataSource<DescriptionData>;
  constructor() { }

  ngOnInit() {}

  ngOnChanges() {
    this.datasource = new MatTableDataSource(this.data);
  }

}
