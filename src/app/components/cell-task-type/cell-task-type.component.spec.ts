import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CellTaskTypeComponent } from './cell-task-type.component';

describe('CellTaskTypeComponent', () => {
  let component: CellTaskTypeComponent;
  let fixture: ComponentFixture<CellTaskTypeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CellTaskTypeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CellTaskTypeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
