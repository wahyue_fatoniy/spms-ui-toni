import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-cell-task-type',
  templateUrl: './cell-task-type.component.html',
  styleUrls: ['./cell-task-type.component.css']
})
export class CellTaskTypeComponent implements OnInit {
  @Input() taskType: string;

  public type = {
    MOS: 'Mos',
    CREATE_ATP: 'Create Atp',
    SUBMIT_ATP: 'Submit Atp',
    ATP: 'Atp',
    CREATE_COMPLETENESS_ATP: 'Create Completeness Atp',
    SUBMIT_COMPLETENESS_ATP: 'Submit Completeness Atp',
    CREATE_BAUT: 'Create Baut',
    SUBMIT_BAUT: 'Submit Baut',
    INSTALLATION: 'Installation',
    INTEGRATION: 'Integration',
    TAKE_DATA: 'Take Data'
  };

  constructor() { }

  ngOnInit() {
  }

}
