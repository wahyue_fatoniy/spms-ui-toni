import { Component, Inject } from '@angular/core';
import { MAT_DIALOG_DATA, MatTableDataSource } from '@angular/material';

export interface Column {
  title: string;
  columnDef: string;
}

export interface FormList<T> {
  formTitle: string;
  columns: Column[];
  data: T[];
}

@Component({
  selector: 'app-form-list',
  templateUrl: './form-list.component.html',
  styleUrls: ['./form-list.component.css']
})

export class FormListComponent<T> {
  datasource: MatTableDataSource<Object>;
  columns: string[] = [];
  constructor(@Inject(MAT_DIALOG_DATA) public value: FormList<T>) {
    this.datasource = new MatTableDataSource(value.data);
    value.columns.forEach(column => {
      this.columns = [...this.columns, column.columnDef];
    });
  }
}
