import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DescDocumentLifeCycleComponent } from './desc-document-life-cycle.component';

describe('DescDocumentLifeCycleComponent', () => {
  let component: DescDocumentLifeCycleComponent;
  let fixture: ComponentFixture<DescDocumentLifeCycleComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DescDocumentLifeCycleComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DescDocumentLifeCycleComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
