import { Component, OnInit, Inject, ChangeDetectionStrategy } from '@angular/core';
import { MAT_DIALOG_DATA } from '@angular/material';
import { DocumentLifecycle } from 'src/app/models/document.model';

@Component({
  selector: 'app-desc-document-life-cycle',
  templateUrl: './desc-document-life-cycle.component.html',
  styleUrls: ['./desc-document-life-cycle.component.css']
})
export class DescDocumentLifeCycleComponent implements OnInit {

  constructor(
    @Inject(MAT_DIALOG_DATA) public data: DocumentLifecycle
  ) { }

  ngOnInit() {
  }

}
