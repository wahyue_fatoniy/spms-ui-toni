import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DocumentLifeCycleComponent } from './document-life-cycle.component';

describe('DocumentLifeCycleComponent', () => {
  let component: DocumentLifeCycleComponent;
  let fixture: ComponentFixture<DocumentLifeCycleComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DocumentLifeCycleComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DocumentLifeCycleComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
