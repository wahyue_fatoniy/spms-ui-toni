import { Component, OnInit, ViewChild, ChangeDetectionStrategy, Input, OnChanges } from '@angular/core';
import { CoreTable, TableConfig } from 'src/app/shared/core-table';
import { DocumentLifecycle } from 'src/app/models/document.model';
import { MatPaginator, MatSort, MatDialog } from '@angular/material';
import { takeUntil } from 'rxjs/operators';
import { DescDocumentLifeCycleComponent } from './desc-document-life-cycle/desc-document-life-cycle.component';

const CONFIG: TableConfig = {
  title: "Document History",
  columns: ["no", "number", "title", "date", "status", "reviewer"],
  typeFilters: [
    { value: "all", label: "All" },
    { value: "number", label: "No" },
    { value: "title", label: "Title" },
    { value: "status", label: "Status" },
    { value: "date", label: "Date" },
    { value: "reviewer", label: "Reviewer" }
  ]
};

@Component({
  selector: 'app-document-life-cycle',
  templateUrl: './document-life-cycle.component.html',
  styleUrls: ['./document-life-cycle.component.css'],
  changeDetection: ChangeDetectionStrategy.OnPush

})
export class DocumentLifeCycleComponent extends CoreTable<DocumentLifecycle> implements OnInit, OnChanges {
  @Input() data: DocumentLifecycle[] = [];
  @ViewChild(MatPaginator)
  paginator: MatPaginator;
  @ViewChild(MatSort)
  sort: MatSort;

  constructor(
    private dialog: MatDialog
  ) {
    super(CONFIG);
  }

  ngOnInit() {
    this.setDatasource();
  }

  ngOnChanges() {
    this.setDatasource();
  }

  setDatasource() {
    console.log(this.data);
    this.datasource.data = this.data;
    this.datasource.paginator = this.paginator;
    this.datasource.sort = this.sort;
    this.filterDatasource();
  }

  public filterDatasource() {
    this.search.valueChanges
    .pipe(takeUntil(this.destroy$))
    .subscribe(keyword => {
      this.datasource.filter = keyword;
    });
    let filterData;
    this.datasource.filterPredicate = (value: DocumentLifecycle, keyword: string) => {
       filterData = {
        number: value.getDocument.getNumber,
        title: value.getDocument.getTitle,
        date: value.getDate,
        status: value.getDocument.getStatus,
        reviewer: value.getDocument.getReviewer
      };
      return this.filterPredicate(filterData, keyword, this.typeFilter);
    };
  }

  public sortData(sort): void {
    this.datasource.data = this.datasource.data.sort((a, b) => {
      const isAsc = sort.direction === "asc";
      switch (sort.active) {
        case "no":
          return this.compare(a.getDocument.getNumber, b.getDocument.getNumber, isAsc);
        case "title":
          return this.compare(
            a.getDocument.getTitle,
            b.getDocument.getTitle,
            isAsc
          );
        case "date":
          return this.compare(a.getDate, b.getDate, isAsc);
        case "status":
          return this.compare(
            a.getDocument.getStatus,
            b.getDocument.getStatus,
            isAsc
          );
        case "reviewer":
          return this.compare(
            a.getDocument.getReviewer,
            b.getDocument.getReviewer,
            isAsc
          );
        default:
          return 0;
      }
    });
  }

  showDescDoc(document: DocumentLifecycle) {
    this.dialog.open(DescDocumentLifeCycleComponent, {
      data: document,
      disableClose: true,
      width: '850px'
    });
  }
}
