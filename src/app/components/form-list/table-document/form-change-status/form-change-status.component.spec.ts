import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FormChangeStatusComponent } from './form-change-status.component';

describe('FormChangeStatusComponent', () => {
  let component: FormChangeStatusComponent;
  let fixture: ComponentFixture<FormChangeStatusComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FormChangeStatusComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FormChangeStatusComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
