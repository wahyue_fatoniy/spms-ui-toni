import { Component, OnInit, Inject, OnDestroy } from '@angular/core';
import { Subject } from 'rxjs';
import { Customer } from 'src/app/models/customer.model';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { MAT_DIALOG_DATA } from '@angular/material';
import { CustomerService } from 'src/service/customer-service/customer.service';
import { takeUntil } from 'rxjs/operators';
import { Document, DocumentStatus, DocumentType, DocumentLifecycle } from 'src/app/models/document.model';

interface FormDocumentData {
  data: Document;
  author: any[];
}

export interface OnCreateDoc {
  document: Document;
  documentLifeCycle: DocumentLifecycle;
}

@Component({
  selector: 'app-form-document',
  templateUrl: './form-document.component.html',
  styleUrls: ['./form-document.component.css']
})
export class FormDocumentComponent implements OnInit, OnDestroy {

  private destroy = new Subject<boolean>();
  public customerData: Customer[] = [];
  public form: FormGroup;
  public onCreateDoc: OnCreateDoc = {
    document: new Document(),
    documentLifeCycle: new DocumentLifecycle
  };
  documentType = [
    DocumentType.ATP,
    DocumentType.COMPLETENESS_ATP,
    DocumentType.BAUT
  ];

  typeLabel = {
    ATP: 'Atp',
    COMPLETENESS_ATP: 'Completeness Atp',
    BAUT: 'Baut',
  };

  constructor(
    @Inject(MAT_DIALOG_DATA) public data: FormDocumentData,
    private customerService: CustomerService,
    private formBuilder: FormBuilder
  ) { }

  ngOnInit() {
    this.getData();
    this.buildForm();
    this.onFormValid();
  }

  ngOnDestroy() {
    this.destroy.next(true);
    this.destroy.complete();
  }

  public getData() {
    this.customerService.subscriptionData()
    .pipe(takeUntil(this.destroy))
    .subscribe((val) => {
      this.customerData = val;
    });
  }

  public buildForm() {
    this.form = this.formBuilder.group({
      id: [this.data.data.getNumber],
      number: [this.data.data.getNumber, Validators.required],
      title: [this.data.data.getId, Validators.required],
      author: [this.data.data.getNIP, Validators.required],
      reviewer: [this.data.data.getReviewer, Validators.required],
      type: [this.data.data.getType, Validators.required],
    });
  }

  public onFormValid() {
    this.form.statusChanges
    .pipe(takeUntil(this.destroy))
    .subscribe(status => {
      if (status === 'VALID') {
        // set document
        let document = new Document();
        document.setId = this.form.value['id'];
        document.setNip = this.form.value['author'];
        document.setNumber = this.form.value['number'];
        document.setReviewer = this.form.value['reviewer'];
        document.setType = this.form.value['type'];
        document.setStatus = DocumentStatus.create;
        document.setTitle = this.form.value['title'];
        document.setAuthor = this.data.author.find(author => author.getNIP === this.form.value['author']) ?
        this.data.author.find(author => author.getNIP === this.form.value['author']).getParty['name'] : null;
        this.onCreateDoc.document = document;

        // set documentLifeCycle
        let documentLifeCycle = new DocumentLifecycle();
        documentLifeCycle.setDescription = null;
        documentLifeCycle.setDocument = document;
        documentLifeCycle.setStatusDoc = DocumentStatus.create;
        documentLifeCycle.setDate = null;
        this.onCreateDoc.documentLifeCycle = documentLifeCycle;
      }
    });
  }

}
