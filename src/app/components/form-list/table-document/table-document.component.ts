import { Component, OnInit, Input, Output, EventEmitter, ViewChild, OnDestroy, OnChanges, ChangeDetectionStrategy } from '@angular/core';
import { CoreTable, TableConfig } from 'src/app/shared/core-table';
import { Document, DocumentLifecycle } from 'src/app/models/document.model';
import { MatDialog, MatPaginator, MatSort } from '@angular/material';
import { FormChangeStatusComponent } from './form-change-status/form-change-status.component';
import { takeUntil } from 'rxjs/operators';
import { Router } from '@angular/router';

export interface OnChangeDocument {
  document: Document;
  documentLifeCycle: DocumentLifecycle;
}

const CONFIG: TableConfig = {
  title: "Documents",
  columns: [
    "no",
    "number",
    "title",
    'status',
    'author',
    'nik',
    'reviewer',
    'action'
  ],
  typeFilters: [
    { value: "all", label: "All" },
    { value: "no", label: "No" },
    { value: "title", label: "Title" },
    { value: "author", label: "Author" },
    { value: "nik", label: "NIK" },
    { value: "reviewer", label: "Reviewer" },
  ]
};

@Component({
  selector: 'app-table-document',
  templateUrl: './table-document.component.html',
  styleUrls: ['./table-document.component.css'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class TableDocumentComponent extends CoreTable<Document>
implements OnInit, OnChanges, OnDestroy {
  @Input() data: Document[] = [];
  @Input() navigateLifeCycle: string;
  @Output() OnChangeStatus: EventEmitter<OnChangeDocument> = new EventEmitter<OnChangeDocument>();
  @Output() OnCreate: EventEmitter<string> = new EventEmitter<string>();
  @Input() loading = false;

  @ViewChild(MatPaginator)
  paginator: MatPaginator;
  @ViewChild(MatSort)
  sort: MatSort;
  constructor(
    public dialog: MatDialog,
    public router: Router
  ) {
    super(CONFIG);
  }

  ngOnInit() {
    this.setDatasource();
  }

  ngOnDestroy() {
    this.destroy$.next(true);
    this.destroy$.complete();
  }

  ngOnChanges() {
    this.setDatasource();
  }

  public setDatasource() {
    this.datasource.data = this.data;
    this.datasource.paginator = this.paginator;
    this.datasource.sort = this.sort;
    this.filterDatasource();
  }

  public filterDatasource() {
    this.search.valueChanges
    .pipe(takeUntil(this.destroy$))
    .subscribe(val => {
      this.datasource.filter = val;
    });

    this.datasource.filterPredicate = (value: Document, keyword: string) => {
     return this.filterPredicate(value, keyword, this.typeFilter);
   };
  }

  public sortData(sort): void {
    this.datasource.data = this.datasource.data.sort((a, b) => {
      const isAsc = sort.direction === "asc";
      switch (sort.active) {
        case "no":
          return this.compare(a.getNumber, b.getNumber, isAsc);
        case "title":
          return this.compare(a.getTitle, b.getTitle, isAsc);
        case "nik":
          return this.compare(a.getNIP, b.getNIP, isAsc);
        case "reviewer":
          return this.compare(a.getReviewer, b.getReviewer, isAsc);
        case "status":
          return this.compare(a.getStatus, b.getStatus, isAsc);
        case "author":
          return this.compare(a.getAuthor, b.getAuthor, isAsc);
        default:
          return 0;
      }
    });
  }

  changeStatus(row: Document) {
    this.dialog.open(FormChangeStatusComponent, {
      data: row,
      width: '1800px',
      disableClose: true
    }).afterClosed().pipe(
      takeUntil(this.destroy$)
    ).subscribe(document => {
      this.OnChangeStatus.emit(document);
    });
  }

  onCreate() {
    this.OnCreate.emit('create');
  }

  rootPage(id: number) {
      this.router.navigate([this.navigateLifeCycle + `/${id}`])
      .then();
  }

}
