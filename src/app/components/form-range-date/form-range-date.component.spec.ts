import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FormRangeDateComponent } from './form-range-date.component';

describe('FormRangeDateComponent', () => {
  let component: FormRangeDateComponent;
  let fixture: ComponentFixture<FormRangeDateComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FormRangeDateComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FormRangeDateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
