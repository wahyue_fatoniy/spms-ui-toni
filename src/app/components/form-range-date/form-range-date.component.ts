import { Component, Inject } from '@angular/core';
import { FormControl, Validators } from '@angular/forms';
import { MAT_DIALOG_DATA } from '@angular/material';

export interface FormRangeDate {
  startDate: Date;
  endDate: Date;
}

@Component({
  selector: 'app-form-range-date',
  templateUrl: './form-range-date.component.html',
  styleUrls: ['./form-range-date.component.css']
})
export class FormRangeDateComponent {
  startDate: FormControl;
  endDate: FormControl;

  constructor(
    @Inject(MAT_DIALOG_DATA)
    public data: FormRangeDate) {
    this.startDate = new FormControl(data.startDate, [Validators.required]);
    this.endDate = new FormControl(data.endDate, [Validators.required]);
  }

}
