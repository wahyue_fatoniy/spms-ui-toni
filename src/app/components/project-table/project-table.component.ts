import {
  Component,
  OnInit,
  ViewChild,
  OnDestroy,
  Input,
  ChangeDetectionStrategy,
  OnChanges,
  EventEmitter,
  Output
} from "@angular/core";
import { SiteWork } from "src/app/models/siteWork.model";
import { projectStatus, Severity } from "src/service/enum";
import { TableConfig, CoreTable } from "src/app/shared/core-table";
import { MatPaginator, MatSort, MatDialog } from "@angular/material";
import { FilterDataPipe } from "src/app/pipes/filterData/filter-data.pipe";
import { DateDurationPipe } from "src/app/pipes/dateDuration/date-duration.pipe";
import { FilterPercentagePipe } from "src/app/pipes/filter-percentage/filter-percentage.pipe";
import { takeUntil } from "rxjs/operators";
import { SiteWorkService } from "src/service/siteWork-service/site-work.service";
import { Router } from "@angular/router";
import { DailyReportService } from "src/service/daily-report-service/daily-report.service";
import { SowService } from "src/service/sow-service/sow.service";
import { Document } from "src/app/models/document.model";
import { DailyReport } from "src/app/models/daliyReport.model";
import { TaskExecution } from "src/app/models/taskExecution.model";
import { XlsProject } from "src/app/models/xls-project.model";
import { XlsExportPipe } from "src/app/pipes/xlsExport/xls-export.pipe";
import { DetailPlannComponent } from "src/app/dashboard-pm/containers/pm-siteWork-table/detail-plann/detail-plann.component";
import { PmSiteWorkValueComponent } from "src/app/dashboard-pm/containers/pm-siteWork-table/pm-site-work-value/pm-site-work-value.component";

const CONFIG: TableConfig = {
  title: "Project",
  columns: [
    "no",
    "projectCode",
    "site",
    "duration",
    "totalOpex",
    "planEd",
    "planTd",
    "planDd",
    "actualEd",
    "actualTd",
    "actualDd",
    "bast",
    "status",
    "severity",
    "startDate",
    "endDate",
    "action"
  ],
  typeFilters: [
    { value: "all", label: "All" },
    { value: "projectCode", label: "Project ID" },
    { value: "site", label: "Site" },
    { value: "startDate", label: "Start Date" },
    { value: "endDate", label: "End Date" },
    { value: "status", label: "Status" },
    { value: "duration", label: "Duration" },
    { value: "severity", label: "Severity" },
    { value: "totalOpex", label: "Total Opex" }
  ]
};

enum poStatus {
  RELEASE = "RELEASE",
  NOT_RELEASE = "NOT_RELEASE"
}

type OpexStatus = "<100" | ">100" | "100";

interface StorageFilterData {
  opex: string;
  status: projectStatus[];
  severity: Severity;
  po: poStatus;
}

type filterType = "severity" | "status" | "poStatus" | "opex";

interface Filter {
  type: filterType;
  source: SiteWork[];
  value: any;
}

@Component({
  selector: "app-project-table",
  templateUrl: "./project-table.component.html",
  styleUrls: ["./project-table.component.css"],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ProjectTableComponent extends CoreTable<SiteWork>
  implements OnInit, OnDestroy, OnChanges {
  @Input() sourceData: SiteWork[] = [];
  @Output() OnClickCell: EventEmitter<number> = new EventEmitter();

  @ViewChild(MatPaginator)
  paginator: MatPaginator;
  @ViewChild(MatSort)
  sort: MatSort;
  public dataPipe: FilterDataPipe = new FilterDataPipe();
  public statusType: projectStatus[] = [
    projectStatus.NOT_STARTED,
    projectStatus.ON_PROGRESS,
    projectStatus.DONE,
    projectStatus.CANCEL
  ];
  public statusLabel = {
    NOT_STARTED: "Not Started",
    ON_PROGRESS: "On Progress",
    DONE: "Done",
    CANCEL: "Cancel"
  };
  public severityLabel = {
    NONE: "None",
    NORMAL: "Normal",
    CRITICAL_I: "Critical I",
    CRITICAL_E: "Critical E",
    MAJOR_I: "Major I",
    MAJOR_E: "Major E"
  };
  public severityStatus: Severity[] = [
    Severity.NORMAL,
    Severity.MAJOR_E,
    Severity.MAJOR_I,
    Severity.CRITICAL_E,
    Severity.CRITICAL_I
  ];
  public poStatus: poStatus[] = [poStatus.RELEASE, poStatus.NOT_RELEASE];
  public ngModelOpex: OpexStatus = "<100";
  public ngModelStatus: projectStatus[] = [projectStatus.ON_PROGRESS];
  public ngModelSeverity: Severity = Severity.NORMAL;
  public ngModelPoStatus: poStatus = poStatus.RELEASE;
  public poStatusLabel = {
    RELEASE: "Release",
    NOT_RELEASE: "Not Release"
  };
  public durationPipe: DateDurationPipe = new DateDurationPipe();
  public percentagePipe: FilterPercentagePipe = new FilterPercentagePipe();
  public xlsExport: XlsExportPipe = new XlsExportPipe();

  constructor(
    public service: SiteWorkService,
    public dialog: MatDialog,
    public router: Router,
    public dailyService: DailyReportService,
    public sowService: SowService
  ) {
    super(CONFIG);
    this.getStorage();
  }

  ngOnInit() {
    this.setDataSource();
  }

  ngOnChanges() {
    this.setDataSource();
  }

  ngOnDestroy() {
    this.destroy$.next(true);
    this.destroy$.complete();
  }

  getStorage() {
    let projectFilter = JSON.parse(localStorage.getItem("projectFilter"));
    if (projectFilter !== null) {
      this.ngModelOpex = projectFilter["opex"];
      this.ngModelPoStatus = projectFilter["po"];
      this.ngModelSeverity = projectFilter["severity"];
      this.ngModelStatus = projectFilter["status"];
    }
  }

  public setDataSource(): void {
    this.sourceData = this.setOpexPercentage(this.sourceData);
    this.sourceData = this.setActualMandays(this.sourceData);
    this.sourceData = this.setPlanMandays(this.sourceData);
    this.sourceData = this.setSiteWorkDuration(this.sourceData);
    this.onFilter({
      type: "status",
      source: this.sourceData,
      value: this.ngModelStatus
    });
    this.datasource.paginator = this.paginator;
    this.datasource.sort = this.sort;
    this.filterDataSource();
  }

  public setBastStatus(row: Document[]): string {
    if (row) {
      let bast = row.find(doc => doc.getType === "BAST");
      return bast ? bast.getStatus : "None";
    }
  }

  public setOpexPercentage(siteWorks: SiteWork[]): SiteWork[] {
    siteWorks = siteWorks.map((siteWork: SiteWork) => {
      const poPercentage = parseFloat(
        ((25 / 100) * siteWork.getPoValue).toFixed(2)
      );
      siteWork["percentageOpex"] = 0;
      if (poPercentage !== 0) {
        siteWork["percentageOpex"] = Math.floor(
          (siteWork.getTotalOpex / poPercentage) * 100
        );
      }
      return siteWork;
    });
    return siteWorks;
  }

  public setSiteWorkDuration(siteWorks: SiteWork[]): SiteWork[] {
    siteWorks = siteWorks.map((siteWork: SiteWork) => {
      siteWork["duration"] = this.durationPipe.transform(siteWork.getStartDate);
      return siteWork;
    });
    return siteWorks;
  }

  public setActualMandays(siteWorks: SiteWork[]): SiteWork[] {
    siteWorks = siteWorks.map((siteWork: SiteWork) => {
      this.dailyService
        .getBySiteWorkId(siteWork.getId)
        .then((dailys: DailyReport[]) => {
          siteWork["actualEd"] = 0;
          siteWork["actualTd"] = 0;
          siteWork["actualDd"] = 0;
          dailys.forEach((daily: DailyReport) => {
            daily.getTaskExecutions.forEach((task: TaskExecution) => {
              switch (task.getResource["resourceType"]) {
                case "ENGINEER":
                  siteWork["actualEd"] += task.getManHour;
                  break;
                case "TECHNICIAN":
                  siteWork["actualTd"] += task.getManHour;
                  break;
                case "DOCUMENTATION":
                  siteWork["actualDd"] += task.getManHour;
                  break;
              }
            });
          });
        });
      return siteWork;
    });
    return siteWorks;
  }

  public setPlanMandays(siteWorks: SiteWork[]): SiteWork[] {
    siteWorks = siteWorks.map((siteWork: SiteWork) => {
      this.sowService
        .getSowHistories(siteWork.getId)
        .pipe(takeUntil(this.destroy$))
        .subscribe((history: Object[]) => {
          siteWork["planEd"] = 0;
          siteWork["planTd"] = 0;
          siteWork["planDd"] = 0;
          history.forEach(sow => {
            siteWork["planEd"] += sow["sow"]["engineerDay"];
            siteWork["planTd"] += sow["sow"]["technicianDay"];
            siteWork["planDd"] += sow["sow"]["documentationDay"];
          });
        });
      return siteWork;
    });
    return siteWorks;
  }

  private setStorage(): void {
    let projectFilter: StorageFilterData = {
      severity: this.ngModelSeverity,
      status: this.ngModelStatus,
      po: this.ngModelPoStatus,
      opex: this.ngModelOpex
    };
    console.log(projectFilter);
    localStorage.setItem("projectFilter", JSON.stringify(projectFilter));
  }

  public filterDataSource(): void {
    this.search.valueChanges
      .pipe(takeUntil(this.destroy$))
      .subscribe(val => (this.datasource.filter = val));
    this.datasource.filterPredicate = (value: SiteWork, keyword: string) => {
      return this.dataPipe.transform(value, keyword, this.typeFilter);
    };
  }

  public onFilter(event: Filter): void {
    let datasource: SiteWork[] = [];
    if (event.type === "status") {
      datasource = this.service.getPriority(event.value, event.source);
    } else if (event.type === "opex") {
      datasource = this.percentagePipe.transform(
        event.value,
        "percentageOpex",
        event.source
      );
    } else if (event.type === "severity") {
      // datasource = event.source.filter(siteWork => {
      //   return siteWork.getSeverity === event.value;
      // });
    } else if (event.type === "poStatus") {
      datasource = this.filterPoStatus(event.value, event.source);
    }
    this.setStorage();
    datasource = this.filterAll(datasource);
    this.datasource.data = datasource;
  }

  public filterAll(datasource: SiteWork[]): SiteWork[] {
    // filter project status
    datasource = this.service.getPriority(this.ngModelStatus, datasource);
    // filter opex project
    datasource = this.percentagePipe.transform(
      this.ngModelOpex,
      "percentageOpex",
      datasource
    );
    // filter po status
    datasource = this.filterPoStatus(this.ngModelPoStatus, datasource);
    // filter severity
    // datasource = datasource.filter(siteWork => {
    //   return siteWork.getSeverity === this.ngModelSeverity;
    // });
    return datasource;
  }

  public filterPoStatus(valInput: poStatus, siteWorks: SiteWork[]): SiteWork[] {
    return siteWorks.filter(siteWork => {
      if (valInput === poStatus.RELEASE) {
        return siteWork.getPoValue !== 0;
      } else {
        return siteWork.getPoValue === 0;
      }
    });
  }

  public sortData(sort): void {
    this.datasource.data = this.datasource.data.sort((a, b) => {
      const isAsc = sort.direction === "asc";
      switch (sort.active) {
        case "projectCode":
          return this.compare(a.getProjectCode, b.getProjectCode, isAsc);
        case "site":
          return this.compare(a.getSite.getName, b.getSite.getName, isAsc);
        case "startDate":
          return this.compare(a.getStartDate, b.getStartDate, isAsc);
        case "endDate":
          return this.compare(a.getEndDate, b.getEndDate, isAsc);
        case "status":
          return this.compare(a.getStatus, b.getStatus, isAsc);
        default:
          return 0;
      }
    });
  }

  public showDetailValue(data: SiteWork): void {
    this.dialog.open(PmSiteWorkValueComponent, {
      data: data,
      width: "500px"
    });
  }

  public showDetailPlann(data: SiteWork): void {
    this.dialog.open(DetailPlannComponent, {
      data: new SiteWork(data),
      width: "1000px",
      disableClose: true
    });
  }

  public onNavigate(id: number): void {
    this.OnClickCell.emit(id);
  }

  public exportExcel(): void {
    if (this.datasource.data.length !== 0) {
      const projectExport = this.datasource.data.map(sitework => {
        return new XlsProject(sitework);
      });
      this.xlsExport.transform('project.xls', projectExport);
    }
  }
}
