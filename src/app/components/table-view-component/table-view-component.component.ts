import { Component, Input, OnInit, ViewChild, ChangeDetectionStrategy, OnChanges, EventEmitter, Output, ElementRef, AfterViewInit } from '@angular/core';
import { Subject } from 'rxjs';
import { FormControl } from '@angular/forms';
import { MatTableDataSource, MatPaginator, MatSort } from '@angular/material';
import { NormalTextPipe } from 'src/app/shared/normal-text.pipe';
import { takeUntil } from 'rxjs/operators';

interface TypeFilter {
  value: any;
  label: string;
}

interface FilterSelect {
  value: any;
  label: string[];
}


interface Column {
  title: string;
  columnDef: string;
}

export interface TableConfig {
  title: string;
  columns: Column[];
  typeFilters: TypeFilter[];
  selectFilter?: boolean;
  selectFilterPlaceholder?: string;
  selectfilterValue?: FilterSelect[];
  width: string;
}

export interface Options {
  config: TableConfig;
  loading?: boolean;
}

@Component({
  selector: 'app-table-view-component',
  templateUrl: './table-view-component.component.html',
  styleUrls: ['./table-view-component.component.css'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class TableViewComponentComponent<T> implements OnInit, OnChanges, AfterViewInit {
  @Input() options: Options;
  @Input() data: T[];
  @Output() OnClickCell = new EventEmitter<any>();
  @Output() OnSelectFilter = new EventEmitter();

  destroy$: Subject<boolean> = new Subject<boolean>();
  search: FormControl = new FormControl();
  selectFilterControl = new FormControl();
  title: string;
  pageSize: number;
  pageIndex = 0;
  action: boolean;
  pageSizes = [10, 25, 50, 100];
  selectedId: number;
  columns: string[] = [];
  typeFilters: TypeFilter[] = [];
  typeFilter = 'all';
  datasource: MatTableDataSource<T> = new MatTableDataSource([]);
  text: NormalTextPipe = new NormalTextPipe();
  searchDate: FormControl = new FormControl();
  maxDate: Date = new Date();
  @ViewChild(MatPaginator)
  paginator: MatPaginator;
  @ViewChild(MatSort)
  sort: MatSort;

  constructor(
    public el: ElementRef
  ) {}

  ngOnInit() {
    this.onSelectFilterChange();
  }

  ngAfterViewInit() {
  }

  ngOnChanges() {
    this.columns = [];
    this.options.config.columns.forEach(column => {
      this.columns = [...this.columns, column.columnDef];
    });
    this.title = this.options.config.title;
    this.typeFilters = this.options.config.typeFilters;
    const PAGE = localStorage.getItem('pageSize');
    this.pageSize = PAGE ? parseInt(PAGE) : 10;
    this.setDatasource();
  }

  setDatasource() {
    this.datasource.data = this.data;
    this.datasource.sort = this.sort;
    this.datasource.paginator = this.paginator;
    this.filterDatasource();
  }

  public filterDatasource() {
    this.search.valueChanges
    .pipe(takeUntil(this.destroy$))
    .subscribe(keyword => {
      this.datasource.filter = keyword;
    });
    this.datasource.filterPredicate = (value: T, keyword: string) => {
      return this.filterPredicate(value, keyword, this.typeFilter);
    };
  }

  onSetPage(event) {
    this.pageSize = event.pageSize;
    this.pageIndex = event.pageIndex;
    localStorage.setItem('pageSize', event.pageSize);
  }

  public filterPredicate(data: any, keyword: string, typeFilter: string): boolean {
    let result;
    if (typeFilter === 'all') {
      result = JSON.stringify(data);
    } else {
      result = data[ typeFilter ];
    }
    result = this.text.transform(result);
    return result.indexOf(this.text.transform(keyword)) !== -1;
  }

  public compare(a, b, isAsc) {
    return (this.text.transform(a) < this.text.transform(b) ? -1 : 1) * (isAsc ? 1 : -1);
  }

  public rootPage(event: any) {
    this.OnClickCell.emit(event);
  }

  public onSelectFilterChange() {
    this.selectFilterControl.valueChanges
    .pipe(takeUntil(this.destroy$))
    .subscribe(val => {
      this.OnSelectFilter.emit(val);
    });
  }

}
