import { AbstractControl } from '@angular/forms';

export function initialLetterValidate(
  control: AbstractControl
): { [key: string]: boolean } | null {
  if (control['value'] !== null) {
    if (
      control['value'].charAt(0) !== control['value'].charAt(0).toUpperCase()
    ) {
      return { letterError: true };
    }
  }
  return null;
}

  export function PasswordValidation(control: AbstractControl): { [key: string]: boolean } | null {
     let password = control.get('password').value; // to get value in input tag
     let confirmPassword = control.get('confirmPassword').value; // to get value in input tag
      if (password !== confirmPassword) {
          control.get('confirmPassword').setErrors( {matchPassword: true} );
      } else {
          return null;
      }
  }
