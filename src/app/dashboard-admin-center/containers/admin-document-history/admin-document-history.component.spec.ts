import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdminDocumentHistoryComponent } from './admin-document-history.component';

describe('AdminDocumentHistoryComponent', () => {
  let component: AdminDocumentHistoryComponent;
  let fixture: ComponentFixture<AdminDocumentHistoryComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdminDocumentHistoryComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdminDocumentHistoryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
