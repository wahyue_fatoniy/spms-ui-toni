import { Component, OnInit, ViewChild, OnDestroy } from "@angular/core";
import { CoreTable, TableConfig } from "src/app/shared/core-table";
import { DocumentLifecycle } from "src/app/models/document.model";
import { Subject } from "rxjs";
import { MatPaginator, MatSort, MatDialog } from "@angular/material";
import { takeUntil } from "rxjs/operators";
import { DocDescComponent } from "./document-desc/document-desc.component";
import { ActivatedRoute } from "@angular/router";
import { DocumentService } from "src/service/document-service/document.service";

const CONFIG: TableConfig = {
  title: "Document History",
  columns: ["no", "number", "title", "date", "status", "reviewer"],
  typeFilters: [
    { value: "all", label: "All" },
    { value: "number", label: "No" },
    { value: "title", label: "Title" },
    { value: "status", label: "Status" },
    { value: "date", label: "Date" },
    { value: "reviewer", label: "Reviewer" }
  ]
};

@Component({
  selector: "app-admin-document-history",
  templateUrl: "./admin-document-history.component.html",
  styleUrls: ["./admin-document-history.component.css"]
})
export class AdminDocumentHistoryComponent extends CoreTable<DocumentLifecycle>
  implements OnInit, OnDestroy {
  public destroy$ = new Subject<boolean>();
  @ViewChild(MatPaginator)
  paginator: MatPaginator;
  @ViewChild(MatSort)
  sort: MatSort;
  docId: number;

  constructor(
    private dialog: MatDialog,
    private activeRoute: ActivatedRoute,
    private documentService: DocumentService
  ) {
    super(CONFIG);
  }

  ngOnInit() {
    this.getSiteWorkData();
    this.getData();
  }

  ngOnDestroy() {
    this.destroy$.next(true);
    this.destroy$.complete();
  }

  getSiteWorkData() {
    this.activeRoute.params.subscribe(data => {
      if (data) {
        this.docId = parseInt(data.documentId);
      }
    });
  }

  getData() {
    this.documentService.getHistoryDoc(this.docId)
    .pipe(takeUntil(this.destroy$))
    .subscribe((data: DocumentLifecycle[]) => {
      this.setDatasource(data);
    });
  }

  public setDatasource(data) {
    this.datasource.data = data;
    this.datasource.paginator = this.paginator;
    this.datasource.sort = this.sort;
    this.filterDatasource();
  }

  public filterDatasource() {
    this.search.valueChanges
      .pipe(takeUntil(this.destroy$))
      .subscribe(keyword => {
        this.datasource.filter = keyword;
      });
    let filterData;
    this.datasource.filterPredicate = (
      value: DocumentLifecycle,
      keyword: string
    ) => {
      filterData = {
        number: value.getDocument.getNumber,
        title: value.getDocument.getTitle,
        date: value.getDate,
        status: value.getDocument.getStatus,
        reviewer: value.getDocument.getReviewer
      };
      return this.filterPredicate(filterData, keyword, this.typeFilter);
    };
  }

  public sortData(sort): void {
    this.datasource.data = this.datasource.data.sort((a, b) => {
      const isAsc = sort.direction === "asc";
      switch (sort.active) {
        case "no":
          return this.compare(a.getDocument.getNumber, b.getDocument.getNumber, isAsc);
        case "title":
          return this.compare(
            a.getDocument.getTitle,
            b.getDocument.getTitle,
            isAsc
          );
        case "date":
          return this.compare(a.getDate, b.getDate, isAsc);
        case "status":
          return this.compare(
            a.getDocument.getStatus,
            b.getDocument.getStatus,
            isAsc
          );
        case "reviewer":
          return this.compare(
            a.getDocument.getReviewer,
            b.getDocument.getReviewer,
            isAsc
          );
        default:
          return 0;
      }
    });
  }

  showDescDoc(document: DocumentLifecycle) {
    console.log(document);
    this.dialog.open(DocDescComponent, {
      data: document,
      disableClose: true,
      width: "850px"
    });
  }
}
