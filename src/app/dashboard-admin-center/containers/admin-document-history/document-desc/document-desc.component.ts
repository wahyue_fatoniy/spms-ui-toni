import { Component, OnInit, Inject } from '@angular/core';
import { MAT_DIALOG_DATA } from '@angular/material';
import { DocumentLifecycle } from 'src/app/models/document.model';

@Component({
  selector: 'app-document-desc',
  templateUrl: './document-desc.component.html',
  styleUrls: ['./document-desc.component.css']
})
export class DocDescComponent implements OnInit {

  constructor(
    @Inject(MAT_DIALOG_DATA) public data: DocumentLifecycle
  ) { }

  ngOnInit() {
  }

}
