import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdminDocumentFormComponent } from './admin-document-form.component';

describe('AdminDocumentFormComponent', () => {
  let component: AdminDocumentFormComponent;
  let fixture: ComponentFixture<AdminDocumentFormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdminDocumentFormComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdminDocumentFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
