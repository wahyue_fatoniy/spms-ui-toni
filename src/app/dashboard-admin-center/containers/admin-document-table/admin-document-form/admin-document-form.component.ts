import { Component, OnInit, Inject, OnDestroy } from '@angular/core';
import { MAT_DIALOG_DATA } from '@angular/material';
import { CustomerService } from 'src/service/customer-service/customer.service';
import { AdminService } from 'src/service/admin-service/admin.service';
import { Subject, combineLatest } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { Customer } from 'src/app/models/customer.model';
import { Admin } from 'src/app/models/admin.model';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Document, DocumentStatus, DocumentLifecycle } from 'src/app/models/document.model';
import { AdminCenter } from 'src/app/models/adminCenter.model';

interface FormDocumentData {
  data: Document;
  author: AdminCenter;
}

export interface OnCreateDoc {
  document: Document;
  documentLifeCycle: DocumentLifecycle;
}

@Component({
  selector: 'app-admin-document-form',
  templateUrl: './admin-document-form.component.html',
  styleUrls: ['./admin-document-form.component.css']
})
export class AdminDocumentFormComponent implements OnInit, OnDestroy {

  private destroy = new Subject<boolean>();
  public customerData: Customer[] = [];
  public adminData: Admin[] = [];
  public form: FormGroup;
  onCreateDoc: OnCreateDoc = {
    document: new Document(),
    documentLifeCycle: new DocumentLifecycle()
  };

  constructor(
    @Inject(MAT_DIALOG_DATA) public data: FormDocumentData,
    private customerService: CustomerService,
    private formBuilder: FormBuilder
  ) {
    console.log(data);
  }

  ngOnInit() {
    this.getData();
    this.buildForm();
    this.onFormValid();
  }

  ngOnDestroy() {
    this.destroy.next(true);
    this.destroy.complete();
  }

  public getData() {
    this.customerService.subscriptionData()
    .pipe(takeUntil(this.destroy))
    .subscribe((val: Customer[]) => {
      this.customerData = val;
    });
  }

  public buildForm() {
    this.form = this.formBuilder.group({
      id: [this.data.data.getNumber],
      number: [this.data.data.getNumber, Validators.required],
      title: [this.data.data.getTitle, Validators.required],
      author: [ {value: this.data.author.getParty['name'], disabled: true}, Validators.required],
      reviewer: [this.data.data.getReviewer, Validators.required]
    });
  }

  public onFormValid() {
    this.form.statusChanges
    .pipe(takeUntil(this.destroy))
    .subscribe(status => {
      if (status === 'VALID') {
        let document = new Document();
        document.setId = this.form.value['id'];
        document.setNumber = this.form.value['number'];
        document.setReviewer = this.form.value['reviewer'];
        document.setStatus = DocumentStatus.create;
        document.setTitle = this.form.value['title'];
        document.setAuthor = this.data.author.getParty['name'];
        document.setNip = this.data.author.getNIP;
        this.onCreateDoc.document = document;

        // set documentLifeCycle
        let documentLifeCycle = new DocumentLifecycle();
        documentLifeCycle.setDescription = null;
        documentLifeCycle.setDocument = document;
        documentLifeCycle.setStatusDoc = DocumentStatus.create;
        documentLifeCycle.setDate = null;
        this.onCreateDoc.documentLifeCycle = documentLifeCycle;
      }
    });
  }
}
