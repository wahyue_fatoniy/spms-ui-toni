import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdminDocumentTableComponent } from './admin-document-table.component';

describe('AdminDocumentTableComponent', () => {
  let component: AdminDocumentTableComponent;
  let fixture: ComponentFixture<AdminDocumentTableComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdminDocumentTableComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdminDocumentTableComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
