import { Component, OnInit, OnDestroy, ViewChild } from "@angular/core";
import { CoreTable, TableConfig } from "src/app/shared/core-table";
import { Document, DocumentType } from "src/app/models/document.model";
import { MatDialog, MatPaginator, MatSort } from "@angular/material";
import {
  AdminDocumentFormComponent,
  OnCreateDoc
} from "./admin-document-form/admin-document-form.component";
import { CustomerService } from "src/service/customer-service/customer.service";
import { combineLatest, Subject } from "rxjs";
import { takeUntil } from "rxjs/operators";
import { Router, ActivatedRoute } from "@angular/router";
import { DocumentService } from "src/service/document-service/document.service";
import { SiteWorkService } from "src/service/siteWork-service/site-work.service";
import { SiteWork } from "src/app/models/siteWork.model";
import { AdminCenterService } from "src/service/admin-center/admin-center.service";
import { AdminCenter } from "src/app/models/adminCenter.model";
import { AAAService } from "src/app/shared/aaa/aaa.service";
import { FormStatusUpdateComponent, FormUpdateStatus } from "../../presentational/form-status-update/form-status-update.component";

const CONFIG: TableConfig = {
  title: "Documents",
  columns: [
    "no",
    "number",
    "title",
    "status",
    "author",
    "nik",
    "reviewer",
    "action"
  ],
  typeFilters: [
    { value: "all", label: "All" },
    { value: "no", label: "No" },
    { value: "title", label: "Title" },
    { value: "author", label: "Author" },
    { value: "nik", label: "NIK" },
    { value: "reviewer", label: "Reviewer" }
  ]
};

@Component({
  selector: "app-admin-document-table",
  templateUrl: "./admin-document-table.component.html",
  styleUrls: ["./admin-document-table.component.css"]
})
export class AdminDocumentTableComponent extends CoreTable<Document>
  implements OnInit, OnDestroy {
  private destroy = new Subject<boolean>();
  @ViewChild(MatPaginator)
  paginator: MatPaginator;
  @ViewChild(MatSort)
  sort: MatSort;
  adminData: AdminCenter[] = [];
  siteworkId: number = null;
  sitework: SiteWork;

  constructor(
    private dialog: MatDialog,
    private customerService: CustomerService,
    private adminService: AdminCenterService,
    private router: Router,
    private documentService: DocumentService,
    private activeRoute: ActivatedRoute,
    private siteworkService: SiteWorkService,
    private aaaService: AAAService
  ) {
    super(CONFIG);
  }

  ngOnInit() {
    this.getSiteWorkId();
    this.getData();
    this.subscriptionData();
  }

  ngOnDestroy() {
    this.destroy.next(true);
    this.destroy.complete();
  }

  getSiteWorkId() {
    this.activeRoute.params.subscribe(data => {
      if (data) {
        this.siteworkId = parseInt(data.siteworkId);
      }
    });
  }

  public getData() {
    this.siteworkService
      .subscriptionData()
      .subscribe((siteworks: SiteWork[]) => {
        this.sitework = siteworks.find(val => val.getId === this.siteworkId);
        this.setDatasource(new SiteWork(this.sitework).getDocuments);
      });
    combineLatest(this.customerService.getData(), this.adminService.getData())
      .pipe(takeUntil(this.destroy))
      .subscribe(() => {});
  }

  getAuthAdmins(adminCenters: AdminCenter[]): void {
    this.adminData = adminCenters.filter(_set => {
      return (
        !this.aaaService.isAuthorized("read:project:{province:$}:{job:$}") ||
        this.aaaService.isAuthorized(
          `read:project:{province:$}:{job:${_set.getId}}`
        )
      );
    });
    console.log(this.adminData);
  }

  subscriptionData() {
    this.adminService
      .subscriptionData()
      .pipe(takeUntil(this.destroy))
      .subscribe(admins => {
        this.getAuthAdmins(admins);
      });
  }

  public setDatasource(docs: Document[] = []) {
    this.datasource.data = docs.filter(doc => doc.getType === "BAST");
    this.datasource.paginator = this.paginator;
    this.datasource.sort = this.sort;
    this.filterDatasource();
  }

  public filterDatasource() {
    this.search.valueChanges.pipe(takeUntil(this.destroy)).subscribe(val => {
      this.datasource.filter = val;
    });

    this.datasource.filterPredicate = (value: Document, keyword: string) => {
      return this.filterPredicate(value, keyword, this.typeFilter);
    };
  }

  public sortData(sort): void {
    this.datasource.data = this.datasource.data.sort((a, b) => {
      const isAsc = sort.direction === "asc";
      switch (sort.active) {
        case "no":
          return this.compare(a.getNumber, b.getNumber, isAsc);
        case "title":
          return this.compare(a.getTitle, b.getTitle, isAsc);
        case "nik":
          return this.compare(a.getNIP, b.getNIP, isAsc);
        case "reviewer":
          return this.compare(a.getReviewer, b.getReviewer, isAsc);
        case "status":
          return this.compare(a.getStatus, b.getStatus, isAsc);
        case "author":
          return this.compare(a.getAuthor, b.getAuthor, isAsc);
        default:
          return 0;
      }
    });
  }

  public onCreate() {
    this.dialog
      .open(AdminDocumentFormComponent, {
        data: {
          data: new Document(),
          author: new AdminCenter(this.adminData[0])
        },
        width: "800px",
        disableClose: true
      })
      .afterClosed()
      .pipe(takeUntil(this.destroy))
      .subscribe((val: OnCreateDoc) => {
        console.log(val);
        if (val) {
          val.document.setType = DocumentType.BAST;
          this.sitework.setDocument = [
            ...this.sitework.getDocuments,
            val.document
          ];
          this.siteworkService
            .update(this.sitework)
            .then((sitework: SiteWork) => {
              if (sitework) {
                let newDocument =
                  sitework.getDocuments[sitework.getDocuments.length - 1];
                val.documentLifeCycle.setDocument = newDocument;
                this.documentService
                  .createHistoryDoc(val.documentLifeCycle)
                  .toPromise()
                  .then(history => {
                    console.log("create history doc", history);
                  });
              }
            });
        }
      });
  }

  public changeStatus(documentId: number): void {
    this.dialog
      .open(FormStatusUpdateComponent, {
        data: this.datasource.data[0],
        width: "850px",
        disableClose: true
      })
      .afterClosed()
      .pipe(takeUntil(this.destroy))
      .subscribe((val: FormUpdateStatus) => {
        console.log(val);
        if (val) {
          this.sitework.setDocument = this.sitework.getDocuments.map(
            document => {
              return document.getId === val.document.getId
                ? val.document
                : document;
            }
          );
          combineLatest(
            this.siteworkService.update(this.sitework),
            this.documentService.createHistoryDoc(val.documentLifeCycle)
          )
            .pipe(takeUntil(this.destroy$))
            .subscribe(document => {
              console.log(document);
            });
        }
      });
  }

  public rootPage(event: number): void {
    this.router
      .navigate([`/home/dashboard-admin-center/document-history/${event}`])
      .then(root => {
        console.log(root);
      });
  }
}
