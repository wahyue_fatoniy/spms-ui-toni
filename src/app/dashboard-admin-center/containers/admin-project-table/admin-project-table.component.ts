import { Component, OnInit, OnDestroy, ViewChild } from '@angular/core';
import { CoreTable, TableConfig } from 'src/app/shared/core-table';
import { SiteWork } from 'src/app/models/siteWork.model';
import { RegionService } from 'src/service/region-service/region.service';
import { Region } from 'src/app/models/region.model';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { SiteWorkService } from 'src/service/siteWork-service/site-work.service';
import { MatPaginator, MatSort } from '@angular/material';
import { Router } from '@angular/router';
import { projectStatus } from 'src/service/enum';

const CONFIG: TableConfig = {
  title: "Project",
  columns: [
    "no",
    "projectId",
    "site",
    'startDate',
    'endDate'
  ],
  typeFilters: [
    { value: "all", label: "All" },
    { value: "projectCode", label: "Project ID" },
    { value: "site", label: "Site" },
    { value: "startDate", label: "Start Date" },
    { value: "endDate", label: "End Date" }
  ]
};

@Component({
  selector: 'app-admin-project-table',
  templateUrl: './admin-project-table.component.html',
  styleUrls: ['./admin-project-table.component.css']
})
export class AdminProjectTableComponent extends CoreTable<SiteWork> implements OnInit, OnDestroy {
  public regionResource: Region[] = [];
  public destroy$ = new Subject<boolean>();
  @ViewChild(MatPaginator)
  paginator: MatPaginator;
  @ViewChild(MatSort)
  sort: MatSort;

  constructor(
    private router: Router,
    private regionService: RegionService,
    private siteWorkService: SiteWorkService
  ) {
    super(CONFIG);
  }

  ngOnInit() {
    this.getData();
  }

  ngOnDestroy() {
    this.destroy$.next(true);
    this.destroy$.complete();
  }

  public get loading() {
    return this.siteWorkService.getLoading;
  }

  public getData() {
    // get sitework done and not bast
    this.siteWorkService.findByStatus([projectStatus.DONE])
    .pipe(takeUntil(this.destroy$))
    .subscribe(() => {
      this.notificationData();
    });
  }

  public notificationData() {
    this.siteWorkService.subscriptionData()
    .pipe(takeUntil(this.destroy$))
    .subscribe((siteWorks: SiteWork[]) => {
      this.setDatasourceData(siteWorks);
    });
  }

  public setDatasourceData(siteWorks: SiteWork[]) {
    this.datasource.data = siteWorks;
    this.datasource.sort = this.sort;
    this.datasource.paginator = this.paginator;
    this.filterDatasource();
  }

  public filterDatasource() {
    this.search.valueChanges
    .pipe(takeUntil(this.destroy$))
    .subscribe(keyword => {
      this.datasource.filter = keyword;
    });
    let filterData;
    this.datasource.filterPredicate = (value: SiteWork, keyword: string) => {
       filterData = {
        projectCode: value.getProjectCode,
        site: value.getSite.getName,
        startDate: value.getStartDate,
        endDate: value.getEndDate
      };
      return this.filterPredicate(filterData, keyword, this.typeFilter);
    };
  }

  public sortData(sort): void {
    this.datasource.data = this.datasource.data.sort((a, b) => {
      const isAsc = sort.direction === "asc";
      switch (sort.active) {
        case "no":
          return this.compare(a.getProjectCode, b.getProjectCode, isAsc);
        case "title":
          return this.compare(a.getSite, b.getSite, isAsc);
        case "nik":
          return this.compare(a.getStartDate, b.getStartDate, isAsc);
        case "reviewer":
          return this.compare(a.getEndDate, b.getEndDate, isAsc);
        default:
          return 0;
      }
    });
  }

  public rootPage(projectId: number) {
    this.router.navigate([`/home/dashboard-admin-center/admin-document/${projectId}`]).then();
  }
}
