import { Component, OnInit } from '@angular/core';
import { ColumnConfig, actionType, ColumnType, OnEvent } from 'src/app/shared/table-builder';
import { ActivatedRoute } from '@angular/router';
import { DocumentService } from 'src/service/document-service/document.service';
import { DocumentLifecycle, DocumentStatus } from 'src/app/models/document.model';

interface TableModel {
  id: number;
  number: string;
  title: string;
  date: Date;
  status: DocumentStatus;
  reviewer: string;
  description: string;
}

@Component({
  selector: 'app-document-lifecycle',
  template: `
    <app-documentlifecycle-table
      [data]="data"
      [title]="title"
      [actionTypes]="actionTypes"
      (onEvent)="onEvent($event)"
      [columnConfigs]="columnConfigs"
    >
    </app-documentlifecycle-table>

  `
})
export class DocumentLifecycleComponent implements OnInit {
  columnConfigs: ColumnConfig[];
  actionTypes: actionType[];
  title: string;
  data: any[];

  docId: number;

  constructor(
    public activeRoute: ActivatedRoute,
    public docService: DocumentService
  ) {

    this.data = [];
    this.title = 'Documents History';
    this.actionTypes = [];
    this.columnConfigs = [
      { label: 'No', key: 'no', columnType: ColumnType.string },
      { label: 'Number', key: 'number', columnType: ColumnType.string },
      { label: 'Title', key: 'title', columnType: ColumnType.string },
      { label: 'Date', key: 'date', columnType: ColumnType.date, format: 'mediumDate'},
      { label: 'Status', key: 'status', columnType: ColumnType.string },
      { label: 'Reviewer', key: 'reviewer', columnType: ColumnType.string },
      { label: 'Description', key: 'description', columnType: ColumnType.string },
    ];
   }

  ngOnInit() {
    this.getDocId();
    this.getServiceData();
  }

  getDocId() {
    this.activeRoute.params.subscribe(data => {
      if (data) {
        this.docId = parseInt(data.id);
      }
    });
  }

  getServiceData() {
    this.docService.getHistoryDoc(this.docId).subscribe((val: DocumentLifecycle[]) => {
      this.data = val.map(doc => {
        return this.setTableModel(doc);
      });
    });
  }

  subscriptionData() { }

  setTableModel(doc: DocumentLifecycle) {
    return <TableModel> {
      id: doc.getId,
      number: doc.getDocument.getNumber,
      title: doc.getDocument.getTitle,
      date: new Date(doc.getDate),
      status: doc.getStatusDoc,
      reviewer: doc.getDocument.getReviewer,
      description: doc.getDescription
    }
  }

  onEvent(event: OnEvent) {
  }

}
