import { Component, OnInit, OnDestroy } from '@angular/core';
import { ColumnType, ColumnConfig, actionType, OnEvent } from 'src/app/shared/table-builder';
import { MatDialog } from '@angular/material';
import { Document, DocumentStatus, DocumentLifecycle } from 'src/app/models/document.model';
import { Router, ActivatedRoute } from '@angular/router';
import { FormDocumentComponent } from '../../presentational/form-document/form-document.component';
import { DocumentService } from 'src/service/document-service/document.service';
import { SiteWorkService } from 'src/service/siteWork-service/site-work.service';
import { SiteWork } from 'src/app/models/siteWork.model';
import { CustomerService } from 'src/service/customer-service/customer.service';
import { combineLatest, Subject, forkJoin } from 'rxjs';
import { AdminCenter } from 'src/app/models/adminCenter.model';
import { AAAService } from 'src/app/shared/aaa/aaa.service';
import { AdminCenterService } from 'src/service/admin-center/admin-center.service';
import { FormStatusUpdateComponent } from '../../presentational/form-status-update/form-status-update.component';
import { pipe } from '@angular/core/src/render3/pipe';
import { takeUntil } from 'rxjs/operators';
import { DocumentHistoryService } from 'src/service/document-history-service/document-history.service';

interface TableModel {
  id: number;
  number: string;
  title: string;
  status: string;
  author: string;
  nip: string;
  reviewer: string;
}

@Component({
  selector: 'app-document',
  template: `
    <app-document-table
      [data]="data"
      [title]="title"
      [actionTypes]="actionTypes"
      (onEvent)="onEvent($event)"
      [columnConfigs]="columnConfigs"
    >
    </app-document-table>
  `,
  styleUrls: ['./document.component.css']
})
export class DocumentComponent implements OnInit, OnDestroy {
  columnConfigs: ColumnConfig[];
  actionTypes: actionType[];
  title: string;
  data: TableModel[];

  projectId: number;
  sitework: SiteWork;
  documents: Document[];
  admin: AdminCenter;
  destroy$: Subject<boolean>;

  constructor(
    public dialog: MatDialog,
    public router: Router,
    public activeRoute: ActivatedRoute,
    public customerService: CustomerService,
    public siteworkService: SiteWorkService,
    public aaaService: AAAService,
    public adminService: AdminCenterService,
    public documentService: DocumentService,
    private historyDocService: DocumentHistoryService
  ) {
    this.data = [];
    this.title = 'Documents';
    this.actionTypes = ['ADD', 'UPDATE', 'VIEW'];
    this.columnConfigs = [
      { label: 'No', key: 'no', columnType: ColumnType.string },
      { label: 'Number', key: 'number', columnType: ColumnType.string },
      { label: 'Title', key: 'title', columnType: ColumnType.string },
      { label: 'Status', key: 'status', columnType: ColumnType.string },
      // { label: 'Author', key: 'author', columnType: ColumnType.string },
      // { label: 'NIK', key: 'nip', columnType: ColumnType.string },
      { label: 'Reviewer', key: 'reviewer', columnType: ColumnType.string },
    ];
    this.destroy$ = new Subject();
  }

  ngOnInit() {
    this.getProjectId();
    this.getServiceData();
    this.getSubscriptionData();
  }

  ngOnDestroy() {
    this.destroy$.next(true);
    this.destroy$.complete();
  }

  getAuthAdmins(adminCenters: AdminCenter[]) {
    return adminCenters.filter(_set => {
      return (
        !this.aaaService.isAuthorized("read:project:{province:$}:{job:$}") ||
        this.aaaService.isAuthorized(
          `read:project:{province:$}:{job:${_set.getId}}`
        )
      );
    });
  }

  getProjectId() {
    this.activeRoute.params.subscribe(data => {
      if (data) {
        this.projectId = parseInt(data.id);
      }
    });
  }

  getServiceData() {
    combineLatest(
      this.customerService.getData(),
      this.adminService.getData(),
      this.documentService.findByPid(this.projectId)
    )
      .pipe(takeUntil(this.destroy$))
      .subscribe(val => { });
  }

  getSubscriptionData() {
    combineLatest(
      this.adminService.subscriptionData(),
      this.documentService.subscriptionData(),
      this.siteworkService.subscriptionData(),
    )
      .subscribe((val: [AdminCenter[], Document[], SiteWork[]]) => {
        this.data = val[1].map(document => {
          return this.setTableModel(document);
        });
        this.sitework = val[2].find(sitework => sitework.getId === this.projectId);
        this.admin = this.getAuthAdmins(val[0])[0];
        this.documents = val[1];
      })
  }

  setTableModel(document: Document) {
    return <TableModel>{
      id: document.getId,
      number: document.getNumber,
      title: document.getTitle,
      status: document.getStatus,
      author: document.getAuthor,
      nip: document.getNIP,
      reviewer: document.getReviewer
    }
  }

  onEvent(event: OnEvent) {
    switch (event.type) {
      case 'ADD':
        this.addDocument();
        break;
      case 'UPDATE':
        this.updateStatusDoc(event.value);
        break;
      case 'VIEW':
        this.redirectDocument(event.value);
        break;
    }
  }

  addDocument() {
    this.dialog.open(FormDocumentComponent, {
      data: { data: new Document(), customers: this.customerService.subscriptionData(), adminCenter: this.admin },
      width: '900px',
      disableClose: true,
    }).afterClosed().subscribe((val: Document) => {
      if (val) {
        val.setStatus = DocumentStatus.create;
        
        this.documentService.createByPid(this.sitework ? this.sitework.getId : null, val)
        .toPromise()
        .then((result: Document) => {
          const historyDoc = new DocumentLifecycle();
          historyDoc.setDocument = result;
          historyDoc.setStatusDoc = DocumentStatus.create;

          this.historyDocService.createHistoryDoc(historyDoc)
          .toPromise()
          .then((history) => { console.log(history) })
        }).catch(err => { })
      }
    });
  }

  updateStatusDoc(doc: TableModel) {
    const document = this.findDocument(doc);
    this.dialog.open(FormStatusUpdateComponent, {
      width: '900px',
      data: document,
      disableClose: true,
    }).afterClosed().subscribe((val: {document: Document, documentLifeCycle: DocumentLifecycle}) => {
      if (val) {
        forkJoin(
          this.documentService.updateById(doc.id, this.sitework.getId, val.document),
          this.historyDocService.createHistoryDoc(val.documentLifeCycle)  
        ).toPromise()
        .then(result => { console.log(result) })
        .catch(err => { console.log(err) })
      }
    })
  }

  findDocument(model: TableModel) {
    return this.documents.find(doc => {
      return doc.getId === model.id;
    })
  }

  redirectDocument(value) {
    this.router.navigate([`home/dashboard-admin-center/document-history/${value['id']}`])
  }
}
