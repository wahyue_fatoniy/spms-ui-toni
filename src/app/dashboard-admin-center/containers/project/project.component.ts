import { Component, OnInit } from '@angular/core';
import { actionType, ColumnConfig, ColumnType, OnEvent } from 'src/app/shared/table-builder';
import { SiteWork } from 'src/app/models/siteWork.model';
import { Router } from '@angular/router';
import { SiteWorkService } from 'src/service/siteWork-service/site-work.service';
import { projectStatus } from 'src/service/enum';
import { Status } from 'src/models/enum/enum';

interface TableModel {
  id: number;
  projectId: string;
  assignmentId: string;
  projectManager: string;
  startDate: string;
  endDate: string;
}

@Component({
  selector: 'app-project',
  template: `
  <app-project-table
    [data]="data"
    [title]="title"
    [actionTypes]="actionTypes"
    (onEvent)="onEvent($event)"
    [columnConfigs]="columnConfigs">
  </app-project-table>
`
})
export class ProjectComponent implements OnInit {
  data: any[];
  title: string;
  actionTypes: actionType[];
  columnConfigs: ColumnConfig[];

  constructor(
    public router: Router,
    public siteworkService: SiteWorkService
  ) {
    this.data = [];
    this.title = 'Project Done';
    this.actionTypes = ['VIEW'];
    this.columnConfigs = [
      { label: 'No. ', key: 'no', columnType: ColumnType.string },
      { label: 'Project ID', key: 'projectId', columnType: ColumnType.string },
      { label: 'Assignment ID', key: 'assignmentId', columnType: ColumnType.string },
      { label: 'Project Manager', key: 'projectManager', columnType: ColumnType.string },
      { label: 'Start Date', key: 'startDate', columnType: ColumnType.date, format: 'shortDate' },
      { label: 'End Date', key: 'endDate', columnType: ColumnType.date, format: 'shortDate' },
    ];
  }

  ngOnInit() {
    this.getSubscriptionData();
    this.getServiceData();
  }

  getServiceData() {
    this.siteworkService.findByStatus([projectStatus.DONE]).subscribe();
  }

  getSubscriptionData() {
    this.siteworkService.subscriptionData().subscribe((val: SiteWork[]) => {
      this.data = val.map(sitework => {
        return this.setTableModel(sitework);
      });
    });
  }

  setTableModel(val: SiteWork) {
    return <TableModel> {
      id: val.getId,
      projectId: val.getProjectCode,
      // assignmentId: val.getAssignmentId,
      projectManager: val.getProjectManager ? val.getProjectManager.getParty['name'] : '',
      startDate: val.getStartDate,
      endDate: val.getEndDate,
    }
  }

  onEvent(event: OnEvent) {
    if (event.type === 'VIEW') {
      this.router.navigate([`home/dashboard-admin-center/document/${event.value['id']}`])
    }
  }

}
