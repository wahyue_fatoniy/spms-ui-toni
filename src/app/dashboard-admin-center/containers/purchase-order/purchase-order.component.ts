import { Component, OnInit, OnDestroy } from '@angular/core';
import { actionType, ColumnConfig, ColumnType, OnEvent } from 'src/app/shared/table-builder';
import { PurchaseOrder } from 'src/app/models/purchaseOrder.model';
import { MatDialog } from '@angular/material';
import { FormPurchaseOrderComponent } from '../../presentational/form-purchase-order/form-purchase-order.component';
import { PurchaseOrderService } from 'src/service/purchasorder-service/purchase-order.service';
import { SiteWorkService } from 'src/service/siteWork-service/site-work.service';
import { CustomerService } from 'src/service/customer-service/customer.service';
import { PartnerService } from 'src/service/partner-service/partner.service';
import { SalesService } from 'src/service/sales-service/sales.service';
import { combineLatest, Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { ToastrService } from 'ngx-toastr';
import { SiteWork } from 'src/app/models/siteWork.model';
import { FormProjectListComponent } from '../../presentational/form-project-list/form-project-list.component';

interface TableModel {
  id: number;
  poNumber: string;
  customer: string;
  partner: string;
  sales: string;
  poValue: number;
  totalInvoice: number;
  publishDate: Date;
  expiredDate: Date;
  status: string;
  siteworks: SiteWork[];
}

@Component({
  selector: 'app-purchase-order',
  template: `
    <app-purchase-order-table
      [data]="data"
      [title]="title"
      [actionTypes]="actionTypes"
      (onEvent)="onEvent($event)"
      [columnConfigs]="columnConfigs">
    </app-purchase-order-table>
  `,
})

export class PurchaseOrderComponent implements OnInit, OnDestroy {
  data: any[];
  title: string;
  actionTypes: actionType[];
  columnConfigs: ColumnConfig[];

  destroy$: Subject<boolean>;
  purchaseOrders: PurchaseOrder[];

  constructor(
    public dialog: MatDialog,
    public poService: PurchaseOrderService,
    public siteworkService: SiteWorkService,
    public customerService: CustomerService,
    public partnerService: PartnerService,
    public salesService: SalesService,
    public toastService: ToastrService
  ) {
    this.data = [];
    this.title = 'Purchase Order';
    this.actionTypes = ['ADD', 'UPDATE', 'DELETE', 'VIEW'];
    this.columnConfigs = [
      { label: 'No. ', key: 'no', columnType: ColumnType.string },
      { label: 'PO Number. ', key: 'poNumber', columnType: ColumnType.string },
      { label: 'Customer', key: 'customer', columnType: ColumnType.string },
      { label: 'Sales', key: 'sales', columnType: ColumnType.string },
      { label: 'PO Value', key: 'poValue', columnType: ColumnType.currency, local: 'Rp. ' },
      { label: 'Total Invoice', key: 'totalInvoice', columnType: ColumnType.currency, local: 'Rp. ' },
      { label: 'Publish Date', key: 'publishDate', columnType: ColumnType.date, format: 'shortDate' },
      { label: 'Expired Date', key: 'expiredDate', columnType: ColumnType.date, format: 'shortDate' },
      { label: 'Status', key: 'status', columnType: ColumnType.status },
    ];
  }

  ngOnInit() {
    this.destroy$ = new Subject();
    this.getSubscriptionData();
    this.getServiceData();
  }

  ngOnDestroy() {
    this.destroy$.next(true);
    this.destroy$.complete();
  }

  getServiceData() {
    combineLatest(
      this.siteworkService.getData(),
      this.customerService.getData(),
      this.partnerService.getData(),
      this.salesService.getData(),
      this.poService.getData(),
      )
      .pipe(takeUntil(this.destroy$))
      .subscribe(val => { })
  }

  getSubscriptionData() {
    combineLatest(
     this.poService.subscriptionData() 
    ).subscribe(val => {
      this.purchaseOrders = val[0];
      this.data = val[0].map(purchase => {
        return this.setTableModel(purchase);
      });
    })
  }

  setTableModel(purchaseOrder: PurchaseOrder): TableModel {
    return {
      id: purchaseOrder.getId,
      poNumber: purchaseOrder.getPoNumber,
      customer: purchaseOrder.getCustomer.getParty ? purchaseOrder.getCustomer.getParty['name'] : '',
      partner: purchaseOrder.getPartner.getParty ? purchaseOrder.getPartner.getParty['name'] : '',
      sales: purchaseOrder.getSales.getParty ? purchaseOrder.getSales.getParty['name'] : '',
      poValue: purchaseOrder.getPoValue ? purchaseOrder.getPoValue : 0,
      totalInvoice: purchaseOrder.getTotalInvoice ? purchaseOrder.getTotalInvoice : 0,
      publishDate: new Date(purchaseOrder.getPublishDate),
      expiredDate: new Date(purchaseOrder.getExpiredDate),
      status: purchaseOrder.getStatus,
      siteworks: purchaseOrder.getSiteWorks ? purchaseOrder.getSiteWorks : []
    };
  }

  onEvent(event: OnEvent) {
    switch (event.type) {
      case 'ADD':
        this.createPurhaseOrder();
      break;
      case 'UPDATE':
        this.updatePurchaseOrder(event.value);
        break;
      case 'DELETE':
        break;
      case 'VIEW':
        this.showProjectList(event.value);
        break;
    }
  }

  createPurhaseOrder() {
    this.dialog.open(FormPurchaseOrderComponent, {
      width: '1200px',
      disableClose: true,
      data: {
        data: new PurchaseOrder(), 
        customers: this.customerService.subscriptionData(),
        partners: this.partnerService.subscriptionData(),
        sales: this.salesService.subscriptionData(),
        siteworks: this.siteworkService.subscriptionData()
      }
    }).afterClosed().subscribe((val: PurchaseOrder) => {
      if (val) {
          this.poService.create(val).then(po => {
            this.toastService.success('Success create Purchase Order', 'Success');
          }).catch(err => {
            this.toastService.error('Failed create Purchase Order', 'Failed');
          });
      }
    })
  }

  updatePurchaseOrder(model: TableModel) {
    const po = this.findPurchaseOrder(model);
    this.dialog.open(FormPurchaseOrderComponent, {
      width: '1200px',
      disableClose: true,
      data: {
        data: po, 
        customers: this.customerService.subscriptionData(),
        partners: this.partnerService.subscriptionData(),
        sales: this.salesService.subscriptionData(),
        siteworks: this.siteworkService.subscriptionData()
      }
    }).afterClosed().subscribe(val => {
      if (val) {
          this.poService.update(val).then(po => {
            this.toastService.success('Success update Purchase Order', 'Success');
            console.log(po);
          }).catch(err => {
            this.toastService.error('Failed update Purchase Order', 'Failed');
          });
      }
    })
  }

  showProjectList(model: TableModel) {
    this.dialog.open(FormProjectListComponent, {
      data: model.siteworks,
      width: '1000px',
      disableClose: true
    });
  }

  findPurchaseOrder(model: TableModel) {
    return this.purchaseOrders.find(purchase => {
      return purchase.getId === model.id;
    });
  }
}
