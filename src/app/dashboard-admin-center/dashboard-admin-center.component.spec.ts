import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DashboardAdminCenterComponent } from './dashboard-admin-center.component';

describe('DashboardAdminCenterComponent', () => {
  let component: DashboardAdminCenterComponent;
  let fixture: ComponentFixture<DashboardAdminCenterComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DashboardAdminCenterComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DashboardAdminCenterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
