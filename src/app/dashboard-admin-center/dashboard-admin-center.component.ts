import { Component, OnInit, OnDestroy } from "@angular/core";
import { Router, NavigationEnd } from "@angular/router";
import { filter } from "rxjs/operators";
import { Location } from "@angular/common";
import { Subscription } from "rxjs";
import { TabsPipePipe, TabsMenu } from "../pipes/tabs-pipe/tabs-pipe.pipe";
import { NumberInStringPipe } from "../pipes/numberInString/number-in-string.pipe";

@Component({
  selector: "app-dashboard-admin-center",
  template: `
    <nav mat-tab-nav-bar style="margin-top:-75px;">
      <a
        style="text-decoration: none"
        mat-tab-link
        *ngFor="let link of links"
        [routerLink]="link.path"
        routerLinkActive
        #rla="routerLinkActive"
        [active]="rla.isActive"
      >
        <img [src]="link?.img" style="margin-bottom:7px;" />&nbsp;
        {{ link.title }}
      </a>
    </nav>
    <div class="container-home">
      <router-outlet></router-outlet>
    </div>
  `,
})
export class DashboardAdminCenterComponent implements OnInit, OnDestroy {
  subscriptions: Subscription;
  tabsPipe: TabsPipePipe = new TabsPipePipe();
  getNumberPipe: NumberInStringPipe = new NumberInStringPipe();

  links: TabsMenu[] = [
    {
      path: "purchase-order",
      title: "Purchase Order",
      img: "/assets/img/collection/budget.png"
    },
    {
      path: "project",
      title: "Project",
      img: "/assets/img/collection/project1.png"
    }
  ];

  constructor(private router: Router, private location: Location) {}

  ngOnInit() {
    this.subscriptions = this.router.events
      .pipe(filter(event => event instanceof NavigationEnd))
      .subscribe(({ urlAfterRedirects }: NavigationEnd) => {
        const path = urlAfterRedirects.split("/").pop();
        const url = urlAfterRedirects.split("/");
        const param = this.getNumberPipe.transform(urlAfterRedirects);
        if (path === "purchase-order" || path === "project") {
          this.links = [
            {
              path: "purchase-order",
              title: "Purchase Order",
              img: "/assets/img/collection/budget.png"
            },
            {
              path: "project",
              title: "Project",
              img: "/assets/img/collection/project1.png"
            }
          ];
        } else if (
          `${url[url.length - 2]}` === `document`
        ) {
          this.links = [
            {
              path: "purchase-order",
              title: "Purchase Order",
              img: "/assets/img/collection/budget.png"
            },
            {
              path: "project",
              title: "Project",
              img: "/assets/img/collection/project1.png"
            },
            {
              path: `document/${param}`,
              title: "Document",
              img: "/assets/img/collection/form.png"
            },
          ];
        } else if (
          `${url[url.length - 2]}` === `document-history`
        ) {
          this.links = [
            ...this.links,
            {
              path: `document-history/${param}`,
              title: "Document History",
              img: "/assets/img/collection/NOT_STARTED.png"
            }
          ];
        }
      });
    if (
      this.location.path() !==
      "/home/dashboard-admin-center/purchase-order"
    ) {
      this.router
        .navigate(["/home/dashboard-admin-center/purchase-order"])
        .catch(err => console.log(err));
    }
  }

  ngOnDestroy() {
    if (this.subscriptions) {
      this.subscriptions.unsubscribe();
    }
  }
}
