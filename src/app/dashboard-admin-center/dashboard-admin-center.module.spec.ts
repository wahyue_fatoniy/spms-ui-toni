import { DashboardAdminCenterModule } from './dashboard-admin-center.module';

describe('DashboardAdminCenterModule', () => {
  let dashboardAdminCenterModule: DashboardAdminCenterModule;

  beforeEach(() => {
    dashboardAdminCenterModule = new DashboardAdminCenterModule();
  });

  it('should create an instance', () => {
    expect(dashboardAdminCenterModule).toBeTruthy();
  });
});
