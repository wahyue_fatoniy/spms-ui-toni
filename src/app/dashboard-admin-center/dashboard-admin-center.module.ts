import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PurchaseOrderComponent } from './containers/purchase-order/purchase-order.component';
import { ProjectComponent } from './containers/project/project.component';
import { DocumentComponent } from './containers/document/document.component';
import { ProjectTableComponent } from './presentational/project-table/project-table.component';
import { PurchaseOrderTableComponent } from './presentational/purchase-order-table/purchase-order-table.component';
import { DocumentTableComponent } from './presentational/document-table/document-table.component';
import { Routes, RouterModule } from '@angular/router';
import { DashboardAdminCenterComponent } from './dashboard-admin-center.component';
import { SharedModule } from '../shared/shared.module';
import { FormPurchaseOrderComponent } from './presentational/form-purchase-order/form-purchase-order.component';
import { DocumentLifecycleComponent } from './containers/document-lifecycle/document-lifecycle.component';
import { DocumentlifecycleTableComponent } from './presentational/documentlifecycle-table/documentlifecycle-table.component';
import { FormDocumentComponent } from './presentational/form-document/form-document.component';
import { FormDocumentLifecycleComponent } from './presentational/form-document-lifecycle/form-document-lifecycle.component';
import { AdminDocumentFormComponent } from './containers/admin-document-table/admin-document-form/admin-document-form.component';
import { DocumentDescComponent } from './presentational/document-desc/document-desc.component';
import { AdminDocumentHistoryComponent } from './containers/admin-document-history/admin-document-history.component';
import { AdminDocumentTableComponent } from './containers/admin-document-table/admin-document-table.component';
import { DocDescComponent } from './containers/admin-document-history/document-desc/document-desc.component';
import { FormStatusUpdateComponent } from './presentational/form-status-update/form-status-update.component';
import { FormProjectListComponent } from './presentational/form-project-list/form-project-list.component';

const routers: Routes = [
  {
    path: '', component: DashboardAdminCenterComponent, children: [
      { path: '', redirectTo: 'purchase-order', pathMatch: 'full' },
      { path: 'purchase-order', component: PurchaseOrderComponent },
      { path: 'project', component: ProjectComponent },
      { path: 'document/:id', component: DocumentComponent },
      { path: 'document-history/:id', component: DocumentLifecycleComponent }
    ]
  }
]

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(routers),
    SharedModule
  ],
  declarations: [
    DashboardAdminCenterComponent,
    PurchaseOrderComponent,
    ProjectComponent,
    DocumentComponent, 
    ProjectTableComponent, 
    PurchaseOrderTableComponent, 
    DocumentTableComponent, 
    FormPurchaseOrderComponent,
    DocumentLifecycleComponent,
    DocumentlifecycleTableComponent,
    FormDocumentComponent,
    FormDocumentLifecycleComponent,
    FormProjectListComponent,

    AdminDocumentFormComponent,
    DocumentDescComponent,
    AdminDocumentHistoryComponent,
    FormStatusUpdateComponent,
    AdminDocumentTableComponent,
    DocDescComponent,
  ],
  entryComponents: [
    FormPurchaseOrderComponent,
    FormDocumentComponent,
    FormDocumentLifecycleComponent,
    FormStatusUpdateComponent,
    FormProjectListComponent
  ]
})
export class DashboardAdminCenterModule { }
