import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DocumentDescComponent } from './document-desc.component';

describe('DocumentDescComponent', () => {
  let component: DocumentDescComponent;
  let fixture: ComponentFixture<DocumentDescComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DocumentDescComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DocumentDescComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
