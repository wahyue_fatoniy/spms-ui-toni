import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DocumentlifecycleTableComponent } from './documentlifecycle-table.component';

describe('DocumentlifecycleTableComponent', () => {
  let component: DocumentlifecycleTableComponent;
  let fixture: ComponentFixture<DocumentlifecycleTableComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DocumentlifecycleTableComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DocumentlifecycleTableComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
