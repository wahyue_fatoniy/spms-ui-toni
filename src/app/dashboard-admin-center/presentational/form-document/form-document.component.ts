import { Component, OnInit, Inject } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material';
import { Document, DocumentType } from 'src/app/models/document.model';
import { Customer } from 'src/app/models/customer.model';
import { FormlyFieldConfig } from '@ngx-formly/core';
import { FormGroup } from '@angular/forms';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { AdminCenter } from 'src/app/models/adminCenter.model';

interface FormData {
  data: Document,
  customers: Observable<Customer[]>,
  adminCenter: AdminCenter;
}

interface FormModel {
  id: number;
  noDocument: string;
  title: string;
  reviewer: string;
  documentType: string;
}

@Component({
  selector: 'app-form-document',
  templateUrl: './form-document.component.html',
  styleUrls: ['./form-document.component.css']
})
export class FormDocumentComponent implements OnInit {
  fields: FormlyFieldConfig[];
  form: FormGroup;
  model:FormModel;

  adminCenter: AdminCenter;

  constructor(
    @Inject(MAT_DIALOG_DATA) public formData: FormData,
    public dialogRef: MatDialogRef<any>
  ) {
    this.form = new FormGroup({});
    this.fields = formData.data.formFieldsConfig;
    this.adminCenter = this.formData.adminCenter;
    this.setFormModel(formData.data);
  }

  ngOnInit() {
    this.hideAuthor();
    this.setReviewerOptions();
    this.setTypeDocOptions();
  }

  setFormModel(document: Document) {
    this.model = {
      id: document.getId,
      noDocument: document.getNumber,
      title: document.getTitle,
      reviewer: document.getReviewer,
      documentType: document.getType
    }
  }

  hideAuthor() {
    this.fields = this.fields.map(field => {
      if (field.id === '2') {
        field.fieldGroup = field.fieldGroup.map(member => {
          member.hideExpression = true;
          return member;
        });
      }
      return field;
    });
  }

  setReviewerOptions() {
    this.fields = this.fields.map(field => {
      if (field.id === '3') {
        field.fieldGroup = field.fieldGroup.map(group => {
          if (group.key === 'reviewer') {
            group.templateOptions.options = this.formData.customers.pipe(
              map(customers => {
                return customers.map(customer => {
                  return { value: customer.getParty['name'], label: customer.getParty['name'] }
                });
              })
            )
          }
          return group;
        });
      }
      return field;
    });
  }

  setTypeDocOptions() {
    this.fields = this.fields.map(field => {
      if (field.id === '3') {
        field.fieldGroup = field.fieldGroup.map(group => {
          if (group.key === 'documentType') {
            group.templateOptions.options = [
              { value: DocumentType.ATP, label: DocumentType.ATP },
              { value: DocumentType.COMPLETENESS_ATP, label: DocumentType.COMPLETENESS_ATP },
              { value: DocumentType.BAUT, label: DocumentType.BAUT },
              { value: DocumentType.BAST, label: DocumentType.BAST },
            ];
          }
          return group;
        });
      }
      return field;
    });
  }

  onClose() {
    const document = new Document();
    document.setId = this.model.id;
    document.setNumber = this.model.noDocument;
    document.setAuthor = this.adminCenter.getParty ? this.adminCenter.getParty['name'] : '';
    document.setNip = this.adminCenter.getNIP;
    document.setReviewer = this.model.reviewer,
    document.setType = DocumentType[this.model.documentType];
    document.setTitle = this.model.title;
    console.log(document);
    this.dialogRef.close(document);
  }

}
