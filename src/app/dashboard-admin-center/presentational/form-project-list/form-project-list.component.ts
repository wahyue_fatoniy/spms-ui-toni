import { Component, OnInit, Inject } from '@angular/core';
import { MAT_DIALOG_DATA } from '@angular/material';
import { SiteWork } from 'src/app/models/siteWork.model';

@Component({
  selector: 'app-form-project-list',
  templateUrl: './form-project-list.component.html',
  styleUrls: ['./form-project-list.component.css']
})
export class FormProjectListComponent implements OnInit {
  displayedColumns: string[];
  datasource: SiteWork[];
  constructor(
    @Inject(MAT_DIALOG_DATA) public siteworks: SiteWork[]
  ) {
    console.log(siteworks);
    this.datasource = siteworks ? siteworks : [];
    this.displayedColumns = ['no', 'projectId', 'site', 'value', 'status']
  }

  ngOnInit() {
  }

}
