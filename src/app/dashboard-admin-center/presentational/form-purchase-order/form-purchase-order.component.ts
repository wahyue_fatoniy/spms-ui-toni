import { Component, OnInit, Inject } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { FormlyFieldConfig } from '@ngx-formly/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material';
import { PurchaseOrder } from 'src/app/models/purchaseOrder.model';
import { Observable, combineLatest } from 'rxjs';
import { Customer } from 'src/app/models/customer.model';
import { Partner } from 'src/app/models/partner.model';
import { Sales } from 'src/app/models/sales.model';
import { SiteWork } from 'src/app/models/siteWork.model';

interface FormData {
  data: PurchaseOrder;
  customers: Observable<Customer[]>;
  partners: Observable<Partner[]>;
  sales: Observable<Sales[]>;
  siteworks: Observable<SiteWork[]>;
}

interface FormModel {
  id: number;
  poNumber: string;
  poValue: number;
  publishDate: Date;
  expiredDate: Date;
  customer: number;
  partner: number;
  sales: number;
  siteworks: number[];
}

@Component({
  selector: 'app-form-purchase-order',
  templateUrl: './form-purchase-order.component.html',
  styleUrls: ['./form-purchase-order.component.css']
})
export class FormPurchaseOrderComponent implements OnInit {
  form: FormGroup;
  model: FormModel;
  fields: FormlyFieldConfig[];

  siteworks: SiteWork[];
  customers: Customer[];
  partners: Partner[];
  sales: Sales[];

  displayedColumns: string[];
  sourceTable: SiteWork[];

  constructor(
    @Inject(MAT_DIALOG_DATA) public formData: FormData,
    public dialogRef: MatDialogRef<any>
  ) { 
    this.form = new FormGroup({});
    this.fields = formData.data.getFormFieldConfigs;
    this.sourceTable = [];
    this.displayedColumns = ['no', 'projectCode', 'site', 'value']
  }

  ngOnInit() { 
    this.getSubscriptionData();
    this.setDisabledPoValue();
    this.setFormModal();
    this.onAddSitework();
  }

  setFormModal() {
    this.model = {
      id: this.formData.data.getId,
      poNumber: this.formData.data.getPoNumber,
      poValue: this.formData.data.getPoValue ? this.formData.data.getPoValue : 0,
      expiredDate: this.formData.data.getExpiredDate ? new Date(this.formData.data.getExpiredDate) : null,
      publishDate: this.formData.data.getPublishDate ? new Date(this.formData.data.getPublishDate) : null,
      customer: this.formData.data.getCustomer ? this.formData.data.getCustomer.getId : null,
      partner: this.formData.data.getPartner ? this.formData.data.getPartner.getId : null,
      sales: this.formData.data.getSales ? this.formData.data.getSales.getId : null,
      siteworks: this.formData.data.getSiteWorks ? this.formData.data.getSiteWorks.map(siteWork => {
        return siteWork.getId;
      }) : []
    };
    this.sourceTable = this.formData.data.getSiteWorks ? this.formData.data.getSiteWorks : [];
  }

  getSubscriptionData() {
    combineLatest(
      this.formData.customers,
      this.formData.partners,
      this.formData.sales,
      this.formData.siteworks
    ).subscribe(val => {
      this.customers = val[0];
      this.partners = val[1];
      this.sales = val[2];
      this.siteworks = val[3];

      this.formData.data.setCustomerOptions = val[0];
      this.formData.data.setPartnerOptions = val[1];
      this.formData.data.setSalesOptions = val[2];
      this.formData.data.setSiteworkOptions = val[3];
    });
  }

  onAddSitework() {
    this.form.valueChanges.subscribe(val => {
      if (val['siteworks']) {
        this.sourceTable = this.siteworks.filter(sitework => {
          return val['siteworks'].findIndex(id => id === sitework.getId) !== -1;
        });
      }
    });
  }

  onValueChanges(i, val: string) {
    let poValue = 0;
    this.sourceTable[i].setPoValue = parseInt(val);
    this.fields = this.fields.map(field => {
      if (field.id === '3') {
          field.fieldGroup = field.fieldGroup.map(member => {
            if (member.key === 'poValue') {
              this.sourceTable.forEach(source => {
                poValue += source.getPoValue ? source.getPoValue : 0;
                member.formControl.setValue(poValue);
              });
            }
            return member;
          });
      }
      return field;
    });
  }


  setDisabledPoValue() {
    this.fields = this.fields.map(field => {
      if (field.id === '3') {
          field.fieldGroup = field.fieldGroup.map(member => {
            if (member.key === 'poValue') {
                member.templateOptions.disabled = true;
            }
            return member;
          });
      }
      return field;
    });
  }

  onClose() {
    const purchaseOrder = new PurchaseOrder();
    purchaseOrder.setId = this.model.id;
    purchaseOrder.setPoNumber = this.model.poNumber;
    purchaseOrder.setPublishDate = new Date(this.model.publishDate);
    purchaseOrder.setExpiredDate = new Date(this.model.expiredDate);
    purchaseOrder.setPoValue = this.model.poValue;
    purchaseOrder.setCustomer = this.customers.find(customer => {
      return customer.getId === this.model.customer;
    })
    purchaseOrder.setPartner = this.partners.find(partner => {
      return partner.getId === this.model.partner;
    });
    purchaseOrder.setSales = this.sales.find(sales => {
      return sales.getId === this.model.sales;
    });
    purchaseOrder.setSiteWorks = this.sourceTable;
    console.log(purchaseOrder);
    this.dialogRef.close(purchaseOrder);
  }

}
