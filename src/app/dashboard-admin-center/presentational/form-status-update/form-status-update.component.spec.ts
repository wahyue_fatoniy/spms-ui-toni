import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FormStatusUpdateComponent } from './form-status-update.component';

describe('FormStatusUpdateComponent', () => {
  let component: FormStatusUpdateComponent;
  let fixture: ComponentFixture<FormStatusUpdateComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FormStatusUpdateComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FormStatusUpdateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
