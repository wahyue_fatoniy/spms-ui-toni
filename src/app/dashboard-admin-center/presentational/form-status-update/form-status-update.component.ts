import { Component, OnInit, Inject, OnDestroy } from '@angular/core';
import { MAT_DIALOG_DATA } from '@angular/material';
import { DocumentLifecycle, Document, DocumentStatus } from 'src/app/models/document.model';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { takeUntil } from 'rxjs/operators';
import { Subject } from 'rxjs';

export interface FormUpdateStatus {
  document: Document;
  documentLifeCycle: DocumentLifecycle;
}

@Component({
  selector: 'app-form-status-update',
  templateUrl: './form-status-update.component.html',
  styleUrls: ['./form-status-update.component.css']
})
export class FormStatusUpdateComponent implements OnInit, OnDestroy {
  form: FormGroup;
  destroy = new Subject<boolean>();
  formData: FormUpdateStatus = {
    document: new Document(),
    documentLifeCycle: new DocumentLifecycle()
  };
  status: DocumentStatus[] = [
    DocumentStatus.create,
    DocumentStatus.submit,
    DocumentStatus.reject,
    DocumentStatus.revision,
    DocumentStatus.approve,
  ];

  statusLabel = {
    CREATE: 'Create',
    SUBMIT: 'Submit',
    REJECT: 'Reject',
    REVISION: 'Revision',
    APPROVE: 'approve'

  };

  constructor(
    @Inject(MAT_DIALOG_DATA) public data: Document,
    public formBuilder: FormBuilder
  ) { }

  ngOnInit(): void {
    this.initializeForm();
    this.onFormValid();
  }

  ngOnDestroy() {
    this.destroy.next(true);
    this.destroy.complete();
  }

  initializeForm(): void {
    this.form = this.formBuilder.group({
      status: [this.data.getStatus, Validators.required],
      description: [null, Validators.required]
    });
  }

  onFormValid(): void {
    this.form.statusChanges
    .pipe(takeUntil(this.destroy))
    .subscribe(status => {
      if (status === 'VALID') {
        this.data.setStatus = this.form.value['status'];
        let documentLifeCycle = new DocumentLifecycle();
        documentLifeCycle.setDescription = this.form.value['description'];
        documentLifeCycle.setDocument = this.data;
        documentLifeCycle.setStatusDoc = this.form.value['status'];
        this.formData.document = this.data;
        this.formData.documentLifeCycle = documentLifeCycle;
      }
    });
  }
}
