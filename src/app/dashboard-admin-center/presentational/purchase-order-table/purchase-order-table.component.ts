import { Component, OnInit, EventEmitter, Output, ViewChild, Input } from '@angular/core';
import { TableBuilder, OnEvent, ColumnConfig, actionType } from 'src/app/shared/table-builder';
import { MatPaginator, MatSort } from '@angular/material';

@Component({
  selector: 'app-purchase-order-table',
  templateUrl: './purchase-order-table.component.html',
  styleUrls: ['./purchase-order-table.component.css']
})
export class PurchaseOrderTableComponent extends TableBuilder implements OnInit {
  @ViewChild(MatPaginator) _paginator: MatPaginator;
  @ViewChild(MatSort) _sort: MatSort;

  @Output() onEvent: EventEmitter<OnEvent>;
  @Input() columnConfigs: ColumnConfig[];
  @Input() actionTypes: actionType[];
  @Input() title: string;
  @Input() data: any[];

  public selectField: boolean;
  public selectedData: any;
  public rowIndex: number;

  constructor() {
    super();
    this.onEvent = new EventEmitter();
  }

  ngOnInit() {
    this.paginator = this._paginator;
    this.sort = this._sort;
    this.setDisplayedColumns();
    this.setTypeFilters();
  }

  ngOnChanges() {
    this.datasource.data = this.data;
  }

  ngOnDestroy() {
    this.subscription.next(true);
    this.subscription.complete();
  }

  onEventClick(event: OnEvent) {
    event.value = this.selectedData;
    this.onEvent.emit(event);
  }

  onSelectRow(element) {
    this.selectedData = element;
    this.selectField = true;
  }

  onDoubleClick(element) {
    this.selectField = false;
    this.selectedData = element;
  }

}
