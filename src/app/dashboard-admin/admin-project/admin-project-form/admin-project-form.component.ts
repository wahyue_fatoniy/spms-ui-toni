import { Component, OnInit, Inject, OnDestroy } from "@angular/core";
import { SiteWork } from "../../../shared/models/site-work/site-work";
import { MAT_DIALOG_DATA, MatTableDataSource } from "@angular/material";
import { ProjectManagerService } from "../../../shared/models/project-manager/project-manager.service";
import { SiteService } from "../../../shared/models/site/site.service";
import { FormControl, Validators } from "@angular/forms";
import { ProjectManager } from "../../../shared/models/project-manager/project-manager";
import { Site } from "../../../shared/models/site/site";
import { AdminService } from "../../../shared/models/admin/admin.service";
import { Admin } from "../../../shared/models/admin/admin";
import { AAAService } from "src/app/shared/aaa/aaa.service";
import { SelectSearchPipe } from "src/app/shared/select-search.pipe";
import { AdminCenter } from "src/app/shared/models/admin-center/adminCenter.model";
import { AdminCenterService } from "src/app/shared/models/admin-center/admin-center.service";
import {
  Reference,
  referenceType
} from "src/app/shared/models/references/references";

interface AdminProjectForm {
  value: SiteWork;
  pm: ProjectManagerService;
  site: SiteService;
  admin: AdminService;
  adminCenter: AdminCenterService;
}

interface ReferenceControl {
  type: FormControl;
  desc: FormControl;
}

@Component({
  selector: "app-admin-project-form",
  templateUrl: "./admin-project-form.component.html",
  styles: []
})
export class AdminProjectFormComponent implements OnInit, OnDestroy {
  siteData: Site[] = [];

  projectCode: FormControl = new FormControl(this.data.value.getProjectCode(), [
    Validators.required
  ]);

  site: FormControl = new FormControl(this.data.value.getSite().getId(), [
    Validators.required
  ]);

  pm: FormControl = new FormControl(
    this.data.value.getProjectManager().getId(),
    [Validators.required]
  );

  admin: FormControl = new FormControl(
    {
      value: this.data.value.getAdmin().getId(),
      disabled: this.aaa.isAuthorized("read:project:{province:$}:{job:$}")
    },
    [Validators.required]
  );

  startDate: FormControl = new FormControl(this.data.value.getStartDate());

  endDate: FormControl = new FormControl(this.data.value.getEndDate());
  adminCenter: FormControl = new FormControl(
    this.data.value.getAdminCenter()
      ? this.data.value.getAdminCenter().getId()
      : null,
    [Validators.required]
  );
  // search site
  searchSite: FormControl;

  sites: Site[] = [];

  searchPipe: SelectSearchPipe;

  displayedColumns = ["no", "type", "description", "delete"];

  referenceDatasource: MatTableDataSource<Reference>;

  referenceOption = [
    { label: "Assignment Id", value: referenceType.assignmentId },
    { label: "Activity Id", value: referenceType.activityId },
    { label: "Reference", value: referenceType.reference }
  ];

  referenceControl: ReferenceControl[] = [];

  constructor(
    @Inject(MAT_DIALOG_DATA) public data: AdminProjectForm,
    private aaa: AAAService
  ) {
    this.referenceDatasource = new MatTableDataSource(
      data.value.getReferences()
    );
    this.searchPipe = new SelectSearchPipe();
    this.searchSite = new FormControl();
    this.siteData = this.getSites();
  }

  ngOnInit() {
    this.initialReferenceControl();
    this.projectCode.valueChanges.subscribe(_set =>
      this.data.value.setProjectCode(_set)
    );
    this.site.valueChanges.subscribe(_id => {
      if (_id) {
        this.data.value.setSite(this.data.site.getById(_id));
      }
    });
    this.pm.valueChanges.subscribe(_id => {
      if (_id) {
        this.data.value.setProjectManager(this.data.pm.getById(_id));
      }
    });
    this.searchSite.valueChanges.subscribe(
      _key =>
        (this.sites = this.searchPipe.transform(
          this.data.site.getTables(),
          _key
        ))
    );
    this.sites = this.data.site.getTables();
    this.admin.valueChanges.subscribe(_admin => {
      this.data.value.setAdmin(this.data.admin.getById(_admin));
    });
    this.startDate.valueChanges.subscribe(_set =>
      this.data.value.setStartDate(_set)
    );
    this.endDate.valueChanges.subscribe(_set =>
      this.data.value.setEndDate(_set)
    );
    this.adminCenter.valueChanges.subscribe(_set => {
      this.data.value.setAdminCenter(this.data.adminCenter.getById(_set));
    });
    console.log(this.data.value);
  }

  public initialReferenceControl() {
    this.data.value.getReferences().forEach((reference, i) => {
      console.log(reference);
      this.referenceControl[i] = {
        type: new FormControl(
          reference.getReferenceType(),
          Validators.required
        ),
        desc: new FormControl(reference.getDescription(), Validators.required)
      };
    });
  }

  ngOnDestroy() {
    this.data.value.setReference(this.referenceDatasource.data);
  }

  public getPM(): ProjectManager[] {
    return this.data.pm.getTables();
  }

  public getAdmins(): Admin[] {
    return this.data.admin.getTables();
  }

  public getSites(): Site[] {
    return this.data.site.getTables();
  }

  public getAdminsCenter(): AdminCenter[] {
    return this.data.adminCenter.getTables();
  }

  public invalid(): Boolean {
    let referenceDesc = this.referenceControl.findIndex(ctrl => {
      return ctrl.desc.invalid;
    });
    let referenceType = this.referenceControl.findIndex(ctrl => {
      return ctrl.type.invalid;
    });
    return (
      this.projectCode.invalid ||
      this.site.invalid ||
      this.pm.invalid ||
      this.admin.invalid ||
      referenceType !== -1 ||
      referenceDesc !== -1
    );
  }

  public onDeleteReference(index: number): void {
    this.referenceDatasource.data = this.referenceDatasource.data.filter(
      (data, i) => {
        return i !== index;
      }
    );
    this.referenceControl = this.referenceControl.filter((data, i) => {
      return i !== index;
    });
  }

  public onAddReference(): void {
    this.referenceDatasource.data = [
      ...this.referenceDatasource.data,
      new Reference()
    ];
    this.referenceControl = [
      ...this.referenceControl,
      {
        type: new FormControl(null, Validators.required),
        desc: new FormControl(null, Validators.required)
      }
    ];
  }

  public onInputRefDesc(index, desc): void {
    this.referenceDatasource.data[index].setDescription(desc);
    this.referenceControl[index].desc.setValue(desc);
  }

  public onSelectReference(index, typeReference): void {
    this.referenceDatasource.data[index].setReferenceType(typeReference);
    this.referenceControl[index].type.setValue(typeReference);
  }
}
