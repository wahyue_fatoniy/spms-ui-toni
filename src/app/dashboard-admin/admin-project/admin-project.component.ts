import { Component, OnInit, ViewChild, OnDestroy, ElementRef } from "@angular/core";
import { FormControl } from "@angular/forms";
import { TableConfig, CoreTable } from "../../shared/core-table";
import { SiteWork, StatusType } from "../../shared/models/site-work/site-work";
import { SiteWorkService } from "../../shared/models/site-work/site-work.service";
import { AdminService } from "../../shared/models/admin/admin.service";
import { ProjectManagerService } from "../../shared/models/project-manager/project-manager.service";
import { MatPaginator, MatSort, Sort, MatDialog } from "@angular/material";
import { DatePipe } from "@angular/common";
import { Admin } from "../../shared/models/admin/admin";
import { Site } from "../../shared/models/site/site";
import { ProjectManager } from "../../shared/models/project-manager/project-manager";
import { SiteService } from "../../shared/models/site/site.service";
import { combineLatest } from "rxjs";
import { AdminProjectFormComponent } from "./admin-project-form/admin-project-form.component";
import { DialogComponent } from "../../shared/dialog/dialog.component";
import { RoleType } from "../../shared/models/role/role";
import { AAAService } from "src/app/shared/aaa/aaa.service";
import { RegionService } from "src/app/shared/models/region/region.service";
import { Region } from "src/app/shared/models/region/region";
import { AdminCenterService } from "src/app/shared/models/admin-center/admin-center.service";
import { AdminCenter } from "src/app/shared/models/admin-center/adminCenter.model";
import { takeUntil } from "rxjs/operators";
import { FormListComponent, FormList } from "src/app/components/form-list/form-list.component";
import { Reference } from "src/app/shared/models/references/references";
import { ImportCsvService } from "src/service/import-csv/import-csv.service";
const CONFIG: TableConfig = {
  title: "Project",
  columns: [
    "no",
    "projectCode",
    "site",
    "pm",
    "status",
    "progress",
    "start",
    "end"
  ],
  typeFilters: [
    { value: "all", label: "All" },
    { value: "projectCode", label: "Project Code" },
    { value: "site", label: "Site" },
    { value: "pm", label: "Project Manager" },
    { value: "status", label: "Status" },
    { value: "start", label: "Start Date" },
    { value: "end", label: "End Date" }
  ]
};

@Component({
  selector: "app-admin-project",
  templateUrl: "./admin-project.component.html",
  styles: []
})
export class AdminProjectComponent extends CoreTable<SiteWork>
  implements OnInit, OnDestroy {
  constructor(
    public siteWork: SiteWorkService,
    public adminService: AdminService,
    public pm: ProjectManagerService,
    public site: SiteService,
    public region: RegionService,
    private dialog: MatDialog,
    public aaa: AAAService,
    public adminCenterService: AdminCenterService,
    public importCsv: ImportCsvService
  ) {
    super(CONFIG);
  }
  public statusLabel = {
    NOT_STARTED: "Not Started",
    ON_PROGRESS: "On Progress",
    DONE: "Done",
    CANCEL: "Cancel"
  };
  public dashboardModel: FormControl = new FormControl([
    StatusType.NotStarted,
    StatusType.Progress
  ]);
  public dashboardSettings: StatusType[] = [
    StatusType.NotStarted,
    StatusType.Progress,
    StatusType.Done,
    StatusType.Cancel
  ];
  @ViewChild(MatPaginator)
  paginator: MatPaginator;
  @ViewChild(MatSort)
  sort: MatSort;
  date = new DatePipe("id");
  formatType = "dd MMMM yyyy";
  currentUserId: number = null;
  admin: Admin;
  collapse = 'show';
  fileControl = new FormControl();

  getSite(row: SiteWork): string {
    return row.getSite().getName();
  }

  getPM(row: SiteWork): string {
    const pm = row.getProjectManager();
    return pm ? pm.getParty().getName() : null;
  }

  getStart(row: SiteWork): string {
    return this.date.transform(row.getStartDate(), this.formatType);
  }

  getEnd(row: SiteWork): string {
    return this.date.transform(row.getEndDate(), this.formatType);
  }

  getAdmin(row: SiteWork): string {
    const admin = row.getAdmin() ? row.getAdmin() : null;
    return admin ? admin.getParty().getName() : null;
  }

  getLoading(): boolean {
    return (
      this.siteWork.getLoading() ||
      this.adminService.getLoading() ||
      this.pm.getLoading()
    );
  }

  ngOnInit() {
    this.initialize();
    combineLatest(
      this.adminService.findAll(),
      this.pm.findAll(),
      this.region.findAll(),
      this.adminCenterService.findAll()
    )
      .pipe(takeUntil(this.destroy$))
      .subscribe(_service => {
        combineLatest(
          this.site.findByRegions(this.getAuthRegions()),
          this.siteWork.findByAdmins(this.getAuthAdmins())
        )
          .pipe(takeUntil(this.destroy$))
          .subscribe(_success => {
            console.log(_success);
            this.setData();
          });
      });
  }

  ngOnDestroy() {
    this.destroy$.next(true);
    this.destroy$.complete();
  }

  setData() {
    this.datasource.data = this.siteWork.getPriority(this.dashboardModel.value);
    console.log(this.datasource.data);
  }

  getAuthRegions(): Region[] {
    const regions = this.region.getTables().filter(_set => {
      return (
        this.aaa.isAuthorized(`read:region:{province:${_set.getId()}}`) ||
        !this.aaa.isAuthorized("read:region:{province:$}")
      );
    });
    console.log("Authority regions", regions);
    return regions;
  }

  getAuthAdmins(): Admin[] {
    const admins = this.adminService.getTables().filter(_set => {
      const region = _set.getRegion() ? _set.getRegion() : null;
      const id = region ? region.getId() : null;
      return (
        !this.aaa.isAuthorized("read:project:{province:$}:{job:$}") ||
        this.aaa.isAuthorized(
          `read:project:{province:${id}}:{job:${_set.getId()}}`
        )
      );
    });
    console.log("Authority admin", admins);
    return admins;
  }

  // initialize from table
  sortData(sort: Sort) {
    if (!sort.active || sort.direction === "") {
      this.setData();
      return;
    }
    this.datasource.data = this.datasource.data.sort((a, b) => {
      const isAsc = sort.direction === "asc";
      switch (sort.active) {
        case "projectCode":
          return this.compare(a.getProjectCode(), b.getProjectCode(), isAsc);
        case "site":
          return this.compare(this.getSite(a), this.getSite(b), isAsc);
        case "pm":
          return this.compare(this.getPM(a), this.getPM(b), isAsc);
        case "status":
          return this.compare(a.getStatus(), b.getStatus(), isAsc);
        case "start":
          return this.compare(this.getStart(a), this.getEnd(b), isAsc);
        case "end":
          return this.compare(this.getEnd(a), this.getEnd(b), isAsc);
        case "progress":
          return this.compare(a.getProgress(), b.getProgress(), isAsc);
        default:
          return 0;
      }
    });
  }

  initialize() {
    const accessAll = this.aaa.isAuthorized(
      "read:project:{province:$}:{job:$}"
    );
    if (!accessAll) {
      this.pushColumn({ value: "admin", label: "Admin" });
    }
    this.action = this.aaa.isAuthorized("write:project");
    this.enabledAction();
    this.datasource.paginator = this.paginator;
    this.datasource.sort = this.sort;
    this.datasource.filterPredicate = (set: SiteWork, keyword: string) => {
      const value = {
        projectCode: set.getProjectCode(),
        site: this.getSite(set),
        pm: this.getPM(set),
        status: set.getStatus(),
        start: this.getStart(set),
        end: this.getEnd(set)
      };
      return this.filterPredicate(value, keyword, this.typeFilter);
    };
    this.search.valueChanges
      .pipe(takeUntil(this.destroy$))
      .subscribe(_search => (this.datasource.filter = _search));
    this.dashboardModel.valueChanges
      .pipe(takeUntil(this.destroy$))
      .subscribe(_set => {
        this.datasource.data = this.siteWork.getPriority(
          this.dashboardModel.value
        );
      });
  }

  getAutocomplete(): string[] {
    // tslint:disable-next-line:prefer-const
    let data = {
      all: [],
      projectCode: [],
      site: [],
      pm: [],
      status: [],
      start: [],
      end: []
    };
    this.siteWork.getTables().forEach(set => {
      data.projectCode.push(set.getProjectCode());
      data.site.push(this.getSite(set));
      data.pm.push(this.getPM(set));
      data.status.push(set.getStatus());
      data.start.push(this.getStart(set));
      data.end.push(this.getEnd(set));
    });
    return this.exist(data[this.typeFilter]).filter(_set => {
      const keyword = this.text.transform(this.search.value);
      const result = this.text.transform(_set);
      return keyword ? result.indexOf(keyword) !== -1 : true;
    });
  }

  getUserAdmin(): Admin {
    const roles = this.aaa.getCurrentUser().getUserRoles();
    let admin: Admin = new Admin();
    for (const role of roles) {
      if (role.getRoleType() === RoleType.Admin) {
        admin = new Admin(JSON.parse(JSON.stringify(role)));
      }
    }
    return admin;
  }

  onCreate() {
    const siteWork = new SiteWork();
    const admin = this.aaa.isAuthorized("read:project:{province:$}:{job:$}")
      ? this.getUserAdmin()
      : new Admin();
    siteWork.setAdmin(admin);
    siteWork.setSite(new Site());
    siteWork.setProjectManager(new ProjectManager());
    siteWork.setAdminCenter(new AdminCenter());
    const dialog = this.dialog.open(AdminProjectFormComponent, {
      width: "1200px",
      data: {
        value: siteWork,
        pm: this.pm,
        site: this.site,
        admin: this.adminService,
        adminCenter: this.adminCenterService
      },
      disableClose: true
    });
    dialog
      .afterClosed()
      .pipe(takeUntil(this.destroy$))
      .subscribe(_result => {
        if (_result) {
          this.siteWork
            .create(_result)
            .pipe(takeUntil(this.destroy$))
            .subscribe(
              _success =>
                (this.datasource.data = this.siteWork.getPriority(
                  this.dashboardModel.value
                ))
            );
        }
      });
  }

  onCancel(row: SiteWork) {
    const dialog = this.dialog.open(DialogComponent, {
      data: {
        id: row.getId(),
        message: `Are you sure to cancel project ${row.getProjectCode()} ${row
          .getSite()
          .getName()} ?`
      },
      disableClose: true
    });
    dialog.afterClosed().subscribe(_id => {
      if (_id) {
        this.siteWork
          .cancel(_id)
          .pipe(takeUntil(this.destroy$))
          .subscribe(
            success =>
              (this.datasource.data = this.siteWork.getPriority(
                this.dashboardModel.value
              ))
          );
      }
    });
  }

  onUpdate(row: SiteWork) {
    console.log(row);
    const dialog = this.dialog.open(AdminProjectFormComponent, {
      width: "1200px",
      data: {
        value: new SiteWork(row),
        pm: this.pm,
        site: this.site,
        admin: this.adminService,
        adminCenter: this.adminCenterService
      },
      disableClose: true
    });
    dialog.afterClosed().subscribe(_result => {
      if (_result) {
        console.log(_result);
        this.siteWork
          .update(row.getId(), _result)
          .subscribe(
            _success =>
              (this.datasource.data = this.siteWork.getPriority(
                this.dashboardModel.value
              ))
          );
      }
    });
  }

  showListReference(event: SiteWork) {
    const data: FormList<Reference> = {
      formTitle: 'Reference List',
      data: event.getReferences(),
      columns: [
        {title: 'No', columnDef: 'no'},
        {title: 'Reference Type', columnDef: 'referenceType'},
        {title: 'Description', columnDef: 'description'},
      ]
    };
    this.dialog.open(FormListComponent, {
      data: data,
      disableClose: true,
      width: '800px'
    });
  }

  handleFileInput(files: FileList) {
    this.importCsv.importCsvProject(files.item(0)).subscribe(val => {
      console.log(JSON.parse(JSON.stringify(val)));
      this.fileControl.reset();
    });
  }
}
