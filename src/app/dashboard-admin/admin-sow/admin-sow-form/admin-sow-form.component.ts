import { Component, OnInit, Inject } from "@angular/core";
import { MAT_DIALOG_DATA, MatDialogRef } from "@angular/material";
import { FormGroup } from "@angular/forms";
import { SowType } from "src/app/models/sow-type.model";
import { FormlyFieldConfig } from "@ngx-formly/core";

interface FormData {
  sowCode: string;
  description: string;
  price: number;
  engineerDay: number;
  technicianDay: number;
  documentationDay: number;
  atpDay: number;
  region: number;
}

@Component({
  selector: "app-admin-sow-form",
  templateUrl: "./admin-sow-form.component.html",
  styles: []
})
export class AdminSowFormComponent implements OnInit {
  form: FormGroup;
  fields: FormlyFieldConfig[];
  formData: FormData;

  constructor(
    @Inject(MAT_DIALOG_DATA) public data: SowType,
    private dialogRef: MatDialogRef<AdminSowFormComponent>
  ) {
    this.form = new FormGroup({});
    this.fields = this.data.formFields();
  }

  ngOnInit() {
    this.setRegionOptions();
    this.setFormData();
  }

  setFormData() {
    this.formData = {
      sowCode: this.data.getSowCode,
      description: this.data.getDescription,
      price: this.data.getPrice,
      engineerDay: this.data.getEngineerday,
      technicianDay: this.data.getTechnicianday,
      documentationDay: this.data.getDocumentationday,
      atpDay: this.data.getAtpDay,
      region: this.data.getRegion.getId,
    };
  }

  setRegionOptions() {
    const options = [
      {
        value: this.data.getRegion.getId,
        label: this.data.getRegion.getName
      }
    ];

    this.fields = this.fields.map(field => {
      if (field.key === "region") {
        field.templateOptions.options = options;
        field.templateOptions.disabled = true;
      }
      return field;
    });
  }

  onClose() {
    const model = new SowType();
    model.setSowCode = this.formData.sowCode;
    model.setDescription = this.formData.description;
    model.setPrice = this.formData.price;
    model.setRegion = this.data.getRegion;
    model.setEngineerDay = this.formData.engineerDay;
    model.setTechnicianDay = this.formData.technicianDay;
    model.setDocumentationDay = this.formData.documentationDay;
    model.setAtpDay = this.formData.atpDay;
    this.dialogRef.close(model);
  }
}
