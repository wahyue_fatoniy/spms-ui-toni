import { Component, OnInit, OnDestroy } from "@angular/core";
import { MatDialog } from "@angular/material";
import {
  TableBuilder,
  ColumnConfig,
  actionType,
  ColumnType
} from "src/app/shared/table-builder";
import { RegionService } from "src/service/region-service/region.service";
import { SowTypeService } from "src/service/sowType-service/sow-type.service";
import { Region } from "src/app/models/region.model";
import { SowType } from "src/app/models/sow-type.model";
import { AAAService } from "src/app/shared/aaa/aaa.service";
import { AdminSowFormComponent } from "./admin-sow-form/admin-sow-form.component";

@Component({
  selector: "app-admin-sow",
  template: `
    <app-table
      style="margin-top: -12px;"
      [columnConfig]="columnConfig"
      [actionTypes]="actionTypes"
      (OnEvent)="onEvent($event)"
      [data]="data"
      [actionTypes]="actionTypes"
    >
    </app-table>
  `,
  styles: []
})
export class AdminSowComponent
  implements OnInit, OnDestroy {
  columnConfig: ColumnConfig[] = [];
  actionTypes: actionType[] = [];
  data: any[] = [];
  adminRegion: Region;
  constructor(
    private dialog: MatDialog,
    private regionService: RegionService,
    private sowTypeService: SowTypeService,
    private aaa: AAAService
  ) {}

  ngOnInit() {
    this.subsRegion();
    this.subsSowType();
    this.initRegionService();
    this.initTable();
  }

  ngOnDestroy() {}

  initTable() {
    this.data = [];
    this.actionTypes = ["ADD", "UPDATE"];
    this.columnConfig = [
      { key: "no", label: "No.", columnType: ColumnType.string },
      { key: "sowCode", label: "Sow Code", columnType: ColumnType.string },
      { key: "description", label: "Description", columnType: ColumnType.string },
      { key: "price", label: "Price", columnType: ColumnType.currency, local: "Rp. " },
      { key: "engineerDay", label: "ED", columnType: ColumnType.string },
      { key: "technicianDay", label: "TD", columnType: ColumnType.string },
      { key: "documentationDay", label: "DD", columnType: ColumnType.string },
      { key: "atpDay", label: "ATP Day", columnType: ColumnType.string },
      { key: "action", label: "Action", columnType: ColumnType.action },
    ];
  }

  initRegionService() {
    this.regionService.getData().subscribe(val => {
      console.log(val);
    });
  }

  subsRegion() {
    this.regionService.subscriptionData().subscribe((val: Region[]) => {
      const regions = val.filter(region => {
        return (
          !this.aaa.isAuthorized("read:sow:{province:$}") ||
          this.aaa.isAuthorized(`read:sow:{province:${region.getId}}`)
        );
      });
      this.adminRegion = regions[0];
      this.initSowTypeService(regions);
    });
  }

  initSowTypeService(region: Region[]) {
    this.sowTypeService.findByRegions(region).subscribe(val => {});
  }

  subsSowType() {
    this.sowTypeService.subscriptionData().subscribe(sowTypes => {
      console.log(sowTypes);
      this.data = sowTypes;
    });
  }

  onEvent(event) {
    if ((event ? event.type : null) === 'ADD') {
      this.createSowType();
    } else {
      this.updateSowType(event.value);
    }
  }

  createSowType() {
    const sowType = new SowType();
    sowType.setRegion = this.adminRegion;
    this.dialog.open(AdminSowFormComponent, {
      width: '500px',
      data: sowType,
      disableClose: true,
    }).afterClosed().subscribe(val => {
      console.log(val);
    });
  }

  updateSowType(sowType: SowType) {
    this.dialog.open(AdminSowFormComponent, {
      width: '500px',
      data: sowType,
      disableClose: true,
    }).afterClosed().subscribe(val => {
      console.log(val);
    });
  }
}
