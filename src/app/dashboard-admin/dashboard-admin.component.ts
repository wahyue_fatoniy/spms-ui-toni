import { Component, OnInit } from "@angular/core";
import { AAAService } from "../shared/aaa/aaa.service";

@Component({
  selector: "app-dashboard-admin",
  template: `
    <nav mat-tab-nav-bar style="margin-top:-75px;">
      <ng-container *ngFor="let link of links">
        <a style="text-decoration: none"
          mat-tab-link
          *ngIf="aaa.isAuthorized(link.scope)"
          [routerLink]="link.path"
          routerLinkActive
          #rla="routerLinkActive"
          [active]="rla.isActive"
        >
          <img [src]="link.image" style="margin-bottom:7px;" />&nbsp;
          {{ link.title }}
        </a>
      </ng-container>
    </nav>
    <div class="container-home"><router-outlet style="margin-top: -16px;"></router-outlet></div>
  `,
  styles: []
})
export class DashboardAdminComponent implements OnInit {
  constructor(public aaa: AAAService) {}
  links = [
    {
      path: "sow",
      title: "SOW",
      image: "/assets/img/collection/sow.png",
      scope: "read:sow"
    },
    {
      path: "project",
      title: "Project",
      image: "/assets/img/collection/project.png",
      scope: "read:project"
    }
  ];

  ngOnInit() {}
}
