import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { Routes, RouterModule } from "@angular/router";


import { AdminProjectComponent } from "./admin-project/admin-project.component";
import { AdminSowComponent } from "./admin-sow/admin-sow.component";
import { TableComponent } from "./table/table.component";
import { SharedModule } from "../shared/shared.module";
import { DashboardAdminComponent } from "./dashboard-admin.component";
import { AdminProjectFormComponent } from "./admin-project/admin-project-form/admin-project-form.component";
import { AdminSowFormComponent } from "./admin-sow/admin-sow-form/admin-sow-form.component";
import { FormlyModule } from "@ngx-formly/core";
import { MatNativeDateModule } from "@angular/material";
import { FormlyMaterialModule } from "@ngx-formly/material";
import { FormlyMatDatepickerModule } from "@ngx-formly/material/datepicker";
import { ReactiveFormsModule, FormsModule } from "@angular/forms";
import { NgxMatSelectSearchModule } from "ngx-mat-select-search";
import { MaterialModule } from "../material.module";

const routes: Routes = [
  {
    path: "",
    component: DashboardAdminComponent,
    children: [
      { path: "", redirectTo: 'sow', pathMatch: 'full' },
      { path: "sow", component: AdminSowComponent },
      { path: "project", component: AdminProjectComponent }
    ]
  }
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    SharedModule,
    ReactiveFormsModule,
    FormsModule,
    MaterialModule,

    FormlyModule,
    FormlyMaterialModule,
    FormlyMatDatepickerModule,
    MatNativeDateModule,
    NgxMatSelectSearchModule,
  ],
  exports: [RouterModule],
  declarations: [
    DashboardAdminComponent,
    TableComponent,
    AdminProjectComponent,
    AdminProjectFormComponent,
    AdminSowComponent,
    AdminSowFormComponent,
  ],
  entryComponents: [
    AdminSowFormComponent,
    AdminProjectFormComponent,
  ]
})
export class DashboardAdminModule {}
