import { Component, OnInit, ViewChild, Input, Output, EventEmitter, OnChanges } from '@angular/core';
import { TableBuilder, ColumnConfig, actionType, OnEvent, ColumnType } from 'src/app/shared/table-builder';
import { MatPaginator, MatSort } from '@angular/material';

@Component({
  selector: 'app-table',
  templateUrl: './table.component.html',
  styleUrls: ['./table.component.scss']
})
export class TableComponent extends TableBuilder  implements OnInit, OnChanges {
  @ViewChild(MatPaginator) _paginator: MatPaginator;
  @ViewChild(MatSort) _sort: MatSort;
  @Output() OnEvent: EventEmitter<any> = new EventEmitter();

  @Input() columnConfig: ColumnConfig[] = [];
  @Input() actionTypes: actionType[] = [];
  @Input() title: string;
  @Input() data: any[];
  columnType = ColumnType;

  constructor() {
    super();
  }

  ngOnInit() {
    this.paginator = this._paginator;
    this.sort = this._sort;
    this.setDisplayedColumns();
    this.setTypeFilters();
  }

  ngOnChanges() {
    this.datasource.data = this.data;
  }

  onClick(event: OnEvent) {
    this.OnEvent.emit(event);
  }
}
