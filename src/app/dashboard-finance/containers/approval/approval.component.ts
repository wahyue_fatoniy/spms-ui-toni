import { Component, OnInit } from '@angular/core';
import { SowType } from 'src/app/models/sow-type.model';
import { actionType, ColumnConfig, ColumnType, OnEvent } from 'src/app/shared/table-builder';
import { SiteWork } from 'src/app/models/siteWork.model';
import { Request, RequestStatus, RequestType } from 'src/app/models/request.model';
import { MatDialog } from '@angular/material';
import { RequestService } from 'src/service/request-service/request.service';
import { SiteWorkService } from 'src/service/siteWork-service/site-work.service';
import { FormSowListComponent } from '../../presentational/form-sow-list/form-sow-list.component';
import { DialogComponent } from 'src/app/shared/dialog/dialog.component';
import { FormRejectComponent } from '../../presentational/form-reject/form-reject.component';
import { FormAddPidComponent } from '../../presentational/form-add-pid/form-add-pid.component';
import { forkJoin } from 'rxjs';
import { ToastrService } from 'ngx-toastr';
import { AAAService } from 'src/app/shared/aaa/aaa.service';
import { User } from 'src/app/models/user.model';
import { Role } from 'src/app/shared/models/role/role';

interface TableModel {
  requestId: number;
  id: number;
  assignmentId: string;
  site: string;
  sowTypes: SowType[];
  status: string;
}


@Component({
  selector: 'app-approval',
  template: `
    <app-approval-table
     [data]="data"
     [title]="title"
     [actionTypes]="actionTypes"
     (onEvent)="onEvent($event)"
     [columnConfigs]="columnConfigs"
    >
    </app-approval-table>
  `,
})
export class ApprovalComponent implements OnInit {
  data: any[];
  title: string;
  actionTypes: actionType[];
  columnConfigs: ColumnConfig[];

  request: Request[] = [];
  projects: SiteWork[] = [];
  approver: User;

  constructor(
    public dialog: MatDialog,
    public requestService: RequestService,
    public siteWorkServie: SiteWorkService,
    public toastService: ToastrService,
    public aaaService: AAAService
  ) {
    this.data = [];
    this.title = 'Approval Project';
    this.actionTypes = ['VIEW', 'ACCEPT', 'REJECT'];
    this.columnConfigs = [
      { label: 'No', key: 'no', columnType: ColumnType.string },
      { label: 'Assignment Id', key: 'assignmentId', columnType: ColumnType.string },
      { label: 'Site', key: 'site', columnType: ColumnType.string },
      { label: 'Requester', key: 'requester', columnType: ColumnType.string },
    ];
  }

  ngOnInit() {
    this.subscriptionData();
    this.getServiceData();
    this.initApprover();
  }

  initApprover() {
    const currentUser = this.aaaService.getCurrentUser() ? this.aaaService.getCurrentUser().getUserMetadata().roles.find(role => {
      return role.roleType === 'USER'
    }) : new Role();

    this.approver = new User();
    this.approver['id'] = currentUser['id'];
    this.approver.setUsername = currentUser['username'];
    this.approver.setPassword = currentUser['password'];
    this.approver.setEmail = currentUser['email'];
  }

  getServiceData() {
    this.requestService.findByStatus(RequestStatus.proposed, RequestType.requested_project_id)
      .subscribe();
  }

  subscriptionData() {
    this.requestService.subscriptionData()
      .subscribe(val => {
        this.request = val;
        this.data = val.filter(request => {
          return request.getStatus === RequestStatus.proposed;
        }).map((request) => {
          return this.setTableModel(request);
        });
        this.projects = val.map(request => {
          return new SiteWork(request.getObject);
        });
      });
  }

  setTableModel(request: Request) {
    return <TableModel>{
      requestId: request.getId,
      assignmentId: request.getObject ? request.getObject['assignmentIdData'] : '',
      id: request.getObject ? request.getObject['id'] : '',
      site: request.getObject['site'] ? request.getObject['site']['name'] : '',
      requester: request.getRequester['party'] ? request.getRequester['party']['name'] : '',
      sowTypes: request.getObject['sowTypes'],
      status: request.getStatus
    }
  }

  onEvent(event: OnEvent) {
    switch (event.type) {
      case 'ACCEPT':
        this.acceptProject(event.value);
        break;
      case 'REJECT':
        this.rejectProject(event.value);
        break;
      case 'VIEW':
        this.showSowList(event.value ? event.value['sowTypes'] : []);
        break;
    }
  }

  acceptProject(model) {
    const request = this.findRequest(model);
    const project = this.findSitework(model);
    this.dialog.open(FormAddPidComponent, {
      width: '500px',
      disableClose: true,
    }).afterClosed().subscribe(val => {
      if (val) {
        request['object'] ? request['object']['projectCode'] = val : request['object'];
        request.setStatus = RequestStatus.accepted;
        request.setEndDate = new Date();
        request.setApprover = this.approver;
        project.setProjectCode = val;
        this.siteWorkServie.update(project)
          .then(() => {
            this.requestService.update(request)
              .then(() => {
                this.toastService.success('Success Approve request Project ID', 'Success');
              }).catch(err => {
                this.toastService.error('Failed Approve request Project ID', 'Failed');
              })
          }).catch(err => {
            this.toastService.error('Failed Approve request Project ID', 'Success');
          })
      }
    });
  }

  rejectProject(model) {
    const request = this.findRequest(model);
    this.dialog.open(FormRejectComponent, {
      data: request,
      width: '500px',
      disableClose: true,
    }).afterClosed().subscribe(val => {
      if (val) {
        request.setApprover = this.approver;
        
        this.requestService.update(val)
          .then(request => {
            this.toastService.success('Success Reject request project ID', 'Success');
            this.actionTypes = [];
            console.log(request);
          })
          .catch(err => {
            this.toastService.error('Failed Reject request project ID', 'Failed');
            console.log(err);
          });
      }
    });
  }

  showSowList(value: Object[]) {
    if (value) {
      const sowTypes = value.map(val => {
        return new SowType(val);
      });
      this.dialog.open(FormSowListComponent, {
        width: '900px',
        disableClose: true,
        data: sowTypes
      });
    }
  }

  findRequest(model: TableModel) {
    return this.request.find(request => {
      return request.getId === model.requestId;
    })
  }

  findSitework(model: TableModel) {
    return this.projects.find(project => {
      return project.getId === model.id;
    })
  }
}
