import { Component, OnInit, ViewChild, OnDestroy } from "@angular/core";
import { TableConfig, CoreTable } from "../../../shared/core-table";
import {
  PurchaseOrder,
  PoStatus
} from "../../../shared/models/purchase-order/purchase-order";
import { InvoiceService } from "../../../shared/models/invoice/invoice.service";
import { PurchaseOrderService } from "../../../shared/models/purchase-order/purchase-order.service";
import { combineLatest } from "rxjs";
import { MatPaginator, MatSort, Sort, MatDialog } from "@angular/material";
import { DatePipe } from "@angular/common";
import { LocalCurrencyPipe } from "../../../pipes/localCurrency/local-currency.pipe";
import { SiteWorkService } from "../../../shared/models/site-work/site-work.service";
import { CustomerService } from "../../../shared/models/customer/customer.service";
import { SalesService } from "../../../shared/models/sales/sales.service";
import { PartnerService } from "../../../shared/models/partner/partner.service";
import {
  SiteWorkListComponent,
  UpdatePoProject
} from "./site-work-list/site-work-list.component";
import { DialogComponent } from "../../../shared/dialog/dialog.component";
import { FormControl } from "@angular/forms";
import { Customer } from "../../../shared/models/customer/customer";
import { Sales } from "../../../shared/models/sales/sales";
import { Partner } from "../../../shared/models/partner/partner";
import { PurchaseOrderFormComponent } from "./purchase-order-form/purchase-order-form.component";
import { Router, ActivatedRoute } from "@angular/router";
import { takeUntil } from "rxjs/operators";
import { AAAService } from "src/app/shared/aaa/aaa.service";

const CONFIG: TableConfig = {
  title: "Purchase Order",
  columns: [
    "no",
    "poNumber",
    "customer",
    "partner",
    "sales",
    "poValue",
    "totalInvoice",
    "publishDate",
    "expiredDate",
    "status"
  ],
  typeFilters: [
    { value: "all", label: "All" },
    { value: "poNumber", label: "PO Number" },
    { value: "poValue", label: "PO Value" },
    { value: "publishDate", label: "Publish Date" },
    { value: "expiredDate", label: "Expired Date" },
    { value: "customer", label: "Customer" },
    { value: "partner", label: "Partner" },
    { value: "sales", label: "Sales" },
    { value: "status", label: "Status" },
    { value: "totalInvoice", label: "Total Invoice" }
  ]
};

@Component({
  selector: "app-finance-purchase-order",
  templateUrl: "./finance-purchase-order.component.html",
  styles: [``]
})
export class FinancePurchaseOrderComponent extends CoreTable<PurchaseOrder>
  implements OnInit, OnDestroy {
  constructor(
    public po: PurchaseOrderService,
    public invoice: InvoiceService,
    public siteWork: SiteWorkService,
    public customer: CustomerService,
    public sales: SalesService,
    public partner: PartnerService,
    private dialog: MatDialog,
    private router: Router,
    private activeRoute: ActivatedRoute,
    private aaa: AAAService,
    private siteworkService: SiteWorkService
  ) {
    super(CONFIG);
  }

  @ViewChild(MatPaginator)
  paginator: MatPaginator;
  @ViewChild(MatSort)
  sort: MatSort;
  date = new DatePipe("id");
  formatType = "dd MMMM yyyy";

  dashboardModel: FormControl = new FormControl();
  dashboardSettings: PoStatus[] = [
    PoStatus.Progress,
    PoStatus.Done,
    PoStatus.Cancel,
    PoStatus.NotStarted
  ];

  getPublishDate(row: PurchaseOrder): Date {
    return row.getPublishDate();
  }

  getExpiredDate(row: PurchaseOrder): Date {
    return row.getExpiredDate();
  }

  getCustomer(row: PurchaseOrder): string {
    return row
      .getCustomer()
      .getParty()
      .getName();
  }

  getSales(row: PurchaseOrder) {
    return row
      .getSales()
      .getParty()
      .getName();
  }

  getPartner(row: PurchaseOrder) {
    return row
      .getPartner()
      .getParty()
      .getName();
  }

  getTotalInvoice(row: PurchaseOrder): string {
    return new LocalCurrencyPipe().transform(row.getTotalInvoice());
  }

  getPoValue(row: PurchaseOrder): string {
    return new LocalCurrencyPipe().transform(row.getTotalProjectValue());
  }

  getLoading(): boolean {
    return this.po.getLoading() || this.invoice.getLoading();
  }

  ngOnInit() {
    this.initialize();
    this.dashboardModel.setValue([PoStatus.Progress]);
    combineLatest(
      this.po.findAll(),
      this.invoice.findAll(),
      this.siteWork.findDone(),
      this.customer.findAll(),
      this.sales.findAll(),
      this.partner.findAll()
    ).subscribe(
      _success =>
        (this.datasource.data = this.po.getPriority(this.dashboardModel.value))
    );
  }

  ngOnDestroy() {
    this.destroy$.next(true);
    this.destroy$.complete();
  }

  // initialize from table
  sortData(sort: Sort) {
    if (!sort.active || sort.direction === "") {
      this.datasource.data = this.po.getPriority(this.dashboardModel.value);
      return;
    }
    this.datasource.data = this.datasource.data.sort((a, b) => {
      const isAsc = sort.direction === "asc";
      switch (sort.active) {
        case "poNumber":
          return this.compare(a.getPoNumber(), b.getPoNumber(), isAsc);
        case "poValue":
          return this.compare(this.getPoValue(a), this.getPoValue(b), isAsc);
        case "publishDate":
          return this.compare(
            this.getPublishDate(a),
            this.getPublishDate(b),
            isAsc
          );
        case "expiredDate":
          return this.compare(
            this.getExpiredDate(a),
            this.getExpiredDate(b),
            isAsc
          );
        case "customer":
          return this.compare(this.getCustomer(a), this.getCustomer(b), isAsc);
        case "sales":
          return this.compare(this.getSales(a), this.getSales(b), isAsc);
        case "partner":
          return this.compare(this.getPartner(a), this.getPartner(b), isAsc);
        case "status":
          return this.compare(a.getStatus(), b.getStatus(), isAsc);
        case "totalInvoice":
          return this.compare(
            this.getTotalInvoice(a),
            this.getTotalInvoice(b),
            isAsc
          );
        default:
          return 0;
      }
    });
  }

  initialize() {
    this.action = this.aaa.isAuthorized("write:po");
    this.datasource.paginator = this.paginator;
    this.datasource.sort = this.sort;
    this.datasource.filterPredicate = (set: PurchaseOrder, keyword: string) => {
      const value = {
        poNumber: set.getPoNumber(),
        poValue: this.getPoValue(set),
        publishDate: this.getPublishDate(set),
        expiredDate: this.getExpiredDate(set),
        customer: this.getCustomer(set),
        sales: this.getSales(set),
        partner: this.getPartner(set),
        status: set.getStatus(),
        totalInvoice: this.getTotalInvoice(set)
      };
      return this.filterPredicate(value, keyword, this.typeFilter);
    };
    this.search.valueChanges.subscribe(
      _search => (this.datasource.filter = _search)
    );
    this.dashboardModel.valueChanges.subscribe(_set => {
      this.datasource.data = this.po.getPriority(this.dashboardModel.value);
    });
  }

  getAutocomplete(): string[] {
    // tslint:disable-next-line:prefer-const
    let data = {
      all: [],
      poNumber: [],
      poValue: [],
      publishDate: [],
      expiredDate: [],
      customer: [],
      sales: [],
      partner: [],
      status: [],
      totalInvoice: []
    };
    this.po.getTables().forEach(set => {
      data.poNumber.push(set.getPoNumber());
      data.poValue.push(this.getPoValue(set));
      data.publishDate.push(this.getPublishDate(set));
      data.expiredDate.push(this.getExpiredDate(set));
      data.customer.push(this.getCustomer(set));
      data.sales.push(this.getSales(set));
      data.partner.push(this.getPartner(set));
      data.status.push(set.getStatus());
      data.totalInvoice.push(this.getTotalInvoice(set));
    });
    return this.exist(data[this.typeFilter]).filter(_set => {
      const keyword = this.text.transform(this.search.value);
      const result = this.text.transform(_set);
      return keyword ? result.indexOf(keyword) !== -1 : true;
    });
  }

  onCreate() {
    const po = new PurchaseOrder();
    po.setCustomer(new Customer());
    po.setSales(new Sales());
    po.setPartner(new Partner());
    po.setSiteWorks([]);
    po.setPoValue(0);
    const dialog = this.dialog.open(PurchaseOrderFormComponent, {
      width: "1800px",
      data: {
        value: po,
        customer: this.customer,
        sales: this.sales,
        partner: this.partner,
        siteWork: this.siteWork
      }
    });
    dialog.afterClosed().subscribe((_set: PurchaseOrder) => {
      if (_set) {
        _set.setSiteWorks(
          _set.getSiteWorks().map(val => {
            return { id: val.getId() };
          })
        );
        console.log(_set);
        this.po.create(_set).subscribe(_success => {
          this.siteWork
            .findDone()
            .pipe(takeUntil(this.destroy$))
            .subscribe(
              _done =>
                (this.datasource.data = this.po.getPriority(
                  this.dashboardModel.value
                ))
            );
        });
      }
    });
  }

  onCancel(row: PurchaseOrder) {
    const dialog = this.dialog.open(DialogComponent, {
      data: {
        id: row.getId(),
        message: `Are you sure to cancel purchase order ${row.getPoNumber()} ${row
          .getCustomer()
          .getParty()
          .getName()} ?`
      }
    });
    dialog.afterClosed().subscribe(_id => {
      if (_id) {
        this.po.cancel(_id).subscribe(result => {
          this.siteWork
            .findDone()
            .pipe(takeUntil(this.destroy$))
            .subscribe(
              _done =>
                (this.datasource.data = this.po.getPriority(
                  this.dashboardModel.value
                ))
            );
        });
      }
    });
  }

  onShowInvoice(row: PurchaseOrder) {
    this.router.navigate([`home/dashboard-finance/invoice/${row.getId()}`]).catch(err => {
      console.log(err);
    });
  }

  onShowProject(row: PurchaseOrder) {
    const dialog = this.dialog.open(SiteWorkListComponent, {
      width: "1800px",
      data: {
        value: new PurchaseOrder(row),
        siteWorkList: this.siteWork
      }
    });
    dialog.afterClosed().subscribe((val: UpdatePoProject) => {
      if (val) {
        // on Remove sitework
        row.setSiteWorks(
          row.getSiteWorks().filter(sitework => {
            return (
              val.removeProject.findIndex(
                remove => remove.getId() === sitework.getId()
              ) === -1
            );
          })
        );

        // on Add Sitework
        row.setSiteWorks([...row.getSiteWorks(), ...val.addProject]);
        console.log(row);
        this.po
          .update(val.poId, row)
          .pipe(takeUntil(this.destroy$))
          .subscribe(po => {
            this.datasource.data = this.po.getPriority(this.dashboardModel.value);
          });
      }
    });
  }
}
