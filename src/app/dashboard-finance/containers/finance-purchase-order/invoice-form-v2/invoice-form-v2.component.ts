import { Component, OnInit, Inject } from '@angular/core';
import { Invoice } from '../../../../shared/models/invoice/invoice';
import { PurchaseOrderService } from '../../../../shared/models/purchase-order/purchase-order.service';
import { MAT_DIALOG_DATA } from '@angular/material';
import { LocalCurrencyPipe } from '../../../../pipes/localCurrency/local-currency.pipe';
import { NumberInStringPipe } from '../../../../pipes/numberInString/number-in-string.pipe';
import { FormControl, Validators } from '@angular/forms';

interface InvoiceFormV2 {
  value: Invoice;
}

@Component({
  selector: 'app-invoice-form-v2',
  templateUrl: './invoice-form-v2.component.html',
  styles: []
})
export class InvoiceFormV2Component implements OnInit {
  constructor(@Inject(MAT_DIALOG_DATA) public data: InvoiceFormV2) {
    console.log(data);
  }
  intToRp: LocalCurrencyPipe = new LocalCurrencyPipe();
  RpToInt: NumberInStringPipe = new NumberInStringPipe();
  invoiceNumber: FormControl = new FormControl(
    this.data.value.getInvoiceNumber(),
    [Validators.required]
  );
  invoiceValue: FormControl = new FormControl(
    this.data.value.getInvoiceValue(),
    [
      Validators.required,
      Validators.max(this.data.value.getPurchaseOrder().getRemainingInvoice())
    ]
  );
  referenceBast: FormControl = new FormControl(
    this.data.value.getReferenceBast(),
    [
      Validators.required
    ]
  );
  dueDate: FormControl = new FormControl(this.data.value.getDueDate(), [
    Validators.required
  ]);

  minDate: Date = new Date();
  invalid(): boolean {
    return (
      this.invoiceValue.invalid ||
      this.referenceBast.invalid ||
      this.dueDate.invalid
    );
  }

  ngOnInit() {
    this.invoiceNumber.valueChanges.subscribe(_set =>
      this.data.value.setInvoiceNumber(_set)
    );
    this.invoiceValue.valueChanges.subscribe(_set => {
      this.invoiceValue.setValue(this.intToRp.transform(_set), { emitEvent: false });
      this.data.value.setInvoiceValue(this.RpToInt.transform(_set));
    });
    this.referenceBast.valueChanges.subscribe(_set =>
      this.data.value.setReferenceBast(_set)
    );
    this.dueDate.valueChanges.subscribe(_set =>
      this.data.value.setDueDate(_set)
    );
  }

}
