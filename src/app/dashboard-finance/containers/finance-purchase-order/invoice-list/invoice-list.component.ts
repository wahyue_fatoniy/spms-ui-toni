import { Component, OnInit, ViewChild } from '@angular/core';
import { InvoiceService } from '../../../../shared/models/invoice/invoice.service';
import { PurchaseOrderService } from '../../../../shared/models/purchase-order/purchase-order.service';
import { PurchaseOrder, PoStatus } from '../../../../shared/models/purchase-order/purchase-order';
import { TableConfig, CoreTable } from '../../../../shared/core-table';
import { Invoice } from '../../../../shared/models/invoice/invoice';
import { MatPaginator, MatSort, Sort, MatDialog } from '@angular/material';
import { DatePipe } from '@angular/common';
import { LocalCurrencyPipe } from '../../../../pipes/localCurrency/local-currency.pipe';
import { FormControl } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { InvoiceFormV2Component } from '../invoice-form-v2/invoice-form-v2.component';
import { DialogComponent } from '../../../../shared/dialog/dialog.component';
import { AAAService } from 'src/app/shared/aaa/aaa.service';
import { pipe } from '@angular/core/src/render3/pipe';
import { takeUntil } from 'rxjs/operators';

const CONFIG: TableConfig = {
  title: 'Invoice',
  columns: [
    'no',
    'invoiceNumber',
    'invoiceDate',
    'invoiceValue',
    'referenceBast',
    'paymentDate',
    'dueDate',
    'submit'
  ],
  typeFilters: [
    { value: 'all', label: 'All' },
    { value: 'invoiceNumber', label: 'Invoice Number' },
    { value: 'invoiceDate', label: 'Invoice Date'},
    { value: 'invoiceValue', label: 'Invoice Value'},
    { value: 'referenceBast', label: 'Reference BAST'},
    { value: 'paymentDate', label: 'Payment Date'},
    { value: 'dueDate', label: 'Due Date'},
    { value: 'submit', label: 'Submit'}
  ]
};


@Component({
  selector: 'app-invoice-list',
  templateUrl: './invoice-list.component.html',
  styles: []
})
export class InvoiceListComponent extends CoreTable<Invoice> implements OnInit {

  constructor(
    public invoice: InvoiceService,
    private po: PurchaseOrderService,
    private dialog: MatDialog,
    private activeRoute: ActivatedRoute,
    private aaaService: AAAService
  ) {
    super(CONFIG);
  }

  @ViewChild(MatPaginator)
  paginator: MatPaginator;
  @ViewChild(MatSort)
  sort: MatSort;
  date = new DatePipe('id');
  formatType = 'dd MMMM yyyy';
  intToRp = new LocalCurrencyPipe();
  poId: number;

  getInvoiceValue(row: Invoice): string {
    return this.intToRp.transform(row.getInvoiceValue());
  }

  getInvoiceDate(row: Invoice): string {
    return this.date.transform(row.getInvoiceDate(), this.formatType);
  }

  getPaymentDate(row: Invoice): string {
      return row.getPaymentDate() ? this.date.transform(row.getPaymentDate(), this.formatType) : null;
  }

  getDueDate(row: Invoice): string {
    return this.date.transform(row.getDueDate(), this.formatType);
  }

  getLoading(): boolean {
    return this.invoice.getLoading() || this.po.getLoading();
  }

  ngOnInit() {
    this.getData();
    this.initialize();
  }

  getData() {
    this.activeRoute.params
    .pipe(takeUntil(this.destroy$))
    .subscribe(data => {
      if (data) {
        this.poId = parseInt(data.poId);
        this.invoice.findByPOId(this.poId)
        .pipe(takeUntil(this.destroy$))
        .subscribe(() => {
          this.datasource.data = this.invoice.getTables();
        });
      }
    });
  }

  // initialize from table
  sortData(sort: Sort) {
    if (!sort.active || sort.direction === '') {
      this.datasource.data = this.invoice.getTables();
      return;
    }
    this.datasource.data = this.datasource.data.sort((a, b) => {
      const isAsc = sort.direction === 'asc';
      switch (sort.active) {
        case 'invoiceNumber':
          return this.compare(a.getInvoiceNumber(), b.getInvoiceNumber(), isAsc);
        case 'invoiceDate':
          return this.compare(this.getInvoiceDate(a), this.getInvoiceDate(b), isAsc);
        case 'invoiceValue':
          return this.compare(this.getInvoiceValue(a), this.getInvoiceValue(b), isAsc);
        case 'referenceBast':
          return this.compare(a.getReferenceBast(), b.getReferenceBast(), isAsc);
        case 'paymentDate':
          return this.compare(this.getPaymentDate(a), this.getPaymentDate(b), isAsc);
        case 'dueDate':
          return this.compare(this.getDueDate(a), this.getDueDate(b), isAsc);
        default:
          return 0;
      }
    });
  }

  initialize() {
    this.action = this.aaaService.isAuthorized('write:po');
    this.enabledAction();
    this.datasource.paginator = this.paginator;
    this.datasource.sort = this.sort;
    this.datasource.filterPredicate = (set: Invoice, keyword: string) => {
      const value = {
        invoiceNumber: set.getInvoiceNumber(),
        invoiceDate : this.getInvoiceDate(set),
        invoiceValue : this.getInvoiceValue(set),
        referenceBast : set.getReferenceBast(),
        paymentDate : this.getPaymentDate(set),
        dueDate : this.getDueDate(set)
      };
      return this.filterPredicate(value, keyword, this.typeFilter);
    };
    this.search.valueChanges.subscribe(
      _search => (this.datasource.filter = _search)
    );
  }

  getAutocomplete(): string[] {
    // tslint:disable-next-line:prefer-const
    let data = {
      all: [],
      invoiceNumber: [],
      invoiceDate: [],
      invoiceValue: [],
      referenceBast: [],
      paymentDate: [],
      dueDate: []
    };
    this.invoice.getTables().forEach(set => {
      data.invoiceNumber.push(set.getInvoiceNumber());
      data.invoiceDate.push(this.getInvoiceDate(set));
      data.invoiceValue.push(this.getInvoiceValue(set));
      data.referenceBast.push(set.getReferenceBast());
      data.paymentDate.push(this.getPaymentDate(set));
      data.dueDate.push(this.getDueDate(set));
    });
    return this.exist(data[this.typeFilter]).filter(_set => {
      const keyword = this.text.transform(this.search.value);
      const result = this.text.transform(_set);
      return keyword ? result.indexOf(keyword) !== -1 : true;
    });
  }

  onCreate() {
    const row = new Invoice();
    console.log(this.po.getById(this.poId));
    row.setPurchaseOrder(this.poId ? this.po.getById(this.poId) : new PurchaseOrder());
    const dialog = this.dialog.open(InvoiceFormV2Component, {
      width: '1000px', data : {value: new Invoice(row)}
    });
    dialog.afterClosed().subscribe(_set => {
      if (_set) {
        _set['paymentDate'] = null;
        this.invoice.create(_set)
        .subscribe(_result => this.datasource.data = this.invoice.getTables());
      }
    });
  }

  onUpdate(row: Invoice) {
    const dialog = this.dialog.open(InvoiceFormV2Component, {
      width: '1000px', data : {value: new Invoice(row)}
    });
    dialog.afterClosed().subscribe(_set => {
      if (_set) {
        this.invoice.update(row.getId(), _set)
        .subscribe(_result => this.datasource.data = this.invoice.getTables());
      }
    });
  }

  onSubmit(row: Invoice) {
    const dialog = this.dialog.open(DialogComponent, {
      data : { id: row.getId(), message : `Are you sure to submit ${row.getInvoiceNumber()} ${this.getInvoiceValue(row)} ?`}
    });
    dialog.afterClosed().subscribe(_set => {
      if (_set) {
        this.invoice.submit(row.getId())
        .subscribe(_result => this.datasource.data = this.invoice.getTables());
      }
    });
  }

  onSetPaymentDate(row: Invoice): void {
    const dialog = this.dialog.open(DialogComponent, {
      data : { id: row.getId(), message : `Are you sure to set Payment Date ${row.getInvoiceNumber()} ${this.getInvoiceValue(row)} ?`}
    });
    dialog.afterClosed().subscribe(_set => {
      if (_set) {
        this.invoice.setPaymentDate(row.getId())
        .subscribe(_result => this.datasource.data = this.invoice.getTables());
      }
    });
  }
}
