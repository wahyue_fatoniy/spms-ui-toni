import { Component, OnInit, Inject } from '@angular/core';
import { MAT_DIALOG_DATA } from '@angular/material';
import { PurchaseOrder } from '../../../../shared/models/purchase-order/purchase-order';
import { SiteWorkService } from '../../../../shared/models/site-work/site-work.service';
import { CustomerService } from '../../../../shared/models/customer/customer.service';
import { PartnerService } from '../../../../shared/models/partner/partner.service';
import { SalesService } from '../../../../shared/models/sales/sales.service';
import { FormControl, Validators } from '@angular/forms';
import { SiteWork } from '../../../../shared/models/site-work/site-work';
import { LocalCurrencyPipe } from '../../../../pipes/localCurrency/local-currency.pipe';
import { Sales } from '../../../../shared/models/sales/sales';
import { Customer } from '../../../../shared/models/customer/customer';
import { Partner } from '../../../../shared/models/partner/partner';
import { NumberInStringPipe } from '../../../../pipes/numberInString/number-in-string.pipe';

interface PurchaseOrderForm {
  value: PurchaseOrder;
  siteWork: SiteWorkService;
  customer: CustomerService;
  partner: PartnerService;
  sales: SalesService;
}

@Component({
  selector: 'app-purchase-order-form',
  templateUrl: './purchase-order-form.component.html',
  styles: [``]
})
export class PurchaseOrderFormComponent implements OnInit {

  constructor(@Inject(MAT_DIALOG_DATA) public data: PurchaseOrderForm) {
    console.log(data);
  }
  poNumber: FormControl = new FormControl(this.data.value.getPoNumber(), [Validators.required]);
  customer: FormControl = new FormControl(this.data.value.getCustomer().getId(), [Validators.required]);
  sales: FormControl = new FormControl(this.data.value.getSales().getId(), [Validators.required]);
  partner: FormControl = new FormControl(this.data.value.getPartner().getId(), [Validators.required]);
  poValue: FormControl = new FormControl({value: this.data.value.getPoValue(), disabled: true});
  siteWork: FormControl = new FormControl(this.data.value.getSiteWorkIds());
  expiredDate: FormControl = new FormControl(this.data.value.getExpiredDate(), [Validators.required]);
  publishDate: FormControl = new FormControl(this.data.value.getPublishDate(), [Validators.required]);
  valueSiteWorks: string[] = [];
  displayedColumns: string[] = ['no', 'projectCode', 'site', 'pm', 'budget', 'poValue'];
  minDate: Date = new Date();
  rpToInt  = new NumberInStringPipe();
  intToRp  = new LocalCurrencyPipe();

  ngOnInit() {
    this.poNumber.valueChanges.subscribe(_set => this.data.value.setPoNumber(_set));
    this.customer.valueChanges.subscribe(_set => {
      if (_set) {
        this.data.value.setCustomer(this.data.customer.getById(_set));
      }
    });
    this.sales.valueChanges.subscribe(_set => {
      if (_set) {
        this.data.value.setSales(this.data.sales.getById(_set));
      }
    });
    this.partner.valueChanges.subscribe(_set => {
      if (_set) {
        this.data.value.setPartner(this.data.partner.getById(_set));
      }
    });
    this.siteWork.valueChanges.subscribe(_set => {
      this.setSiteWorkIds(_set);
      this.poValue.setValue(this.intToRp.transform(0));
    });
    this.expiredDate.valueChanges.subscribe(_set => {
        if (_set && !this.expiredDate.invalid) {
          this.data.value.setExpiredDate(_set);
        }
    });

    this.publishDate.valueChanges.subscribe(_set => {
      if (_set && !this.publishDate.invalid) {
        this.data.value.setPublishDate(_set);
      }
  });

    this.poValue.valueChanges.subscribe(_set => this.data.value.setPoValue(this.rpToInt.transform(_set)));
    this.poValue.setValue(this.intToRp.transform(this.data.value.getTotalPoValue()));
  }

  getPartners(): Partner[] {
    return this.data.partner.getTables();
  }

  getCustomers(): Customer[] {
    return this.data.customer.getTables();
  }

  getSales(): Sales[] {
    return this.data.sales.getTables();
  }

  getSiteWorks(): SiteWork[] {
    return this.data.value.getSiteWorks();
  }

  getProjects(): SiteWork[] {
    return this.data.siteWork.getTables();
  }

  getBudget(row: SiteWork): string {
    return this.intToRp.transform(row.getBudget());
  }

  setSiteWorkIds(ids: number[]) {
    let siteWorks: SiteWork[] = [];
    this.valueSiteWorks = [];
    ids = ids ? ids : [];
    for (let i = 0; i < ids.length ; i++ ) {
      siteWorks.push(this.data.siteWork.getById(ids[i]));
      this.valueSiteWorks[i] = this.intToRp.transform(0);
    }
    this.data.value.setSiteWorks(siteWorks);
  }

  getPM(row: SiteWork): string {
    const pm =  row.getProjectManager();
    const party = pm ? pm.getParty() : null;
    return party ? party.getName() : null;
  }

  invalid(): boolean {
    return this.poNumber.invalid || this.customer.invalid || this.sales.invalid || this.partner.invalid ||
    this.poValue.invalid || this.siteWork.invalid;
  }

  changePoValue(index: number, value: string) {
    const set = this.rpToInt.transform(value);
    let poValue = 0;

    this.valueSiteWorks[index] = this.intToRp.transform(set);
    for (let poSiteWork of this.valueSiteWorks) {
      poValue += this.rpToInt.transform(poSiteWork);
    }
    this.poValue.setValue(this.intToRp.transform(poValue));
    this.data.value.changeValueSiteWork(index, set);
  }
}
