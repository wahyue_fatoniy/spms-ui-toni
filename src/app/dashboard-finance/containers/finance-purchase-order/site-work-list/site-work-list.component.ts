import { Component, OnInit, Inject } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material';
import { SiteWork } from '../../../../shared/models/site-work/site-work';
import { PurchaseOrder } from '../../../../shared/models/purchase-order/purchase-order';
import { LocalCurrencyPipe } from '../../../../pipes/localCurrency/local-currency.pipe';
import { SiteWorkService } from '../../../../shared/models/site-work/site-work.service';
import { FormControl } from '@angular/forms';
import { NumberInStringPipe } from '../../../../pipes/numberInString/number-in-string.pipe';

interface SiteWorkList {
  value: PurchaseOrder;
  siteWorkList: SiteWorkService;
}

export interface UpdatePoProject {
  poId: number;
  addProject: SiteWork[];
  removeProject: SiteWork[];
}

@Component({
  selector: 'app-site-work-list',
  templateUrl: './site-work-list.component.html',
  styles: []
})
export class SiteWorkListComponent implements OnInit {
  projectResource: SiteWork[] = [];
  availableProject: SiteWork[] = [];
  selectedProject: FormControl = new FormControl();
  intToRp: LocalCurrencyPipe = new LocalCurrencyPipe();
  rpToInt: NumberInStringPipe = new NumberInStringPipe();
  dialogData: UpdatePoProject = { poId: null, addProject: [], removeProject: [] };
  poValue: string[] = [];

  constructor(
    @Inject(MAT_DIALOG_DATA) public data: SiteWorkList,
    public matDialogRef: MatDialogRef<SiteWorkListComponent>
  ) {
    this.selectedProject.setValue(this.data.value.getSiteWorks());
    this.projectResource = this.data.value.getSiteWorks();
    this.availableProject = [...this.data.siteWorkList.getTables(), ...this.projectResource];
    this.setPoValue();
    this.onBeforeClose();
  }
  displayedColumns: string[] = ['no', 'projectCode', 'site', 'pm', 'poValue'];

  getPM(row: SiteWork): string {
    const pm =  row.getProjectManager();
    const party = pm ? pm.getParty() : null;
    return party ? party.getName() : null;
  }

  getPoValue(row: SiteWork): string {
    return this.intToRp.transform(row.getPoValue());
  }

  getBudget(row: SiteWork): string {
    return this.intToRp.transform(row.getBudget());
  }

  ngOnInit() { }

  onPoValueChanges(index: number, poValue: number): void {
    this.poValue[index] = this.intToRp.transform(poValue);
    this.projectResource[index]['poValue'] = this.rpToInt.transform(poValue);
  }

  public onSelectProject(val: SiteWork[]): void {
    this.projectResource = val;
    this.setPoValue();
  }

  public setPoValue() {
    this.poValue = this.projectResource.map(resource => this.intToRp.transform(resource.getPoValue()));
  }

  private onBeforeClose(): void {
    this.matDialogRef.beforeClose().subscribe(val => {
      const selectedProjects = this.data.value.getSiteWorks();
      this.dialogData.poId = this.data.value.getId();
      this.dialogData.addProject = this.projectResource.filter((resource: SiteWork) => {
        return (selectedProjects.findIndex(selected => selected.getId() === resource.getId()) === -1);
      });
      this.dialogData.removeProject = selectedProjects.filter((selected: SiteWork) => {
        return (this.projectResource.findIndex(resource => resource.getId() === selected.getId()) === -1);
      });
    });
  }
}
