import { Component, OnInit } from '@angular/core';
import { actionType, ColumnConfig, ColumnType, OnEvent } from 'src/app/shared/table-builder';
import { Invoice } from 'src/app/models/invoice.model';
import { MatDialog } from '@angular/material';
import { FormInvoiceComponent } from '../../presentational/form-invoice/form-invoice.component';
import { ActivatedRoute } from '@angular/router';
import { DialogComponent } from 'src/app/shared/dialog/dialog.component';
import { PurchaseOrderService } from 'src/service/purchasorder-service/purchase-order.service';
import { PurchaseOrder } from 'src/app/models/purchaseOrder.model';
import { InvoiceService } from 'src/service/invoice-service/invoice.service';
import { combineLatest } from 'rxjs';
import { FormPaymentComponent } from '../../presentational/form-payment/form-payment.component';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-invoice',
  template: `
    <app-invoice-table
      [data]="data"
      [title]="title"
      [actionTypes]="actionTypes"
      (onEvent)="onEvent($event)"
      [columnConfigs]="columnConfigs"
      [purchaseOrder]="purchaseOrder"
    >
    </app-invoice-table>
  `,
  styleUrls: ['./invoice.component.css'],
})
export class InvoiceComponent implements OnInit {
  data: any[];
  title: string;
  actionTypes: actionType[];
  columnConfigs: ColumnConfig[];

  poId: number;
  purchaseOrder: PurchaseOrder;

  constructor(
    public dialog: MatDialog,
    public activeRoute: ActivatedRoute,
    public poService: PurchaseOrderService,
    public invoiceService: InvoiceService,
    public toastService: ToastrService
  ) {
    this.data = [];
    this.purchaseOrder = new PurchaseOrder();
    this.title = 'Invoice';
    this.actionTypes = ['ADD', 'UPDATE', 'SUBMIT', 'ACKNOWLEDGE'];
    this.columnConfigs = [
      { label: 'No', key: 'no', columnType: ColumnType.string },
      { label: 'Invoice Number', key: 'invoiceNumber', columnType: ColumnType.string },
      { label: 'Invoice Value', key: 'invoiceValue', columnType: ColumnType.currency, local: 'Rp. ' },
      { label: 'Reference BAST', key: 'referenceBast', columnType: ColumnType.string },
      { label: 'Date', key: 'invoiceDate', columnType: ColumnType.date, format: 'shortDate' },
      { label: 'Payment Date', key: 'paymentDate', columnType: ColumnType.date, format: 'shortDate' },
      { label: 'Due Date', key: 'dueDate', columnType: ColumnType.date, format: 'shortDate' },
      { label: 'Submit', key: 'submit', columnType: ColumnType.status },
    ];
  }

  ngOnInit() {
    this.getPOId();
    this.getSubcriptionData();
    this.getServiceData();
  }

  getPOId() {
    this.activeRoute.params.subscribe(val => {
      if (val) {
        this.poId = parseInt(val.id);
      }
    });
  }

  getServiceData() {
    this.invoiceService.findByPOId(this.poId).subscribe();
  }

  getSubcriptionData() {
    combineLatest(
      this.poService.subscriptionData(),
      this.invoiceService.subscriptionData()
    ).subscribe((val) => {
      this.data = val[1];
      this.purchaseOrder = val[0].find(po => po.getId === this.poId);
      if (this.purchaseOrder) {
        this.title = `Invoice Purchase Order ${this.purchaseOrder.getPoNumber}`
      }
    });
  }

  onEvent(event: OnEvent) {
    switch (event.type) {
      case 'ADD':
        this.addInvoice();
        break;
      case 'UPDATE':
        this.updateInvoice(event.value);
        break;
      case 'SUBMIT':
        this.submitInvoice(event.value);
        break;
      case 'ACKNOWLEDGE':
        this.setPaymentDate(event.value);
        break;
    }
  }

  addInvoice() {
    const invoice = new Invoice();
    invoice.setPo = this.purchaseOrder;
    this.dialog.open(FormInvoiceComponent, {
      data: invoice,
      width: '1000px',
      disableClose: true,
    }).afterClosed().subscribe(val => {
      if (val) {
        this.invoiceService.create(val).then(invoice => {
          this.toastService.success('Success create invoice', 'Success');
          console.log(invoice);
        }).catch(err => {
          this.toastService.error('Failed create Invoice', 'Failed');
        });
      }
    });
  }

  updateInvoice(invoice: Invoice) {
    invoice.setPo = this.purchaseOrder;
    this.dialog.open(FormInvoiceComponent, {
      data: invoice,
      width: '1000px',
      disableClose: true,
    }).afterClosed().subscribe((val: Invoice) => {
      if (val) {
        this.invoiceService.update(val)
          .then(invoice => {
          this.toastService.success('Success update invoice', 'Success');
            console.log(invoice);
          }).catch(err => {
          this.toastService.error('Failed update Invoice', 'Failed');
          });
      }
    });
  }

  submitInvoice(invoice: Invoice) {
    this.dialog.open(DialogComponent, {
      data: { id: invoice.getId, message: `Are you sure to submit invoice ${invoice.getInvoiceNumber}?` },
      width: '600px',
      disableClose: true,
    }).afterClosed().subscribe(val => {
      if (val) {
        this.invoiceService.submit(val).toPromise().then(submit => {
          this.actionTypes = ['ADD', 'ACKNOWLEDGE'];
          this.toastService.success('Success submit invoice', 'Success');
        }).catch(err => {
          this.toastService.error('Failed submit Invoice', 'Failed');
        });
      }
    });
  }

  setPaymentDate(invoice: Invoice) {
    invoice.setPo = this.purchaseOrder;
    this.dialog.open(FormPaymentComponent, {
      width: '400px',
      data: invoice,
      disableClose: true
    }).afterClosed().subscribe(val => {
      if (val) {
        invoice.setPaymentDate = val;
        invoice.setSubmit = 1;
        this.invoiceService.update(invoice).then(value => {
          this.toastService.success('Success set payment date', 'Success');
          this.actionTypes = ['ADD'];
        }).catch(err => {
          this.toastService.error('Failed set payment date', 'Failed');
          console.log(err);
        });
      }
    })
  }
}
