import { Component, OnInit, OnDestroy } from '@angular/core';
import { ColumnConfig, actionType, ColumnType, OnEvent } from 'src/app/shared/table-builder';
import { SiteWork } from 'src/app/models/siteWork.model';
import { MatDialog } from '@angular/material';
import { DialogComponent } from 'src/app/shared/dialog/dialog.component';
import { RequestService } from 'src/service/request-service/request.service';
import { Request, RequestStatus, RequestType } from 'src/app/models/request.model';
import { SowType } from 'src/app/models/sow-type.model';
import { SiteWorkService } from 'src/service/siteWork-service/site-work.service';
import { FormSowListComponent } from '../../presentational/form-sow-list/form-sow-list.component';
import { Finance } from 'src/app/models/finance.model';
import { AAAService } from 'src/app/shared/aaa/aaa.service';
import { combineLatest, Subject } from 'rxjs';
import { FinanceService } from 'src/service/finance-service/finance.service';
import { takeUntil } from 'rxjs/operators';


interface TableModel {
  id: number;
  projectId: string;
  assignmentId: string;
  site: string;
  sowTypes: SowType[];
  status: string;
  projectManager: string;
  payment: number;
}

@Component({
  selector: 'app-project',
  template: `
  <app-project-table
     [data]="data"
     [title]="title"
     [actionTypes]="actionTypes"
     (onEvent)="onEvent($event)"
     [columnConfigs]="columnConfigs"
  >
  </app-project-table>`
})
export class ProjectComponent implements OnInit, OnDestroy {
  data: any[];
  title: string;
  actionTypes: actionType[];
  columnConfigs: ColumnConfig[];

  destroy$: Subject<boolean>;
  request: Request[] = [];
  siteworks: SiteWork[] = [];
  finance: Finance;

  constructor(
    public dialog: MatDialog,
    public requestService: RequestService,
    public siteWorkServie: SiteWorkService,
    public aaaService: AAAService,
    public financeService: FinanceService,
  ) {
    this.data = [];
    this.title = 'Project';
    this.actionTypes = ['VIEW', 'REQUEST'];
    this.columnConfigs = [
      { label: 'No', key: 'no', columnType: ColumnType.string },
      { label: 'Project Id', key: 'projectId', columnType: ColumnType.string },
      { label: 'Assignment Id', key: 'assignmentId', columnType: ColumnType.string },
      { label: 'Project Manager', key: 'projectManager', columnType: ColumnType.string },
      { label: 'Payment', key: 'payment', columnType: ColumnType.progressBar },
    ];
    this.destroy$ = new Subject();
  }

  getAuthFinances(finances: Finance[]) {
    return finances.filter(_set => {
      return (
        !this.aaaService.isAuthorized("read:project:{province:$}:{job:$}") ||
        this.aaaService.isAuthorized( 
          `read:project:{province:$}:{job:${_set.getId}}`
        )
      );
    });
  }

  ngOnInit() {
    this.subscriptionData();
    this.getServiceData();
  }

  ngOnDestroy() {
    this.destroy$.next(true);
    this.destroy$.complete();
  }

  getServiceData() {
    combineLatest(
      this.siteWorkServie.findNotPaid(),
      this.financeService.getData()
    )
    .pipe(takeUntil(this.destroy$))
    .subscribe(val => {
      console.log(val);
    });
  }

  subscriptionData() {
    combineLatest(
      this.siteWorkServie.subscriptionData(),
      this.financeService.subscriptionData()
    ).subscribe((val: [SiteWork[], Finance[]]) => {
        //PERLU AUTHENTICATION AGAR DAPAT USER YANG MASUK SAAT INI MASIH DAPAT SEMUA
        this.finance = this.getAuthFinances(val[1])[0];
        this.siteworks = val[0];
        this.data = val[0].map(sitework => {
          return this.setTableModel(sitework);
        })
      });
  }

  setTableModel(sitework: SiteWork) {
    const persentasePayment = (sitework.getTotalInvoice / sitework.getTotalPoValue) * 100;
    return <TableModel>{
      id: sitework.getId,
      projectId: sitework.getProjectCode,
      // assignmentId: sitework.getAssignmentId,
      site: sitework.getSite ? sitework.getSite.getName : '',
      projectManager: sitework.getProjectManager ? sitework.getProjectManager.getParty['name'] : '',
      sowTypes: sitework.getSowTypes,
      status: sitework.getStatus,
      payment: persentasePayment ? persentasePayment.toFixed(0) : 0
    }
  }

  onEvent(event: OnEvent) {
    switch (event.type) {
      case 'REQUEST':
        this.requestCorrectionPid(event.value);
        break;
      case 'VIEW':
        this.showSowList(event.value ? event.value['sowTypes'] : []);
        break;
    }
  }

  requestCorrectionPid(val: TableModel) {
    this.dialog.open(DialogComponent, {
      data: { id: val.id, message: `Are sure to request correction Project Id ${val.projectId ? val.projectId : ''}?` },
      width: '500px',
      disableClose: true,
    }).afterClosed().subscribe(id => {
      if (id) {
          this.createRequestPid(id);
      }
    });
  }

  createRequestPid(projectId: number) {
    const sitework = this.findSitework(projectId);
    const request = new Request();
    request.setObject = sitework;
    request.setStatus = RequestStatus.proposed;
    request.setRequester = this.finance;
    request.setStartDate = new Date();
    request.setType = RequestType.correction_project_id;
    console.log(request);
    // this.requestService.create(request)
    // .then()
  }

  showSowList(value: Object[]) {
    if (value) {
      const sowTypes = value.map(val => {
        return new SowType(val);
      });
      this.dialog.open(FormSowListComponent, {
        width: '900px',
        disableClose: true,
        data: sowTypes
      });
    }
  }

  findSitework(id: number) {
    return this.siteworks.find(project => {
      return project.getId === id;
    })
  }
}
