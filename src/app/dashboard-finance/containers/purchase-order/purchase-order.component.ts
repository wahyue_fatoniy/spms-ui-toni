import { Component, OnInit } from '@angular/core';
import { actionType, ColumnConfig, ColumnType, OnEvent } from 'src/app/shared/table-builder';
import { Router } from '@angular/router';
import { MatDialog } from '@angular/material';
import { PurchaseOrderService } from 'src/service/purchasorder-service/purchase-order.service';
import { PurchaseOrder } from 'src/app/models/purchaseOrder.model';
import { FormProjectListComponent } from '../../presentational/form-project-list/form-project-list.component';

interface TableModel {
  id: number;
  poNumber: string;
  customer: string;
  sales: string;
  partner: string;
  poValue: number;
  totalInvoice: number;
  publishDate: Date;
  expiredDate: Date;
  status: string;
}

@Component({
  selector: 'app-purchase-order',
  template: `
    <app-purchaseorder-table
      [data]="data"
      [title]="title"
      [actionTypes]="actionTypes"
      (onEvent)="onEvent($event)"
      [columnConfigs]="columnConfigs"
    >
    </app-purchaseorder-table>
  `,
})
export class PurchaseOrderComponent implements OnInit {
  data: any[];
  title: string;
  actionTypes: actionType[];
  columnConfigs: ColumnConfig[];
  purchaseOrder: PurchaseOrder[];

  constructor(
    public router: Router,
    public dialog: MatDialog,
    public poService: PurchaseOrderService
  ) {
    this.data = [];
    this.title = 'Purchase Order';
    this.actionTypes = ['VIEW', 'ADD'];
    this.columnConfigs = [
      { label: 'No. ', key: 'no', columnType: ColumnType.string },
      { label: 'PO Number. ', key: 'poNumber', columnType: ColumnType.string },
      { label: 'Customer', key: 'customer', columnType: ColumnType.string },
      { label: 'Sales', key: 'sales', columnType: ColumnType.string },
      { label: 'PO Value', key: 'poValue', columnType: ColumnType.currency, local: 'Rp. ' },
      { label: 'Total Invoice', key: 'totalInvoice', columnType: ColumnType.currency, local: 'Rp. ' },
      { label: 'Publish Date', key: 'publishDate', columnType: ColumnType.date, format: 'shortDate' },
      { label: 'Expired Date', key: 'expiredDate', columnType: ColumnType.date, format: 'shortDate' },
      { label: 'Status', key: 'status', columnType: ColumnType.status },
    ];
  }

  ngOnInit() {
    this.getSubscriptionData();
    this.getServiceData();
  }

  getServiceData() {
    this.poService.getData().subscribe();
  }

  getSubscriptionData() {
    this.poService.subscriptionData()
      .subscribe(val => {
        this.purchaseOrder = val;
        this.data = val.map(po => {
          return this.setTableModel(po);
        });
      });
  }

  setTableModel(purchaseOrder: PurchaseOrder): TableModel {
    return {
      id: purchaseOrder.getId,
      poNumber: purchaseOrder.getPoNumber,
      customer: purchaseOrder.getCustomer.getParty ? purchaseOrder.getCustomer.getParty['name'] : '',
      partner: purchaseOrder.getPartner.getParty ? purchaseOrder.getPartner.getParty['name'] : '',
      sales: purchaseOrder.getSales.getParty ? purchaseOrder.getSales.getParty['name'] : '',
      poValue: purchaseOrder.getPoValue ? purchaseOrder.getPoValue : 0,
      totalInvoice: purchaseOrder.getTotalInvoice ? purchaseOrder.getTotalInvoice : 0,
      publishDate: new Date(purchaseOrder.getPublishDate),
      expiredDate: new Date(purchaseOrder.getExpiredDate),
      status: purchaseOrder.getStatus
    };
  }

  onEvent(event: OnEvent) {
    if (event.type === 'VIEW') {
      this.showProjects(event.value);
    } else {
      this.router.navigate([`home/dashboard-finance/invoice/${event.value['id']}`]).catch(err => {
        console.log(err);
      });
    }
  }

  showProjects(model: TableModel) {
    const po = this.purchaseOrder.find(po => po.getId === model.id);
    this.dialog.open(FormProjectListComponent, {
      data: po.getSiteWorks ? po.getSiteWorks : [],
      width: '1000px',
      disableClose: true
    });
  }



}
