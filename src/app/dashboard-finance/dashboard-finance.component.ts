import { Component, OnDestroy } from "@angular/core";
import { TabsMenu } from "../pipes/tabs-pipe/tabs-pipe.pipe";
import { Router, NavigationEnd } from "@angular/router";
import { filter } from "rxjs/operators";
import { Location } from "@angular/common";
import { Subscription } from "rxjs";

@Component({
  selector: "app-dashboard-finance",
  template: `
    <nav mat-tab-nav-bar style="margin-top:-75px;">
      <a
        *ngFor="let link of tabsMenu"
        mat-tab-link
        style="text-decoration: none"
        [routerLink]="link.path"
        routerLinkActive
        #rla="routerLinkActive"
        [active]="rla.isActive"
      >
        <img [src]="link.img" style="margin-bottom:7px;" />&nbsp;
        {{ link.title }}
      </a>
    </nav>
    <div class="container-home">
      <router-outlet></router-outlet>
    </div>
  `,
  styles: [],
})
export class DashboardFinanceComponent implements OnDestroy {
  tabsMenu: TabsMenu[] = [
    {
      path: "dashboard",
      title: "Dashboard",
      img: "/assets/img/collection/pm.png"
    },
    {
      path: "purchase-order",
      title: "Purchase Order",
      img: "/assets/img/collection/budget.png"
    },
    {
      path: "project",
      title: "Project",
      img: "/assets/img/collection/project1.png"
    },
  ];
  subscription: Subscription;

  constructor(
    private router: Router, private location: Location
  ) {

    this.subscription = this.router.events
      .pipe(filter(event => event instanceof NavigationEnd))
      .subscribe(({ urlAfterRedirects }: NavigationEnd) => {
        const path = urlAfterRedirects.split("/").pop();
        const url = urlAfterRedirects.split("/");

        if (url[url.length - 2] === 'invoice') {
          this.tabsMenu = [
            {
              path: "dashboard",
              title: "Dashboard",
              img: "/assets/img/collection/pm.png"
            },
            {
              path: "approval",
              title: "Approval",
              img: "/assets/img/collection/approvedDoc.png"
            },
            {
              path: "purchase-order",
              title: "Purchase Order",
              img: "/assets/img/collection/budget.png"
            },
            {
              path: `invoice/${parseInt(url[url.length - 1])}`,
              title: "Invoice",
              img: "/assets/img/collection/invoice.png"
            },
            {
              path: "project",
              title: "Project",
              img: "/assets/img/collection/project1.png"
            },
          ];
        } else {
          this.tabsMenu = [
            {
              path: "dashboard",
              title: "Dashboard",
              img: "/assets/img/collection/pm.png"
            },
            {
              path: "approval",
              title: "Approval",
              img: "/assets/img/collection/approvedDoc.png"
            },
            {
              path: "purchase-order",
              title: "Purchase Order",
              img: "/assets/img/collection/budget.png"
            },
            {
              path: "project",
              title: "Project",
              img: "/assets/img/collection/project1.png"
            },
          ];
        }
      });
    if (
      this.location.path() !==
      "/home/dashboard-finance/project"
    ) {
      this.router
        .navigate(["/home/dashboard-finance/dashboard"])
        .catch(err => console.log(err));
    }
  }

  ngOnDestroy() {
    if (this.subscription) {
      this.subscription.unsubscribe();
    }
  }

}
