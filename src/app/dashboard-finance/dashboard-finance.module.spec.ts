import { DashboardFinanceModule } from './dashboard-finance.module';

describe('DashboardFinanceModule', () => {
  let dashboardFinanceModule: DashboardFinanceModule;

  beforeEach(() => {
    dashboardFinanceModule = new DashboardFinanceModule();
  });

  it('should create an instance', () => {
    expect(dashboardFinanceModule).toBeTruthy();
  });
});
