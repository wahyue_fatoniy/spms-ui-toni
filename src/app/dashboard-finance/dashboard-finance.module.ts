import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { DashboardFinanceComponent } from './dashboard-finance.component';
import { SharedModule } from '../shared/shared.module';
import { ProjectComponent } from './containers/project/project.component';
import { PurchaseOrderComponent } from './containers/purchase-order/purchase-order.component';
import { ProjectTableComponent } from './presentational/project-table/project-table.component';
import { FormAddPidComponent } from './presentational/form-add-pid/form-add-pid.component';
import { PurchaseorderTableComponent } from './presentational/purchaseorder-table/purchaseorder-table.component';
import { FinancePurchaseOrderComponent } from './containers/finance-purchase-order/finance-purchase-order.component';
import { InvoiceFormV2Component } from './containers/finance-purchase-order/invoice-form-v2/invoice-form-v2.component';
import { InvoiceListComponent } from './containers/finance-purchase-order/invoice-list/invoice-list.component';
import { InvoiceComponent } from './containers/invoice/invoice.component';
import { InvoiceTableComponent } from './presentational/invoice-table/invoice-table.component';
import { FormInvoiceComponent } from './presentational/form-invoice/form-invoice.component';
import { FormRejectComponent } from './presentational/form-reject/form-reject.component';
import { MaterialModule } from '../material.module';
import { FormSowListComponent } from './presentational/form-sow-list/form-sow-list.component';
import { FormPaymentComponent } from './presentational/form-payment/form-payment.component';
import { ApprovalTableComponent } from './presentational/approval-table/approval-table.component';
import { ApprovalComponent } from './containers/approval/approval.component';
import { DashboardComponent } from './containers/dashboard/dashboard.component';
import { FormProjectListComponent } from './presentational/form-project-list/form-project-list.component';

const routes: Routes = [
  { path: '', component: DashboardFinanceComponent, children: [
    { path: '', pathMatch: 'full', redirectTo: '' },
    { path: 'dashboard', component: DashboardComponent },
    { path: 'approval', component: ApprovalComponent },
    { path: 'project', component: ProjectComponent },
    { path: 'purchase-order', component: PurchaseOrderComponent },
    { path: 'invoice/:id', component: InvoiceComponent }
  ] }
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    SharedModule,
    MaterialModule,
  ],
  declarations: [
    DashboardFinanceComponent,
    ProjectComponent,
    PurchaseOrderComponent,
    ProjectTableComponent,
    FormAddPidComponent,
    PurchaseorderTableComponent,
    FinancePurchaseOrderComponent,
    InvoiceFormV2Component,
    InvoiceListComponent,
    InvoiceComponent,
    InvoiceTableComponent,
    FormInvoiceComponent,
    FormRejectComponent,
    FormSowListComponent,
    FormPaymentComponent,
    ApprovalTableComponent,
    ApprovalComponent,
    DashboardComponent,
    FormProjectListComponent,
  ],
  entryComponents: [
    FormAddPidComponent,
    InvoiceFormV2Component,
    FormInvoiceComponent,
    FormRejectComponent,
    FormSowListComponent,
    FormPaymentComponent,
    FormProjectListComponent,
  ]
})
export class DashboardFinanceModule { }
