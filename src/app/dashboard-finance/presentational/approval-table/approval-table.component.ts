import { Component, OnInit, EventEmitter, ViewChild, Output, Input } from '@angular/core';
import { TableBuilder, OnEvent, ColumnConfig, actionType } from 'src/app/shared/table-builder';
import { MatPaginator, MatSort } from '@angular/material';

@Component({
  selector: 'app-approval-table',
  templateUrl: './approval-table.component.html',
  styleUrls: ['./approval-table.component.css']
})
export class ApprovalTableComponent extends TableBuilder implements OnInit {
  @ViewChild(MatPaginator) _paginator: MatPaginator;
  @ViewChild(MatSort) _sort: MatSort;

  @Output() onEvent: EventEmitter<OnEvent>;
  @Input() columnConfigs: ColumnConfig[];
  @Input() actionTypes: actionType[];
  @Input() title: string;
  @Input() data: any[];

  public selectField: boolean;
  public selectedData: any;
  public rowIndex: number;

  constructor() {
    super();
    this.onEvent = new EventEmitter();
  }

  ngOnInit() {
    this.paginator = this._paginator;
    this.sort = this._sort;
    this.setDisplayedColumns();
    this.setTypeFilters();
  }

  ngOnChanges() {
    this.datasource.data = this.data;
  }

  ngOnDestroy() {
    this.subscription.next(true);
    this.subscription.complete();
  }

  onSelectRow(element) {
    this.selectField = true;
    this.selectedData = element;

    if (element['status'] === 'ACCEPTED') {
        this.actionTypes = [];
    }
  }

  onEventClick(value: OnEvent) {
    value.value = this.selectedData;
    if (value.type === 'HOVER' ) {
      if (this.selectField) {
        this.onEvent.emit(value);
      }
    } else {
      this.onEvent.emit(value);
    }
  }

  onDoubleClick() {
    this.rowIndex = null;
    this.selectField = false;
  }

}
