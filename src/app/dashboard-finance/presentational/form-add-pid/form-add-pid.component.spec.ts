import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FormAddPidComponent } from './form-add-pid.component';

describe('FormAddPidComponent', () => {
  let component: FormAddPidComponent;
  let fixture: ComponentFixture<FormAddPidComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FormAddPidComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FormAddPidComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
