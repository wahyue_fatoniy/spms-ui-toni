import { Component, OnInit, Inject } from '@angular/core';
import { MAT_DIALOG_DATA } from '@angular/material';
import { SiteWork } from 'src/app/models/siteWork.model';
import { FormControl, Validators } from '@angular/forms';

@Component({
  selector: 'app-form-add-pid',
  templateUrl: './form-add-pid.component.html',
  styleUrls: ['./form-add-pid.component.css']
})
export class FormAddPidComponent implements OnInit {
  control: FormControl;

  constructor(
  ) {this.control = new FormControl(null, Validators.required);
  }

  ngOnInit() {
  }

}
