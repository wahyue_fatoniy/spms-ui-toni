import { Component, OnInit, Inject } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material';
import { Invoice } from 'src/app/models/invoice.model';
import { FormGroup } from '@angular/forms';
import { FormlyFieldConfig } from '@ngx-formly/core';
import { PurchaseOrder } from 'src/app/models/purchaseOrder.model';

interface FormModel {
  id: number,
  invoiceNumber: string;
  invoiceValue: number;
  invoiceDate: Date;
  dueDate: Date;
  referenceBast: string;
}


@Component({
  selector: 'app-form-invoice',
  templateUrl: './form-invoice.component.html',
  styleUrls: ['./form-invoice.component.css']
})
export class FormInvoiceComponent implements OnInit {
  fields: FormlyFieldConfig[];
  form: FormGroup;
  model: FormModel;
  purchaseOrder: PurchaseOrder;

  constructor(
    @Inject(MAT_DIALOG_DATA) public formData: Invoice,
    public dialogRef: MatDialogRef<any>
  ) {
    this.form = new FormGroup({});
    this.fields = formData.formFieldConfig;
    this.purchaseOrder = formData.getPo;
  }

  ngOnInit() {
    this.setFormModel();
    this.setRemainingInvoice();
  }

  setRemainingInvoice() {
    this.fields = this.fields.map(field => {
      if (field.id === 'row_1') {
        field.fieldGroup = field.fieldGroup.map(member => {
          if (member.key === 'invoiceValue') {
            member.templateOptions.max = (this.purchaseOrder.getPoValue - this.purchaseOrder.getTotalInvoice) > 0 ? (this.purchaseOrder.getPoValue - this.purchaseOrder.getTotalInvoice) : null;
            member.expressionProperties = {
              'templateOptions.description' : '"Remaining: "+"Rp. "+(this.field.templateOptions.max)'
            }
            member.validation = {
              messages : {
                max: "Exceed the remaining!"
              }
            }
          }
          return member;
        });
      }
      return field;
    });
  }

  setFormModel() {
    this.model = {
      id: this.formData.getId,
      invoiceValue: this.formData.getInvoiceValue,
      invoiceDate: new Date(this.formData.getInvoiceDate),
      invoiceNumber: this.formData.getInvoiceNumber,
      dueDate: new Date(this.formData.getDueDate),
      referenceBast: this.formData.getRefBast
    }
  }

  onClose() {
    const invoice = new Invoice();
    invoice.setId = this.model.id;
    invoice.setInvoiceDate = this.model.invoiceDate;
    invoice.setInvoiceValue = this.model.invoiceValue;
    invoice.setInvoiceNumber = this.model.invoiceNumber;
    invoice.setDueDate = this.model.dueDate;
    invoice.setRefBast = this.model.referenceBast;
    invoice.setPo = this.purchaseOrder;
    invoice.setSubmit = 0;
    this.dialogRef.close(invoice);
  }

}
