import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-form-payment',
  templateUrl: './form-payment.component.html',
  styleUrls: ['./form-payment.component.css']
})
export class FormPaymentComponent implements OnInit {
  paymentDate: Date;
  constructor() {
    this.paymentDate = null;
  }

  ngOnInit() {
  }

}
