import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FormProjectListComponent } from './form-project-list.component';

describe('FormProjectListComponent', () => {
  let component: FormProjectListComponent;
  let fixture: ComponentFixture<FormProjectListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FormProjectListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FormProjectListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
