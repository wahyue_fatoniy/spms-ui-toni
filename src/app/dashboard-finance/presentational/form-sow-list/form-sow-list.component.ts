import { Component, Inject } from '@angular/core';
import { MAT_DIALOG_DATA } from '@angular/material';
import { SowType } from 'src/app/models/sow-type.model';

@Component({
  selector: 'app-form-sow-list',
  templateUrl: './form-sow-list.component.html',
  styleUrls: ['./form-sow-list.component.css']
})
export class FormSowListComponent {
  displayedColumns: string[];

  constructor(
    @Inject(MAT_DIALOG_DATA) public formData: SowType[]
  ) { 
    this.displayedColumns = ['no', 'sowCode', 'description']
  }
}
