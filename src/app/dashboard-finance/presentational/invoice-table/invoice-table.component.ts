import { Component, OnInit, ViewChild, EventEmitter, Output, Input } from '@angular/core';
import { TableBuilder, OnEvent, ColumnConfig, actionType } from 'src/app/shared/table-builder';
import { MatPaginator, MatSort, MatDialog } from '@angular/material';
import { PurchaseOrder } from 'src/app/models/purchaseOrder.model';
import { SiteWork } from 'src/app/models/siteWork.model';
import { FormProjectListComponent } from '../form-project-list/form-project-list.component';
import { Invoice } from 'src/app/models/invoice.model';

@Component({
  selector: 'app-invoice-table',
  templateUrl: './invoice-table.component.html',
  styleUrls: ['./invoice-table.component.css']
})
export class InvoiceTableComponent extends TableBuilder implements OnInit {
  @ViewChild(MatPaginator) _paginator: MatPaginator;
  @ViewChild(MatSort) _sort: MatSort;

  @Output() onEvent: EventEmitter<OnEvent>;
  @Input() columnConfigs: ColumnConfig[];
  @Input() actionTypes: actionType[];
  @Input() title: string;
  @Input() data: any[];
  @Input() purchaseOrder: PurchaseOrder;

  public selectField: boolean;
  public selectedData: any;
  public rowIndex: number;
  public totalInvoice: number;

  constructor(
    private dialog: MatDialog
  ) {
    super();
    this.onEvent = new EventEmitter();
  }

  ngOnInit() {
    this.paginator = this._paginator;
    this.sort = this._sort;
    this.setDisplayedColumns();
    this.setTypeFilters();
  }

  ngOnChanges() {
    console.log(this.data);
    this.datasource.data = this.data;
    this.setTotalInvoice();
    this.setHideButton(this.actionTypes);
  }

  ngOnDestroy() {
    this.subscription.next(true);
    this.subscription.complete();
  }

  setTotalInvoice() {
    this.totalInvoice = 0;
    this.data.forEach((invoice: Invoice) => {
      if (invoice.getSubmit) {
        this.totalInvoice += invoice.getInvoiceValue ? invoice.getInvoiceValue : 0;
      }
    });
  }

  onSelectRow(element) {
    this.selectField = true;
    this.selectedData = element;
    this.actionTypes = ['ADD', 'UPDATE', 'SUBMIT'];
    if (element['submit']) {
      this.setHideButton(['ADD', 'ACKNOWLEDGE']);
    }
    if (element['paymentDate'] !== null && element['paymentDate'] !== undefined) {
      this.setHideButton(['ADD']);
    }
  }

  setHideButton(buttons: actionType[]) {
    if (this.totalInvoice >= this.purchaseOrder.getPoValue) {
      this.actionTypes = buttons.filter(button => button !== 'ADD');
    } else {
      this.actionTypes = buttons;
    }
  }

  onShowListProject(projects: SiteWork[]) {
    this.dialog.open(FormProjectListComponent, {
      width: '1000px',
      data: projects,
      disableClose: true
    });
  }

  onEventClick(value: OnEvent) {
    value.value = this.selectedData;
    this.onEvent.emit(value);
  }

  onUnselectRow() {
    this.selectField = false;
    this.selectedData = null;
    this.rowIndex = null;
  }

}
