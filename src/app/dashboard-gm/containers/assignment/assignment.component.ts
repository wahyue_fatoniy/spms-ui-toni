import { Component, OnInit, OnDestroy } from '@angular/core';
import { actionType, ColumnConfig, ColumnType, OnEvent } from 'src/app/shared/table-builder';
import { MatDialog } from '@angular/material';
import { DialogComponent } from 'src/app/shared/dialog/dialog.component';
import { SiteWork } from 'src/app/models/siteWork.model';
import { FormRejectAssignmentComponent } from '../../presentational/form-reject-assignment/form-reject-assignment.component';
import { Request, RequestStatus, RequestType } from 'src/app/models/request.model';
import { Subject, forkJoin } from 'rxjs';
import { takeUntil, combineAll } from 'rxjs/operators';
import { RequestService } from 'src/service/request-service/request.service';
import { AssignmentService } from 'src/service/assignment-service/assignment.service';
import { FormViewApprovalComponent } from '../../presentational/form-view-approval/form-view-approval.component';
import { ToastrService } from 'ngx-toastr';
import { AAAService } from 'src/app/shared/aaa/aaa.service';
import { User } from 'src/app/models/user.model';
import { Role } from 'src/app/shared/models/role/role';
import { Contract, ContractStatus } from 'src/app/models/contract.model';
import { ContractService } from 'src/service/contract-service/contract.service';

interface TableModel {
  id: number;
  number: string;
  startDate: string;
  siteWorks: SiteWork[];
  customer: string;
  requester: string;
}

@Component({
  selector: 'app-assignment',
  template: `
    <app-table
      [data]="data"
      [title]="title"
      [actionTypes]="actionTypes"
      (onEvent)="onEvent($event)"
      [columnConfigs]="columnConfigs"
      >
    </app-table>
  `,
})
export class AssignmentComponent implements OnInit, OnDestroy {
  data: TableModel[];
  title: string;
  actionTypes: actionType[];
  columnConfigs: ColumnConfig[];


  destroy$: Subject<boolean>;
  requests: Request[];
  approver: User;

  constructor(
    public dialog: MatDialog,
    public requestService: RequestService,
    public assignmentService: AssignmentService,
    public toastService: ToastrService,
    public aaaService: AAAService,
    public contractService: ContractService
  ) {
    this.data = [];
    this.title = 'Approval';
    this.actionTypes = ['ACCEPT', 'REJECT', 'VIEW'];
    this.columnConfigs = [
      { label: 'No.', key: 'no', columnType: ColumnType.string },
      { label: 'Number', key: 'number', columnType: ColumnType.string },
      { label: 'Activity Id', key: 'activityId', columnType: ColumnType.string },
      { label: 'Customer', key: 'customer', columnType: ColumnType.string },
      { label: 'Date', key: 'startDate', columnType: ColumnType.date, format: 'mediumDate' },
      { label: 'Requester', key: 'requester', columnType: ColumnType.string },
    ];
  }

  ngOnInit() {
    this.destroy$ = new Subject();
    this.getServiceData();
    this.subscriptionData();
  }

  ngOnDestroy() {
    this.destroy$.next(true);
    this.destroy$.complete();
  }

  initApprover() {
    const currentUser = this.aaaService.getCurrentUser() ? this.aaaService.getCurrentUser().getUserMetadata().roles.find(role => {
      return role.roleType === 'USER'
    }) : new Role();

    this.approver = new User();
    this.approver['id'] = currentUser['id'];
    this.approver.setUsername = currentUser['username'];
    this.approver.setPassword = currentUser['password'];
    this.approver.setEmail = currentUser['email'];
  }

  getServiceData() {
    this.requestService.findByStatus(RequestStatus.proposed, RequestType.submitted_assignment)
      .pipe(
        takeUntil(this.destroy$)
      )
      .subscribe();
  }

  subscriptionData() {
    this.requestService.subscriptionData().subscribe((val: Request[]) => {
      this.requests = val;
      this.data = val.filter(value => {
        return value.getStatus === RequestStatus.proposed;
      }).map(request => {
        return this.setTableModel(request);
      });
    });
  }

  setTableModel(data: Request): TableModel {
    return {
      id: data.getId,
      number: data.getObject['number'],
      siteWorks: data.getObject['siteWorks'] ? data.getObject['siteWorks'].map(sitework => new SiteWork(sitework)) : [],
      customer: data.getObject['customer'] ? data.getObject['customer']['party']['name'] : '',
      startDate: data.getStartDate,
      requester: data.getRequester ? data.getRequester['party']['name'] : ''
    }
  };
  
  onEvent(event: OnEvent) {
    switch (event.type) {
      case 'ACCEPT':
        this.approveRequest(event.value);
        break;
      case 'REJECT':
        this.rejectRequest(event.value);
        break;
      case 'VIEW':
        this.showProjectList(event.value);
        break;
    }
  }

  approveRequest(model: TableModel) {
    const request = this.findRequest(model);
    this.dialog.open(DialogComponent, {
      data: { id: model.id, message: `Are you sure to accept assignment with id ${model.number}?` },
      disableClose: true,
      width: '600px;'
    }).afterClosed().subscribe(val => {
      if (val) {
        const contract = new Contract(request.getObject);
        request.setStatus = RequestStatus.accepted;
        request.setEndDate = new Date();
        request.setApprover = this.approver;
        contract.setStatus = ContractStatus.APPROVED;

        forkJoin(
          this.contractService.update(contract),
          this.requestService.update(request)
        ).toPromise()
        .then(val => {
          this.toastService.success('Success approve assignment', 'Success');
          console.log(val);
        })
        .catch(err => {
          this.toastService.error('Failed approve assignment', 'Failed');
          console.log(err);
        });
      }
    });
  }


  rejectRequest(model: TableModel) {
    const request = this.findRequest(model);
    this.dialog.open(FormRejectAssignmentComponent, {
      data: request,
      disableClose: true,
      width: '600px'
    }).afterClosed().subscribe(val => {
      if (val) {
        const contract = new Contract(request.getObject);
        contract.setStatus = ContractStatus.CREATED;
        request.setApprover = this.approver;

        forkJoin(
          this.requestService.update(request),
          this.contractService.update(contract)
        ).toPromise().then(value => {
          this.toastService.success('Success reject assignment', 'Success');
          console.log(value);
        }).catch(err => {
          this.toastService.error('Failed reject assignment', 'Failed');
          console.log(err);
        });
      }
    });
  }

  showProjectList(model: TableModel) {
    const request = this.findRequest(model);
    const contract = new Contract(request.getObject);
    this.dialog.open(FormViewApprovalComponent, {
      data: contract,
      disableClose: true,
      width: '1200px'
    });
  }

  findRequest(model: TableModel): Request {
    return this.requests.find(request => {
      return request.getId === model.id;
    });
  }

}
