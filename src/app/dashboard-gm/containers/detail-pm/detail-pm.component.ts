import { Component, OnInit } from "@angular/core";
import { ProjectManagerService } from "src/service/projectManager-service/project-manager.service";
import { SiteWorkService } from "src/service/siteWork-service/site-work.service";
import { ProjectManager } from "src/app/models/projectManager.model";
import { Subject } from "rxjs";
import { takeUntil } from "rxjs/operators";
import { WidgetData } from "src/app/shared/widget-component/widget-component.component";
import { SiteWork } from "src/app/models/siteWork.model";
import { LocalCurrencyPipe } from "src/app/pipes/localCurrency/local-currency.pipe";
import { Router } from "@angular/router";
import { Document } from "src/app/models/document.model";
import { RangeDate } from "src/app/shared/range-date/range-date.component";

export interface PieChartOptions {
  view: number[];
  data: Object[];
  animation: boolean;
  labels: boolean;
  legend: boolean;
  legendTitle: string;
  legendPosition: string;
}

@Component({
  selector: "app-detail-pm",
  templateUrl: "./detail-pm.component.html",
  styleUrls: ["./detail-pm.component.css"]
})
export class DetailPmComponent implements OnInit {
  private destroy$ = new Subject();
  public pmData: ProjectManager[] = [];
  public projectDone: WidgetData;
  public projectOnProgress: WidgetData;
  public projectNotStarted: WidgetData;
  public projectCancel: WidgetData;
  public totalProject: WidgetData;
  public poValueDone: WidgetData;
  public poValueOnProgress: WidgetData;
  private localCurrency: LocalCurrencyPipe = new LocalCurrencyPipe();
  public pieChartOptions: PieChartOptions = {
    view: [670, 300],
    data: [
      { name: "Done", value: 0 },
      { name: "On Progress", value: 0 },
      { name: "Not Started", value: 0 },
      { name: "Cancel", value: 0 }
    ],
    animation: true,
    labels: true,
    legend: true,
    legendTitle: "Status",
    legendPosition: "right"
  };
  public documentReject: WidgetData;
  public documentApprove: WidgetData;
  public documentCreate: WidgetData;
  public documentSubmit: WidgetData;
  public rangeDate: RangeDate = {
    fromDate: new Date(),
    toDate: new Date(
      new Date().setDate(new Date().getDate() - new Date().getDay() + 30)
    )
  };
  public pmId: number = null;

  constructor(
    private pmService: ProjectManagerService,
    private siteWorkService: SiteWorkService,
    private router: Router
  ) {}

  ngOnInit() {
    this.getLocalStorage();
    this.getServiceData();
    this.subscriptionData();
  }

  get loading() {
    return this.siteWorkService.getLoading || this.pmService.getLoading;
  }

  public getServiceData() {
    this.pmService
      .getData()
      .toPromise()
      .then();
  }

  private getLocalStorage(): void {
    let pmId = JSON.parse(localStorage.getItem("pmId"));
    let rangeDate = JSON.parse(localStorage.getItem("gmRangeDate"));
    if (pmId !== null) {
      this.pmId = pmId;
    }
    if (rangeDate !== null) {
      this.rangeDate.fromDate = rangeDate["fromDate"];
      this.rangeDate.toDate = rangeDate["toDate"];
    }
  }

  public subscriptionData() {
    this.pmService
      .subscriptionData()
      .pipe(takeUntil(this.destroy$))
      .subscribe(pm => {
        this.pmData = pm;
      });
  }

  public onSelectPm(pmId: number): void {
    this.pmId = pmId;
    this.setLocalStorage([{ key: "pmId", value: pmId.toString() }]);
    if (pmId !== 0) {
      this.siteWorkService
        .findByPMRange(
          this.pmId,
          this.rangeDate.fromDate,
          this.rangeDate.toDate
        )
        .pipe(takeUntil(this.destroy$))
        .subscribe((val: SiteWork[]) => {
          this.setWidget(val);
        });
    } else {
      this.siteWorkService.findByRange(this.rangeDate.fromDate, this.rangeDate.toDate)
      .pipe(takeUntil(this.destroy$))
      .subscribe((val: SiteWork[]) => {
        this.setWidget(val);
      });
    }
  }

  public setWidget(sitework: SiteWork[]): void {
    this.totalByStatus(sitework);
    this.getTotalPoValue(sitework);
    this.setPieChartStatus(sitework);
    this.setWidgetDocument(sitework);
  }

  public setPieChartStatus(siteworks: SiteWork[]): void {
    let done = siteworks.filter(val => val.getStatus === "DONE").length;
    let notStarted = siteworks.filter(val => val.getStatus === "NOT_STARTED")
      .length;
    let onProgress = siteworks.filter(val => val.getStatus === "ON_PROGRESS")
      .length;
    let cancel = siteworks.filter(val => val.getStatus === "CANCEL").length;

    this.pieChartOptions = {
      view: [670, 300],
      data: [
        { name: "Done", value: done ? done : 0 },
        { name: "On Progress", value: onProgress ? onProgress : 0 },
        { name: "Not Started", value: notStarted ? notStarted : 0 },
        { name: "Cancel", value: cancel ? cancel : 0 }
      ],
      animation: true,
      labels: true,
      legend: true,
      legendTitle: "Status",
      legendPosition: "right"
    };
  }

  public setWidgetDocument(siteworks: SiteWork[]): void {
    let documents: Document[] = [];
    let docCreate: Document[] = [];
    let docSubmit: Document[] = [];
    let docReject: Document[] = [];
    let docApprove: Document[] = [];
    siteworks.forEach(sitework => {
      documents = [...documents, ...sitework.getDocuments];
    });
    documents = documents.filter(doc => doc.getType !== "BAST");
    docCreate = documents.filter(doc => doc.getStatus === "CREATE");
    docSubmit = documents.filter(doc => doc.getStatus === "SUBMIT");
    docReject = documents.filter(doc => doc.getStatus === "REJECT");
    docApprove = documents.filter(doc => doc.getStatus === "APPROVE");

    this.documentCreate = {
      title: "Document Create",
      value: docCreate.length,
      icon: "file_copy",
      iconColor: "primary"
    };
    this.documentSubmit = {
      title: "Document Submit",
      value: docSubmit.length,
      icon: "send",
      iconColor: "primary"
    };
    this.documentReject = {
      title: "Document Reject",
      value: docReject.length,
      icon: "cached",
      iconColor: "warn"
    };
    this.documentApprove = {
      title: "Document Approve",
      value: docApprove.length,
      icon: "done",
      iconColor: "primary"
    };
  }

  public totalByStatus(siteWorks: SiteWork[]): void {
    this.projectCancel = {
      title: "Project Cancel",
      value: siteWorks.filter(siteWork => siteWork.getStatus === "CANCEL")
        .length,
      iconColor: "primary",
      icon: "store"
    };

    this.projectDone = {
      title: "Project Done",
      value: siteWorks.filter(siteWork => siteWork.getStatus === "DONE").length,
      iconColor: "primary",
      icon: "store"
    };

    this.projectNotStarted = {
      title: "Project Not Started",
      value: siteWorks.filter(siteWork => siteWork.getStatus === "NOT_STARTED")
        .length,
      iconColor: "primary",
      icon: "store"
    };

    this.projectOnProgress = {
      title: "Project On Progress",
      value: siteWorks.filter(siteWork => siteWork.getStatus === "ON_PROGRESS")
        .length,
      iconColor: "primary",
      icon: "store"
    };

    this.totalProject = {
      title: "Total Project",
      value: siteWorks.length,
      iconColor: "primary",
      icon: "store"
    };
  }

  public getTotalPoValue(siteWorks: SiteWork[]): void {
    let valueDone = 0;
    let valueOnProgress = 0;
    siteWorks
      .filter(sitework => sitework.getStatus === "DONE")
      .map(sitework => {
        valueDone += sitework.getPoValue;
      });

    siteWorks
      .filter(sitework => sitework.getStatus === "ON_PROGRESS")
      .map(sitework => {
        valueOnProgress += sitework.getPoValue;
      });
    this.poValueDone = {
      title: "PO Value On Done",
      value: this.localCurrency.transform(valueDone),
      iconColor: "accent",
      icon: "attach_money"
    };
    this.poValueOnProgress = {
      title: "PO Value On Progress",
      value: this.localCurrency.transform(valueOnProgress),
      iconColor: "accent",
      icon: "attach_money"
    };
  }

  public setLocalStorage(storage: { key: string; value: string }[]): void {
    storage.forEach(store => {
      localStorage.setItem(store.key, store.value);
    });
  }

  public onSelectDate(event: RangeDate): void {
    if (event) {
      this.setLocalStorage([
        { key: "gmRangeDate", value: JSON.stringify(event) }
      ]);
      this.rangeDate.fromDate = event.fromDate;
      this.rangeDate.toDate = event.toDate;
      if (this.pmId !== 0) {
        this.siteWorkService
          .findByPMRange(this.pmId, event.fromDate, event.toDate)
          .pipe(takeUntil(this.destroy$))
          .subscribe((val: SiteWork[]) => {
            this.setWidget(val);
          });
      } else {
        this.siteWorkService.findByRange(event.fromDate, event.toDate)
        .pipe(takeUntil(this.destroy$))
        .subscribe((val: SiteWork[]) => {
          this.setWidget(val);
        });
      }
    }
  }
}
