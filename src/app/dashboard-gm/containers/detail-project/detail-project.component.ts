import { Component, OnInit } from "@angular/core";
import { SiteWorkService } from "src/service/siteWork-service/site-work.service";
import { takeUntil } from "rxjs/operators";
import { Subject } from "rxjs";
import { TaskService } from "src/service/task-service/task.service";
import { SiteWork } from "src/app/models/siteWork.model";
import { Task } from "src/app/models/task.model";
import { WidgetData } from "src/app/shared/widget-component/widget-component.component";
import { DescriptionData } from "src/app/components/card-description/card-description.component";
import { DateDurationPipe } from "src/app/pipes/dateDuration/date-duration.pipe";
import { LocalCurrencyPipe } from "src/app/pipes/localCurrency/local-currency.pipe";
import { PieChartOptions } from "../detail-pm/detail-pm.component";
import { ActivatedRoute } from "@angular/router";

@Component({
  selector: "app-detail-project",
  templateUrl: "./detail-project.component.html",
  styleUrls: ["./detail-project.component.css"]
})
export class DetailProjectComponent implements OnInit {
  destroy$ = new Subject();
  taskDone: WidgetData;
  taskOnProgress: WidgetData;
  taskNotStarted: WidgetData;
  taskCancel: WidgetData;
  descriptionData: DescriptionData[] = [];
  descriptionTitle = "SOW Description";
  dateDurationPipe: DateDurationPipe = new DateDurationPipe();
  siteWorkDuration: WidgetData;
  poValue: WidgetData;
  taskTotal: WidgetData;
  localCurrencyPipe: LocalCurrencyPipe = new LocalCurrencyPipe();
  siteWorkTitle = "";
  siteworkId: number;
  pieChartOptions: PieChartOptions = {
    view: [670, 300],
    data: [
      {
        name: "Internal I",
        value: 0
      },
      {
        name: "Internal E",
        value: 0
      },
      {
        name: "Major I",
        value: 0
      },
      {
        name: "Major E",
        value: 0
      }
    ],
    animation: true,
    labels: true,
    legend: true,
    legendTitle: "Status",
    legendPosition: "right"
  };
  constructor(
    public siteWorkService: SiteWorkService,
    public taskService: TaskService,
    public activeRoute: ActivatedRoute
  ) {}

  ngOnInit() {
    this.getRouterParams();
    this.subscriptionData();
  }

  get loading(): boolean {
    return this.siteWorkService.getLoading || this.taskService.getLoading;
  }

  getRouterParams() {
    this.activeRoute.params
    .pipe(takeUntil(this.destroy$))
    .subscribe((param: {siteworkId: string}) => {
      if (param) {
        this.siteworkId = parseInt(param.siteworkId);
      }
    });
  }

  subscriptionData() {
    this.siteWorkService
      .subscriptionData()
      .pipe(takeUntil(this.destroy$))
      .subscribe((val: SiteWork[]) => {
        if (val.length !== 0) {
          let sitework = val.find(find => find.getId === this.siteworkId);
          this.siteWorkTitle = sitework ? sitework.getSite.getName : "";
          this.taskService
            .findBySiteWorkId(sitework ? sitework.getId : null)
            .toPromise()
            .then((tasks: Task[]) => {
              this.getDuration(sitework);
              this.getTotalByStatus(tasks);
              this.getPoValue(sitework);
              this.setWidgetTasks(tasks);
            });
        }
      });
  }

  getDuration(siteWork: SiteWork) {
    // show siteWorkDuration
    this.siteWorkDuration = {
      title: "Duration",
      value: this.dateDurationPipe.transform(siteWork.getStartDate),
      iconColor: "primary",
      icon: "date_range"
    };
  }

  getTotalByStatus(tasks: Task[]) {
    let taskDone = 0;
    let taskCancel = 0;
    let taskNotStarted = 0;
    let taskOnProgress = 0;
    taskDone = tasks.filter(task => task.getStatus === "DONE").length;
    taskCancel = tasks.filter(task => task.getStatus === "CANCEL").length;
    taskNotStarted = tasks.filter(task => task.getStatus === "NOT_STARTED")
      .length;
    taskOnProgress = tasks.filter(task => task.getStatus === "ON_PROGRESS")
      .length;

    this.taskCancel = {
      title: "Task Cancel",
      value: taskCancel,
      iconColor: "primary",
      icon: "assignment"
    };
    this.taskDone = {
      title: "Task Done",
      value: taskDone,
      iconColor: "primary",
      icon: "assignment"
    };

    this.taskNotStarted = {
      title: "Task Not Started",
      value: taskNotStarted,
      iconColor: "primary",
      icon: "assignment"
    };

    this.taskOnProgress = {
      title: "Task On Progress",
      value: taskOnProgress,
      iconColor: "primary",
      icon: "assignment"
    };
    this.taskTotal = {
      title: "Task Total",
      value: tasks.length,
      iconColor: "primary",
      icon: "assignment"
    };
  }

  getPoValue(sitework: SiteWork) {
    this.poValue = {
      title: "PO Value",
      value: this.localCurrencyPipe.transform(sitework.getPoValue),
      iconColor: "accent",
      icon: "attach_money"
    };
  }

  setWidgetTasks(tasks: Task[]) {
    this.pieChartOptions.data = [
      // {
      //   name: "Internal I",
      //   value: tasks.filter(task => task.getSeverity === "INTERNAL_I").length
      // },
      // {
      //   name: "Internal E",
      //   value: tasks.filter(task => task.getSeverity === "INTERNAL_E").length
      // },
      // {
      //   name: "Major I",
      //   value: tasks.filter(task => task.getSeverity === "MAJOR_I").length
      // },
      // {
      //   name: "Major E",
      //   value: tasks.filter(task => task.getSeverity === "MAJOR_E").length
      // }
    ];
  }
}
