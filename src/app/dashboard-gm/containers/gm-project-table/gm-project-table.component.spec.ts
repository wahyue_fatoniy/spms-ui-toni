import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GmProjectTableComponent } from './gm-project-table.component';

describe('GmProjectTableComponent', () => {
  let component: GmProjectTableComponent;
  let fixture: ComponentFixture<GmProjectTableComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GmProjectTableComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GmProjectTableComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
