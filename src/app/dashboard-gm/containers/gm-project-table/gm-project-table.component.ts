import { Component, OnInit, OnDestroy } from "@angular/core";
import { SiteWork } from "src/app/models/siteWork.model";
import { SiteWorkService } from "src/service/siteWork-service/site-work.service";
import { Subject } from "rxjs";
import { takeUntil } from "rxjs/operators";
import { TaskService } from "src/service/task-service/task.service";
import { Router } from "@angular/router";
import { ProjectManagerService } from "src/service/projectManager-service/project-manager.service";
// import { XlsExportPipe } from "src/app/pipes/xlsExport/xls-export.pipe";
import { actionType, ColumnConfig, OnEvent, ColumnType } from "src/app/shared/table-builder";
import { AssignmentStatus } from "src/app/models/assignment.model";

interface TableModel {
  id: number;
  projectId: string;
  assignmentId: string;
  site: string;
  projectManager: string;
  value: number;
  progress: number;
  payment: number;
}

@Component({
  selector: "app-gm-project-table",
  template: `
    <app-project-table
      [data]="data"
      [title]="title"
      [actionTypes]="actionTypes"
      [columnConfigs]="columnConfigs"
      (onEvent)="onEvent($event)"
    >
    </app-project-table>
  `,
})
export class GmProjectTableComponent
  implements OnInit, OnDestroy {
  public data: TableModel[];
  public title: string;
  public actionTypes: actionType[];
  public columnConfigs: ColumnConfig[];

  // private xlsExport: XlsExportPipe = new XlsExportPipe();
  private destroy$: Subject<boolean> = new Subject();

  constructor(
    public siteWorkService: SiteWorkService,
    public taskService: TaskService,
    public pmService: ProjectManagerService,
    public router: Router
  ) { }

  ngOnInit() {
    this.getSubscriptionData();
    this.getServiceData();
    this.data = [];
    this.title = 'Project';
    this.actionTypes = [];
    this.columnConfigs = [
      { label: 'No', key: 'no', columnType: ColumnType.string },
      { label: 'Project ID', key: 'projectId', columnType: ColumnType.string },
      { label: 'Assignment ID', key: 'assignmentId', columnType: ColumnType.string },
      { label: 'Project Manager', key: 'projectManager', columnType: ColumnType.string},
      { label: 'Value', key: 'value', columnType: ColumnType.currency, local: 'Rp. ' },
      { label: 'Progress', key: 'progress', columnType: ColumnType.progressBar },
      { label: 'Payment', key: 'payment', columnType: ColumnType.progressBar },
    ];
  }

  ngOnDestroy() {
    this.destroy$.next(true);
    this.destroy$.complete();
  }

  public getServiceData() {
    this.siteWorkService.findNotPaid().subscribe(val => {
    });
  }

  public getSubscriptionData(): void {
    this.siteWorkService
      .subscriptionData()
      .subscribe((siteWorks: SiteWork[]) => {
        this.data = this.setTableModel(siteWorks);
      });
  }

  setTableModel(siteworks: SiteWork[]): TableModel[] {
    return siteworks.map(sitework => {
      const persentasePayment = (sitework.getTotalInvoice / sitework.getTotalPoValue) * 100;
      return {
        id: sitework.getId,
        projectId: sitework.getProjectCode,
        assignmentId: null,
        site: sitework.getSite.getName,
        projectManager: sitework.getProjectManager.getParty ? sitework.getProjectManager.getParty['name'] : '',
        value: sitework.getPoValue,
        progress: sitework.getProgress ? sitework.getProgress : 0,
        payment: persentasePayment ? parseInt(persentasePayment.toFixed(0)) : 0,
      }
    });
  }

  onEvent(event: OnEvent) {
    console.log(event);
  }
}
