import { Component, OnInit, OnDestroy } from '@angular/core';
import { actionType, ColumnConfig, ColumnType, OnEvent } from 'src/app/shared/table-builder';
import { PurchaseOrderService } from 'src/service/purchasorder-service/purchase-order.service';
import { Subscription } from 'rxjs';
import { PurchaseOrder } from 'src/app/models/purchaseOrder.model';


interface TableModel {
  id: number;
  poNumber: string;
  customer: string;
  partner: string;
  sales: string;
  poValue: number;
  totalInvoice: number;
  publishDate: string;
  expiredDate: string;
}

@Component({
  selector: 'app-purchase-order',
  template: `
    <app-purchaseorder-table
      [data]="data"
      [title]="title"
      [actionTypes]="actionTypes"
      [columnConfigs]="columnConfigs"
      (onEvent)="onEvent($event)"
    >
    </app-purchaseorder-table>
  `,
})
export class PurchaseOrderComponent implements OnInit, OnDestroy {
  public data: TableModel[];
  public title: string;
  public actionTypes: actionType[];
  public columnConfigs: ColumnConfig[];

  public subscription: Subscription;

  constructor(
    private poService: PurchaseOrderService
  ) { }

  ngOnInit() {
    this.getSubscriptionData();
    this.getServiceData();
    this.data = [];
    this.title = 'Purchase Order';
    this.actionTypes = [];
    this.columnConfigs = [
      { label: 'No. ', key: 'no', columnType: ColumnType.string },
      { label: 'PO Number. ', key: 'poNumber', columnType: ColumnType.string },
      { label: 'Customer', key: 'customer', columnType: ColumnType.string },
      { label: 'Partner', key: 'partner', columnType: ColumnType.string },
      { label: 'Sales', key: 'sales', columnType: ColumnType.string },
      { label: 'PO Value', key: 'poValue', columnType: ColumnType.currency, local: 'Rp. ' },
      { label: 'Total Invoice', key: 'totalInvoice', columnType: ColumnType.currency, local: 'Rp. ' },
      { label: 'Publish Date', key: 'publishDate', columnType: ColumnType.date, format: 'mediumDate' },
      { label: 'Expired Date', key: 'expiredDate', columnType: ColumnType.date, format: 'mediumDate' },
    ];
  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
  }

  getServiceData() {
    this.subscription = this.poService.getData()
      .subscribe()
  }

  getSubscriptionData() {
    this.poService.subscriptionData().subscribe(po => {
      this.data = this.setTableModel(po);
    });
  }

  setTableModel(pos: PurchaseOrder[]): TableModel[] {
    return pos.map(po => {
      return {
        id: po.getId,
        poNumber: po.getPoNumber,
        customer: po.getCustomer.getParty ? po.getCustomer.getParty['name'] : '',
        partner: po.getPartner.getParty ? po.getPartner.getParty['name'] : '',
        sales: po.getSales.getParty ? po.getSales.getParty['name'] : '',
        poValue: po.getPoValue,
        totalInvoice: po.getTotalInvoice,
        publishDate: po.getPublishDate,
        expiredDate: po.getExpiredDate
      }
    });
  }

  onEvent(event: OnEvent) {
  }
}
