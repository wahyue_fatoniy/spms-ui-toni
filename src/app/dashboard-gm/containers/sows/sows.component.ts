import { Component, OnInit } from '@angular/core';
import { ColumnType, actionType, ColumnConfig, OnEvent } from 'src/app/shared/table-builder';
import { SowType } from 'src/app/models/sow-type.model';
import { MatDialog } from '@angular/material';
import { FormSowsComponent } from '../../presentational/form-sows/form-sows.component';
import { RegionService } from 'src/service/region-service/region.service';
import { SowTypeService } from 'src/service/sowType-service/sow-type.service';
import { combineLatest } from 'rxjs';
import { Region } from 'src/app/models/region.model';
import { DialogComponent } from 'src/app/shared/dialog/dialog.component';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-sows',
  template: `
    <app-sows-table 
      [data]="data"
      [title]="title"
      [actionTypes]="actionTypes"
      [columnConfigs]="columnConfigs"
      (onEvent)="onEvent($event)">
    </app-sows-table>
  `,
})
export class SowsComponent implements OnInit {
  public data: any[];
  public title: string;
  public actionTypes: actionType[];
  public columnConfigs: ColumnConfig[];

  public regions: Region[];

  constructor(
    public dialog: MatDialog,
    public regionService: RegionService,
    public sowTypeService: SowTypeService,
    public toastService: ToastrService
  ) { }

  ngOnInit() {
    this.getServiceData();
    this.getSubscriptionData();
    this.data = [];
    this.title = 'Sow';
    this.actionTypes = ["ADD", "UPDATE"];
    this.columnConfigs = [
      { key: "no", label: "No.", columnType: ColumnType.string },
      { key: "sowCode", label: "Sow Code", columnType: ColumnType.string },
      { key: "description", label: "Description", columnType: ColumnType.string },
      { key: "price", label: "Price", columnType: ColumnType.currency, local: "Rp. " },
      { key: "engineerDay", label: "ED", columnType: ColumnType.string },
      { key: "technicianDay", label: "TD", columnType: ColumnType.string },
      { key: "documentationDay", label: "DD", columnType: ColumnType.string },
      { key: "atpDay", label: "ATP Day", columnType: ColumnType.string },
      { key: "region", label: "Region", columnType: ColumnType.object, objectKey: 'name' },
    ];
  }

  getServiceData() {
    combineLatest(
      this.regionService.getData(),
      this.sowTypeService.getData(),
    ).subscribe(val => {
    });
  }

  getSubscriptionData() {
    combineLatest(
      this.regionService.subscriptionData(),
      this.sowTypeService.subscriptionData()
    )
    .subscribe(val => {
      this.regions = val[0];
      this.data = val[1];
    });
  }

  onEvent(event: OnEvent) {
    switch (event.type) {
      case 'ADD':
        this.addSow();
        break;
      case 'UPDATE':
        this.updateSow(event.value);
        break;
      case 'DELETE': 
        this.deleteSow(event.value);
    }
  }

  addSow() {
    this.dialog.open(FormSowsComponent, {
      data: { data: new SowType(), regions: this.regions },
      width: '900px',
      disableClose: true,
    }).afterClosed().subscribe((val: SowType) => {
      if (val) {
        this.sowTypeService.create(val).then(sowType => {
          this.toastService.success('Success create sow', 'Success');
          console.log(sowType);
        }).catch(err => {
          this.toastService.error('Failed create sow', 'Failed');
          console.error(err);
        });
      }
    });
  }

  updateSow(sowType: SowType) {
    this.dialog.open(FormSowsComponent, {
      data: { data: sowType, regions: this.regions },
      width: '900px', 
      disableClose: true,
    }).afterClosed().subscribe(val => {
      if (val) {
        this.sowTypeService.update(val).then(val => {
          this.toastService.success('Success update sow', 'Success');
          console.log(val);
        }).catch(err => {
          this.toastService.error('Failed update sow', 'Failed');
          console.log(err);
        }); 
      }
    });
  }

  deleteSow(sowType: SowType) {
    this.dialog.open(DialogComponent, {
      width: '600px',
      data: { id: sowType.getId, message: `Are tou sure to delete SOW with code ${sowType.getSowCode}?` },
      disableClose: true,
    }).afterClosed().subscribe(val => {
      if (val) {
        console.log(val);
      }
    });
  }
}
