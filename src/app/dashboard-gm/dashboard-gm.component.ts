import { Component, OnInit, OnDestroy } from "@angular/core";
import { Subscription } from "rxjs";
import { Router, ActivatedRoute } from "@angular/router";
import { Location } from "@angular/common";
import { TabsPipePipe } from "../pipes/tabs-pipe/tabs-pipe.pipe";
import { TabsMenu } from "../dashboard-pm/dashboard-pm.component";
import { NumberInStringPipe } from "../pipes/numberInString/number-in-string.pipe";

@Component({
  selector: "app-dashboard-gm",
  template: `
    <nav mat-tab-nav-bar style="margin-top:-75px;">
      <a
        style="text-decoration: none"
        mat-tab-link
        *ngFor="let link of links"
        [routerLink]="link.path"
        routerLinkActive
        #rla="routerLinkActive"
        [active]="rla.isActive"
      >
        <img [src]="link.img" style="margin-bottom:7px;" />&nbsp;
        {{ link.title }}
      </a>
    </nav>
    <div class="container-home">
      <router-outlet></router-outlet>
    </div>
  `
})
export class DashboardGmComponent {
  public links: TabsMenu[] = [
    {
      path: "dashboard",
      title: "Dashboard",
      img: "/assets/img/collection/pm.png"
    },
    {
      path: "assignment",
      title: "Approval",
      img: "/assets/img/collection/approvedDoc.png"
    },
    {
      path: "sows",
      title: "Sow",
      img: "/assets/img/collection/sow.png"
    },
    {
      path: "project",
      title: "Project",
      img: "/assets/img/collection/project1.png"
    },
    // {
    //   path: "purchase-order",
    //   title: "Purchase Order",
    //   img: "/assets/img/collection/finance.png"
    // }
  ];
  constructor() { }


}
