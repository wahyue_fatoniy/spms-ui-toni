import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { DetailPmComponent } from './containers/detail-pm/detail-pm.component';
import { DashboardGmComponent } from './dashboard-gm.component';
import { SharedModule } from '../shared/shared.module';
import { NgxChartsModule } from '@swimlane/ngx-charts';
import { NgxMatDrpModule } from 'ngx-mat-daterange-picker';
import { TableComponent } from './presentational/table/table.component';
import { AssignmentComponent } from './containers/assignment/assignment.component';
import { FormListProjectComponent } from './presentational/form-list-project/form-list-project.component';
import { FormListSowComponent } from './presentational/form-list-sow/form-list-sow.component';
import { GmProjectTableComponent } from './containers/gm-project-table/gm-project-table.component';
import { ProjectTableComponent } from './presentational/project-table/project-table.component';
import { PurchaseOrderComponent } from './containers/purchase-order/purchase-order.component';
import { SowsComponent } from './containers/sows/sows.component';
import { SowsTableComponent } from './presentational/sows-table/sows-table.component';
import { FormSowsComponent } from './presentational/form-sows/form-sows.component';
import { PurchaseorderTableComponent } from './presentational/purchaseorder-table/purchaseorder-table.component';
import { FormlyModule } from '@ngx-formly/core';
import { FormlyMaterialModule } from '@ngx-formly/material';
import { FormRejectAssignmentComponent } from './presentational/form-reject-assignment/form-reject-assignment.component';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material';
import { WidgetComponentComponent } from '../shared/widget-component/widget-component.component';
import { MaterialModule } from '../material.module';
import { FormViewApprovalComponent } from './presentational/form-view-approval/form-view-approval.component';
import { FormProjectComponent } from './presentational/form-project/form-project.component';

const routes: Routes = [
  {
    path: '', component: DashboardGmComponent, children: [
      { path: '', redirectTo: 'dashboard', pathMatch: 'full' },
      { path: 'dashboard', component: DetailPmComponent },
      { path: 'assignment', component: AssignmentComponent },
      { path: 'project', component: GmProjectTableComponent },
      { path: 'purchase-order', component: PurchaseOrderComponent },
      { path: 'sows', component: SowsComponent },
    ]
  }
];

@NgModule({
  imports: [
    CommonModule,
    SharedModule,
    RouterModule.forChild(routes),
    NgxChartsModule,
    NgxMatDrpModule,
    MaterialModule,

    FormlyModule,
    FormlyMaterialModule,
  ],
  declarations: [
    DetailPmComponent,
    DashboardGmComponent,
    TableComponent,
    AssignmentComponent,
    FormListProjectComponent,
    FormListSowComponent,
    GmProjectTableComponent,
    ProjectTableComponent,
    PurchaseOrderComponent,
    PurchaseorderTableComponent,
    SowsComponent,
    SowsTableComponent,
    FormSowsComponent,
    FormRejectAssignmentComponent,
    FormViewApprovalComponent,
    FormProjectComponent,
  ],
  entryComponents: [
    FormSowsComponent,
    FormListProjectComponent,
    FormListSowComponent,
    FormRejectAssignmentComponent,
    FormViewApprovalComponent
  ],
  providers: [
    { provide: MAT_DIALOG_DATA, useValue: {} },
     { provide: MatDialogRef, useValue: {} }
  ]
})
export class DashboardGmModule { }