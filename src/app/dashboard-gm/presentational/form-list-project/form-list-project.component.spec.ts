import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FormListProjectComponent } from './form-list-project.component';

describe('FormListProjectComponent', () => {
  let component: FormListProjectComponent;
  let fixture: ComponentFixture<FormListProjectComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FormListProjectComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FormListProjectComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
