import { Component, Inject } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialog } from '@angular/material';
import { SiteWork } from 'src/app/models/siteWork.model';
import { FormListSowComponent } from '../form-list-sow/form-list-sow.component';
import { SowType } from 'src/app/models/sow-type.model';

interface FormModel {
  assignmentCode: string;
  site: string;
  value: number;
  sowTypes: SowType[]
}

@Component({
  selector: 'app-form-list-project',
  templateUrl: './form-list-project.component.html',
  styleUrls: ['./form-list-project.component.css']
})
export class FormListProjectComponent {
  sources: FormModel[];
  public displayedColumns: string[];

  constructor(
    @Inject(MAT_DIALOG_DATA) public formData: SiteWork[],
    public dialog: MatDialog
  ) {
    this.setSources();
    this.displayedColumns = ['no', 'site', 'value', 'sow']
  }

  setSources() {
    let source: FormModel;
    this.sources = this.formData.map(data => {
      source =  {
        assignmentCode: data.getAssignmentCode,
        site: data.getSite ? data.getSite.getName : '',
        value: 0,
        sowTypes: data.getSowTypes
      }
      data.getSowTypes.forEach(type => {
        source.value += type.getPrice;
      });
      return source;
    });
  }

  onHoverSow(element: SowType[]) {
    this.dialog.open(FormListSowComponent, {
      data: element,
      width: '1000px',
    });
  }
}
