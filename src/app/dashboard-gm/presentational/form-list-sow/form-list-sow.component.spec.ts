import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FormListSowComponent } from './form-list-sow.component';

describe('FormListSowComponent', () => {
  let component: FormListSowComponent;
  let fixture: ComponentFixture<FormListSowComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FormListSowComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FormListSowComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
