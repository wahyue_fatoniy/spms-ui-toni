import { Component, Inject } from '@angular/core';
import { MAT_DIALOG_DATA } from '@angular/material';
import { SowType } from 'src/app/models/sow-type.model';

@Component({
  selector: 'app-form-list-sow',
  templateUrl: './form-list-sow.component.html',
  styleUrls: ['./form-list-sow.component.css']
})
export class FormListSowComponent {
  displayedColumns: string[];

  constructor(
    @Inject(MAT_DIALOG_DATA) public formData: SowType[]
  ) { 
    this.displayedColumns = ['no', 'sowCode', 'description']
  }

}
