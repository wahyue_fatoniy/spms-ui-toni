import { Component, OnInit, Inject } from '@angular/core';
import { MAT_DIALOG_DATA } from '@angular/material';
import { SiteWork } from 'src/app/models/siteWork.model';
import { FormlyFieldConfig } from '@ngx-formly/core';
import { FormGroup } from '@angular/forms';
import { DateDurationPipe } from 'src/app/pipes/dateDuration/date-duration.pipe';

interface FormModel {
  projectId: string;
  assignmentId: string;
  projectManager: string;
  site: string;
  startDate: string;
  endDate: string;
  duration: string;
}

@Component({
  selector: 'app-form-project',
  templateUrl: './form-project.component.html',
  styleUrls: ['./form-project.component.css']
})
export class FormProjectComponent implements OnInit {
  durationPipe: DateDurationPipe;
  fields: FormlyFieldConfig[];
  model: FormModel;
  form: FormGroup;

  constructor(
    @Inject(MAT_DIALOG_DATA) public formData: SiteWork
  ) {
    this.form = new FormGroup({});
    this.durationPipe = new DateDurationPipe();
    this.fields = this.setFormFields();
  }

  setFormMOdel() {
    this.model = {
      projectId: this.formData.getProjectCode,
      assignmentId: this.formData['assignmentId'],
      projectManager: this.formData.getProjectManager.getParty ? this.formData.getProjectManager.getParty['name'] : '',
      site: this.formData.getSite.getName,
      startDate: this.formData.getStartDate,
      endDate: this.formData.getEndDate,
      duration: this.durationPipe.transform(this.formData.getStartDate) 
    }
  }

  setFormFields(): FormlyFieldConfig[] {
    const formFields: FormlyFieldConfig[] = [
      {
        fieldGroupClassName: 'row',
        fieldGroup: [
          {
            className: 'col-4',
            key: 'projectId',
            type: 'input',
            templateOptions: {
              placeholder: 'Project ID',
              disabled: true,
            }
          },
          {
            className: 'col-4',
            key: 'assignmentId',
            type: 'input',
            templateOptions: {
              placeholder: 'Assignment ID',
              disabled: true,
            }
          },
          {
            className: 'col-4',
            key: 'projectManager',
            type: 'input',
            templateOptions: {
              placeholder: 'Project Manager',
              disabled: true,
            }
          }
        ]
      },
      {
        fieldGroupClassName: 'row',
        fieldGroup: [
          {
            className: 'col-4',
            key: 'startDate',
            type: 'input',
            templateOptions: {
              placeholder: 'Start Date',
              disabled: true,
            }
          },
          {
            className: 'col-4',
            key: 'endDate',
            type: 'input',
            templateOptions: {
              placeholder: 'End Date',
              disabled: true,
            }
          },
          {
            className: 'col-4',
            key: 'duration',
            type: 'input',
            templateOptions: {
              placeholder: 'Duration ',
              disabled: true,
            }
          }
        ]
      },
      {
        fieldGroupClassName: 'row',
        fieldGroup: [
          {
            className: 'col-4',
            key: 'site',
            type: 'input',
            templateOptions: {
              placeholder: 'Site',
              disabled: true,
            }
          }
        ]
      }
    ];
    return formFields;
  }

  ngOnInit() {
  }

}
