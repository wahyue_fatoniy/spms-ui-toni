import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FormRejectAssignmentComponent } from './form-reject-assignment.component';

describe('FormRejectAssignmentComponent', () => {
  let component: FormRejectAssignmentComponent;
  let fixture: ComponentFixture<FormRejectAssignmentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FormRejectAssignmentComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FormRejectAssignmentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
