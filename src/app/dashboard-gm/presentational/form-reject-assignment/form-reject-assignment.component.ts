import { Component, Inject } from '@angular/core';
import { FormControl, Validators } from '@angular/forms';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material';
import { Request, RequestStatus } from 'src/app/models/request.model';

@Component({
  selector: 'app-form-reject-assignment',
  templateUrl: './form-reject-assignment.component.html',
  styleUrls: ['./form-reject-assignment.component.css']
})
export class FormRejectAssignmentComponent {
  control: FormControl;
  constructor(
    @Inject(MAT_DIALOG_DATA) public formData: Request,
    private dialogRef: MatDialogRef<any>
  ) {
    this.control = new FormControl(null, Validators.required);
  }

  onClose() {
    this.formData.setDescription = this.control.value;
    this.formData.setStatus = RequestStatus.rejected;
    this.dialogRef.close(this.formData);
  }
}
