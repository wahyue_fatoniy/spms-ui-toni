import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FormSowsComponent } from './form-sows.component';

describe('FormSowsComponent', () => {
  let component: FormSowsComponent;
  let fixture: ComponentFixture<FormSowsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FormSowsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FormSowsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
