import { Component, OnInit, Inject } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material';
import { SowType } from 'src/app/models/sow-type.model';
import { FormlyFieldConfig } from '@ngx-formly/core';
import { FormGroup } from '@angular/forms';
import { Region } from 'src/app/models/region.model';

interface FormModel {
  id: number,
  sowCode: string;
  description: string;
  price: number;
  engineerDay: number;
  technicianDay: number;
  documentationDay: number;
  atpDay: number;
  region: number;
}

interface FormData {
  data: SowType;
  regions: Region[];
}

@Component({
  selector: 'app-form-sows',
  templateUrl: './form-sows.component.html',
  styleUrls: ['./form-sows.component.css']
})
export class FormSowsComponent implements OnInit {
  fields: FormlyFieldConfig[];
  form: FormGroup;
  model: FormModel;

  regions: Region[];

  constructor(
    @Inject(MAT_DIALOG_DATA) public formData: FormData,
    public dialogRef: MatDialogRef<any>
  ) {
    this.form = new FormGroup({});
    this.fields = formData.data.formFields();
    this.regions = formData.regions;
  }

  ngOnInit() {
    this.setFormModel();
    this.setRegionOptions();
  }

  setFormModel() {
    this.model = {
      id: this.formData.data.getId,
      sowCode: this.formData.data.getSowCode,
      description: this.formData.data.getDescription,
      price: this.formData.data.getPrice,
      engineerDay: this.formData.data.getEngineerday,
      technicianDay: this.formData.data.getTechnicianday,
      documentationDay: this.formData.data.getDocumentationday,
      atpDay: this.formData.data.getAtpDay,
      region: this.formData.data.getRegion ? this.formData.data.getRegion.getId : null,
    };
  }

  setRegionOptions() {
    this.fields = this.fields.map(field => {
      if (field.id === '4') {
        field.fieldGroup = field.fieldGroup.map(group => {
          if (group.key === 'region') {
            group.templateOptions.options = this.regions.map(region => {
              return { value: region.getId, label: region.getName }
            });

            if (this.model.region) {
              group.templateOptions.disabled = true;
            }
          }
          return group;
        })
      }
      return field;
    });
  }

  onClose() {
    const model = new SowType();
    model.setId = this.model.id;
    model.setSowCode = this.model.sowCode;
    model.setDescription = this.model.description;
    model.setPrice = this.model.price;
    model.setRegion = this.regions.find(region => {
      return region.getId === this.model.region;
    });
    model.setEngineerDay = this.model.engineerDay;
    model.setTechnicianDay = this.model.technicianDay;
    model.setDocumentationDay = this.model.documentationDay;
    model.setAtpDay = this.model.atpDay;
    this.dialogRef.close(model);
  }

}
