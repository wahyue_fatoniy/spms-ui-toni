import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FormViewApprovalComponent } from './form-view-approval.component';

describe('FormViewApprovalComponent', () => {
  let component: FormViewApprovalComponent;
  let fixture: ComponentFixture<FormViewApprovalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FormViewApprovalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FormViewApprovalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
