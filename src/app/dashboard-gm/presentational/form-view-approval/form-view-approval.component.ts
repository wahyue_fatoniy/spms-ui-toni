import { Component, OnInit, Inject } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { FormlyFieldConfig } from '@ngx-formly/core';
import { MAT_DIALOG_DATA } from '@angular/material';
import { Assignment } from 'src/app/models/assignment.model';
import { SiteWork } from 'src/app/models/siteWork.model';
import { Contract } from 'src/app/models/contract.model';

interface FormModel {
  id: number;
  number: string;
  name: string;
  startDate: Date;
  endDate: Date;
  customer: number;
}

@Component({
  selector: 'app-form-view-approval',
  templateUrl: './form-view-approval.component.html',
  styleUrls: ['./form-view-approval.component.css']
})

export class FormViewApprovalComponent implements OnInit {
  displayedColumns: string[];
  fields: FormlyFieldConfig[];
  model: FormModel;
  form: FormGroup;

  projects: SiteWork[];

  constructor(
    @Inject(MAT_DIALOG_DATA) public formData: Contract
  ) {
    this.form = new FormGroup({});
    this.fields = formData.formFieldConfig;

    this.setOptionsCustomer();
    this.setHideGeneralManager();
    this.setDisabledField();
    this.setFormModel();
    this.initTableProjects();
    this.displayedColumns = ['no', 'site', 'value', 'sow'];
  }

  ngOnInit() { }

  setFormModel() {
    this.model = {
      id: this.formData.getId,
      number: this.formData.getNumber,
      name: 'this.formData.getName',
      startDate: new Date(this.formData.getStartDate),
      endDate: new Date(this.formData.getEndDate),
      customer: this.formData.getCustomer ? this.formData.getCustomer.getId : null
    }
  }

  initTableProjects() {
    this.projects = this.formData.getSiteworks ? this.formData.getSiteworks : [];
    this.projects = this.projects.map(project => {
      project['value'] = 0;
      project.getSowTypes.forEach(type => {
        project['value'] += type.getPrice;
      });
      return project;
    });
  }

  setDisabledField() {
    this.fields = this.fields.map(field => {
      if (field.fieldGroup) {
        field.fieldGroup = field.fieldGroup.map(group => {
          group.templateOptions.disabled = true;
          return group;
        });
      } else {
        if (field.key === 'description') {
          field.templateOptions.disabled = true;
        }
      }
      return field;
    });
  }

  setOptionsCustomer() {
    this.fields = this.fields.map(field => {
      if (field.id === '1') {
        field.fieldGroup = field.fieldGroup.map(group => {
          if (group.key === 'customer') {
            group.templateOptions.options = group.templateOptions.options = [
              { label: this.formData.getCustomer ? this.formData.getCustomer.getParty['name'] : '', value: this.formData.getProjectManager ? this.formData.getCustomer.getId : null }
            ];
          }
          return group;
        })
      }
      return field;
    });
  }

  setHideGeneralManager() {
    this.fields = this.fields.map(field => {
      if (field.id === '2') {
        field.fieldGroup = field.fieldGroup.map(group => {
          if (group.key === 'generalManager') {
            group.hideExpression = true;
          }
          return group;
        })
      }
      return field;
    });
  }

}
