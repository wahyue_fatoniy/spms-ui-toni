import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PurchaseorderTableComponent } from './purchaseorder-table.component';

describe('PurchaseorderTableComponent', () => {
  let component: PurchaseorderTableComponent;
  let fixture: ComponentFixture<PurchaseorderTableComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PurchaseorderTableComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PurchaseorderTableComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
