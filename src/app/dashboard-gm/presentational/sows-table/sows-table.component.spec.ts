import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SowsTableComponent } from './sows-table.component';

describe('SowsTableComponent', () => {
  let component: SowsTableComponent;
  let fixture: ComponentFixture<SowsTableComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SowsTableComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SowsTableComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
