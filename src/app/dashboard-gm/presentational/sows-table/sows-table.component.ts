import { Component, OnInit, EventEmitter, Output, ViewChild, Input } from '@angular/core';
import { TableBuilder, OnEvent, actionType, ColumnConfig } from 'src/app/shared/table-builder';
import { MatPaginator, MatSort } from '@angular/material';

@Component({
  selector: 'app-sows-table',
  templateUrl: './sows-table.component.html',
  styleUrls: ['./sows-table.component.css']
})
export class SowsTableComponent extends TableBuilder implements OnInit {

  
  @ViewChild(MatPaginator) _paginator: MatPaginator;
  @ViewChild(MatSort) _sort: MatSort;

  @Output() onEvent: EventEmitter<OnEvent>;
  @Input() columnConfigs: ColumnConfig[];
  @Input() actionTypes: actionType[];
  @Input() title: string;
  @Input() data: any[];

  public selectField: boolean;
  public selectedData: any;
  public rowIndex: number;

  constructor() {
    super();
    this.onEvent = new EventEmitter();
  }

  ngOnInit() {
    this.paginator = this._paginator;
    this.sort = this._sort;
    this.setDisplayedColumns();
    this.setTypeFilters();
  }

  ngOnChanges() {
    this.datasource.data = this.data;
  }

  ngOnDestroy() {
    this.subscription.next(true);
    this.subscription.complete();
  }

  onSelectRow(element) {
    this.selectField = true;
    this.selectedData = element;
  }

  onEventClick(value: OnEvent) {
    value.value = this.selectedData;
    this.onEvent.emit(value);
  }

  onReload() {
    this.selectedData = {};
    this.selectField = false;
    this.rowIndex = null;
  }

  onUnselectRow() {
    this.selectField = false;
    this.rowIndex = null;
  }

}
