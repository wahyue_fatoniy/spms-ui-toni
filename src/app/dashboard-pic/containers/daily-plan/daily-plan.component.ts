import { Component, OnInit, OnDestroy } from '@angular/core';
import { ColumnConfig, actionType, OnEvent, ColumnType } from 'src/app/shared/table-builder';
import { MatDialog } from '@angular/material';
import { PmWeeklyPlanningFormComponent } from '../../presentational/pm-weekly-planning-form/pm-weekly-planning-form.component';
import { Router, ActivatedRoute } from '@angular/router';
import { TaskService } from 'src/service/task-service/task.service';
import { DocumentationService } from 'src/service/documentation-service/documentation.service';
import { EngineerService } from 'src/service/engineer-service/engineer.service';
import { TechnicianService } from 'src/service/technician-service/technician.service';
import { Subject, combineLatest } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { DailyPlan } from 'src/app/models/dailyPlan.model';
import { SiteWorkService } from 'src/service/siteWork-service/site-work.service';
import { SiteWork } from 'src/app/models/siteWork.model';
import { DailyPlanService } from 'src/service/dailyplan-service/daily-plan.service';
import { ToastrService } from 'ngx-toastr';
import { DialogComponent } from 'src/app/shared/dialog/dialog.component';
import { Request, RequestStatus, RequestType } from 'src/app/models/request.model';
import { AAAService } from 'src/app/shared/aaa/aaa.service';
import { CoordinatorService } from 'src/service/coordinator-service/coordinator.service';
import { Coordinator } from 'src/app/models/coordinator.model';
import { RequestService } from 'src/service/request-service/request.service';

interface TableModel {
  id: number;
  date: Date;
  opex: number;
}

@Component({
  selector: 'app-daily-plan',
  template: `
    <app-dailyplan-table
      [data]="data"
      [title]="title"
      [actionTypes]="actionTypes"
      (onEvent)="onEvent($event)"
      [columnConfigs]="columnConfigs"
    >
    </app-dailyplan-table>
  `
})
export class DailyPlanComponent implements OnInit, OnDestroy {
  columnConfigs: ColumnConfig[];
  actionTypes: actionType[];
  title: string;
  data: TableModel[];

  destroy$: Subject<boolean>;
  projectId: number;
  sitework: SiteWork;
  dailyPlans: DailyPlan[];
  coordinator: Coordinator;

  constructor(
    public dialog: MatDialog,
    public route: Router,
    public taskService: TaskService,
    public documentationService: DocumentationService,
    public enginnerService: EngineerService,
    public technicianService: TechnicianService,
    public activeRoute: ActivatedRoute,
    public siteworkService: SiteWorkService,
    public dailyPlanService: DailyPlanService,
    public toastService: ToastrService,
    public aaaService: AAAService,
    public picService: CoordinatorService,
    public requestService: RequestService
  ) {
    this.data = [];
    this.title = 'Daily Planning';
    this.actionTypes = ['ADD', 'UPDATE', 'REQUEST'];
    this.columnConfigs = [
      { label: 'No', key: 'no', columnType: ColumnType.string },
      { label: 'Date', key: 'date', columnType: ColumnType.date, format: 'mediumDate' },
      // { label: 'Opex', key: 'opex', columnType: ColumnType.currency, local: 'Rp. ' },
    ];
    this.destroy$ = new Subject();
  }

  ngOnInit() {
    this.subscriptionData();
    this.subscriptionPic();
    this.runPicService();
    this.getProjectId();
    this.serviceData();
  }

  ngOnDestroy() {
    this.destroy$.next(true);
    this.destroy$.complete();
  }

  getAuthPic(coordiantors: Coordinator[]) {
    return coordiantors.filter(_set => {
      const region = _set.getRegion ? _set.getRegion : null;
      const id = region ? region.getId : null;
      return (
        !this.aaaService.isAuthorized("read:project:{province:$}:{job:$}") ||
        this.aaaService.isAuthorized(
          `read:project:{province:${id}}:{job:${_set.getId}}`
        ))
    });
  }

  runPicService() {
    this.picService.getData()
    .pipe(takeUntil(this.destroy$))
    .subscribe(val => {});
  }

  subscriptionPic() {
    this.picService.subscriptionData().subscribe(val => {
      if (val.length !== 0) {
        this.coordinator = this.getAuthPic(val) ? this.getAuthPic(val)[0] : null;
      }
    });
  }



  getProjectId() {
    this.activeRoute.params.subscribe(val => {
      if (val) {
        this.projectId = parseInt(val.id);
      }
    });
  }

  serviceData() {
    combineLatest(
      this.taskService.findBySiteWorkId(this.projectId),
      this.documentationService.getData(),
      this.technicianService.getData(),
      this.enginnerService.getData(),
      this.dailyPlanService.findByProjectId(this.projectId)
    )
      .pipe(takeUntil(this.destroy$))
      .subscribe();
  }

  subscriptionData() {
    combineLatest(
      this.siteworkService.subscriptionData(),
      this.dailyPlanService.subscriptionData(),
      this.taskService.subscriptionData()
    )
      .pipe(takeUntil(this.destroy$))
      .subscribe(val => {
        console.log(val);
        this.sitework = val[0].find(sitework => sitework.getId === this.projectId);
        this.dailyPlans = val[1];
        this.data = val[1].map(plan => {
          return this.setTableModel(plan);
        });
      });
  }

  setTableModel(dailyPlan: DailyPlan) {
    return <TableModel>{
      id: dailyPlan.getId,
      date: dailyPlan.getDate ? new Date(dailyPlan.getDate) : null,
      opex: dailyPlan.getOpex ? dailyPlan.getOpex : 0
    }
  }

  onEvent(event: OnEvent) {
    console.log(event);
    switch (event.type) {
      case 'ADD':
        this.addDailyPlann();
        break;
      case 'UPDATE':
        this.updateDailyPlann(event.value);
        break;
      case 'REQUEST':
        this.onRequestOpex(event.value);
        break;
      default:
        break;
    }
  }

  addDailyPlann() {
    this.dialog.open(PmWeeklyPlanningFormComponent, {
      width: '2000px',
      data: {
        data: new DailyPlan(),
        sitework: this.sitework,
        tasks: this.taskService.subscriptionData(),
        technicians: this.technicianService.subscriptionData(),
        engineers: this.enginnerService.subscriptionData(),
        documentations: this.documentationService.subscriptionData(),
        alarms: [],
      },
      disableClose: true,
      autoFocus: true,
      maxHeight: '96vh'
    }).afterClosed().subscribe((dailyPlan: DailyPlan) => {
      if (dailyPlan) {
        console.log(dailyPlan);
        this.dailyPlanService.create(dailyPlan)
          .then(result => {
            this.toastService.success('Success create Daily Planning');
            console.log(result);
          })
          .catch(err => {
            this.toastService.error('Failed create Daily Planning');
            console.log(err);
          });
      }
    })
  }

  updateDailyPlann(value) {
    const dailyPlan = this.findDailyPlan(value ? value.id : null);
    this.dialog.open(PmWeeklyPlanningFormComponent, {
      width: '2000px',
      data: {
        data: dailyPlan,
        sitework: this.sitework,
        tasks: this.taskService.subscriptionData(),
        technicians: this.technicianService.subscriptionData(),
        engineers: this.enginnerService.subscriptionData(),
        documentations: this.documentationService.subscriptionData(),
        alarms: [],
      },
      disableClose: true,
      autoFocus: true,
      maxHeight: '96vh'
    }).afterClosed().subscribe(dailyPlan => {
      if (dailyPlan) {
        this.dailyPlanService.update(dailyPlan)
        .then(result => {
          console.log(result);
          this.toastService.success('Success update Daily Planning');
        })
        .catch(err => {
          this.toastService.error('Failed update Daily Planning');
        })
      }
    })
  }

  onRequestOpex(model: TableModel) {
    const plan = this.findDailyPlan(model.id);
    this.dialog.open(DialogComponent, {
      data: {id: plan.getId, message: 'Are you sure to request opex?'},
      width: '500px',
      disableClose: true
    }).afterClosed().subscribe(val => {
      if (val) {
        const request = new Request();
        request.setObject = plan;
        request.setStatus = RequestStatus.proposed;
        request.setType = RequestType.requested_opex;
        request.setStartDate = new Date();
        request.setRequester = this.coordinator;

        this.requestService.create(request)
        .then(() => {
          this.actionTypes = ['ADD'];
          this.toastService.success('Success create request opex', 'success');
        })
        .catch(err => {
          this.toastService.error('Failed create request opex', 'failed');
        })
      }
    });
  }

  findDailyPlan(id: number) {
    return this.dailyPlans.find(plan => {
      return plan.getId === id;
    });
  }
}
