import { Component, OnInit } from '@angular/core';
import { ColumnConfig, actionType, OnEvent, ColumnType } from 'src/app/shared/table-builder';
import { MatDialog } from '@angular/material';
import { ActivatedRoute } from '@angular/router';
import { DailyPlanService } from 'src/service/dailyplan-service/daily-plan.service';
import { DailyReportService } from 'src/service/daily-report-service/daily-report.service';
import { combineLatest } from 'rxjs';
import { DailyPlan } from 'src/app/models/dailyPlan.model';
import { DailyreportFormComponent } from '../../presentational/dailyreport-form/dailyreport-form.component';
import { Task } from 'src/app/models/task.model';
import { Resource } from 'src/app/models/resource.model';
import { SiteWorkService } from 'src/service/siteWork-service/site-work.service';
import { SiteWork } from 'src/app/models/siteWork.model';
import { DailyReport } from 'src/app/models/daliyReport.model';
import { ToastrService } from 'ngx-toastr';
import { DialogComponent } from 'src/app/shared/dialog/dialog.component';

interface TableModel {
  id: number;
  date: Date,
}

@Component({
  selector: 'app-daily-report',
  template: `
    <app-dailyreport-table
      [data]="data"
      [title]="title"
      [actionTypes]="actionTypes"
      (onEvent)="onEvent($event)"
      [columnConfigs]="columnConfigs"
    >
    </app-dailyreport-table>
  `,
  styleUrls: ['./daily-report.component.css']
})
export class DailyReportComponent implements OnInit {
  columnConfigs: ColumnConfig[];
  actionTypes: actionType[];
  title: string;
  data: TableModel[];

  dailyPlanning: DailyPlan;
  sitework: SiteWork;
  resources: Resource[];
  tasks: Task[];
  projectId: number;
  reports: DailyReport[];

  constructor(
    public dialog: MatDialog,
    public activeRoute: ActivatedRoute,
    public dailyPlanService: DailyPlanService,
    public dailyreportService: DailyReportService,
    public siteworkService: SiteWorkService,
    public toastService: ToastrService
  ) {
    this.data = [];
    this.title = 'Daily Report';
    this.actionTypes = ['ADD', 'UPDATE', 'SUBMIT'];
    this.columnConfigs = [
      { label: 'No', key: 'no', columnType: ColumnType.string },
      { label: 'Date', key: 'date', columnType: ColumnType.date, format: 'mediumDate' },
    ];
  }

  ngOnInit() {
    this.getProjectId();
    this.runSubscription();
    this.runService();
  }

  getProjectId() {
    this.activeRoute.params
      .subscribe(val => {
        if (val) {
          this.projectId = parseInt(val.id);
        }
      });
  }

  runService() {
    combineLatest(
      this.dailyPlanService.getData(),
      this.dailyreportService.findBySiteWorkId(this.projectId)
    )
      .subscribe(val => { })
  }

  runSubscription() {
    combineLatest(
      this.dailyPlanService.subscriptionData(),
      this.dailyreportService.subscriptionData(),
      this.siteworkService.subscriptionData(),
    )
      .subscribe(val => {
        this.dailyPlanning = val[0][0];
        this.sitework = val[2].find(sitework => sitework.getId === this.projectId);
        this.tasks = this.dailyPlanning ? this.dailyPlanning.getTasks : [];
        this.resources = this.dailyPlanning ? this.dailyPlanning.getResources : [];
        this.reports = val[1] ? val[1] : [];
        this.data = val[1].map(report => {
          return this.setTableModel(report);
        })
      });
  }

  setTableModel(report: DailyReport) {
    return <TableModel>{
      id: report.getId,
      date: report.getDate ? new Date(report.getDate) : null
    }
  }

  onEvent(event: OnEvent) {
    switch (event.type) {
      case 'ADD':
        this.createDailyReport();
        break;
      case 'UPDATE':
        this.updateDailyreport(event.value);
        break;
      case 'SUBMIT':
        this.submitDailyReport(event.value);
        break;
    }
  }

  createDailyReport() {
    this.dialog.open(DailyreportFormComponent, {
      data: {
        data: new DailyReport(),
        tasks: this.tasks ? this.tasks : [],
        resources: this.resources ? this.resources : [],
        sitework: this.sitework ? this.sitework : []
      },
      width: '1200px',
      disableClose: true,
      maxHeight: '96vh',
    }).afterClosed().subscribe((val: DailyReport) => {
      if (val) {
        this.dailyreportService.create(val).then(daily => {
          console.log(daily);
          this.toastService.success('Success create report');
        }).catch(err => {
          this.toastService.error('Failed create report');
          console.log(err);
        });
      }
    });
  }

  updateDailyreport(model: TableModel) {
    const report = this.findReport(model.id);
    this.dialog.open(DailyreportFormComponent, {
      data: {
        data: report,
        tasks: this.tasks ? this.tasks : [],
        resources: this.resources ? this.resources : [],
        sitework: this.sitework ? this.sitework : []
      },
      width: '1200px',
      disableClose: true,
      maxHeight: '96vh',
    }).afterClosed().subscribe((val: DailyReport) => {
      if (val) {
        console.log(val);
        this.dailyreportService.update(val).then(daily => {
          console.log(daily);
          this.toastService.success('Success update report', 'success');
        }).catch(err => {
          this.toastService.error('Failed update report', 'failed');
          console.log(err);
        });
      }
    });
  }

  submitDailyReport(model: TableModel) {
    const report = this.findReport(model.id);
    this.dialog.open(DialogComponent, {
      data: { id: model.id, message: 'Are you sure to submit?' },
      width: '500px',
      disableClose: true,
    }).afterClosed().subscribe(val => {
      if (val) {
        this.dailyreportService.update(report)
        .then(() => {
          this.toastService.success('Success submit report', 'success');
        }).catch(() => {
          this.toastService.error('Failed submit report', 'failed');
        });
      }
    });
  }

  findReport(id: number) {
    return this.reports.find(report => {
      return report.getId === id;
    });
  }

}
