import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DocumentLifecycleComponent } from './document-lifecycle.component';

describe('DocumentLifecycleComponent', () => {
  let component: DocumentLifecycleComponent;
  let fixture: ComponentFixture<DocumentLifecycleComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DocumentLifecycleComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DocumentLifecycleComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
