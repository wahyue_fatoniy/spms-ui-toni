import { Component, OnInit } from '@angular/core';
import { ColumnType, ColumnConfig, actionType, OnEvent } from 'src/app/shared/table-builder';
import { MatDialog } from '@angular/material';
import { FormDocumentComponent } from '../../presentational/form-document/form-document.component';
import { Document, DocumentLifecycle } from 'src/app/models/document.model';
import { DocumentationService } from 'src/service/documentation-service/documentation.service';
import { ToastrService } from 'ngx-toastr';
import { CustomerService } from 'src/service/customer-service/customer.service';
import { combineLatest } from 'rxjs';
import { DocumentService } from 'src/service/document-service/document.service';
import { ActivatedRoute, Router } from '@angular/router';
import { FormStatusUpdateComponent } from '../../presentational/form-status-update/form-status-update.component';
import { DocumentHistoryService } from 'src/service/document-history-service/document-history.service';

interface TableModel {
  id: number;
  number: string;
  status: string;
  author: string;
  nik: string;
  reviewer: string;
}

@Component({
  selector: 'app-document',
  template: `
    <app-document-table
      [data]="data"
      [title]="title"
      [actionTypes]="actionTypes"
      (onEvent)="onEvent($event)"
      [columnConfigs]="columnConfigs"
    >
    </app-document-table>
  `,
  styleUrls: ['./document.component.css']
})
export class DocumentComponent implements OnInit {
  columnConfigs: ColumnConfig[];
  actionTypes: actionType[];
  title: string;
  data: TableModel[];

  projectId: number;
  documents: Document[];

  constructor(
    public dialog: MatDialog,
    public documentService: DocumentService,
    public toastService: ToastrService,
    public documentationService: DocumentationService,
    public customerService: CustomerService,
    public activeRoute: ActivatedRoute,
    public docHistoryService: DocumentHistoryService,
    public router: Router
  ) { 
    this.data = [];
    this.title = 'Documents';
    this.actionTypes = ['ADD', 'UPDATE'];
    this.columnConfigs = [
      { label: 'No', key: 'no', columnType: ColumnType.string },
      { label: 'Number', key: 'number', columnType: ColumnType.string },
      { label: 'Status', key: 'status', columnType: ColumnType.string },
      { label: 'Author', key: 'author', columnType: ColumnType.string },
      { label: 'NIK', key: 'nik', columnType: ColumnType.string },
      { label: 'Reviewer', key: 'reviewer', columnType: ColumnType.string },
    ];
  }

  ngOnInit() {
    this.getProjectId();
    this.runSubscriptionService()
    this.runService();
  }

  getProjectId() {
    this.activeRoute.params.subscribe(data => {
      if (data) {
        this.projectId = parseInt(data.id);
      }
    });
  }

  runService() {
    combineLatest(
      this.customerService.getData(),
      this.documentationService.getData(),
      this.documentService.findByPid(this.projectId)
    ).toPromise()
    .then();
  }

  runSubscriptionService() {
    this.documentService.subscriptionData().subscribe(val => {
      this.documents = val;
      this.data = val.map(document => {
        return this.setTableModel(document);
      });
    });
  }

  setTableModel(document: Document) {
    return <TableModel> {
      id: document.getId,
      status: document.getStatus,
      number: document.getNumber,
      author: document.getAuthor,
      nik: document.getNIP,
      reviewer: document.getReviewer
    }
  }

  onEvent(event: OnEvent) {
    switch (event.type) {
      case 'ADD':
        this.addDocument(); 
        break;
      case 'UPDATE': 
        this.updateDocument(event.value);
      break;
      case 'VIEW':
        this.viewHistoryDoc(event.value);
        break;
    }
  }

  addDocument() {
    this.dialog.open(FormDocumentComponent, {
      data: { 
        data: new Document(), 
        customers: this.customerService.subscriptionData(), 
        documentations: this.documentationService.subscriptionData() },
      width: '900px',
      disableClose: true,
    }).afterClosed().subscribe((document: Document) => {
      if (document) {
        this.documentService.createByPid(this.projectId, document)
        .toPromise()
        .then(result => {
          console.log(result);
          this.toastService.success('Success create document', 'success');
        })
        .catch(err => {
          console.log(err);
          this.toastService.error('Failed create document', 'failed');
        });
          console.log(document);
      }
    });
  }

  updateDocument(model: TableModel) {
    const doc = this.findDocument(model.id);
    this.dialog.open(FormStatusUpdateComponent, {
      data: doc,
      width: '900px',
      disableClose: true,
    }).afterClosed().subscribe((val: { document: Document, documentLifeCycle: DocumentLifecycle }) => {
      if (val) {
        const document = val.document;
        const docHistory = val.documentLifeCycle;
        this.documentService.updateById(document.getId, this.projectId, document)
        .toPromise().then(result => {
          this.docHistoryService.create(docHistory)
          .then(history => {
            console.log(history);
            this.toastService.success('Success update document', 'success');
          })
          .catch(err => {
            console.log(err);
            this.toastService.error('Failed update document', 'failed');
          })
        }).catch(err => {
          this.toastService.error('Failed update document', 'failed');
          console.log(err);
        });
      }
    });
  }

  viewHistoryDoc(model: TableModel) {
    this.router.navigate([`home/dashboard-pic/document-lifecycle/${model ? model.id : null}`])
    .catch(err => console.log(err));
  }

  findDocument(id: number) {
    return this.documents.find(doc => {
      return doc.getId === id;
    });
  }

}
