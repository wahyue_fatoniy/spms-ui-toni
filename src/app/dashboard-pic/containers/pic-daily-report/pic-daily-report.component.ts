import { Component, OnInit, ViewChild, OnDestroy } from "@angular/core";
import { TableConfig, CoreTable } from "../../../shared/core-table";
import { DailyReport } from "../../../shared/models/daily-report/daily-report";
import { DailyReportService } from "../../../shared/models/daily-report/daily-report.service";
import { SiteWorkService } from "../../../shared/models/site-work/site-work.service";
import { MatDialog, MatPaginator, MatSort, Sort } from "@angular/material";
import { DatePipe } from "@angular/common";
import { SiteWork } from "../../../shared/models/site-work/site-work";
import { TaskExecutionListComponent } from "./task-execution-list/task-execution-list.component";
import { WeeklyPlanningService } from "../../../shared/models/weekly-planning/weekly-planning.service";
import { combineLatest } from "rxjs";
import { DailyReportFormComponent } from "../../presentational/daily-report-form/daily-report-form.component";
import { DialogComponent } from "../../../shared/dialog/dialog.component";
import { AAAService } from "src/app/shared/aaa/aaa.service";
import { Document } from "src/app/models/document.model";
import { AdminDocumentFormComponent } from "src/app/dashboard-admin-center/containers/admin-document-table/admin-document-form/admin-document-form.component";
import { DocumentationService } from "src/service/documentation-service/documentation.service";
import { Documentation } from "src/app/models/documentation.model";
import { CustomerService } from "src/service/customer-service/customer.service";
import { takeUntil } from "rxjs/operators";
import { ActivatedRoute } from "@angular/router";

const CONFIG: TableConfig = {
  title: "Daily Report",
  columns: ["no", "date", "opex", "submit"],
  typeFilters: [
    { value: "all", label: "All" },
    { value: "site", label: "Site" },
    { value: "date", label: "Date" },
    { value: "opex", label: "Opex" }
  ]
};

@Component({
  selector: "app-pic-daily-report",
  templateUrl: "./pic-daily-report.component.html",
  styles: []
})
export class PicDailyReportComponent extends CoreTable<DailyReport>
  implements OnInit, OnDestroy {
  constructor(
    public report: DailyReportService,
    public siteWorkService: SiteWorkService,
    public planning: WeeklyPlanningService,
    public dialog: MatDialog,
    public aaa: AAAService,
    public documentService: DocumentationService,
    public customerService: CustomerService,
    public activeRoute: ActivatedRoute
  ) {
    super(CONFIG);
  }

  @ViewChild(MatPaginator)
  paginator: MatPaginator;
  @ViewChild(MatSort)
  sort: MatSort;
  date = new DatePipe("id");
  formatType = "dd MMMM yyyy";
  currentUserId: number = null;
  siteWork: SiteWork = new SiteWork();
  documentations: Documentation[] = [];

  getProjects(): SiteWork[] {
    return this.siteWorkService.getTables()
      ? this.siteWorkService.getTables()
      : [];
  }

  getLoading(): boolean {
    return this.report.getLoading();
  }

  getDate(row: DailyReport): string {
    return this.date.transform(row.getDate(), this.formatType);
  }

  getCordinator(row: DailyReport): string {
    const team = row.getSiteWork().getTeam();
    return team
      ? team
          .getCordinator()
          .getParty()
          .getName()
      : null;
  }

  getSite(row: DailyReport): string {
    const siteWork = row ? row.getSiteWork() : null;
    const site = siteWork ? siteWork.getSite() : null;
    return site ? site.getName() : null;
  }

  ngOnInit() {
    this.subscriptionData();
    this.initialize();
    this.getData();
  }

  ngOnDestroy() {
    this.destroy$.next(true);
    this.destroy$.complete();
  }

  getData() {
    this.activeRoute.params.pipe(takeUntil(this.destroy$)).subscribe(data => {
      if (data) {
        this.report
          .findBySiteWorkId(data.id)
          .pipe(takeUntil(this.destroy$))
          .subscribe(() => {
            this.datasource.data = this.report.getPriority();
            combineLatest(
              this.siteWorkService.findById(data.id),
              this.planning.findBySiteWorkId(data.id),
              this.documentService.getData(),
              this.customerService.getData()
            )
              .pipe(takeUntil(this.destroy$))
              .subscribe(val => {
                this.siteWork = val[0];
              });
          });
      }
    });
  }

  subscriptionData() {
    this.documentService
      .subscriptionData()
      .pipe(takeUntil(this.destroy$))
      .subscribe(doc => {
        this.documentations = doc;
      });
  }

  // initialize from table
  sortData(sort: Sort) {
    if (!sort.active || sort.direction === "") {
      this.datasource.data = this.report.getPriority();
      return;
    }
    this.datasource.data = this.datasource.data.sort((a, b) => {
      const isAsc = sort.direction === "asc";
      switch (sort.active) {
        case "site":
          return this.compare(this.getSite(a), this.getSite(b), isAsc);
        case "date":
          return this.compare(this.getDate(a), this.getDate(b), isAsc);
        default:
          return 0;
      }
    });
  }

  initialize() {
    this.action = true;
    this.enabledAction();
    this.datasource.paginator = this.paginator;
    this.datasource.sort = this.sort;
    this.datasource.filterPredicate = (value: DailyReport, keyword: string) => {
      const set = {
        cordinator: this.getCordinator(value),
        site: this.getSite(value),
        date: this.getDate(value)
      };
      return this.filterPredicate(set, keyword, this.typeFilter);
    };
    this.search.valueChanges
      .pipe(takeUntil(this.destroy$))
      .subscribe(_search => (this.datasource.filter = _search));
  }

  getAutocomplete(): string[] {
    // tslint:disable-next-line:prefer-const
    let data = { all: [], site: [], date: [] };
    this.report.getTables().forEach(set => {
      data.site.push(this.getSite(set));
      data.date.push(this.getDate(set));
    });
    return this.exist(data[this.typeFilter]).filter(_set => {
      const keyword = this.text.transform(this.search.value);
      const result = this.text.transform(_set);
      return keyword ? result.indexOf(keyword) !== -1 : true;
    });
  }

  onCreate() {
    const value = new DailyReport();
    value.setSiteWork(this.siteWork ? this.siteWork : new SiteWork());
    this.dialog
      .open(DailyReportFormComponent, {
        width: "1800px",
        data: {
          value: value,
          planning: this.planning,
          siteWork: this.siteWorkService
        },
        disableClose: true
      })
      .afterClosed()
      .pipe(takeUntil(this.destroy$))
      .subscribe(_report => {
        if (_report) {
          console.log("create report", _report);
          this.report
            .create(_report)
            .pipe(takeUntil(this.destroy$))
            .subscribe(
              _success => (this.datasource.data = this.report.getPriority())
            );
        }
      });
  }

  onUpdate(row: DailyReport) {
    this.dialog
      .open(DailyReportFormComponent, {
        width: "1800px",
        data: {
          value: new DailyReport(row),
          planning: this.planning,
          siteWork: this.siteWorkService
        },
        disableClose: true
      })
      .afterClosed()
      .pipe(takeUntil(this.destroy$))
      .subscribe(_report => {
        if (_report) {
          console.log("update", _report);
          this.report
            .update(row.getId(), _report)
            .pipe(takeUntil(this.destroy$))
            .subscribe(
              _success => (this.datasource.data = this.report.getPriority())
            );
        }
      });
  }

  onSubmit(row: DailyReport) {
    this.dialog
      .open(DialogComponent, {
        data: {
          id: row.getId(),
          message: `Are you sure to submit daily report on ${this.getDate(
            row
          )} ?`
        },
        disableClose: true
      })
      .afterClosed()
      .pipe(takeUntil(this.destroy$))
      .subscribe(_id => {
        if (_id) {
          this.report.submit(_id)
          .pipe(takeUntil(this.destroy$))
          .subscribe((dailyReport: DailyReport) => {
            console.log(dailyReport);
            this.datasource.data = this.report.getPriority();
            this.planning
              .findBySiteWorkId(this.siteWork.getId())
              .pipe(takeUntil(this.destroy$))
              .subscribe();
          });
        }
      });
  }

  invalid(): boolean {
    let invalid = false;
    if (this.datasource.data.find(_val => _val.getSubmit() === false)) {
      invalid = true;
    }
    return invalid;
  }

  onShowTaskExecution(row: DailyReport) {
    this.dialog.open(TaskExecutionListComponent, {
      width: "1800px",
      data: { value: new DailyReport(row) }
    });
  }

  onCreateDocument() {
    this.dialog
      .open(AdminDocumentFormComponent, {
        data: { data: new Document(), author: this.documentations },
        width: "800px",
        disableClose: true
      })
      .afterClosed()
      .pipe(takeUntil(this.destroy$))
      .subscribe(doc => {
        console.log(doc);
      });
  }
}
