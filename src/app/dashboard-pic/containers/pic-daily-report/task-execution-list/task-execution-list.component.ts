import { Component, OnInit, Inject } from '@angular/core';
import { DailyReport } from '../../../../shared/models/daily-report/daily-report';
import { MAT_DIALOG_DATA } from '@angular/material';
import { TaskExecution } from '../../../../shared/models/task-execution/task-execution';
import { Resource } from '../../../../shared/models/resource/resource';

interface TaskExecutionList {
  value: DailyReport;
}

@Component({
  selector: 'app-task-execution-list',
  templateUrl: './task-execution-list.component.html',
  styles: [`
    .mat-column-employee {
      width: 170px;
    }
  `]
})
export class TaskExecutionListComponent implements OnInit {
  taskExecution: {taskId: number, resources: string[] }[] = [];

  constructor(@Inject(MAT_DIALOG_DATA) public data: TaskExecutionList) {
    this.setManHour();
  }
  displayedColumns: string[] = ['no', 'activity', 'manHour', 'status', 'resource'];
  format = 'dd MMMM yyyy';

  getTaskExecutions(): TaskExecution[] {
    return this.data.value.getTaskExecutions().filter((_val, i, arr) => {
        return i === arr.findIndex(_arr => _arr.getTask().getId() === _val.getTask().getId());
    });
  }

  ngOnInit() {
  }

  setManHour() {
    let resources: string[];
    let manHour: number;
    this.getTaskExecutions().forEach((_val, i) => {
      resources = this.data.value.getTaskExecutions()
      .filter(_task => _val.getTask().getId() === _task.getTask().getId() )
      .map(_resource => _resource.getResource().getParty().getName());
       manHour = (resources.length * _val.getManHour());
      _val.setManHour(parseFloat(manHour.toFixed(2)));
      this.taskExecution[i] = { taskId: _val.getTask().getId(), resources: resources };
    });
  }

}
