import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PicDocumentComponent } from './pic-document.component';

describe('PicDocumentComponent', () => {
  let component: PicDocumentComponent;
  let fixture: ComponentFixture<PicDocumentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PicDocumentComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PicDocumentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
