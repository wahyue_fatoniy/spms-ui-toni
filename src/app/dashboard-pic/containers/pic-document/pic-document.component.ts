import { Component, OnInit, OnDestroy } from "@angular/core";
import { Document, DocumentLifecycle } from "src/app/models/document.model";
import { SiteWorkService } from "src/service/siteWork-service/site-work.service";
import { Subject, combineLatest } from "rxjs";
import { takeUntil } from "rxjs/operators";
import { ActivatedRoute } from "@angular/router";
import { MatDialog } from "@angular/material";
import { FormDocumentComponent } from "src/app/components/form-list/table-document/form-document/form-document.component";
import { DocumentationService } from "src/service/documentation-service/documentation.service";
import { Documentation } from "src/app/models/documentation.model";
import { CustomerService } from "src/service/customer-service/customer.service";
import { SiteWork } from "src/app/models/siteWork.model";
import { DocumentService } from "src/service/document-service/document.service";

@Component({
  selector: "app-pic-document",
  templateUrl: "./pic-document.component.html",
  styleUrls: ["./pic-document.component.css"]
})
export class PicDocumentComponent implements OnInit, OnDestroy {
  tableData: Document[] = [];
  destroy$: Subject<boolean> = new Subject();
  pathNavigateLifeCycle = "/home/dashboard-pic/lifeCycleDocument";
  documentations: Documentation[] = [];
  siteworkId: number;
  sitework: SiteWork;

  constructor(
    private siteWorkService: SiteWorkService,
    private activeRoute: ActivatedRoute,
    private dialog: MatDialog,
    private documentationService: DocumentationService,
    private customerService: CustomerService,
    private documentService: DocumentService
  ) {}

  ngOnInit() {
    this.subscriptionData();
    this.getSiteworkId();
    this.getData();
  }

  ngOnDestroy() {
    this.destroy$.next(true);
    this.destroy$.complete();
  }

  get loading() {
    return this.siteWorkService.getLoading;
  }

  getSiteworkId() {
    this.activeRoute.params.pipe(takeUntil(this.destroy$)).subscribe(data => {
      if (data) {
        this.siteworkId = parseInt(data.id);
      }
    });
  }

  getData() {
    this.siteWorkService
      .findById(this.siteworkId)
      .pipe(takeUntil(this.destroy$))
      .subscribe(() => {
        combineLatest(
          this.documentationService.getData(),
          this.customerService.getData()
        )
          .pipe(takeUntil(this.destroy$))
          .subscribe();
      });
  }

  subscriptionData() {
    combineLatest(
      this.siteWorkService.subscriptionData(),
      this.documentationService.subscriptionData()
    )
      .pipe(takeUntil(this.destroy$))
      .subscribe((val: [SiteWork[], Documentation[]]) => {
        this.documentations = val[1];
        this.sitework = val[0].find(
          sitework => sitework.getId === this.siteworkId
        );
        this.tableData = this.sitework
          ? this.sitework.getDocuments.filter(doc => doc.getType !== "BAST")
          : [];
        console.log(this.sitework);
      });
  }

  onChangeStatus(doc: {
    document: Document;
    documentLifeCycle: DocumentLifecycle;
  }) {
    if (doc) {
      this.sitework.setDocument = this.sitework.getDocuments.map(document => {
        return document.getId === doc.document.getId ? doc.document : document;
      });
      combineLatest(
        this.siteWorkService.update(this.sitework),
        this.documentService.createHistoryDoc(doc.documentLifeCycle)
      )
        .pipe(takeUntil(this.destroy$))
        .subscribe(val => {
          console.log(val);
        });
    }
  }

  onCreate(event) {
    this.dialog
      .open(FormDocumentComponent, {
        data: { data: new Document(), author: this.documentations },
        width: "1800px",
        disableClose: true
      })
      .afterClosed()
      .pipe(takeUntil(this.destroy$))
      .subscribe(
        (doc: { document: Document; documentLifeCycle: DocumentLifecycle }) => {
          this.sitework.setDocument = [
            ...this.sitework.getDocuments,
            doc.document
          ];
          this.siteWorkService.update(this.sitework).then((sitework: SiteWork) => {
            if (sitework) {
              let newDocument = sitework.getDocuments[sitework.getDocuments.length - 1];
                doc.documentLifeCycle.setDocument = newDocument;
                this.documentService.createHistoryDoc(doc.documentLifeCycle)
                .toPromise().then(history => {
                  console.log('create history doc', history);
                });
              // console.log(val);
            }
          });
        }
      );
  }
}
