import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PicLifeCycleDocumentComponent } from './pic-life-cycle-document.component';

describe('PicLifeCycleDocumentComponent', () => {
  let component: PicLifeCycleDocumentComponent;
  let fixture: ComponentFixture<PicLifeCycleDocumentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PicLifeCycleDocumentComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PicLifeCycleDocumentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
