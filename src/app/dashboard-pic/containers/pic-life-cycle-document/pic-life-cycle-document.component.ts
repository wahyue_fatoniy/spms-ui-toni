import { Component, OnInit, OnDestroy } from '@angular/core';
import { DocumentLifecycle } from 'src/app/models/document.model';
import { ActivatedRoute } from '@angular/router';
import { Subject } from 'rxjs';
import { DocumentService } from 'src/service/document-service/document.service';
import { takeUntil } from 'rxjs/operators';
import { SortArrayPipe } from 'src/app/pipes/sortArray/sort-array.pipe';

@Component({
  selector: 'app-pic-life-cycle-document',
  templateUrl: './pic-life-cycle-document.component.html',
  styleUrls: ['./pic-life-cycle-document.component.css']
})
export class PicLifeCycleDocumentComponent implements OnInit, OnDestroy {
  public data: DocumentLifecycle[] = [];
  public destroy$: Subject<boolean> = new Subject();
  public sortArray: SortArrayPipe = new SortArrayPipe();

  constructor(
    private activeRoute: ActivatedRoute,
    private documentService: DocumentService
  ) { }

  ngOnInit() {
    this.activeRoute.params
    .pipe(takeUntil(this.destroy$))
    .subscribe((val: {id: number}) => {
      if (val) {
        this.documentService.getHistoryDoc(val.id).pipe(
          takeUntil(this.destroy$)
        )
        .subscribe((doc: DocumentLifecycle[]) => {
          this.data = this.sortArray.transform(doc, 'asc', 'id');
          console.log(this.data);
        });
      }
    });
  }

  ngOnDestroy() {
    this.destroy$.next(true);
    this.destroy$.complete();
  }

}
