import { Component, OnInit, ViewChild, OnDestroy } from "@angular/core";
import { TableConfig, CoreTable } from "../../../shared/core-table";
import { WeeklyPlanningService } from "../../../shared/models/weekly-planning/weekly-planning.service";
import { SiteWorkService } from "../../../shared/models/site-work/site-work.service";
import { WeeklyPlanning } from "../../../shared/models/weekly-planning/weekly-planning";
import { MatPaginator, MatSort, Sort, MatDialog } from "@angular/material";
import { DatePipe } from "@angular/common";
import { SiteWork } from "../../../shared/models/site-work/site-work";
import { LocalCurrencyPipe } from "../../../pipes/localCurrency/local-currency.pipe";
import { TaskListComponent } from "./task-list/task-list.component";
import { TeamListComponent } from "./team-list/team-list.component";
import { AAAService } from "src/app/shared/aaa/aaa.service";
import { ActivatedRoute } from "@angular/router";
import { takeUntil } from "rxjs/operators";

const CONFIG: TableConfig = {
  title: "Weekly Planning",
  columns: ["no", "startDate", "endDate", "status"],
  typeFilters: [
    { value: "all", label: "All" },
    { value: "startDate", label: "Start Date" },
    { value: "endDate", label: "End Date" },
    { value: "operational", label: "Operational" }
  ]
};

@Component({
  selector: "app-pic-planning",
  templateUrl: "./pic-planning.component.html",
  styles: [``]
})
export class PicPlanningComponent extends CoreTable<WeeklyPlanning>
  implements OnInit, OnDestroy {
  constructor(
    public planning: WeeklyPlanningService,
    public dialog: MatDialog,
    public aaa: AAAService,
    public activeRoute: ActivatedRoute,
    public siteWorkService: SiteWorkService
  ) {
    super(CONFIG);
  }

  @ViewChild(MatPaginator)
  paginator: MatPaginator;
  @ViewChild(MatSort)
  sort: MatSort;
  date = new DatePipe("id");
  formatType = "dd MMMM yyyy";
  currentUserId: number = null;
  siteWork: SiteWork;

  getLoading(): boolean {
    return (
      this.planning.getLoading()
    );
  }

  getStart(value: WeeklyPlanning): string {
    return this.date.transform(value.getStartDate(), this.formatType);
  }

  getEnd(value: WeeklyPlanning): string {
    return this.date.transform(value.getEndDate(), this.formatType);
  }

  getCordinator(value: WeeklyPlanning): string {
    const team = value.getSiteWork().getTeam();
    return team
      ? team
          .getCordinator()
          .getParty()
          .getName()
      : null;
  }

  getOperational(value: WeeklyPlanning): string {
    const format = new LocalCurrencyPipe();
    const temp = format.transform(value.getOperational());
    return temp;
  }

  ngOnInit() {
    this.initialize();
    this.getData();
  }

  ngOnDestroy() {
    this.destroy$.next(true);
    this.destroy$.complete();
  }

  getData() {
    this.activeRoute.params.pipe(takeUntil(this.destroy$)).subscribe(data => {
      if (data) {
        this.planning
          .findBySiteWorkId(data ? data.id : null)
          .pipe(takeUntil(this.destroy$))
          .subscribe(
            set => (this.datasource.data = this.planning.getPriority())
          );
        this.siteWorkService.findById(data.id)
        .pipe(takeUntil(this.destroy$))
        .subscribe(val => {
          this.siteWork = val;
        });
      }
    });
  }

  // initialize from table
  sortData(sort: Sort) {
    if (!sort.active || sort.direction === "") {
      this.datasource.data = this.planning.getPriority();
      return;
    }
    this.datasource.data = this.datasource.data.sort((a, b) => {
      const isAsc = sort.direction === "asc";
      switch (sort.active) {
        case "startDate":
          return this.compare(this.getStart(a), this.getEnd(b), isAsc);
        case "endDate":
          return this.compare(this.getEnd(a), this.getEnd(b), isAsc);
        default:
          return 0;
      }
    });
  }

  initialize() {
    this.action = true;
    this.enabledAction();
    this.datasource.paginator = this.paginator;
    this.datasource.sort = this.sort;
    this.datasource.filterPredicate = (
      value: WeeklyPlanning,
      keyword: string
    ) => {
      const set = {
        startDate: this.getStart(value),
        endDate: this.getEnd(value),
      };
      return this.filterPredicate(set, keyword, this.typeFilter);
    };
    this.search.valueChanges
    .pipe(takeUntil(this.destroy$))
    .subscribe(
      _search => (this.datasource.filter = _search)
    );
  }

  getAutocomplete(): string[] {
    // tslint:disable-next-line:prefer-const
    let data = {
      all: [],
      startDate: [],
      endDate: [],
    };
    this.planning.getTables().forEach(set => {
      data.startDate.push(this.getStart(set));
      data.endDate.push(this.getEnd(set));
    });
    return this.exist(data[this.typeFilter]).filter(_set => {
      const keyword = this.text.transform(this.search.value);
      const result = this.text.transform(_set);
      return keyword ? result.indexOf(keyword) !== -1 : true;
    });
  }

  onShowTask(row: WeeklyPlanning) {
    this.dialog.open(TaskListComponent, {
      width: "1800px",
      data: { value: new WeeklyPlanning(row) }
    });
  }

  onShowTeam(row: WeeklyPlanning) {
   this.dialog.open(TeamListComponent, {
      width: "1000px",
      data: { value: new WeeklyPlanning(row) }
    });
  }
}
