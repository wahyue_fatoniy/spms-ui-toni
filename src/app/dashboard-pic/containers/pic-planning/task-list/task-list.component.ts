import { Component, OnInit, Inject } from '@angular/core';
import { MAT_DIALOG_DATA } from '@angular/material';
import { WeeklyPlanning } from '../../../../shared/models/weekly-planning/weekly-planning';
import { Task } from '../../../../shared/models/task/task';

interface TaskList {
  value: WeeklyPlanning;
}

@Component({
  selector: 'app-task-list',
  templateUrl: './task-list.component.html',
  styles: []
})
export class TaskListComponent implements OnInit {
  constructor(@Inject(MAT_DIALOG_DATA) public data: TaskList) {}
  displayedColumns: string[] = ['no', 'activity', 'status', 'severity', 'desc'];

  getTasks(): Task[] {
    return this.data.value.getTasks();
  }

  ngOnInit() {}
}
