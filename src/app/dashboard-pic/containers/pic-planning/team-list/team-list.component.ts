import { Component, OnInit, Input, Inject } from '@angular/core';
import { MAT_DIALOG_DATA } from '@angular/material';
import { WeeklyPlanning } from '../../../../shared/models/weekly-planning/weekly-planning';

interface TeamList {
  value: WeeklyPlanning;
}

@Component({
  selector: 'app-team-list',
  templateUrl: './team-list.component.html',
  styles: []
})
export class TeamListComponent implements OnInit {

  constructor(@Inject(MAT_DIALOG_DATA) public data: TeamList) { }
  displayedColumns: string[] = ['no', 'type', 'name', 'phone', 'email'];
  format = 'dd MMMM yyyy';

  ngOnInit() {
  }

  getTeams() {
    const team = this.data.value.getSiteWork().getTeam();
    return [
      team.getCordinator(), ...team.getResources()
    ];
  }

}
