import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PicProjectComponent } from './pic-project.component';

describe('PicProjectComponent', () => {
  let component: PicProjectComponent;
  let fixture: ComponentFixture<PicProjectComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PicProjectComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PicProjectComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
