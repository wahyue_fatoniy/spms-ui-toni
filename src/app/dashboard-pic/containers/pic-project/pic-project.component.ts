import { Component, OnInit, OnDestroy } from "@angular/core";
import { Options } from "src/app/components/table-view-component/table-view-component.component";
import { Subject } from "rxjs";
import { AAAService } from "src/app/shared/aaa/aaa.service";
import { Coordinator } from "src/app/models/coordinator.model";
import { CoordinatorService } from "src/service/coordinator-service/coordinator.service";
import { SiteWorkService } from "src/service/siteWork-service/site-work.service";
import { takeUntil } from "rxjs/operators";
import { DatePipe } from "@angular/common";
import { SiteWork } from "src/app/models/siteWork.model";
import { Router } from "@angular/router";

interface ViewData {
  projectCode: string;
  site: string;
  startDate: string;
  endDate: string;
}

@Component({
  selector: "app-pic-project",
  templateUrl: "./pic-project.component.html",
  styleUrls: ["./pic-project.component.css"]
})
export class PicProjectComponent implements OnInit, OnDestroy {
  public option: Options;
  public data: ViewData[] = [];
  private destroy$: Subject<boolean> = new Subject();
  private coordinator: Coordinator;
  private datePipe: DatePipe = new DatePipe("id");
  private siteWorks: SiteWork[];

  constructor(
    public aaa: AAAService,
    public picService: CoordinatorService,
    public siteWorkService: SiteWorkService,
    public router: Router
  ) {}

  ngOnInit() {
    this.option = {
      loading: true,
      config: {
        title: "Project",
        columns: [
          { title: "No.", columnDef: "no" },
          { title: "Project ID", columnDef: "projectCode" },
          { title: "Site", columnDef: "site" },
          { title: "Start Date", columnDef: "startDate" },
          { title: "End Date", columnDef: "endDate" }
        ],
        typeFilters: [
          { value: "projectCode", label: "Projec ID" },
          { value: "site", label: "Site" },
          { value: "startDate", label: "Start Date" },
          { value: "endDate", label: "End Date" }
        ],
        width: "100%"
      }
    };
    this.getData();
    this.subscriptionData();
  }

  ngOnDestroy() {
    this.destroy$.next(true);
    this.destroy$.complete();
  }

  getAuthCordinator() {
    this.coordinator = new Coordinator(
      this.aaa.getCurrentUser()
        ? this.aaa
            .getCurrentUser()
            .getUserMetadata()
            .roles.find(role => role["roleType"] === "CORDINATOR")
        : {}
    );
    // this.picService
    //   .subscriptionData()
    //   .subscribe((coordinators: Coordinator[]) => {
    //     let region: Region = new Region();
    //     let id: number;
    //     this.coordinator = coordinators.filter(_set => {
    //       region = _set.getRegion ? _set.getRegion : region;
    //       id = region.getId ? region.getId : null;
    //       return (
    //         this.aaa.isAuthorized(
    //           `read:project:{province:${id}}:{job:${_set.getId}}`
    //         ) || !this.aaa.isAuthorized("read:project:{province:$}:{job:$}")
    //       );
    //     })[0];
    //     console.log(this.coordinator);
    //   });
  }

  getData() {
    this.picService
      .getData()
      .pipe(takeUntil(this.destroy$))
      .subscribe(() => {
        this.getAuthCordinator();
        this.siteWorkService
          .findByPics([this.coordinator])
          .pipe(takeUntil(this.destroy$))
          .subscribe(() => {
            this.option = {
              loading: false,
              config: this.option.config
            };
          });
      });
  }

  subscriptionData() {
    this.siteWorkService
      .subscriptionData()
      .pipe(takeUntil(this.destroy$))
      .subscribe(siteworks => {
        this.siteWorks = siteworks;
        this.data = [];
        siteworks.forEach(sitework => {
          this.data = [
            ...this.data,
            {
              projectCode: sitework.getProjectCode,
              site: sitework.getSite.getName,
              startDate: sitework.getStartDate
                ? this.datePipe.transform(
                    sitework.getStartDate,
                    "dd MMMM yyyy",
                    "",
                    "id"
                  )
                : "None",
              endDate: sitework.getEndDate
                ? this.datePipe.transform(
                    sitework.getEndDate,
                    "dd MMMM yyyy",
                    "",
                    "id"
                  )
                : "None"
            }
          ];
        });
      });
  }

  onClickCell(event: ViewData) {
    if (event) {
      const siteWorkId = this.siteWorks.find(
        val => val.getProjectCode === event.projectCode
      )
        ? this.siteWorks.find(val => val.getProjectCode === event.projectCode)
            .getId
        : null;
      this.router.navigate([`home/dashboard-pic/task/${siteWorkId}`]).then();
    }
  }
}
