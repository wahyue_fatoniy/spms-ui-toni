import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FormDescSeverityComponent } from './form-desc-severity.component';

describe('FormDescSeverityComponent', () => {
  let component: FormDescSeverityComponent;
  let fixture: ComponentFixture<FormDescSeverityComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FormDescSeverityComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FormDescSeverityComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
