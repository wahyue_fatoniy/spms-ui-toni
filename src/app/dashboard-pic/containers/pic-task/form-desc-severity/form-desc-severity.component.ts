import { Component, OnInit, Inject } from '@angular/core';
import { MAT_DIALOG_DATA } from '@angular/material';
import { Task } from 'src/app/shared/models/task/task';

@Component({
  selector: 'app-form-desc-severity',
  templateUrl: './form-desc-severity.component.html',
  styleUrls: ['./form-desc-severity.component.css']
})
export class FormDescSeverityComponent implements OnInit {

  constructor(
    @Inject(MAT_DIALOG_DATA) public data: Task
  ) { }

  ngOnInit() {
  }

}
