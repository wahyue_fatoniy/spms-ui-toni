import { Component, OnInit, ViewChild, OnDestroy } from "@angular/core";
import { TableConfig, CoreTable } from "../../../shared/core-table";
import { SiteWorkService } from "../../../shared/models/site-work/site-work.service";
import { TaskService } from "../../../shared/models/task/task.service";
import { Task } from "../../../shared/models/task/task";
import { MatDialog, MatPaginator, MatSort, Sort } from "@angular/material";
import { FormControl } from "@angular/forms";
import { SiteWork } from "../../../shared/models/site-work/site-work";
import { TaskType, ActivityType } from "../../../shared/models/activity/activity";
import { ReportAlarmComponent } from "./report-alarm/report-alarm.component";
import { AAAService } from "src/app/shared/aaa/aaa.service";
import { ActivatedRoute } from "@angular/router";
import { takeUntil } from "rxjs/operators";
import { FormDescSeverityComponent } from "./form-desc-severity/form-desc-severity.component";

const CONFIG: TableConfig = {
  title: "Task",
  columns: ["no", "activity", "status", "severity", 'desc'],
  typeFilters: [
    { value: "all", label: "All" },
    { value: "activity", label: "Activity" },
    { value: "status", label: "Status" },
    { value: "severity", label: "Severity" },
  ]
};

@Component({
  selector: "app-pic-task",
  templateUrl: "./pic-task.component.html",
  styles: []
})
export class PicTaskComponent extends CoreTable<Task> implements OnInit, OnDestroy {
  constructor(
    public task: TaskService,
    private dialog: MatDialog,
    public aaa: AAAService,
    public activeRoute: ActivatedRoute,
    public siteWorkService: SiteWorkService
  ) {
    super(CONFIG);
  }
  @ViewChild(MatPaginator)
  paginator: MatPaginator;
  @ViewChild(MatSort)
  sort: MatSort;
  siteWorkModel: FormControl = new FormControl();
  siteWork: SiteWork;
  getSow(value: Task): string {
    return value
      .getSow()
      .getSowType()
      .getSowCode();
  }

  getTaskType(value: Task): TaskType {
    return value.getActivity().getType();
  }

  getActivity(value: Task): ActivityType {
    return value.getActivity().getName();
  }

  getLoading(): boolean {
    return (
      this.task.getLoading()
    );
  }

  ngOnInit() {
    this.initialize();
    this.getData();
  }

  ngOnDestroy() {
    this.destroy$.next(true);
    this.destroy$.complete();
  }

  getData() {
    this.activeRoute.params.pipe(takeUntil(this.destroy$)).subscribe(data => {
      if (data) {
        this.task
          .findBySiteWorkId(data ? data.id : null)
          .pipe(takeUntil(this.destroy$))
          .subscribe(set => {
            this.datasource.data = this.task.getPriority();
          });
          this.siteWorkService.findById(data.id)
          .pipe(takeUntil(this.destroy$))
          .subscribe(val => {
            this.siteWork = val;
          });
      }
    });
  }

  initialize() {
    this.enabledAction();
    this.datasource.paginator = this.paginator;
    this.datasource.sort = this.sort;
    this.datasource.filterPredicate = (value: Task, keyword: string) => {
      const set = {
        taskType: this.getTaskType(value),
        activity: this.getActivity(value),
        status: value.getStatus(),
        severity: value.getSeverity(),
        note: value.getNote()
      };
      return this.filterPredicate(set, keyword, this.typeFilter);
    };
    this.search.valueChanges
      .pipe(takeUntil(this.destroy$))
      .subscribe(_search => (this.datasource.filter = _search));
  }

  // initialize from table
  sortData(sort: Sort) {
    if (!sort.active || sort.direction === "") {
      this.datasource.data = this.task.getPriority();
      return;
    }
    this.datasource.data = this.datasource.data.sort((a, b) => {
      const isAsc = sort.direction === "asc";
      switch (sort.active) {
        case "activity":
          return this.compare(this.getActivity(a), this.getActivity(b), isAsc);
        case "status":
          return this.compare(a.getStatus(), b.getStatus(), isAsc);
        case "severity":
          return this.compare(a.getSeverity(), b.getSeverity(), isAsc);
        case "note":
          return this.compare(a.getNote(), b.getNote(), isAsc);
        default:
          return 0;
      }
    });
  }

  getAutocomplete(): string[] {
    // tslint:disable-next-line:prefer-const
    let data = {
      all: [],
      activity: [],
      status: [],
      severity: [],
      note: []
    };
    this.task.getTables().forEach(set => {
      data.activity = [this.getActivity(set), ...data.activity];
      data.status = [set.getStatus(), ...data.status];
      data.severity = [set.getSeverity(), ...data.severity];
      data.note = [set.getNote(), ...data.note];
    });
    return this.exist(data[this.typeFilter]).filter(_set => {
      const keyword = this.text.transform(this.search.value);
      const result = this.text.transform(_set);
      return keyword ? result.indexOf(keyword) !== -1 : true;
    });
  }

  onSetAlarm(row: Task) {
    const dialog = this.dialog.open(ReportAlarmComponent, {
      width: "900px",
      data: { value: new Task(row) }
    });
    dialog.afterClosed().subscribe((_result: Task) => {
      if (_result) {
        this.task
          .update(_result.getId(), _result)
          .subscribe(
            _success => (this.datasource.data = this.task.getPriority())
          );
      }
    });
  }

  showDetailSeverity(event: Task) {
    this.dialog.open(FormDescSeverityComponent, {
      data: event,
      width: '1200px'
    });
  }
}
