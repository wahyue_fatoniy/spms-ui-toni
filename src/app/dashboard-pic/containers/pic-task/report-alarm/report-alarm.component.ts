import { Component, OnInit, Inject, OnDestroy } from '@angular/core';
import { Task, SeverityType } from '../../../../shared/models/task/task';
import { MAT_DIALOG_DATA } from '@angular/material';
import { FormControl, Validators } from '@angular/forms';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';

interface ReportAlarm {
  value: Task;
}

@Component({
  selector: 'app-report-alarm',
  templateUrl: './report-alarm.component.html',
  styles: []
})
export class ReportAlarmComponent implements OnInit, OnDestroy {
  constructor(@Inject(MAT_DIALOG_DATA) public data: ReportAlarm) {}
  destroy$ = new Subject();
  note: FormControl = new FormControl(this.data.value.getNote());
  severity: FormControl = new FormControl(this.data.value.getSeverity(), [
    Validators.required
  ]);
  severities: SeverityType[] = [
    SeverityType.Normal,
    SeverityType.MajorI,
    SeverityType.MajorE,
    SeverityType.CriticalI,
    SeverityType.CriticalE
  ];

  ngOnInit() {
    this.note.valueChanges
    .pipe(takeUntil(this.destroy$))
    .subscribe(_note => this.data.value.setNote(_note));
    this.severity.valueChanges
    .pipe(takeUntil(this.destroy$))
    .subscribe(_severity => {
      if (
        _severity === SeverityType.Normal
      ) {
        this.note.setValue(null);
      }
      this.data.value.setSeverity(_severity);
    });
  }

  getTask(): string {
    return (
      this.data.value.getActivity().getName()
    );
  }

  invalid(): boolean {
    return this.severity.invalid || this.note.invalid;
  }

  ngOnDestroy() {
    this.destroy$.next(true);
    this.destroy$.complete();
  }
}
