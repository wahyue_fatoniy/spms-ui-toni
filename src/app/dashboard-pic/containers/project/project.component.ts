import { Component, OnInit, OnDestroy } from '@angular/core';
import { ColumnConfig, actionType, OnEvent, ColumnType } from 'src/app/shared/table-builder';
import { SiteWork } from 'src/app/models/siteWork.model';
import { Router } from '@angular/router';
import { Coordinator } from 'src/app/models/coordinator.model';
import { CoordinatorService } from 'src/service/coordinator-service/coordinator.service';
import { AAAService } from 'src/app/shared/aaa/aaa.service';
import { SiteWorkService } from 'src/service/siteWork-service/site-work.service';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';

interface TableModel {
  id: number;
  projectId: string;
  site: string;
  projectManager: string;
  progress: number;
  value: number;
  startDate: Date,
  endDate: Date,
}

@Component({
  selector: 'app-project',
  template: `
  <app-project-table
    [data]="data"
    [title]="title"
    [actionTypes]="actionTypes"
    (onEvent)="onEvent($event)"
    [columnConfigs]="columnConfigs"
  >
  </app-project-table>`
})
export class ProjectComponent implements OnInit, OnDestroy {
  columnConfigs: ColumnConfig[];
  actionTypes: actionType[];
  title: string;
  data: any[];

  destroy$: Subject<boolean>

  constructor(
    public route: Router,
    public picService: CoordinatorService,
    public aaaService: AAAService,
    public siteworkService: SiteWorkService,
  ) {
    this.data = [{}];
    this.actionTypes = ['ADD', 'VIEW'];
    this.title = 'Project';
    this.columnConfigs = [
      { label: 'No', key: 'no', columnType: ColumnType.string },
      { label: 'Project Id', key: 'projectId', columnType: ColumnType.string },
      { label: 'Site', key: 'site', columnType: ColumnType.string },
      { label: 'Project Manager', key: 'projectManager', columnType: ColumnType.string },
      { label: 'Start Date', key: 'startDate', columnType: ColumnType.date, format: 'shortDate' },
      { label: 'End Date', key: 'endDate', columnType: ColumnType.date, format: 'shortDate' },
      { label: 'Value', key: 'value', columnType: ColumnType.currency, local: 'Rp. ' },
      { label: 'Progress', key: 'progress', columnType: ColumnType.progressBar },
    ];
    this.destroy$ = new Subject();
  }

  ngOnInit() {
    this.getSubscriptionData();
    this.getSubscriptionPic();
    this.getServicePic();
  }

  ngOnDestroy() {
    this.destroy$.next(true);
    this.destroy$.complete();
  }

  getAuthPic(coordiantors: Coordinator[]) {
    return coordiantors.filter(_set => {
      const region = _set.getRegion ? _set.getRegion : null;
      const id = region ? region.getId : null;
      return (
        !this.aaaService.isAuthorized("read:project:{province:$}:{job:$}") ||
        this.aaaService.isAuthorized(
          `read:project:{province:${id}}:{job:${_set.getId}}`
        ))
    });
  }

  getServicePic() {
    this.picService.getData()
    .pipe(takeUntil(this.destroy$))
    .subscribe(val => {});
  }

  getSubscriptionPic() {
    this.picService.subscriptionData().subscribe(val => {
      if (val.length !== 0) {
        this.siteworkService.findByPics(this.getAuthPic(val)).subscribe();
      }
    });
  }

  getSubscriptionData() {
    this.siteworkService.subscriptionData()
    .pipe(takeUntil(this.destroy$))
    .subscribe(val => {
      // this.data = val.map(project => {
      //   return this.setTableModel(project);
      // })
    });
  }

  setTableModel(sitework: SiteWork) {
    return <TableModel> {
      id: sitework.getId,
      projectId: sitework.getProjectCode,
      projectManager: sitework.getProjectManager ? sitework.getProjectManager.getParty ? sitework.getProjectManager.getParty['name'] : '' : '',
      site: sitework.getSite ? sitework.getSite.getName : '',
      progress: 0,
      value: sitework.getPoValue,
      endDate: sitework.getEndDate ? new Date(sitework.getEndDate) : null,
      startDate: sitework.getStartDate ? new Date(sitework.getStartDate) : null
    }
  }

  onEvent(event: OnEvent) {
    this.route.navigate([`/home/dashboard-pic/daily-planning/${event.value ? event.value['id'] : null}`])
      .catch(err => console.error(err));
  }
}
