import { Component, OnInit, OnDestroy } from "@angular/core";
import { Router, ActivatedRoute, NavigationEnd } from "@angular/router";
import { Subscription } from "rxjs";
import { filter } from "rxjs/operators";
import { Location } from "@angular/common";
import { NumberInStringPipe } from "../pipes/numberInString/number-in-string.pipe";

@Component({
  selector: "app-dashboard-pic",
  template: `
    <nav mat-tab-nav-bar style="margin-top:-75px;">
      <a
        mat-tab-link
        style="text-decoration: none"
        *ngFor="let link of tabsMenu"
        [routerLink]="link.path"
        routerLinkActive
        #rla="routerLinkActive"
        [active]="rla.isActive"
      >
        <img [src]="link.img" style="margin-bottom:7px;" />&nbsp;
        {{ link.title }}
      </a>
    </nav>
    <div class="container-home">
      <router-outlet></router-outlet>
    </div>
  `,
  styles: []
})
export class DashboardPicComponent implements OnInit, OnDestroy {
  public pathLocation: string;
  public subscriptions: Subscription;
  public getNumber: NumberInStringPipe = new NumberInStringPipe();
  constructor(
    public location: Location,
    public router: Router,
    public activatedRoute: ActivatedRoute
  ) {}

  public tabsMenu = [
    {
      path: "project",
      title: "Project",
      img: "/assets/img/collection/project1.png"
    }
  ];

  ngOnInit() {
    let paramId = null;
    this.subscriptions = this.router.events
      .pipe(filter(event => event instanceof NavigationEnd))
      .subscribe(({ urlAfterRedirects }: NavigationEnd) => {
        paramId = this.getNumber.transform(urlAfterRedirects);
        if (urlAfterRedirects === "/home/dashboard-pic/project") {
          this.tabsMenu = this.tabsMenu.filter(
            val => val.path === "project"
          );
        } else if (
          urlAfterRedirects !== `/home/dashboard-pic/document-lifecycle/${paramId}`
        ) {
          this.tabsMenu = [
            {
              path: "project",
              title: "Project",
              img: "/assets/img/collection/project1.png"
            },
            {
              path: `daily-planning/${paramId}`,
              title: "Daily Planning",
              img: "/assets/img/collection/calendar.png"
            },
            {
              path: `daily-report/${paramId}`,
              title: "Daily Report",
              img: "/assets/img/collection/report.png"
            },
            {
              path: `document/${paramId}`,
              title: "Document",
              img: "/assets/img/collection/form.png"
            },
          ];
        }
        else if (
          urlAfterRedirects ===
          `/home/dashboard-pic/document-lifecycle/${paramId}`
        ) {
          this.tabsMenu = [
            ...this.tabsMenu,
            {
              path: `document-lifecycle/${paramId}`,
              title: "Document History",
              img: "/assets/img/collection/NOT_STARTED.png"
            }
          ];
        }
      });
    if (this.location.path() !== "/home/dashboard-pic/project") {
      this.router
        .navigate(["/home/dashboard-pic/project"])
        .catch(err => console.log(err));
    }
  }

  ngOnDestroy() {
    if (this.subscriptions) {
      this.subscriptions.unsubscribe();
    }
  }
}
