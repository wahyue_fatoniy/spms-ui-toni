import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { DashboardPicComponent } from './dashboard-pic.component';
import { SharedModule } from '../shared/shared.module';
import { ProjectComponent } from './containers/project/project.component';
import { DailyPlanComponent } from './containers/daily-plan/daily-plan.component';
import { DailyReportComponent } from './containers/daily-report/daily-report.component';
import { DocumentComponent } from './containers/document/document.component';
import { ProjectTableComponent } from './presentational/project-table/project-table.component';
import { DailyplanTableComponent } from './presentational/dailyplan-table/dailyplan-table.component';
import { DailyreportTableComponent } from './presentational/dailyreport-table/dailyreport-table.component';
import { DocumentTableComponent } from './presentational/document-table/document-table.component';
import { DocumentlifecycleTableComponent } from './presentational/documentlifecycle-table/documentlifecycle-table.component';
import { PmWeeklyPlanningFormComponent } from './presentational/pm-weekly-planning-form/pm-weekly-planning-form.component';
import { FormDocumentComponent } from './presentational/form-document/form-document.component';
// import { DailyReportFormComponent } from './presentational/daily-report-form/daily-report-form.component';
import { FormSelectTaskComponent } from './presentational/form-select-task/form-select-task.component';
import { DocumentLifecycleComponent } from './containers/document-lifecycle/document-lifecycle.component';
import { FormDocumentLifecycleComponent } from './presentational/form-document-lifecycle/form-document-lifecycle.component';
import { FormStatusUpdateComponent } from './presentational/form-status-update/form-status-update.component';
import { DailyreportFormComponent } from './presentational/dailyreport-form/dailyreport-form.component';
import { FormReportAlarmComponent } from './presentational/form-report-alarm/form-report-alarm.component';
import { AlarmFormComponent } from './presentational/alarm-form/alarm-form.component';

const routes: Routes = [
  {
    path: '', component: DashboardPicComponent, children: [
      { path: '', redirectTo: 'project', pathMatch: 'full' },
      { path: 'project', component: ProjectComponent },
      { path: 'daily-planning/:id', component: DailyPlanComponent },
      { path: 'daily-report/:id', component: DailyReportComponent },
      { path: 'document/:id', component: DocumentComponent },
      { path: 'document-lifecycle/:id', component: DocumentLifecycleComponent },
    ]
  }
];

@NgModule({
  imports: [
    CommonModule,
    SharedModule,
    RouterModule.forChild(routes),
  ],
  declarations: [
    DashboardPicComponent,
    ProjectComponent,
    DailyPlanComponent,
    DailyReportComponent,
    DocumentComponent,
    ProjectTableComponent,
    DailyplanTableComponent,
    DailyreportTableComponent,
    DocumentTableComponent,
    DocumentlifecycleTableComponent,
    DocumentLifecycleComponent,
    PmWeeklyPlanningFormComponent,
    FormDocumentComponent,
    // DailyReportFormComponent,
    FormSelectTaskComponent,
    FormDocumentLifecycleComponent,
    FormStatusUpdateComponent,
    DailyreportFormComponent,
    FormReportAlarmComponent,
    AlarmFormComponent
  ],
  entryComponents: [
    PmWeeklyPlanningFormComponent,
    FormDocumentComponent,
    // DailyReportFormComponent,
    FormSelectTaskComponent,
    FormDocumentLifecycleComponent,
    FormStatusUpdateComponent,
    DailyreportFormComponent,
    FormReportAlarmComponent

  ]
})
export class DashboardPicModule { }
