import { Component, OnInit, Inject, OnDestroy } from "@angular/core";
import { MAT_DIALOG_DATA, MatDialog } from "@angular/material";
import { FormControl, Validators } from "@angular/forms";
import { Resource } from "../../../shared/models/resource/resource";
import { DatePipe } from "@angular/common";
import { NumberInStringPipe } from "src/app/pipes/numberInString/number-in-string.pipe";
import { FormSelectTaskComponent } from "../form-select-task/form-select-task.component";
import { Subject } from "rxjs";
import { takeUntil } from "rxjs/operators";
import { StatusType } from "src/app/shared/models/site-work/site-work";
import { ReportAlarmComponent } from "../../containers/pic-task/report-alarm/report-alarm.component";
import { AAAService } from "src/app/shared/aaa/aaa.service";
import { Coordinator } from "src/app/models/coordinator.model";
import { PmDescTaskComponent } from "src/app/dashboard-pm/containers/pm-task-table/pm-desc-task/pm-desc-task.component";
import { DailyPlan } from "src/app/models/dailyPlan.model";
import { DailyReport } from "src/app/models/daliyReport.model";
import { TaskExecution } from "src/app/models/taskExecution.model";
import { TaskPlan, Task } from "src/app/models/task.model";

interface FormData {
  value: DailyReport;
  planning: DailyPlan;
}

interface TaskExecutionConfig {
  taskId: number;
  resourceIds: number[];
}

@Component({
  selector: "app-dail/`y-report-form",
  templateUrl: "./daily-report-form.component.html",
  styles: []
})

export class DailyReportFormComponent {
  // datePipe: DatePipe = new DatePipe("id");
  // sourceTaskExecutions: TaskExecution[];
  // availableTask: TaskPlan[];
  // sourceTask: TaskPlan[];
  // selectedTask: TaskPlan[];
  // substrChar: NumberInStringPipe = new NumberInStringPipe();
  // taskExecutionResources: TaskExecutionConfig[] = [];
  // destroy$: Subject<boolean> = new Subject();
  // coordinator: Coordinator;

  // constructor(
  //   @Inject(MAT_DIALOG_DATA)
  //   public formData: FormData,
  //   public dialog: MatDialog,
  //   private aaa: AAAService
  // ) {
  //   this.sourceTaskExecutions = formData.value.getTaskExecutions;
  // }

  // date: FormControl = new FormControl(
  //   this.formData.value.getDate,
  //   Validators.required
  // );
  
  // displayedColumns: string[] = [
  //   "no",
  //   "activity",
  //   "manHours",
  //   "status",
  //   "severity",
  //   "resource",
  //   "action"
  // ];
  // status: boolean[] = [];
  // manHour: number[] = [];
  // activities = {
  //   CREATE_BAUT: "Create Baut",
  //   SUBMIT_BAUT: "Submit Baut",
  //   MOS: "Mos",
  //   INSTALLATION: "Installation",
  //   INTEGRATION: "Integration",
  //   TAKE_DATA: "Take Data",
  //   ATP: "Atp",
  //   CREATE_ATP: "Create Atp",
  //   SUBMIT_ATP: "Submit Atp",
  //   CREATE_COMPLETENESS_ATP: "Create Completeness Atp",
  //   SUBMIT_COMPLETENESS_ATP: "Submit Completeness Atp"
  // };

  // ngOnInit() {
  //   this.initTaskExecution();
  //   this.initSelectedTask();
  //   this.initSourceTask();
 //   this.initAvailableTask();
  //   this.changeDate();
  // }

  // getCoordinator(): boolean {
  //   let retval = false;
  //   this.coordinator = new Coordinator(
  //     this.aaa.getCurrentUser()
  //       ? this.aaa
  //           .getCurrentUser()
  //           .getUserMetadata()
  //           .roles.find(role => role["roleType"] === "CORDINATOR")
  //       : {}
  //   );
  //   if (this.coordinator) {
  //     if (this.coordinator.getParty["partyType"] === "ORGANIZATION") {
  //       retval = true;
  //     }
  //   }
  //   return retval;
  // }

  // public initSelectedTask(): void {
  //   this.selectedTask = this.getSelectedTask().map(task => new TaskPlan(task));
  //   // this.getSelectedTask().forEach((val, i) => {
  //   //   this.changeStatus(i, val.getFinish);
  //   // });
  // }

  // public initSourceTask(): void {
  //   this.sourceTask = this.formData.planning.getTasks
  //     .map(task => new TaskPlan(task))
  //     .filter(task => {
  //       return (
  //         this.selectedTask.findIndex(
  //           select => select.getId === task.getId
  //         ) === -1
  //       );
  //     });
  // }

  // public initAvailableTask(): void {
  //   this.availableTask = this.selectedTask.filter(
  //     (_task: TaskPlan) => _task.getPlanStatus !== "DONE"
  //   );
  //   let taskReady = this.filterTaskReady(this.sourceTask, this.selectedTask);
  //   this.availableTask = [...taskReady.taskReady, ...this.availableTask];
  //   this.sourceTask = taskReady.sourceTask;
  //   console.log("available", this.availableTask);
  //   console.log("source", this.sourceTask);
  //   console.log("select", this.selectedTask);
  // }

  // initTaskExecution(): void {
  //   let manHour: number;
  //   let resourceIds: number[];
  //   this.sourceTaskExecutions.forEach((_val, i) => {
  //     resourceIds = this.sourceTaskExecutions
  //       .filter(resource => {
  //         return resource.getTask.getId === _val.getTask.getId;
  //       })
  //       .map(val => val.getResource['id']);

  //     this.setTaskExecutionResources(_val.getTask.getId, resourceIds);
  //     manHour = resourceIds.length * _val.getManHour;
  //     _val.setManHour = parseFloat(manHour.toFixed(2));
  //   });
  // }

  // ngOnDestroy() {
  //   let taskExecutions: TaskExecution[] = [];
  //   const siteWork = this.formData.value.getSiteWork;
  //   const resources = siteWork
  //     ? siteWork.getTeam.getResources
  //     : [];
  //   let taskExecution: TaskExecution;
  //   let resource: Resource;
  //   let manhour: number;
  //   this.taskExecutionResources.forEach(_config => {
  //     _config.resourceIds.forEach(id => {
  //       taskExecution = new TaskExecution(
  //         this.getTaskExecutions.find(
  //           _taskExecution =>
  //             _taskExecution.getTask.getId === _config.taskId
  //         )
  //       );
  //       resource = resources.find(_resource => _resource.getId() === id);
  //       manhour = taskExecution.getManHour() / _config.resourceIds.length;
  //       taskExecution.setResource(resource);
  //       taskExecution.setManHour(parseFloat(manhour.toFixed(2)));
  //       taskExecutions = [...taskExecutions, taskExecution];
  //     });
  //   });
    // taskExecutions = taskExecutions.map((_val, i) => {
    //   if (this.sourceTaskExecutions[i]) {
    //     _val.setId(this.sourceTaskExecutions[i].getId());
    //   } else {
    //     _val.setId(null);
    //   }
    //   return _val;
    // });
  //   this.formData.value.setOpex(this.substrChar.transform(this.opex));
  //   this.formData.value.setTaskExecutions(taskExecutions);
  // }

  // getSelectedTask(): Task[] {
  //   return this.sourceTaskExecutions.filter((_val, i, arr) => {
  //     return arr.findIndex(_arr => _arr.getTask.getId === _val.getTask.getId)
  //   }).map(taskExecution => {
  //     return taskExecution.getTask;
  //   });
  // }

  // getResources(): Resource[] {
  //   const siteWork = this.formData.value.getSiteWork();
  //   return siteWork.getTeam() ? siteWork.getTeam().getResources() : [];
  // }

  // setTaskExecution(tasks: TaskPlan[]): void {
  //   this.formData.value.setTaskExecutionsByTasks(tasks);
  // }

  // changeManHours(index: number, value: number): void {
  //   this.formData.value.changeManHours(index, value);
  // }

  // changeResources(taskId: number, ids: number[]): void {
  //   this.setTaskExecutionResources(taskId, ids);
  //   console.log(this.taskExecutionResources);
  // }

  // changeDate(): void {
  //   this.date.valueChanges.subscribe(_date => {
  //     if (_date) {
  //       _date = this.datePipe.transform(
  //         new Date(_date),
  //         "yyyy-MM-ddTHH:mm:ssZ",
  //         "+0000"
  //       );
  //       this.formData.value.setDate = _date;
  //     }
  //   });
  // }

  // invalid(): boolean {
  //   return (
  //     this.date.invalid || this.formData.value.getTaskExecutions.length === 0
  //   );
  // }

  // deleteTask(id: number): void {
  //   let task = new TaskPlan(
  //     this.formData.value
  //       .getTaskExecutions
  //       .find(_val => _val.getTask.getId === id)
  //       .getTask
  //   );
  //   task.setPlanStatus = task.getStatus;
  //   let taskRemove = this.filterSelectedRemove(task, this.selectedTask);
  //   let availableRemove = this.filterAvailableRemove(
  //     taskRemove,
  //     this.availableTask
  //   );
  //   this.onRemoveTask(task, taskRemove, availableRemove);
  // }

  // changeStatus(index: number, value: boolean): void {
  //   this.formData.value.changeFinish(index, value);
  //   let task = new TaskPlan(
  //     this.formData.value.getTaskExecutions[index].getTask
  //   );
  //   if (value === true) {
  //     task.setPlanStatus = StatusType.Done;
  //     this.selectedTask = filterSelected(this.selectedTask, task);
  //     this.onStatusDone(this.selectedTask);
  //   } else {
  //     task.setPlanStatus = task.getStatus;
  //     this.selectedTask = filterSelected(this.selectedTask, task);
  //     let tasksRemove = this.filterSelectedRemove(task, this.selectedTask);
  //     let availableRemove = this.filterAvailableRemove(
  //       tasksRemove,
  //       this.availableTask
  //     );
  //     tasksRemove = tasksRemove.filter(
  //       remove => remove.getId !== task.getId
  //     );
  //     this.onRemoveTask(task, tasksRemove, availableRemove);
  //   }
  //   function filterSelected(
  //     selectedPlan: TaskPlan[],
  //     taskPlan: TaskPlan
  //   ): TaskPlan[] {
  //     return selectedPlan.map((selected: TaskPlan) => {
  //       return selected.getId === taskPlan.getId ? taskPlan : selected;
  //     });
  //   }
  // }

  // public onRemoveTask(
  //   taskPlan: TaskPlan,
  //   tasksRemove: TaskPlan[],
  //   availableRemove: TaskPlan[]
  // ): void {
  //   console.log(tasksRemove, availableRemove);
  //   let taskNotDone = taskPlan;
  //   this.selectedTask = this.selectedTask.filter(selected => {
  //     return (
  //       tasksRemove.findIndex(remove => remove.getId() === selected.getId()) ===
  //       -1
  //     );
  //   });
  //   this.availableTask = this.availableTask.filter(available => {
  //     return (
  //       availableRemove.findIndex(
  //         remove => available.getId() === remove.getId()
  //       ) === -1
  //     );
  //   });
  //   this.setTaskExecution(this.selectedTask);
  //   this.sourceTask = [
  //     ...this.sourceTask,
  //     ...availableRemove,
  //     ...tasksRemove
  //   ].filter(task => {
  //     return !task.isReady();
  //   });
  //   this.availableTask = [...this.availableTask, taskNotDone].filter(
  //     (available, i, arr) => {
  //       return i === arr.findIndex(task => task.getId() === available.getId());
  //     }
  //   );
  //   this.onRemoveTaskResources(this.selectedTask);
  // }

  // public filterSelectedRemove(
  //   taskRemove: TaskPlan,
  //   selectedTask: TaskPlan[]
  // ): TaskPlan[] {
  //   let selectRemove: TaskPlan[] = [];
  //   selectedTask = selectedTask.filter((task: TaskPlan) => {
  //     if (task.isRequirement(taskRemove)) {
  //       selectRemove = [...selectRemove, new TaskPlan(task)];
  //     }
  //     selectRemove.forEach((select: TaskPlan) => {
  //       if (task.isRequirement(select)) {
  //         selectRemove = [...selectRemove, new TaskPlan(task)];
  //       }
  //     });
  //   });
  //   selectRemove = [...selectRemove, taskRemove].filter((select, i, arr) => {
  //     return i === arr.findIndex(val => val.getId() === select.getId());
  //   });
  //   selectRemove = selectRemove.map(remove => {
  //     remove.setPlanStatus(remove.getStatus());
  //     return remove;
  //   });
  //   return selectRemove;
  // }

  // filterAvailableRemove(
  //   selectRemove: TaskPlan[],
  //   availableTask: TaskPlan[]
  // ): TaskPlan[] {
  //   let availableRemove: TaskPlan[] = [];
  //   availableTask.forEach(available => {
  //     if (
  //       selectRemove.findIndex(select => available.isRequirement(select)) !== -1
  //     ) {
  //       availableRemove = [...availableRemove, available];
  //     }
  //   });
  //   return availableRemove;
  // }

  // public filterTaskReady(
  //   sourceTask: TaskPlan[],
  //   selectedTask: TaskPlan[]
  // ): { sourceTask: TaskPlan[]; taskReady: TaskPlan[] } {
  //   let i = 0;
  //   let taskIsReady: TaskPlan[] = [];
  //   sourceTask = sourceTask.filter((source: TaskPlan) => {
  //     i = 0;
  //     selectedTask.forEach((selected: TaskPlan) => {
  //       if (selected.getPlanStatus === StatusType.Done) {
  //         if (source.isRequirement(selected)) {
  //           i = i + 1;
  //         }
  //       }
  //     });
  //     if (
  //       i === source.getRequirements.length &&
  //       source.getRequirements.length !== 0
  //     ) {
  //       taskIsReady = [...taskIsReady, new TaskPlan(source)];
  //       return false;
  //     }
  //     if (source.isInitial() || source.isReady()) {
  //       taskIsReady = [...taskIsReady, new TaskPlan(source)];
  //       return false;
  //     }
  //     return true;
  //   });
  //   return { sourceTask: sourceTask, taskReady: taskIsReady };
  // }

  // onRemoveTaskResources(selectedTask: TaskPlan[]) {
  //   this.taskExecutionResources = this.taskExecutionResources.filter(task => {
  //     return selectedTask.findIndex(val => val.getId() === task.taskId) !== -1;
  //   });
  // }

  // onStatusDone(tasks: TaskPlan[]): void {
  //   this.availableTask = this.availableTask.filter(available => {
  //     return (
  //       tasks.findIndex(selected => {
  //         if (
  //           selected.getPlanStatus() === StatusType.Done &&
  //           selected.getId() === available.getId()
  //         ) {
  //           return true;
  //         }
  //         return false;
  //       }) === -1
  //     );
  //   });
  //   let filterTaskReady = this.filterTaskReady(this.sourceTask, tasks);

  //   this.availableTask = [
  //     ...this.availableTask,
  //     ...filterTaskReady.taskReady
  //   ].filter((task, i, arr) => {
  //     return i === arr.findIndex(val => val.getId() === task.getId());
  //   });
  //   this.sourceTask = filterTaskReady.sourceTask;
  // }

  // onSelectTask() {
  //   let checkedTask: TaskPlan[] = this.selectedTask
  //     .filter(task => {
  //       return task.getPlanStatus() !== StatusType.Done;
  //     })
  //     .map(val => new TaskPlan(val));
  //   this.dialog
  //     .open(FormSelectTaskComponent, {
  //       data: {
  //         sourceTask: this.availableTask,
  //         checkedTask: checkedTask
  //       },
  //       width: "95%",
  //       disableClose: true
  //     })
  //     .afterClosed()
  //     .pipe(takeUntil(this.destroy$))
  //     .subscribe((tasks: TaskPlan[]) => {
  //       if (tasks) {
  //         this.onCheckedTask(tasks, checkedTask);
  //         this.onAddSelectedTask(tasks);
  //         this.setTaskExecution(this.selectedTask);
  //         tasks.forEach((task: TaskPlan) => {
  //           if (
  //             !this.taskExecutionResources.find(
  //               config => config.taskId === task.getId()
  //             )
  //           ) {
  //             this.setTaskExecutionResources(task.getId(), []);
  //           }
  //         });
  //       }
  //     });
  // }

  // onCheckedTask(tasks: TaskPlan[], checkedTask: TaskPlan[]) {
  //   let checked = checkedTask.filter(check => {
  //     return (
  //       tasks.findIndex((val: TaskPlan) => val.getId() === check["id"]) === -1
  //     );
  //   });
  //   this.selectedTask = this.selectedTask.filter(task => {
  //     return checked.findIndex(val => val["id"] === task.getId()) === -1;
  //   });
  //   this.onRemoveTaskResources(this.selectedTask);
  // }

  // onAddSelectedTask(tasks: TaskPlan[]): void {
  //   this.selectedTask = [...this.selectedTask, ...tasks].filter(
  //     (task, i, arr) => i === arr.findIndex(val => val.getId() === task.getId())
  //   );
  // }

  // setSeverityTask(task: Task) {
  //   this.dialog
  //     .open(ReportAlarmComponent, {
  //       data: { value: new Task(task) },
  //       width: "1800px",
  //       disableClose: true
  //     })
  //     .afterClosed()
  //     .pipe(takeUntil(this.destroy$))
  //     .subscribe((val: TaskPlan) => {
  //       if (val) {
  //         this.selectedTask = this.selectedTask.map(select => {
  //           return val.getId() === select.getId() ? new TaskPlan(val) : select;
  //         });
  //         this.getTaskExecutions().map(execution => {
  //           if (execution.getTask().getId() === val.getId()) {
  //             execution.setTask(new TaskPlan(val));
  //           }
  //           return execution;
  //         });
  //       }
  //     });
  // }

  // showDescTask(task: TaskPlan) {
  //   this.dialog.open(PmDescTaskComponent, {
  //     data: task,
  //     width: "1000px"
  //   });
  // }

  // /**set variable for save resourcesId*/
  // setTaskExecutionResources(taskId: number, resourceIds: number[]): void {
  //   if (this.taskExecutionResources.length === 0) {
  //     this.taskExecutionResources[0] = {
  //       taskId: taskId,
  //       resourceIds: resourceIds
  //     };
  //   } else {
  //     if (
  //       this.taskExecutionResources.findIndex(_i => _i.taskId === taskId) !== -1
  //     ) {
  //       this.taskExecutionResources = this.taskExecutionResources.map(
  //         config => {
  //           if (config.taskId === taskId) {
  //             config.resourceIds = resourceIds;
  //           }
  //           return config;
  //         }
  //       );
  //     } else {
  //       this.taskExecutionResources = [
  //         ...this.taskExecutionResources,
  //         { taskId: taskId, resourceIds: resourceIds }
  //       ];
  //     }
  //   }
  // }
}
