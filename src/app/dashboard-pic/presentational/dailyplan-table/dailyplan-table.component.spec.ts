import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DailyplanTableComponent } from './dailyplan-table.component';

describe('DailyplanTableComponent', () => {
  let component: DailyplanTableComponent;
  let fixture: ComponentFixture<DailyplanTableComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DailyplanTableComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DailyplanTableComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
