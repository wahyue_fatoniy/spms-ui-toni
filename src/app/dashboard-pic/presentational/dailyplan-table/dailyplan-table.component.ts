import { Component, OnInit, ViewChild, EventEmitter, Output, Input } from '@angular/core';
import { MatPaginator, MatSort } from '@angular/material';
import { OnEvent, ColumnConfig, actionType, TableBuilder } from 'src/app/shared/table-builder';

@Component({
  selector: 'app-dailyplan-table',
  templateUrl: './dailyplan-table.component.html',
  styleUrls: ['./dailyplan-table.component.css']
})
export class DailyplanTableComponent extends TableBuilder implements OnInit {
  
  @ViewChild(MatPaginator) _paginator: MatPaginator;
  @ViewChild(MatSort) _sort: MatSort;

  @Output() onEvent: EventEmitter<OnEvent>;
  @Input() columnConfigs: ColumnConfig[];
  @Input() actionTypes: actionType[];
  @Input() title: string;
  @Input() data: any[];

  public selectField: boolean;
  public selectedData: any;
  public rowIndex: number;

  constructor() {
    super();
    this.onEvent = new EventEmitter();
  }

  ngOnInit() {
    this.paginator = this._paginator;
    this.sort = this._sort;
    this.setDisplayedColumns();
    this.setTypeFilters();
  }

  ngOnChanges() {
    this.datasource.data = this.data;
  }

  ngOnDestroy() {
    this.subscription.next(true);
    this.subscription.complete();
  }

  onSelectRow(element) {
    this.selectField = true;
    this.selectedData = element;
  }

  onEventClick(value: OnEvent) {
    value.value = this.selectedData;
    this.onEvent.emit(value);
  }

  onDoubleClick(element){    
    this.selectedData = element;
    this.onEvent.emit({value: element, type: 'UPDATE'});
    this.selectField = false;
    this.rowIndex = null;
  }
}
