import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DailyreportFormComponent } from './dailyreport-form.component';

describe('DailyreportFormComponent', () => {
  let component: DailyreportFormComponent;
  let fixture: ComponentFixture<DailyreportFormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DailyreportFormComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DailyreportFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
