import { Component, OnInit, Inject } from '@angular/core';
import { DailyReport } from 'src/app/models/daliyReport.model';
import { Resource } from 'src/app/models/resource.model';
import { SiteWork } from 'src/app/models/siteWork.model';
import { MAT_DIALOG_DATA, MatDialogRef, MatDialog } from '@angular/material';
import { Task, TaskPlan, TaskStatus } from 'src/app/models/task.model';
import { TaskExecution } from 'src/app/models/taskExecution.model';
import { FormReportAlarmComponent } from '../form-report-alarm/form-report-alarm.component';
import { FormControl } from '@angular/forms';

interface FormData {
  data: DailyReport;
  sitework: SiteWork;
  tasks: Task[];
  resources: Resource[];
}

interface ResourceConfig {
  taskId: number;
  resourceIds: number[];
}


@Component({
  selector: 'app-dailyreport-form',
  templateUrl: './dailyreport-form.component.html',
  styleUrls: ['./dailyreport-form.component.css']
})
export class DailyreportFormComponent implements OnInit {
  availableTasks: TaskPlan[];
  sourceTasks: TaskPlan[];
  selectedTasks: TaskPlan[];

  resources: Resource[];
  sitework: SiteWork;
  resourcesConfig: ResourceConfig[];
  dateControl: FormControl;
  noteControl: FormControl;

  columnsTaskAvailable: string[];
  columnsTaskSelected: string[];
  columnsAlarm: string[];

  constructor(
    @Inject(MAT_DIALOG_DATA) public formData: FormData,
    public dialogRef: MatDialogRef<any>,
    public dialog: MatDialog,
  ) {
    this.columnsTaskAvailable = ['ssow', 'description', 'activity'];
    this.columnsTaskSelected = ['ssow', 'description', 'activity', 'manhours', 'status', 'resources', 'remove'];
    this.columnsAlarm = ['severity', 'actTime', 'description'];
    this.resources = formData.resources;
    this.sitework = formData.sitework;
    this.resourcesConfig = [];
    this.dateControl = new FormControl(formData.data.getDate ? new Date(formData.data.getDate) : null);
    this.noteControl = new FormControl(formData.data.getNote);
  }

  ngOnInit() {
    this.initSelectedTasks();
    this.initAvailableAndSourceTask();
  }

  initSelectedTasks() {
    this.selectedTasks = this.filterTaskExecutionToTask(this.formData.data['taskExecutions'] ? this.formData.data['taskExecutions'] : []);
  }

  initAvailableAndSourceTask() {
    const temporarySourceTask = this.formData.tasks.map(task => new TaskPlan(task));
    const availableAndSourceTask = this.getAvailableAndSourceTask(temporarySourceTask, this.selectedTasks);
    this.availableTasks = availableAndSourceTask.availableTask.filter(task => {
      return this.selectedTasks.findIndex(selected => selected.getId === task.getId) === -1;
    });
    this.sourceTasks = availableAndSourceTask.sourceTask;
  }

  filterTaskExecutionToTask(taskExecutions: TaskExecution[]): TaskPlan[] {
    const taskPlans = taskExecutions.filter((task, i, arr) => {
      return i === arr.findIndex(val => val.getTask.getId === task.getTask.getId)
    })
      .map(taskExecution => {
        const taskPlan = new TaskPlan(taskExecution.getTask);
        taskExecutions.forEach(execution => {
          if (execution.getTask.getId === taskPlan.getId) {
            taskPlan.manhour += execution.getManHour ? execution.getManHour : 0;
            taskPlan.resources = [...taskPlan.resources, execution.getResource['id']]
          }
        })
        return taskPlan;
      });
    return taskPlans;
  }

  public getAvailableAndSourceTask(
    sourceTask: TaskPlan[],
    selectedTask: TaskPlan[]
  ): { sourceTask: TaskPlan[]; availableTask: TaskPlan[] } {
    let i = 0;
    let availableTask: TaskPlan[] = [];
    sourceTask = sourceTask.filter((source: TaskPlan) => {
      i = 0;
      selectedTask.forEach((selected: TaskPlan) => {
        if (selected.getPlanStatus === TaskStatus.Done) {
          if (source.isRequirement(selected)) {
            i = i + 1;
          }
        }
      });
      if (
        i === source.getRequirements.length &&
        source.getRequirements.length !== 0
      ) {
        availableTask = [...availableTask, new TaskPlan(source)];
        return false;
      }
      if (source.isInitial() || source.isReady()) {
        availableTask = [...availableTask, new TaskPlan(source)];
        return false;
      }
      return true;
    });
    return { sourceTask: sourceTask, availableTask: availableTask };
  }

  filterAvailableToRemove(
    selectRemove: TaskPlan[],
    availableTask: TaskPlan[]
  ): TaskPlan[] {
    let availableRemove: TaskPlan[] = [];
    availableTask.forEach(available => {
      if (
        selectRemove.findIndex(select => available.isRequirement(select)) !== -1
      ) {
        availableRemove = [...availableRemove, available];
      }
    });
    return availableRemove;
  }


  public filterSelectedRemove(
    taskRemove: TaskPlan,
    selectedTask: TaskPlan[]
  ): TaskPlan[] {
    let selectRemove: TaskPlan[] = [];
    selectedTask.forEach((task: TaskPlan) => {
      if (task.isRequirement(taskRemove)) {
        selectRemove = [...selectRemove, new TaskPlan(task)];
      }
      selectRemove.forEach((select: TaskPlan) => {
        if (task.isRequirement(select)) {
          selectRemove = [...selectRemove, new TaskPlan(task)];
        }
      });
    });
    selectRemove = [...selectRemove, taskRemove].filter((select, i, arr) => {
      return i === arr.findIndex(val => val.getId === select.getId);
    });
    selectRemove = selectRemove.map(remove => {
      remove.setPlanStatus = remove.getStatus;
      return remove;
    });
    return selectRemove;
  }


  public onRemoveTask(
    tasksRemove: TaskPlan[],
    availableRemove: TaskPlan[]
  ): void {
    this.selectedTasks = this.selectedTasks.filter(selected => {
      return (
        tasksRemove.findIndex(remove => remove.getId === selected.getId) ===
        -1
      );
    });
    this.availableTasks = this.availableTasks.filter(available => {
      return (
        availableRemove.findIndex(
          remove => available.getId === remove.getId
        ) === -1
      );
    });
    this.sourceTasks = [
      ...this.sourceTasks,
      ...availableRemove,
      ...tasksRemove
    ].filter(task => {
      return !task.isReady();
    });
    console.log(
      `AVAILABLE: `, this.availableTasks,
      `SELECTED: `, this.selectedTasks,
      `SOURCE: `, this.sourceTasks,
    );
  }

  onSelectTask(value: TaskPlan) {
    this.availableTasks = this.availableTasks.filter(task => {
      return value.getId !== task.getId;
    });
    this.selectedTasks = [...this.selectedTasks, value];
  }

  onChecked(checked: boolean, index: number) {
    let taskPlan = this.selectedTasks[index];
    if (checked) {
      taskPlan.setPlanStatus = TaskStatus.Done;
      const filterTask = this.getAvailableAndSourceTask(this.sourceTasks, this.selectedTasks);
      this.sourceTasks = filterTask.sourceTask;
      this.availableTasks = [...filterTask.availableTask, ...this.availableTasks];
    } else {
      taskPlan.setPlanStatus = taskPlan.getStatus;
      let selectedRemove = this.filterSelectedRemove(taskPlan, this.selectedTasks);
      const availableRemove = this.filterAvailableToRemove(selectedRemove, this.availableTasks);
      selectedRemove = selectedRemove.filter(selected => selected.getId !== taskPlan.getId);
      this.onRemoveTask(selectedRemove, availableRemove);
    }
  }

  onDeleteTaskSelected(value: TaskPlan) {
    value.setPlanStatus = value.getStatus;
    let selectedRemove = this.filterSelectedRemove(value, this.selectedTasks);
    const availableRemove = this.filterAvailableToRemove(selectedRemove, this.availableTasks);
    selectedRemove = selectedRemove.filter(selected => selected.getId !== value.getId);
    this.onRemoveTask(selectedRemove, availableRemove);
    this.selectedTasks = this.selectedTasks.filter(selected => {
      return selected.getId !== value.getId;
    });
    this.availableTasks = [...this.availableTasks, value].filter(
      (available, i, arr) => {
        return i === arr.findIndex(task => task.getId === available.getId);
      }
    );
  }

  onSetSeverityTask(value: TaskPlan) {
    this.dialog.open(FormReportAlarmComponent, {
      data: value,
      width: '700px',
      disableClose: true,
    }).afterClosed().subscribe((val: Task) => {
      if (val) {
        value.setNote = val.getNote;
        // value.setSeverity = val.getSeverity;
        this.selectedTasks = this.selectedTasks.map(task => {
          return value.getId === task.getId ? value : task;
        });
      }
    });
  }

  onAddAlarm() {
  }

  onUpdateAlarm() {
  }

  onDeleteAlarm() {
  }

  onClose() {
    const dailyReport = new DailyReport();
    dailyReport.setId = this.formData.data.getId;
    dailyReport.setDate = this.dateControl.value;
    dailyReport.setSitework = this.sitework;
    dailyReport.setSubmit = false;
    dailyReport.setNote = this.noteControl.value;
    dailyReport.setTasexecutions = this.generateTaskExecutions();
    console.log(dailyReport);
    this.dialogRef.close(dailyReport);
  }

  generateTaskExecutions(): TaskExecution[] {
    let taskExecutions: TaskExecution[] = [];
    this.selectedTasks.forEach(task => {
      task.setStatus = task.getPlanStatus;
      task.resources.forEach((id: number) => {
        const taskExecution = new TaskExecution();
        taskExecution.setDate = this.dateControl.value;
        taskExecution.setTask = task;
        taskExecution.setFinish = task.getPlanStatus === TaskStatus.Done ? true : false;
        taskExecution.setNote = task.getNote;
        taskExecution.setResource = this.resources.find(resource => resource['id'] == id);
        taskExecution.setManHour = parseFloat((task.manhour / task.resources.length).toFixed(1));
        taskExecutions = [...taskExecutions, taskExecution];
      })
    });
    const sourceTaskexecution = this.formData.data['taskExecutions'] ? this.formData.data['taskExecutions'] : []
    taskExecutions = taskExecutions.map((val, i) => {
      if (sourceTaskexecution[i]) {
        val.setId = sourceTaskexecution[i].getId;
      }
      return val;
    });
    return taskExecutions;
  }
}
