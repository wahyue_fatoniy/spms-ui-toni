import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DailyreportTableComponent } from './dailyreport-table.component';

describe('DailyreportTableComponent', () => {
  let component: DailyreportTableComponent;
  let fixture: ComponentFixture<DailyreportTableComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DailyreportTableComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DailyreportTableComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
