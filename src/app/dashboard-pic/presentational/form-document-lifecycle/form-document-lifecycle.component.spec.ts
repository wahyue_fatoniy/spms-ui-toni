import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FormDocumentLifecycleComponent } from './form-document-lifecycle.component';

describe('FormDocumentLifecycleComponent', () => {
  let component: FormDocumentLifecycleComponent;
  let fixture: ComponentFixture<FormDocumentLifecycleComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FormDocumentLifecycleComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FormDocumentLifecycleComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
