import { Component, OnInit, Inject } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material';
import { Document, DocumentType, DocumentStatus } from 'src/app/models/document.model';
import { Customer } from 'src/app/models/customer.model';
import { FormlyFieldConfig } from '@ngx-formly/core';
import { FormGroup } from '@angular/forms';
import { Documentation } from 'src/app/models/documentation.model';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

interface FormData {
  data: Document,
  customers: Observable<Customer[]>,
  documentations: Observable<Documentation[]>;
}

@Component({
  selector: 'app-form-document',
  templateUrl: './form-document.component.html',
  styleUrls: ['./form-document.component.css']
})
export class FormDocumentComponent implements OnInit {
  fields: FormlyFieldConfig[];
  model: Document;
  form: FormGroup;
  customers: Observable<Customer[]>;
  documentations: Observable<Documentation[]>;

  documentationList: Documentation[];

  constructor(
    @Inject(MAT_DIALOG_DATA) public formData: FormData,
    public dialogRef: MatDialogRef<any>
  ) {
    this.form = new FormGroup({});
    this.model = formData.data;
    this.fields = formData.data.formFieldsConfig;
    this.documentations = formData.documentations;
    this.customers = formData.customers;
  }

  ngOnInit() {
    this.runSubscription();
    this.setHideNipField();
    this.setAuthorOptions();
    this.setReviewerOptions();
    this.setTypeDocOptions();
  }

  runSubscription() {
    this.documentations.subscribe(documentations => {
      this.documentationList = documentations;
    });
  }

  setHideNipField() {
    this.fields = this.fields.map(field => {
      if (field.id === '2') {
        field.fieldGroup = field.fieldGroup.map(member => {
          if (member.key === 'nip') {
            member.hideExpression = true;
          }
          return member;
        });
      }
      return field;
    });
  }

  setAuthorOptions() {
    this.fields = this.fields.map(field => {
      if (field.id === '2') {
        field.fieldGroup = field.fieldGroup.map(group => {
          if (group.key === 'author') {
            group.templateOptions.options = this.documentations.pipe(
              map(documentations => {
                return documentations.map(documentation => {
                  return { value: documentation.getId, label: documentation.getParty['name'] }
                });
              })
            )
          }
          return group;
        });
      }
      return field;
    });
  }

  setReviewerOptions() {
    this.fields = this.fields.map(field => {
      if (field.id === '3') {
        field.fieldGroup = field.fieldGroup.map(group => {
          if (group.key === 'reviewer') {
            group.templateOptions.options = this.customers.pipe(
              map(customers => {
                return customers.map(customer => {
                  return { value: customer.getParty['name'], label: customer.getParty['name'] }
                });
              })
            )
          }
          return group;
        });
      }
      return field;
    });
  }

  setTypeDocOptions() {
    this.fields = this.fields.map(field => {
      if (field.id === '3') {
        field.fieldGroup = field.fieldGroup.map(group => {
          if (group.key === 'documentType') {
            group.templateOptions.options = [
              { value: DocumentType.ATP, label: DocumentType.ATP },
              { value: DocumentType.COMPLETENESS_ATP, label: DocumentType.COMPLETENESS_ATP },
              { value: DocumentType.BAUT, label: DocumentType.BAUT },
              { value: DocumentType.BAST, label: DocumentType.BAST },
            ];
          }
          return group;
        });
      }
      return field;
    });
  }

  onClose() {
    const authorId = parseInt(this.model.getAuthor);
    this.model.setStatus = DocumentStatus.create;

    this.model.setAuthor = this.documentationList.find(doc => doc.getId === authorId) ?
      this.documentationList.find(doc => doc.getId === authorId).getParty ?
        this.documentationList.find(doc => doc.getId === authorId).getParty['name'] : null : null;

    this.model.setNip = this.documentationList.find(doc => doc.getId === authorId) ?
      this.documentationList.find(doc => doc.getId === authorId).getNIP : null;

    console.log(this.model);
    this.dialogRef.close(this.model);
  }
}
