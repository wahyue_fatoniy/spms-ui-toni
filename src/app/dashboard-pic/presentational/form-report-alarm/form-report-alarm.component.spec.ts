import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FormReportAlarmComponent } from './form-report-alarm.component';

describe('FormReportAlarmComponent', () => {
  let component: FormReportAlarmComponent;
  let fixture: ComponentFixture<FormReportAlarmComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FormReportAlarmComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FormReportAlarmComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
