import { Component, OnInit, Inject } from '@angular/core';
import { Task, SeverityType } from 'src/app/models/task.model';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material';
import { FormControl, Validators } from '@angular/forms';
import { takeUntil } from 'rxjs/operators';
import { Subject } from 'rxjs';

@Component({
  selector: 'app-form-report-alarm',
  templateUrl: './form-report-alarm.component.html',
  styleUrls: ['./form-report-alarm.component.css']
})
export class FormReportAlarmComponent implements OnInit {

  destroy$ = new Subject();
  note: FormControl;
  severity: FormControl;
  severities: SeverityType[];
  task: Task;

  constructor(
    @Inject(MAT_DIALOG_DATA) public data: Task,
    public dialgRef: MatDialogRef<any>
  ) {
    this.task = new Task(data);
    this.note = new FormControl(this.task.getNote ? this.task.getNote : null);
    this.severity = new FormControl(null, [
      Validators.required
    ]);
    this.severities = [
      SeverityType.NORMAL,
      SeverityType.MAJOR_I,
      SeverityType.MAJOR_E,
      SeverityType.CRITICAL_I,
      SeverityType.CRITICAL_E
    ];
  }


  ngOnInit() {
    this.initSeverityControl();
    this.initNoteControl();
  }

  initNoteControl() {
    this.note.valueChanges
      .pipe(takeUntil(this.destroy$))
      .subscribe(_note => this.task.setNote = _note ? _note : null);
  }

  initSeverityControl() {
    // this.severity.valueChanges
    //   .pipe(takeUntil(this.destroy$))
    //   .subscribe(_severity => {
    //     if (
    //       _severity === SeverityType.NORMAL
    //     ) {
    //       this.note.setValue(null);
    //     }
    //     this.task.setSeverity = _severity ? _severity : null;
    //   });
  }

  getTask(): string {
    return this.task.getActivity ? this.task.getActivity.getName : null;
  }

  ngOnDestroy() {
    this.destroy$.next(true);
    this.destroy$.complete();
  }

  onClose() {
    this.dialgRef.close(this.task);
  }

}
