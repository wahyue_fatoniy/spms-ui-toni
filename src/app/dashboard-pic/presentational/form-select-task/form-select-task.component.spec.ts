import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FormSelectTaskComponent } from './form-select-task.component';

describe('FormSelectTaskComponent', () => {
  let component: FormSelectTaskComponent;
  let fixture: ComponentFixture<FormSelectTaskComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FormSelectTaskComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FormSelectTaskComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
