import { Component, OnInit, Inject } from "@angular/core";
import {
  MAT_DIALOG_DATA,
  MatCheckboxChange
} from "@angular/material";
import { Task, TaskPlan } from "src/app/shared/models/task/task";

export interface FormSelectData {
  sourceTask: TaskPlan[];
  checkedTask: TaskPlan[];
}

@Component({
  selector: "app-form-select-task",
  templateUrl: "./form-select-task.component.html",
  styleUrls: ["./form-select-task.component.css"]
})
export class FormSelectTaskComponent implements OnInit {
  displayedColumns: string[] = ["check", 'activity', "status", 'desc'];
  constructor(
    @Inject(MAT_DIALOG_DATA) public data: FormSelectData
  ) {
  }

  ngOnInit() {
    this.setCheckedSource();
  }

  onChecked(event: MatCheckboxChange) {
    let task = JSON.parse(JSON.stringify(event.source.value));
    if (event.checked === true) {
      this.data.checkedTask.push(task);
    } else if (event.checked === false) {
      this.data.checkedTask = this.data.checkedTask.filter(val => {
        return val["id"] !== task["id"];
      });
    }
    this.data.checkedTask = this.data.checkedTask.map(val => new TaskPlan(new Task(val)));
    console.log(this.data.checkedTask);
  }

  setCheckedSource() {
    this.data.sourceTask.forEach(source => {
      source['checked'] = false;
      this.data.checkedTask.forEach(checked => {
        if (checked.getId() === source.getId()) {
          source['checked'] = true;
        }
      });
    });
    console.log(this.data);
  }
}
