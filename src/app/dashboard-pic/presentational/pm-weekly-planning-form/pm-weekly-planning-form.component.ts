import { Component, OnInit, Inject, OnDestroy } from "@angular/core";
import { FormGroup, FormBuilder, Validators } from "@angular/forms";
import { MAT_DIALOG_DATA, MatDialog, MatDialogRef } from "@angular/material";
import { Subject, Observable, combineLatest } from "rxjs";
import { DatePipe } from "@angular/common";
import { takeUntil } from "rxjs/operators";
import { LocalCurrencyPipe } from "src/app/pipes/localCurrency/local-currency.pipe";
import { Task } from "src/app/models/task.model";
import { NumberInStringPipe } from "src/app/pipes/numberInString/number-in-string.pipe";
import { DailyPlan } from "src/app/models/dailyPlan.model";
import { Engineer } from "src/app/models/engineer.model";
import { Technician } from "src/app/models/technician.model";
import { Documentation } from "src/app/models/documentation.model";
import { SiteWork } from "src/app/models/siteWork.model";
import { Resource } from "src/app/models/resource.model";
import { Alarm } from "src/app/models/alarm.model";

interface FormData {
  data: DailyPlan;
  sitework: SiteWork;
  tasks: Observable<Task[]>;
  engineers: Observable<Engineer[]>;
  technicians: Observable<Technician[]>;
  documentations: Observable<Documentation[]>;
  alarms: Alarm[];
}

@Component({
  selector: "app-pm-weekly-planning-form",
  templateUrl: "./pm-weekly-planning-form.component.html",
  styleUrls: ["./pm-weekly-planning-form.component.css"]
})

export class PmWeeklyPlanningFormComponent implements OnInit, OnDestroy {
  public form: FormGroup;
  public sourceTask: Task[] = [];
  public availableTask: Task[] = [];
  public selectedTask: Task[] = [];
  public localCurrencyPipe: LocalCurrencyPipe = new LocalCurrencyPipe();
  public setToNumber: NumberInStringPipe = new NumberInStringPipe();
  public minDate: Date;
  public activity = {
    CREATE_BAUT: "Create Baut",
    SUBMIT_BAUT: "Submit Baut",
    MOS: "Mos",
    INSTALLATION: "Installation",
    INTEGRATION: "Integration",
    TAKE_DATA: "Take Data",
    ATP: "Atp",
    CREATE_ATP: 'Create Atp',
    SUBMIT_ATP: 'Submit Atp',
    CREATE_COMPLETENESS_ATP: "Create Completeness Atp",
    SUBMIT_COMPLETENESS_ATP: "Submit Completeness Atp"
  };
  public status = {
    ON_PROGRESS: "On Progress",
    DONE: "Done",
    CANCEL: "Cancel",
    NOT_STARTED: "Not Started"
  };
  public datePipe: DatePipe = new DatePipe("id");
  public planning: DailyPlan;
  public destroy$ = new Subject<boolean>();

  public tasks: Observable<Task[]>;
  public technicians: Observable<Technician[]>;
  public engineers: Observable<Engineer[]>;
  public documentation: Observable<Documentation[]>;
  public sitework: SiteWork;
  public resources: Resource[];
  public selectedResources: Resource[];
  public alarms: Alarm[];
  public displayedAlarms: string[];

  constructor(
    @Inject(MAT_DIALOG_DATA) public formData: FormData,
    public formBuilder: FormBuilder,
    public dialogRef: MatDialogRef<any>
  ) {
    this.planning = formData.data;
    this.tasks = formData.tasks;
    this.technicians = formData.technicians;
    this.engineers = formData.engineers;
    this.documentation = formData.documentations;
    this.sitework = formData.sitework;
    this.displayedAlarms = ['severity', 'actTime', 'description'];
    this.alarms = [];
  }

  ngOnInit() {
    this.getResources();
    this.setFormValidation();
    this.initialData();
    this.initResources();
  }

  ngOnDestroy() {
    this.destroy$.next(true);
    this.destroy$.complete();
  }

  getResources() {
    combineLatest(
      this.engineers,
      this.technicians,
      this.documentation
    ).subscribe(val => {
      this.resources = [...val[0], ...val[1], ...val[2]];
    })
  }

  public setFormValidation(): void {
    this.form = this.formBuilder.group({
      id: [this.planning.getId],
      date: [
        this.planning.getDate
          ? new Date(this.planning.getDate.toString())
          : new Date(),
        Validators.required
      ],
      tasks: [this.planning.getTasks, Validators.required],
      note: [this.planning.getNote],
    });
  }

  public initResources() {
    const resources = this.planning.getResources.map(resource => {
      return resource;
    })
    this.selectedResources = resources ? resources : [];
  }

  public initialData(): void {
    this.tasks
      .pipe(takeUntil(this.destroy$))
      .subscribe((tasks: Task[]) => {
        this.sourceTask = tasks.filter(val => {
          return (
            !val.isFinished() &&
            !this.planning.getTasks.find(task => task.getId === val.getId)
          );
        }).map(val => new Task(val));

        this.selectedTask = this.planning.getTasks.map(val => new Task(val))
          ? this.planning.getTasks.filter(val => !val.isFinished()).map(val => new Task(val))
          : [];
        this.initialAvailableTask();
      });
  }

  initialAvailableTask(): void {
    this.availableTask = this.sourceTask.filter(task => {
      return task.isInitial() || task.isReady();
    });
    if (this.selectedTask.length !== 0) {
      this.availableTask = this.filterTaskReady(this.sourceTask, this.selectedTask).taskReady;
    }
    this.sourceTask = this.sourceTask.filter(val => {
      return !this.availableTask.find(
        available => available["id"] === val["id"]
      );
    });
  }

  onSelectTask(task: Task) {
    let taskIsReady: { sourceTask: Task[], taskReady: Task[] };
    /**remove data in availableTask with taskId*/
    this.availableTask = this.availableTask.filter((val: Task) => {
      return val.getId !== task.getId;
    });
    this.selectedTask = [...this.selectedTask, task];
    taskIsReady = this.filterTaskReady(this.sourceTask, this.selectedTask);
    this.sourceTask = taskIsReady.sourceTask;
    this.availableTask = [...this.availableTask, ...taskIsReady.taskReady];
    this.form.get("tasks").setValue(this.selectedTask);
  }

  onRemoveTask(value: Task) {
    let selectRemove: Task[] = this.filterTaskRemove(value, this.selectedTask);
    let availableRemove: Task[] = this.filterAvailableRemove(selectRemove, this.availableTask);
    this.selectedTask = this.selectedTask.filter(select => {
      return !selectRemove.find(remove => remove.getId === select.getId);
    });
    this.availableTask = this.availableTask.filter(available => {
      return availableRemove.findIndex(remove => available.getId === remove.getId) === -1;
    });
    selectRemove = selectRemove.filter((select, i, arr) => {
      if (
        arr.find(val => {
          return select.isRequirement(val);
        })
      ) {
        availableRemove = [...availableRemove, select];
      }
      return !arr.find(val => {
        return select.isRequirement(val);
      });
    });
    this.availableTask = [...this.availableTask, ...selectRemove];
    this.form.get("tasks").setValue(this.selectedTask);
    /**concat predecessor data to source tasks*/
    this.sourceTask = [...this.sourceTask, ...availableRemove];
  }

  filterTaskRemove(taskRemove: Task, selectedTask: Task[]): Task[] {
    let selectRemove: Task[] = [];
    selectedTask = selectedTask.filter((task: Task, i, arr) => {
      if (task.isRequirement(taskRemove)) {
        selectRemove = [...selectRemove, new Task(task)];
      }
      selectRemove.forEach(select => {
        if (task.isRequirement(select)) {
          selectRemove = [...selectRemove, new Task(task)];
        }
      });
    });
    selectRemove = [...selectRemove, taskRemove].filter((select, i, arr) => {
      return i === arr.findIndex(val => val.getId === select.getId);
    });
    return selectRemove;
  }

  filterAvailableRemove(selectRemove: Task[], availableTask: Task[]): Task[] {
    let availableRemove: Task[] = [];
    availableTask.forEach(available => {
      if (
        selectRemove.findIndex(select => available.isRequirement(select)
        ) !== -1) {
        availableRemove = [...availableRemove, available];
      }
    });
    return availableRemove;
  }

  filterTaskReady(sourceTask: Task[], selectedTask: Task[]): { sourceTask: Task[], taskReady: Task[] } {
    // console.log(sourceTask, selectedTask);
    let i = 0;
    let taskIsReady: Task[] = [];
    sourceTask = sourceTask.filter((val: Task) => {
      i = 0;
      selectedTask.forEach(available => {
        if (val.isRequirement(available)) {
          i = i + 1;
        }
      });
      if (i === val.getRequirements.length) {
        taskIsReady = [...taskIsReady, new Task(val)];
        return false;
      }
      return true;
    });
    return { sourceTask: sourceTask, taskReady: taskIsReady };
  }

  onSelectResource(val) {
    if (val) {
      this.selectedResources = [
        ...this.selectedResources,
        val
      ].filter((resource, i, arr) => {
        return i === arr.findIndex(value => (value ? value['id'] : null) === resource['id'])
      });
    }
  }

  onDeleteResource(id: number) {
    if (id) {
      this.selectedResources = this.selectedResources.filter(resource => {
        return resource['id'] !== id;
      });
    }
  }

  onClose() {
    const dailyPlan = new DailyPlan();
    dailyPlan.setId = this.planning.getId;
    dailyPlan.setDate = this.form.get('date').value;
    dailyPlan.setResources = this.selectedResources;
    dailyPlan.setTasks = this.form.get('tasks').value;
    dailyPlan.setSitework = this.sitework;
    dailyPlan.setNote = this.form.get('note').value;
    console.log(dailyPlan);
    this.dialogRef.close(dailyPlan);
  }

}
