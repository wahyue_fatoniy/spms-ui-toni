import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ApprovalBudgetComponent } from './approval-budget.component';

describe('ApprovalBudgetComponent', () => {
  let component: ApprovalBudgetComponent;
  let fixture: ComponentFixture<ApprovalBudgetComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ApprovalBudgetComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ApprovalBudgetComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
