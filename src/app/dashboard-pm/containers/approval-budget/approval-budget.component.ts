import { Component, OnInit } from '@angular/core';
import { ColumnConfig, actionType, ColumnType, OnEvent } from 'src/app/shared/table-builder';
import { RequestService } from 'src/service/request-service/request.service';
import { RequestStatus, RequestType, Request } from 'src/app/models/request.model';
import { MatDialog } from '@angular/material';
import { ToastrService } from 'ngx-toastr';
import { DialogComponent } from 'src/app/shared/dialog/dialog.component';
import { AAAService } from 'src/app/shared/aaa/aaa.service';
import { ProjectManagerService } from 'src/service/projectManager-service/project-manager.service';
import { combineLatest } from 'rxjs';
import { ProjectManager } from 'src/app/models/projectManager.model';
import { Role } from 'src/app/shared/models/role/role';
import { User } from 'src/app/models/user.model';
import { FormViewPlanningComponent } from '../../presentational/form-view-planning/form-view-planning.component';
import { DailyPlan } from 'src/app/models/dailyPlan.model';

interface TableModel {
  id: number;
  projectId: string;
  date: Date;
  opex: number;
  requester: string;
}

@Component({
  selector: 'app-approval-budget',
  template: `
    <app-approval-budget-table
      [data]="data"
      [title]="title"
      [actionTypes]="actionTypes"
      (onEvent)="onEvent($event)"
      [columnConfigs]="columnConfigs"
    >
    <app-approval-budget-table>
  `,
})
export class ApprovalBudgetComponent implements OnInit {
  columnConfigs: ColumnConfig[];
  actionTypes: actionType[];
  title: string;
  data: any[];

  requests: Request[];
  projectManager: ProjectManager;
  approver: User;

  constructor(
    public requestService: RequestService,
    public dialog: MatDialog,
    public toastService: ToastrService,
    public aaaService: AAAService,
    public pmService: ProjectManagerService,
  ) {
    this.data = [{}];
    this.title = 'Approval Opex'
    this.actionTypes = ['ACCEPT', 'REJECT', 'VIEW'];
    this.columnConfigs = [
      { label: 'No.', key: 'no', columnType: ColumnType.string },
      { label: 'Project ID', key: 'projectId', columnType: ColumnType.string },
      { label: 'Date', key: 'date', columnType: ColumnType.date, format: 'mediumDate' },
      { label: 'Opex', key: 'opex', columnType: ColumnType.currency, local: 'Rp. ' },
      { label: 'Requester', key: 'requester', columnType: ColumnType.string },
    ];
  }

  ngOnInit() {
    this.runPmSubscription();
    this.runSubscription();
    this.runService();
    this.initApprover();
  }

  initApprover() {
    const currentUser = this.aaaService.getCurrentUser() ? this.aaaService.getCurrentUser().getUserMetadata().roles.find(role => {
      return role.roleType === 'USER'
    }) : new Role();

    this.approver = new User();
    this.approver['id'] = currentUser['id'];
    this.approver.setUsername = currentUser['username'];
    this.approver.setPassword = currentUser['password'];
    this.approver.setEmail = currentUser['email'];
  }

  getPMAuth(projectManagers: ProjectManager[]) {
    projectManagers = projectManagers.filter(_set => {
      const region = _set.getRegion ? _set.getRegion : null;
      const id = region ? region.getId : null;
      return !this.aaaService.isAuthorized('read:project:{province:$}:{job:$}') ||
        this.aaaService.isAuthorized(`read:project:{province:${id}}:{job:${_set.getId}}`);
    });
    return projectManagers;
  }

  runPmSubscription() {
    this.pmService.subscriptionData()
      .subscribe((val: ProjectManager[]) => {
        this.projectManager = this.getPMAuth(val) ? this.getPMAuth(val)[0] : null;
      });
  }

  runService() {
    combineLatest(
      this.pmService.getData(),
      this.requestService.findByStatus(RequestStatus.proposed, RequestType.requested_opex),
    )
      .subscribe();
  }

  runSubscription() {
    this.requestService.subscriptionData().subscribe(val => {
      this.requests = val;
      // this.data = val.map(request => {
      //   return this.setTableModel(request);
      // });
    })
  }

  setTableModel(request: Request) {
    return <TableModel>{
      id: request.getId,
      projectId: request.getObject ? request.getObject['siteWork'] ? request.getObject['siteWork']['projectCode'] : null : null,
      date: request.getStartDate ? new Date(request.getStartDate) : null,
      requester: request.getRequester ? request.getRequester['party'] ? request.getRequester['party']['name'] : '' : '',
      opex: request.getObject ? request.getObject['opex'] : 0
    }
  }

  onEvent(event: OnEvent) {
    switch (event.type) {
      case 'ACCEPT':
        this.acceptRequest(event.value);
        break;
      case 'REJECT':
        this.rejectRequest(event.value);
        break;
      case 'VIEW':
        this.viewTask(event.value);
        break;
    }
  }

  acceptRequest(model: TableModel) {
    const request = this.findRequest(model.id);
    this.dialog.open(DialogComponent, {
      data: { id: model.id, message: `Are tou sure to approve request opex with value ${model.opex}?` },
      width: '550px',
      disableClose: true,
    }).afterClosed().subscribe(val => {
      if (val) {
        request.setStatus = RequestStatus.accepted;
        request.setApprover = this.approver;
        this.requestService.update(request)
          .then(() => {
            this.toastService.success('Success approve request opex', 'Success');
            this.actionTypes = [];
          })
          .catch(() => {
            this.toastService.error('Failed approve request opex', 'Failed');
          })
      }
    });
  }

  rejectRequest(model: TableModel) {
    const request = this.findRequest(model.id);
    this.dialog.open(DialogComponent, {
      data: { id: model.id, message: `Are tou sure to reject request opex with value ${model.opex}?` },
      width: '550px',
      disableClose: true,
    }).afterClosed().subscribe(val => {
      if (val) {
        request.setStatus = RequestStatus.rejected;
        request.setApprover = this.approver;
        this.requestService.update(request)
          .then(() => {
            this.toastService.success('Success reject request opex', 'Success');
            this.actionTypes = [];
          })
          .catch(() => {
            this.toastService.error('Failed reject request opex', 'Failed');
          })
      }
    });
  }

  viewTask(model: TableModel) {
    let planning = this.findRequest(model.id);
    this.dialog.open(FormViewPlanningComponent, {
      data: new DailyPlan(planning ? planning.getObject : null),
      width: '800px',
      disableClose: true
    });
  }

  findRequest(id: number) {
    return this.requests.find(request => {
      return request.getId === id;
    });
  }
}
