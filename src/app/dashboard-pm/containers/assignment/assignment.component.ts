import { Component, OnInit, OnDestroy, AfterViewInit } from '@angular/core';
import { actionType, ColumnConfig, ColumnType, OnEvent } from 'src/app/shared/table-builder';
import { MatDialog } from '@angular/material';
import { DialogComponent } from 'src/app/shared/dialog/dialog.component';
import { FormAssignmentComponent } from '../../presentational/form-assignment/form-assignment.component';
import { SowTypeService } from 'src/service/sowType-service/sow-type.service';
import { CustomerService } from 'src/service/customer-service/customer.service';
import { GeneralManagerService } from 'src/service/general-manager/general-manager.service';
import { SiteService } from 'src/service/site-service/site.service';
import { combineLatest, Subject, forkJoin } from 'rxjs';
import { AssignmentService } from 'src/service/assignment-service/assignment.service';
import { SiteWork } from 'src/app/models/siteWork.model';
import { AAAService } from 'src/app/shared/aaa/aaa.service';
import { ProjectManager } from 'src/app/models/projectManager.model';
import { ProjectManagerService } from 'src/service/projectManager-service/project-manager.service';
import { Request, RequestStatus, RequestType } from 'src/app/models/request.model';
import { RequestService } from 'src/service/request-service/request.service';
import { takeUntil } from 'rxjs/operators';
import { MailService } from 'src/service/mail-service/mail.service';
import { ToastrService } from 'ngx-toastr';
import { Mail } from 'src/app/models/mail.model';
import { Router } from '@angular/router';
import { RouteService } from 'src/service/route-service/route.service';
import { Contract, ContractStatus } from 'src/app/models/contract.model';
import { ContractService } from 'src/service/contract-service/contract.service';

interface TableModel {
  id: number;
  number: string;
  startDate: string;
  endDate: string;
  siteWorks: SiteWork[];
  customer: string;
  generalManager: string;
  status: ContractStatus;
}

@Component({
  selector: 'app-assignment',
  template: `
    <app-request-assignment-table
      [data]="data"
      [title]="title"
      [actionTypes]="actionTypes"
      (onEvent)="onEvent($event)"
      [columnConfigs]="columnConfigs"
      >
    </app-request-assignment-table>
  `,
  styleUrls: ['./assignment.component.css']
})
export class AssignmentComponent implements OnInit, OnDestroy {
  columnConfigs: ColumnConfig[];
  actionTypes: actionType[];
  title: string;
  data: any[];

  destroy$: Subject<boolean>;
  projectManager: ProjectManager;
  contracts: Contract[];
  requests: Request[];

  constructor(
    private dialog: MatDialog,
    private sowTypeService: SowTypeService,
    private siteService: SiteService,
    private customerService: CustomerService,
    private gmService: GeneralManagerService,
    private pmService: ProjectManagerService,
    private aaaService: AAAService,
    private requestService: RequestService,
    private mailService: MailService,
    private toastService: ToastrService,
    private route: Router,
    private routeService: RouteService,
    private contractService: ContractService
  ) {
    this.data = [];
    this.title = 'Contract(s)'
    this.actionTypes = ['ADD', 'UPDATE', 'DELETE', 'SUBMIT', 'VIEW'];
    this.columnConfigs = [
      { label: 'No.', key: 'no', columnType: ColumnType.string },
      { label: 'Number', key: 'number', columnType: ColumnType.string },
      { label: 'Customer', key: 'customer', columnType: ColumnType.string },
      { label: 'Approver', key: 'generalManager', columnType: ColumnType.string },
      { label: 'Start Date', key: 'startDate', columnType: ColumnType.date, format: 'shortDate' },
      { label: 'End Date', key: 'endDate', columnType: ColumnType.date, format: 'shortDate' },
      { label: 'Last Daily Report', key: 'lastReport', columnType: ColumnType.date, format: 'shortDate' },
      { label: 'Status', key: 'status', columnType: ColumnType.status },
    ];
  }

  ngOnInit() {
    this.destroy$ = new Subject()
    this.getSubscriptionData();
    this.getServiceData();
  }

  ngOnDestroy() {
    this.destroy$.next(true);
    this.destroy$.unsubscribe();
  }

  getAuthPm(projectManagers: ProjectManager[]): ProjectManager[] {
    return projectManagers.filter(_set => {
      const region = _set.getRegion ? _set.getRegion : null;
      const id = region ? region.getId : null;
      return (
        !this.aaaService.isAuthorized("read:project:{province:$}:{job:$}") ||
        this.aaaService.isAuthorized(
          `read:project:{province:${id}}:{job:${_set.getId}}`
        )
      );
    });
  }

  getServiceData() {
    combineLatest(
      this.gmService.getData(),
      this.siteService.getData(),
      this.sowTypeService.getData(),
      this.customerService.getData(),
      this.contractService.getData(),
      this.pmService.getData(),
      this.requestService.findByStatus(RequestStatus.rejected, RequestType.submitted_assignment)
    )
      .pipe(takeUntil(this.destroy$))
      .subscribe();
  }

  getSubscriptionData() {
    combineLatest(
      this.contractService.subscriptionData(),
      this.pmService.subscriptionData(),
      this.requestService.subscriptionData(),
    ).subscribe((val: [Contract[], ProjectManager[], Request[]]) => {
      this.contracts = val[0];
      this.data = val[0].map(contract => {
        return this.setTableModel(contract);
      });
      this.projectManager = this.getAuthPm(val[1])[0];
      this.requests = val[2];
    });
  }

  setTableModel(data: Contract): TableModel {
    return {
      id: data.getId,
      number: data.getNumber,
      startDate: data.getStartDate,
      endDate: data.getEndDate,
      siteWorks: data.getSiteworks,
      customer: data.getCustomer['party'] ? data.getCustomer['party']['name'] : '',
      generalManager: data.getGeneralManager['party'] ? data.getGeneralManager['party']['name'] : '',
      status: data.getStatus
    }
  };

  onEvent(event: OnEvent) {
    switch (event.type) {
      case 'ADD':
        this.addAssignment();
        break;
      case 'UPDATE':
        this.updateAssignment(event.value);
        break;
      case 'DELETE':
        this.deleteAssignment(event.value);
        break;
      case 'SUBMIT':
        this.submitAssignment(event.value);
        break;
      case 'VIEW':
        this.showListProject(event.value);
        break;
    }
  }

  addAssignment() {
    this.dialog.open(FormAssignmentComponent, {
      width: '1100px',
      data: {
        data: new Contract(),
        customers: this.customerService.subscriptionData(),
        generalManagers: this.gmService.subscriptionData(),
        sowTypes: this.sowTypeService.subscriptionData(),
        sites: this.siteService.subscriptionData(),
        projectManager: this.projectManager,
      },
      disableClose: true,
    }).afterClosed().subscribe((val: Contract) => {
      if (val) {
        this.contractService.create(val).then(contract => {
          console.log(val);
          this.toastService.success('Success Created Assignment', 'Success');
        }).catch(err => {
          console.log(err);
          this.toastService.error('Failed Created Assignment', 'Failed');
        });
      }
    });
  }

  updateAssignment(value: TableModel) {
    this.dialog.open(FormAssignmentComponent, {
      width: '1100px',
      data: {
        data: this.contracts.find(contract => contract.getId === value['id']),
        customers: this.customerService.subscriptionData(),
        generalManagers: this.gmService.subscriptionData(),
        sowTypes: this.sowTypeService.subscriptionData(),
        sites: this.siteService.subscriptionData(),
        projectManager: this.projectManager,
      },
      disableClose: true,
    }).afterClosed().subscribe(val => {
      if (val) {
        console.log(val);
        this.contractService.update(val).then(res => {
          this.toastService.success('Success Update Assignment', 'Success');
          console.log(res);
        }).catch(err => {
          this.toastService.error('Failed Update Assignment', 'Failed');
          console.log(err);
        });
      }
    });
  }

  deleteAssignment(value: TableModel) {
    const assignment = this.findContract(value);
    this.dialog.open(DialogComponent, {
      width: '450px',
      data: { id: value.id, message: `Are you sure to delete Assignment with ID ${assignment.getNumber}?` },
      disableClose: true,
    }).afterClosed().subscribe(val => {
      if (val) {
        this.contractService.delete(val).then(res => {
          this.toastService.success('Success delete assignment', 'Success');
          this.actionTypes = ['ADD'];
        }).catch(err => {
          this.toastService.error('Failed delete assignment', 'Failed');
          console.log(err);
        });
      }
    })
  }

  submitAssignment(value: TableModel) {
    const assignment = this.findContract(value);
    const request = this.findRequest(value);
    this.dialog.open(DialogComponent, {
      width: '450px',
      data: { id: value.id, message: `Are you sure to submit with ID ${assignment.getNumber}?` },
      disableClose: true,
    }).afterClosed().subscribe(id => {
      if (id) {
        if (request) {
          this.updateRequest(request, assignment);
        } else {
          this.createRequestApproval(assignment);
        }
      }
    });
  }

  createRequestApproval(contract: Contract) {
    contract.setStatus = ContractStatus.PROPOSED;
    let request = new Request();
    request.setStartDate = new Date();
    request.setStatus = RequestStatus.proposed;
    request.setType = RequestType.submited_contract;
    request.setRequester = this.projectManager;
    request.setObject = contract;
    console.log(contract);
    forkJoin(
      this.contractService.update(contract),
      this.requestService.create(request)
    ).toPromise().then(val => {
      this.sendMail(contract.getNumber);
      this.toastService.success('Success submit assignment', 'Success');
      this.actionTypes = ['ADD'];
    }).catch(err => {
      // console.log(err);
      this.toastService.error('Failed submit Assignment', 'FAILED');
    });
  }

  updateRequest(request: Request, contract: Contract) {
    request.setStatus = RequestStatus.proposed;
    contract.setStatus = ContractStatus.PROPOSED;
    forkJoin(
      this.contractService.update(contract),
      this.requestService.update(request)
    ).toPromise()
      .then(val => {
        this.sendMail(contract.getNumber);
        this.toastService.success('Success submit assignment', 'Success');
        console.log(val);
      }).catch(err => {
        this.toastService.error('Failed submit assignment', 'Failed');
        console.log(err);
      });
  }

  showListProject(value: TableModel) {
    const contract = this.findContract(value)
    this.routeService.updatePathParamState({path: 'contract-detail', param: contract});
    this.route.navigate([`home/dashboard-pm/contract-detail`])
    .then()
    .catch(err => {
      console.log(err);
    });
    console.log(value);
  }

  sendMail(assignmentId: string) {
    // const email = new Mail();
    // email._to = 'toni240598@gmail.com';
    // email._cc = [];
    // email._subject = `Request Assignment ${assignmentId} Approval`;
    // email._text = `Request Assignment dengan ID ${assignmentId}, oleh Project Manager ${this.projectManager ? this.projectManager.getParty ? this.projectManager.getParty['name'] : '' : ''}`
    // this.mailService.send(email).subscribe(val => {
    //   console.log(val);
    // }, err => console.log(err));
  }

  findContract(model: TableModel) {
    return this.contracts.find(contract => {
      return contract.getId === model.id;
    });
  }

  findRequest(model: TableModel) {
    return this.requests.find(request => {
      return (request.getObject ? request.getObject['id'] : null) === model.id;
    });
  }
}
