import { Component, OnInit } from '@angular/core';
import { RouteService } from 'src/service/route-service/route.service';
import { ContractService } from 'src/service/contract-service/contract.service';
import { Contract } from 'src/app/models/contract.model';
import { Router } from '@angular/router';
import { DailyReportService } from 'src/service/daily-report-service/daily-report.service';

@Component({
  selector: 'app-contract-detail',
  template: `
    <app-view-contract-detail
      [data]="contract"
      (onEvent)="onEvent($event)"
    >
    <app-view-contract-detail>
  `,
})
export class ContractDetailComponent implements OnInit {
  contract: Contract;

  constructor(
    public routeService: RouteService,
    public contractService: ContractService,
    public route: Router,
    public dailyReportService: DailyReportService
  ) { }

  ngOnInit() {
    this.routeService.pathParam.subscribe(val => {
      if (val) {
        this.contract = val.param ? new Contract(val.param) : null;
        this.setLastReport();
      }
    });
  }

  setLastReport() {
    if (this.contract) {
      this.contract.setSiteworks = this.contract.getSiteworks.map(sitework => {
        this.dailyReportService.findBySiteWorkId(sitework.getId).subscribe(report => {
          console.log(report);
        });
        return sitework;
      });
    }
  }

  onEvent(event) {
    const id = event.value ? event.value['id'] : null;
    this.routeService.updatePathParamState({param: event.value, path: 'detail-project'});
    this.route.navigate([`home/dashboard-pm/detail-project`])
    .catch(err => {
      console.log(err);
    });
    console.log(event);
  }

}
