import { Component, OnInit } from "@angular/core";
import { AAAService } from "src/app/shared/aaa/aaa.service";
import { ProjectManagerService } from "src/app/shared/models/project-manager/project-manager.service";
import { SiteWorkService } from "src/service/siteWork-service/site-work.service";
import { Subject } from "rxjs";
import { takeUntil } from "rxjs/operators";
import { WidgetData } from "src/app/shared/widget-component/widget-component.component";
import { SiteWork } from "src/app/models/siteWork.model";
import { LocalCurrencyPipe } from "src/app/pipes/localCurrency/local-currency.pipe";
import { Document } from "src/app/models/document.model";
import { RangeDate } from "src/app/shared/range-date/range-date.component";
import { ExportExcelService } from "src/service/export-excel.service";
import { PieChartOptions } from "src/app/shared/pie-chart/pie-chart.component";
import { XlsExportPipe } from "src/app/pipes/xlsExport/xls-export.pipe";

@Component({
  selector: "app-dashboard",
  templateUrl: "./dashboard.component.html",
  styleUrls: ["./dashboard.component.css"]
})
export class DashboardComponent implements OnInit {
  private destroy$: Subject<boolean> = new Subject();
  public documentReject: WidgetData;
  public documentApprove: WidgetData;
  public documentCreate: WidgetData;
  public documentSubmit: WidgetData;
  public totalProject: WidgetData;
  public pieChartOptions: PieChartOptions = {
    view: [670, 300],
    data: [],
    animation: true,
    labels: true,
    legend: true,
    legendTitle: "Status",
    legendPosition: "right"
  };
  public valueOnProgress: WidgetData;
  public valueOnDone: WidgetData;
  private localCurrencyPipe: LocalCurrencyPipe = new LocalCurrencyPipe();
  private pmId: number[] = [];
  public range: RangeDate = {
    fromDate: new Date(),
    toDate: new Date(
      new Date().setDate(new Date().getDate() - new Date().getDay() + 30)
    )
  };
  public xlsExport: XlsExportPipe = new XlsExportPipe();
  public siteworks: SiteWork[] = [];

  constructor(
    private aaa: AAAService,
    private pmService: ProjectManagerService,
    private siteworkService: SiteWorkService,
    private excelService: ExportExcelService
  ) {
  }

  ngOnInit() {
    this.getLocalstorage();
    this.subscriptionData();
    this.getServiceData();
  }

  public getAuthPMs(): number[] {
    const pms = this.pmService.getTables().filter(_set => {
      const region = _set.getRegion() ? _set.getRegion() : null;
      const id = region ? region.getId() : null;
      return (
        !this.aaa.isAuthorized("read:project:{province:$}:{job:$}") ||
        this.aaa.isAuthorized(
          `read:project:{province:${id}}:{job:${_set.getId()}}`
        )
      );
    });
    return pms.map(pm => pm.getId());
  }

  getLocalstorage(): void {
    let rangeDate = JSON.parse(localStorage.getItem("pmRangeDate"));
    if (rangeDate !== null) {
      this.range.fromDate = new Date(rangeDate["fromDate"]);
      this.range.toDate = new Date(rangeDate["toDate"]);
    }
  }

  getServiceData(): void {
    this.pmService
      .findAll()
      .pipe(takeUntil(this.destroy$))
      .subscribe(() => {
        this.pmId = this.getAuthPMs();
        if (this.pmId.length !== 0) {
          this.siteworkService
            .findByPMRange(this.pmId[0], this.range.fromDate, this.range.toDate)
            .pipe(takeUntil(this.destroy$))
            .subscribe();
        }
      });
  }

  public subscriptionData(): void {
    this.siteworkService
      .subscriptionData()
      .pipe(takeUntil(this.destroy$))
      .subscribe(val => {
        this.siteworks = val;
        this.setTotalProject(val);
        this.setWidgetDocument(val);
        this.setPieChartStatus(val);
        this.setWidgetPoValue(val);
      });
  }

  public setPieChartStatus(siteworks: SiteWork[]): void {
    let done = siteworks.filter(val => val.getStatus === "DONE").length;
    let notStarted = siteworks.filter(val => val.getStatus === "NOT_STARTED")
      .length;
    let onProgress = siteworks.filter(val => val.getStatus === "ON_PROGRESS")
      .length;
    let cancel = siteworks.filter(val => val.getStatus === "CANCEL").length;

    this.pieChartOptions.data = [
      { name: "Done", value: done },
      { name: "On Progress", value: onProgress },
      { name: "Not Started", value: notStarted },
      { name: "Cancel", value: cancel }
    ];
  }

  public setTotalProject(siteWorks: SiteWork[]): void {
    this.totalProject = {
      title: "Total Project",
      value: siteWorks.length,
      icon: "store",
      iconColor: "primary"
    };
  }

  public setWidgetDocument(siteworks: SiteWork[]): void {
    let documents: Document[] = [];
    let docCreate: Document[] = [];
    let docSubmit: Document[] = [];
    let docReject: Document[] = [];
    let docApprove: Document[] = [];
    siteworks.forEach(sitework => {
      documents = [...documents, ...sitework.getDocuments];
    });
    documents = documents.filter(doc => doc.getType !== "BAST");
    docCreate = documents.filter(doc => doc.getStatus === "CREATE");
    docSubmit = documents.filter(doc => doc.getStatus === "SUBMIT");
    docReject = documents.filter(doc => doc.getStatus === "REJECT");
    docApprove = documents.filter(doc => doc.getStatus === "APPROVE");

    this.documentCreate = {
      title: "Document Create",
      value: docCreate.length,
      icon: "file_copy",
      iconColor: "primary"
    };
    this.documentSubmit = {
      title: "Document Submit",
      value: docSubmit.length,
      icon: "send",
      iconColor: "primary"
    };
    this.documentReject = {
      title: "Document Reject",
      value: docReject.length,
      icon: "cached",
      iconColor: "warn"
    };
    this.documentApprove = {
      title: "Document Approve",
      value: docApprove.length,
      icon: "done",
      iconColor: "primary"
    };
  }

  public setWidgetPoValue(siteworks: SiteWork[]): void {
    let valueDone = 0;
    let valueOnProgress = 0;
    siteworks
      .filter(sitework => sitework.getStatus === "DONE")
      .map(sitework => {
        valueDone += sitework.getPoValue;
      });

    siteworks
      .filter(sitework => sitework.getStatus === "ON_PROGRESS")
      .map(sitework => {
        valueOnProgress += sitework.getPoValue;
      });

    this.valueOnDone = {
      title: "Value On Done",
      value: this.localCurrencyPipe.transform(valueDone),
      icon: "attach_money",
      iconColor: "primary"
    };
    this.valueOnProgress = {
      title: "Value On Progress",
      value: this.localCurrencyPipe.transform(valueOnProgress),
      icon: "attach_money",
      iconColor: "warn"
    };
  }

  public setRangeDate(date: RangeDate): void {
    if (date && this.pmId.length !== 0) {
      localStorage.setItem("pmRangeDate", JSON.stringify(date));
      this.range = {
        fromDate: date.fromDate,
        toDate: date.toDate
      };
      this.siteworkService
        .findByPMRange(this.pmId[0], date.fromDate, date.toDate)
        .pipe(takeUntil(this.destroy$))
        .subscribe();
    }
  }

}
