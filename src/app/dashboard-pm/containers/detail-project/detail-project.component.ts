import { Component, OnInit, OnDestroy } from '@angular/core';
import { RouteService } from 'src/service/route-service/route.service';
import { SiteWork } from 'src/app/models/siteWork.model';
import { TaskService } from 'src/service/task-service/task.service';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';

@Component({
  selector: 'app-detail-project',
  template: `
    <app-view-project-detail
      [data]="sitework"
    >
    </app-view-project-detail>`
})
export class DetailProjectComponent implements OnInit, OnDestroy {
  sitework: SiteWork;
  destroy$: Subject<boolean>;

  constructor(
    public routeService: RouteService,
    public taskService: TaskService
  ) { }

  ngOnInit() {
    this.destroy$ = new Subject();
    this.getSitework();
  }

  ngOnDestroy() {
    this.destroy$.next(true);
    this.destroy$.complete();
  }

  getSitework() {
    this.routeService.pathParam
    .pipe(takeUntil(this.destroy$))
    .subscribe(val => {
      if (val) {
        this.sitework = val.param;
        this.runService();
      }
    });
  }

  runService() {
    this.taskService.findBySiteWorkId(this.sitework ? this.sitework.getId : null)
    .pipe(takeUntil(this.destroy$))
    .subscribe(val => {
      this.sitework['tasks'] = val;
    });
  }

}
