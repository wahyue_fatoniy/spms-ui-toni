import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PmAlarmTableComponent } from './pm-alarm-table.component';

describe('PmAlarmTableComponent', () => {
  let component: PmAlarmTableComponent;
  let fixture: ComponentFixture<PmAlarmTableComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PmAlarmTableComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PmAlarmTableComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
