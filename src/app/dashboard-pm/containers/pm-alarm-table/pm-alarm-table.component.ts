import { Component, OnInit, ViewChild } from "@angular/core";
import { CoreTable, TableConfig } from "src/app/shared/core-table";
import { Task } from "src/app/models/task.model";
import { MatPaginator, MatSort, MatDialog } from "@angular/material";
import { Subject } from "rxjs";
import { takeUntil } from "rxjs/operators";
import { TaskService } from "src/service/task-service/task.service";
import { FormControl } from "@angular/forms";
import { SowType } from "src/app/models/sow-type.model";
import { ShowDescSowComponent } from "./show-desc-sow/show-desc-sow.component";
import { ShowDescTaskComponent } from "./show-desc-task/show-desc-task.component";
import { ActivatedRoute } from "@angular/router";

const CONFIG: TableConfig = {
  title: "Alarm",
  columns: ["no", "sowCode", "taskType", "activity", "severity"],
  typeFilters: [
    { value: "all", label: "All" },
    { value: "projectCode", label: "Project ID" },
    { value: "site", label: "Site" },
    { value: "startDate", label: "Start Date" },
    { value: "endDate", label: "End Date" },
    { value: "status", label: "Status" },
    { value: "duration", label: "Duration" },
    { value: "severity", label: "Severity" },
    { value: "totalOpex", label: "Total Opex" }
  ]
};

@Component({
  selector: "app-pm-alarm-table",
  templateUrl: "./pm-alarm-table.component.html",
  styleUrls: ["./pm-alarm-table.component.css"]
})
export class PmAlarmTableComponent extends CoreTable<Task> implements OnInit {
  @ViewChild(MatPaginator)
  paginator: MatPaginator;
  @ViewChild(MatSort)
  sort: MatSort;
  destroy = new Subject<boolean>();
  formFilterSeverity = new FormControl();
  tasksSource: Task[] = [];

  severityLabel = {
    CRITICAL_I: "Critical I",
    CRITICAL_E: "Critical E",
    MAJOR_I: "Major I",
    MAJOR_E: "Major E"
  };

  severityValue = ["CRITICAL_I", "CRITICAL_E", "MAJOR_I", "MAJOR_E"];

  constructor(
    private taskService: TaskService,
    private dialog: MatDialog,
    private activeRoute: ActivatedRoute
  ) {
    super(CONFIG);
  }

  ngOnInit() {
    this.getData();
  }

  public get loading() {
    return this.taskService.getLoading;
  }

  public getData(): void {
    this.activeRoute.params
    .pipe(takeUntil(this.destroy))
    .subscribe((data: {id: number}) => {
      if (data) {
        this.taskService
          .findBySiteWorkId(data.id)
          .pipe(takeUntil(this.destroy))
          .subscribe(() => {
            this.initializeDataSource();
          });
      }
    });
  }

  public initializeDataSource(): void {
    this.taskService
      .subscriptionData()
      .pipe(takeUntil(this.destroy))
      .subscribe((tasks: Task[]) => {
        this.tasksSource = this.sortSeverity(tasks);
        this.datasource.data = this.tasksSource;
        this.datasource.sort = this.sort;
        this.datasource.paginator = this.paginator;
        this.filterDatasource();
      });
  }

  public filterDatasource() {
    this.search.valueChanges
      .pipe(takeUntil(this.destroy))
      .subscribe(val => (this.datasource.filter = val));

    this.datasource.filterPredicate = (data: Task, keyword: string) => {
      return this.filterPredicate(data, keyword, this.typeFilter);
    };
    this.filterBySeverity();
  }

  public sortData(sort): void {
    this.datasource.data = this.datasource.data.sort((a, b) => {
      const isAsc = sort.direction === "asc";
      switch (sort.active) {
        case "sowCode":
          return this.compare(
            a.getSow.getSowType.getSowCode,
            b.getSow.getSowType.getSowCode,
            isAsc
          );
        case "taskType":
          // return this.compare(a.getTaskType, b.getTaskType, isAsc);
        case "activity":
          return this.compare(
            a.getActivity.getName,
            b.getActivity.getName,
            isAsc
          );
        case "severity":
          // return this.compare(a.getSeverity, b.getSeverity, isAsc);
        default:
          return 0;
      }
    });
  }

  public filterBySeverity() {
    // this.formFilterSeverity.valueChanges
    //   .pipe(takeUntil(this.destroy))
    //   .subscribe((val: string[]) => {
    //     val.forEach(severity => {
    //       this.datasource.data = this.sortSeverity(
    //         this.tasksSource.filter(task => task.getSeverity === severity)
    //       );
    //     });
    //   });
  }

  sortSeverity(tasks: Task[]): Task[] {
    let internalCritical = [];
    let eksternalCritical = [];
    let internalMajor = [];
    let eksternalMajor = [];
    // internalCritical = tasks.filter(task => {
    //   return task.getSeverity === "CRITICAL_I";
    // });
    // eksternalCritical = tasks.filter(task => {
    //   return task.getSeverity === "CRITICAL_E";
    // });
    // internalMajor = tasks.filter(task => {
    //   return task.getSeverity === "MAJOR_I";
    // });
    // eksternalMajor = tasks.filter(task => {
    //   return task.getSeverity === "MAJOR_E";
    // });
    return [
      ...internalCritical,
      ...eksternalCritical,
      ...internalMajor,
      ...eksternalMajor
    ];
  }

  showDescSow(sowType: SowType) {
    this.dialog.open(ShowDescSowComponent, {
      width: "850px",
      data: sowType,
      disableClose: true
    });
  }

  showDescSeverity(task: Task) {
    this.dialog.open(ShowDescTaskComponent, {
      data: task,
      width: "850px",
      disableClose: true
    });
  }
}
