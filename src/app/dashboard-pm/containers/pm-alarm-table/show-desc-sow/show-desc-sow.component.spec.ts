import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ShowDescSowComponent } from './show-desc-sow.component';

describe('ShowDescSowComponent', () => {
  let component: ShowDescSowComponent;
  let fixture: ComponentFixture<ShowDescSowComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ShowDescSowComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ShowDescSowComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
