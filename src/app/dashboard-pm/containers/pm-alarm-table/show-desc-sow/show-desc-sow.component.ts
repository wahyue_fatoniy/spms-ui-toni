import { Component, OnInit, Inject } from '@angular/core';
import { MAT_DIALOG_DATA } from '@angular/material';
import { SowType } from 'src/app/models/sow-type.model';

@Component({
  selector: 'app-show-desc-sow',
  templateUrl: './show-desc-sow.component.html',
  styleUrls: ['./show-desc-sow.component.css']
})
export class ShowDescSowComponent implements OnInit {

  constructor(
    @Inject(MAT_DIALOG_DATA) public data: SowType
  ) { }

  ngOnInit() {
  }

}
