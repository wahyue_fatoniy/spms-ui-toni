import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ShowDescTaskComponent } from './show-desc-task.component';

describe('ShowDescTaskComponent', () => {
  let component: ShowDescTaskComponent;
  let fixture: ComponentFixture<ShowDescTaskComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ShowDescTaskComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ShowDescTaskComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
