import { Component, OnInit, Inject } from '@angular/core';
import { MAT_DIALOG_DATA } from '@angular/material';
import { Task } from 'src/app/models/task.model';

@Component({
  selector: 'app-show-desc-task',
  templateUrl: './show-desc-task.component.html',
  styleUrls: ['./show-desc-task.component.css']
})
export class ShowDescTaskComponent implements OnInit {

  constructor(
    @Inject(MAT_DIALOG_DATA) public data: Task
  ) { }

  ngOnInit() {
  }

}
