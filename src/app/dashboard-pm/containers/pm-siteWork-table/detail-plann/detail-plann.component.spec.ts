import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DetailPlannComponent } from './detail-plann.component';

describe('DetailPlannComponent', () => {
  let component: DetailPlannComponent;
  let fixture: ComponentFixture<DetailPlannComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DetailPlannComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DetailPlannComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
