import { Component, OnInit, Inject, OnDestroy } from '@angular/core';
import { SiteWork } from 'src/app/models/siteWork.model';
import { DailyReportService } from 'src/service/daily-report-service/daily-report.service';
import { MAT_DIALOG_DATA } from '@angular/material';
import { DailyReport } from 'src/app/models/daliyReport.model';
import { Subscription } from 'rxjs';
import { Task } from 'src/app/models/task.model';

export interface FormDetailPlann {
  siteWork: SiteWork;
  dailyService: DailyReportService;
}

@Component({
  selector: 'app-detail-plann',
  templateUrl: './detail-plann.component.html',
  styleUrls: ['./detail-plann.component.css']
})
export class DetailPlannComponent implements OnInit, OnDestroy {
  displayedColumns: string[] = ['ed', 'td', 'dd'];
  tableData = [ {
      actualEd: 0,
      plannEd: 0,
      actualTd: 0,
      plannTd: 0,
      actualDd: 0,
      plannDd: 0
    }
  ];
  subscription: Subscription;
  constructor(
    @Inject(MAT_DIALOG_DATA) public data: SiteWork,
    public dailyService: DailyReportService
  ) {
  }

  ngOnInit() {
    this.getPlannMandays();
    this.getActualMandays();
  }

  loading() {
    return this.dailyService.getLoading;
  }

  getActualMandays() {
    this.dailyService.getBySiteWorkId(this.data.getId).then((val: DailyReport[]) => {
      val.forEach(dailyReport => dailyReport.getTaskExecutions.forEach(taskExecution => {
          if (taskExecution.getTask.getActivity.getType === 'ENGINEER') {
            this.tableData[0].actualEd += taskExecution.getManHour;
            this.tableData[0].actualEd = parseFloat((this.tableData[0].actualEd / 8).toFixed(2));
          } else if (taskExecution.getTask.getActivity.getType === 'TECHNICIAN') {
            this.tableData[0].actualTd += taskExecution.getManHour;
            this.tableData[0].actualTd = parseFloat((this.tableData[0].actualTd / 8).toFixed(2));
          } else {
            this.tableData[0].actualDd += taskExecution.getManHour;
            this.tableData[0].actualDd = parseFloat((this.tableData[0].actualDd / 8).toFixed(2));
          }
        })
      );
    });
  }

  getPlannMandays() {
    // const sows = this.data.getTasks.filter((task: Task, i, arr) => {
    //   return i === arr.findIndex((val: Task) => val.getSow.getId === task.getSow.getId);
    // }).map(task => task.getSow.getSowType);
    // sows.forEach(sowType => {
    //   this.tableData[0].plannEd += sowType.getEngineerday;
    //   this.tableData[0].plannTd += sowType.getTechnicianday;
    //   this.tableData[0].plannDd += sowType.getDocumentationday;
    //   this.tableData[0].plannEd = parseFloat(this.tableData[0].plannEd.toFixed(2));
    //   this.tableData[0].plannTd = parseFloat(this.tableData[0].plannTd.toFixed(2));
    //   this.tableData[0].plannDd = parseFloat(this.tableData[0].plannDd.toFixed(2));
    // });
  }

  ngOnDestroy() {
    if (this.subscription) {
      this.subscription.unsubscribe();
    }
  }

}
