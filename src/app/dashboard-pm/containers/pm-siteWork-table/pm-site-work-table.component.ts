import { Component, OnInit, OnDestroy, ViewChild } from "@angular/core";
import { MatDialog, MatPaginator, MatSort } from "@angular/material";
import { Router } from "@angular/router";
import { PmSiteWorkValueComponent } from "./pm-site-work-value/pm-site-work-value.component";
import { takeUntil } from "rxjs/operators";
import { AAAService } from "src/app/shared/aaa/aaa.service";
import { SiteWorkService } from "src/service/siteWork-service/site-work.service";
import { SiteWork } from "src/app/models/siteWork.model";
import { DailyReportService } from "src/service/daily-report-service/daily-report.service";
import { DailyReport } from "src/app/models/daliyReport.model";
import { DateDurationPipe } from "src/app/pipes/dateDuration/date-duration.pipe";
import { FilterPercentagePipe } from "src/app/pipes/filter-percentage/filter-percentage.pipe";
import { SowService } from "src/service/sow-service/sow.service";
import { Severity, projectStatus } from "src/service/enum";
import { TaskExecution } from "src/app/models/taskExecution.model";
import { Document } from "src/app/models/document.model";
import { XlsExportPipe } from "src/app/pipes/xlsExport/xls-export.pipe";
import { XlsProject } from "src/app/models/xls-project.model";
import { DomSanitizer, SafeResourceUrl } from "@angular/platform-browser";
import { TableConfig, CoreTable } from "src/app/shared/core-table";
import { FilterDataPipe } from "src/app/pipes/filterData/filter-data.pipe";
import { FormStartProjectComponent } from "../../presentational/form-start-project/form-start-project.component";
import { combineLatest } from "rxjs";
import { CoordinatorService } from "src/service/coordinator-service/coordinator.service";
import { Coordinator } from "src/app/models/coordinator.model";
import { DialogComponent } from "src/app/shared/dialog/dialog.component";
import { Request, RequestType, RequestStatus } from "src/app/models/request.model";
import { RequestService } from "src/service/request-service/request.service";
import { ProjectManager } from "src/app/models/projectManager.model";
import { ProjectManagerService } from "src/service/projectManager-service/project-manager.service";
import { ContractService } from "src/service/contract-service/contract.service";
import { ContractStatus, Contract } from "src/app/models/contract.model";

const CONFIG: TableConfig = {
  title: "Project",
  columns: [
    "no",
    "projectCode",
    // "site",
    "duration",
    "totalOpex",
    "ed",
    "td",
    "dd",
    "bast",
    "status",
    "severity",
    "startDate",
    "endDate",
  ],
  typeFilters: [
    { value: "all", label: "All" },
    { value: "projectCode", label: "Project ID" },
    // { value: "site", label: "Site" },
    { value: "startDate", label: "Start Date" },
    { value: "endDate", label: "End Date" },
    { value: "status", label: "Status" },
    { value: "duration", label: "Duration" },
    { value: "severity", label: "Severity" },
    { value: "totalOpex", label: "Total Opex" }
  ]
};

enum poStatus {
  RELEASE = "RELEASE",
  NOT_RELEASE = "NOT_RELEASE"
}

type OpexStatus = "<100" | ">100" | "100";

interface StorageFilterData {
  opex: string;
  status: projectStatus[];
  severity: Severity;
  po: poStatus;
}

type filterType = "severity" | "status" | "poStatus" | "opex";

interface Filter {
  type: filterType;
  source: SiteWork[];
  value: any;
}

@Component({
  selector: "app-pm-site-work-table",
  templateUrl: "./pm-site-work-table.component.html",
})
export class PmSiteWorkTableComponent extends CoreTable<SiteWork>
  implements OnInit, OnDestroy {
  @ViewChild(MatPaginator)
  paginator: MatPaginator;
  @ViewChild(MatSort)
  sort: MatSort;
  public sourceData: SiteWork[] = [];
  public dataPipe: FilterDataPipe = new FilterDataPipe();
  public statusType: projectStatus[] = [
    projectStatus.NOT_STARTED,
    projectStatus.ON_PROGRESS,
    projectStatus.DONE,
    projectStatus.CANCEL
  ];
  public statusLabel = {
    NOT_STARTED: "Not Started",
    ON_PROGRESS: "On Progress",
    DONE: "Done",
    CANCEL: "Cancel"
  };

  public severityStatus: Severity[] = [
    Severity.NORMAL,
    Severity.MAJOR_E,
    Severity.MAJOR_I,
    Severity.CRITICAL_E,
    Severity.CRITICAL_I
  ];
  public severityLabel = {
    NONE: "None",
    NORMAL: "Normal",
    CRITICAL_I: "Critical I",
    CRITICAL_E: "Critical E",
    MAJOR_I: "Major I",
    MAJOR_E: "Major E"
  };
  public poStatus: poStatus[] = [poStatus.RELEASE, poStatus.NOT_RELEASE];
  public ngModelOpex: OpexStatus = "<100";
  public ngModelStatus: projectStatus[] = [projectStatus.ON_PROGRESS];
  public ngModelSeverity: Severity = Severity.NORMAL;
  public ngModelPoStatus: poStatus = poStatus.RELEASE;
  public poStatusLabel = {
    RELEASE: "Release",
    NOT_RELEASE: "Not Release"
  };
  public coordinators: Coordinator[];
  public durationPipe: DateDurationPipe = new DateDurationPipe();
  public percentagePipe: FilterPercentagePipe = new FilterPercentagePipe();
  public xlsExport: XlsExportPipe = new XlsExportPipe();
  public resourceUrl: SafeResourceUrl;
  public actionTypes: string[];
  public selectField: boolean;
  public selectData: any;
  public rowIndex: number;

  constructor(
    public siteworkService: SiteWorkService,
    public dialog: MatDialog,
    public router: Router,
    public pmService: ProjectManagerService,
    public aaaService: AAAService,
    public dailyService: DailyReportService,
    public sowService: SowService,
    public domSanitizer: DomSanitizer,
    public coordinatorService: CoordinatorService,
    public contractService: ContractService,
    public requestService: RequestService,
  ) {
    super(CONFIG);
  }

  ngOnInit() {
    this.actionTypes = ['DETAIL', 'REQUEST', 'START']
    this.getStorage();
    this.setDataSource();
    this.getServiceData();
  }

  ngOnDestroy() {
    this.destroy$.next(true);
    this.destroy$.complete();
  }

  getStorage() {
    let projectFilter = JSON.parse(localStorage.getItem("projectFilter"));
    if (projectFilter !== null) {
      this.ngModelOpex = projectFilter["opex"];
      this.ngModelPoStatus = projectFilter["po"];
      this.ngModelSeverity = projectFilter["severity"];
      this.ngModelStatus = projectFilter["status"];
    }
  }


  getPMAuth(projectManagers: ProjectManager[]) {
    projectManagers = projectManagers.filter(_set => {
      const region = _set.getRegion ? _set.getRegion : null;
      const id = region ? region.getId : null;
      return !this.aaaService.isAuthorized('read:project:{province:$}:{job:$}') ||
        this.aaaService.isAuthorized(`read:project:{province:${id}}:{job:${_set.getId}}`);
    });
    return projectManagers;
  }

  public getServiceData() {
    combineLatest(
      this.pmService
        .getData(),
      this.coordinatorService.getData(),
      this.contractService.findByStatus(ContractStatus.APPROVED)
      )
      .pipe(takeUntil(this.destroy$))
      .subscribe(val => {
        this.siteworkService.findByPMs(this.getPMAuth([])).subscribe();
      });
  }

  public setDataSource(): void {
    combineLatest(
      this.siteworkService
        .subscriptionData(),
      this.coordinatorService.subscriptionData(),
      this.contractService.subscriptionData()
      )
      .pipe(takeUntil(this.destroy$))
      .subscribe((val: [SiteWork[], Coordinator[], Contract[]]) => {
        let siteWorks = [];
        val[2].map((contract, i) => {
          siteWorks = [...siteWorks, ...contract.getSiteworks]
        });
        this.coordinators = val[1];
        this.filterDataSource();
        this.sourceData = siteWorks;
        this.sourceData = this.setOpexPercentage(this.sourceData);
        this.sourceData = this.setActualMandays(this.sourceData);
        this.sourceData = this.setPlanMandays(this.sourceData);
        this.sourceData = this.setSiteWorkDuration(this.sourceData);
        this.datasource.data = this.sourceData;
        console.log(this.sourceData);
        this.datasource.paginator = this.paginator;
        this.datasource.sort = this.sort;
        this.filterDataSource();
      });
  }

  public setBastStatus(row: Document[]): string {
    if (row) {
      let bast = row.find(doc => doc.getType === "BAST");
      return bast ? bast.getStatus : "None";
    }
  }

  public setOpexPercentage(siteWorks: SiteWork[]): SiteWork[] {
    siteWorks = siteWorks.map((siteWork: SiteWork) => {
      const poPercentage = parseFloat(
        ((25 / 100) * siteWork.getPoValue).toFixed(2)
      );
      siteWork["percentageOpex"] = 0;
      if (poPercentage !== 0) {
        siteWork["percentageOpex"] = Math.floor(
          (siteWork.getTotalOpex / poPercentage) * 100
        );
      }
      return siteWork;
    });
    return siteWorks;
  }

  public setSiteWorkDuration(siteWorks: SiteWork[]): SiteWork[] {
    siteWorks = siteWorks.map((siteWork: SiteWork) => {
      siteWork["duration"] = this.durationPipe.transform(siteWork.getStartDate);
      return siteWork;
    });
    return siteWorks;
  }

  public setActualMandays(siteWorks: SiteWork[]): SiteWork[] {
    siteWorks = siteWorks.map((siteWork: SiteWork) => {
      this.dailyService
        .getBySiteWorkId(siteWork.getId)
        .then((dailys: DailyReport[]) => {
          siteWork["actualEd"] = 0;
          siteWork["actualTd"] = 0;
          siteWork["actualDd"] = 0;
          dailys.forEach((daily: DailyReport) => {
            daily.getTaskExecutions.forEach((task: TaskExecution) => {
              switch (task.getResource["resourceType"]) {
                case "ENGINEER":
                  siteWork["actualEd"] += task.getManHour;
                  break;
                case "TECHNICIAN":
                  siteWork["actualTd"] += task.getManHour;
                  break;
                case "DOCUMENTATION":
                  siteWork["actualDd"] += task.getManHour;
                  break;
              }
            });
          });
        });
      return siteWork;
    });
    return siteWorks;
  }

  public setPlanMandays(siteWorks: SiteWork[]): SiteWork[] {
    siteWorks = siteWorks.map((siteWork: SiteWork) => {
      this.sowService
        .getSowHistories(siteWork.getId)
        .pipe(takeUntil(this.destroy$))
        .subscribe((history: Object[]) => {
          siteWork["planEd"] = 0;
          siteWork["planTd"] = 0;
          siteWork["planDd"] = 0;
          history.forEach(sow => {
            siteWork["planEd"] += sow["sow"]["engineerDay"];
            siteWork["planTd"] += sow["sow"]["technicianDay"];
            siteWork["planDd"] += sow["sow"]["documentationDay"];
          });
        });
      return siteWork;
    });
    return siteWorks;
  }

  private setStorage(): void {
    let projectFilter: StorageFilterData = {
      severity: this.ngModelSeverity,
      status: this.ngModelStatus,
      po: this.ngModelPoStatus,
      opex: this.ngModelOpex
    };
    localStorage.setItem("projectFilter", JSON.stringify(projectFilter));
  }

  public filterDataSource(): void {
    this.search.valueChanges
      .pipe(takeUntil(this.destroy$))
      .subscribe(val => (this.datasource.filter = val));
    this.datasource.filterPredicate = (value: SiteWork, keyword: string) => {
      return this.dataPipe.transform(value, keyword, this.typeFilter);
    };
  }

  public onFilter(event: Filter): void {
    let datasource: SiteWork[] = [];
    if (event.type === "status") {
      datasource = this.siteworkService.getPriority(event.value, event.source);
    } else if (event.type === "opex") {
      datasource = this.percentagePipe.transform(
        event.value,
        "percentageOpex",
        event.source
      );
    } else if (event.type === "severity") {
      // datasource = event.source.filter(siteWork => {
      //   return siteWork.getSeverity === event.value;
      // });
    } else if (event.type === "poStatus") {
      datasource = this.filterPoStatus(event.value, event.source);
    }
    datasource = this.filterAll(datasource);
    this.datasource.data = datasource;
    this.setStorage();
  }

  public filterAll(datasource: SiteWork[]): SiteWork[] {
    // filter project status
    datasource = this.siteworkService.getPriority(this.ngModelStatus, datasource);
    // filter opex project
    datasource = this.percentagePipe.transform(
      this.ngModelOpex,
      "percentageOpex",
      datasource
    );
    // filter po status
    datasource = this.filterPoStatus(this.ngModelPoStatus, datasource);
    // filter severity
    // datasource = datasource.filter(siteWork => {
    //   return siteWork.getSeverity === this.ngModelSeverity;
    // });
    return datasource;
  }

  public filterPoStatus(valInput: poStatus, siteWorks: SiteWork[]): SiteWork[] {
    return siteWorks.filter(siteWork => {
      if (valInput === poStatus.RELEASE) {
        return siteWork.getPoValue !== 0;
      } else {
        return siteWork.getPoValue === 0;
      }
    });
  }

  public sortData(sort): void {
    this.datasource.data = this.datasource.data.sort((a, b) => {
      const isAsc = sort.direction === "asc";
      switch (sort.active) {
        case "projectCode":
          return this.compare(a.getProjectCode, b.getProjectCode, isAsc);
        case "site":
          return this.compare(a.getSite.getName, b.getSite.getName, isAsc);
        case "startDate":
          return this.compare(a.getStartDate, b.getStartDate, isAsc);
        case "endDate":
          return this.compare(a.getEndDate, b.getEndDate, isAsc);
        case "status":
          return this.compare(a.getStatus, b.getStatus, isAsc);
        default:
          return 0;
      }
    });
  }

  // public showDetailPlann(data: SiteWork): void {
  //   this.dialog.open(DetailPlannComponent, {
  //     data: new SiteWork(data),
  //     width: "1000px",
  //     disableClose: true
  //   });
  // }

  public setInternalReport() {
    if (this.datasource.data.length !== 0) {
      let projectData = this.datasource.data.map((sitework: SiteWork) => {
        return new XlsProject(sitework);
      });
      this.xlsExport.transform("pm_report.xls", projectData);
    }
  }
  
  public setExternalReport() {
    const siteworkIds = this.datasource.data.map(data => data.getId);
    if (siteworkIds.length !== 0) {
      this.siteworkService.getReport(siteworkIds).subscribe(blobFile => {
        const objectURL = window.URL.createObjectURL(blobFile);
        let elementHref = document.createElement("a");
        elementHref.href = objectURL;
        elementHref.download = "customer_report.xlsx";
        document.body.appendChild(elementHref);
        elementHref.click();
      });
    }
  }

  onSelectRow(element: SiteWork) {
    this.selectField = true;
    this.selectData = element;
    if (element.getStartDate) {
      this.actionTypes = this.actionTypes.filter(type => type !== 'START')
    }

    if (element.getProjectCode) {
      this.actionTypes = this.actionTypes.filter(type => type !== 'REQUEST')
    }
  }

  onEventClick(event) {
    switch (event) {
      case 'DETAIL':
        this.showDetailValue(this.selectData);
        break;
      case 'REQUEST':
        this.requestProjectId(this.selectData);
        break;
      case 'START':
        this.startProject(this.selectData);
        break;
    }
  }

  public startProject(data: SiteWork) {
    this.dialog.open(FormStartProjectComponent, {
      width: '900px',
      data: { data: data, coordinators: this.coordinators },
      disableClose: true
    }).afterClosed().subscribe(val => {
      console.log(val);
    });
  }

  public requestProjectId(data: SiteWork) {
    this.dialog.open(DialogComponent, {
      width: '500px',
      data: { id: data.getId, message: `Are you sure to request id project ${data.getSite.getName}?` }
    }).afterClosed().subscribe(val => {
      if (val) {
        this.createRequestPID(data);
      }
    });
  }

  createRequestPID(val: SiteWork) {
    const request = new Request();
    request.setObject = val;
    request.setStartDate = new Date();
    request.setRequester = this.getPMAuth([])[0];
    request.setType = RequestType.requested_project_id;
    request.setStatus = RequestStatus.proposed;
    this.requestService.create(request).then(val => {
      console.log(val);
    }).catch(err => {
      console.log(err);
    });
  }

  public showDetailValue(data: SiteWork): void {
    this.dialog.open(PmSiteWorkValueComponent, {
      data: data,
      width: "500px"
    }).afterClosed().subscribe(val => {
      console.log(val);
    });
  }
}
