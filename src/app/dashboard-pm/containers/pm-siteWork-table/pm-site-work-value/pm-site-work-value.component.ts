import { Component, OnInit, Inject } from '@angular/core';
import { MAT_DIALOG_DATA } from '@angular/material';
import { SiteWork } from '../../../../models/siteWork.model';

@Component({
  selector: 'app-pm-site-work-value',
  templateUrl: './pm-site-work-value.component.html',
  styles: ['']
})
export class PmSiteWorkValueComponent implements OnInit {
  public poValue: number;
  public estimasiPO: number;
  constructor(
    @Inject(MAT_DIALOG_DATA) public data: SiteWork
  ) { }

  ngOnInit() {
    this.poValue = this.data.getPoValue ? this.data.getPoValue : 0;
    this.estimasiPO = this.data.getEstimateValue ? this.data.getEstimateValue : 0;
  }

}
