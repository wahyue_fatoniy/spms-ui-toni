import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PmDescTaskComponent } from './pm-desc-task.component';

describe('PmDescTaskComponent', () => {
  let component: PmDescTaskComponent;
  let fixture: ComponentFixture<PmDescTaskComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PmDescTaskComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PmDescTaskComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
