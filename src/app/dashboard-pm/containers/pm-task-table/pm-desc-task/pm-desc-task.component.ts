import { Component, OnInit, Inject } from '@angular/core';
import { MAT_DIALOG_DATA } from '@angular/material';
import { Task } from 'src/app/models/task.model';

@Component({
  selector: 'app-pm-desc-task',
  templateUrl: './pm-desc-task.component.html',
  styleUrls: ['./pm-desc-task.component.css']
})
export class PmDescTaskComponent implements OnInit {
  public type = {
    MOS: 'Mos',
    CREATE_ATP: 'Create Atp',
    SUBMIT_ATP: 'Submit Atp',
    ATP: 'Atp',
    CREATE_COMPLETENESS_ATP: 'Create Completeness Atp',
    SUBMIT_COMPLETENESS_ATP: 'Submit Completeness Atp',
    CREATE_BAUT: 'Create Baut',
    SUBMIT_BAUT: 'Submit Baut',
    INSTALLATION: 'Installation',
    INTEGRATION: 'Integration',
    TAKE_DATA: 'Take Data'
  };
  constructor(
    @Inject(MAT_DIALOG_DATA) public data: Task
  ) { }

  ngOnInit() {
  }

}
