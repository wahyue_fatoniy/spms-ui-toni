import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PmTaskFormComponent } from './pm-task-form.component';

describe('PmTaskFormComponent', () => {
  let component: PmTaskFormComponent;
  let fixture: ComponentFixture<PmTaskFormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PmTaskFormComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PmTaskFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
