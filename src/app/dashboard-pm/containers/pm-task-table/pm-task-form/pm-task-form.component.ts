import { Component, OnInit, Inject, OnDestroy } from '@angular/core';
import { MAT_DIALOG_DATA } from '@angular/material';
import {
  FormBuilder,
  Validators,
  FormControl,
} from '@angular/forms';
import { combineLatest, Subject } from 'rxjs';
import { SowTypeService } from '../../../../../service/sowType-service/sow-type.service';
import { SiteWorkService } from '../../../../../service/siteWork-service/site-work.service';
import { SowType } from '../../../../models/sow-type.model';
import { SiteWork } from '../../../../models/siteWork.model';
import { takeUntil } from 'rxjs/operators';

@Component({
  selector: 'app-pm-task-form',
  templateUrl: './pm-task-form.component.html',
  styleUrls: ['./pm-task-form.component.css']
})
export class PmTaskFormComponent implements OnInit, OnDestroy {
  public sowTypeSource: SowType[] = [];
  public availableSowType: SowType[] = [];
  public sowType: FormControl;
  private destroy$: Subject<boolean> = new Subject();
  public description = null;

  constructor(
    @Inject(MAT_DIALOG_DATA)
    public formBuilder: FormBuilder,
    public siteWorkService: SiteWorkService,
    public sowTypeService: SowTypeService
  ) {}

  ngOnInit() {
    this.sowType = new FormControl(null, [Validators.required]);
    this.getDataService();
    this.onSetDescription();
  }

  ngOnDestroy() {
    this.destroy$.next(true);
    this.destroy$.unsubscribe();
  }

  private getDataService(): void {
    combineLatest(
      this.siteWorkService.subscriptionData(),
      this.sowTypeService.subscriptionData()
    )
    .pipe(takeUntil(this.destroy$))
    .subscribe((val: [SiteWork[], SowType[]]) => {
      this.sowTypeSource = val[1];
      if (val[0].length !== 0) {
        this.filterSowType(val[0][0]);
      }
    });
  }

  private filterSowType(siteWork: SiteWork): void {
    if (siteWork) {
      this.availableSowType = this.sowTypeSource.filter(sowType => {
        return sowType.getRegion.getId === siteWork.getSite.getRegion.getId;
      });
    }
  }

  onSetDescription() {
    this.sowType.valueChanges.subscribe((val: SowType) => {
      this.description = val.getDescription;
    });
  }

}
