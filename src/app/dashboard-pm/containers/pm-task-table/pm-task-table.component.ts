import { Component, OnInit, ViewChild, OnDestroy } from "@angular/core";
import { MatPaginator, MatSort, MatDialog } from "@angular/material";
import { CoreTable, TableConfig } from "../../../shared/core-table";
import { PmTaskFormComponent } from "./pm-task-form/pm-task-form.component";
import { takeUntil } from "rxjs/operators";
import { FilterDataPipe } from "../../../pipes/filterData/filter-data.pipe";
import { SiteWork } from "../../../models/siteWork.model";
import { Task } from "../../../models/task.model";
import { SowTypeService } from "../../../../service/sowType-service/sow-type.service";
import { TaskService } from "../../../../service/task-service/task.service";
import { SiteWorkService } from "../../../../service/siteWork-service/site-work.service";
import { ToastrService } from "ngx-toastr";
import { combineLatest } from "rxjs";
import { ActivatedRoute } from "@angular/router";
import { PmDescTaskComponent } from "./pm-desc-task/pm-desc-task.component";
import { FormControl } from "@angular/forms";

const CONFIG: TableConfig = {
  title: "Task",
  columns: ["no", "activity", "startDate", "endDate", "status", "severity"],
  typeFilters: [
    { value: "all", label: "All" },
    { value: "activity", label: "activity" },
    { value: "startDate", label: "Start Date" },
    { value: "endDate", label: "End Date" },
    { value: "status", label: "Status" },
    { value: "severity", label: "Severity" }
  ]
};

@Component({
  selector: "app-pm-task-table",
  templateUrl: "./pm-task-table.component.html",
  styleUrls: ["./pm-task-table.scss"]
})
export class PmTaskTableComponent extends CoreTable<Task>
  implements OnInit, OnDestroy {
  @ViewChild(MatPaginator)
  paginator: MatPaginator;
  @ViewChild(MatSort)
  sort: MatSort;
  public filterDataPipe: FilterDataPipe = new FilterDataPipe();
  public severityLabel = {
    CRITICAL_I: "Critical I",
    CRITICAL_E: "Critical E",
    MAJOR_I: "Major I",
    MAJOR_E: "Major E",
    NORMAL: "Normal"
  };
  public severityValue = [
    "CRITICAL_I",
    "CRITICAL_E",
    "MAJOR_I",
    "MAJOR_E",
    "NORMAL"
  ];
  public formFilterSeverity = new FormControl();
  public sourceTasks: Task[] = [];
  public siteworkId: number;
  constructor(
    public dialog: MatDialog,
    public siteWorkService: SiteWorkService,
    public taskService: TaskService,
    public sowTypeService: SowTypeService,
    public toast: ToastrService,
    public activeRoute: ActivatedRoute
  ) {
    super(CONFIG);
  }

  ngOnInit() {
    this.getRouterParam();
    this.subscriptionTasks();
    this.getServiceData();
    this.filterBySeverity();
  }

  ngOnDestroy() {
    this.destroy$.next(true);
    this.destroy$.unsubscribe();
  }

  public get loading(): boolean {
    return this.taskService.getLoading;
  }

  public getRouterParam(): void {
    this.activeRoute.params.pipe(takeUntil(this.destroy$)).subscribe(param => {
      if (param) {
        this.siteworkId = parseInt(param.id);
      }
    });
  }

  public getServiceData(): void {
    combineLatest(
      this.siteWorkService.findById(this.siteworkId),
      this.taskService.findBySiteWorkId(this.siteworkId)
    )
      .pipe(takeUntil(this.destroy$))
      .subscribe((val: [SiteWork, Task[]]) => {
        this.sowTypeService
          .findByRegions([val[0].getSite.getRegion])
          .pipe(takeUntil(this.destroy$))
          .subscribe();
      });
  }

  public subscriptionTasks(): void {
    this.taskService
      .subscriptionData()
      .pipe(takeUntil(this.destroy$))
      .subscribe((tasks: Task[]) => {
        this.sourceTasks = tasks;
        this.datasource.paginator = this.paginator;
        this.datasource.sort = this.sort;
        this.formFilterSeverity.setValue([
          "CRITICAL_I",
          "CRITICAL_E",
          "NORMAL"
        ]);
        this.filterDataSource();
      });
  }

  public filterDataSource(): void {
    this.search.valueChanges
      .pipe(takeUntil(this.destroy$))
      .subscribe(keyword => {
        this.datasource.filter = keyword;
      });

    this.datasource.filterPredicate = (data: Task, keyword: string) => {
      const dataFilter = {
        activity: data.getActivity.getName,
        startDate: data.getStartDate,
        endDate: data.getEndDate,
        // severity: data.getSeverity,
        status: data.getStatus
      };
      return this.filterDataPipe.transform(
        dataFilter,
        keyword,
        this.typeFilter
      );
    };
  }

  public sortData(sort): void {
    this.datasource.filteredData = this.datasource.filteredData.sort((a, b) => {
      const isAsc = sort.direction === "asc";
      switch (sort.active) {
        case "activity":
          return this.compare(
            a.getActivity.getName,
            b.getActivity.getName,
            isAsc
          );
        case "startDate":
          return this.compare(a.getStartDate, b.getStartDate, isAsc);
        case "endDate":
          return this.compare(a.getEndDate, b.getEndDate, isAsc);
        case "status":
          return this.compare(a.getStatus, b.getStatus, isAsc);
        case "severiy":
          return;
          // return this.compare(a.getSeverity, b.getSeverity, isAsc);
        default:
          return 0;
      }
    });
  }

  public onCreate(): void {
    const dialog = this.dialog.open(PmTaskFormComponent, {
      width: "1500px"
    });

    dialog
      .afterClosed()
      .pipe(takeUntil(this.destroy$))
      .subscribe(val => {
        if (val) {
          this.taskService
            .createTask(this.siteworkId, val.getId)
            .then(() => {
              this.siteWorkService
                .findById(this.siteworkId)
                .pipe(takeUntil(this.destroy$))
                .subscribe();
            })
            .catch(() => {});
        }
      });
  }

  public showDescTask(task: Task): void {
    this.dialog.open(PmDescTaskComponent, {
      data: new Task(task),
      width: "1800px"
    });
  }

  public filterBySeverity(): void {
    // this.formFilterSeverity.valueChanges
    //   .pipe(takeUntil(this.destroy$))
    //   .subscribe((val: string[]) => {
    //     this.datasource.data = this.sortSeverity(
    //       this.sourceTasks.filter(task => {
    //         return val.find(severity => severity === task.getSeverity);
    //       })
    //     );
    //   });
  }

  public sortSeverity(tasks: Task[]): Task[] {
    let internalCritical = [];
    let eksternalCritical = [];
    let internalMajor = [];
    let eksternalMajor = [];
    let normal = [];
    // internalCritical = tasks.filter(task => {
    //   return task.getSeverity === "CRITICAL_I";
    // });
    // eksternalCritical = tasks.filter(task => {
    //   return task.getSeverity === "CRITICAL_E";
    // });
    // internalMajor = tasks.filter(task => {
    //   return task.getSeverity === "MAJOR_I";
    // });
    // eksternalMajor = tasks.filter(task => {
    //   return task.getSeverity === "MAJOR_E";
    // });
    // normal = tasks.filter(task => {
    //   return task.getSeverity === "NORMAL";
    // });
    return [
      ...internalCritical,
      ...eksternalCritical,
      ...internalMajor,
      ...eksternalMajor,
      ...normal
    ];
  }
}
