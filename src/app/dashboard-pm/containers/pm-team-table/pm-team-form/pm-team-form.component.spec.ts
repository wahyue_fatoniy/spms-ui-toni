import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PmTeamFormComponent } from './pm-team-form.component';

describe('PmTeamFormComponent', () => {
  let component: PmTeamFormComponent;
  let fixture: ComponentFixture<PmTeamFormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PmTeamFormComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PmTeamFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
