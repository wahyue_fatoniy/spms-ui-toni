import { Component, OnInit, Inject } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material';
import { combineLatest, Subscription } from 'rxjs';
import { Engineer } from 'src/app/models/engineer.model';
import { SiteWork } from 'src/app/models/siteWork.model';
import { Technician } from 'src/app/models/technician.model';
import { Documentation } from 'src/app/models/documentation.model';
import { Coordinator } from 'src/app/models/coordinator.model';
import { EngineerService } from 'src/service/engineer-service/engineer.service';
import { TechnicianService } from 'src/service/technician-service/technician.service';
import { DocumentationService } from 'src/service/documentation-service/documentation.service';
import { CoordinatorService } from 'src/service/coordinator-service/coordinator.service';
import { Team } from 'src/app/models/team.model';
import { Resource } from 'src/app/models/resource.model';

export interface TeamDialogData {
  team: Team;
  siteWork: SiteWork;
}

@Component({
  selector: 'app-pm-team-form',
  templateUrl: './pm-team-form.component.html',
  styleUrls: ['./pm-team-form.component.css']
})
export class PmTeamFormComponent implements OnInit {
  public form: FormGroup;
  public engineerData: Engineer[] = [];
  public technicianData: Technician[] = [];
  public documentationData: Documentation[] = [];
  public picData:  Coordinator[] = [];
  public subscriptions: Subscription;

  constructor(
    @Inject(MAT_DIALOG_DATA) public teamDialogData: TeamDialogData,
    public matDialogRef: MatDialogRef<PmTeamFormComponent>,
    public formBuilder: FormBuilder,
    public engineerService: EngineerService,
    public technicianService: TechnicianService,
    public documentService: DocumentationService,
    public coordinatorService: CoordinatorService,
  ) { }

  ngOnInit() {
    console.log(this.teamDialogData);
    this.form = this.formBuilder.group({
      cordinator: [this.teamDialogData.team.getCoordinator, Validators.required],
      engineer: [this.teamDialogData.team.getResources
        .filter((resource: Resource) => resource['resourceType'] === 'ENGINEER')],
      technician: [this.teamDialogData.team.getResources
        .filter((resource: Resource) => resource['resourceType'] === 'TECHNICIAN')],
      documentation: [this.teamDialogData.team.getResources
        .filter((resource: Resource) => resource['resourceType'] === 'DOCUMENTATION')]
    });
    this.getServiceData();
    this.onFormValid();
    this.onBeforeClose();
  }

  private getServiceData(): void {
   this.subscriptions = combineLatest(
      this.coordinatorService.subscriptionData(),
      this.engineerService.subscriptionData(),
      this.technicianService.subscriptionData(),
      this.documentService.subscriptionData(),
    ).subscribe((valRole: [Coordinator[], Engineer[], Technician[], Documentation[]]) => {
      this.picData = valRole[0];
      this.engineerData = valRole[1];
      this.technicianData = valRole[2];
      this.documentationData = valRole[3];
    });
  }

  private onFormValid(): void {
    this.form.statusChanges.subscribe(status => {
      if (status === 'VALID') {
        this.form.value['resources'] = [...this.form.value['engineer'],
         ...this.form.value['technician'], ...this.form.value['documentation']];
        this.teamDialogData['team'] = new Team(this.form.value);
      }
    });
  }

  public onBeforeClose(): void {
    this.matDialogRef.beforeClose().toPromise().then(result => {
      this.subscriptions.unsubscribe();
    });
  }

  public compareSelectedItems(c1: any, c2: any): boolean {
    return c1 && c2 ? c1.id === c2.id : c1 === c2;
  }
}
