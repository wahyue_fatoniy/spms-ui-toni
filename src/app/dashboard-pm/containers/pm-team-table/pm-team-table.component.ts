import { Component, OnInit, ViewChild, OnDestroy } from "@angular/core";
import { TableConfig, CoreTable } from "../../../shared/core-table";
import { MatPaginator, MatSort, MatDialog } from "@angular/material";
import { PmTeamFormComponent } from "./pm-team-form/pm-team-form.component";
import { takeUntil } from "rxjs/operators";
import { ToastrService } from "ngx-toastr";
import { SiteWork } from "../../../models/siteWork.model";
import { SiteWorkService } from "../../../../service/siteWork-service/site-work.service";
import { Team } from "../../../models/team.model";
import { EngineerService } from "src/service/engineer-service/engineer.service";
import { TechnicianService } from "src/service/technician-service/technician.service";
import { CoordinatorService } from "src/service/coordinator-service/coordinator.service";
import { DocumentationService } from "src/service/documentation-service/documentation.service";
import { combineLatest } from "rxjs";
import { ActivatedRoute } from "@angular/router";

const CONFIG: TableConfig = {
  title: "Team",
  columns: ["no", "name", "address", "email", "phone", "nip"],
  typeFilters: [
    { value: "all", label: "All" },
    { value: "name", label: "Name" },
    { value: "address", label: "Address" },
    { value: "email", label: "Email" },
    { value: "phone", label: "Phone" },
    { value: "nip", label: "NIP" }
  ]
};

@Component({
  selector: "app-pm-team-table",
  templateUrl: "./pm-team-table.component.html",
  styles: [""]
})
export class PmTeamTableComponent extends CoreTable<Object>
  implements OnInit, OnDestroy {
  public siteWork: SiteWork;
  public siteWorkId: number;
  @ViewChild(MatPaginator)
  paginator: MatPaginator;
  @ViewChild(MatSort)
  sort: MatSort;
  siteworkId = null;

  constructor(
    public siteWorkService: SiteWorkService,
    public engineerService: EngineerService,
    public technicianService: TechnicianService,
    public coordinatorService: CoordinatorService,
    public docService: DocumentationService,
    public dialog: MatDialog,
    public toast: ToastrService,
    public activeRoute: ActivatedRoute
  ) {
    super(CONFIG);
  }

  ngOnInit() {
    this.getRouterParam();
    this.subscriptionSiteworks();
  }

  ngOnDestroy() {
    this.destroy$.next(true);
    this.destroy$.unsubscribe();
  }

  public get loading(): boolean {
    return this.siteWorkService.getLoading;
  }

  public getRouterParam(): void {
    this.activeRoute.params
      .pipe(takeUntil(this.destroy$))
      .subscribe((data: { id: string }) => {
        this.siteWorkId = parseInt(data.id);
      });
  }

  public getServiceData(regionId: number): void {
    combineLatest(
      this.technicianService.findByRegionId(regionId),
      this.engineerService.findByRegionId(regionId),
      this.coordinatorService.findByRegionId(regionId),
      this.docService.findByRegionId(regionId)
    )
      .pipe(takeUntil(this.destroy$))
      .subscribe(val => {
        console.log(val);
      });
  }

  public subscriptionSiteworks(): void {
    this.siteWorkService
      .subscriptionData()
      .pipe(takeUntil(this.destroy$))
      .subscribe((siteWorks: SiteWork[]) => {
        this.datasource.data = [];
        this.siteWork = new SiteWork(
          siteWorks.find(siteWork => {
            return siteWork.getId === this.siteWorkId;
          })
        );
        if (this.siteWork) {
          let regionId = this.siteWork.getSite
            ? this.siteWork.getSite.getRegion.getId
            : null;
          this.getServiceData(regionId);
          if (this.siteWork.getTeam.getCoordinator.getId) {
            this.datasource.data = [
              this.siteWork.getTeam.getCoordinator,
              ...this.siteWork.getTeam.getResources
            ];
          } else {
            this.datasource.data = this.siteWork.getTeam
              ? this.siteWork.getTeam.getResources
              : [];
          }
        }
      });
    this.datasource.paginator = this.paginator;
    this.datasource.sort = this.sort;
    this.filterDataSource();
  }

  public filterDataSource(): void {
    this.search.valueChanges
      .pipe(takeUntil(this.destroy$))
      .subscribe(val => (this.datasource.filter = val));
    this.datasource.filterPredicate = (data: Team, keyWord: string) => {
      const dataFilter = {
        name: data["party"]["name"],
        address: data["party"]["address"],
        email: data["party"]["email"],
        phone: data["party"]["phone"],
        nip: data["nip"]
      };
      return this.filterPredicate(dataFilter, keyWord, this.typeFilter);
    };
  }

  public sortData(sort): void {
    this.datasource.data = this.datasource.data.sort((a, b) => {
      const isAsc = sort.direction === "asc";
      switch (sort.active) {
        case "name":
          return this.compare(a["party"]["name"], b["party"]["name"], isAsc);
        case "address":
          return this.compare(
            a["party"]["address"],
            b["party"]["address"],
            isAsc
          );
        case "email":
          return this.compare(a["party"]["email"], b["party"]["email"], isAsc);
        case "phone":
          return this.compare(a["party"]["phone"], b["party"]["phone"], isAsc);
        case "nip":
          return this.compare(a["nip"], b["nip"], isAsc);
        default:
          return 0;
      }
    });
  }

  public onCreate(data: Object[]): void {
    const team = {
      cordinator: data.find(valData => valData["roleType"] === "CORDINATOR"),
      resources: [
        ...data.filter(valData => valData["roleType"] !== "CORDINATOR")
      ]
    };
    const dialog = this.dialog.open(PmTeamFormComponent, {
      data: { team: new Team(team), siteWork: new SiteWork(this.siteWork) },
      width: "850px"
    });

    dialog.afterClosed().subscribe((valDialog: Team) => {
      if (valDialog) {
        valDialog["id"] = this.siteWork.getTeam.getId;
        this.siteWork.setTeam = valDialog;
        this.siteWorkService
          .update(this.siteWork)
          .then()
          .catch(err => {
            console.log(err);
          });
      }
    });
  }
}
