import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PmBudgetDetailComponent } from './pm-budget-detail.component';

describe('PmBudgetDetailComponent', () => {
  let component: PmBudgetDetailComponent;
  let fixture: ComponentFixture<PmBudgetDetailComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PmBudgetDetailComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PmBudgetDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
