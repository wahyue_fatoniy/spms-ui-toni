import { Component, OnInit, Inject } from '@angular/core';
import { MAT_DIALOG_DATA } from '@angular/material';
import { WeeklyPlanning } from '../../../../models/weeklyPlanning.model';

@Component({
  selector: 'app-pm-budget-detail',
  templateUrl: './pm-budget-detail.component.html',
  styleUrls: ['./pm-budget-detail.component.css']
})
export class PmBudgetDetailComponent implements OnInit {
  public budget = 0;
  public budgetUsed = 0;
  constructor(
    @Inject(MAT_DIALOG_DATA) public data: WeeklyPlanning
  ) {
    console.log(data);
  }

  ngOnInit() {
  }

}
