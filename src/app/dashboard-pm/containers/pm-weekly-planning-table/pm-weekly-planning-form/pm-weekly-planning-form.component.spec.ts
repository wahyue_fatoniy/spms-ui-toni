import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PmWeeklyPlanningFormComponent } from './pm-weekly-planning-form.component';

describe('PmWeeklyPlanningFormComponent', () => {
  let component: PmWeeklyPlanningFormComponent;
  let fixture: ComponentFixture<PmWeeklyPlanningFormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PmWeeklyPlanningFormComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PmWeeklyPlanningFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
