import { Component, OnInit, Inject, OnDestroy } from "@angular/core";
import { FormGroup, FormBuilder, Validators } from "@angular/forms";
import { MAT_DIALOG_DATA, MatDialog } from "@angular/material";
import { LocalCurrencyPipe } from "../../../../pipes/localCurrency/local-currency.pipe";
import { NumberInStringPipe } from "../../../../pipes/numberInString/number-in-string.pipe";
import * as moment from "moment";
import { Subject } from "rxjs";
import { DatePipe } from "@angular/common";
import { SiteWorkService } from "../../../../../service/siteWork-service/site-work.service";
import { Task } from "../../../../models/task.model";
import { WeeklyPlanning } from "../../../../models/weeklyPlanning.model";
import { TaskService } from "src/service/task-service/task.service";
import { takeUntil } from "rxjs/operators";
import { PmDescTaskComponent } from "../../pm-task-table/pm-desc-task/pm-desc-task.component";

@Component({
  selector: "app-pm-weekly-planning-form",
  templateUrl: "./pm-weekly-planning-form.component.html",
  styleUrls: ["./pm-weekly-planning-form.component.css"]
})
export class PmWeeklyPlanningFormComponent implements OnInit, OnDestroy {
  public form: FormGroup;
  public sourceTask: Task[] = [];
  public availableTask: Task[] = [];
  public selectedTask: Task[] = [];
  public localCurrencyPipe: LocalCurrencyPipe = new LocalCurrencyPipe();
  public setToNumber: NumberInStringPipe = new NumberInStringPipe();
  public maxEndDate: Date;
  public activity = {
    CREATE_BAUT: "Create Baut",
    SUBMIT_BAUT: "Submit Baut",
    MOS: "Mos",
    INSTALLATION: "Installation",
    INTEGRATION: "Integration",
    TAKE_DATA: "Take Data",
    ATP: "Atp",
    CREATE_ATP: 'Create Atp',
    SUBMIT_ATP: 'Submit Atp',
    CREATE_COMPLETENESS_ATP: "Create Completeness Atp",
    SUBMIT_COMPLETENESS_ATP: "Submit Completeness Atp"
  };
  public status = {
    ON_PROGRESS: "On Progress",
    DONE: "Done",
    CANCEL: "Cancel",
    NOT_STARTED: "Not Started"
  };
  public datePipe: DatePipe = new DatePipe("id");
  public planning: WeeklyPlanning;
  public destroy$ = new Subject<boolean>();

  constructor(
    @Inject(MAT_DIALOG_DATA) public data: WeeklyPlanning,
    public formBuilder: FormBuilder,
    public siteWorkService: SiteWorkService,
    public taskService: TaskService,
    public dialog: MatDialog
  ) {
    this.planning = new WeeklyPlanning(data);
  }

  ngOnInit() {
    this.setFormValidation();
    this.onStartDateChanges();
    this.initialData();
    this.onFormValid();
  }

  ngOnDestroy() {
    this.destroy$.next(true);
    this.destroy$.complete();
  }

  public setFormValidation(): void {
    this.form = this.formBuilder.group({
      id: [this.planning.getId],
      startDate: [
        this.planning.getStartDate
          ? new Date(this.planning.getStartDate.toString())
          : null,
        Validators.required
      ],
      endDate: [
        {
          value: this.planning.getStartDate
            ? new Date(this.planning.getEndDate.toString())
            : null,
          disabled: true
        },
        Validators.required
      ],
      tasks: [this.planning.getTasks, Validators.required]
    });
  }

  public initialData(): void {
    this.taskService
      .subscriptionData()
      .pipe(takeUntil(this.destroy$))
      .subscribe((tasks: Task[]) => {
        this.sourceTask = tasks.filter(val => {
          return (
            !val.isFinished() &&
            !this.planning.getTasks.find(task => task.getId === val.getId)
          );
        }).map(val => new Task(val));
        this.selectedTask = this.planning.getTasks.map(val => new Task(val))
          ? this.planning.getTasks.filter(val => !val.isFinished()).map(val => new Task(val))
          : [];
        this.initialAvailableTask();
      });
  }

  initialAvailableTask(): void {
    this.availableTask = this.sourceTask.filter(task => {
      return task.isInitial() || task.isReady();
    });
    if (this.selectedTask.length !== 0) {
      this.availableTask = this.filterTaskReady(this.sourceTask, this.selectedTask).taskReady;
    }
    this.sourceTask = this.sourceTask.filter(val => {
      return !this.availableTask.find(
        available => available["id"] === val["id"]
      );
    });
  }

  onSelectTask(task: Task) {
    let taskIsReady: {sourceTask: Task[], taskReady: Task[]};
    /**remove data in availableTask with taskId*/
    this.availableTask = this.availableTask.filter((val: Task) => {
      return val.getId !== task.getId;
    });
    this.selectedTask = [...this.selectedTask, task];
    taskIsReady = this.filterTaskReady(this.sourceTask, this.selectedTask);
    this.sourceTask = taskIsReady.sourceTask;
    this.availableTask = [...this.availableTask, ...taskIsReady.taskReady];
    this.form.get("tasks").setValue(this.selectedTask);
  }

  onRemoveTask(value: Task) {
    let selectRemove: Task[] = this.filterTaskRemove(value, this.selectedTask);
    let availableRemove: Task[] = this.filterAvailableRemove(selectRemove, this.availableTask);
    this.selectedTask = this.selectedTask.filter(select => {
      return !selectRemove.find(remove => remove.getId === select.getId);
    });
    this.availableTask = this.availableTask.filter(available => {
      return availableRemove.findIndex(remove => available.getId === remove.getId) === -1;
    });
    selectRemove = selectRemove.filter((select, i, arr) => {
      if (
        arr.find(val => {
          return select.isRequirement(val);
        })
      ) {
        availableRemove = [...availableRemove, select];
      }
      return !arr.find(val => {
        return select.isRequirement(val);
      });
    });
    this.availableTask = [...this.availableTask, ...selectRemove];
    this.form.get("tasks").setValue(this.selectedTask);
    /**concat predecessor data to source tasks*/
    this.sourceTask = [...this.sourceTask, ...availableRemove];
  }

  filterTaskRemove(taskRemove: Task, selectedTask: Task[]): Task[] {
    let selectRemove: Task[] = [];
    selectedTask = selectedTask.filter((task: Task, i, arr) => {
      if (task.isRequirement(taskRemove)) {
        selectRemove = [...selectRemove, new Task(task)];
      }
      selectRemove.forEach(select => {
        if (task.isRequirement(select)) {
          selectRemove = [...selectRemove, new Task(task)];
        }
      });
    });
    selectRemove = [...selectRemove, taskRemove].filter((select, i, arr) => {
      return i === arr.findIndex(val => val.getId === select.getId);
    });
    return selectRemove;
  }

  filterAvailableRemove(selectRemove: Task[], availableTask: Task[]): Task[] {
    let availableRemove: Task[] = [];
    availableTask.forEach(available => {
      if (
          selectRemove.findIndex(select => available.isRequirement(select)
        ) !== -1) {
        availableRemove = [...availableRemove, available];
      }
    });
    return availableRemove;
  }

  filterTaskReady(sourceTask: Task[], selectedTask: Task[]): {sourceTask: Task[], taskReady: Task[]} {
    let i = 0;
    let taskIsReady: Task[] = [];
    sourceTask = sourceTask.filter((val: Task) => {
      i = 0;
      selectedTask.forEach(available => {
        if (val.isRequirement(available)) {
          i = i + 1;
        }
      });
      if (i === val.getRequirements.length) {
        taskIsReady = [...taskIsReady, new Task(val)];
        return false;
      }
      return true;
    });
    return {sourceTask: sourceTask, taskReady: taskIsReady };
  }

  onShowDescription(task: Task) {
    this.dialog.open(PmDescTaskComponent, {
      data: task,
      width: '1000px',
    });
  }

  public onStartDateChanges(): void {
    this.form
      .get("startDate")
      .valueChanges.pipe(takeUntil(this.destroy$))
      .subscribe(val => {
        this.maxEndDate = new Date(
          moment(new Date(val))
            .endOf("month")
            .toString()
        );
        this.form.get("endDate").enable({ onlySelf: true });
        this.form.get("endDate").reset();
      });
  }

  public onFormValid(): void {
    this.form.statusChanges.pipe(takeUntil(this.destroy$)).subscribe(status => {
      if (status === "VALID") {
        const endDate = new Date(this.form.getRawValue()["endDate"]);
        endDate.setHours(23, 59, 59, 999);
        this.planning.setStartDate = this.setUTCDate(
          this.form.value["startDate"]
        );
        this.planning.setEndDate = this.setUTCDate(endDate);
        this.planning.setTasks = this.form.value["tasks"];
      }
    });
  }

  public setUTCDate(date: Date): string {
    const utcDate = this.datePipe.transform(
      date,
      "yyyy-MM-ddTHH:mm:ssZ",
      "+0000"
    );
    return utcDate;
  }

}
