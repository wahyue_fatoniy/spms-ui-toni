import { Component, OnInit, ViewChild, OnDestroy } from "@angular/core";
import { CoreTable, TableConfig } from "../../../shared/core-table";
import { MatPaginator, MatSort, MatDialog } from "@angular/material";
import { Subject, combineLatest } from "rxjs";
import { PmWeeklyPlanningFormComponent } from "./pm-weekly-planning-form/pm-weekly-planning-form.component";
import { Task } from "../../../../models/task-service/task.model";
import { PmWeeklyPlanningTaskComponent } from "./pm-weekly-planning-task/pm-weekly-planning-task.component";
import { DialogComponent } from "../../../shared/dialog/dialog.component";
import { FilterDataPipe } from "../../../pipes/filterData/filter-data.pipe";
import { Range } from "ngx-mat-daterange-picker";
import { WeeklyPlanningServiceService } from "../../../../service/weeklyPlanning-service/weekly-planning-service.service";
import { WeeklyPlanning } from "../../../models/weeklyPlanning.model";
import { takeUntil } from "rxjs/operators";
import { ToastrService } from "ngx-toastr";
import { TaskService } from "src/service/task-service/task.service";
import { ActivatedRoute } from "@angular/router";
import { SiteWork } from "src/app/models/siteWork.model";

const CONFIG: TableConfig = {
  title: "Weekly Planning",
  columns: ["no", "startDate", "endDate", "submit", 'action'],
  typeFilters: [
    { value: "all", label: "All" },
    { value: "startDate", label: "Start Date" },
    { value: "endDate", label: "End Date" }
  ]
};

interface RangeDate {
  fromDate: Date;
  toDate: Date;
}

@Component({
  selector: "app-pm-weekly-planning-table",
  templateUrl: "./pm-weekly-planning-table.component.html",
  styleUrls: ["./pm-weekly-planning-table.component.css"]
})
export class PmWeeklyPlanningTableComponent extends CoreTable<WeeklyPlanning>
  implements OnInit, OnDestroy {
  @ViewChild(MatPaginator)
  public paginator: MatPaginator;
  @ViewChild(MatSort)
  public sort: MatSort;
  public filterData: FilterDataPipe = new FilterDataPipe();
  public destroy: Subject<boolean> = new Subject<boolean>();
  public siteworkId = null;

  constructor(
    public service: WeeklyPlanningServiceService,
    public dialog: MatDialog,
    public toast: ToastrService,
    public taskService: TaskService,
    public activeRoute: ActivatedRoute
  ) {
    super(CONFIG);
  }

  ngOnInit() {
    this.getRouterParam();
    this.subscriptionPlanning();
    this.getServiceData();
  }

  ngOnDestroy() {
    this.destroy.next(true);
    this.destroy.complete();
  }

  public getRouterParam(): void {
    this.activeRoute.params
    .pipe(takeUntil(this.destroy))
    .subscribe(param => {
      this.siteworkId = parseInt(param.id);
    });
  }

  public get loading(): boolean {
    return this.service.getLoading;
  }

  public getServiceData(): void {
    combineLatest(
      this.service.getData(),
      this.taskService.findBySiteWorkId(this.siteworkId)
    )
      .pipe(takeUntil(this.destroy))
      .subscribe();
  }

  public subscriptionPlanning(): void {
    this.service
      .subscriptionData()
      .pipe(takeUntil(this.destroy))
      .subscribe(planning => {
        console.log(planning);
        this.datasource.data = planning
          .filter(val => val.getSiteWork.getId === this.siteworkId)
          .map((planningVal: WeeklyPlanning) => {
            planningVal["endDate"] = new Date(
              planningVal["endDate"].toString()
            );
            planningVal["startDate"] = new Date(
              planningVal["startDate"].toString()
            );
            return planningVal;
          });
          console.log(this.datasource.data);
        this.datasource.sort = this.sort;
        this.datasource.paginator = this.paginator;
        this.filterDataSource();
      });
  }

  public filterDataSource(): void {
    this.datasource.filterPredicate = (
      data: WeeklyPlanning,
      keyWord: string
    ) => {
      return this.filterData.transform(data, keyWord, this.typeFilter);
    };
    this.search.valueChanges
      .pipe(takeUntil(this.destroy))
      .subscribe(val => (this.datasource.filter = val));
  }


  public sortData(sort): void {
    this.datasource.data = this.datasource.data.sort((a, b) => {
      const isAsc = sort.direction === "asc";
      switch (sort.active) {
        case "startDate":
          return this.compare(a.getStartDate, b.getStartDate, isAsc);
        case "endDate":
          return this.compare(
            a.getEndDate,
            b.getEndDate,
            isAsc
          );
        case "submit":
          return this.compare(a.getSubmit, b.getSubmit, isAsc);
        default:
          return 0;
      }
    });
  }

  public onCreate(): void {
    let weeklyPlanning = new WeeklyPlanning();
    weeklyPlanning.setSiteWork = new SiteWork({id: this.siteworkId});
    const dialog = this.dialog.open(PmWeeklyPlanningFormComponent, {
      width: "1800px",
      data: weeklyPlanning,
      disableClose: true
    });
    dialog
      .afterClosed()
      .pipe(takeUntil(this.destroy))
      .subscribe(val => {
        if (val) {
          console.log(val);
          this.service
            .create(val)
            .then()
            .catch(err => this.toast.error(err.message, err.status));
        }
      });
  }

  public onUpdate(data: WeeklyPlanning): void {
    const dialog = this.dialog.open(PmWeeklyPlanningFormComponent, {
      width: "1800px",
      data: data,
      disableClose: true
    });
    dialog
      .afterClosed()
      .pipe(takeUntil(this.destroy))
      .subscribe(val => {
        if (val) {
          this.service
            .update(val)
            .then()
            .catch(err => console.log(err));
        }
      });
  }

  public onSubmit(row: WeeklyPlanning): void {
    const dialog = this.dialog.open(DialogComponent, {
      data: {
        id: row.getId,
        message: `Are you sure to submit Weekly Planning ?`
      }
    });
    dialog
      .afterClosed()
      .pipe(takeUntil(this.destroy))
      .subscribe(_id => {
        if (_id) {
          this.service
            .submit(_id)
            .then()
            .catch(err => this.toast.error(err.message, err.status));
        }
      });
  }

  public updateRange(val: Range): void {
    // this.service
    //   .getByRangeDate(this.siteworkId, val.fromDate, val.toDate)
    //   .pipe(takeUntil(this.destroy))
    //   .subscribe();
  }

  public showTask(task: Task[]): void {
    this.dialog.open(PmWeeklyPlanningTaskComponent, {
      data: task,
      width: "1800px"
    });
  }
}
