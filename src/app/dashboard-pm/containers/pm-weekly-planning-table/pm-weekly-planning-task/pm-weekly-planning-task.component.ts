import { Component, OnInit, Inject } from '@angular/core';
import { MAT_DIALOG_DATA } from '@angular/material';
import { Task } from '../../../../../models/task-service/task.model';

@Component({
  selector: 'app-pm-weekly-planning-task',
  templateUrl: './pm-weekly-planning-task.component.html',
  styleUrls: ['./pm-weekly-planning-task.component.css']
})
export class PmWeeklyPlanningTaskComponent implements OnInit {

  constructor(@Inject(MAT_DIALOG_DATA) public data: Task[]) {
    console.log(this.data);
  }
  displayedColumns: string[] = ['no', 'activity', 'status', 'severity', 'desc'];

  getTasks(): Task[] {
    return this.data;
  }

  ngOnInit() {
  }

}
