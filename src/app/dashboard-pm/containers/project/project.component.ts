import { Component, OnInit, OnDestroy } from '@angular/core';
import { ColumnConfig, actionType, ColumnType, OnEvent } from 'src/app/shared/table-builder';
import { SiteWorkService } from 'src/service/siteWork-service/site-work.service';
import { combineLatest, Subject } from 'rxjs';
import { AAAService } from 'src/app/shared/aaa/aaa.service';
import { ProjectManagerService } from 'src/service/projectManager-service/project-manager.service';
import { ProjectManager } from 'src/app/models/projectManager.model';
import { takeUntil } from 'rxjs/operators';
import { RequestService } from 'src/service/request-service/request.service';
import { CoordinatorService } from 'src/service/coordinator-service/coordinator.service';
import { DailyReportService } from 'src/service/daily-report-service/daily-report.service';
import { SiteWork } from 'src/app/models/siteWork.model';
import { DailyReport } from 'src/app/models/daliyReport.model';
import { TaskExecution } from 'src/app/models/taskExecution.model';
import { DateDurationPipe } from 'src/app/pipes/dateDuration/date-duration.pipe';
import { SowService } from 'src/service/sow-service/sow.service';
import { RequestStatus, RequestType, Request } from 'src/app/models/request.model';
import { FormStartProjectComponent } from '../../presentational/form-start-project/form-start-project.component';
import { MatDialog } from '@angular/material';
import { Coordinator } from 'src/app/models/coordinator.model';
import { DialogComponent } from 'src/app/shared/dialog/dialog.component';
import { AssignmentStatus } from 'src/app/models/assignment.model';
import { TaskService } from 'src/service/task-service/task.service';
import { ToastrService } from 'ngx-toastr';
import { ActivatedRoute, Router } from '@angular/router';
import { RouteService } from 'src/service/route-service/route.service';
import { ContractService } from 'src/service/contract-service/contract.service';
import { Contract } from 'src/app/models/contract.model';

interface TableModel {
  id: number;
  projectId: string;
  contractNumber: string;
  ed: string;
  td: string;
  dd: string;
  opex: string;
  percentageOpex: number;
  poValue: number;
  // severity: string;
  status: string;
  startDate: string;
  endDate: string;
}

@Component({
  selector: 'app-project',
  template: `
  <app-project-table
    [data]="data"
    [title]="title"
    [actionTypes]="actionTypes"
    (onEvent)="onEvent($event)"
    [columnConfigs]="columnConfigs"
    >
  </app-project-table>
  `
})
export class ProjectComponent implements OnInit, OnDestroy {
  columnConfigs: ColumnConfig[];
  actionTypes: actionType[];
  title: string;
  data: TableModel[];

  coordinators: Coordinator[];
  projectManager: ProjectManager;
  siteworks: SiteWork[];
  requests: Request[];

  destroy$: Subject<boolean>;
  durationPipe: DateDurationPipe;
  contractId: number;

  constructor(
    public dialog: MatDialog,
    public siteWorkService: SiteWorkService,
    public aaaService: AAAService,
    public pmService: ProjectManagerService,
    public requestService: RequestService,
    public picService: CoordinatorService,
    public dailyService: DailyReportService,
    public sowService: SowService,
    public taskService: TaskService,
    public toastService: ToastrService,
    public routeService: RouteService,
    public contractService: ContractService,
    public route: Router
  ) {
    this.data = [];
    this.title = 'Project(s)';
    this.actionTypes = ['REQUEST', 'SUBMIT', 'VIEW'];
    this.columnConfigs = [
      { label: 'No', key: 'no', columnType: ColumnType.string },
      { label: 'Project ID', key: 'projectId', columnType: ColumnType.string },
      { label: 'Contract Number', key: 'contractNumber', columnType: ColumnType.string },
      { label: 'Duration', key: 'duration', columnType: ColumnType.string },
      { label: 'Opex', key: 'opex', columnType: ColumnType.string },
      { label: 'ED Plan / Actual', key: 'ed', columnType: ColumnType.string },
      { label: 'TD Plan / Actual', key: 'td', columnType: ColumnType.string },
      { label: 'DD Plan / Actual', key: 'dd', columnType: ColumnType.string },
      { label: 'Severity', key: 'severity', columnType: ColumnType.severity },
      { label: 'Status', key: 'status', columnType: ColumnType.status },
      { label: 'Start Date', key: 'startDate', columnType: ColumnType.date, format: 'shortDate' },
      { label: 'End Date', key: 'endDate', columnType: ColumnType.date, format: 'shortDate' },
    ];
    this.destroy$ = new Subject();
    this.durationPipe = new DateDurationPipe()
  }

  ngOnInit() {
    this.getContractId();
    this.getSubscriptionData();
    this.getPmSubscription()
    this.getServiceData();
  }

  ngOnDestroy() {
    this.destroy$.next(true);
    this.destroy$.complete();
  }

  getContractId() {
    this.routeService.pathParam
      .pipe(takeUntil(this.destroy$))
      .subscribe(val => {
        if (val) {
          const id = val.param ? parseInt((val.param).toString()) : null;
          this.contractId = id;
        }
      });
  }

  getPMAuth(projectManagers: ProjectManager[]) {
    projectManagers = projectManagers.filter(_set => {
      const region = _set.getRegion ? _set.getRegion : null;
      const id = region ? region.getId : null;
      return !this.aaaService.isAuthorized('read:project:{province:$}:{job:$}') ||
        this.aaaService.isAuthorized(`read:project:{province:${id}}:{job:${_set.getId}}`);
    });
    return projectManagers;
  }

  getServiceData() {
    combineLatest(
      this.pmService.getData(),
      this.requestService.findByType(RequestType.requested_project_id),
      this.picService.getData(),
      this.contractService.findById(this.contractId)
    )
      .pipe(takeUntil(this.destroy$))
      .subscribe();
  }

  getPmSubscription() {
    this.pmService.subscriptionData()
      .subscribe((val: ProjectManager[]) => {
        this.projectManager = this.getPMAuth(val)[0];
        if (this.projectManager) {
          this.siteWorkService.findByAssignmentPm(this.projectManager.getId, AssignmentStatus.CREATED)
            .pipe(takeUntil(this.destroy$))
            .subscribe(val => {
              // console.log(val);  
            });
        }
      });
  }

  getSubscriptionData() {
    combineLatest(
      this.siteWorkService.subscriptionData(),
      this.requestService.subscriptionData(),
      this.picService.subscriptionData(),
      this.contractService.subscriptionData(),
    )
      .pipe(takeUntil(this.destroy$))
      .subscribe(val => {
        this.requests = val[1];
        this.coordinators = val[2];

        this.siteworks = this.compareWithContractSitework(val[3] ? val[3][0] : null, val[0]);
        this.siteworks = this.setStatusFromRequest(this.siteworks, val[1]);
        this.siteworks = this.setTemporaryProp(this.siteworks);
        this.setTableModel(this.siteworks);
      });
  }

  compareWithContractSitework(contract: Contract, siteworks: SiteWork[]) {
    const siteworksContract = contract ? contract.getSiteworks : [];
    siteworks = siteworks.filter(sitework => {
      return siteworksContract.find(val => val.getId === sitework.getId);
    });
    return siteworks
  }

  setStatusFromRequest(siteworks: SiteWork[], requests: Request[]) {
    let request: Request;
    siteworks = siteworks.map(sitework => {
      request = requests.find(request => request.getObject['id'] === sitework.getId);
      if (request) {
        sitework['pidStatus'] = request.getStatus;
      } else {
        sitework['pidStatus'] = 'IDLE';
      }
      return sitework;
    });
    return siteworks;
  }

  setTemporaryProp(siteworks: SiteWork[]): SiteWork[] {
    return siteworks.map(sitework => {
      sitework['actualEd'] = 0;
      sitework['actualDd'] = 0;
      sitework['actualTd'] = 0;
      sitework['planEd'] = 0;
      sitework['planDd'] = 0;
      sitework['planTd'] = 0;
      sitework["percentageOpex"] = 0;
      sitework["duration"] = 0;
      return sitework;
    });
  }

  setTableModel(val: SiteWork[]) {
    let siteworks = val;
    siteworks = this.setOpexPercentage(siteworks);
    siteworks = this.setSiteWorkDuration(siteworks);
    siteworks = this.setActualMandays(siteworks);
    siteworks = this.setPlanMandays(siteworks);
    this.data = siteworks.map(sitework => {
      return {
        id: sitework.getId,
        projectId: sitework.getProjectCode ? sitework.getProjectCode : 'Not Available',
        contractNumber: sitework.getcontractNo,
        duration: sitework['duration'] ? sitework['duration'] : 0,
        ed: `${sitework['actualEd']} / ${sitework['planEd']}`,
        td: `${sitework['actualTd']} / ${sitework['planTd']}`,
        dd: `${sitework['actualDd']} / ${sitework['planDd']}`,
        opex: `${sitework.getTotalOpex} / ${sitework['percentageOpex']}%`,
        percentageOpex: sitework['percentageOpex'],
        poValue: sitework.getPoValue ? sitework.getPoValue : 0,
        // severity: sitework.getSeverity,
        status: sitework.getStatus,
        startDate: sitework.getStartDate,
        endDate: sitework.getEndDate,
        pidStatus: sitework['pidStatus']
      }
    });
  }

  public setOpexPercentage(siteWorks: SiteWork[]): SiteWork[] {
    siteWorks = siteWorks.map((siteWork: SiteWork) => {
      const poPercentage = parseFloat(
        ((25 / 100) * siteWork.getPoValue).toFixed(2)
      );
      if (poPercentage !== 0) {
        siteWork["percentageOpex"] = Math.floor(
          (siteWork.getTotalOpex / poPercentage) * 100
        );
      }
      return siteWork;
    });
    return siteWorks;
  }

  public setSiteWorkDuration(siteWorks: SiteWork[]): SiteWork[] {
    siteWorks = siteWorks.map((siteWork: SiteWork) => {
      siteWork["duration"] = siteWork.getStartDate ? this.durationPipe.transform(siteWork.getStartDate) : 0;
      return siteWork;
    });
    return siteWorks;
  }

  public setActualMandays(siteWorks: SiteWork[]): SiteWork[] {
    siteWorks = siteWorks.map((siteWork: SiteWork) => {
      this.dailyService
        .getBySiteWorkId(siteWork.getId)
        .then((dailys: DailyReport[]) => {
          dailys.forEach((daily: DailyReport) => {
            daily.getTaskExecutions.forEach((task: TaskExecution) => {
              switch (task.getResource["resourceType"]) {
                case "ENGINEER":
                  siteWork["actualEd"] += task.getManHour ? task.getManHour : 0;
                  break;
                case "TECHNICIAN":
                  siteWork["actualTd"] += task.getManHour ? task.getManHour : 0;
                  break;
                case "DOCUMENTATION":
                  siteWork["actualDd"] += task.getManHour ? task.getManHour : 0;
                  break;
              }
            });
          });
        });
      return siteWork;
    });
    return siteWorks;
  }

  public setPlanMandays(siteWorks: SiteWork[]): SiteWork[] {
    siteWorks = siteWorks.map((siteWork: SiteWork) => {
      this.sowService
        .getSowHistories(siteWork.getId)
        .pipe(takeUntil(this.destroy$))
        .subscribe((history: Object[]) => {
          // siteWork["planEd"] = 0;
          // siteWork["planTd"] = 0;
          // siteWork["planDd"] = 0;
          history.forEach(sow => {
            siteWork["planEd"] += sow["sow"] ? sow["sow"]["engineerDay"] ? sow["sow"]["engineerDay"] : 0 : 0;
            siteWork["planTd"] += sow["sow"] ? sow["sow"]["technicianDay"] ? sow["sow"]["technicianDay"] : 0 : 0;
            siteWork["planDd"] += sow["sow"] ? sow["sow"]["documentationDay"] ? sow["sow"]["documentationDay"] : 0 : 0;
          });
        });
      return siteWork;
    });
    return siteWorks;
  }


  onEvent(event: OnEvent) {
    const sitework = this.findSitework(event.value);
    switch (event.type) {
      case 'REQUEST':
        this.requestProjectId(sitework);
        break;
      case 'SUBMIT':
        this.startProject(sitework);
        break;
      case 'VIEW':
        this.showDetailProject(sitework);
        break;
    }
  }

  showDetailProject(sitework: SiteWork) {
    this.routeService.updatePathParamState({param: sitework, path: 'detail-project'});
    this.route.navigate(['home/dashboard-pm/detail-project'])
    .catch(err => console.log(err));
  }

  public startProject(data: SiteWork) {
    this.dialog.open(FormStartProjectComponent, {
      width: '900px',
      data: { data: data, coordinators: this.coordinators },
      disableClose: true
    }).afterClosed().subscribe((val: SiteWork) => {
      if (val) {
        val.setCoordinator = val.coordinatorObject(val.getCoordinator ? val.getCoordinator.getId : null);
        this.siteWorkService.update(val).then(sitework => {
          this.actionTypes = [];
          val.getSowTypes ? val.getSowTypes.forEach(type => {
            this.createTask(val.getId, type.getId);
          }) : [];
          this.toastService.success('Success start project', 'Success');
        })
          .catch(err => {
            this.toastService.error('Failed start project', 'Failed');
            // console.log(err);
          });
      }
    });
  }

  public createTask(siteworkId: number, sowTypeId: number) {
    this.taskService.createTask(siteworkId, sowTypeId)
      .then(result => {
        this.toastService.success('Success create task project', 'Success');
      })
      .catch(err => {
        this.toastService.error('Failed create task project', 'Failed');
      })
  }

  public requestProjectId(data: SiteWork) {
    const request = this.findRequest(data.getId);
    this.dialog.open(DialogComponent, {
      width: '500px',
      data: { id: data ? data.getId : null, message: `Are you sure to request id project ${data.getSite.getName}?` }
    }).afterClosed().subscribe(val => {
      if (val) {
        if (request) {
          this.updateRequest(request);
        } else {
          this.createRequestPID(data);
        }
      }
    });
  }

  updateRequest(request: Request) {
    request.setStatus = RequestStatus.proposed;
    this.requestService.update(request).then(val => {
      this.actionTypes = [];
      this.toastService.success('Success request project id', 'Success');
      // console.log(val);
    }).catch(err => {
      this.toastService.error('Failed request project id', 'Failed');
      // console.log(err);
    });
  }

  createRequestPID(val: SiteWork) {
    const request = new Request();
    request.setObject = val;
    request.setStartDate = new Date();
    request.setRequester = this.projectManager;
    request.setType = RequestType.requested_project_id;
    request.setStatus = RequestStatus.proposed;
    this.requestService.create(request).then(val => {
      this.toastService.success('Success request project id', 'Success');
      this.actionTypes = [];
      // console.log(val);
    }).catch(err => {
      this.toastService.error('Failed request project id', 'Failed');
      // console.log(err);
    });
  }

  findSitework(model: TableModel) {
    return this.siteworks.find(sitework => {
      return sitework.getId === model.id;
    });
  }

  findRequest(pid) {
    return this.requests.find(request => {
      return (request.getObject ? request.getObject['id'] : null) === pid;
    });
  }
}
