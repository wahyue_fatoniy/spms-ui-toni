import { Component, OnInit, OnDestroy } from "@angular/core";
import { Router } from "@angular/router";
import { takeUntil } from "rxjs/operators";
import { Subject } from "rxjs";
import { RouteService } from "src/service/route-service/route.service";
import { Contract } from "../models/contract.model";

export interface TabsMenu {
  path: string;
  title: string;
  img?: string;
}

@Component({
  selector: "app-dashboard-pm",
  template: `
    <nav mat-tab-nav-bar style="margin-top: -75px;">
      <a
        mat-tab-link
        style="text-decoration: none"
        *ngFor="let nav of tabsMenu"
        [routerLink]="nav.path"
        routerLinkActive
        #rla="routerLinkActive"
        [active]="rla.isActive"
        (click)="onRoute(nav.path)"
        >
        <img
          [src]="'/assets/img/collection/' + nav.img"
          style="margin-bottom:10px;"
        />&nbsp;
        {{ nav.title }}
      </a>
    </nav>
    <div class="container-home">
      <router-outlet></router-outlet>
    </div>
  `,
})
export class DashboardPmComponent implements OnInit, OnDestroy {
  public tabsMenu: TabsMenu[];
  public destroy$: Subject<boolean>;
  public contract: Contract;
  public previousRoute: string;

  constructor(
    public route: Router,
    public routeService: RouteService
  ) {
    this.destroy$ = new Subject();
    this.tabsMenu = [
      { path: "dashboard", title: "Dashboard", img: "pm.png" },
      { path: "approval", title: "Approval", img: "approvedDoc.png" },
      { path: `contract`, title: "Contract(s)", img: "assignment.png" },
      { path: `project`, title: "Project(s)", img: "project1.png" },
    ];
  }

  ngOnInit() {
    this.activeRoute();
    this.route
      .navigate(["/home/dashboard-pm/dashboard"])
      .catch(err => console.log(err));
  }

  ngOnDestroy() {
    this.destroy$.next(true);
    this.destroy$.unsubscribe();
  }

  onRoute(path: string) {
    if (path === 'contract-detail') {
      this.routeService.updatePathParamState({ path: path, param: this.contract })
    } else {
      this.routeService.updatePathParamState({ path: path })
    }
  }

  activeRoute() {
    this.routeService.pathParam
      .pipe(takeUntil(this.destroy$))
      .subscribe(route => {
        if (route) {
          switch (route.path) {
            case 'dashboard':
              this.tabsMenu = [
                { path: 'dashboard', title: "Dashboard", img: "pm.png" },
                { path: "approval", title: "Approval", img: "approvedDoc.png" },
                { path: `contract`, title: "Contract(s)", img: "assignment.png" },
                { path: `project`, title: "Project(s)", img: "project1.png" },
              ];
              break;
            case 'approval':
              this.tabsMenu = [
                { path: "dashboard", title: "Dashboard", img: "pm.png" },
                { path: 'approval', title: "Approval", img: "approvedDoc.png" },
                { path: `contract`, title: "Contract(s)", img: "assignment.png" },
                { path: `project`, title: "Project(s)", img: "project1.png" },
              ];
              break;
            case 'contract':
              const path = route.param ? `contract/${route.param}` : 'contract'
              this.tabsMenu = [
                { path: "dashboard", title: "Dashboard", img: "pm.png" },
                { path: "approval", title: "Approval", img: "approvedDoc.png" },
                { path: path, title: "Contract(s)", img: "assignment.png" },
                { path: `project`, title: "Project(s)", img: "project1.png" },
              ];
              break;
            case 'contract-detail':
              this.contract = route.param ? route.param : this.contract;
              this.tabsMenu = [
                { path: "dashboard", title: "Dashboard", img: "pm.png" },
                { path: 'approval', title: "Approval", img: "approvedDoc.png" },
                { path: `contract`, title: "Contract(s)", img: "assignment.png" },
                { path: `project`, title: "Project(s)", img: "project1.png" },
                { path: 'contract-detail', title: "Contract Detail", img: "assignment.png" },
              ];
              break;
            case 'project':
              this.tabsMenu = [
                { path: "dashboard", title: "Dashboard", img: "pm.png" },
                { path: 'approval', title: "Approval", img: "approvedDoc.png" },
                { path: `contract`, title: "Contract(s)", img: "assignment.png" },
                { path: `project`, title: "Project(s)", img: "project1.png" },
              ];
              break;
            case 'detail-project':
              if (this.previousRoute === 'contract-detail') {
                this.tabsMenu = [
                  { path: "dashboard", title: "Dashboard", img: "pm.png" },
                  { path: 'approval', title: "Approval", img: "approvedDoc.png" },
                  { path: `contract`, title: "Contract(s)", img: "assignment.png" },
                  { path: `project`, title: "Project(s)", img: "project1.png" },
                  { path: 'contract-detail', title: "Contract Detail", img: "assignment.png" },
                  { path: `detail-project`, title: "Project Detail", img: "project1.png" },
                ];
              } else {
                this.tabsMenu = [
                  { path: "dashboard", title: "Dashboard", img: "pm.png" },
                  { path: 'approval', title: "Approval", img: "approvedDoc.png" },
                  { path: `contract`, title: "Contract(s)", img: "assignment.png" },
                  { path: `project`, title: "Project(s)", img: "project1.png" },
                  { path: `detail-project`, title: "Project Detail", img: "project1.png" },
                ];
              }
              break;
          }
        }
        this.previousRoute = route ? route.path : null;
      })
  }
}
