import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DashboardComponent } from './containers/dashboard/dashboard.component';
import { PmSiteWorkTableComponent } from './containers/pm-siteWork-table/pm-site-work-table.component';
import { RouterModule, Routes } from '@angular/router';
import { DashboardPmComponent } from './dashboard-pm.component';
import { AssignmentComponent } from './containers/assignment/assignment.component';
import { MaterialModule } from '../material.module';
import { SharedModule } from '../shared/shared.module';
import { FormAssignmentComponent } from './presentational/form-assignment/form-assignment.component';
import { FormlyMaterialModule } from '@ngx-formly/material';
import { FormlyModule } from '@ngx-formly/core';
import { MatNativeDateModule } from '@angular/material';
import { FormlyMatDatepickerModule } from '@ngx-formly/material/datepicker';
import { FormSowListComponent } from './presentational/form-sow-list/form-sow-list.component';
import { PmSiteWorkValueComponent } from './containers/pm-siteWork-table/pm-site-work-value/pm-site-work-value.component';
import { FormProjectComponent } from './presentational/form-project/form-project.component';
import { FormListProjectComponent } from './presentational/form-list-project/form-list-project.component';
import { FormStartProjectComponent } from './presentational/form-start-project/form-start-project.component';
import { PmAlarmTableComponent } from './containers/pm-alarm-table/pm-alarm-table.component';
import { PmTaskFormComponent } from './containers/pm-task-table/pm-task-form/pm-task-form.component';
import { PmTeamFormComponent } from './containers/pm-team-table/pm-team-form/pm-team-form.component';
import { PmTeamTableComponent } from './containers/pm-team-table/pm-team-table.component';
import { PmWeeklyPlanningFormComponent } from './containers/pm-weekly-planning-table/pm-weekly-planning-form/pm-weekly-planning-form.component';
import { PmWeeklyPlanningTableComponent } from './containers/pm-weekly-planning-table/pm-weekly-planning-table.component';
import { PmBudgetDetailComponent } from './containers/pm-weekly-planning-table/pm-budget-detail/pm-budget-detail.component';
import { PmTaskTableComponent } from './containers/pm-task-table/pm-task-table.component';
import { PmWeeklyPlanningTaskComponent } from './containers/pm-weekly-planning-table/pm-weekly-planning-task/pm-weekly-planning-task.component';
import { ApprovalBudgetComponent } from './containers/approval-budget/approval-budget.component';
import { ApprovalBudgetTableComponent } from './presentational/approval-budget-table/approval-budget-table.component';
import { RequestAssignmentTableComponent } from './presentational/request-assignment-table/request-assignment-table.component';
import { ProjectTableComponent } from './presentational/project-table/project-table.component';
import { ProjectComponent } from './containers/project/project.component';
import { DetailProjectComponent } from './containers/detail-project/detail-project.component';
import { FormViewPlanningComponent } from './presentational/form-view-planning/form-view-planning.component';
import { ContractDetailComponent } from './containers/contract-detail/contract-detail.component';
import { ViewContractDetailComponent } from './presentational/view-contract-detail/view-contract-detail.component';
import { ViewProjectDetailComponent } from './presentational/view-project-detail/view-project-detail.component';

const routes: Routes = [
  {
    path: '', component: DashboardPmComponent, children: [
      { path: '', redirectTo: 'dashboard', pathMatch: 'full' },
      { path: 'dashboard', component: DashboardComponent },

      { path: 'approval', component: ApprovalBudgetComponent },

      { path: 'contract-detail', component: ContractDetailComponent },

      //Contract Route
      { path: 'contract', component: AssignmentComponent },
      { path: 'contract/:id', component: AssignmentComponent },

      { path: 'project/:id', component: ProjectComponent },
      { path: 'project', component: ProjectComponent },
      { path: 'detail-project', component: DetailProjectComponent },
    ]
  }
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    MaterialModule,
    SharedModule,

    FormlyMaterialModule,
    FormlyModule.forRoot({
      validationMessages: [
        { name: 'required', message: 'This field is required' },
      ],
    }),
    MatNativeDateModule,
    FormlyMatDatepickerModule,
  ],
  declarations: [
    //PRESENTATIONAL
    PmSiteWorkTableComponent,
    FormAssignmentComponent,
    FormSowListComponent,
    FormProjectComponent,
    FormListProjectComponent,
    FormStartProjectComponent,
    ApprovalBudgetTableComponent,
    RequestAssignmentTableComponent,

    //OLD COMPONENT
    PmSiteWorkValueComponent,
    PmAlarmTableComponent,
    PmTaskFormComponent,
    PmTeamFormComponent,
    PmTeamTableComponent,
    PmWeeklyPlanningFormComponent,
    PmWeeklyPlanningTableComponent,
    PmBudgetDetailComponent,
    PmTaskTableComponent,
    PmWeeklyPlanningTaskComponent,

    //CONTAINER
    DashboardComponent,
    DashboardPmComponent,
    AssignmentComponent,
    ApprovalBudgetComponent,
    ProjectTableComponent,
    ProjectComponent,
    DetailProjectComponent,
    FormViewPlanningComponent,
    ContractDetailComponent,
    ViewContractDetailComponent,
    ViewProjectDetailComponent,
  ],
  entryComponents: [
    FormAssignmentComponent,
    FormSowListComponent,
    PmSiteWorkValueComponent,
    FormProjectComponent,
    FormListProjectComponent,
    FormStartProjectComponent,
    FormViewPlanningComponent,
  ],
})
export class DashboardPmModule { }
