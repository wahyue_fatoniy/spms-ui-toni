import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ApprovalBudgetTableComponent } from './approval-budget-table.component';

describe('ApprovalBudgetTableComponent', () => {
  let component: ApprovalBudgetTableComponent;
  let fixture: ComponentFixture<ApprovalBudgetTableComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ApprovalBudgetTableComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ApprovalBudgetTableComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
