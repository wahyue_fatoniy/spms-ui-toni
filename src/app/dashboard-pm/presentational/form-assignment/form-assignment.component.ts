import { Component, OnInit, Inject } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { FormlyFieldConfig } from '@ngx-formly/core';
import { MAT_DIALOG_DATA, MatDialogRef, MatDialog } from '@angular/material';
import { Customer } from 'src/app/models/customer.model';
import { GeneralManager } from 'src/app/models/generalManager.model';
import { Site } from 'src/models/site-service/site.model';
import { SowType } from 'src/app/models/sow-type.model';
import { SiteWork } from 'src/app/models/siteWork.model';
import { FormProjectComponent } from '../form-project/form-project.component';
import { Observable, combineLatest } from 'rxjs';
import { map } from 'rxjs/operators';
import { ProjectManager } from 'src/app/models/projectManager.model';
import { Contract, ContractStatus } from 'src/app/models/contract.model';

interface FormData {
  data: Contract,
  customers: Observable<Customer[]>;
  generalManagers: Observable<GeneralManager[]>;
  sites: Observable<Site[]>;
  sowTypes: Observable<SowType[]>;
  projectManager: ProjectManager;
}

interface FormModel {
  id: number;
  number: string;
  // name: string;
  startDate: Date;
  endDate: Date;
  generalManager: number;
  customer: number;
  value: number;
  description: string;
}

@Component({
  selector: 'app-form-assignment',
  templateUrl: './form-assignment.component.html',
  styleUrls: ['./form-assignment.component.css']
})
export class FormAssignmentComponent implements OnInit {
  displayedColumns: string[];
  fields: FormlyFieldConfig[];
  model: FormModel;
  form: FormGroup;

  // Data select options field
  generalManagers: Observable<GeneralManager[]>;
  customers: Observable<Customer[]>;
  sowTypes: Observable<SowType[]>;
  sites: Observable<Site[]>;
  projects: SiteWork[];
  projectManager: ProjectManager;

  listGeneralManager: GeneralManager[] = [];
  listCustomer: Customer[] = [];
  rowIndex: number;
  selectField: boolean;
  selectedProject: any;
  totalSSOW: number;

  constructor(
    @Inject(MAT_DIALOG_DATA) public formData: FormData,
    private dialogRef: MatDialogRef<Contract>,
    private dialog: MatDialog
  ) {
    this.form = new FormGroup({});
    this.fields = formData.data.formFieldConfig;
    this.projectManager = this.formData.projectManager;
    this.generalManagers = formData.generalManagers;
    this.customers = formData.customers;
    this.sowTypes = formData.sowTypes;
    this.sites = formData.sites;
    this.initSiteWorkTable();
    this.displayedColumns = ['no', 'id', 'site', 'sow', 'value'];
    this.selectField = false;
  }

  ngOnInit() {
    this.getSubscriptionData();
    this.setFormModel();
    this.setOptionsCustomer();
    this.setOptionsGeneralManager();
    // this.hideDescriptionField();
  }

  initSiteWorkTable() {
    this.formData.data.setSiteworks = this.formData.data.getSiteworks ? this.formData.data.getSiteworks.map(sitework => {
      sitework = this.setValue(sitework);
      return sitework;
    }) : [];
    this.projects = this.formData.data.getSiteworks;
    this.setTotalSSOW();
  }

  getSubscriptionData() {
    combineLatest(
      this.customers,
      this.generalManagers
    ).subscribe(val => {
      this.listCustomer = val[0];
      this.listGeneralManager = val[1];
    });
  }

  setFormModel() {
    this.model = {
      id: this.formData.data.getId,
      number: this.formData.data.getNumber,
      generalManager: this.formData.data.getGeneralManager ? this.formData.data.getGeneralManager.getId : null,
      customer: this.formData.data.getCustomer ? this.formData.data.getCustomer.getId : null,
      startDate: new Date(this.formData.data.getStartDate),
      endDate: new Date(this.formData.data.getEndDate),
      description: this.formData.data.getDescription,
      value: this.formData.data.getValue
    }
  }

  hideDescriptionField() {
    this.fields = this.fields.map(field => {
      if (field.key === 'description') {
        field.hideExpression = true;
      }
      return field;
    });
  }

  setOptionsCustomer() {
    this.fields = this.fields.map(field => {
      if (field.id === '1') {
        field.fieldGroup = field.fieldGroup.map(group => {
          if (group.key === 'customer') {
            group.templateOptions.options = this.customers.
              pipe(
                map(customers => {
                  return customers.map(customer => {
                    return { value: customer.getId, label: customer.getParty['name'] }
                  })
                })
              );
          }
          return group;
        })
      }
      return field;
    });
  }

  setOptionsGeneralManager() {
    this.fields = this.fields.map(field => {
      if (field.id === '1') {
        field.fieldGroup = field.fieldGroup.map(group => {
          if (group.key === 'generalManager') {
            group.templateOptions.options = this.generalManagers.pipe(
              map(managers => {
                return managers.map(manager => {
                  return { value: manager.getId, label: manager.getParty['name'] }
                })
              })
            )
          }
          return group;
        })
      }
      return field;
    });
  }

  onAddProject() {
    this.dialog.open(FormProjectComponent, {
      width: '1000px',
      data: { data: new SiteWork(), sites: this.sites, sowTypes: this.sowTypes, projectManager: this.projectManager },
      disableClose: true,
    }).afterClosed().subscribe((val: SiteWork) => {
      if (val) {
        console.log(val);
        val = this.setValue(val);
        this.projects = [...this.projects, val];
        this.setTotalSSOW();
      }
    });
  }

  onUpdateProject() {
    const index = this.projects.findIndex(project => project.getAssignmentCode === this.selectedProject.getAssignmentCode);
    this.dialog.open(FormProjectComponent, {
      width: '1000px',
      data: { data: this.selectedProject, sites: this.sites, sowTypes: this.sowTypes, projectManager: this.projectManager },
      disableClose: true,
    }).afterClosed().subscribe((val: SiteWork) => {
      if (val) {
        val = this.setValue(val);
        this.projects = this.projects.map((project, i) => {
          return i === index ? val : project;
        });
        this.setTotalSSOW();
      }
    });
  }

  onDeleteProject() {
    this.projects = this.projects.filter(project => {
      return project.getAssignmentCode !== this.selectedProject.getAssignmentCode
    });
    this.setTotalSSOW();
  }

  onSelectRow(element) {
    this.selectField = true;
    this.selectedProject = element;
  }

  setTotalSSOW() {
    this.totalSSOW = 0;
    this.projects.forEach(project => {
      this.totalSSOW += project['value'] ? project['value'] : 0; 
    });
  }

  setValue(sitework: SiteWork) {
    sitework['value'] = 0;
    sitework.getSowTypes.forEach(type => {
      sitework['value'] += type.getPrice;
    });
    return sitework;
  }

  onClose() {
    const contract = new Contract();
    contract.setId = this.model.id;
    contract.setNumber = this.model.number;
    contract.setStatus = ContractStatus.CREATED;
    contract.setSiteworks = this.projects;
    contract.setStartDate = this.model.startDate;
    contract.setEndDate = this.model.endDate;
    contract.setProjectManager = this.projectManager;
    contract.setDescription = this.model.description;
    contract.setValue = this.model.value;
    contract.setCustomer = this.listCustomer.find(customer => {
      return customer.getId === this.model.customer
    });

    contract.setGeneralManager = this.listGeneralManager.find(gm => {
      return gm.getId === this.model.generalManager;
    });
    console.log(contract);
    // ON MODAL CLOSE
    this.dialogRef.close(contract);
  }
}
