import { Component, OnInit, Inject } from '@angular/core';
import { MAT_DIALOG_DATA } from '@angular/material';
import { SiteWork } from 'src/app/models/siteWork.model';

interface FormModel {
  assignmentCode: string;
  site: string;
  value: number;
}

@Component({
  selector: 'app-form-list-project',
  templateUrl: './form-list-project.component.html',
  styleUrls: ['./form-list-project.component.css']
})
export class FormListProjectComponent {
  sources: FormModel[];
  displayedColumns: string[];

  constructor(
    @Inject(MAT_DIALOG_DATA) public formData: SiteWork[]
  ) {
    let source: FormModel;
    this.sources = formData.map(data => {
      source =  {
        assignmentCode: data.getAssignmentCode,
        site: data.getSite.getName,
        value: 0,
      }
      data.getSowTypes.forEach(type => {
        source.value += type.getPrice;
      });
      return source;
    });
    this.displayedColumns = ['no', 'id' ,'site', 'value']
  }
}
