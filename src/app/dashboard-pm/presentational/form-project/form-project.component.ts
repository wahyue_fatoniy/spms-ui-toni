import { Component, Inject, OnInit } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material';
import { SiteWork } from 'src/app/models/siteWork.model';
import { Site } from 'src/app/models/site.model';
import { SowType } from 'src/app/models/sow-type.model';
import { FormlyFieldConfig } from '@ngx-formly/core';
import { FormGroup } from '@angular/forms';
import { Observable, combineLatest } from 'rxjs';
import { map, filter } from 'rxjs/operators';
import { ProjectManager } from 'src/app/models/projectManager.model';

interface FormData {
  data: SiteWork;
  sites: Observable<Site[]>;
  sowTypes: Observable<SowType[]>;
  projectManager: ProjectManager;
}

interface FormModel {
  assignmentCode: string;
  site: number;
  sow: number[];
}

@Component({
  selector: 'app-form-project',
  templateUrl: './form-project.component.html',
  styleUrls: ['./form-project.component.css']
})
export class FormProjectComponent implements OnInit {
  displayedColumns: string[];
  sites: Observable<Site[]>;
  sowTypes: Observable<SowType[]>;
  sowsDatasource: SowType[];

  projectManager: ProjectManager;
  fields: FormlyFieldConfig[];
  model: FormModel;
  form: FormGroup;

  sowSelected: SowType;
  listSites: Site[] = [];
  listSowTypes: SowType[] = [];

  constructor(
    @Inject(MAT_DIALOG_DATA) public formData: FormData,
    public dialogRef: MatDialogRef<any>
  ) {
    this.form = new FormGroup({});

    this.sowsDatasource = formData.data.getSowTypes ? formData.data.getSowTypes : [];
    this.displayedColumns = ['no', 'sowCode', 'description', 'action'];
    this.sites = formData.sites;
    this.sowTypes = formData.sowTypes;
    this.projectManager = formData.projectManager;
    this.setFieldConfig();
    this.setFormModel();
  }

  ngOnInit() {
    this.getSubscriptionData();
  }

  getSubscriptionData() {
    combineLatest(
      this.sites,
      this.sowTypes
    ).subscribe(val => {
      this.listSites = val[0];
      this.listSowTypes = val[1];
    });
  }

  setFormModel() {
    this.model = {
      assignmentCode: this.formData.data.getAssignmentCode,
      site: this.formData.data.getSite ? this.formData.data.getSite.getId : null,
      sow: this.formData.data.getSowTypes ? this.formData.data.getSowTypes.map(type => {
        return type.getId;
      }) : []
    }
  }

  setFieldConfig() {
    this.fields = [
      {
        fieldGroupClassName: 'row',
        fieldGroup: [
          {
            className: 'col-6',
            key: 'assignmentCode',
            type: 'input',
            templateOptions: {
              label: 'Id',
            }
          },
          {
            className: 'col-6',
            key: 'site',
            type: 'select',
            templateOptions: {
              label: 'Site',
              required: true,
              options: this.sites.
                pipe(
                  map(sites => {
                    return sites.map(site => {
                      return { value: site.getId, label: site.getName }
                    });
                  })
                )
            }
          }
        ]
      },
    ]
  }

  onSelectSow(event) {
    this.sowsDatasource = [...this.sowsDatasource, event];
  }

  onDeleteSow(index) {
    this.sowsDatasource = this.sowsDatasource.filter((sow, i) => {
      return i !== index;
    })
  }

  onClose() {
    const sitework = new SiteWork();
    sitework.setAssignmentCode = this.model.assignmentCode;
    sitework.setSowTypes = this.sowsDatasource;
    sitework.setProjectManager = this.projectManager;
    sitework.setSite = this.listSites.find(site => {
      return site.getId === this.model.site
    });
    this.dialogRef.close(sitework);
  }

}
