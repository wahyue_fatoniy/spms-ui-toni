import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FormSowListComponent } from './form-sow-list.component';

describe('FormSowListComponent', () => {
  let component: FormSowListComponent;
  let fixture: ComponentFixture<FormSowListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FormSowListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FormSowListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
