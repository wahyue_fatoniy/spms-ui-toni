import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FormStartProjectComponent } from './form-start-project.component';

describe('FormStartProjectComponent', () => {
  let component: FormStartProjectComponent;
  let fixture: ComponentFixture<FormStartProjectComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FormStartProjectComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FormStartProjectComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
