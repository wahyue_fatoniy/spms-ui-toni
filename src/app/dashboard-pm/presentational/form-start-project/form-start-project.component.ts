import { Component, Inject } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material';
import { FormlyFieldConfig } from '@ngx-formly/core';
import { FormGroup } from '@angular/forms';
import { SiteWork } from 'src/app/models/siteWork.model';
import { Coordinator } from 'src/app/models/coordinator.model';

interface FormData {
  data: SiteWork;
  coordinators: Coordinator[];
}

@Component({
  selector: 'app-form-start-project',
  templateUrl: './form-start-project.component.html',
  styleUrls: ['./form-start-project.component.css']
})
export class FormStartProjectComponent {
  fields: FormlyFieldConfig[];
  model: {
    startDate: string;
    endDate: string;
    coordinator: number;
  };
  form: FormGroup;

  constructor(
    @Inject(MAT_DIALOG_DATA) public formData: FormData,
    public dialogRef: MatDialogRef<any>
  ) {
    this.form = new FormGroup({});
    this.setFormFieldConfig();
    this.setFormModel();
  }

  setFormModel() {
    this.model = {
      startDate: null,
      endDate: null,
      coordinator: null
    }
  }

  setFormFieldConfig() {
    this.fields = [
      {
        fieldGroupClassName: 'row',
        fieldGroup: [
          {
            className: 'col-6',
            key: 'startDate',
            type: 'datepicker',
            templateOptions: {
              label: 'Start Date',
              required: true,
              readonly: true,
            }
          },
          {
            className: 'col-6',
            key: 'endDate',
            type: 'datepicker',
            templateOptions: {
              label: 'End Date',
              required: true,
              readonly: true,
            }
          }
        ]
      },
      {
        fieldGroupClassName: 'row',
        fieldGroup: [
          {
            className: 'col-6',
            key: 'coordinator',
            type: 'select',
            templateOptions: {
              label: 'Coordinator',
              required: true,
              options: this.formData.coordinators.map(coordinator => {
                return { value: coordinator.getId, label: coordinator.getParty['name'] }
              }),
            }
          }
        ]
      }
    ]
  }

  onClose() {
    this.formData.data.setStartDate = this.model.startDate;
    this.formData.data.setEndDate = this.model.endDate;
    this.formData.data.setCoordinator = this.formData.coordinators.find(coordinator => {
      return coordinator.getId === this.model.coordinator;
    });
    this.dialogRef.close(this.formData.data);
  }
}
