import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FormViewPlanningComponent } from './form-view-planning.component';

describe('FormViewPlanningComponent', () => {
  let component: FormViewPlanningComponent;
  let fixture: ComponentFixture<FormViewPlanningComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FormViewPlanningComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FormViewPlanningComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
