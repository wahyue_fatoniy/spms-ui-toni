import { Component, OnInit, Inject } from '@angular/core';
import { DailyPlan } from 'src/app/models/dailyPlan.model';
import { MAT_DIALOG_DATA } from '@angular/material';
import { LocalCurrencyPipe } from 'src/app/pipes/localCurrency/local-currency.pipe';
import { Task } from 'src/app/models/task.model';
import { Resource } from 'src/app/models/resource.model';

@Component({
  selector: 'app-form-view-planning',
  templateUrl: './form-view-planning.component.html',
  styleUrls: ['./form-view-planning.component.css']
})
export class FormViewPlanningComponent implements OnInit {
  currencyPipe: LocalCurrencyPipe = new LocalCurrencyPipe();
  projectId: string;
  opex: string;
  site: string;
  date: Date;
  displayed
  resourceColumns: string[];
  taskColumns: string[];
  tasks: Task[];
  resources: Resource[];

  constructor(
    @Inject(MAT_DIALOG_DATA) public data: DailyPlan
  ) {
    this.opex = this.currencyPipe.transform(data.getOpex);
    this.site = data ? data.getSitework ? data.getSitework.getSite.getName : '' : '';
    this.projectId = data ? data.getSitework ? data.getSitework.getProjectCode : '' : '';
    this.date = data.getDate ? new Date(data.getDate) : null;

    this.resources = data ? data.getResources : [];
    this.tasks = data ? data.getTasks : [];
    this.resourceColumns = ['no', 'name', 'role'];
    this.taskColumns = ['no', 'activity']
  }

  ngOnInit() {
  }

}
