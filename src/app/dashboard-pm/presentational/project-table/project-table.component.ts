import { Component, OnInit, ViewChild, EventEmitter, Output, Input } from '@angular/core';
import { MatPaginator, MatSort } from '@angular/material';
import { TableBuilder, OnEvent, ColumnConfig, actionType } from 'src/app/shared/table-builder';
import { FilterPercentagePipe } from 'src/app/pipes/filter-percentage/filter-percentage.pipe';
import { SiteWork } from 'src/app/models/siteWork.model';

interface FilterProject {
  value: any;
  type: 'status' | 'severity' | 'opex' | 'po';
}

enum SiteWorkPoStatus {
  RELEASE,
  NOT_RELEASE
}

enum SiteWorkSeverity {
  NONE,
  NORMAL,
  MAJOR_I,
  MAJOR_E,
  CRITICAL_I,
  CRITICAL_E,
}

enum SiteWorkOpex {
  '>100',
  '<100',
  '=100'
}

enum SiteWorkStatus {
  NOT_STARTED,
  DONE,
  CANCEL,
  ON_PROGRESS
}

@Component({
  selector: 'app-project-table',
  templateUrl: './project-table.component.html',
  styleUrls: ['./project-table.component.css']
})
export class ProjectTableComponent extends TableBuilder implements OnInit {
  @ViewChild(MatPaginator) _paginator: MatPaginator;
  @ViewChild(MatSort) _sort: MatSort;
  public percentagePipe: FilterPercentagePipe;

  @Output() onEvent: EventEmitter<OnEvent>;
  @Input() columnConfigs: ColumnConfig[];
  @Input() actionTypes: actionType[];
  @Input() title: string;
  @Input() data: any[];

  public selectField: boolean;
  public rowIndex: number;
  public selectedData: any;
  public sourceFilter: any[];

  ngModelSeverity: number[];
  ngModelStatus: number[];
  ngModelOpex: number;
  ngModelPo: number;

  severityOptions = [
    'None',
    'Normal',
    'Major I',
    'Major E',
    'Critical I',
    'Critical E',
  ];

  statusOptions = [
    "Not Started",
    "On Progress",
    "Done",
    "Cancel"
  ];

  poOptions = [
    'Release',
    'Not Release'
  ];


  constructor() {
    super();
    this.onEvent = new EventEmitter();
    this.selectField = false;
    this.percentagePipe = new FilterPercentagePipe();
    this.ngModelOpex = 1;
    this.ngModelPo = 0;
    this.ngModelSeverity = [];
    this.ngModelStatus = [];
    this.sourceFilter = [];
  }

  ngOnInit() {
    this.paginator = this._paginator;
    this.sort = this._sort;
    this.setDisplayedColumns();
    this.setTypeFilters();
  }

  ngOnChanges() {
    this.getStorage();
    this.datasource.data = this.data;
    this.sourceFilter = this.data;
    this.onFilter({value:[1], type: 'status'});
  }

  ngOnDestroy() {
    this.subscription.next(true);
    this.subscription.complete();
  }

  public onFilter(event: FilterProject): void {
    let projects = this.sourceFilter;
    if (event.type === "status") {
      projects = this.filterStatus(
        projects,
        this.ngModelStatus.map(number => {
          return SiteWorkStatus[number];
        })
      );
    } else if (event.type === "opex") {
      projects = this.filterOpex(projects, SiteWorkOpex[event.value]);
    } else if (event.type === "severity") {
      projects = this.filterSeverity(
        projects,
        this.ngModelSeverity.map(number => {
          return SiteWorkSeverity[number];
        })
      );
    } else if (event.type === 'po') {
      projects = this.filterPoStatus(projects, SiteWorkPoStatus[event.value]);
    }
    this.datasource.data = this.filterAll(projects);
    this.setStorage();
  }

  public filterAll(datasource: Object[]): Object[] {
    datasource = this.filterStatus(
      datasource,
      this.ngModelStatus.map(number => {
        return SiteWorkStatus[number];
      })
    );
    datasource = this.filterSeverity(
      datasource,
      this.ngModelSeverity.map(number => {
        return SiteWorkSeverity[number];
      })
    );
    datasource = this.filterPoStatus(datasource, SiteWorkPoStatus[this.ngModelPo]);
    datasource = this.filterOpex(datasource, SiteWorkOpex[this.ngModelOpex]);
    return datasource;
  }


  public filterStatus(siteWorks: Object[], status: string[]): Object[] {
    let projects = siteWorks.sort((a, b) => b['id'] - a['id']);
    projects = projects.filter((project, i, arr) => {
      return status.findIndex(val => val === project['status']) !== -1;
    });
    return projects;
  }

  public filterSeverity(siteWorks: Object[], severity: string[]) {
    siteWorks = siteWorks.filter((sitework, i, arr) => {
      return severity.findIndex(val => val === sitework['severity']) !== -1;
    });
    return siteWorks;
  }

  public filterOpex(siteWorks: Object[], opex: string) {
    siteWorks = this.percentagePipe.transform(
      opex,
      "percentageOpex",
      siteWorks
    );
    return siteWorks;
  }

  public filterPoStatus(siteWorks: Object[], poStatus: string): Object[] {
    return siteWorks.filter(siteWork => {
      if (poStatus === 'RELEASE') {
        return siteWork['poValue'] > 0;
      } else {
        return siteWork['poValue'] === 0;
      }
    });
  }

  public getStorage() {
    let projectFilter = JSON.parse(localStorage.getItem("projectFilter"));
    if (projectFilter !== null) {
      this.ngModelOpex = projectFilter["opex"];
      this.ngModelPo = projectFilter["po"];
      this.ngModelSeverity = projectFilter["severity"];
      this.ngModelStatus = projectFilter["status"];
    }
  }

  public setStorage(): void {
    let projectFilter = {
      severity: this.ngModelSeverity,
      status: this.ngModelStatus,
      po: this.ngModelPo,
      opex: this.ngModelOpex
    };
    localStorage.setItem("projectFilter", JSON.stringify(projectFilter));
  }


  onSelectRow(element: SiteWork) {
    this.selectField = true;
    this.selectedData = element;
    this.actionTypes = [];
    if (element['pidStatus'] === "ACCEPTED" && !element['startDate']) {
      this.actionTypes = ['SUBMIT'];
    }

    if (element['pidStatus'] === 'IDLE' || element['pidStatus'] === 'REJECTED') {
      this.actionTypes = ['REQUEST'];
    }
  }

  onEventClick(type) {
    this.onEvent.emit({value: this.selectedData, type: type})
  }

  onDblClick(element) {
    this.onEvent.emit({type: 'VIEW', value: element})
  }
}
