import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RequestAssignmentTableComponent } from './request-assignment-table.component';

describe('RequestAssignmentTableComponent', () => {
  let component: RequestAssignmentTableComponent;
  let fixture: ComponentFixture<RequestAssignmentTableComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RequestAssignmentTableComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RequestAssignmentTableComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
