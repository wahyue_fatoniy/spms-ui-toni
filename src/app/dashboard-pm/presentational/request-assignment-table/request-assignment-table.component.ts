import { Component, OnInit, ViewChild, EventEmitter, Output, Input } from '@angular/core';
import { MatPaginator, MatSort } from '@angular/material';
import { OnEvent, ColumnConfig, actionType, TableBuilder } from 'src/app/shared/table-builder';

@Component({
  selector: 'app-request-assignment-table',
  templateUrl: './request-assignment-table.component.html',
  styleUrls: ['./request-assignment-table.component.css']
})
export class RequestAssignmentTableComponent extends TableBuilder implements OnInit {
  @ViewChild(MatPaginator) _paginator: MatPaginator;
  @ViewChild(MatSort) _sort: MatSort;

  @Output() onEvent: EventEmitter<OnEvent>;
  @Input() columnConfigs: ColumnConfig[];
  @Input() actionTypes: actionType[];
  @Input() title: string;
  @Input() data: any[];

  public selectField: boolean;
  public rowIndex: number;
  public selectedData: any;

  constructor() {
    super();
    this.onEvent = new EventEmitter();
    this.selectField = false;
  }

  ngOnInit() {
    this.paginator = this._paginator;
    this.sort = this._sort;
    this.setDisplayedColumns();
    this.setTypeFilters();
  }

  ngOnChanges() {
    this.datasource.data = this.data;
  }

  ngOnDestroy() {
    this.subscription.next(true);
    this.subscription.complete();
  }

  onEventClick(value: OnEvent) {
    value.value = this.selectedData;
    this.onEvent.emit(value);
  }

  onSelectRow(element) {
    this.selectField = true;
    this.selectedData = element;
    this.setButton(element['status']);
  }

  onDblClick(element) {
    this.selectedData = element;
    this.selectField = false;
    if (element['status'] === 'APPROVED') {
      this.onEvent.emit({ type: 'VIEW', value: element });
    }
  }

  setButton(status: string) {
    switch (status) {
      case 'CREATED':
        this.actionTypes = ['ADD', 'UPDATE', 'DELETE', 'SUBMIT'];
        break;
      case 'APPROVED':
        this.actionTypes = ['ADD', 'VIEW'];
        break;
      default:
        this.actionTypes = ['ADD'];
    }
  }
}
