import { Component, OnInit, Input, OnChanges, Output, EventEmitter } from '@angular/core';
import { Contract } from 'src/app/models/contract.model';
import { SiteWork } from 'src/app/models/siteWork.model';

@Component({
  selector: 'app-view-contract-detail',
  templateUrl: './view-contract-detail.component.html',
  styleUrls: ['./view-contract-detail.component.css']
})
export class ViewContractDetailComponent implements OnInit, OnChanges {
  @Input() data: Contract;
  @Output() onEvent: EventEmitter<{ value: any, type: string }> = new EventEmitter();

  siteworkColumns: string[];
  poColumns: string[];

  siteworks: SiteWork[];
  persentaseProjectDone: number;


  constructor() {
    this.siteworkColumns = ['no', 'id', 'site', 'startDate', 'endDate', 'progress', 'lastReport','value'];
    this.poColumns = ['no', 'poNumber', 'poValue','invoiceNumber', 'payment']
  }

  ngOnInit() { }

  ngOnChanges() {
    this.siteworks = this.data ? this.data.getSiteworks ? this.data.getSiteworks : [] : [];
    this.setValueSitework();
    this.setPersentaseProjectDone();
  }

  setValueSitework() {
    this.siteworks = this.siteworks.map(sitework => {
      sitework['value'] = 0;
      sitework.getSowTypes.forEach(type => {
        sitework['value'] += type.getPrice ? type.getPrice : 0;
      });
      return sitework;
    });
  }

  setPersentaseProjectDone() {
    this.persentaseProjectDone = 0;
    let projectDone = 0;
    this.siteworks.forEach(sitework => {
      if (sitework.getStatus === 'DONE') {
        projectDone += 1;
      }
    });
    if (projectDone !== 0) {
      this.persentaseProjectDone = parseInt(((projectDone / this.siteworks.length)*100).toString());
    }
  }

  onSelectRow(val) {
    this.onEvent.emit({ value: val, type: 'DETAIL' })
  }
}
