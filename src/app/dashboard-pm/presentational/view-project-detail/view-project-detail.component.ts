import { Component, OnInit, Input, OnChanges } from '@angular/core';
import { SiteWork } from 'src/app/models/siteWork.model';
import { Task, SeverityType } from 'src/app/models/task.model';
import { SowType } from 'src/app/models/sow-type.model';

interface  SSOWTableModel {
  sowCode: string;
  description: string;
  progress: number;
}

@Component({
  selector: 'app-view-project-detail',
  templateUrl: './view-project-detail.component.html',
})
export class ViewProjectDetailComponent implements OnInit, OnChanges {
  @Input() data: SiteWork = new SiteWork();
  totalOpex: number;
  site: string;
  tasks: Task[];
  coordinator: string;
  startDate: Date;
  endDate: Date;
  projectId: string;  
  sowTypes: SowType[];
  lastDailyReport: Date = new Date();
  lastDailyPlanning: Date = new Date();

  majorI: number;
  majorE: number;
  criticalI: number;
  criticalE: number;

  alarmColumns: string[];
  ssowColumns: string[];

  alarmsActive: Task[];
  ssowData: SSOWTableModel[];

  constructor() {
    this.alarmColumns = ['no', 'activity', 'severity', 'description'];
    this.ssowColumns = ['no', 'sowCode', 'description','progress'];
    this.majorE = 0;
    this.majorI = 0;
    this.criticalE = 0;
    this.criticalI = 0;
  }

  ngOnInit() { }

  ngOnChanges() {
    this.projectId = this.data ? this.data.getProjectCode : '';
    this.startDate = this.data ? this.data.getStartDate ? new Date(this.data.getStartDate) : null : null;
    this.endDate = this.data ? this.data.getEndDate ? new Date(this.data.getEndDate) : null : null;
    this.site = this.data ? this.data.getSite ? this.data.getSite.getName : '' : '';
    this.totalOpex = this.data ? this.data.getTotalOpex : 0;
    this.coordinator = this.data ? this.data.getCoordinator ? this.data.getCoordinator.getParty['name'] : '' : '';
    this.tasks = this.data ? this.data['tasks'] ? this.data['tasks'] : [] : [];
    this.sowTypes = this.data ? this.data.getSowTypes : [];
    this.setAlarmActive();
    this.setSSOWData();
    this.setTotalAlarm();
  }

  setTotalAlarm() {
  }

  setAlarmActive() {
  }

  setSSOWData() {
    this.ssowData = [];
    this.sowTypes  = this.sowTypes.map(sow => {
      sow['tasks'] = [];
      this.tasks.forEach(task => {
        if ((task.getSow ? task.getSow.getSowType.getSowCode : '') === sow.getSowCode) {
          sow['tasks'] = [...sow['tasks'], task];
        }
      });
      return sow;
    });

    this.sowTypes = this.sowTypes.map(sow => {
      sow['taskDone'] = 0;
      sow['progress'] = 0;
      (sow['tasks'] ? sow['tasks'] : []).forEach(task => {
        if (task['status'] === 'DONE') {
          sow['taskDone'] += 1;
        }
      });
      if (sow['taskDone'] !== 0) {
          sow['progress'] = (sow['taskDone'] / sow['tasks'].length)*100;
      }
      this.ssowData = [
        ...this.ssowData,
        {
          sowCode: sow.getSowCode,
          description: sow.getDescription,
          progress: sow['progress'] ? sow['progress'] : 0
        }
      ]
      return sow;
    });
  }
}
