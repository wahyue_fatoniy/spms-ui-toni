import { Component, OnInit } from "@angular/core";

@Component({
  selector: "app-home",
  template: `
    <div class="wrapper">
      <app-sidebar></app-sidebar>
      <div class="main-panel">
        <!-- <app-header></app-header> -->
        <div class="main-content" style="padding-bottom: 0;">
          <div class="container-fluid">
            <router-outlet></router-outlet>
          </div>
        </div>
      </div>
    </div>
  `,
  styles: []
})
export class HomeComponent implements OnInit {
  constructor() {}

  ngOnInit() {}
}
