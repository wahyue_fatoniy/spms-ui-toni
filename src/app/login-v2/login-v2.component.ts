import { Component, OnInit } from '@angular/core';
import { FormControl, Validators } from '@angular/forms';
import { AAAService } from '../shared/aaa/aaa.service';
import { UserAuthService, ConnectionType } from '../shared/aaa/user/user.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login-v2',
  templateUrl: './login-v2.component.html',
  styleUrls: ['./login-v2.component.css']
})
export class LoginV2Component implements OnInit {

  constructor(public aaa: AAAService, private user: UserAuthService, private router: Router) {}
  email: FormControl = new FormControl(null , [Validators.required]);
  password: FormControl = new FormControl(null, [Validators.required]);
  hide = true;

  ngOnInit() {
    // if (!this.auth.isAuthenticated()) {
    //   this.auth.loginView();
    // } else {
    //   this.router.navigate([this.auth.getRedirect()]);
    // }
  }

  invalid(): boolean {
    return this.email.invalid || this.password.invalid;
  }

  login() {
    this.aaa.authenticate(this.email.value, this.password.value);
  }

  loginByFacebook() {
    this.user.loginBySocialMedia(ConnectionType.FB).subscribe(_result => {
      console.log(_result);
    });
  }

  loginByGoogle() {
    this.user.loginBySocialMedia(ConnectionType.Google).subscribe(_result => {
      console.log(_result);
    });
  }
}
