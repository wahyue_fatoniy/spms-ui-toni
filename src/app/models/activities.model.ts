export enum ResourceType {
  ENGINEER = 'ENGINEER',
  TECHNICIAN = 'TECHNICIAN',
  DOCUMENTATION = 'DOCUMENTATION'
}

export class Activities {
  private id: number;
  private number: number;
  private type: ResourceType;
  private name: string;

  constructor(input?: Object) {
    if (input) {
      this.id = input['id'];
      this.number = input['number'];
      this.type = input['type'];
      this.name = input['name'];
    }
  }

  public get getId() {
    return this.id;
  }

  public get getNumber() {
    return this.number;
  }
  public get getType() {
    return this.type;
  }

  public get getName() {
    return this.name;
  }
}
