import { Role } from './role.model';
import { Person } from './person.model';
import { DatePipe } from '@angular/common';

const datePipe: DatePipe = new DatePipe('id');


export class AdminCenter extends Role {
  private nip: string;
  protected roleType = 'ADMIN_CENTER';

  constructor(input?: Object) {
    super();
    if (input) {
      this.id = input['id'];
      this.party = new Person(input['party']);
      this.startDate = this.setDate(input['startDate'] ? input['startDate'] : new Date());
      this.endDate = input['endDate'];
      this.nip = input['nip'];

    }
  }

  public get getId() {
    return this.id;
  }

  public get getNIP() {
    return this.nip;
  }

  public get getRoleType() {
    return this.roleType;
  }

  public get getParty() {
    return this.party;
  }

  public get getStartDate()  {
    return this.startDate;
  }

  public get getEndDate() {
    return this.endDate;
  }

  private setDate(date: string | Date): string {
    return datePipe.transform(date, 'yyyy-MM-ddTHH:mm:ssZ', '+0000');
  }
}
