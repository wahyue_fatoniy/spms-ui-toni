import { SiteWork } from "./siteWork.model";
import { Severity } from "src/models/enum/enum";

export class Alarm {
    private id: number;
    private sitework: SiteWork;
    private severity: Severity;
    private actTime: Date;
    private clearTime: Date;
    private description: string;

    constructor(input?: Alarm) {
        if (input) {
            this.id = input['id'];
            this.sitework = input['sitework'] ? new SiteWork(input['sitework']) : null;
            this.severity = input['severity'];
            this.actTime = input['actTime'];
            this.clearTime = input['clearTime'];
            this.description = input['description'];
        }
    }

    public get getId() {
        return this.id;
    }

    public set setId(id: number) {
        this.id = id;
    }

    public get getSitework() {
        return this.sitework;
    }

    public set setSitework(sitework: SiteWork) {
        this.sitework = sitework;
    }

    public get getSeverity() {
        return this.severity;
    }

    public set setSeverity(severity: Severity) {
        this.severity = severity;
    }

    public get getActTime() {
        return this.actTime;
    }

    public set setActTime(actTime: Date) {
        this.actTime = actTime;
    }

    public get getClearTime() {
        return this.clearTime;
    }

    public set setClearTime(clearTime: Date) {
        this.clearTime = clearTime;
    }

    public get getDescription() {
        return this.description;
    }

    public set setDescription(desc: string) {
        this.description = desc;
    }
}