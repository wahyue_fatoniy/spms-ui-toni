import { Customer } from "./customer.model";
import { ProjectManager } from "./projectManager.model";
import { GeneralManager } from "./generalManager.model";
import { FormlyFieldConfig } from "@ngx-formly/core";
import { SiteWork } from "./siteWork.model";
import { DatePipe } from "@angular/common";
import { Observable } from "rxjs";
import { map } from "rxjs/operators";

const datePipe: DatePipe = new DatePipe('id');

export enum AssignmentStatus {
    CREATED = 'CREATED',
    PROPOSED = 'PROPOSED',
    APPROVED = 'APPROVED',
    WORK_COMPLETED = 'WORK_COMPLETED',
    CANCEL = 'CANCEL',
    FINISH = 'FINISH'
}

export class Assignment {
    private id: number;
    private assignmentId: string;
    private contractNo: string;
    private activityId: string;
    private startDate: string;
    private endDate: string;
    private siteWorks: SiteWork[];
    private customer: Customer;
    private projectManager: ProjectManager;
    private generalManager: GeneralManager;
    private status: AssignmentStatus;
    private description: string;

    constructor(input?: Assignment) {
        if (input) {
            this.id = input['id'];
            this.assignmentId = input['assignmentId'];
            this.activityId = input['activityId'];
            this.startDate = input['startDate'];
            this.endDate = input['endDate'];
            this.siteWorks = input['siteWorks'] ? input['siteWorks'].map(sitework => new SiteWork(sitework)) : [];
            this.customer = new Customer(input['customer']);
            this.projectManager = new ProjectManager(input['projectManager']);
            this.generalManager = new GeneralManager(input['generalManager']);
            this.status = input['status'];
        }
    }

    public get getId() {
        return this.id;
    }

    public get getAssignmentId() {
        return this.assignmentId;
    }

    public get getActivityId() {
        return this.activityId;
    }

    public get getStartDate() {
        return this.startDate;
    }

    public get getEndDate() {
        return this.endDate;
    }

    public get getCustomer() {
        return this.customer;
    }

    public get getSiteworks() {
        return this.siteWorks;
    }

    public get getProjectManager() {
        return this.projectManager;
    }

    public get getGeneralManager() {
        return this.generalManager;
    }

    public get getDescription() {
        return this.description;
    }

    public get getStatus() {
        return this.status;
    }

    public set setId(id: number) {
        this.id = id;
    }

    public set setAssignmentId(id: string) {
        this.assignmentId = id;
    }

    public set setStartDate(date: Date) {
        this.startDate = this.setDate(date);
    }

    public set setEndDate(date: Date) {
        this.endDate = this.setDate(date);
    }

    public set setActivityId(id: string) {
        this.activityId = id;
    }

    public set setSiteworks(siteWorks: SiteWork[]) {
        this.siteWorks = siteWorks;
    }

    public set setCustomer(customer: Customer) {
        this.customer = customer;
    }

    public set setProjectManager(projectManager: ProjectManager) {
        this.projectManager = projectManager;
    }

    public set setGeneralManager(generalManager: GeneralManager) {
        this.generalManager = generalManager;
    }

    public set setStatus(status: AssignmentStatus) {
        this.status = status;
    }

    public set setDescription(description: string) {
        this.description = description;
    }

    public get formFieldConfig(): FormlyFieldConfig[] {
        return [
            {
                id: '1',
                fieldGroupClassName: 'row',
                fieldGroup: [
                    {
                        className: 'col-4',
                        key: 'assignmentId',
                        type: 'input',
                        templateOptions: {
                            label: 'Assignment Id',
                            required: true,
                        },
                    },
                    {
                        className: 'col-4',
                        key: 'activityId',
                        type: 'input',
                        templateOptions: {
                            label: 'Activity Id',
                        }
                    },
                    {
                        className: 'col-4',
                        key: 'customer',
                        type: 'select',
                        templateOptions: {
                            label: 'Customer',
                            required: true,
                            options: []
                        }
                    },

                ]
            },
            {
                id: '2',
                fieldGroupClassName: 'row',
                fieldGroup: [
                    {
                        className: 'col-4',
                        key: 'generalManager',
                        type: 'select',
                        templateOptions: {
                            label: 'General Manager',
                            required: true,
                            options: [],
                        }
                    },
                    {
                        className: 'col-4',
                        key: 'startDate',
                        type: 'datepicker',
                        templateOptions: {
                            label: 'Start Date',
                            readonly: true,
                            required: true,
                            datepickeroptions: {
                            },

                        }
                    },
                    {
                        className: 'col-4',
                        key: 'endDate',
                        type: 'datepicker',
                        templateOptions: {
                            label: 'End Date',
                            readonly: true,
                            required: true,
                            datepickeroptions: {
                            }
                        }
                    },
                ]
            },
            // {
            //     key: 'description',
            //     type: 'input',
            //     templateOptions: {
            //         label: 'Description',
            //         // required: true,
            //     }
            // },
        ];

    }
 
    public setCustomerOptions(customers: Observable<Customer[]>): FormlyFieldConfig[] {
        let formFieldConfig = this.formFieldConfig;
        formFieldConfig = formFieldConfig.map(field => {
            if (field.id === '1') {
                field.fieldGroup = field.fieldGroup.map(member => {
                    if (member.key === 'customer') {
                        // if (customers instanceof Observable) {
                            member.templateOptions.options = customers.pipe(
                                map(value => {
                                    value.map(customer => {
                                        return { label: customer.getParty['name'], value: customer.getId }
                                    })
                                    return value;
                                })
                            );
                        // } else {
                        //     member.templateOptions.options = customers.map(customer => {
                        //         return { label: customer.getParty['name'], value: customer.getId }
                        //     })
                        // }
                    }
                    return member;
                });
            }
            return field;
        })
        return formFieldConfig;
    }

    public setGmOptions(generalManagers: Observable<GeneralManager[]>): FormlyFieldConfig[] {
        let formFieldConfig = this.formFieldConfig;
        formFieldConfig = formFieldConfig.map(field => {
            if (field.id === '2') {
                field.fieldGroup = field.fieldGroup.map(member => {
                    if (member.key === 'generalManager') {
                        // if (generalManagers instanceof Observable) {
                            member.templateOptions.options = generalManagers.pipe(
                                map(value => {
                                    value.map(gm => {
                                        return { label: gm.getParty['name'], value: gm.getId }
                                    })
                                    return value;
                                })
                            );
                        // } else {
                        //     member.templateOptions.options = generalManagers.map(gm => {
                        //         return { label: gm.getParty['name'], value: gm.getId }
                        //     })
                        // }
                    }
                    return member;
                });
            }
            return field;
        })
        return formFieldConfig;
    }

    private setDate(date: string | Date): string {
        return datePipe.transform(date, 'yyyy-MM-ddTHH:mm:ssZ', '+0000');
    }

}