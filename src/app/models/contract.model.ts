import { Customer } from "./customer.model";
import { ProjectManager } from "./projectManager.model";
import { GeneralManager } from "./generalManager.model";
import { FormlyFieldConfig } from "@ngx-formly/core";
import { SiteWork } from "./siteWork.model";
import { DatePipe } from "@angular/common";
import { Observable } from "rxjs";
import { map } from "rxjs/operators";

const datePipe: DatePipe = new DatePipe('id');

export enum ContractStatus {
    CREATED = 'CREATED',
    PROPOSED = 'PROPOSED',
    APPROVED = 'APPROVED',
    WORK_COMPLETED = 'WORK_COMPLETED',
    CANCEL = 'CANCEL',
    FINISH = 'FINISH'
}

export class Contract {
    private id: number;
    private contractNumber: string;
    // private name: string;
    private startDate: string;
    private endDate: string;
    private siteWorks: SiteWork[];
    private customer: Customer;
    private projectManager: ProjectManager;
    private generalManager: GeneralManager;
    private status: ContractStatus;
    private description: string;
    private value: number;

    constructor(input?: Contract) {
        if (input) {
            this.id = input['id'];
            // this.name = input['name'];
            this.contractNumber = input['contractNumber'];
            this.startDate = input['startDate'];
            this.endDate = input['endDate'];
            this.siteWorks = input['siteWorks'] ? input['siteWorks'].map(sitework => new SiteWork(sitework)) : [];
            this.customer = new Customer(input['customer']);
            this.projectManager = new ProjectManager(input['projectManager']);
            this.generalManager = new GeneralManager(input['generalManager']);
            this.status = input['status'];
            this.value = input['value']
        }
    }

    public get getNumber() {
        return this.contractNumber;
    }

    public set setNumber(no: string) {
        this.contractNumber = no;
    }

    public get getId() {
        return this.id;
    }

    public get getStartDate() {
        return this.startDate;
    }

    public get getEndDate() {
        return this.endDate;
    }

    public get getCustomer() {
        return this.customer;
    }

    public get getSiteworks() {
        return this.siteWorks;
    }

    public get getProjectManager() {
        return this.projectManager;
    }

    public get getGeneralManager() {
        return this.generalManager;
    }

    public get getDescription() {
        return this.description;
    }

    public get getStatus() {
        return this.status;
    }

    public get getValue() {
        return this.value ? this.value : 0;
    }

    public set setValue(value: number) {
        this.value = value;
    }

    public set setId(id: number) {
        this.id = id;
    }

    public set setStartDate(date: Date) {
        this.startDate = this.setDate(date);
    }

    public set setEndDate(date: Date) {
        this.endDate = this.setDate(date);
    }

    public set setSiteworks(siteWorks: SiteWork[]) {
        this.siteWorks = siteWorks;
    }

    public set setCustomer(customer: Customer) {
        this.customer = customer;
    }

    public set setProjectManager(projectManager: ProjectManager) {
        this.projectManager = projectManager;
    }

    public set setGeneralManager(generalManager: GeneralManager) {
        this.generalManager = generalManager;
    }

    public set setStatus(status: ContractStatus) {
        this.status = status;
    }

    public set setDescription(description: string) {
        this.description = description;
    }

    public get formFieldConfig(): FormlyFieldConfig[] {
        return [
            {
                id: '1',
                fieldGroupClassName: 'row',
                fieldGroup: [
                    {
                        className: 'col-3',
                        key: 'number',
                        type: 'input',
                        templateOptions: {
                            label: 'Number',
                            required: true,
                        },
                    },
                    
                    {
                        className: 'col-3',
                        key: 'customer',
                        type: 'select',
                        templateOptions: {
                            label: 'Customer',
                            required: true,
                            options: []
                        }
                    },
                    {
                        className: 'col-3',
                        key: 'generalManager',
                        type: 'select',
                        templateOptions: {
                            label: 'General Manager',
                            required: true,
                            options: [],
                        }
                    },
                    {
                        className: 'col-3',
                        key: 'value',
                        type: 'input',
                        templateOptions: {
                            label: 'Value',
                            required: true,
                        }
                    },
                ]
            },
            {
                id: '2',
                fieldGroupClassName: 'row',
                fieldGroup: [
                    {
                        className: 'col-2',
                        key: 'startDate',
                        type: 'datepicker',
                        templateOptions: {
                            label: 'Start Date',
                            required: true,
                            datepickeroptions: {
                            },

                        },
                    },
                    {
                        className: 'col-2',
                        key: 'endDate',
                        type: 'datepicker',
                        templateOptions: {
                            label: 'End Date',
                            readonly: true,
                            required: true,
                            datepickeroptions: {
                            }
                        }
                    },
                    {
                        className: 'col-8',
                        key: 'description',
                        type: 'input',
                        templateOptions: {
                            label: 'Description',
                            // required: true,
                        }
                    },
                ]
            },
        ];

    }

    public setCustomerOptions(customers: Observable<Customer[]>): FormlyFieldConfig[] {
        let formFieldConfig = this.formFieldConfig;
        formFieldConfig = formFieldConfig.map(field => {
            if (field.id === '1') {
                field.fieldGroup = field.fieldGroup.map(member => {
                    if (member.key === 'customer') {
                        // if (customers instanceof Observable) {
                        member.templateOptions.options = customers.pipe(
                            map(value => {
                                value.map(customer => {
                                    return { label: customer.getParty['name'], value: customer.getId }
                                })
                                return value;
                            })
                        );
                        // } else {
                        //     member.templateOptions.options = customers.map(customer => {
                        //         return { label: customer.getParty['name'], value: customer.getId }
                        //     })
                        // }
                    }
                    return member;
                });
            }
            return field;
        })
        return formFieldConfig;
    }

    public setGmOptions(generalManagers: Observable<GeneralManager[]>): FormlyFieldConfig[] {
        let formFieldConfig = this.formFieldConfig;
        formFieldConfig = formFieldConfig.map(field => {
            if (field.id === '2') {
                field.fieldGroup = field.fieldGroup.map(member => {
                    if (member.key === 'generalManager') {
                        // if (generalManagers instanceof Observable) {
                        member.templateOptions.options = generalManagers.pipe(
                            map(value => {
                                value.map(gm => {
                                    return { label: gm.getParty['name'], value: gm.getId }
                                })
                                return value;
                            })
                        );
                        // } else {
                        //     member.templateOptions.options = generalManagers.map(gm => {
                        //         return { label: gm.getParty['name'], value: gm.getId }
                        //     })
                        // }
                    }
                    return member;
                });
            }
            return field;
        })
        return formFieldConfig;
    }

    private setDate(date: string | Date): string {
        return datePipe.transform(date, 'yyyy-MM-ddTHH:mm:ssZ', '+0000');
    }

}