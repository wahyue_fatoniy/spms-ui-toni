import { Role } from './role.model';
import { Party } from './party.model';
import { Region } from './region.model';
import { Person } from './person.model';
import { Organization } from './organization.model';
import { DatePipe } from '@angular/common';

const datePipe: DatePipe = new DatePipe('id');

export class Coordinator extends Role {
  protected roleType: string;
  private region: Region;
  private nip: number;

  constructor(input?: Object) {
    super();
    if (input) {
      this.id = input['id'];
        this.startDate = this.setDate(input['startDate'] ? input['startDate'] : new Date());
        this.endDate = input['endDate'];
        this.region = new Region(input['region']);
        this.nip = input['nip'];
        this.setParty = input['party'];
        this.roleType = 'CORDINATOR';
    }
  }

  public get getId() {
    return this.id;
  }

  public set setId(id: number) {
    this.id = id;
  }

  public get getRoleType() {
    return this.roleType;
  }

  public get getParty() {
    return this.party;
  }

  public get getStartDate() {
    return this.startDate;
  }

  public get getEndDate() {
    return this.endDate;
  }

  public get getRegion() {
    return this.region;
  }

  public get getNIP() {
    return this.nip;
  }

  public set setParty(party: Party) {
    if (party) {
      if (this.nip) {
        this.party = new Person(party);
      } else {
        this.party = new Organization(party);
      }
    }
  }


  private setDate(date: string | Date): string {
    return datePipe.transform(date, 'yyyy-MM-ddTHH:mm:ssZ', '+0000');
  }
}
