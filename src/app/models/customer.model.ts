import { Role } from './role.model';
import { Person } from './person.model';
import { Party } from './party.model';
import { Organization } from './organization.model';
import { DatePipe } from '@angular/common';

const datePipe: DatePipe = new DatePipe('id');


export class Customer extends Role {
  protected roleType = 'CUSTOMER';

  constructor(input?: Object) {
    super();
    if (input) {
      this.id = input['id'];
      this.setParty = input['party'];
       this.startDate = this.setDate(input['startDate'] ? input['startDate'] : new Date());
      this.endDate = input['endDate'];
    }
  }

  public get getId() {
    return this.id;
  }

  public get getRoleType() {
    return this.roleType;
  }

  public get getParty() {
    return this.party;
  }

  public get getStartDate() {
    return this.startDate;
  }

  public get getEndDate() {
    return this.endDate;
  }

  public set setParty(party: Party) {
    if (party) {
      if (party['partyType'] === 'PERSON') {
        this.party = new Person(party);
      } else if (party['partyType'] === 'ORGANIZATION') {
        this.party =  new Organization(party);
      } else {
        this.party = party;
      }
    }
  }

  private setDate(date: string | Date): string {
    return datePipe.transform(date, 'yyyy-MM-ddTHH:mm:ssZ', '+0000');
  }
}
