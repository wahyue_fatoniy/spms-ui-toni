import { SiteWork } from "./siteWork.model";
import { Task } from "./task.model";
import { Resource } from "./resource.model";
import { DatePipe } from "@angular/common";

const datePipe: DatePipe = new DatePipe('id');

export class DailyPlan {
    private id: number;
    private sitework: SiteWork;
    private tasks: Task[];
    private date: string;
    private resources: Resource[];
    private opex: number;
    private note: string;

    constructor(input?: DailyPlan) {
        if (input) {
            this.id = input['id'];
            this.sitework = input['sitework'] ? new SiteWork(input['sitework']) : null,
                this.tasks = input['tasks'] ? input['tasks'].map(task => new Task(task)) : [],
                this.date = input['date'] ? input['date'] : null,
                this.resources = input['resources'] ? input['resources'] : [],
                this.opex = input['opex'] ? input['opex'] : 0;
                this.note = input['note'];
        }
    }

    public get getId() {
        return this.id;
    }

    public get getSitework() {
        return this.sitework;
    }

    public get getTasks() {
        return this.tasks ? this.tasks : [];
    }

    public get getDate() {
        return this.date;
    }

    public get getResources() {
        return this.resources ? this.resources : [];
    }

    public get getOpex() {
        return this.opex;
    }

    public set setId(id: number) {
        this.id = id;
    }

    public set setSitework(sitework: SiteWork) {
        this.sitework = sitework;
    }

    public set setTasks(tasks: Task[]) {
        this.tasks = tasks;
    }

    public set setDate(date: Date) {
        this.date = this.formatDate(date);
    }

    public set setResources(resources: Resource[]) {
        this.resources = resources;
    }

    public set setOpex(opex: number) {
        this.opex = opex;
    }

    public get getNote() {
        return this.note;
    }

    public set setNote(note: string) {
        this.note = note;
    }

    private formatDate(date: string | Date): string {
        return datePipe.transform(date, 'yyyy-MM-ddTHH:mm:ssZ', '+0000');
    }
}