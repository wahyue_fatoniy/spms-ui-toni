import { SiteWork } from './siteWork.model';
import { TaskExecution } from './taskExecution.model';
import { DatePipe } from '@angular/common';

const datePipe: DatePipe = new DatePipe('id');

export class DailyReport {
  private id: number;
  private siteWork: SiteWork;
  private taskExecutions: TaskExecution[];
  private submit: boolean;
  private date: string;
  private note: string;

  constructor(input?: Object) {
    if (input) {
      this.id = input['id'];
      this.siteWork = new SiteWork(input['siteWork']);
      this.taskExecutions = input['taskExecutions'] ? input['taskExecutions'].map(task => new TaskExecution(task)) : [];
      this.submit = input['submit'];
      this.date = input['date'];
    }
  }

  public get getId() {
    return this.id;
  }

  public set setId(id: number) {
    this.id = id;
  }

  public get getSiteWork() {
    return this.siteWork;
  }

  public set setSitework(sitework: SiteWork) {
    this.siteWork = sitework;
  }

  public get getTaskExecutions() {
    return this.taskExecutions ? this.taskExecutions : [];
  }

  public set setTasexecutions(taskExecutions: TaskExecution[]) {
    this.taskExecutions = taskExecutions;
  }

  public get getSubmit() {
    return this.submit;
  }

  public set setSubmit(submit: boolean) {
    this.submit = submit;
  }

  public get getDate() {
    return this.date;
  }

  public set setDate(date: Date) {
    this.date = this.formatDate(date);
  }

  public get getNote() {
    return this.note;
  }

  public set setNote(note: string) {
    this.note = note;
  }

  private formatDate(date: string | Date): string {
    return datePipe.transform(date, 'yyyy-MM-ddTHH:mm:ssZ', '+0000');
  }

}
