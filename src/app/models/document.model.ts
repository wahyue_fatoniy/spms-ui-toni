import { FormlyFieldConfig } from "@ngx-formly/core";
import { Customer } from "./customer.model";
import { Documentation } from "./documentation.model";

export enum DocumentStatus {
  create = 'CREATE',
  submit = 'SUBMIT',
  revision = 'REVISION',
  reject = 'REJECT',
  approve = 'APPROVE',
  rti = 'RTI'
}

export enum DocumentType {
  ATP = 'ATP',
  COMPLETENESS_ATP = 'COMPLETENESS_ATP',
  BAUT = 'BAUT',
  BAST = 'BAST'
}

export class Document {
  private id: number;
  private noDocument: string;
  private title: string;
  private statusDocument: DocumentStatus;
  private author: string;
  private nip: string;
  private reviewer: string;
  private documentType: DocumentType;


  constructor(input?: Object) {
    if (input) {
      this.id = input['id'];
      this.noDocument = input['noDocument'];
      this.title = input['title'];
      this.statusDocument = input['statusDocument'];
      this.author = input['author'];
      this.nip = input['nip'];
      this.reviewer = input['reviewer'];
      this.documentType = input['documentType'];
    }
  }

  public get getId() {
    return this.id;
  }

  public get getNumber() {
    return this.noDocument;
  }

  public get getTitle() {
    return this.title;
  }

  public get getStatus() {
    return this.statusDocument;
  }

  public get getAuthor() {
    return this.author;
  }

  public get getNIP() {
    return this.nip;
  }

  public get getReviewer() {
    return this.reviewer;
  }

  public set setId(id: number) {
    this.id = id;
  }

  public set setNumber(no: string) {
    this.noDocument = no;
  }

  public set setTitle(title: string) {
    this.title = title;
  }

  public set setStatus(status: DocumentStatus) {
    this.statusDocument = status;
  }

  public set setNip(nip: string) {
    this.nip = nip;
  }

  public set setReviewer(reviewer: string) {
    this.reviewer = reviewer;
  }

  public set setAuthor(author: string) {
    this.author = author;
  }

  public get getType() {
    return this.documentType;
  }

  public set setType(type: DocumentType) {
    this.documentType = type;
  }

  public get formFieldsConfig(): FormlyFieldConfig[] {
    return [
      {
        id: '1',
        fieldGroupClassName: 'row',
        fieldGroup: [
          {
            className: 'col-6',
            key: 'noDocument',
            type: 'input',
            templateOptions: {
              placeholder: 'Number',
              required: true,
            }
          },
          {
            className: 'col-6',
            key: 'title',
            type: 'input',
            templateOptions: {
              placeholder: 'Title',
              required: true,
            }
          }
        ]
      },
      {
        id: '3',
        fieldGroupClassName: 'row',
        fieldGroup: [
          {
            className: 'col-6',
            key: 'reviewer',
            type: 'select',
            templateOptions: {
              placeholder: 'Reveiwer',
              required: true,
              options: []
            }
          },
          {
            className: 'col-6',
            key: 'documentType',
            type: 'select',
            templateOptions: {
              placeholder: 'Document Type',
              required: true,
              options: [],
            }
          }
        ]
      },
      {
        id: '2',
        fieldGroupClassName: 'row',
        fieldGroup: [
          {
            className: 'col-6',
            key: 'author',
            type: 'select',
            templateOptions: {
              placeholder: 'Author',
              required: true,
              options: [],
            },
          },
          {
            className: 'col-6',
            key: 'nip',
            type: 'input',
            templateOptions: {
              placeholder: 'NIP',
              required: true,
            },
          },
        ]
      },
    ];
  }


  public setReviewerOptions(customers: Customer[]): FormlyFieldConfig[] {
    return this.formFieldsConfig.map(field => {
      if (field.id === '3') {
        field.fieldGroup = field.fieldGroup.map(group => {
          if (group.key === 'reviewer') {
            group.templateOptions.options = customers.map(customer => {
              return { label: customer.getParty['name'], value: customer.getId }
            });
          }
          return group;
        });
      }
      return field;
    });
  }

  public setDocTypeOptions(docTypes: { value: any, label: string }[]): FormlyFieldConfig[] {
    return this.formFieldsConfig.map(field => {
      if (field.id === '3') {
        field.fieldGroup = field.fieldGroup.map(group => {
          if (group.key === 'documentType') {
            group.templateOptions.options = docTypes;
          }
          return group;
        });
      }
      return field;
    });
  }

  public setAuthorOptions(documentations: Documentation[]): FormlyFieldConfig[] {
    return this.formFieldsConfig.map(field => {
      if (field.id === '2') {
        field.fieldGroup = field.fieldGroup.map(group => {
          if (group.key === 'author') {
            group.templateOptions.options = documentations.map(doc => {
              return { value: doc.getId, label: doc.getParty['name'] }
            });;
          }
          return group;
        });
      }
      return field;
    });
  }

}

export class DocumentLifecycle {
  private id: number;
  private document: Document;
  private date: Date;
  private description: string;
  private statusDocument: DocumentStatus;

  constructor(input?: Object) {
    if (input) {
      this.id = input['id'];
      this.document = new Document(input['document']);
      this.date = input['date'];
      this.description = input['description'];
      this.statusDocument = input['statusDocument'];
    }
  }

  public get getId() {
    return this.id;
  }

  public get getDocument() {
    return this.document;
  }

  public get getDate() {
    return this.date;
  }

  public get getDescription() {
    return this.description;
  }

  public set setId(id: number) {
    this.id = id;
  }

  public set setDocument(document: Document) {
    this.document = document;
  }

  public set setDate(date: Date) {
    this.date = date;
  }

  public set setDescription(description: string) {
    this.description = description;
  }

  public get getStatusDoc() {
    return this.statusDocument;
  }

  public set setStatusDoc(status: DocumentStatus) {
    this.statusDocument = status;
  }
}

