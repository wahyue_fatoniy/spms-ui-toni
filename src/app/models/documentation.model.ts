import { Person } from './person.model';
import { Resource } from './resource.model';
import { Region } from './region.model';
import { DatePipe } from '@angular/common';

const datePipe: DatePipe = new DatePipe('id');


export class Documentation extends Resource {
  private nip: string;
  private region: Region;
  protected roleType = 'DOCUMENTATION';
  protected resourceType = 'DOCUMENTATION';

  constructor(input?: Object) {
    super();
    if (input) {
      this.id = input['id'];
      this.party = new Person(input['party']);
      this.startDate = this.setDate(input['startDate'] ? input['startDate'] : new Date());
      this.endDate = input['endDate'];
      this.nip = input['nip'];
      this.region = new Region(input['region']);
    }
  }

  public get getId() {
    return this.id;
  }

  public get getNIP() {
    return this.nip;
  }

  public get getRoleType() {
    return this.roleType;
  }

  public getResourceType() {
    return this.resourceType;
  }

  public get getParty() {
    return this.party;
  }

  public get getStartDate()  {
    return this.startDate;
  }

  public get getEndDate() {
    return this.endDate;
  }

  public get getRegion() {
    return this.region;
  }

  private setDate(date: string | Date): string {
    return datePipe.transform(date, 'yyyy-MM-ddTHH:mm:ssZ', '+0000');
  }
}
