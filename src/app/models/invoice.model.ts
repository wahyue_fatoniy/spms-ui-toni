import { SiteWork } from './siteWork.model';
import { PurchaseOrder } from './purchaseOrder.model';
import { DatePipe } from '@angular/common';
import { FormlyFieldConfig } from '@ngx-formly/core';

const datePipe: DatePipe = new DatePipe('id');

export class Invoice {
  private id: number;
  private invoiceNumber: string;
  private invoiceDate: string;
  private dueDate: string;
  private invoiceValue: number;
  private referenceBast: string;
  private paymentDate: string;
  private submit: number;
  private purchaseOrder: PurchaseOrder;
  private siteWorks: SiteWork[];

  constructor(input?: Object) {
    if (input) {
      this.id = input['id'];
      this.invoiceNumber = input['invoiceNumber'];
      this.invoiceDate = input['invoiceDate'] ? this.setDate(input['invoiceDate']) : null;
      this.purchaseOrder = new PurchaseOrder(input['po']);
      this.siteWorks = input['siteWorks']
        ? input['siteWorks'].map(val => new SiteWork(val))
        : [];
      this.dueDate = this.setDate(input['dueDate']);
      this.paymentDate = input['paymentDate'] ? this.setDate(input['paymentDate']) : null;
      this.invoiceValue = input['invoiceValue'];
      this.referenceBast = input['referenceBast'];
      this.submit = input['submit'];
    }
  }

  public get getId() {
    return this.id;
  }

  public set setId(id: number) {
    this.id = id;
  }

  public get getInvoiceNumber() {
    return this.invoiceNumber;
  }

  public set setInvoiceNumber(number: string) {
    this.invoiceNumber = number;
  }

  public get getInvoiceDate() {
    return this.invoiceDate;
  }

  public set setInvoiceDate(date: Date) {
    this.invoiceDate = this.setDate(date);
  }

  public get getPo() {
    return this.purchaseOrder;
  }

  public set setPo(po: PurchaseOrder) {
    this.purchaseOrder = po;
  }

  public get getSiteWorks() {
    return this.siteWorks;
  }

  public set setSiteWorks(siteworks: SiteWork[]) {
    this.siteWorks = siteworks;
  }

  public get getDueDate() {
    return this.dueDate;
  }

  public set setDueDate(date: Date) {
    this.dueDate = this.setDate(date);
  }

  public get getPaymentDate() {
    return this.paymentDate;
  }

  public set setPaymentDate(date: string) {
    this.paymentDate = this.setDate(date);
  }

  public get getInvoiceValue() {
    return this.invoiceValue;
  }

  public set setInvoiceValue(val: number) {
    this.invoiceValue = val;
  }

  public get getRefBast() {
    return this.referenceBast;
  }

  public set setRefBast(bast: string) {
    this.referenceBast = bast;
  }

  public get getSubmit() {
    return this.submit;
  }

  public set setSubmit(submit: number) {
    this.submit = submit;
  }

  private setDate(date: string | Date): string {
    return datePipe.transform(date, 'yyyy-MM-ddTHH:mm:ssZ', '+0000');
  }

  public get formFieldConfig(): FormlyFieldConfig[] {
    return [
      {
        id: 'row_1',
        fieldGroupClassName: 'row',
        fieldGroup: [
          {
            className: 'col-6',
            key: 'invoiceNumber',
            type: 'input',
            templateOptions: {
              placeholder: 'Invoice Number',
              required: true,
            }
          },
          {
            className: 'col-6',
            key: 'invoiceValue',
            type: 'input',
            templateOptions: {
              placeholder: 'Invoice Value',
              required: true,
            },
          }
        ]
      },
      {
        id: 'row_2',
        fieldGroupClassName: 'row',
        fieldGroup: [
          {
            className: 'col-6',
            key: 'invoiceDate',
            type: 'datepicker',
            templateOptions: {
              placeholder: 'Invoice Date',
              required: true,
              readonly: true,
            }
          },
          {
            className: 'col-6',
            key: 'dueDate',
            type: 'datepicker',
            templateOptions: {
              placeholder: 'Due Date',
              required: true,
              readonly: true
            }
          }
        ]
      },
      {

        id: 'row_3',
        fieldGroupClassName: 'row',
        fieldGroup: [
          {
            className: 'col-6',
            key: 'referenceBast',
            type: 'input',
            templateOptions: {
              placeholder: 'Reference BAST',
              required: true,
            }
          },
        ]
      }
    ]
  }
}
