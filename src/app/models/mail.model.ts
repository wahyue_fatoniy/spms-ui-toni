export class Mail {
    private to: string;
    private cc: string[];
    private subject: string;
    private text: string;

    constructor() {}

    set _to(to: string) {
        this.to = to
    }

    set _cc(cc: string[]) {
        this.cc = cc;
    }

    set _subject(subject: string) {
        this.subject = subject;
    }

    set _text(text: string) {
        this.text = text;
    }

    get _to() {
        return this.to;
    }

    get _cc() {
        return this.cc;
    }

    get _subject() {
        return this.subject;
    }

    get _text() {
        return this.text;
    }
}
// finance@satunol.com
// admincenter@satunol.com

// apabila tidak ada email yang dituju