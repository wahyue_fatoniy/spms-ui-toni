import { SiteWork } from "./siteWork.model";

export class Opex {
    private id: number;
    private sitework: SiteWork;
    private date: Date;
    private proposeValue: number;
    private approveValue: number;
    private description: string;

    constructor(input?: Opex) {
        if (input) {
            this.id = input['id'];
            this.sitework = input['sitework'] ? new SiteWork(input['sitework']) : null;
            this.date = input['date'];
            this.proposeValue = input['proposeValue'] ? input['proposeValue'] : 0;
            this.approveValue = input['approveValue'] ? input['approveValue'] : 0;
            this.description = input['description'];
        }
    }

    public get getId() {
        return this.id;
    }

    public set setId(id: number) {
        this.id = id;
    }

    public get getSitework() {
        return this.sitework;
    }

    public set setSitework(sitework: SiteWork) {
        this.sitework = sitework;
    }

    public get getDate() {
        return this.date;
    }

    public set setDate(date: Date) {
        this.date = date;
    }

    public get getProposeValue() {
        return this.proposeValue;
    }

    public set setProposeValue(value: number) {
        this.proposeValue = value;
    }

    public get getApproveValue() {
        return this.approveValue;
    }

    public set setApproveValue(value: number) {
        this.approveValue = value;
    }

    public get getDescription() {
        return this.description;
    }

    public set setDescription(desc: string) {
        this.description = desc;
    }
}