import { Party } from './party.model';
import { Role } from './role.model';

export class Organization extends Party {
  protected partyType = 'ORGANIZATION';
  private roles: Role[];

  constructor(input?: Object) {
    super();
    if (input) {
      this.id = input['id'];
      this.name = input['name'];
      this.address = input['address'];
      this.email = input['email'];
      this.phone = input['phone'];
      this.roles = input['roles'];
    }
  }

  public get getId() {
    return this.id;
  }

  public get getName() {
    return this.name;
  }

  public get getRoles() {
    return this.roles;
  }

  public get getPhone() {
    return this.phone;
  }

  public get getAddress() {
    return this.address;
  }

  public get getEmail() {
    return this.email;
  }

  public set setRoles(roles: Role[]) {
    this.roles = roles;
  }
}
