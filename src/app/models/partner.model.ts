import { Role } from './role.model';
import { Organization } from './organization.model';

export class Partner extends Role {
  protected roleType = 'PARTNER';

  constructor(input?: Object) {
    super();
    if (input) {
      this.id = input['id'];
      this.party = new Organization(input['party']);
      this.startDate = input['startDate'];
      this.endDate = input['endDate'];
    }
  }

  public get getId() {
    return this.id;
  }

  public get getRoleType() {
    return this.roleType;
  }

  public get getParty() {
    return this.party;
  }

  public get getStartDate()  {
    return this.startDate;
  }

  public get getEndDate() {
    return this.endDate;
  }
}
