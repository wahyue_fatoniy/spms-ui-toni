export abstract class Party {
  protected id: number;
  protected name: string;
  protected address: string;
  protected email: string;
  protected phone: string;
  protected birthDate: string;
  protected abstract partyType: string;
}
