import { Party } from './party.model';
import { Role } from './role.model';
import { DatePipe } from '@angular/common';

enum Gender {
  MALE = 'MALE',
  FEMALE = 'FEMALE'
}

const datePipe: DatePipe = new DatePipe('id');

export class Person extends Party {
  private firstName: string;
  private midName: string;
  private lastName: string;
  private gender: string;
  private roles: Role[];
  protected partyType = 'PERSON';

  constructor(input?: Object) {
    super();
    if (input) {
      this.id = input['id'];
      this.roles = input['roles'] ? input['roles'] : [];
      this.name = input['name'];
      this.phone = input['phone'];
      this.address = input['address'];
      this.gender = Gender[input['gender']];
      this.email = input['email'];
      this.birthDate = this.setDate(input['birthDate']);
      this.setName = input['name'];
    }
  }


  public get getId() {
    return this.id;
  }

  public get getName() {
    return this.name;
  }

  public get getRoles() {
    return this.roles ? this.roles : [];
  }

  public get getPhone() {
    return this.phone;
  }

  public get getAddress() {
    return this.address;
  }

  public get getEmail() {
    return this.email;
  }

   public get getBirthDate() {
    return this.birthDate;
  }

  public get getGender() {
    return this.gender;
  }

  public get getFirstName() {
    return this.firstName;
  }

  public get getMidName() {
    return this.midName;
  }

  public get getLastName() {
    return this.lastName;
  }

  public set setRoles(roles: any[]) {
    this.roles = roles;
  }

  private setDate(date: string | Date): string {
    return datePipe.transform(date, 'yyyy-MM-ddTHH:mm:ssZ', '+0000');
  }


  private set setName(value: string) {
    if (value) {
      const names = value.split(' ');
      const set = name => (name ? name : ' ');
      (this.firstName = set(names.shift())),
        (this.lastName = set(names.pop())),
        (this.midName = set(names.join(' ')));
    }
  }
}
