import { Customer } from './customer.model';
import { Partner } from './partner.model';
import { Sales } from './sales.model';
import { SiteWork } from './siteWork.model';
import { DatePipe } from '@angular/common';
import { FormlyFieldConfig } from '@ngx-formly/core';

const datePipe: DatePipe = new DatePipe('id');

export enum PoStatus {
  Progress = "ON_PROGRESS",
  Done = "DONE",
  NotStarted = "NOTSTARTED",
  Cancel = "CANCEL"
}

export class PurchaseOrder {
  private id: number;
  private poNumber: string;
  private publishDate: string;
  private expiredDate: string;
  private totalInvoice: number;
  private cancel: boolean;
  private submit: boolean;
  private customer: Customer;
  private partner: Partner;
  private sales: Sales;
  private siteWorks: SiteWork[];
  private formFieldConfigs: FormlyFieldConfig[];
  private totalProjectValue: number;

  constructor(input?: Object) {
    if (input) {
      (this.id = input['id']), (this.poNumber = input['poNumber']);
      this.publishDate = this.setDate(input['publishDate']);
      this.expiredDate = this.setDate(input['expiredDate']);
      this.totalInvoice = input['totalInvoice'] ? input['totalInvoice'] : 0;
      this.cancel = input['cancel'];
      this.submit = input['submit'];
      this.siteWorks =
        input['siteWorks'] instanceof Array
          ? input['siteWorks'].map(project => new SiteWork(project))
          : [];
      this.customer = new Customer(input['customer']);
      this.sales = new Sales(input['sales']);
      this.partner = new Partner(input['partner']);
      this.totalProjectValue = input['totalProjectValue'];
    }
    this.setFormFieldConfigs();
  }

  public get getId() {
    return this.id;
  }

  public set setId(id: number) {
    this.id = id;
  }

  public get getPoNumber() {
    return this.poNumber;
  }

  public set setPoNumber(number: string) {
    this.poNumber = number;
  }

  public get getPoValue() {
    return this.totalProjectValue;
  }

  public set setPoValue(val: number) {
    this.totalProjectValue = val;
  }

  public get getPublishDate() {
    return this.publishDate;
  }

  public set setPublishDate(date: Date) {
    this.publishDate = this.setDate(date);
  }

  public get getExpiredDate() {
    return this.expiredDate;
  }

  public set setExpiredDate(date: Date) {
    this.expiredDate = this.setDate(date);
  }

  public get getCustomer() {
    return this.customer;
  }

  public set setCustomer(customer: Customer) {
    this.customer = customer;
  }

  public get getTotalInvoice() {
    return this.totalInvoice;
  }

  public set getTotalInvoice(val: number) {
    this.totalInvoice = val;
  }

  public get getCancel() {
    return this.cancel;
  }

  public get getSubmit() {
    return this.submit;
  }

  public get getPartner() {
    return this.partner;
  }

  public set setPartner(partner: Partner) {
    this.partner = partner;
  }

  public get getSales() {
    return this.sales;
  }

  public set setSales(sales: Sales) {
    this.sales = sales;
  }

  public get getSiteWorks() {
    return this.siteWorks;
  }

  public set setSiteWorks(siteworks: SiteWork[]) {
    this.siteWorks = siteworks;
  }

  public get getStatus(): PoStatus {
    let status: PoStatus;
    if (this.cancel) {
      status = PoStatus.Cancel;
    } else if (
      (this.totalInvoice === this.totalProjectValue ||
        this.totalInvoice > this.totalProjectValue) &&
      this.totalInvoice !== 0
    ) {
      status = PoStatus.Done;
    } else if (
      this.totalInvoice !== 0 &&
      this.totalInvoice < this.totalProjectValue
    ) {
      status = PoStatus.Progress;
    } else {
      status = PoStatus.NotStarted;
    }
    return status;
  }


  private setFormFieldConfigs() {
    this.formFieldConfigs = [
      {
        id: '1',
        fieldGroupClassName: 'row',
        fieldGroup: [
          {
            className: 'col-4',
            key: 'poNumber',
            type: 'input',
            templateOptions: {
              placeholder: 'PO Number',
              required: true,
            }
          },
          {
            className: 'col-4',
            key: 'publishDate',
            type: 'datepicker',
            templateOptions: {
              placeholder: 'Publish Date',
              required: true,
            }
          },
          {
            className: 'col-4',
            key: 'expiredDate',
            type: 'datepicker',
            templateOptions: {
              placeholder: 'Expired Date',
              required: true,
            }
          }
        ]
      },
      {
        id: '2',
        fieldGroupClassName: 'row',
        fieldGroup: [
          {
            className: 'col-4',
            key: 'customer',
            type: 'select',
            templateOptions: {
              placeholder: 'Customer',
              required: true,
              options: []
            }
          },
          {
            className: 'col-4',
            key: 'partner',
            type: 'select',
            templateOptions: {
              placeholder: 'Partner',
              required: true,
              options: []
            }
          },
          {
            className: 'col-4',
            key: 'sales',
            type: 'select',
            templateOptions: {
              placeholder: 'Sales',
              required: true,
              options: []
            }
          }
        ]
      },
      {
        id: '3',
        fieldGroupClassName: 'row',
        fieldGroup: [
          {
            className: 'col-3',
            key: 'poValue',
            type: 'input',
            templateOptions: {
              placeholder: 'PO Value',
              required: true,
            }
          },
          {
            className: 'col-9',
            key: 'siteworks',
            type: 'select',
            templateOptions: {
              placeholder: 'Project',
              required: true,
              multiple: true,
              options: []
            }
          }
        ]
      }
    ]
  }

  public get getFormFieldConfigs() {
    return this.formFieldConfigs;
  }

  public set setCustomerOptions(customers: Customer[]) {
    this.formFieldConfigs = this.formFieldConfigs.map(field => {
      if (field.id === '2') {
        field.fieldGroup = field.fieldGroup.map(member => {
          if (member.key === 'customer') {
            member.templateOptions.options = customers.map(customer => {
              return { value: customer.getId, label: customer.getParty['name'] }
            });
          }
          return member;
        });
      }
      return field;
    });
  }

  public set setPartnerOptions(partners: Partner[]) {
    this.formFieldConfigs = this.formFieldConfigs.map(field => {
      if (field.id === '2') {
        field.fieldGroup = field.fieldGroup.map(member => {
          if (member.key === 'partner') {
            member.templateOptions.options = partners.map(partner => {
              return { value: partner.getId, label: partner.getParty['name'] }
            });
          }
          return member;
        });
      }
      return field;
    });
  }

  public set setSalesOptions(sales: Sales[]) {
    this.formFieldConfigs = this.formFieldConfigs.map(field => {
      if (field.id === '2') {
        field.fieldGroup = field.fieldGroup.map(member => {
          if (member.key === 'sales') {
            member.templateOptions.options = sales.map(val => {
              return { value: val.getId, label: val.getParty['name'] }
            });
          }
          return member;
        });
      }
      return field;
    });
  }

  public set setSiteworkOptions(siteworks: SiteWork[]) {
    this.formFieldConfigs = this.formFieldConfigs.map(field => {
      if (field.id === '3') {
        field.fieldGroup = field.fieldGroup.map(member => {
          if (member.key === 'siteworks') {
            member.templateOptions.options = siteworks.map(val => {
              return { value: val.getId, label: val.getSite.getName }
            });
          }
          return member;
        });
      }
      return field;
    });
  }

  private setDate(date: string | Date): string {
    return datePipe.transform(date, 'yyyy-MM-ddTHH:mm:ssZ', '+0000');
  }
}
