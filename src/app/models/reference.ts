export enum referenceType {
  assignmentId = 'ASSIGNMENT_ID',
  activityId = 'ACTIVITY_ID',
  reference = 'REFERENCE',
}

export class Reference {
  private id: number;
  private referenceType: referenceType;
  private description: string;

  constructor(input?: Reference) {
    Object.assign(this, input);
  }

  get getId() {
    return this.id;
  }

  set setId(id: number) {
    this.id = id;
  }

  get getReferenceType() {
    return this.referenceType;
  }

  set setReferenceType(reference: referenceType) {
    this.referenceType = reference;
  }

  get getDescription() {
    return this.description;
  }

  set setDescription(desc: string) {
    this.description = desc;
  }
}
