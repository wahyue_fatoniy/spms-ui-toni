export class Region {
  private id: number;
  private name: string;
  private latitude: number;
  private longitude: number;

  constructor(input?: Object) {
    if (input) {
        this.id = input['id'];
        this.name = input['name'];
        this.latitude = input['latitude'];
        this.longitude = input['longitude'];
    }
  }

  public get getId() {
    return this.id;
  }

  public get getName() {
    return this.name;
  }

  public get getLatitude() {
    return this.latitude;
  }

  public get getLongitude() {
    return this.longitude;
  }
}
