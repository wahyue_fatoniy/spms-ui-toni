import { DatePipe } from "@angular/common";
import { User } from "./user.model";

export enum RequestStatus {
    accepted = "ACCEPTED",
    rejected = 'REJECTED',
    proposed = 'PROPOSED',
    idle = 'IDLE',
}

export enum RequestType {
    requested_project_id = 'REQUESTED_PROJECT_ID',
    submitted_assignment = 'SUBMITED_ASSIGNMENT',
    correction_project_id = 'CORRECTION_PROJECT_ID',
    requested_opex = 'REQUESTED_OPEX',
    submited_contract = 'REQUESTED_CONTRACT',
}

const datePipe: DatePipe = new DatePipe('id');

export class Request {
    private id: number;
    private startDate: string;
    private endDate: string;
    private object: any;
    private requester: User;
    private approver: User;
    private description: string;
    private type: RequestType;
    private status: RequestStatus;

    constructor(input?: Request) {
        if (input) {
            this.id = input['id'];
            this.startDate = input['startDate'];
            this.endDate = input['endDate'];
            this.object = input['object'];
            this.requester = input['requester'];
            this.approver = input['approver'];
            this.description = input['description'];
            this.type = input['type'];
            this.status = input['status'] ? input['status'] : RequestStatus.idle;
        }
    }

    public get getType(): string {
        return this.type;
    }

    public set setType(value: RequestType) {
        this.type = value;
    }

    public get getStatus(): RequestStatus {
        return this.status;
    }

    public set setStatus(value: RequestStatus) {
        this.status = value;
    }


    public get getId(): number {
        return this.id;
    }

    public set setId(value: number) {
        this.id = value;
    }

    public get getStartDate(): string {
        return this.startDate;
    }

    public set setStartDate(value: Date) {
        this.startDate = this.setDate(value);
    }

    public get getEndDate(): string {
        return this.endDate;
    }

    public set setEndDate(value: Date) {
        this.endDate = this.setDate(value);
    }

    public get getObject(): any {
        return this.object;
    }

    public set setObject(value: any) {
        this.object = value;
    }

    public get getRequester(): any {
        return this.requester;
    }

    public set setRequester(value: any) {
        this.requester = value;
    }

    public get getApprover(): User {
        return this.approver;
    }

    public set setApprover(value: User) {
        this.approver = value;
    }

    public get getDescription(): string {
        return this.description;
    }

    private setDate(date: string | Date): string {
        return datePipe.transform(date, 'yyyy-MM-ddTHH:mm:ss', '+0000') + 'Z';
    }

    public set setDescription(value: string) {
        this.description = value;
    }
}
