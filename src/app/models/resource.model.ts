import { Role } from './role.model';

export abstract class Resource extends Role {
  protected abstract resourceType: string;
}
