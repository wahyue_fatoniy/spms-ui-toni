import { Party } from './party.model';

export abstract class Role {
  protected id: number;
  protected party: Party;
  protected startDate: string;
  protected endDate: string;
  protected abstract roleType: string;
}
