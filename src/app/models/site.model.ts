import { Region } from './region.model';
import { Area } from './area.model';

export class Site {
  private id: number;
  private siteCode: string;
  private name: string;
  private region: Region;
  private area: Area;
  private latitude: number;
  private longitude: number;

  constructor(input?: Object) {
    if (input) {
      this.id = input['id'];
      this.siteCode = input['siteCode'];
      this.name = input['name'];
      this.region = new Region( input['region']);
      this.area = new Area(input['area']);
      this.latitude = input['latitude'];
      this.longitude = input['longitude'];
    }
  }

  public get getId() {
    return this.id;
  }

  public get getSiteCode() {
    return this.siteCode;
  }

  public get getName() {
    return this.name;
  }

  public get getRegion() {
    return this.region;
  }

  public get getArea() {
    return this.area;
  }

  public get getLatitude() {
    return this.latitude;
  }

  public get getLongitude() {
    return this.longitude;
  }
}
