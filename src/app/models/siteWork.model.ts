import { Site } from './site.model';
import { Team } from './team.model';
import { Admin } from './admin.model';
import { ProjectManager } from './projectManager.model';
import { DatePipe } from '@angular/common';
import { Document } from './document.model';
import { AdminCenter } from './adminCenter.model';
import { projectStatus, Severity } from 'src/service/enum';
import { Reference } from './reference';
import { SowType } from './sow-type.model';
import { Task } from './task.model';
import { Coordinator } from './coordinator.model';

const datePipe: DatePipe = new DatePipe('id');

export class SiteWork {
  private id: number;
  private projectCode: string;
  private assignmentCode: string;
  private contractNumberData: string;
  private startDate: string;
  private endDate: string;
  private status: projectStatus;
  private poValue: number;
  private site: Site;
  private team: Team;
  private admin: Admin;
  private projectManager: ProjectManager;
  private totalOpex: number;
  private documents: Document[];
  private adminCenter: AdminCenter;
  // private severity: Severity;
  private estimatedProjectValue: number;
  private progress: number;
  private references: Reference[];
  private sowTypes: SowType[];
  private cordinator: Coordinator;

  // temporary member
  private createdDate: string;
  private totalInvoice: number;
  private totalPoValue: number;

  constructor(input?: Object) {
    if (input) {
      console.log(input);
        this.id = input['id'];
        this.projectCode = input['projectCode'];
        this.contractNumberData = input['contractNumberData'];
        this.startDate = input['startDate'];
        this.endDate = input['endDate'];
        this.status = input['status'] ? input['status'] : 'NOT_STARTED';
        this.poValue = input['poValue'] ? input['poValue'] : 0;
        this.site = input['site'] ? new Site(input['site']) : null;
        this.team = input['team'] ? new Team(input['team']) : null;
        this.admin = input['admin'] ? new Admin(input['admin']) : null;
        this.projectManager = input['projectManager'] ? new ProjectManager(input['projectManager']) : null;
        this.createdDate = input['createdDate'];
        this.adminCenter = input['adminCenter'] ? new AdminCenter(input['adminCenter']) : null;
        this.documents = input['documents'] ? input['documents'].map(doc => new Document(doc)) : null;
        this.totalOpex = input['totalOpex'] ? input['totalOpex'] : 0;
        // this.severity = input['severity'] ? input['severity'] : 'NORMAL'; 
        this.estimatedProjectValue = input['estimatedProjectValue'];
        this.progress = input['progress'] ? input['progress'] : 0;
        this.assignmentCode = input['assignmentCode'];
        this.sowTypes = input['sowTypes'] ?input['sowTypes'].map(type => new SowType(type)) : [];
        this.cordinator = input['cordinator'] ? new Coordinator(input['cordinator']) : null;
        this.totalInvoice = input['totalInvoice'] ? input['totalInvoice'] : 0;
        this.totalPoValue = input['totalPoValue'] ? input['totalPoValue'] : 0;
    }
  }

  public get getId() {
    return this.id;
  }

  public set setId(id: number) {
    this.id = id;
  }

  public get getProjectCode() {
    return this.projectCode;
  }

  public set setProjectCode(code: string) {
    this.projectCode = code;
  }

  public get getcontractNo() {
    return this.contractNumberData;
  }

  public set setcontractNo(id: string) {
    this.contractNumberData = id;
  }

  public get getSite() {
    return this.site;
  }

  public set setSite(site: Site) {
    this.site = site;
  }

  public get getStatus() {
    return this.status;
  }

  public set setStatus(status: projectStatus) {
    this.status = status;
  }

  public get getStartDate() {
    return this.startDate;
  }

  public set setStartDate(date: string) {
    this.startDate = this.setDate(date);;
  }

  public get getEndDate() {
    return this.endDate;
  }

  public set setEndDate(date: string) {
    this.endDate = this.setDate(date);
  }

  public get getPoValue() {
    return this.poValue;
  }

  public get getSowTypes() {
    return this.sowTypes ? this.sowTypes : [];
  }

  public get getAssignmentCode() {
    return this.assignmentCode;
  }

  public set setAssignmentCode(code: string) {
    this.assignmentCode = code;
  }

  public set setSowTypes(sowType: SowType[]) {
    this.sowTypes = sowType;
  }

  public set setPoValue(value: number) {
    this.poValue = value;
  }

  public get getProjectManager() {
    return this.projectManager;
  }

  public set setProjectManager(manager: ProjectManager) {
    this.projectManager = manager;
  }

  public get getTeam() {
    return this.team;
  }

  public set setTeam(team: Team) {
    this.team = team;
  }

  public get getAdmin() {
    return this.admin;
  }

  public set setAdmin(admin: Admin) {
    this.admin = admin;
  }

  public set setAdminCenter(admin: AdminCenter) {
    this.adminCenter = admin;
  }

  public get getProgress() {
    return this.progress;
  }

  public set setProgress(progress: number) {
    this.progress = progress;
  }

  public get getTotalOpex() {
    return this.totalOpex;
  }

  // public get getSeverity() {
  //   return this.severity;
  // }

  // public set setSeverity(severity: Severity) {
  //   this.severity = severity;
  // }

  public get getDocuments() {
    return this.documents;
  }

  public set setDocument(documents: Document[]) {
    this.documents = documents;
  }

  public set setTotalOpex(opex: number) {
    this.totalOpex = opex;
  }

  public get getEstimateValue() {
    return this.estimatedProjectValue;
  }

  public get getAdminCenter() {
    return this.adminCenter;
  }

  public get getReference() {
    return this.references;
  }

  public set setReferences(references: Reference[]) {
    this.references = references;
  }

  public get getCoordinator() {
    return this.cordinator;
  }

  public set setCoordinator(coordinator: Coordinator) {
    this.cordinator = coordinator;
  }

  public get getTotalInvoice() {
    return this.totalInvoice;
  }

  public get getTotalPoValue() {
    return this.totalPoValue;
  }

  private setDate(date: string | Date): string {
    return datePipe.transform(date, 'yyyy-MM-ddTHH:mm:ssZ', '+0000');
  }

  public coordinatorObject(picId: number) {
    const coordinator = new Coordinator();
    coordinator.setId = picId;
    return coordinator;
  }

  public managerObject(pmId: number) {
    const manager = new ProjectManager();
    manager.setId = pmId;
    return manager;
  }

  public setTaskToSow(tasks: Task[]): SowType[] {
    return tasks.filter((task, i, arr) => {
      return arr.findIndex(arrType => arrType.getSow.getId === task.getSow.getId) !== -1;
    }).map(task => {
      return task.getSow.getSowType;
    });
  }
}
