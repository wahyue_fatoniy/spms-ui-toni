import { Region } from './region.model';
import {FormlyFieldConfig} from '@ngx-formly/core';

export class SowType {
  private id: number;
  private sowCode: string;
  private description: string;
  private price: number;
  private engineerDay: number;
  private technicianDay: number;
  private documentationDay: number;
  private atpDay: number;
  private region: Region;

  constructor(input?: Object) {
    if (input) {
      this.id = input['id'];
      this.sowCode = input['sowCode'];
      this.description = input['description'];
      this.price = input['price'];
      this.engineerDay = input['engineerDay'];
      this.technicianDay = input['technicianDay'];
      this.documentationDay = input['documentationDay'];
      this.region = new Region(input['region']);
      this.atpDay = input['atpDay'];
    }
  }

  public get getId() {
    return this.id;
  }

  public get getSowCode() {
    return this.sowCode;
  }

  public get getDescription() {
    return this.description;
  }

  public get getPrice() {
    return this.price;
  }

  public get getEngineerday() {
    return this.engineerDay;
  }

  public get getTechnicianday() {
    return this.technicianDay;
  }

  public get getDocumentationday() {
    return this.documentationDay;
  }

  public get getRegion() {
    return this.region;
  }

  public get getAtpDay() {
    return this.atpDay;
  }

  public set setId(id: number) {
    this.id = id;
  }

  public set setSowCode(sowCode: string) {
    this.sowCode = sowCode;
  }

  public set setDescription(description: string) {
    this.description = description;
  }


  public set setPrice(price: number) {
    this.price = price;
  }

  public set setEngineerDay(engineerDay: number) {
    this.engineerDay = engineerDay;
  }

  public set setTechnicianDay(technicianDay: number) {
    this.technicianDay = technicianDay;
  }

  public set setDocumentationDay(documentationDay: number) {
    this.documentationDay = documentationDay;
  }

  public set setRegion(region: Region) {
    this.region = region;
  }

  public set setAtpDay(atpDay: number) {
    this.atpDay = atpDay;
  }

  public formFields(): FormlyFieldConfig[] {
    return [
      {
        id: '1',
        fieldGroupClassName: 'row',
        fieldGroup: [
          {
            className: 'col-6',
            key: 'sowCode',
            type: 'input',
            templateOptions: {
              label: 'SOW Code',
              required: true,
              type: 'text'
            },
            validation: {
              messages: {
                required: 'SOW Code is required'
              }
            }
          },
          {
            className: 'col-6',
            key: 'description',
            type: 'input',
            templateOptions: {
              label: 'Description',
              required: true,
              type: 'text',
            },
            validation: {
              messages: {
                required: 'Description is required'
              }
            }
          },
        ]
      },
      {
        id: '2',
        fieldGroupClassName: 'row',
        fieldGroup: [
          {
            className: 'col-6',
            key: 'price',
            type: 'input',
            templateOptions: {
              label: 'Price',
              required: true,
              type: 'currency'
            },
            validation: {
              messages: {
                required: 'Price is required'
              }
            }
          },
          {
            className: 'col-6',
            key: 'engineerDay',
            type: 'input',
            templateOptions: {
              label: 'Engineer Day',
              required: true,
              type: 'number'
            },
            validation: {
              messages: {
                required: 'Engineer Day is required'
              }
            }
          },
        ]
      },
      {
        id: '3',
        fieldGroupClassName: 'row',
        fieldGroup: [
          {
            className: 'col-6',
            key: 'technicianDay',
            type: 'input',
            templateOptions: {
              label: 'Technician Day',
              required: true,
              type: 'number'
            },
            validation: {
              messages: {
                required: 'Technician Day is required'
              }
            }
          },
          {
            className: 'col-6',
            key: 'documentationDay',
            type: 'input',
            templateOptions: {
              label: 'Documentation Day',
              required: true,
              type: 'number'
            },
            validation: {
              messages: {
                required: 'Documentation Day is required'
              }
            }
          },
        ]
      },
      {
        id: '4',
        fieldGroupClassName: 'row',
        fieldGroup: [
          {
            className: 'col-6',
            key: 'atpDay',
            type: 'input',
            templateOptions: {
              label: 'ATP Day',
              required: true,
              type: 'number'
            },
            validation: {
              messages: {
                required: 'ATP Day is required'
              }
            }
          },
          {
            className: 'col-6',
            key: 'region',
            type: 'select',
            templateOptions: {
              label: 'Region',
              required: true,
              options: [],
            },
            validation: {
              messages: {
                required: 'Region is required'
              }
            }
          }
        ]
      },
    ];
  }
}
