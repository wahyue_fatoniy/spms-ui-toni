import { SoW } from "./sow.model";
import { SiteWork } from "./siteWork.model";
import { Activities } from "./activities.model";

export enum SeverityType {
  CRITICAL_I = "CRITICAL_I",
  CRITICAL_E = "CRITICAL_E",
  MAJOR_I = "MAJOR_I",
  MAJOR_E = "MAJOR_E",
  NORMAL = "NORMAL"
}

export enum TaskStatus {
  Progress = 'ON_PROGRESS',
  Done = 'DONE',
  NotStarted = 'NOT_STARTED',
  Cancel = 'CANCEL'
}

export class Task {
  private id: number;
  private status: TaskStatus;
  private startDate: string;
  private endDate: string;
  // private severity: string;
  private sow: SoW;
  private siteWork: SiteWork;
  private activity: Activities;
  private note: string;
  private requirements: Task[];
  private manDay: number;

  constructor(input?: Object) {
    if (input) {
      this.id = input["id"];
      this.status = input["status"];
      this.startDate = input["startDate"];
      this.endDate = input["endDate"];
      // this.severity = SeverityType[input["severity"]];
      this.sow = new SoW(input["sow"]);
      this.siteWork = input["siteWork"];
      this.activity = new Activities(input["activity"]);
      this.note = input["note"];
      this.requirements = input["requirements"]
        ? input["requirements"].map(task => new Task(task)) : [];
      this.manDay = input["manDay"]
        ? parseFloat(parseInt(input["manDay"]).toFixed(2))
        : 0;
    }
  }

  public get getId() {
    return this.id;
  }

  public set setId(id: number) {
    this.id = id;
  }

  public get getStatus() {
    return this.status;
  }

  public set setStatus(status: TaskStatus) {
    this.status = status;
  }

  public get getStartDate() {
    return this.startDate;
  }

  public set setStartDate(startDate: string) {
    this.startDate = startDate;
  }

  public get getEndDate() {
    return this.endDate;
  }

  public set setEndDate(endDate: string) {
    this.endDate = endDate;
  }

  // public get getSeverity() {
  //   return this.severity;
  // }

  // public set setSeverity(severity: string) {
  //   this.severity = severity;
  // }

  public get getSow() {
    return this.sow;
  }

  public set setSow(sow: SoW) {
    this.sow = sow;
  }

  public get getSiteWork() {
    return this.siteWork;
  }

  public set setSiteWork(siteWork: SiteWork) {
    this.siteWork = siteWork;
  }

  public get getActivity() {
    return this.activity;
  }

  public set setActivity(activity: Activities) {
    this.activity = activity;
  }

  public get getNote() {
    return this.note;
  }

  public set setNote(note: string) {
    this.note = note;
  }

  public get getRequirements() {
    return this.requirements;
  }

  public set setRequirements(tasks: Task[]) {
    this.requirements = tasks;
  }

  public get getManday() {
    return this.manDay;
  }

  public set setManday(manday: number) {
    this.manDay = manday;
  }

  // public get getTaskType() {
  //   return this.taskType;
  // }

  public isNotStarted(): boolean {
    let retVal = false;
    if (this.status === "NOT_STARTED") {
      retVal = true;
    }
    return retVal;
  }

  public isOnProgress(): boolean {
    let retVal = false;
    if (this.status === "ON_PROGRESS") {
      retVal = true;
    }
    return retVal;
  }

  public isFinished(): boolean {
    let retVal = false;
    if (this.status === "DONE") {
      retVal = true;
    }
    return retVal;
  }

  public isInitial(): boolean {
    let retVal = false;
    if (this.requirements.length === 0 && !this.isFinished()) {
      retVal = true;
    }
    return retVal;
  }

  public isReady(): boolean {
    let retVal = false;
    let i = 0;
    if (!this.isInitial() && !this.isFinished()) {
      this.requirements.forEach(requirement => {
        if (requirement.getStatus === "DONE") {
          i = i + 1;
        }
      });
      if (i === this.requirements.length) {
        retVal = true;
      }
    }
    return retVal;
  }

  public isRequirement(taskRequirement: Task) {
    let retVal = false;
    if (
      this.requirements.findIndex(
        task => task.getId === taskRequirement.getId
      ) !== -1
    ) {
      retVal = true;
    }
    return retVal;
  }

  public isPredecessor(nextTask: Task): boolean {
    let retVal = false;
    return retVal;
  }

  public isPreceededBy(prevTask: Task): boolean {
    let retVal = false;
    return retVal;
  }
}

export class TaskPlan extends Task {
  private planStatus: TaskStatus;
  private  _resources: number[];
  private  _manhour: number;

  constructor(task?: Task) {
    super(task);
    this.planStatus = this.getStatus;
    this._resources = [];
    this._manhour = 0;
  }

  public get getPlanStatus() {
    return this.planStatus;
  }

  public set setPlanStatus(status: TaskStatus) {
    this.planStatus = status;
  }

  public get resources() {
    return this._resources;
  }

  public set resources(resources: number[]) {
    this._resources = resources;
  }

  public get manhour() {
    return this._manhour;
  }

  public set manhour(manhour: number) {
    this._manhour = manhour;
  }
}
