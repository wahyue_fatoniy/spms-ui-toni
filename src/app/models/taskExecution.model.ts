import { Task } from "./task.model";
import { Resource } from "./resource.model";
import { Engineer } from "./engineer.model";
import { Technician } from "./technician.model";
import { Documentation } from "./documentation.model";
import { DatePipe } from "@angular/common";

const datePipe: DatePipe = new DatePipe('id');

export class TaskExecution {
  private id: number;
  private manHour: number;
  private finish: boolean;
  private note: string;
  private date: string;
  private task: Task;
  private resource: Resource;

  constructor(input?: Object) {
    if (input) {
      this.id = input["id"];
      this.manHour = input["manHour"];
      this.finish = input["finish"];
      this.note = input["note"];
      this.date = input["date"];
      this.task = new Task(input["task"]);
      this.setResource = input["resource"];
    }
  }

  public get getId(): number {
    return this.id;
  }

  public set setId(id: number) {
    this.id = id;
  }

  public get getManHour() {
    return this.manHour;
  }

  public set setManHour(manHour: number) {
    this.manHour = manHour;
  }

  public get getFinish() {
    return this.finish;
  }

  public set setFinish(finish: boolean) {
    this.finish = finish;
  }

  public get getNote() {
    return this.note;
  }

  public set setNote(note: string) {
    this.note = note;
  }

  public get getDate() {
    return this.date;
  }

  public set setDate(date: Date) {
    this.date = this.formatDate(date);
  }

  public get getTask() {
    return this.task;
  }

  public set setTask(task: Task) {
    this.task = task;
  }

  public get getResource() {
    return this.resource;
  }

  private formatDate(date: string | Date): string {
    return datePipe.transform(date, 'yyyy-MM-ddTHH:mm:ssZ', '+0000');
  }


  public set setResource(resource: Resource) {
    if (resource) {
      switch (resource["roleType"]) {
        case "ENGINEER":
          {
            this.resource = new Engineer(resource);
          }
          break;
        case "TECHNICIAN":
          {
            this.resource = new Technician(resource);
          }
          break;
        case "DOCUMENTATION": {
          this.resource = new Documentation(resource);
        }
      }
    }
  }
}
