import { Resource } from "./resource.model";
import { Engineer } from "./engineer.model";
import { Technician } from "./technician.model";
import { Documentation } from "./documentation.model";
import { Coordinator } from "./coordinator.model";

export class Team {
  private id: number;
  private cordinator: Coordinator;
  private resources: Resource[];

  constructor(input?: Object) {
    if (input) {
      this.id = input["id"];
      this.cordinator = new Coordinator(input["cordinator"]);
      this.resources = input["resources"]
        ? input["resources"].map(resource => {
            switch (resource["roleType"]) {
              case "ENGINEER": {
                return new Engineer(resource);
              }
              case "TECHNICIAN": {
                return new Technician(resource);
              }
              case "DOCUMENTATION": {
                return new Documentation(resource);
              }
            }
          })
        : [];
    }
  }

  public get getId() {
    return this.id;
  }

  public get getResources() {
    return this.resources === undefined || this.resources === null
      ? []
      : this.resources;
  }

  public get getCoordinator() {
    return this.cordinator;
  }

  public set setCoordinator(coordinator: Coordinator) {
    this.cordinator = coordinator;
  }
}
