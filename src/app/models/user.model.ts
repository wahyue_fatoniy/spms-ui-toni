import { Role } from "./role.model";

export class User extends Role {
  private username: string;
  private password: string;
  private email: string;
  protected roleType = 'USER';

  constructor(input?: User) {
    super();
    if (input) {
      this.username = input['username'];
      this.password = input['password'];
      this.email = input['email'];
    }
  }

  public get getUsername() {
    return this.username;
  }

  public set setUsername(username: string) {
      this.username = username;
  }

  public get getpassword() {
    return this.password;
  }

  public set setPassword(password: string) {
      this.password = password;
  }

  public get getEmail() {
    return this.email;
  }

  public set setEmail(email: string) {
      this.email = email;
  }
}
