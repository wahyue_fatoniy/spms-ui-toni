import { Task } from './task.model';
import { SiteWork } from './siteWork.model';
import { DatePipe } from '@angular/common';

const datePipe: DatePipe = new DatePipe('id');

export class WeeklyPlanning {
  private id: number;
  private siteWork: SiteWork;
  private tasks: Task[];
  private startDate: string | Date ;
  private endDate: string | Date;
  private submit: boolean;

  constructor(input?: Object) {
    if (input) {
      this.id = input['id'];
      this.startDate = this.setDate(input['startDate']);
      this.endDate = this.setDate(input['endDate']);
      this.tasks =
        input['tasks'] instanceof Array
          ? input['tasks'].map(task => new Task(task))
          : [];
      this.siteWork = new SiteWork(input['siteWork']);
      this.submit = input['submit'] ? input['submit'] : false;
    }
  }

  public get getId() {
    return this.id;
  }

  public get getStartDate() {
    return this.startDate;
  }

  public set setStartDate(date: Date | string) {
    this.startDate = date;
  }

  public get getEndDate() {
    return this.endDate;
  }

  public set setEndDate(date: Date | string) {
    this.endDate = date;
  }

  public get getTasks() {
    return this.tasks;
  }

  public set setTasks(tasks: Task[]) {
    this.tasks = tasks;
  }

  public get getSiteWork() {
    return this.siteWork;
  }

  public set setSiteWork(siteWork: SiteWork) {
    this.siteWork = siteWork;
  }

  public get getSubmit() {
    return this.submit;
  }

  private setDate(date: string | Date): string {
    return datePipe.transform(date, 'yyyy-MM-ddTHH:mm:ssZ', '+0000');
  }
}
