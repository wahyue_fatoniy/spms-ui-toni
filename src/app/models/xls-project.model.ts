import { SiteWork } from "./siteWork.model";
import { projectStatus, Severity } from "src/service/enum";
import { DatePipe } from "@angular/common";
import { LocalCurrencyPipe } from "../pipes/localCurrency/local-currency.pipe";

const datePipe: DatePipe = new DatePipe('id');
const currency: LocalCurrencyPipe = new LocalCurrencyPipe();

export class XlsProject {
  private project_id: string;
  private start_sate: string;
  private end_date: string;
  private site_name: string;
  private po_value: string;
  private status: projectStatus;
  private estimated_value: string;
  private severity: Severity;
  private total_opex: string;
  private admin: string;
  private project_manager: string;
  private admin_center: string;

  constructor(input: SiteWork) {
    if (input) {
      this.project_id = input.getProjectCode;
      this.start_sate = this.setDate(input.getStartDate);
      this.end_date = this.setDate(input.getEndDate);
      this.site_name = input.getSite.getName;
      this.po_value = currency.transform(input.getPoValue);
      this.status = input.getStatus;
      this.estimated_value = currency.transform(input.getEstimateValue);
      // this.severity = input.getSeverity;
      this.total_opex = currency.transform(input.getTotalOpex);
      this.admin = input.getAdmin ? input.getAdmin.getParty ? input.getAdmin.getParty['name'] : '' : '';
      this.project_manager = input.getProjectManager ? input.getProjectManager.getParty ? input.getProjectManager.getParty['name'] : '' : '';
      this.admin_center = input.getAdminCenter ? input.getAdminCenter.getParty ? input.getAdminCenter.getParty['name'] : '' : '';
    }
  }

  private setDate(date: string | Date): string {
    return datePipe.transform(date, 'mediumDate');
  }
}
