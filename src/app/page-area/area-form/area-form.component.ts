import { Component, OnInit, Inject } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material';
import { Area } from '../../../models/area-service/area.model';
import { RegionService } from '../../../models/region-service/region.service';
import { Region } from '../../../models/region-service/region.model';
import { Subscription } from 'rxjs';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { initialLetterValidate } from '../../customValidator';

@Component({
  selector: 'app-area-form',
  templateUrl: './area-form.component.html',
  styleUrls: ['./area-form.component.css']
})
export class AreaFormComponent implements OnInit {
  regionData: Array<Region>;
  subscriptions: Subscription;
  form: FormGroup;

  constructor(
    public dialogRef: MatDialogRef<AreaFormComponent>,
    @Inject(MAT_DIALOG_DATA) public area: Area,
    public regionService: RegionService,
    public formBuilder: FormBuilder
  ) {}

  ngOnInit() {
    this.getServiceData();
    this.setFormValidation();
    this.onFormValid();
    this.onBeforeClose();
  }

  getServiceData(): void {
    this.regionService.getAll.then((regions: Array<Region>) => {
      this.regionData = regions;
    });
  }

  setFormValidation(): void {
    this.form = this.formBuilder.group({
      id: [this.area['id']],
      name: [
        this.area['name'],
        Validators.compose([Validators.required, initialLetterValidate])
      ],
      region: [this.area['region'], Validators.required],
      latitude: [this.area.getLatitude, Validators.required],
      longitude: [this.area.getLongitude, Validators.required],
    });
  }

  onFormValid(): void {
    this.subscriptions = this.form.statusChanges.subscribe((status: string) => {
      if (status === 'VALID') {
        this.area = new Area(this.form.value);
      }
    });
  }

  onBeforeClose(): void {
    this.subscriptions.add(
      this.dialogRef.beforeClose().subscribe(() => {
        this.subscriptions.unsubscribe();
      })
    );
  }

  compareSelectedItems(c1: any, c2: any): boolean {
    return c1 && c2 ? c1.id === c2.id : c1 === c2;
  }
}
