import { Component, OnInit, OnDestroy, ViewChild } from '@angular/core';
import {
  MatTableDataSource,
  MatDialog,
  MatPaginator,
  MatSort
} from '@angular/material';
import { FormDialogRemoveComponent } from '../../template/form-dialog-remove/form-dialog-remove.component';
import { Subscription } from 'rxjs';
import { AreaService } from '../../../models/area-service/area.service';
import { Area } from '../../../models/area-service/area.model';
import { AreaTableEvent, Filter } from '../../../models/interface/interface';
import { AreaFormComponent } from '../area-form/area-form.component';
import { FilterDataPipe } from '../../pipes/filterData/filter-data.pipe';

@Component({
  selector: 'app-area-table',
  templateUrl: './area-table.component.html',
  styleUrls: ['./area-table.component.css']
})
export class AreaTableComponent implements OnInit, OnDestroy {
  displayedColumns: string[] = [
    'no',
    'name',
    'region',
    'latitude',
    'longitude',
    'action'
  ];
  pageSize: number;
  pageIndex = 0;
  subscribe = false;
  loading = true;
  datasource: MatTableDataSource<Object> = new MatTableDataSource();
  tableTitle = 'Area Table';
  subData: Subscription;
  @ViewChild(MatPaginator)
  paginator: MatPaginator;
  @ViewChild(MatSort)
  matSort: MatSort;
  filterDataPipe = new FilterDataPipe();
  typeFilter = 'all';
  typeFilters: Filter[] = [
    { value: 'all', label: 'All Column' },
    { value: 'name', label: 'Name' },
    { value: { value: 'region', key: 'name' }, label: 'Region' },
    { value: 'latitude', label: 'Latitude' },
    { value: 'longitude', label: 'Longitude' }
  ];

  constructor(public service: AreaService, public dialog: MatDialog) {}

  ngOnInit() {
    /**get previous pageSize from localStorage */
    const PAGE = localStorage.getItem('pageSize');
    /**assignment variable pageSize with value PAGE */
    this.pageSize = PAGE ? parseInt(PAGE, 0) : 10;

    this.service.getAll
      .then(() => {
        this.subscribe = true;
        this.setTable();
      })
      .catch(err => console.error(err));
  }

  ngOnDestroy() {
    if (this.subscribe) {
      this.subData.unsubscribe();
    }
  }

  onSetPage(event): void {
    /**assignment variable pageSize with value event paginator */
    this.pageSize = event.pageSize;
    /**assignment variable pageIndex with value event paginator */
    this.pageIndex = event.pageIndex;
    /**save current pageSize in localStorage */
    localStorage.setItem('pageSize', event.pageSize);
  }

  setTable(): void {
    this.subData = this.service.getArea.subscribe((area: Array<Area>) => {
      /**set loading to false */
      this.loading = false;
      /**assignment datasource data with siteWork data */
      this.datasource.data = area;
      /**sorting datasource data*/
      this.datasource.sort = this.matSort;
      /**assignment datasource paginator */
      this.datasource.paginator = this.paginator;
      /**set datasource filter predicate to filter data datasource data */
      this.datasource.filterPredicate = (value: Area, keyword: string) => {
        return this.filterDataPipe.transform(value, keyword, this.typeFilter);
      };
    });
  }

  onClickAction(value: AreaTableEvent): void {
    if (value['action'] === 'delete') {
      const remove = this.dialog.open(FormDialogRemoveComponent, {
        data: {
          data: value['value']['id'],
          message: `Are you sure to delete ${value['value']['name']} ?`
        }
      });
      const subDialogRemove: Subscription = remove
        .afterClosed()
        .subscribe(result => {
          if (result !== 'cancel') {
            this.service.delete(result).catch(err => console.error(err));
          }
          subDialogRemove.unsubscribe();
        });
    } else {
      const dialog = this.dialog.open(AreaFormComponent, {
        width: '850px',
        disableClose: true,
        data: value['value']
      });
      const subDialog: Subscription = dialog.afterClosed().subscribe(result => {
        if (result !== 'cancel') {
          if (value['action'] === 'create') {
            this.service.create(result).catch(err => console.error(err));
          } else {
            this.service.update(result).catch(err => console.error(err));
          }
        }
        subDialog.unsubscribe();
      });
    }
  }
}
