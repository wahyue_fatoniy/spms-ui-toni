import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-page-area',
  template: `<app-area-table></app-area-table>`,
  styles: []
})
export class PageAreaComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
