import { Component, OnInit, Inject, OnDestroy } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { Organization } from '../../../models/organization-service/organization.model';
import {
  FormBuilder,
  FormGroup,
  Validators,
  FormControl
} from '@angular/forms';
import { initialLetterValidate } from '../../customValidator';
import { Subscription } from 'rxjs';
import { InSelectRole } from '../../page-person/person-form/person-form.component';
import { Customer } from '../../../models/customer-service/customer.model';
import { Partner } from '../../../models/partner-service/partner.model';
import { Subcont } from '../../../models/subcont-service/subcont.model';

@Component({
  selector: 'app-form-organization',
  templateUrl: './form-organization.component.html',
  styleUrls: ['./form-organization.component.css']
})
export class FormOrganizationComponent implements OnInit, OnDestroy {
  form: FormGroup;
  subscription: Subscription;
  public roles: Array<InSelectRole> = [
    { role: 'PARTNER', title: 'PARTNER',  img: 'partner.png' },
    { role: 'CUSTOMER', title: 'CUSTOMER', img: 'customer.png' },
    { role: 'CORDINATOR', title: 'SUBCONT', img: 'sales.png' }
  ];
  rolesControl: FormControl;

  constructor(
    public dialogRef: MatDialogRef<FormOrganizationComponent>,
    @Inject(MAT_DIALOG_DATA) public organization: Organization,
    public formBuilder: FormBuilder
  ) {
    this.organization.getRoles = this.organization.getRoles.filter(role => role['endDate'] === null);
  }

  ngOnInit() {
    this.setFormValidation();
    this.onFormValid();
    this.onSelectRoles();
  }

  ngOnDestroy() {
    if (this.subscription) {
      if (this.subscription.closed === false) {
        this.setOrganizationRole();
        this.subscription.unsubscribe();
      }
    }
  }

  setFormValidation(): void {
    this.form = this.formBuilder.group({
      id: [this.organization.getId],
      name: [
        this.organization.getName,
        Validators.compose([Validators.required, initialLetterValidate])
      ],
      address: [this.organization.getAddress, Validators.required],
      email: [
        this.organization.getEmail,
        Validators.compose([Validators.required, Validators.email])
      ],
      phone: [this.organization.getPhone, Validators.required]
    });
    this.rolesControl = new FormControl(
      this.organization.getRoles.map(role => role['roleType'])
    );
  }

  onFormValid(): void {
    this.subscription = this.form.statusChanges.subscribe(status => {
      if (status === 'VALID') {
        this.form.value['roles'] = this.organization.getRoles;
        this.organization = new Organization(this.form.value);
      }
    });
  }


  public onSelectRoles(): void {
    this.subscription.add(
      this.rolesControl.valueChanges.subscribe((roles: string[]) => {
        const continueData = this.organization.getRoles.filter(data =>
          roles.find(role => role === data['roleType'])
        );
        this.organization.getRoles = [...continueData];
        for (let role of roles) {
          if (!continueData.find(data => data['roleType'] === role)) {
            this.organization.getRoles = [
              {
                roleType: role
              },
              ...continueData
            ];
          }
        }
      })
    );
  }

  public setOrganizationRole(): void {
    let roles = [];
    for (let role of this.organization.getRoles) {
      switch (role['roleType']) {
        case 'CUSTOMER': {
          roles = [
            ...roles,
            new Customer({ ...role, party: this.organization })
          ];
          break;
        }
        case 'PARTNER': {
          roles = [
            ...roles,
            new Partner({ ...role, party: this.organization })
          ];
          break;
        }
        case 'CORDINATOR': {
          roles = [
            ...roles,
            new Subcont({ ...role, party: this.organization })
          ];
        }
      }
    }
    this.organization.getRoles = roles;
  }

  public compareSelectedItem(val1: any, val2: any): boolean {
    return val1.id && val2.id ? val1.id === val2.id : val1 === val2;
  }
}
