import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FormUserOrganizationComponent } from './form-user-organization.component';

describe('FormUserOrganizationComponent', () => {
  let component: FormUserOrganizationComponent;
  let fixture: ComponentFixture<FormUserOrganizationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FormUserOrganizationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FormUserOrganizationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
