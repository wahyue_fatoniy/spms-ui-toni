import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-page-organization',
  template: `
    <app-table-organization></app-table-organization>
  `,
  styles: []
})
export class PageOrganizationComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
