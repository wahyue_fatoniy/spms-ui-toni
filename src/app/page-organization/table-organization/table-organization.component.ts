import { Component, OnInit, OnDestroy, ViewChild } from "@angular/core";
import {
  MatTableDataSource,
  MatDialog,
  MatPaginator,
  MatSort
} from "@angular/material";
import { FormDialogRemoveComponent } from "../../template/form-dialog-remove/form-dialog-remove.component";
import { FormOrganizationComponent } from "../form-organization/form-organization.component";
import { Organization } from "../../../models/organization-service/organization.model";
import { OrganizationService } from "../../../models/organization-service/organization.service";
import { Subscription, Subject, combineLatest } from "rxjs";
import {
  OrganizationTableEvent,
  Filter
} from "../../../models/interface/interface";
import { RoleService } from "../../../models/role-service/role.service";
import { DatePipe } from "@angular/common";
import { FilterDataPipe } from "../../pipes/filterData/filter-data.pipe";
import { FormUserOrganizationComponent } from "../form-user-organization/form-user-organization.component";
import { RoleAuthService } from "src/app/shared/aaa/role/role.service";
import { UserAuthService } from "src/app/shared/aaa/user/user.service";
import { UserService } from "src/app/shared/models/user/user.service";
import { takeUntil } from "rxjs/operators";
import { Party } from "src/app/shared/models/party/party";
import { UserAuth } from "src/app/shared/aaa/user/user";
import { User } from "src/app/shared/models/user/user";

@Component({
  selector: "app-table-organization",
  templateUrl: "./table-organization.component.html",
  styleUrls: ["./table-organization.component.css"]
})
export class TableOrganizationComponent implements OnInit, OnDestroy {
  displayedColumns: string[] = [
    "no",
    "name",
    "address",
    "email",
    "phone",
    "action"
  ];
  pageSize: number;
  pageIndex = 0;
  datasource: MatTableDataSource<Object> = new MatTableDataSource();
  tableTitle = "Organization";
  subData: Subscription;
  datePipe: DatePipe = new DatePipe("id");
  @ViewChild(MatPaginator)
  paginator: MatPaginator;
  @ViewChild(MatSort)
  matSort: MatSort;
  filterDataPipe = new FilterDataPipe();
  typeFilter = "all";
  typeFilters: Filter[] = [
    { value: "all", label: "All Column" },
    { value: "name", label: "Name" },
    { value: "address", label: "address" },
    { value: "email", label: "Email" },
    { value: "phone", label: "Phone" }
  ];
  destroy$: Subject<boolean> = new Subject();

  constructor(
    public service: OrganizationService,
    public dialog: MatDialog,
    public roleService: RoleService,
    private roleAuth: RoleAuthService,
    private userAuth: UserAuthService,
    private user: UserService
  ) {}

  ngOnInit() {
    this.service.getAll
      .then(() => {
        const PAGE = localStorage.getItem("pageSize");
        /**assignment variable pageSize with value PAGE */
        this.pageSize = PAGE ? parseInt(PAGE, 0) : 10;
        this.setTable();
      })
      .catch(err => console.error(err));
    this.userAuth
      .getToken()
      .pipe(takeUntil(this.destroy$))
      .subscribe(_token => {
        this.roleAuth
          .findRoles(_token.getAccessToken())
          .pipe(takeUntil(this.destroy$))
          .subscribe();
      });
  }

  ngOnDestroy() {
    if (this.subData) {
      if (this.subData.closed === false) {
        this.subData.unsubscribe();
      }
    }
  }

  public onSetPage(event): void {
    this.pageSize = event.pageSize;
    this.pageIndex = event.pageIndex;
    localStorage.setItem("pageSize", event.pageSize);
  }

  getRoleUser(row: Organization) {
    if (row.getRoles.findIndex(role => role['roleType'] === 'CORDINATOR') !== -1) {
      return true;
    }
    return false;
  }

  setTable(): void {
    this.subData = this.service.getOrganization.subscribe(
      (organization: Organization[]) => {
        console.log(organization);
        this.datasource.data = organization;
        /**sorting datasource data*/
        this.datasource.sort = this.matSort;
        /**assignment datasource paginator */
        this.datasource.paginator = this.paginator;
        /**set datasource filter predicate to filter data datasource data */
        this.datasource.filterPredicate = (
          value: Organization,
          keyword: string
        ) => {
          return this.filterDataPipe.transform(value, keyword, this.typeFilter);
        };
      }
    );
  }

  public onDelete(value: Organization): void {
    const remove = this.dialog.open(FormDialogRemoveComponent, {
      data: {
        data: value,
        message: `Are you sure to delete ${value["name"]} ?`
      }
    });
    this.subData.add(
      remove.afterClosed().subscribe(result => {
        if (result !== "cancel") {
          this.service.delete(result).catch(err => console.error(err));
        }
      })
    );
  }

  onClickAction(value: OrganizationTableEvent): void {
    const dialog = this.dialog.open(FormOrganizationComponent, {
      width: "900px",
      disableClose: true,
      data: new Organization(value["value"])
    });
    this.subData.add(
      dialog.afterClosed().subscribe((result: Organization) => {
        if (result) {
          if (value["action"] === "create") {
            this.service
              .create(result)
              .then(val => {
                let roles = [];
                for (let role of result.getRoles) {
                  roles = [...roles, { ...role, party: val }];
                }
                this.roleService.create(roles).then(role => {
                  console.log(role);
                });
              })
              .catch(err => console.error(err));
          } else {
            const currentRoles = result.getRoles ? result.getRoles : [];
            const previousRoles = value.value["roles"];
            let inActiveRoles = [];
            let rolesUpdate = [];
            let newRoles = [];
            newRoles = currentRoles.filter(current => {
              return getIndexRoles(previousRoles, current) === -1;
            });
            rolesUpdate = currentRoles.filter(current => {
              return getIndexRoles(previousRoles, current) !== -1;
            });
            inActiveRoles = previousRoles
              .filter(previous => {
                return (
                  getIndexRoles(currentRoles, previous) === -1 &&
                  previous.endDate === null
                );
              })
              .map(role => {
                role["endDate"] = this.datePipe.transform(
                  new Date(),
                  "yyyy-MM-ddTHH:mm:ssZ",
                  "+0000"
                );
                return role;
              });

            let valUpdate = [];
            let valCreate = [];

            for (let role of [...rolesUpdate, ...inActiveRoles]) {
              valUpdate = [...valUpdate, { ...role, party: result }];
            }
            // this.roleService.update(valUpdate).catch(err => console.log(err));

            for (let role of newRoles) {
              valCreate = [...valCreate, { ...role, party: result }];
            }
            // this.roleService.create(valCreate).catch(err => console.log(err));
            console.log('org create', valCreate);
            // this.service.update(result).catch(err => console.log(err));
          }
        }
        function getIndexRoles(roles: any[], role): number {
          return roles.findIndex(previous => {
            if (previous["roleType"] === role["roleType"]) {
              return previous.id === role.id;
            }
          });
        }
      })
    );
  }

  public onCreateUser(organization: Organization): void {
    this.userAuth.findByEmail(organization.getEmail)
    .pipe(takeUntil(this.destroy$))
    .subscribe(user => {
      const party = new Party(JSON.parse(JSON.stringify(organization)));
      this.dialog.open(FormUserOrganizationComponent, {
        width: "500px",
        data: { value: user, role: this.roleAuth, party: party }
      })
      .afterClosed().subscribe((_set: any) => {
        console.log(_set);
        if (_set) {
          if (_set.user) {
            const userAuth: UserAuth = new UserAuth(_set.data);
            userAuth.setEmail(party.getEmail());
            userAuth.setName(party.getName());
            const value: User = new User(_set.user);
            const roles: string[] = this.roleAuth.getIdsByRoleName(
              userAuth.getRoles()
            );
            this.userAuth.createUser(userAuth).subscribe(_userAuth => {
              this.userAuth.getToken().subscribe(_token => {
                combineLatest(
                  this.userAuth.addRoles(
                    _userAuth,
                    [],
                    roles,
                    _token.getAccessToken()
                  ),
                  this.user.create(value)
                ).subscribe(_success => {    });
              });
            });
          } else {
            const userAuth: UserAuth = new UserAuth(_set.data);
            this.userAuth.findByEmail(userAuth.getEmail()).subscribe(_user => {
              this.userAuth.getToken().subscribe(_token => {
                this.userAuth
                  .addRoles(
                    userAuth,
                    this.roleAuth.getIdsByRoleName(_user.getRoles()),
                    this.roleAuth.getIdsByRoleName(userAuth.getRoles()),
                    _token.getAccessToken()
                  )
                  .subscribe(_success => {
                  });
              });
            });
          }
        }
      });
    });
  }
}
