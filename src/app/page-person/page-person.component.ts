import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-page-person',
  template: `
  <app-person-table></app-person-table>
  `,
  styles: []
})
export class PagePersonComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
