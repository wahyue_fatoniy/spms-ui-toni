import { Component, OnInit, Inject, OnDestroy } from "@angular/core";
import { MatDialogRef, MAT_DIALOG_DATA } from "@angular/material";
import {
  FormBuilder,
  FormGroup,
  Validators,
  FormControl
} from "@angular/forms";
import { initialLetterValidate } from "../../customValidator";
import { RegionService } from "../../../service/region-service/region.service";
import { Region } from "../../models/region.model";
import { Subject } from "rxjs";
import { takeUntil } from "rxjs/operators";
import { Person } from "src/app/models/person.model";
import { Admin } from "src/app/models/admin.model";
import { Finance } from "src/app/models/finance.model";
import { Sales } from "src/app/models/sales.model";
import { ProjectManager } from "src/app/models/projectManager.model";
import { Coordinator } from "src/app/models/coordinator.model";
import { Customer } from "src/app/models/customer.model";
import { Engineer } from "src/app/models/engineer.model";
import { Technician } from "src/app/models/technician.model";
import { Documentation } from "src/app/models/documentation.model";
import { Role } from "src/app/models/role.model";
import { AdminCenter } from "src/app/models/adminCenter.model";
import { GeneralManager } from "src/app/models/generalManager.model";

export interface InSelectRole {
  role: string;
  title: string;
  img: string;
}

interface RoleConfig {
  role: string;
  regionIds: number[];
}

@Component({
  selector: "app-person-form",
  templateUrl: "./person-form.component.html",
  styleUrls: ["./person-form.component.css"]
})
export class PersonFormComponent implements OnInit, OnDestroy {
  public startDate: Date = new Date(1990, 0, 1);
  public form: FormGroup;
  public roles: InSelectRole[] = [
    { role: "GENERAL_MANAGER", title: "General Manager", img: "admin.png" },
    { role: "ADMIN", title: "Admin", img: "customer.png" },
    { role: "FINANCE", title: "Finance", img: "finance2.png" },
    { role: "SALES", title: "Sales", img: "sales.png" },
    { role: "PROJECT_MANAGER", title: "Project Manager", img: "pm.png" },
    { role: "CORDINATOR", title: "Coordinator", img: "cordinator.png" },
    { role: "CUSTOMER", title: "Customer", img: "customer.png" },
    { role: "ENGINEER", title: "Engineer", img: "ENGINEER.png" },
    { role: "TECHNICIAN", title: "Technician", img: "TECHNICIAN.png" },
    { role: "DOCUMENTATION", title: "Documentation", img: "DOCUMENTATION.png" },
    {
      role: "ADMIN_CENTER",
      title: "Admin Center",
      img: "admin_center.png"
    },
    {
      role: "SUBCONT_ENGINEER",
      title: "Subcont Engineer",
      img: "ENGINEER.png"
    },
    {
      role: "SUBCONT_TECHNICIAN",
      title: "Subcont Technician",
      img: "TECHNICIAN.png"
    },
    {
      role: "SUBCONT_DOCUMENTATION",
      title: "Subcont  Documentation",
      img: "DOCUMENTATION.png"
    },
    {
      role: "USER",
      title: "User System",
      img: "user.png"
    }
  ];
  public rolesConfig: RoleConfig[] = [];
  public displayedColumns: string[] = ["role", "region", "action"];
  public nipControl: FormControl = new FormControl();
  public regionData: Region[];
  public destroy: Subject<boolean> = new Subject<boolean>();

  constructor(
    public dialogRef: MatDialogRef<PersonFormComponent>,
    @Inject(MAT_DIALOG_DATA) public data: Person,
    public formBuilder: FormBuilder,
    public regionService: RegionService
  ) {}

  ngOnInit() {
    this.getRegion();
    this.initFormValidation();
    this.initSubcontPerson();
    this.initPersonRole();
    this.setValidationNip();
    this.onFormValid();
  }

  ngOnDestroy() {
    // generate role in destroy
    let roles: { id: number; roleType: string; region: Region }[] = [];
    let region: Region;
    this.rolesConfig.map(_val => {
      _val.regionIds.forEach(_id => {
        region = this.regionData.find(_region => _region.getId === _id);
        roles = [...roles, { id: null, roleType: _val.role, region: region }];
      });

      if (_val.regionIds.length === 0) {
        roles = [...roles, { id: null, roleType: _val.role, region: null }];
      }
    });

    // on update role
    roles = roles.map(val => {
      region = new Region(val.region);
      this.data.getRoles.forEach(role => {
        if (
          role["roleType"] === val.roleType &&
          new Region(role["region"]).getId === region.getId
        ) {
          val.id = role["id"];
        }
      });
      return val;
    });
    this.onSetPersonRole(roles);
    this.destroy.next(true);
    this.destroy.complete();
  }

  public getRegion(): void {
    this.regionService
      .subscriptionData()
      .pipe(takeUntil(this.destroy))
      .subscribe(regions => {
        this.regionData = regions;
      });
  }

  private initSubcontPerson(): void {
    for (let role of this.data.getRoles) {
      if (role["roleType"] === "ENGINEER" && !role["nip"]) {
        role["roleType"] = "SUBCONT_ENGINEER";
      } else if (role["roleType"] === "CORDINATOR" && !role["nip"]) {
        role["roleType"] = "SUBCONT_ENGINEER";
      } else if (role["roleType"] === "TECHNICIAN" && !role["nip"]) {
        role["roleType"] = "SUBCONT_TECHNICIAN";
      } else if (role["roleType"] === "DOCUMENTATION" && !role["nip"]) {
        role["roleType"] = "SUBCONT_DOCUMENTATION";
      }
    }
  }

  initPersonRole(): void {
    let regionIds: number[];
    this.data.getRoles
      .filter((role, i, arr) => {
        // filter unique role
        return (
          i === arr.findIndex(_arr => _arr["roleType"] === role["roleType"]) &&
          role["endDate"] === null
        );
      })
      .forEach(val => {
        // get region ids with roleType
        regionIds = this.data.getRoles
          .filter(
            role =>
              val["roleType"] === role["roleType"] && role["endDate"] === null
          )
          .map(role => (role["region"] ? role["region"]["id"] : ""));

        this.rolesConfig = [
          ...this.rolesConfig,
          { role: val["roleType"], regionIds: regionIds }
        ];

        if (val["nip"]) {
          this.nipControl.setValue(val["nip"]);
        }
      });
  }

  public initFormValidation(): void {
    this.form = this.formBuilder.group({
      id: [this.data.getId],
      name: [this.data.getName, Validators.compose([initialLetterValidate])],
      address: [
        this.data.getAddress,
        Validators.compose([initialLetterValidate])
      ],
      email: [this.data.getEmail, Validators.compose([Validators.email])],
      phone: [this.data.getPhone],
      gender: [this.data.getGender],
      birthDate: [
        this.data.getBirthDate ? new Date(this.data.getBirthDate) : null
      ]
    });
  }

  public onFormValid(): void {
    this.form.statusChanges.pipe(takeUntil(this.destroy)).subscribe(status => {
      if (status === "VALID") {
        this.form.value["roles"] = this.data.getRoles;
        this.data = new Person(this.form.value);
      }
    });
  }

  public onSelectRole(role: string): void {
    this.setRoleConfig(role, []);
    this.setValidationNip();
    this.setValidationRegion();
  }

  public onSelectRegion(regionIds: number[], role: string): void {
    this.setRoleConfig(role, regionIds);
    this.setValidationRegion();
  }

  public onDeleteRole(role: string): void {
    this.rolesConfig = this.rolesConfig.filter(_val => {
      return _val.role !== role;
    });
    this.setValidationNip();
    this.setValidationRegion();
  }

  public setValidationNip() {
    const roleRequiredNip = [
      "ENGINEER",
      "TECHNICIAN",
      "DOCUMENTATION",
      "FINANCE",
      "PROJECT_MANAGER",
      "SALES",
      "CORDINATOR",
      "ADMIN",
      "ADMIN_CENTER",
      'GENERAL_MANAGER'
    ];
    if (
      this.rolesConfig.findIndex(val => {
        return roleRequiredNip.findIndex(role => role === val.role) !== -1;
      }) !== -1
    ) {
      this.nipControl.setValidators(Validators.required);
      this.nipControl.enable({onlySelf: true, emitEvent: false});
      this.nipControl.setValue(this.nipControl.value);
    } else {
      this.nipControl.clearValidators();
      this.nipControl.disable({onlySelf: true, emitEvent: false});
      this.nipControl.setValue(null);
    }
  }

  public setValidationRegion(): boolean {
    const roleRequiredRegion = [
      "ENGINEER",
      "TECHNICIAN",
      "DOCUMENTATION",
      "PROJECT_MANAGER",
      "CORDINATOR",
      "ADMIN"
    ];
    return (
      this.rolesConfig
        .filter(role => {
          return (
            roleRequiredRegion.findIndex(roleType => roleType === role.role) !==
            -1
          );
        })
        .findIndex(role => role.regionIds.length === 0) > -1
    );
  }

  public setRoleConfig(role: string, regionIds: number[]): void {
    if (this.rolesConfig.length === 0) {
      this.rolesConfig = [{ role: role, regionIds: regionIds }];
    } else {
      if (this.rolesConfig.findIndex(_val => _val.role === role) !== -1) {
        this.rolesConfig = this.rolesConfig.map(_config => {
          if (_config.role === role) {
            _config.regionIds = regionIds;
          }
          return _config;
        });
      } else {
        this.rolesConfig = [
          ...this.rolesConfig,
          { role: role, regionIds: regionIds }
        ];
      }
    }
  }

  private onSetPersonRole(val: { roleType: string; region: Region }[]): void {
    let role: any;
    let roles: Role[] = [];
    val.forEach(data => {
      role = {
        ...data,
        party: new Person(this.form.getRawValue()),
        nip:
          data["roleType"] === "SUBCONT_ENGINEER" ||
          data["roleType"] === "SUBCONT_TECHNICIAN" ||
          data["roleType"] === "SUBCONT_DOCUMENTATION"
            ? null
            : this.nipControl.value
      };
      switch (data["roleType"]) {
        case "ADMIN": {
          roles = [...roles, new Admin(role)];
          break;
        }
        case "FINANCE": {
          roles = [...roles, new Finance(role)];
          break;
        }
        case "SALES": {
          roles = [...roles, new Sales(role)];
          break;
        }
        case "PROJECT_MANAGER": {
          roles = [...roles, new ProjectManager(role)];
          break;
        }
        case "CORDINATOR": {
          roles = [...roles, new Coordinator(role)];
          break;
        }
        case "CUSTOMER": {
          roles = [...roles, new Customer(role)];
          break;
        }
        case "ENGINEER": {
          roles = [...roles, new Engineer(role)];
          break;
        }
        case "TECHNICIAN": {
          roles = [...roles, new Technician(role)];
          break;
        }
        case "DOCUMENTATION": {
          roles = [...roles, new Documentation(role)];
          break;
        }
        case "SUBCONT_ENGINEER": {
          roles = [...roles, new Engineer(role)];
          break;
        }
        case "SUBCONT_TECHNICIAN": {
          roles = [...roles, new Technician(role)];
          break;
        }
        case "SUBCONT_DOCUMENTATION": {
          roles = [...roles, new Documentation(role)];
          break;
        }
        case "ADMIN_CENTER": {
          roles = [...roles, new AdminCenter(role)];
          break;
        }
        case "GENERAL_MANAGER": {
          roles = [...roles, new GeneralManager(role)];
          break;
        }
      }
    });
    this.data.setRoles = roles;
  }
}
