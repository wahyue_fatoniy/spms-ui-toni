import { Component, ViewChild, OnDestroy, OnInit } from "@angular/core";
import { MatDialog, MatPaginator, MatSort } from "@angular/material";
import { PersonFormComponent } from "../person-form/person-form.component";
import { FormDialogRemoveComponent } from "../../template/form-dialog-remove/form-dialog-remove.component";
import { forkJoin, Subject } from "rxjs";
import { Person } from "../../models/person.model";
import { RoleService } from "../../../models/role-service/role.service";
import { FilterDataPipe } from "../../pipes/filterData/filter-data.pipe";
import { UserService } from "../../../models/user-service/user.service";
import { DatePipe } from "@angular/common";
import { AAAService } from "../../shared/aaa/aaa.service";
import { RoleAuthService } from "src/app/shared/aaa/role/role.service";
import { UserAuthService } from "src/app/shared/aaa/user/user.service";
import { UserFormComponent } from "../user-form/user-form.component";
import { Party } from "src/app/shared/models/party/party";
import { UserAuth } from "src/app/shared/aaa/user/user";
import { User } from "src/models/user-service/user.model";
import { CoreTable, TableConfig } from "../../shared/core-table";
import { PersonService } from "../../../service/person-service/person.service";
import { combineLatest } from "rxjs";
import { RegionService } from "../../../service/region-service/region.service";
import { takeUntil } from "rxjs/operators";

const CONFIG: TableConfig = {
  title: "Person",
  columns: ["no", "name", "address", "email", "phone", "gender", "birthDate"],
  typeFilters: [
    { value: "all", label: "All Column" },
    { value: "name", label: "Name" },
    { value: "address", label: "address" },
    { value: "email", label: "Email" },
    { value: "phone", label: "Phone" },
    { value: "gender", label: "Gender" },
    { value: "birthDate", label: "Birth Date" }
  ]
};

@Component({
  selector: "app-person-table",
  templateUrl: "./person-table.component.html",
  styleUrls: ["./person-table.component.css"]
})
export class PersonTableComponent extends CoreTable<Person>
  implements OnInit, OnDestroy {
  displayedColumns: string[] = [
    "no",
    "name",
    "address",
    "email",
    "phone",
    "gender",
    "birthDate",
    "action"
  ];
  datePipe: DatePipe = new DatePipe("id");
  @ViewChild(MatPaginator)
  paginator: MatPaginator;
  @ViewChild(MatSort)
  matSort: MatSort;
  filterDataPipe = new FilterDataPipe();
  typeFilter = "all";
  destroy: Subject<boolean> = new Subject<boolean>();

  constructor(
    public service: PersonService,
    public regionService: RegionService,
    public dialog: MatDialog,
    public roleService: RoleService,
    public aaa: AAAService,
    public userService: UserService,
    private roleAuth: RoleAuthService,
    private userAuth: UserAuthService,
    private user: UserService
  ) {
    super(CONFIG);
  }

  ngOnInit() {
    combineLatest(this.service.getData(), this.regionService.getData())
      .pipe(takeUntil(this.destroy))
      .subscribe(() => {
        this.setTable();
        this.userAuth
          .getToken()
          .pipe(takeUntil(this.destroy))
          .subscribe(_token => {
            this.roleAuth
              .findRoles(_token.getAccessToken())
              .pipe(takeUntil(this.destroy))
              .subscribe();
          });
      });
  }

  ngOnDestroy() {
    this.destroy.next(true);
    this.destroy.complete();
  }

  get loading(): boolean {
    return this.service.getLoading;
  }

  public setTable(): void {
    this.service.subscriptionData().subscribe((person: Person[]) => {
      person.forEach((elem: Object) => {
        elem["birthDate"] = new Date(elem["birthDate"]);
      });
      this.datasource.data = person;
      /**sorting datasource data*/
      this.datasource.sort = this.matSort;
      /**assignment datasource paginator */
      this.datasource.paginator = this.paginator;
      /**set datasource filter predicate to filter data datasource data */
      this.datasource.filterPredicate = (value: Person, keyword: string) => {
        return this.filterDataPipe.transform(value, keyword, this.typeFilter);
      };
    });
  }

  public onDelete(data: Person): void {
    const remove = this.dialog.open(FormDialogRemoveComponent, {
      data: {
        data: data,
        message: `Are you sure to delete ${data.getName} ?`
      }
    });
    remove.afterClosed().subscribe(result => {
      if (result !== "cancel") {
        this.service.delete(result).catch(err => console.log(err));
      }
    });
  }

  public onCreate(): void {
    const dialog = this.dialog.open(PersonFormComponent, {
      width: "1000px",
      disableClose: true,
      data: new Person()
    });
    dialog.afterClosed().subscribe((person: Person) => {
      if (person) {
        console.log(person);
        let roles = [];
        this.service
          .createWithReturn(person)
          .then(personVal => {
            for (let role of person.getRoles) {
              roles = [...roles, { ...role, party: personVal }];
            }
            this.roleService.create(roles).then(roleVal => {
              console.log(roleVal);
            });
            this.service
              .getData()
              .pipe(takeUntil(this.destroy))
              .subscribe();
          })
          .catch(err => console.log(err));
      }
    });
  }

  public onUpdate(person: Person): void {
    const dialog = this.dialog.open(PersonFormComponent, {
      width: "1000px",
      disableClose: true,
      data: new Person(JSON.parse(JSON.stringify(person)))
    });
    dialog.afterClosed().subscribe((personVal: Person) => {
      if (personVal) {
        const currentRoles = personVal.getRoles;
        const previousRoles = person.getRoles;
        let newRoles = [];
        let rolesUpdate = [];
        let inActiveRoles = [];
        let valUpdate = [];
        let valCreate = [];
        newRoles = this.getNewRoles(previousRoles, currentRoles);
        rolesUpdate = this.getRolesUpdate(previousRoles, currentRoles);
        inActiveRoles = this.getInActiveRoles(previousRoles, currentRoles);

        console.log(
          "new: ",
          newRoles,
          "update: ",
          rolesUpdate,
          "inActive: ",
          inActiveRoles
        );

        for (let role of [...rolesUpdate, ...inActiveRoles]) {
          valUpdate = [...valUpdate, { ...role, party: personVal }];
        }
        /**update role person*/
        this.roleService.update(valUpdate).catch(err => console.log(err));

        for (let role of newRoles) {
          valCreate = [...valCreate, { ...role, party: personVal }];
        }
        /**create new role person*/
        this.roleService.create(valCreate).catch(err => console.log(err));

        /**update person*/
        this.service.update(personVal).catch(err => console.log(err));

        this.service
          .getData()
          .pipe(takeUntil(this.destroy))
          .subscribe();
      }
    });
  }

  public getNewRoles(previous, currents) {
    return currents.filter(current => {
      return this.getIndexRoles(previous, current) === -1;
    });
  }

  public getInActiveRoles(previous, currents) {
    return previous
      .filter(prev => {
        return (
          this.getIndexRoles(currents, prev) === -1 &&
          prev["endDate"] === null &&
          prev["roleType"] !== "USER"
        );
      })
      .map(role => {
        console.log(role);
        role["endDate"] = this.datePipe.transform(
          new Date(),
          "yyyy-MM-ddTHH:mm:ssZ",
          "+0000"
        );
        return role;
      });
  }

  public getRolesUpdate(previous, currents) {
    return currents.filter(current => {
      return this.getIndexRoles(previous, current) !== -1;
    });
  }

  public getIndexRoles(roles: any[], role): number {
    return roles.findIndex(previous => {
      if (previous["roleType"] === role["roleType"]) {
        return previous.id === role.id;
      }
    });
  }

  public onCreateUser(person: Person): void {
    this.userAuth.findByEmail(person.getEmail)
    .pipe(takeUntil(this.destroy))
    .subscribe(user => {
      console.log(user.getUserDetail());
      const party = new Party(JSON.parse(JSON.stringify(person)));
      this.dialog.open(UserFormComponent, {
        width: "500px",
        data: { value: user, role: this.roleAuth, party: party }
      })
      .afterClosed().subscribe((_set: any) => {
        console.log(_set);
        if (_set) {
          if (_set.user) {
            const userAuth: UserAuth = new UserAuth(_set.data);
            userAuth.setEmail(party.getEmail());
            userAuth.setName(party.getName());
            const value: User = new User(_set.user);
            const roles: string[] = this.roleAuth.getIdsByRoleName(
              userAuth.getRoles()
            );
            this.userAuth.createUser(userAuth).subscribe(_userAuth => {
              this.userAuth.getToken().subscribe(_token => {
                forkJoin(
                  this.userAuth.addRoles(
                    _userAuth,
                    [],
                    roles,
                    _token.getAccessToken()
                  ),
                  this.user.create(value)
                ).subscribe(_success => {    });
              });
            });
          } else {
            const userAuth: UserAuth = new UserAuth(_set.data);
            this.userAuth.findByEmail(userAuth.getEmail()).subscribe(_user => {
              this.userAuth.getToken().subscribe(_token => {
                this.userAuth
                  .addRoles(
                    userAuth,
                    this.roleAuth.getIdsByRoleName(_user.getRoles()),
                    this.roleAuth.getIdsByRoleName(userAuth.getRoles()),
                    _token.getAccessToken()
                  )
                  .subscribe(_success => {
                  });
              });
            });
          }
        }
      });
    });
  }
}
