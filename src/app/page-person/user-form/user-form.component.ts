import { Component, OnInit, Inject, OnDestroy } from "@angular/core";
import {
  FormBuilder,
  FormGroup,
  Validators,
  FormControl
} from "@angular/forms";
import { PasswordValidation } from "../../customValidator";
import { MAT_DIALOG_DATA } from "@angular/material";
import { Subject } from "rxjs";
import { UserAuth } from "../../shared/aaa/user/user";
import { RoleAuthService } from "src/app/shared/aaa/role/role.service";
import { User } from "src/app/shared/models/user/user";
import { RoleType } from "src/app/shared/models/role/role";
import { Party } from "src/app/shared/models/party/party";
import { UserAuthService } from "src/app/shared/aaa/user/user.service";
import { RoleAuth } from "src/app/shared/aaa/role/role";
import { takeUntil } from "rxjs/operators";

export interface UserForm {
  value: UserAuth;
  role: RoleAuthService;
  party: Party;
}

@Component({
  selector: "app-user-form",
  templateUrl: "./user-form.component.html",
  styleUrls: ["./user-form.component.css"]
})
export class UserFormComponent implements OnInit, OnDestroy {
  form: FormGroup;
  public hidePassword = true;
  public disabledPassword = false;
  public title = "Register";
  public action = "create";
  roles: FormControl = new FormControl(this.data.value.getUserRoles(), [
    Validators.required
  ]);
  destroy$: Subject<boolean> = new Subject();

  constructor(
    @Inject(MAT_DIALOG_DATA) public data: UserForm,
    public formBuilder: FormBuilder,
    private userAuth: UserAuthService
  ) {}
  user: User;
  loading: boolean;

  ngOnInit() {
    this.setFormValidation();
    this.onInitialize();
    this.form
      .get("username")
      .valueChanges.pipe(takeUntil(this.destroy$))
      .subscribe(_set => {
        if (_set) {
          this.data.value.setUsername(_set);
          this.user.setUsername(_set);
        }
      });
    this.form
      .get("passwordGroup")
      .get("password")
      .valueChanges.pipe(takeUntil(this.destroy$))
      .subscribe(_set => {
        if (_set) {
          this.data.value.setPassword(_set);
          this.user.setPassword(_set);
        }
      });
    this.form
      .get("roles")
      .valueChanges.pipe(takeUntil(this.destroy$))
      .subscribe(_set => {
        if (_set && this.data.value) {
          this.data.value.setRoles(_set);
        }
      });
    this.userAuth
      .getToken()
      .pipe(takeUntil(this.destroy$))
      .subscribe(_token => {
        this.data.role
          .findRolesByUserId(this.data.value.getId(), _token.getAccessToken())
          .pipe(takeUntil(this.destroy$))
          .subscribe((_success: RoleAuth[]) => {
            this.form
              .get("roles")
              .setValue(
                _success.length === 0
                  ? null
                  : _success.map(_set => _set.getName())
              );
          });
      });
  }

  ngOnDestroy() {
    this.destroy$.next(true);
    this.destroy$.complete();
  }

  getPassword(): string {
    const user = this.data.value.getUserDetail();
    return user ? user.getPassword() : null;
  }

  getUsername(): string {
    const user = this.data.value.getUserDetail();
    return user ? user.getUsername() : null;
  }

  onInitialize(): void {
    if (this.data.value.getUserDetail()) {
      this.title = "Update Roles";
      this.action = "update";
      this.disabledPassword = true;
      this.form.get("username").disable();
      this.form.get("passwordGroup").disable();
      // this.form.get('email').disable();
    } else {
      this.user = new User();
      this.user.setRoleType(RoleType.User);
      this.user.setParty(this.data.party);
      this.user.setEmail(this.data.party.getEmail());
    }
  }

  setFormValidation(): void {
    this.form = this.formBuilder.group({
      username: [this.getUsername(), Validators.required],
      passwordGroup: this.formBuilder.group(
        {
          password: [
            this.getPassword(),
            Validators.compose([Validators.required, Validators.minLength(6)])
          ],
          confirmPassword: [this.getPassword(), Validators.required]
        },
        { validator: PasswordValidation }
      ),
      roles: [[], Validators.required]
    });
  }

  invalid(): boolean {
    return this.roles.invalid;
  }

  getRoles(): RoleAuth[] {
    return this.data.role.getRoleSpms();
  }
}
