import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-page-region',
  template: `<app-region-table></app-region-table>`,
  styles: []
})
export class PageRegionComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
