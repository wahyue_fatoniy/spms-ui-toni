import { Component, OnInit, Inject } from '@angular/core';
import { Region } from '../../../models/region-service/region.model';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { initialLetterValidate } from '../../customValidator';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-region-form',
  templateUrl: './region-form.component.html',
  styleUrls: ['./region-form.component.css']
})
export class RegionFormComponent implements OnInit {
  form: FormGroup;
  subscriptions: Subscription;

  constructor(
    public dialogRef: MatDialogRef<RegionFormComponent>,
    @Inject(MAT_DIALOG_DATA) public region: Region,
    public formBuilder: FormBuilder
  ) {}

  ngOnInit() {
    this.setFormValidation();
    this.onFormValid();
    this.onBeforeClose();
  }

  setFormValidation(): void {
    this.form = this.formBuilder.group({
      id: [this.region['id']],
      name: [
        this.region['name'],
        Validators.compose([Validators.required, initialLetterValidate])
      ],
      latitude: [this.region.getLatitude, Validators.required],
      longitude: [this.region.getLongitude, Validators.required]
    });
  }

  onFormValid(): void {
    this.subscriptions = this.form.statusChanges.subscribe((status: string) => {
      if (status === 'VALID') {
        this.region = new Region(this.form.value);
        console.log(this.region);
      }
    });
  }

  onBeforeClose(): void {
    this.subscriptions.add(
      this.dialogRef.beforeClose().subscribe(() => {
        this.subscriptions.unsubscribe();
      })
    );
  }
}
