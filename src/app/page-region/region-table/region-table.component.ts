import { Component, OnInit, OnDestroy, ViewChild } from '@angular/core';
import { MatTableDataSource, MatDialog, MatPaginator, MatSort } from '@angular/material';
import { FormDialogRemoveComponent } from '../../template/form-dialog-remove/form-dialog-remove.component';
import { Subscription } from 'rxjs';
import { AreaTableEvent, Filter } from '../../../models/interface/interface';
import { RegionService } from '../../../models/region-service/region.service';
import { Region } from '../../../models/region-service/region.model';
import { RegionFormComponent } from '../region-form/region-form.component';
import { FilterDataPipe } from '../../pipes/filterData/filter-data.pipe';

@Component({
  selector: 'app-region-table',
  templateUrl: './region-table.component.html',
  styleUrls: ['./region-table.component.css']
})
export class RegionTableComponent implements OnInit, OnDestroy {
  displayedColumns: string[] = [
    'no',
    'name',
    'latitude',
    'longitude',
    'action'
  ];
  pageSize: number;
  pageIndex = 0;
  subscribe = false;
  loading = true;
  datasource: MatTableDataSource<Object> = new MatTableDataSource();
  tableTitle = 'Region Table';
  subData: Subscription;
  @ViewChild(MatPaginator)
  paginator: MatPaginator;
  @ViewChild(MatSort)
  matSort: MatSort;
  filterDataPipe = new FilterDataPipe();
  typeFilter = 'all';

  constructor(
    public service: RegionService,
    public dialog: MatDialog
  ) { }

  ngOnInit() {
    /**get previous pageSize from localStorage */
    const PAGE = localStorage.getItem('pageSize');
    /**assignment variable pageSize with value PAGE */
    this.pageSize = PAGE ? parseInt(PAGE, 0) : 10;

    this.service.getAll.then(() => {
      this.subscribe = true;
      this.setTable();
    })
    .catch(err => console.error(err));
  }

  ngOnDestroy() {
    if (this.subscribe) {
      this.subData.unsubscribe();
    }
  }

  onSetPage(event): void {
    /**assignment variable pageSize with value event paginator */
    this.pageSize = event.pageSize;
    /**assignment variable pageIndex with value event paginator */
    this.pageIndex = event.pageIndex;
    /**save current pageSize in localStorage */
    localStorage.setItem('pageSize', event.pageSize);
  }

  setTable(): void {
    this.subData = this.service.getRegion.subscribe((region: Array<Region>) => {
       /**set loading to false */
       this.loading = false;
       /**assignment datasource data with siteWork data */
       this.datasource.data = region;
       /**sorting datasource data*/
       this.datasource.sort = this.matSort;
       /**assignment datasource paginator */
       this.datasource.paginator = this.paginator;
       /**set datasource filter predicate to filter data datasource data */
       this.datasource.filterPredicate = (value: Region, keyword: string) => {
         return this.filterDataPipe.transform(value, keyword, this.typeFilter);
       };
    });
  }

  onClickAction(value: AreaTableEvent): void {
    if (value['action'] === 'delete') {
      const remove = this.dialog.open(FormDialogRemoveComponent, {
        data: {
          data: value['value']['id'],
          message: `Are you sure to delete ${value['value']['name']} ?`
        }
      });
      const subDialogRemove: Subscription = remove.afterClosed().subscribe(result => {
        if (result !== 'cancel') {
          this.service.delete(result)
          .catch(err => console.error(err));
        }
        subDialogRemove.unsubscribe();
      });

    } else {
      const dialog = this.dialog.open(RegionFormComponent, {
        width: '850px',
        disableClose: true,
        data: value['value']
      });
      const subDialog: Subscription = dialog.afterClosed().subscribe(result => {
        if (result !== 'cancel') {
          if (value['action'] === 'create') {
            this.service.create(result)
            .catch(err => console.error(err));
          } else {
            this.service.update(result)
            .catch(err => console.error(err));
          }
        }
        subDialog.unsubscribe();
      });
    }
  }
}
