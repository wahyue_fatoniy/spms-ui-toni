import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PageAdminCenterComponent } from './page-admin-center.component';

describe('PageAdminCenterComponent', () => {
  let component: PageAdminCenterComponent;
  let fixture: ComponentFixture<PageAdminCenterComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PageAdminCenterComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PageAdminCenterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
