import { Component, OnInit } from '@angular/core';
import { AdminCenterService } from 'src/service/admin-center/admin-center.service';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { Options } from 'src/app/components/table-view-component/table-view-component.component';
import { AdminCenter } from 'src/app/models/adminCenter.model';
import { DatePipe } from '@angular/common';

@Component({
  selector: 'app-page-admin-center',
  templateUrl: './page-admin-center.component.html',
  styleUrls: ['./page-admin-center.component.css']
})
export class PageAdminCenterComponent implements OnInit {
  destroy$: Subject<boolean> = new Subject();
  options: Options;
  data: Object[] = [];
  datePipe: DatePipe = new DatePipe('id');
  constructor(
    public adminService: AdminCenterService
  ) { }

  ngOnInit() {
    this.options = {
      loading: true,
      config: {
        title: "Admin Center",
        columns: [
          { title: "No.", columnDef: "no" },
          { title: "Name", columnDef: "name" },
          { title: "Address", columnDef: "address" },
          { title: "Email", columnDef: "email" },
          { title: "Phone", columnDef: "phone" },
          { title: 'BirthDate', columnDef: 'birthDate' },
          { title: 'NIP', columnDef: 'nip' },
        ],
        typeFilters: [
          { value: "name", label: "Name" },
          { value: "address", label: "Address" },
          { value: "email", label: "Email" },
          { value: "phone", label: "Phone" },
          { value: "gender", label: "Gender" },
          { value: "birthDate", label: "BirthDate" },
          { value: "nip", label: "NIP" },
        ],
        width: '150%'
      }
    };
    this.getData();
    this.subscriptionData();
  }

  getData() {
    this.adminService.getData()
    .pipe(takeUntil(this.destroy$))
    .subscribe(() => {
      this.options = {
        loading: false,
        config: this.options.config
      };
    });
  }

  subscriptionData() {
    this.adminService.subscriptionData()
    .pipe(takeUntil(this.destroy$))
    .subscribe(val => {
      this.data = [];
      val.forEach(admin => {
      this.data = [
        ...this.data,
        {
          name: admin.getParty['name'],
          address: admin.getParty['address'],
          email: admin.getParty['email'],
          phone: admin.getParty['phone'],
          gender: admin.getParty['gender'],
          birthDate: this.datePipe.transform(admin.getParty['birthDate'], 'dd MMMM yyyy'),
          nip: admin.getNIP
        }
      ];
    });
    });
  }
}
