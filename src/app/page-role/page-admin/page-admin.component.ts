import { Component, OnInit, ViewChild, OnDestroy } from "@angular/core";
import { CoreTable, TableConfig } from "src/app/shared/core-table";
import { Admin } from "src/app/models/admin.model";
import { MatPaginator, MatSort } from "@angular/material";
import { AdminService } from "src/service/admin-service/admin.service";
import { Subject } from "rxjs";
import { takeUntil } from "rxjs/operators";

const CONFIG: TableConfig = {
  title: "Admin",
  columns: [
    "no",
    "name",
    "address",
    "email",
    "phone",
    "gender",
    "birthDate",
    "nip",
    "region"
  ],
  typeFilters: [
    { value: "all", label: "All" },
    { value: "name", label: "Name" },
    { value: "address", label: "Address" },
    { value: "email", label: "Email" },
    { value: "phone", label: "Phone" },
    { value: "gender", label: "Gender" },
    { value: "birthDate", label: "Birth Date" },
    { value: "nip", label: "NIP" },
    { value: "region", label: "Region" }
  ]
};

@Component({
  selector: "app-page-admin",
  templateUrl: "./page-admin.component.html",
  styleUrls: ["./page-admin.component.css"]
})
export class PageAdminComponent extends CoreTable<Admin>
implements OnInit, OnDestroy {
  @ViewChild(MatPaginator)
  paginator: MatPaginator;
  @ViewChild(MatSort)
  sort: MatSort;
  destroy: Subject<boolean> = new Subject();

  constructor(public service: AdminService) {
    super(CONFIG);
  }

  ngOnInit() {
    this.service.getData()
    .pipe(takeUntil(this.destroy))
    .subscribe(() => this.initDataSource());
  }

  ngOnDestroy() {
    this.destroy.next(true);
    this.destroy.complete();
  }

  public get loading(): boolean {
    return this.service.getLoading;
  }

  initDataSource(): void {
    this.service.subscriptionData()
    .pipe(takeUntil(this.destroy))
    .subscribe((val: Admin[]) => {
      val.forEach((admin: Object) => {
        admin["party"]["birthDate"] = new Date(admin["party"]["birthDate"]);
      });
      this.datasource.data = val.filter(admin => admin.getEndDate === null);
      this.datasource.sort = this.sort;
      /**assignment datasource paginator */
      this.datasource.paginator = this.paginator;
      /**set datasource filter predicate to filter data datasource data */
      let filterData: any;
      this.datasource.filterPredicate = (value: Admin, keyword: string) => {
        filterData = {
          name: value.getParty["name"],
          address: value.getParty["address"],
          email: value.getParty["email"],
          phone: value.getParty["phone"],
          gender: value.getParty["gender"],
          birthDate: value.getParty["birthDate"],
          nip: value.getNIP,
          region: value.getRegion.getName
        };
        return this.filterPredicate(filterData, keyword, this.typeFilter);
      };
    });
  }

  public sortData(sort): void {
    this.datasource.filteredData = this.datasource.filteredData.sort((a, b) => {
      const isAsc = sort.direction === "asc";
      switch (sort.active) {
        case "name":
          return this.compare(a.getParty["name"], b.getParty["name"], isAsc);
        case "address":
          return this.compare(
            a.getParty["address"],
            b.getParty["address"],
            isAsc
          );
        case "email":
          return this.compare(a.getParty["email"], b.getParty["email"], isAsc);
        case "phone":
          return this.compare(a.getParty["phone"], b.getParty["phone"], isAsc);
        case "gender":
          return this.compare(
            a.getParty["gender"],
            b.getParty["gender"],
            isAsc
          );
        case "birthDate":
          return this.compare(
            a.getParty["birthDate"].toString(),
            b.getParty["birthDate"].toString(),
            isAsc
          );
        case "region":
          return this.compare(a.getRegion["name"], b.getRegion["name"], isAsc);
        default:
          return 0;
      }
    });
  }
}
