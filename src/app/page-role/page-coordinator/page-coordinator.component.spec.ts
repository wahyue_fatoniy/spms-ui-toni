import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PageCoordinatorComponent } from './page-coordinator.component';

describe('PageCoordinatorComponent', () => {
  let component: PageCoordinatorComponent;
  let fixture: ComponentFixture<PageCoordinatorComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PageCoordinatorComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PageCoordinatorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
