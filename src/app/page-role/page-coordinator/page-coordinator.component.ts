import { Component, OnInit, OnDestroy, ViewChild } from "@angular/core";
import { TableConfig, CoreTable } from "src/app/shared/core-table";
import { Coordinator } from "src/app/models/coordinator.model";
import { CoordinatorService } from "src/service/coordinator-service/coordinator.service";
import { MatPaginator, MatSort } from "@angular/material";
import { Subject } from "rxjs";
import { takeUntil } from "rxjs/operators";

const CONFIG: TableConfig = {
  title: "Coordinator",
  columns: [
    "no",
    "name",
    "address",
    "email",
    "phone",
    "gender",
    "birthDate",
    "nip",
    'region'
  ],
  typeFilters: [
    { value: "all", label: "All" },
    { value: "name", label: "Name" },
    { value: "address", label: "Address" },
    { value: "email", label: "Email" },
    { value: "phone", label: "Phone" },
    { value: "gender", label: "Gender" },
    { value: "birthDate", label: "Birth Date" },
    { value: "nip", label: "NIP" },
    { value: "region", label: "Region" }
  ]
};

@Component({
  selector: "app-page-coordinator",
  templateUrl: "./page-coordinator.component.html",
  styleUrls: ["./page-coordinator.component.css"]
})
export class PageCoordinatorComponent extends CoreTable<Coordinator>
  implements OnInit, OnDestroy {
  @ViewChild(MatPaginator)
  paginator: MatPaginator;
  @ViewChild(MatSort)
  sort: MatSort;
  destroy: Subject<boolean> = new Subject();

  constructor(public service: CoordinatorService) {
    super(CONFIG);
  }

  ngOnInit() {
    this.service
      .getData()
      .pipe(takeUntil(this.destroy))
      .subscribe(() => this.initDataSource());
  }

  ngOnDestroy() {
    this.destroy.next(true);
    this.destroy.complete();
  }

  public get loading(): boolean {
    return this.service.getLoading;
  }

  initDataSource(): void {
    this.service
      .subscriptionData()
      .pipe(takeUntil(this.destroy))
      .subscribe((val: Coordinator[]) => {
        val.forEach((coordinator: Object) => {
          coordinator["party"]["birthDate"] = new Date(
            coordinator["party"]["birthDate"]
          );
        });
        this.datasource.data = val;
        this.datasource.sort = this.sort;
        /**assignment datasource paginator */
        this.datasource.paginator = this.paginator;
        /**set datasource filter predicate to filter data datasource data */
        let filterData: any;
        this.datasource.filterPredicate = (
          value: Coordinator,
          keyword: string
        ) => {
          filterData = {
            name: value.getParty["name"],
            address: value.getParty["address"],
            email: value.getParty["email"],
            phone: value.getParty["phone"],
            gender: value.getParty["gender"],
            birthDate: value.getParty["birthDate"],
            nip: value.getNIP
          };
          return this.filterPredicate(filterData, keyword, this.typeFilter);
        };
      });
  }

  public sortData(sort): void {
    this.datasource.filteredData = this.datasource.filteredData.sort((a, b) => {
      const isAsc = sort.direction === "asc";
      switch (sort.active) {
        case "name":
          return this.compare(a.getParty["name"], b.getParty["name"], isAsc);
        case "address":
          return this.compare(
            a.getParty["address"],
            b.getParty["address"],
            isAsc
          );
        case "email":
          return this.compare(a.getParty["email"], b.getParty["email"], isAsc);
        case "phone":
          return this.compare(a.getParty["phone"], b.getParty["phone"], isAsc);
        case "gender":
          return this.compare(
            a.getParty["gender"],
            b.getParty["gender"],
            isAsc
          );
        case "birthDate":
          return this.compare(
            a.getParty["birthDate"].toString(),
            b.getParty["birthDate"].toString(),
            isAsc
          );
        case "region":
          return this.compare(a.getRegion["name"], b.getRegion["name"], isAsc);
        default:
          return 0;
      }
    });
  }
}
