import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PageCustomerComponent } from './page-customer.component';

describe('PageCustomerComponent', () => {
  let component: PageCustomerComponent;
  let fixture: ComponentFixture<PageCustomerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PageCustomerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PageCustomerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
