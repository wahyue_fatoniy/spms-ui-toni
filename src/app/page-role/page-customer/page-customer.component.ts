import { Component, OnInit, OnDestroy, ViewChild } from '@angular/core';
import { CoreTable, TableConfig } from 'src/app/shared/core-table';
import { Customer } from 'src/app/models/customer.model';
import { MatPaginator, MatSort } from '@angular/material';
import { Subject } from 'rxjs';
import { CustomerService } from 'src/service/customer-service/customer.service';
import { takeUntil } from 'rxjs/operators';

const CONFIG: TableConfig = {
  title: "Customer",
  columns: [
    "no",
    "name",
    "address",
    "email",
    "phone"
  ],
  typeFilters: [
    { value: "all", label: "All" },
    { value: "name", label: "Name" },
    { value: "address", label: "Address" },
    { value: "email", label: "Email" },
    { value: "phone", label: "Phone" }
  ]
};

@Component({
  selector: 'app-page-customer',
  templateUrl: './page-customer.component.html',
  styleUrls: ['./page-customer.component.css']
})
export class PageCustomerComponent extends CoreTable<Customer>
implements OnInit, OnDestroy {
  @ViewChild(MatPaginator)
  paginator: MatPaginator;
  @ViewChild(MatSort)
  sort: MatSort;
  destroy: Subject<boolean> = new Subject();

  constructor(public service: CustomerService) {
    super(CONFIG);
  }

  ngOnInit() {
    this.service.getData()
    .pipe(takeUntil(this.destroy))
    .subscribe(() => this.initDataSource());
  }

  ngOnDestroy() {
    this.destroy.next(true);
    this.destroy.complete();
  }

  public get loading(): boolean {
    return this.service.getLoading;
  }

  initDataSource(): void {
    this.service.subscriptionData()
    .pipe(takeUntil(this.destroy))
    .subscribe((val: Customer[]) => {
      this.datasource.data = val;
      this.datasource.sort = this.sort;
      /**assignment datasource paginator */
      this.datasource.paginator = this.paginator;
      /**set datasource filter predicate to filter data datasource data */
      let filterData: any;
      this.datasource.filterPredicate = (value: Customer, keyword: string) => {
        filterData = {
          name: value.getParty["name"],
          address: value.getParty["address"],
          email: value.getParty["email"],
          phone: value.getParty["phone"]
        };
        return this.filterPredicate(filterData, keyword, this.typeFilter);
      };
    });
  }

  public sortData(sort): void {
    this.datasource.filteredData = this.datasource.filteredData.sort((a, b) => {
      const isAsc = sort.direction === "asc";
      switch (sort.active) {
        case "name":
          return this.compare(a.getParty["name"], b.getParty["name"], isAsc);
        case "address":
          return this.compare(
            a.getParty["address"],
            b.getParty["address"],
            isAsc
          );
        case "email":
          return this.compare(a.getParty["email"], b.getParty["email"], isAsc);
        case "phone":
          return this.compare(a.getParty["phone"], b.getParty["phone"], isAsc);
        default:
          return 0;
      }
    });
  }

}
