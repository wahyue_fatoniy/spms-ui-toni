import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PageEngineerComponent } from './page-engineer.component';

describe('PageEngineerComponent', () => {
  let component: PageEngineerComponent;
  let fixture: ComponentFixture<PageEngineerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PageEngineerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PageEngineerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
