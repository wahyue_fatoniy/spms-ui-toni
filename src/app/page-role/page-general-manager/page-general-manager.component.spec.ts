import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PageGeneralManagerComponent } from './page-general-manager.component';

describe('PageGeneralManagerComponent', () => {
  let component: PageGeneralManagerComponent;
  let fixture: ComponentFixture<PageGeneralManagerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PageGeneralManagerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PageGeneralManagerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
