import { Component, OnInit } from "@angular/core";
import { GeneralManager } from "src/app/models/generalManager.model";
import { Options } from "src/app/components/table-view-component/table-view-component.component";
import { Subject } from "rxjs";
import { GeneralManagerService } from "src/service/general-manager/general-manager.service";
import { takeUntil } from "rxjs/operators";
import { DatePipe } from "@angular/common";

@Component({
  selector: "app-page-general-manager",
  templateUrl: "./page-general-manager.component.html",
  styleUrls: ["./page-general-manager.component.css"]
})
export class PageGeneralManagerComponent implements OnInit {
  destroy$: Subject<boolean> = new Subject();
  options: Options;
  data: Object[] = [];
  datePipe: DatePipe = new DatePipe('id');

  constructor(private gmService: GeneralManagerService) {}

  ngOnInit() {
    this.options = {
      loading: true,
      config: {
        title: "General Manager",
        columns: [
          { title: "No.", columnDef: "no" },
          { title: "Name", columnDef: "name" },
          { title: "Address", columnDef: "address" },
          { title: "Email", columnDef: "email" },
          { title: "Phone", columnDef: "phone" },
          { title: "BirthDate", columnDef: "birthDate" },
          { title: "NIP", columnDef: "nip" }
        ],
        typeFilters: [
          { value: "name", label: "Name" },
          { value: "address", label: "Address" },
          { value: "email", label: "Email" },
          { value: "phone", label: "Phone" },
          { value: "gender", label: "Gender" },
          { value: "birthDate", label: "BirthDate" },
          { value: "nip", label: "NIP" }
        ],
        width: '150%'
      }
    };
    this.getData();
    this.subscriptionData();
  }

  getData() {
    this.gmService
      .getData()
      .pipe(takeUntil(this.destroy$))
      .subscribe(() => {
        this.options = {
          loading: false,
          config: this.options.config
        };
      });
  }

  subscriptionData() {
    this.gmService
      .subscriptionData()
      .pipe(takeUntil(this.destroy$))
      .subscribe((val: GeneralManager[]) => {
      this.data = [];
        val.forEach(gm => {
          this.data = [
            ...this.data,
            {
              name: gm.getParty['name'],
              address: gm.getParty['address'],
              email: gm.getParty['email'],
              phone: gm.getParty['phone'],
              gender: gm.getParty['gender'],
              birthDate: this.datePipe.transform(gm.getParty['birthDate'], 'dd MMMM yyyy'),
              nip: gm.getNIP
            }
          ];
        });
      });
  }
}
