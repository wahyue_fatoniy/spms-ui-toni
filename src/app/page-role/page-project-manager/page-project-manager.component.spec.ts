import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PageProjectManagerComponent } from './page-project-manager.component';

describe('PageProjectManagerComponent', () => {
  let component: PageProjectManagerComponent;
  let fixture: ComponentFixture<PageProjectManagerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PageProjectManagerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PageProjectManagerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
