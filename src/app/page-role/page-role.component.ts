import { Component } from "@angular/core";
import { MatTabChangeEvent } from "@angular/material";
import { PageSalesComponent } from "./page-sales/page-sales.component";
import { PageProjectManagerComponent } from "./page-project-manager/page-project-manager.component";
import { PageCoordinatorComponent } from "./page-coordinator/page-coordinator.component";
import { PageEngineerComponent } from "./page-engineer/page-engineer.component";
import { PageTechnicianComponent } from "./page-technician/page-technician.component";
import { PageDocumentationComponent } from "./page-documentation/page-documentation.component";
import { PagePartnerComponent } from "./page-partner/page-partner.component";
import { PageCustomerComponent } from "./page-customer/page-customer.component";
import { PageAdminComponent } from "./page-admin/page-admin.component";
import { PageSubcontComponent } from "./page-subcont/page-subcont.component";
import { PageFinanceComponent } from "./page-finance/page-finance.component";
import { PageAdminCenterComponent } from "./page-admin-center/page-admin-center.component";
import { PageGeneralManagerComponent } from "./page-general-manager/page-general-manager.component";

interface Pages {
  title: string;
  component: any;
  img: string;
}
@Component({
  selector: "app-page-role",
  template: `
    <mat-tab-group
      style="margin-top: -75px"
      (selectedTabChange)="tabChange($event)"
    >
      <mat-tab *ngFor="let page of pages; let i = index">
        <ng-template mat-tab-label>
          <img
            src="/assets/img/collection/{{ page.img }}"
            style="margin-bottom: 10px;"
          />&nbsp;&nbsp;
          {{ page.title }}
        </ng-template>
        <div class="container-home">
          <span *ngIf="i === index">
            <ng-container *ngComponentOutlet="page.component"></ng-container>
          </span>
        </div>
      </mat-tab>
    </mat-tab-group>
  `,
  styleUrls: ["./page-role.component.css"]
})
export class PageRoleComponent {
  index: number;
  pages: Pages[] = [
    {
      title: "General Manager",
      component: PageGeneralManagerComponent,
      img: "admin.png"
    },
    {
      title: "Project Manager",
      component: PageProjectManagerComponent,
      img: "pm.png"
    },
    { title: "Finance", component: PageFinanceComponent, img: "finance2.png" },
    { title: "Admin", component: PageAdminComponent, img: "customer.png" },
    {
      title: "Admin Center",
      component: PageAdminCenterComponent,
      img: "admin_center.png"
    },
    {
      title: "Coordinator",
      component: PageCoordinatorComponent,
      img: "cordinator.png"
    },
    { title: "Sales", component: PageSalesComponent, img: "sales.png" },
    {
      title: "Engineer",
      component: PageEngineerComponent,
      img: "ENGINEER.png"
    },
    {
      title: "Technician",
      component: PageTechnicianComponent,
      img: "TECHNICIAN.png"
    },
    {
      title: "Documentation",
      component: PageDocumentationComponent,
      img: "DOCUMENTATION.png"
    },
    { title: "Subcont", component: PageSubcontComponent, img: "subcont.png" },
    { title: "Partner", component: PagePartnerComponent, img: "partner.png" },
    {
      title: "Customer",
      component: PageCustomerComponent,
      img: "customer2.png"
    }
  ];

  constructor() {
    this.index = 0;
  }

  tabChange(event: MatTabChangeEvent) {
    this.index = event.index;
  }
}
