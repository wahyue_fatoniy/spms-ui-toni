import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PageSubcontComponent } from './page-subcont.component';

describe('PageSubcontComponent', () => {
  let component: PageSubcontComponent;
  let fixture: ComponentFixture<PageSubcontComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PageSubcontComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PageSubcontComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
