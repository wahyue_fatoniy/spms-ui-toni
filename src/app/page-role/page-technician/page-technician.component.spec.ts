import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PageTechnicianComponent } from './page-technician.component';

describe('PageTechnicianComponent', () => {
  let component: PageTechnicianComponent;
  let fixture: ComponentFixture<PageTechnicianComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PageTechnicianComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PageTechnicianComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
