import { Component, OnInit, AfterViewInit, ElementRef } from '@angular/core';
import { Router } from '@angular/router';
import { AAAService } from '../shared/aaa/aaa.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit, AfterViewInit {

  constructor(
    private aaa: AAAService,
    private router: Router,
    private elementRef: ElementRef
    ) {
  }

  ngOnInit() {
    const auth = this.aaa.isAuthenticated();
    if (!auth) {
      console.log('got to login');
      this.router.navigate(["login"]);
    } else {
      this.router.config = this.aaa.getCurrentUser().setRouterRedirect(this.router.config);
    }
  }

  ngAfterViewInit() {
    this.elementRef.nativeElement.ownerDocument.body.style.backgroundColor = '#eeeeee';
 }

}
