import { Pipe, PipeTransform } from "@angular/core";
import * as moment from "moment";

@Pipe({
  name: "dateDuration"
})
export class DateDurationPipe implements PipeTransform {
  transform(date: any): any {
    let duration = moment([
      new Date().getFullYear(),
      new Date().getMonth(),
      new Date().getDate()
    ])
      .diff(
        moment([
          new Date(date).getFullYear(),
          new Date(date).getMonth(),
          new Date(date).getDate()
        ]),
        "months",
        true
      )
      .toFixed(1);

      if (duration.charAt(0) === '-') {
        return '0.0';
      } else {
        return duration;
      }
  }
}
