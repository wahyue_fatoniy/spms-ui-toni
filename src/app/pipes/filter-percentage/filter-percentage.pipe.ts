import { Pipe, PipeTransform } from "@angular/core";
import { NumberInStringPipe } from "../numberInString/number-in-string.pipe";

@Pipe({
  name: "filterPercentage"
})
export class FilterPercentagePipe implements PipeTransform {
  private getNumber: NumberInStringPipe = new NumberInStringPipe();
  transform(symbol: string, field: string, data: Object[]): any[] {
    let operator = symbol.charAt(0);
    let percentageValue = this.getNumber.transform(symbol);
    if (operator === "<") {
      data = data.filter(val => {
        return (val ? val[field] : null) < percentageValue;
      });
    } else if (operator === ">") {
      data = data.filter(val => {
        return (val ? val[field] : null) > percentageValue;
      });
    } else if (operator === "=") {
      data = data.filter(val => {
        return (val ? val[field] : null) === percentageValue;
      });
    }
    return data;
  }
}
