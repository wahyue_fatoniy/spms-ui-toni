import { Pipe, PipeTransform } from '@angular/core';
import { DatePipe } from '@angular/common';
import { ReplaySubject } from 'rxjs';

@Pipe({
  name: 'filterData'
})
export class FilterDataPipe implements PipeTransform {
  private format: DatePipe = new DatePipe('id');
  transform<T>(data: T, keyword: string, typeFilter: string | Object): boolean {
    keyword = keyword.toLowerCase();
    /**assignment value with parameter data */
    let value = Object.assign(JSON.parse(JSON.stringify({})), data);
    /**set date to longDate*/
    value = this.toStringDate(value);
    if (typeFilter === 'all') {
      /**set Object to JSON stringify  */
      value = JSON.stringify(value);
    } else {
      /**set Object to JSON stringify  */
      if (typeof typeFilter === 'object') {
        /** typeFilter object if key value of data is object
         * {id: 1, party: { id: 1, name:'test' }}.
         * data of typeFilter === object { value: 'party', key: 'name' }
         * */
        value = JSON.stringify(value[typeFilter['value']][typeFilter['key']]);
      } else {
        value = JSON.stringify(value[typeFilter]);
      }
    }
    if (value !== undefined && value !== null) {
      value = value
        .toString()
        .trim()
        .toLowerCase();
      return value.indexOf(keyword) !== -1;
    }
  }

  private toStringDate(data: Object) {
    for (const i in data) {
      if (Object.prototype.toString.call(data[i]) === '[object Date]') {
        data[i] = this.format.transform(data[i], 'longDate');
      }
    }
    return data;
  }
}
