import { Pipe, PipeTransform } from '@angular/core';
import { ReplaySubject } from 'rxjs';

@Pipe({
  name: 'filterReplay'
})
export class FilterReplayPipe implements PipeTransform {
  emitValue: ReplaySubject<Object> = new ReplaySubject<Array<Object>>(1);

  transform(data: Array<any>, keyword: string): void {
    this.emitValue.next(data.filter(val => {
      val = JSON.stringify(val);
      val = val.toString().trim().toLowerCase();
      return val.indexOf(keyword.toLowerCase()) !== -1;
    }));
  }

}
