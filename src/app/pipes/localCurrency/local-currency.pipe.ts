import { Pipe, PipeTransform } from '@angular/core';
import { CurrencyPipe } from '@angular/common';
import { NumberInStringPipe } from '../numberInString/number-in-string.pipe';

@Pipe({
  name: 'localCurrency'
})
export class LocalCurrencyPipe implements PipeTransform {
  private currencyPipe: CurrencyPipe = new CurrencyPipe('id');
  private numberInStringPipe: NumberInStringPipe = new NumberInStringPipe();

  constructor() {}

  transform(value: number): string | null {
      value = this.numberInStringPipe.transform(value);
      return this.currencyPipe.transform(value, 'Rp. ', 'symbol', '1.0');
  }
}
