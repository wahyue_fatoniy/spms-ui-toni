import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'numberInString'
})
export class NumberInStringPipe implements PipeTransform {
  transform(value: any): number {
    if (value !== null && value !== undefined) {
      value = value.toString();
      if (value.match(/[+-]?\d+/g) !== null && value.match(/[+-]?\d+/g) !== undefined) {
        return parseInt(value.match(/[+-]?\d+/g).join(''), 0);
      }
    }
  }
}
