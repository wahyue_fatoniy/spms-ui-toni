import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LocalCurrencyPipe } from './localCurrency/local-currency.pipe';
import { NumberInStringPipe } from './numberInString/number-in-string.pipe';
import { FilterDataPipe } from './filterData/filter-data.pipe';
import { FilterReplayPipe } from './filterReplay/filter-replay.pipe';
import { DateDurationPipe } from './dateDuration/date-duration.pipe';
import { FilterPercentagePipe } from './filter-percentage/filter-percentage.pipe';
import { TabsPipePipe } from './tabs-pipe/tabs-pipe.pipe';
import { XlsExportPipe } from './xlsExport/xls-export.pipe';
import { SortArrayPipe } from './sortArray/sort-array.pipe';

@NgModule({
  imports: [CommonModule],
  declarations: [
    LocalCurrencyPipe,
    NumberInStringPipe,
    FilterDataPipe,
    FilterReplayPipe,
    DateDurationPipe,
    FilterPercentagePipe,
    TabsPipePipe,
    XlsExportPipe,
    SortArrayPipe,
  ],
  exports: [LocalCurrencyPipe, NumberInStringPipe]
})
export class PipesModule {}
