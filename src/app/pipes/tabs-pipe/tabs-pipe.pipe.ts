import { Pipe, PipeTransform } from '@angular/core';

export interface TabsMenu {
  path: string;
  title: string;
  img?: string;
}

@Pipe({
  name: 'tabsPipe'
})
export class TabsPipePipe implements PipeTransform {

  transform(paths: string[], data?: TabsMenu[]): any {
    return data.filter((tab, i, arr) => {
      return paths.find(path => path === tab.path);
    });
  }

}
