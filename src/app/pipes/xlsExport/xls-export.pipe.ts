import { Pipe, PipeTransform } from '@angular/core';
import XlsExport from 'xlsexport/xls-export.js';

@Pipe({
  name: 'xlsExport'
})
export class XlsExportPipe implements PipeTransform {

  transform(title: string, data: Object[]): void {
    let xls = new XlsExport(data, title);
    xls.exportToXLS(title);
  }

}
