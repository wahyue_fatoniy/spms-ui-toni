import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FormPmAdminComponent } from './form-pm-admin.component';

describe('FormPmAdminComponent', () => {
  let component: FormPmAdminComponent;
  let fixture: ComponentFixture<FormPmAdminComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FormPmAdminComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FormPmAdminComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
