import { Component, OnInit, Inject } from '@angular/core';
import { FormControl, Validators } from '@angular/forms';
import { ProjectManager } from 'src/app/models/projectManager.model';
import { MAT_DIALOG_DATA } from '@angular/material';

export interface FormPmAdmin {
  startDate: Date;
  endDate: Date;
  pmID: number;
  pmData: ProjectManager[];
}

@Component({
  selector: 'app-form-pm-admin',
  templateUrl: './form-pm-admin.component.html',
  styleUrls: ['./form-pm-admin.component.css']
})
export class FormPmAdminComponent implements OnInit {
  startDate: FormControl;
  endDate: FormControl;
  pm: FormControl;
  projectManager: FormControl;
  pmData: ProjectManager[] = [];

  constructor(
    @Inject(MAT_DIALOG_DATA)
    public data: FormPmAdmin) {
    this.startDate = new FormControl(data.startDate, Validators.required);
    this.endDate = new FormControl(data.endDate, Validators.required);
    this.projectManager = new FormControl(data.pmID, Validators.required);
    this.pmData = data.pmData;
  }

  ngOnInit() { }

}
