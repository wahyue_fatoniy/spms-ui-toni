import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FormSummaryAdminComponent } from './form-summary-admin.component';

describe('FormSummaryAdminComponent', () => {
  let component: FormSummaryAdminComponent;
  let fixture: ComponentFixture<FormSummaryAdminComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FormSummaryAdminComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FormSummaryAdminComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
