import { Component, OnInit, Inject } from '@angular/core';
import { FormControl, Validators } from '@angular/forms';
import { MAT_DIALOG_DATA } from '@angular/material';

export interface FormSummaryAdmin {
  startDate: Date;
  endDate: Date;
}

@Component({
  selector: 'app-form-summary-admin',
  templateUrl: './form-summary-admin.component.html',
  styleUrls: ['./form-summary-admin.component.css']
})
export class FormSummaryAdminComponent {
  startDate: FormControl;
  endDate: FormControl;

  constructor(
    @Inject(MAT_DIALOG_DATA)
    public data: FormSummaryAdmin) {
    this.startDate = new FormControl(data.startDate, [Validators.required]);
    this.endDate = new FormControl(data.endDate, [Validators.required]);
  }

}
