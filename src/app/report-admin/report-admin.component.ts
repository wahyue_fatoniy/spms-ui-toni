import { Component, OnInit, OnDestroy } from '@angular/core';
import { TypeReport } from '../report/report.component';
import { Subject } from 'rxjs';
import { MatDialog } from '@angular/material';
import { AAAService } from '../shared/aaa/aaa.service';
import { FormSummaryAdminComponent, FormSummaryAdmin } from './form-summary-admin/form-summary-admin.component';
import { FormPmAdminComponent, FormPmAdmin } from './form-pm-admin/form-pm-admin.component';
import { DatePipe } from '@angular/common';
import { takeUntil } from 'rxjs/operators';
import { AdminService } from '../shared/models/admin/admin.service';
import { SiteWorkService } from '../shared/models/site-work/site-work.service';
import { Admin } from '../shared/models/admin/admin';
import { SiteWork } from '../shared/models/site-work/site-work';
import { ProjectManager } from '../shared/models/project-manager/project-manager';

@Component({
  selector: 'app-report-admin',
  templateUrl: './report-admin.component.html',
  styleUrls: ['./report-admin.component.css']
})
export class ReportAdminComponent implements OnInit, OnDestroy {
  public pmReport: TypeReport;
  public summaryReport: TypeReport;
  public pmData: ProjectManager[] = [];
  public destroy$: Subject<boolean> = new Subject<boolean>();
  private adminId: number = null;
  datePipe: DatePipe = new DatePipe('id');

  constructor(
    private aaa: AAAService,
    private adminService: AdminService,
    private siteWorkService: SiteWorkService,
    private matDialog: MatDialog
  ) {}

  ngOnInit() {
    this.adminService.findAll().subscribe(() => {
      this.getServiceData();
      this.reportInit();
    });
  }

  ngOnDestroy() {
    this.destroy$.next(true);
    this.destroy$.complete();
  }

  getAuthAdmins(): Admin[] {
    const admins =  this.adminService.getTables().filter(_set => {
        const region = _set.getRegion() ? _set.getRegion() : null;
        const id  = region ? region.getId() : null;
        return !this.aaa.isAuthorized("read:project:{province:$}:{job:$}") ||
        this.aaa.isAuthorized(`read:project:{province:${id}}:{job:${_set.getId()}}`);
    });
    console.log("Authority admin", admins);
    this.adminId = admins.length !== 0 ? admins[0].getId() : null;
    return admins;
  }

  private getServiceData(): void {
    this.siteWorkService.findByAdmins(this.getAuthAdmins())
    .pipe(takeUntil(this.destroy$))
    .subscribe((data: SiteWork[]) => {
      this.pmData = data.map((siteWork: SiteWork) => {
        return siteWork['projectManager'];
      })
      .filter((pm, i, ArrPm) => {
        return i === ArrPm.findIndex(uniquePm => uniquePm['id'] === pm['id']);
      });
    });
  }

  public reportInit(): void {
    let start = new Date();
    start.setMonth(start.getMonth() - 1);
    this.paramSummaryReport(start, new Date());
    this.paramPmReport(null, start, new Date());
  }

  public onSetPm(): void {
    const dialog = this.matDialog.open(FormPmAdminComponent, {
      width: '500px',
      data: {
        startDate: new Date(this.pmReport.parameter['start']),
        endDate: new Date(this.pmReport.parameter['end']),
        pmData: this.pmData,
        pmID: this.pmReport.parameter['pmId']
      },
      disableClose: true
    });
    dialog.afterClosed()
    .pipe(takeUntil(this.destroy$))
    .subscribe((val: FormPmAdmin) => {
      if (val) {
        this.paramPmReport(val.pmID, val.startDate, val.endDate);
      }
    });
  }

  private paramPmReport(pmId: number, start: Date, end: Date): void {
    this.pmReport = {
      report: 'admin_report',
      parameter: {
        adminId: this.adminId,
        pmId: pmId,
        start: this.datePipe.transform(start, 'yyyy-MM-dd'),
        end: this.datePipe.transform(end, 'yyyy-MM-dd')
      }
    };
  }

  public onSetSummary(): void {
    const dialog = this.matDialog.open(FormSummaryAdminComponent, {
      width: '500px',
      data: {
        startDate: new Date(this.summaryReport.parameter['start']),
        endDate: new Date(this.summaryReport.parameter['end'])
      },
      disableClose: true
    });
    dialog.afterClosed()
    .pipe(takeUntil(this.destroy$))
    .subscribe((val: FormSummaryAdmin) => {
      if (val) {
        this.paramSummaryReport(val.startDate, val.endDate);
      }
    });
  }

  private paramSummaryReport(start: Date, end: Date): void {
    this.summaryReport = {
      report: 'summary_admin',
      parameter: {
        adminId: this.adminId,
        start: this.datePipe.transform(start, 'yyyy-MM-dd'),
        end: this.datePipe.transform(end, 'yyyy-MM-dd')
      }
    };
  }
}
