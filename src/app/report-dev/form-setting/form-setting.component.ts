import { Component, OnInit, Inject } from "@angular/core";
import { FormControl, Validators } from "@angular/forms";
import { MAT_DIALOG_DATA } from "@angular/material";

export interface FormSummaryDev {
  startDate: Date;
  endDate: Date;
}

@Component({
  selector: "app-form-setting",
  templateUrl: "./form-setting.component.html",
  styles: ['']
})
export class FormSettingComponent implements OnInit {
  startDate: FormControl;
  endDate: FormControl;

  constructor(
    @Inject(MAT_DIALOG_DATA)
    public data: FormSummaryDev) {
    this.startDate = new FormControl(data.startDate, [Validators.required]);
    this.endDate = new FormControl(data.endDate, [Validators.required]);
  }

  ngOnInit() {}
}
