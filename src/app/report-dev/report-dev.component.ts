import { Component, OnInit } from '@angular/core';
import { TypeReport } from '../report/report.component';
import { MatDialog } from '@angular/material';
import { FormSettingComponent, FormSummaryDev } from './form-setting/form-setting.component';
import { DatePipe } from '@angular/common';

@Component({
  selector: 'app-report-dev',
  templateUrl: './report-dev.component.html',
  styles: ['']
})
export class ReportDevComponent implements OnInit {
  public summaryReport: TypeReport;
  public datePipe: DatePipe = new DatePipe('id');

  constructor(
    public dialog: MatDialog
  ) { }

  ngOnInit() {
    this.reportInit();
  }


  public reportInit(): void {
    let start = new Date();
    start.setMonth(start.getMonth() - 1);
    this.paramSummaryReport(start, new Date());
  }

  private paramSummaryReport(start: Date, end: Date): void {
    this.summaryReport = {
      report: 'developer_report',
      parameter: {
        start: this.datePipe.transform(start, 'yyyy-MM-dd'),
        end: this.datePipe.transform(end, 'yyyy-MM-dd')
      }
    };
  }

  public onSetting(): void {
    const dialog = this.dialog.open(FormSettingComponent, {
      width: '500px',
      data: {
        startDate: new Date(this.summaryReport.parameter['start']),
        endDate: new Date(this.summaryReport.parameter['end'])
      }
    });

    dialog.afterClosed().subscribe((date: FormSummaryDev) => {
      if (date) {
        console.log(date);
        this.paramSummaryReport(date.startDate, date.endDate);
      }
    });
  }
}
