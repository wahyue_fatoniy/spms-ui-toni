import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FormReportFinanceComponent } from './form-report-finance.component';

describe('FormReportFinanceComponent', () => {
  let component: FormReportFinanceComponent;
  let fixture: ComponentFixture<FormReportFinanceComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FormReportFinanceComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FormReportFinanceComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
