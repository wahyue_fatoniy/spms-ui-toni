import { Component, OnInit, Inject } from '@angular/core';
import { FormControl, Validators } from '@angular/forms';
import { MAT_DIALOG_DATA } from '@angular/material';

export interface FormSummaryFinance {
  startDate: Date;
  endDate: Date;
}

@Component({
  selector: 'app-form-report-finance',
  templateUrl: './form-report-finance.component.html',
  styleUrls: ['./form-report-finance.component.css']
})
export class FormReportFinanceComponent {
  startDate: FormControl;
  endDate: FormControl;

  constructor(
    @Inject(MAT_DIALOG_DATA)
    public data: FormSummaryFinance) {
    this.startDate = new FormControl(data.startDate, [Validators.required]);
    this.endDate = new FormControl(data.endDate, [Validators.required]);
  }

}
