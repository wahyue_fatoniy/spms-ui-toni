import { Component, OnInit, OnDestroy } from '@angular/core';
import { TypeReport } from '../report/report.component';
import { MatDialog } from '@angular/material';
import { FormReportFinanceComponent, FormSummaryFinance } from './form-report-finance/form-report-finance.component';
import { Subject } from 'rxjs';
import { DatePipe } from '@angular/common';


@Component({
  selector: 'app-report-finance',
  templateUrl: './report-finance.component.html',
  styleUrls: ['./report-finance.component.css']
})
export class ReportFinanceComponent implements OnInit, OnDestroy {
  public summaryReport: TypeReport;
  public destroy: Subject<boolean> = new Subject<boolean>();
  public datePipe: DatePipe = new DatePipe('id');

  constructor(public dialog: MatDialog) { }

  ngOnInit() {
    let start = new Date();
    start.setMonth(start.getMonth() - 1);
    this.paramSummaryReport(start, new Date());
  }

  ngOnDestroy() {
    this.destroy.next(true);
    this.destroy.complete();
  }

  public onSetReport(): void {
    const dialog = this.dialog.open(FormReportFinanceComponent, {
      width: '500px',
      data: {
        startDate: new Date(this.summaryReport.parameter['start']),
        endDate: new Date(this.summaryReport.parameter['end']),
      }
    });
    dialog.afterClosed().subscribe((val: FormSummaryFinance) => {
      if (val) {
        this.paramSummaryReport(val.startDate, val.endDate);
      }
    });
  }

  private paramSummaryReport(startDate: Date, endDate: Date): void {
    this.summaryReport = {
      report: 'finance_report',
      parameter: {
        start: this.datePipe.transform(startDate, 'yyyy-MM-dd'),
        end: this.datePipe.transform(endDate, 'yyyy-MM-dd')
      }
    };
  }

}
