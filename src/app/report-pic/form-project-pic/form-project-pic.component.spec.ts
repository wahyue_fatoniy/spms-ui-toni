import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FormProjectPicComponent } from './form-project-pic.component';

describe('FormProjectPicComponent', () => {
  let component: FormProjectPicComponent;
  let fixture: ComponentFixture<FormProjectPicComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FormProjectPicComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FormProjectPicComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
