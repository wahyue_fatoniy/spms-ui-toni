import { Component, OnInit, Inject } from '@angular/core';
import { FormControl, Validators } from '@angular/forms';
import { SiteWork } from '../../shared/models/site-work/site-work';
import { MAT_DIALOG_DATA } from '@angular/material';

export interface FormProjectPic {
  startDate: Date;
  endDate: Date;
  projectID: number;
  siteWorkData: SiteWork[];
}

@Component({
  selector: 'app-form-project-pic',
  templateUrl: './form-project-pic.component.html',
  styleUrls: ['./form-project-pic.component.css']
})
export class FormProjectPicComponent {
  startDate: FormControl;
  endDate: FormControl;
  pm: FormControl;
  siteWork: FormControl;
  siteWorkData: SiteWork[] = [];

  constructor(
    @Inject(MAT_DIALOG_DATA)
    public data: FormProjectPic) {
    this.startDate = new FormControl(data.startDate, Validators.required);
    this.endDate = new FormControl(data.endDate, Validators.required);
    this.siteWork = new FormControl(data.projectID, Validators.required);
    this.siteWorkData = data.siteWorkData;
  }
}
