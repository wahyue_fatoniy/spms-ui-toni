import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FormSummaryPicComponent } from './form-summary-pic.component';

describe('FormSummaryPicComponent', () => {
  let component: FormSummaryPicComponent;
  let fixture: ComponentFixture<FormSummaryPicComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FormSummaryPicComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FormSummaryPicComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
