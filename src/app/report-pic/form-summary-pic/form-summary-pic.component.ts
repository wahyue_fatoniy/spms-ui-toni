import { Component, OnInit, Inject } from '@angular/core';
import { FormControl, Validators } from '@angular/forms';
import { MAT_DIALOG_DATA } from '@angular/material';

export interface FormSummaryPic {
  startDate: Date;
  endDate: Date;
}

@Component({
  selector: 'app-form-summary-pic',
  templateUrl: './form-summary-pic.component.html',
  styleUrls: ['./form-summary-pic.component.css']
})
export class FormSummaryPicComponent {
  startDate: FormControl;
  endDate: FormControl;

  constructor(
    @Inject(MAT_DIALOG_DATA)
    public data: FormSummaryPic) {
    this.startDate = new FormControl(data.startDate, [Validators.required]);
    this.endDate = new FormControl(data.endDate, [Validators.required]);
  }

}
