import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ReportPicComponent } from './report-pic.component';

describe('ReportPicComponent', () => {
  let component: ReportPicComponent;
  let fixture: ComponentFixture<ReportPicComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ReportPicComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ReportPicComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
