import { Component, OnInit, OnDestroy } from '@angular/core';
import { TypeReport } from '../report/report.component';
import { combineLatest, Subscription, Subject } from 'rxjs';
import { Coordinator } from '../../models/coordinatorService/coordinator.model';
import { FormControl } from '@angular/forms';
import { MatDialog } from '@angular/material';
import { AAAService } from '../shared/aaa/aaa.service';
import { DatePipe } from '@angular/common';
import { FormSummaryPicComponent, FormSummaryPic } from './form-summary-pic/form-summary-pic.component';
import { FormProjectPicComponent, FormProjectPic } from './form-project-pic/form-project-pic.component';
import { Cordinator } from '../shared/models/cordinator/cordinator';
import { CordinatorService } from '../shared/models/cordinator/cordinator.service';
import { SiteWorkService } from '../shared/models/site-work/site-work.service';
import { SiteWork } from '../shared/models/site-work/site-work';
import { takeUntil } from 'rxjs/operators';

@Component({
  selector: 'app-report-pic',
  templateUrl: './report-pic.component.html',
  styleUrls: ['./report-pic.component.css']
})
export class ReportPicComponent implements OnInit, OnDestroy {
  public siteWorkReport: TypeReport;
  public summaryReport: TypeReport;
  private picId: number = null;
  public siteWorkData: SiteWork[] = [];
  public siteWorkControl: FormControl;
  public datePipe: DatePipe =  new DatePipe('id');
  public destroy: Subject<boolean> = new Subject<boolean>();

  constructor(
    private aaa: AAAService,
    private picService: CordinatorService,
    private siteWorkService: SiteWorkService,
    private matDialog: MatDialog
  ) {}

  ngOnInit() {
    this.picService.findAll()
    .pipe(takeUntil(this.destroy))
    .subscribe(() => {
      this.getServiceData();
      this.reportInit();
    });
  }

  ngOnDestroy() {
    this.destroy.next(true);
    this.destroy.complete();
  }

  getAuthCordinator(): Cordinator[] {
    const cordinators = this.picService.getTables().filter(_set => {
       const region = _set.getRegion() ? _set.getRegion() : null;
       const id = region ? region.getId() : null;
       return this.aaa.isAuthorized(`read:project:{province:${id}}:{job:${_set.getId()}}`)
       || !this.aaa.isAuthorized('read:project:{province:$}:{job:$}');
    });
    console.log('Cordinators Authoriity', cordinators);
    this.picId = cordinators.length !== 0 ? cordinators[0]['id'] : null;
    return cordinators;
  }

  private getServiceData(): void {
    this.siteWorkService.findByCordinators(this.getAuthCordinator())
    .pipe(takeUntil(this.destroy))
    .subscribe((val) => {
     this.siteWorkData = val;
    });
  }

  public reportInit(): void {
    let start = new Date();
    start.setMonth(start.getMonth() - 1);
    this.paramSummaryReport(start, new Date());
    this.paramProjectReport(null, start, new Date());
  }

  public onSetProject(): void {
    const dialog = this.matDialog.open(FormProjectPicComponent, {
      width: '500px',
      disableClose: true,
      data: {
        startDate: new Date(this.siteWorkReport.parameter['start']),
        endDate: new Date(this.siteWorkReport.parameter['end']),
        siteWorkData: this.siteWorkData,
        projectID: this.siteWorkReport.parameter['projectId']
      },
    });

    dialog.afterClosed().subscribe((val: FormProjectPic) => {
      if (val) {
        this.paramProjectReport(val.projectID, val.startDate, val.endDate);
      }
    });
  }

  private paramProjectReport(projectId: number, startDate: Date, endDate: Date): void {
    this.siteWorkReport = {
      report: 'pic_report',
      parameter: {
        picId: this.picId,
        projectId: projectId,
        start: this.datePipe.transform(startDate, 'yyyy-MM-dd'),
        end: this.datePipe.transform(endDate, 'yyyy-MM-dd')
      }
    };
  }

  public onSetSummary(): void {
    const dialog = this.matDialog.open(FormSummaryPicComponent, {
      width: '500px',
      disableClose: true,
      data: {
        startDate: new Date(this.summaryReport.parameter['start']),
        endDate: new Date(this.summaryReport.parameter['end'])
      }
    });

    dialog.afterClosed().subscribe((val: FormSummaryPic) => {
      if (val) {
        this.paramSummaryReport(val.startDate, val.endDate);
      }
    });
  }

  private paramSummaryReport(startDate: Date, endDate: Date): void {
    this.summaryReport = {
      report: 'pic_report_summary',
      parameter: {
        picId: this.picId,
        start: this.datePipe.transform(startDate, 'yyyy-MM-dd'),
        end: this.datePipe.transform(endDate, 'yyyy-MM-dd')
      }
    };
  }
}
