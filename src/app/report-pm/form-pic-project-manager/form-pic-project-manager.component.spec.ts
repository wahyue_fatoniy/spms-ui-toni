import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FormPicProjectManagerComponent } from './form-pic-project-manager.component';

describe('FormPicProjectManagerComponent', () => {
  let component: FormPicProjectManagerComponent;
  let fixture: ComponentFixture<FormPicProjectManagerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FormPicProjectManagerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FormPicProjectManagerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
