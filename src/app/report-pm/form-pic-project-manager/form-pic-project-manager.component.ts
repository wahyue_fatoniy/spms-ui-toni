import { Component, OnInit, Inject } from '@angular/core';
import { Coordinator } from 'src/app/models/coordinator.model';
import { FormControl, Validators } from '@angular/forms';
import { ProjectManager } from '../../shared/models/project-manager/project-manager';
import { MAT_DIALOG_DATA } from '@angular/material';

export interface FormPicPm {
  startDate: Date;
  endDate: Date;
  picID: number;
  picData: Coordinator[];
}

@Component({
  selector: 'app-form-pic-project-manager',
  templateUrl: './form-pic-project-manager.component.html',
  styleUrls: ['./form-pic-project-manager.component.css']
})
export class FormPicProjectManagerComponent implements OnInit {
  startDate: FormControl;
  endDate: FormControl;
  pm: FormControl;
  coordinator: FormControl;
  picData: Coordinator[] = [];

  constructor(
    @Inject(MAT_DIALOG_DATA)
    public data: FormPicPm) {
    this.startDate = new FormControl(data.startDate, Validators.required);
    this.endDate = new FormControl(data.endDate, Validators.required);
    this.coordinator = new FormControl(data.picID, Validators.required);
    this.picData = data.picData;
  }
  ngOnInit() {}
}
