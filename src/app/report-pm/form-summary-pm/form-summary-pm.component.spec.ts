import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FormSummaryPmComponent } from './form-summary-pm.component';

describe('FormSummaryPmComponent', () => {
  let component: FormSummaryPmComponent;
  let fixture: ComponentFixture<FormSummaryPmComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FormSummaryPmComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FormSummaryPmComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
