import { Component, OnInit, Inject } from '@angular/core';
import { FormControl, Validators } from '@angular/forms';
import { MAT_DIALOG_DATA } from '@angular/material';

export interface FormSummaryPm {
  startDate: Date;
  endDate: Date;
}

@Component({
  selector: 'app-form-summary-pm',
  templateUrl: './form-summary-pm.component.html',
  styleUrls: ['./form-summary-pm.component.css']
})
export class FormSummaryPmComponent implements OnInit {
  startDate: FormControl;
  endDate: FormControl;

  constructor(
    @Inject(MAT_DIALOG_DATA)
    public data: FormSummaryPm) {
    this.startDate = new FormControl(data.startDate, [Validators.required]);
    this.endDate = new FormControl(data.endDate, [Validators.required]);
  }

  ngOnInit() {
  }

}
