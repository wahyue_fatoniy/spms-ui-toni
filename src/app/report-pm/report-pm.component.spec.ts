import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ReportPmComponent } from './report-pm.component';

describe('ReportPmComponent', () => {
  let component: ReportPmComponent;
  let fixture: ComponentFixture<ReportPmComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ReportPmComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ReportPmComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
