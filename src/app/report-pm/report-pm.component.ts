import { Component, OnInit, OnDestroy } from '@angular/core';
import { TypeReport } from '../report/report.component';
import { Subscription, Subject } from 'rxjs';
import { MatDialog } from '@angular/material';
import { AAAService } from '../shared/aaa/aaa.service';
import { DatePipe } from '@angular/common';
import { FormSummaryPmComponent, FormSummaryPm } from './form-summary-pm/form-summary-pm.component';
import { FormPicProjectManagerComponent, FormPicPm } from './form-pic-project-manager/form-pic-project-manager.component';
import { SiteWorkService } from 'src/service/siteWork-service/site-work.service';
import { SiteWork } from '../models/siteWork.model';
import { takeUntil } from 'rxjs/operators';
import { ProjectManagerService } from 'src/service/projectManager-service/project-manager.service';
import { ProjectManager } from '../models/projectManager.model';

@Component({
  selector: 'app-report-pm',
  templateUrl: './report-pm.component.html',
  styleUrls: ['./report-pm.component.css']
})
export class ReportPmComponent implements OnInit, OnDestroy {
  public picReport: TypeReport;
  public summaryReport: TypeReport;
  private pmId: number = null;
  public picData: Object[] = [];
  public subscriptions: Subscription;
  public datePipe: DatePipe = new DatePipe('id');
  public destroy: Subject<boolean> = new Subject<boolean>();

  constructor(
    private aaa: AAAService,
    private pmService: ProjectManagerService,
    private projectService: SiteWorkService,
    private matDialog: MatDialog
  ) {}

  ngOnInit() {
    this.getSubscriptionData();
    this.getSubscriptionPm();
    this.getPmService();
    this.reportInit();
  }

  ngOnDestroy() {
    this.destroy.next(true);
    this.destroy.complete();
  }


  public getAuthPMs(projectManagers: ProjectManager[]): ProjectManager[] {
    const pms = projectManagers.filter(_set => {
      const region = _set.getRegion ? _set.getRegion : null;
      const id = region ? region.getId : null;
      return !this.aaa.isAuthorized('read:project:{province:$}:{job:$}') ||
      this.aaa.isAuthorized(`read:project:{province:${id}}:{job:${_set.getId}}`);
    });
    console.log('Project Manager Authority', pms);
    this.pmId = pms.length !== 0 ? pms[0]['id'] : null;
    return pms;
  }

  getPmService() {
    this.pmService.getData()
    .pipe(takeUntil(this.destroy))
    .subscribe((val) => {
    });
  }

  getSubscriptionPm() {
    this.pmService.subscriptionData().subscribe(val => {
      this.projectService.findByPMs(this.getAuthPMs(val))
      .pipe(takeUntil(this.destroy))
      .subscribe();
    });
  }

  private getSubscriptionData(): void {
    this.projectService.subscriptionData()
    .pipe(takeUntil(this.destroy))
    .subscribe((data: SiteWork[]) => {
      this.picData = data.map((siteWork: SiteWork) => {
          return siteWork.getTeam.getCoordinator;
      }).filter((pic, i, ArrPic) => {
        if (pic) {
          return i === ArrPic.findIndex(uniquePic => uniquePic['id'] === pic['id']);
        }
      });
    });
  }

  public reportInit(): void {
    let start = new Date();
    start.setMonth(start.getMonth() - 1);
    this.paramSummaryReport(start, new Date());
    this.paramPicReport(null, start, new Date());
  }

  public onSetPic(): void {
    const dialog = this.matDialog.open(FormPicProjectManagerComponent, {
      width: '500px',
      disableClose: true,
      data: {
        startDate: new Date(this.picReport.parameter['start']),
        endDate: new Date(this.picReport.parameter['end']),
        picID: this.picReport.parameter['picId'],
        picData: this.picData
      }
    });
    dialog.afterClosed().subscribe((val: FormPicPm) => {
      if (val) {
        this.paramPicReport(val.picID, val.startDate, val.endDate);
      }
    });
  }

  private paramPicReport(picId: number, startDate: Date, endDate: Date): void {
    this.picReport = {
      report: 'pm_report',
      parameter: {
        pmId: this.pmId,
        picId: picId,
        start: this.datePipe.transform(startDate, 'yyyy-MM-dd'),
        end: this.datePipe.transform(endDate, 'yyyy-MM-dd')
      }
    };
  }

  public onSetSummary(): void {
    const dialog = this.matDialog.open(FormSummaryPmComponent, {
      width: '500px',
      disableClose: true,
      data: {
        startDate: new Date(this.summaryReport.parameter['start']),
        endDate: new Date(this.summaryReport.parameter['end'])
      }
    });
    dialog.afterClosed().subscribe((val: FormSummaryPm) => {
      if (val) {
        this.paramSummaryReport(val.startDate, val.endDate);
      }
    });
  }

  private paramSummaryReport(startDate: Date, endDate: Date): void {
    this.summaryReport = {
      report: 'pm_report_summary',
      parameter: {
        pmId: this.pmId,
        start: this.datePipe.transform(startDate, 'yyyy-MM-dd'),
        end: this.datePipe.transform(endDate, 'yyyy-MM-dd')
      }
    };
  }
}
