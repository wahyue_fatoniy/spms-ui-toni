import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormSummaryPmComponent } from './form-summary-pm/form-summary-pm.component';

@NgModule({
  imports: [
    CommonModule
  ],
  declarations: [
    // FormSummaryPmComponent
  ],
})
export class ReportPmModule { }
