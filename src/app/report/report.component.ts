import { Component, OnInit, Input, OnChanges } from '@angular/core';
import { DomSanitizer, SafeResourceUrl } from '@angular/platform-browser';
import { HttpClient } from '@angular/common/http';

export interface TypeReport {
  report: string;
  parameter?: Object;
}

@Component({
  selector: 'app-report',
  templateUrl: './report.component.html',
  styleUrls: ['./report.component.css']
})
export class ReportComponent implements OnInit, OnChanges {
  public resourceUrl: SafeResourceUrl;
  public loading = false;
  @Input()
  report: TypeReport;

  constructor(public domSanitizer: DomSanitizer, public http: HttpClient) {}

  ngOnInit() { }

  ngOnChanges() {
    if (this.report) {
      this.generateReport();
    }
  }

  private generateReport(): void {
    this.loading = true;
    const loginUrl = `http://spms-satunol.ddns.net:3015/jasperserver/rest/login?j_username=jasperadmin&j_password=jasperadmin`;
    const reportUrl = `http://spms-satunol.ddns.net:3015/jasperserver/rest_v2/reports/Report/spms_report/${this.report.report}.html?` +
    this.setReportParam();

    this.http.get(loginUrl, { withCredentials: true }).subscribe(result => {
      this.http
        .get(reportUrl, { responseType: 'blob', withCredentials: true })
        .subscribe(blobFile => {
          const objectURL = window.URL.createObjectURL(blobFile);
          this.resourceUrl = this.domSanitizer.bypassSecurityTrustResourceUrl(
            objectURL
          );
          this.loading = false;
        }, error => this.loading = false );
    });
  }

  private setReportParam(): string {
      let keysParam = Object.keys(this.report.parameter);
      let parameter = '';
      for (let i of keysParam) {
        if (keysParam.length > 1) {
          parameter = parameter + `${i}=${this.report.parameter[i]}&`;
        } else {
          parameter = `${i}=${this.report.parameter[i]}`;
        }
      }
      if (keysParam.length > 1) {
        parameter = parameter.slice(0, -1);
      }
      console.log(parameter);
      return parameter;
  }
}
