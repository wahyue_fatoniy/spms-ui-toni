import { NgModule } from "@angular/core";
import { PagePersonComponent } from "./page-person/page-person.component";
import { RouterModule, Routes, Router } from "@angular/router";
import { PersonFormComponent } from "./page-person/person-form/person-form.component";
import { PersonTableComponent } from "./page-person/person-table/person-table.component";
import { MaterialModule } from "./material.module";
import { FormsModule, FormBuilder, ReactiveFormsModule } from "@angular/forms";
import { FlexLayoutModule } from "@angular/flex-layout";
import { Daterangepicker } from "ng2-daterangepicker";
import { NgxDaterangepickerMd } from "ngx-daterangepicker-material";
import { NgxMatDrpModule } from "ngx-mat-daterange-picker";
import { NgxMatSelectSearchModule } from "ngx-mat-select-search";
import { NgxChartsModule } from "@swimlane/ngx-charts";

import { FormDialogRemoveComponent } from "./template/form-dialog-remove/form-dialog-remove.component";
import { PageOrganizationComponent } from "./page-organization/page-organization.component";
import { PageRegionComponent } from "./page-region/page-region.component";
import { PageAreaComponent } from "./page-area/page-area.component";
import { AreaTableComponent } from "./page-area/area-table/area-table.component";
import { AreaFormComponent } from "./page-area/area-form/area-form.component";
import { RegionTableComponent } from "./page-region/region-table/region-table.component";
import { RegionFormComponent } from "./page-region/region-form/region-form.component";
import { PipesModule } from "./pipes/pipes.module";
import { FormTableComponent } from "./template/form-table/form-table.component";
import { DragulaModule, DragulaService } from "ng2-dragula";
import { RoleCellComponent } from "./shared/cell/role-cell/role-cell.component";
import { SowCellComponent } from "./shared/cell/sow-cell/sow-cell.component";
import { UserFormComponent } from "./page-person/user-form/user-form.component";
import { ReportComponent } from "./report/report.component";
import { ReportPicComponent } from "./report-pic/report-pic.component";
import { ReportPmComponent } from "./report-pm/report-pm.component";
import { ReportAdminComponent } from "./report-admin/report-admin.component";
import { ReportDevComponent } from "./report-dev/report-dev.component";
import { AuthGuard } from "./shared/aaa/auth.guard";
import { LoginV2Component } from "./login-v2/login-v2.component";
import { HomeComponent } from "./home/home.component";
import { SiteV2Component } from "./site-v2/site-v2.component";
import { ToastrModule } from "ngx-toastr";
import { FormSettingComponent } from "./report-dev/form-setting/form-setting.component";
import { FormSummaryAdminComponent } from "./report-admin/form-summary-admin/form-summary-admin.component";
import { FormPmAdminComponent } from "./report-admin/form-pm-admin/form-pm-admin.component";
import { FormPicProjectManagerComponent } from "./report-pm/form-pic-project-manager/form-pic-project-manager.component";
import { FormSummaryPicComponent } from "./report-pic/form-summary-pic/form-summary-pic.component";
import { FormProjectPicComponent } from "./report-pic/form-project-pic/form-project-pic.component";
import { FormReportFinanceComponent } from "./report-finance/form-report-finance/form-report-finance.component";
import { PageRoleComponent } from "./page-role/page-role.component";
import { PageAdminComponent } from "./page-role/page-admin/page-admin.component";
import { PageCustomerComponent } from "./page-role/page-customer/page-customer.component";
import { PageSalesComponent } from "./page-role/page-sales/page-sales.component";
import { PagePartnerComponent } from "./page-role/page-partner/page-partner.component";
import { PageEngineerComponent } from "./page-role/page-engineer/page-engineer.component";
import { PageTechnicianComponent } from "./page-role/page-technician/page-technician.component";
import { PageDocumentationComponent } from "./page-role/page-documentation/page-documentation.component";
import { PageCoordinatorComponent } from "./page-role/page-coordinator/page-coordinator.component";
import { PageProjectManagerComponent } from "./page-role/page-project-manager/page-project-manager.component";
import { PageFinanceComponent } from "./page-role/page-finance/page-finance.component";
import { PageSubcontComponent } from "./page-role/page-subcont/page-subcont.component";
import { TableOrganizationComponent } from "./page-organization/table-organization/table-organization.component";
import { SiteFormV2Component } from "./site-v2/site-form-v2/site-form-v2.component";
import { FormOrganizationComponent } from "./page-organization/form-organization/form-organization.component";
import { AdminProjectTableComponent } from "./dashboard-admin-center/containers/admin-project-table/admin-project-table.component";
import { ShowDescSowComponent } from "./dashboard-pm/containers/pm-alarm-table/show-desc-sow/show-desc-sow.component";
import { ShowDescTaskComponent } from "./dashboard-pm/containers/pm-alarm-table/show-desc-task/show-desc-task.component";
import { AngularFontAwesomeModule } from "angular-font-awesome";
import { PageAdminCenterComponent } from "./page-role/page-admin-center/page-admin-center.component";
import { PageGeneralManagerComponent } from "./page-role/page-general-manager/page-general-manager.component";
import { DocumentLifeCycleComponent } from "./components/form-list/table-document/document-life-cycle/document-life-cycle.component";
import { FormChangeStatusComponent } from "./components/form-list/table-document/form-change-status/form-change-status.component";
import { FormDescSeverityComponent } from "./dashboard-pic/containers/pic-task/form-desc-severity/form-desc-severity.component";
import { DescDocumentLifeCycleComponent } from "./components/form-list/table-document/document-life-cycle/desc-document-life-cycle/desc-document-life-cycle.component";
import { FormDocumentComponent } from "./components/form-list/table-document/form-document/form-document.component";
import { FormUserOrganizationComponent } from "./page-organization/form-user-organization/form-user-organization.component";
import { CommonModule } from "@angular/common";
import { DetailPlannComponent } from "./dashboard-pm/containers/pm-siteWork-table/detail-plann/detail-plann.component";
import { PmDescTaskComponent } from "./dashboard-pm/containers/pm-task-table/pm-desc-task/pm-desc-task.component";
import { ReportFinanceComponent } from "./report-finance/report-finance.component";
import { SiteWorkListComponent } from "./dashboard-finance/containers/finance-purchase-order/site-work-list/site-work-list.component";
import { PurchaseOrderFormComponent } from "./dashboard-finance/containers/finance-purchase-order/purchase-order-form/purchase-order-form.component";
import { DetailProjectComponent } from "./dashboard-gm/containers/detail-project/detail-project.component";
import { ReportAlarmComponent } from "./dashboard-pic/containers/pic-task/report-alarm/report-alarm.component";
import { TaskExecutionListComponent } from "./dashboard-pic/containers/pic-daily-report/task-execution-list/task-execution-list.component";
import { PicDailyReportComponent } from "./dashboard-pic/containers/pic-daily-report/pic-daily-report.component";
import { PicDocumentComponent } from "./dashboard-pic/containers/pic-document/pic-document.component";
import { PicLifeCycleDocumentComponent } from "./dashboard-pic/containers/pic-life-cycle-document/pic-life-cycle-document.component";
import { TaskListComponent } from "./dashboard-pic/containers/pic-planning/task-list/task-list.component";
import { TeamListComponent } from "./dashboard-pic/containers/pic-planning/team-list/team-list.component";
import { PicPlanningComponent } from "./dashboard-pic/containers/pic-planning/pic-planning.component";
import { PicTaskComponent } from "./dashboard-pic/containers/pic-task/pic-task.component";
import { SharedModule } from "./shared/shared.module";
import { FormSummaryPmComponent } from "./report-pm/form-summary-pm/form-summary-pm.component";
import { PicProjectComponent } from "./dashboard-pic/containers/pic-project/pic-project.component";

const appRouter: Routes = [
  { path: "login", component: LoginV2Component },
  {
    path: "home",
    component: HomeComponent,
    canActivate: [AuthGuard],
    children: [
      { path: "person", component: PagePersonComponent },
      { path: "organization", component: PageOrganizationComponent },
      { path: "site", component: SiteV2Component },
      {
        path: "role",
        component: PageRoleComponent
      },

      {
        path: "region",
        component: PageRegionComponent
      },
      {
        path: "area",
        component: PageAreaComponent
      },
      {
        path: "admin",
        component: PageAdminComponent
      },
      {
        path: "customer",
        component: PageCustomerComponent
      },
      {
        path: "sales",
        component: PageSalesComponent
      },
      {
        path: "partner",
        component: PagePartnerComponent
      },
      {
        path: "engineer",
        component: PageEngineerComponent
      },
      {
        path: "technician",
        component: PageTechnicianComponent
      },
      {
        path: "documentation",
        component: PageDocumentationComponent
      },
      {
        path: "coordinator",
        component: PageCoordinatorComponent
      },
      {
        path: "subcont",
        component: PageSubcontComponent
      },
      {
        path: "projectManager",
        component: PageProjectManagerComponent
      },
      {
        path: "dashboard-admin",
        loadChildren: './dashboard-admin/dashboard-admin.module#DashboardAdminModule',
      },
      {
        path: "dashboard-pic",
        loadChildren: './dashboard-pic/dashboard-pic.module#DashboardPicModule'
      },
      {
        path: "dashboard-pm",
        loadChildren: './dashboard-pm/dashboard-pm.module#DashboardPmModule'
      },
      {
        path: "dashboard-finance",
        loadChildren: './dashboard-finance/dashboard-finance.module#DashboardFinanceModule'
      },
      {
        path: "dashboard-admin-center",
        loadChildren: './dashboard-admin-center/dashboard-admin-center.module#DashboardAdminCenterModule'
      },
      {
        path: "dashboard-gm",
        loadChildren: './dashboard-gm/dashboard-gm.module#DashboardGmModule'
      },
      {
        path: "report-pic",
        component: ReportPicComponent
      },
      {
        path: "report-pm",
        component: ReportPmComponent
      },
      {
        path: "report-admin",
        component: ReportAdminComponent
      },
      {
        path: "report-finance",
        component: ReportFinanceComponent
      },
      {
        path: "report-developer",
        component: ReportDevComponent
      }
    ]
  }
];

@NgModule({
  declarations: [
    PagePersonComponent,
    PersonFormComponent,
    PersonTableComponent,
    FormDialogRemoveComponent,
    PageOrganizationComponent,
    TableOrganizationComponent,
    PageAdminComponent,
    PageCustomerComponent,
    PageSalesComponent,
    PagePartnerComponent,
    PageRegionComponent,
    PageAreaComponent,
    AreaTableComponent,
    AreaFormComponent,
    RegionTableComponent,
    RegionFormComponent,
    PageEngineerComponent,
    PageTechnicianComponent,
    PageDocumentationComponent,
    PageCoordinatorComponent,
    PageSubcontComponent,
    FormTableComponent,
    RoleCellComponent,
    SowCellComponent,
    PageProjectManagerComponent,
    PageFinanceComponent,
    UserFormComponent,
    ReportComponent,
    ReportPicComponent,
    ReportPmComponent,
    ReportAdminComponent,
    ReportFinanceComponent,
    ReportDevComponent,
    FormSettingComponent,
    FormSummaryAdminComponent,
    FormPmAdminComponent,
    FormPicProjectManagerComponent,
    FormSummaryPicComponent,
    FormProjectPicComponent,
    FormReportFinanceComponent,
    PageRoleComponent,
    SiteV2Component,
    SiteFormV2Component,
    FormOrganizationComponent,
    DetailPlannComponent,
    AdminProjectTableComponent,
    ShowDescSowComponent,
    ShowDescTaskComponent,
    PageAdminCenterComponent,
    PageGeneralManagerComponent,
    DocumentLifeCycleComponent,
    FormChangeStatusComponent,
    FormDescSeverityComponent,
    DescDocumentLifeCycleComponent,
    FormDocumentComponent,
    PmDescTaskComponent,
    FormUserOrganizationComponent,
    SiteWorkListComponent,
    PurchaseOrderFormComponent,
    DetailProjectComponent,
    ReportAlarmComponent,
    TaskExecutionListComponent,
    PicDailyReportComponent,
    PicDocumentComponent,
    PicLifeCycleDocumentComponent,
    TaskListComponent,
    TeamListComponent,
    PicPlanningComponent,
    PicProjectComponent,
    PicTaskComponent,
    FormSummaryPmComponent
  ],
  imports: [
    CommonModule,
    RouterModule.forRoot(appRouter, { useHash: true }),
    MaterialModule,
    FormsModule,
    ReactiveFormsModule,
    FlexLayoutModule,
    PipesModule,
    Daterangepicker,
    NgxDaterangepickerMd,
    DragulaModule.forRoot(),
    NgxMatDrpModule,
    NgxMatSelectSearchModule,
    ToastrModule.forRoot(),
    AngularFontAwesomeModule,
    NgxChartsModule,
    SharedModule
  ],
  exports: [
    RouterModule,
    FormsModule,
    RoleCellComponent,
    SowCellComponent,
    NgxDaterangepickerMd,
    NgxMatSelectSearchModule,
    MaterialModule,
  ],
  entryComponents: [
    PersonFormComponent,
    FormDialogRemoveComponent,
    AreaFormComponent,
    RegionFormComponent,
    FormTableComponent,
    UserFormComponent,
    FormSettingComponent,
    FormSummaryAdminComponent,
    FormPmAdminComponent,
    FormPicProjectManagerComponent,
    FormSummaryPicComponent,
    FormProjectPicComponent,
    FormReportFinanceComponent,
    PageFinanceComponent,
    SiteFormV2Component,
    FormOrganizationComponent,
    DetailPlannComponent,
    ShowDescSowComponent,
    ShowDescTaskComponent,
    PageAdminCenterComponent,
    PageGeneralManagerComponent,
    FormChangeStatusComponent,
    FormDescSeverityComponent,
    DescDocumentLifeCycleComponent,
    FormDocumentComponent,
    PmDescTaskComponent,
    FormUserOrganizationComponent,
  ],
  providers: [FormBuilder, DragulaService]
})
export class RoutersModule {
  constructor(public router: Router) {
    this.router.errorHandler = (error: any) => {
      console.log(error);
    };
  }
}
