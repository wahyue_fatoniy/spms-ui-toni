import { Injectable } from "@angular/core";
import { AAA } from "./aaa";
import { UserAuth } from "./user/user";
import { Router } from "@angular/router";
import { UserAuthService } from './user/user.service';
import { UserService } from '../models/user/user.service';

@Injectable({
  providedIn: "root"
})
export class AAAService implements AAA {
  constructor(
    private user: UserAuthService,
    private userModel: UserService,
    private router: Router
  ) {}

  getLoading(): boolean {
    return this.user.getLoading();
  }

  getError() {
    return this.user.getError() ? this.user.getError() : null;
  }

  clearError() {
    this.user.clearError();
    console.log(this.getError());
  }

  // private setUserMetadata(roles: Role[]) {
  //   const
  // }

  authenticate(username: string, password: string) {
    localStorage.removeItem('user');
    localStorage.removeItem('expired');
    this.user.login(username, password).subscribe(_token => {
      this.user.getProfile(_token.getAccessToken()).subscribe(_profile => {
        this.user.getToken().subscribe(_token2 => {
          this.user
            .getUserById(_profile.getId(), _token2.getAccessToken())
            .subscribe(_user => {
              this.userModel.findRolesByEmail(_user.getEmail()).subscribe(_userModel => {
                  console.log(_userModel);
                  _user.setUserRoles(_userModel);
                  localStorage.setItem('user', JSON.stringify(_user));
                  const expired = new Date();
                  expired.setHours(expired.getHours() + 12);
                  localStorage.setItem('expired', expired.toString());
                  this.router.config = this.getCurrentUser().setRouterRedirect(this.router.config);
                  this.router.navigate([this.getCurrentUser().getRedirect()]);
              });
            });
        });
      });
    });
  }

  logout() {
    this.router.navigate(["/login"]);
    localStorage.removeItem('user');
    localStorage.removeItem('expired');
  }

  private isDynamic(scope: string, key: string): boolean {
    return scope.indexOf(key) !== -1;
  }

  getAuthorization(): string[] {
    const user = this.getCurrentUser();
    let scopes = user ? user.getPermissions() : [];
    for (const scope of scopes) {
      if (this.isDynamic(scope, '{province:$}') && this.isDynamic(scope, '{job:$}')) {
          for (const role of user.getUserRoles()) {
            const province = role['region'] ? role['region'].id : null;
            scopes.push(scope.split('{province:$}:{job:$}')
            .join(`{province:${province ? province : '*'}}:{job:${role.getId()}}`));
          }
      }
      if (this.isDynamic(scope, '{province:$}')) {
        for (const role of user.getUserRoles()) {
          const province = role['region'] ? role['region'].id : null;
          scopes.push(scope.split(`{province:$}`).join(`{province:${province ? province : '*'}}`));
        }
      }
      if (this.isDynamic(scope, '{job:$}') && !this.isDynamic(scope, '{province:$}')) {
          for (const role of user.getUserRoles()) {
            scopes.push(scope.split('{job:$}').join(`{job:${role.getId()}}`));
          }
      }
    }
    return scopes;
  }

  getCurrentUser(): UserAuth {
    const user = localStorage.getItem('user');
    return user ? new UserAuth(JSON.parse(user)) : new UserAuth();
  }

  isAuthenticated(): boolean {
    const expired = localStorage.getItem('expired');
    const date = new Date(expired);
    const now = new Date();
    return expired ? now.getTime() < date.getTime() : false;
  }

  isAuthorized(permission: string): boolean {
    const index = this.getAuthorization().findIndex(
      _permission => _permission === permission
    );
    return index !== -1;
  }

}
