import { UserAuth } from './user/user';
import { Observable } from 'rxjs';

export class AAAData {
  constructor(input?: AAAData) {
    Object.assign(this, input);
  }
  username?: string;
  password?: string;
  connection?: string;
  email: string;
  roles: string[];
  person: UserAuth;
}


export interface AAA {
  authenticate(username: string, password: string);
  logout();
  getAuthorization(): string[];
  getCurrentUser(): UserAuth;
  isAuthenticated(): boolean;
  isAuthorized(key: string): boolean;
}
