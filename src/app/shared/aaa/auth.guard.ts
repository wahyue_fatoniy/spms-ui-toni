import { Injectable } from '@angular/core';
import { CanActivate, Router } from '@angular/router';
import { AAAService } from './aaa.service';


@Injectable({
  providedIn: 'root'
})
export class AuthGuard implements CanActivate {
  constructor(private aaa: AAAService, public router: Router) {}
  canActivate(): boolean {
    const auth = this.aaa.isAuthenticated();
    let valid = true;
    if (!auth) {
      console.log('got to login');
      this.router.navigate(["login"]);
      valid = false;
    }
    return valid;
  }
}
