import { Injectable } from '@angular/core';
import { CoreServiceConfig, CoreService } from '../../core-service';
import { Permission } from './permission';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { environment } from 'src/environments/environment';
import { map, catchError } from 'rxjs/operators';
const CONFIG: CoreServiceConfig = {};

@Injectable({
  providedIn: 'root'
})
export class PermissionService extends CoreService<Permission> {

  constructor(http: HttpClient) {
    super(CONFIG, http);
  }

  findPermissions(token: string): Observable<Permission[]> {
    this.setLoading(true);
    return this.http.get(`${environment.authUrl}/permissions`, {
      headers: new HttpHeaders().set("Authorization", `Bearer ${token}`)
    })
    .pipe(
      map((_permissions: any) => {
          const permissions = _permissions.permissions.map(_set => new Permission(_set));
          this.setLoading(false);
          this.setTables(permissions);
          console.log(permissions);
          return permissions;
      }),
      catchError(_error => {
        this.setLoading(false);
        this.setStatusError(this.setError(_error));
        return throwError(this.getError());
      })
    );
  }

  createPermission(value: Permission, token: string): Observable<Permission> {
    value.setApplicationId('TPbF5PKI20F1yTLMle0eqy4ck2UzFGej');
    value.setType('client');
    this.setLoading(true);
    return this.http.post(`${environment.authUrl}/permissions`, value, {
      headers : new HttpHeaders().set("Authorization", `Bearer ${token}`)
    }).pipe(
      map((_result: any) => {
          const permission = new Permission(_result);
          this.setLoading(false);
          let permissions = this.getTables();
          permissions.unshift(permission);
          this.setTables(permissions);
          console.log(permission);
          return permission;
      }),
      catchError(_error => {
        this.setLoading(false);
        this.setStatusError(this.setError(_error));
        return throwError(this.getError());
      })
    );
  }

  editPermission(id: string, value: Permission, token: string): Observable<Permission> {
    this.setLoading(true);
    value.setId(undefined);
    return this.http.put(`${environment.authUrl}/permissions/${id}`, value, {
      headers : new HttpHeaders().set("Authorization", `Bearer ${token}`)
    }).pipe(
      map((_result: any) => {
          const permission = new Permission(_result);
          this.setLoading(false);
          let permissions = this.getTables();
          permissions = permissions.map(_set => _set.getId() === id ? value : _set);
          this.setTables(permissions);
          console.log(permission);
          return permission;
      }),
      catchError(_error => {
        this.setLoading(false);
        this.setStatusError(this.setError(_error));
        return throwError(this.getError());
      })
    );
  }

  deletePermission(id: string, token: string): Observable<string> {
    this.setLoading(true);
    return this.http.delete(`${environment.authUrl}/permissions/${id}`, {
      headers : new HttpHeaders().set("Authorization", `Bearer ${token}`)
    }).pipe(
      map((_result: any) => {
        this.setLoading(false);
        let permissions = this.getTables();
        permissions = permissions.filter(_set => _set.getId() !== id);
        this.setTables(permissions);
        return id;
      }),
      catchError(_error => {
        this.setLoading(false);
        this.setStatusError(this.setError(_error));
        return throwError(this.getError());
      })
    );
  }

  getPermissionById(id: string): Permission {
    const temp = this.getTables();
    const index = temp.findIndex(_set => _set.getId() === id);
    return index !== -1 ? temp[index] : null;
  }

}
