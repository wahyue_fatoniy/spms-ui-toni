export class Permission {
  constructor(input?: Permission) {
    Object.assign(this, input);
  }

  private applicationType: string;
  private applicationId: string;
  private description: string;
  private name: string;
  private _id: string;

  getType(): string {
    return this.applicationType;
  }

  setType(applicationType: string) {
    this.applicationType = applicationType;
  }

  getApplicationId(): string {
    return this.applicationId;
  }

  setApplicationId(applicationId: string) {
    this.applicationId = applicationId;
  }

  getDescription(): string {
    return this.description;
  }

  setDescription(description: string) {
    this.description = description;
  }

  getName(): string {
    return this.name;
  }

  setName(name: string) {
    this.name = name;
  }

  getId(): string {
    return this._id;
  }

  setId(id: string) {
    this._id = id;
  }

}
