import { Injectable } from "@angular/core";
import { CoreService, CoreServiceConfig } from "../../core-service";
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { RoleAuth } from "./role";
import { Observable, throwError, of } from "rxjs";
import { environment } from "src/environments/environment";
import { map, catchError } from "rxjs/operators";
const CONFIG: CoreServiceConfig = {};

@Injectable({
  providedIn: "root"
})
export class RoleAuthService extends CoreService<RoleAuth> {
  constructor(http: HttpClient) {
    super(CONFIG, http);
  }

  findRoles(token: string): Observable<RoleAuth[]> {
    this.setLoading(true);
    return this.http
      .get(`${environment.authUrl}/roles`, {
        headers: new HttpHeaders().set("Authorization", `Bearer ${token}`)
      })
      .pipe(
        map((_permissions: any) => {
          const roles = _permissions.roles.map(_set => new RoleAuth(_set));
          this.setLoading(false);
          this.setTables(roles);
          console.log(roles);
          return roles;
        }),
        catchError(_error => {
          this.setLoading(false);
          this.setStatusError(this.setError(_error));
          return throwError(this.getError());
        })
      );
  }

  createRole(value: RoleAuth, token: string): Observable<RoleAuth> {
    value.setApplicationId("TPbF5PKI20F1yTLMle0eqy4ck2UzFGej");
    value.setType("client");
    this.setLoading(true);
    return this.http
      .post(`${environment.authUrl}/roles`, value, {
        headers: new HttpHeaders().set("Authorization", `Bearer ${token}`)
      })
      .pipe(
        map((_result: any) => {
          const role = new RoleAuth(_result);
          this.setLoading(false);
          let roles = this.getTables();
          roles.unshift(role);
          this.setTables(roles);
          console.log(role);
          return role;
        }),
        catchError(_error => {
          this.setLoading(false);
          this.setStatusError(this.setError(_error));
          return throwError(this.getError());
        })
      );
  }

  editRole(id: string, value: RoleAuth, token: string): Observable<RoleAuth> {
    this.setLoading(true);
    value.setId(undefined);
    return this.http
      .put(`${environment.authUrl}/roles/${id}`, value, {
        headers: new HttpHeaders().set("Authorization", `Bearer ${token}`)
      })
      .pipe(
        map((_result: any) => {
          const role = new RoleAuth(_result);
          this.setLoading(false);
          let roles = this.getTables();
          roles = roles.map(_set => (_set.getId() === id ? value : _set));
          this.setTables(roles);
          console.log(role);
          return role;
        }),
        catchError(_error => {
          this.setLoading(false);
          this.setStatusError(this.setError(_error));
          return throwError(this.getError());
        })
      );
  }

  deleteRole(id: string, token: string): Observable<string> {
    this.setLoading(true);
    return this.http
      .delete(`${environment.authUrl}/roles/${id}`, {
        headers: new HttpHeaders().set("Authorization", `Bearer ${token}`)
      })
      .pipe(
        map((_result: any) => {
          this.setLoading(false);
          let roles = this.getTables();
          roles = roles.filter(_set => _set.getId() !== id);
          this.setTables(roles);
          return id;
        }),
        catchError(_error => {
          this.setLoading(false);
          this.setStatusError(this.setError(_error));
          return throwError(this.getError());
        })
      );
  }

  findRolesByUserId(id: string, token: string) {
    this.setLoading(true);
    if (id) {
      return this.http.get(`${environment.authUrl}/users/${id}/roles`, {
        headers: new HttpHeaders().set("Authorization", `Bearer ${token}`)
      })
      .pipe(
        map((_roles: RoleAuth[]) => {
          const roles = _roles.map(_set => new RoleAuth(_set));
          this.setLoading(false);
          console.log(roles);
          return roles;
        }),
        catchError(_error => {
          this.setLoading(false);
          this.setStatusError(this.setError(_error));
          return throwError(this.getError());
        })
      );
    } else {
      this.setLoading(false);
      return of([]);
    }
  }

  getRoleByName(name: string): RoleAuth {
    const temp = this.getTables();
    const index = temp.findIndex(_set => _set.getName() === name);
    return index !== -1 ? temp[index] : null;
  }

  getIdsByRoleName(roles: string[]): string[] {
    let ids = [];
    for (const role of roles) {
      const set = this.getRoleByName(role);
      if (set) {
        ids.push(set.getId());
      }
    }
    return ids;
  }

  getRoleSpms(): RoleAuth[] {
    let roles = this.getTables()
    .filter(_set => {
      return _set.getName().indexOf("-spms") !== -1;
    });
    return roles;
  }

}
