import { Permission } from '../permission/permission';

export class RoleAuth extends Permission {

  private permissions: string[];
  private users: string[];

  getPermissions(): string[] {
    return this.permissions;
  }

  setPermissions(permissions: string[]) {
    this.permissions = permissions;
  }

  getUsers(): string[] {
    return this.users;
  }

  setUsers(users: string[]) {
    this.users = users;
  }

}
