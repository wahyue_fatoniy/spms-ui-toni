export class ProfileUser {

  constructor(input?: ProfileUser) {
    Object.assign(this, input);
    if (input && input['https://example.com/roles']) {
      this.roles = input['https://example.com/roles'];
    }
  }

  private sub: string;
  private nickname: string;
  private name: string;
  private profile: string;
  private update_at: string;
  private roles: string;

  getId(): string {
    return this.sub;
  }

  getUsername(): string {
    return this.nickname;
  }

  getEmail(): string {
    return this.name;
  }

  getProfile(): string {
    return this.profile;
  }

  getUpdateAt(): Date {
    return new Date(this.update_at);
  }

  getRoles(): string {
    return this.roles;
  }

}
