export class TokenUser {
  constructor(input?: TokenUser) {
    Object.assign(this, input);
  }

  private access_token: string;
  private id_token: string;
  private scope: string;
  private expires_in: string;
  private token_type: string;

  getAccessToken(): string {
    return this.access_token;
  }

  getIdToken(): string {
    return this.id_token;
  }

  getScopes(): string {
    return this.scope;
  }

  getExpiresIn(): Date {
    return new Date(this.expires_in);
  }

  getTokenType(): string {
    return this.token_type;
  }

}
