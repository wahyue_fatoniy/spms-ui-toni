import { Injectable } from "@angular/core";
import { HttpClient, HttpHeaders, HttpRequest } from "@angular/common/http";
import { Observable, throwError, forkJoin, of } from "rxjs";
import { TokenUser } from "./token-user";
import { environment } from "src/environments/environment";
import { map, catchError, mergeMap } from "rxjs/operators";
import { CoreService, CoreServiceConfig } from "../../core-service";
import { UserAuth } from "./user";
import { ProfileUser } from "./profile-user";
import { UserService } from '../../models/user/user.service';
const CONFIG: CoreServiceConfig = {};
export enum ConnectionType {
  FB = "facebook",
  Google = "google-oauth2"
}


@Injectable({
  providedIn: "root"
})
export class UserAuthService extends CoreService<UserAuth> {
  constructor(http: HttpClient, private user: UserService) {
    super(CONFIG, http);
  }

  login(username: string, password: string): Observable<TokenUser> {
    this.setLoading(true);
    return this.http
      .post(`https://ngx-ems.auth0.com/oauth/token`, {
        grant_type: "http://auth0.com/oauth/grant-type/password-realm",
        username: username,
        password: password,
        client_id: "0yA4RowMRy9YIVbFlcBQU06Ky6uA7UDr",
        realm: "SpmsDB"
      })
      .pipe(
        map((_token: TokenUser) => {
          const token = new TokenUser(_token);
          this.setLoading(false);
          console.log(token);
          return token;
        }),
        catchError(_error => {
          this.setLoading(false);
          this.setStatusError(this.setError(_error));
          return throwError(this.getError());
        })
      );
  }

  getProfile(token: string): Observable<ProfileUser> {
    this.setLoading(true);
    return this.http
      .get(`https://ngx-ems.auth0.com/userinfo`, {
        headers: new HttpHeaders().set("Authorization", `Bearer ${token}`)
      })
      .pipe(
        map((_profile: ProfileUser) => {
          const profile = new ProfileUser(_profile);
          this.setLoading(false);
          console.log(profile);
          return profile;
        }),
        catchError(_error => {
          this.setLoading(false);
          this.setStatusError(this.setError(_error));
          return throwError(this.getError());
        })
      );
  }

  getToken(): Observable<TokenUser> {
    this.setLoading(true);
    return this.http
      .post(`https://ngx-ems.auth0.com/oauth/token`, {
        client_id: "7oCb2l0C04OSOd0FMhevJfjctbxnusuG",
        client_secret:
          "BCZwRA0Lg8WtZyY8ZEGGWkp3VOE5OJgyeDqQwkce9zeb7jqFWmeAbQFdxHwj6BY9",
        audience: "urn:auth0-authz-api",
        grant_type: "client_credentials"
      })
      .pipe(
        map((_token: TokenUser) => {
          const token = new TokenUser(_token);
          this.setLoading(false);
          console.log(token);
          return token;
        }),
        catchError(_error => {
          this.setLoading(false);
          this.setStatusError(this.setError(_error));
          return throwError(this.getError());
        })
      );
  }

  getTokenV2(): Observable<TokenUser> {
    this.setLoading(true);
    return this.http
      .post(`https://ngx-ems.auth0.com/oauth/token`, {
        client_id: "7oCb2l0C04OSOd0FMhevJfjctbxnusuG",
        client_secret:
          "BCZwRA0Lg8WtZyY8ZEGGWkp3VOE5OJgyeDqQwkce9zeb7jqFWmeAbQFdxHwj6BY9",
        audience: "https://ngx-ems.auth0.com/api/v2/",
        grant_type: "client_credentials"
      })
      .pipe(
        map((_token: TokenUser) => {
          const token = new TokenUser(_token);
          this.setLoading(false);
          console.log(token);
          return token;
        }),
        catchError(_error => {
          this.setLoading(false);
          this.setStatusError(this.setError(_error));
          return throwError(this.getError());
        })
      );
  }

  getUserById(id: string, token: string): Observable<UserAuth> {
    this.setLoading(true);
    return this.http
      .get(`${environment.authUrl}/users/${id}`, {
        headers: new HttpHeaders().set("Authorization", `Bearer ${token}`)
      })
      .pipe(
        map((_user: UserAuth) => {
          const user = new UserAuth(_user);
          this.setLoading(false);
          console.log(user);
          return user;
        }),
        catchError(_error => {
          this.setLoading(false);
          this.setStatusError(this.setError(_error));
          return throwError(this.getError());
        })
      );
  }

  findUsers(token: string): Observable<UserAuth[]> {
    this.setLoading(true);
    return this.http
      .get(`${environment.authUrl}/users`, {
        headers: new HttpHeaders().set("Authorization", `Bearer ${token}`)
      })
      .pipe(
        mergeMap((_users: any) => {
          let services = [];
          for (const user of _users.users) {
            if (user.identities[0].connection === "AMS") {
              services = [...services, this.getUserById(user.user_id, token)];
            }
          }
          return forkJoin(...services);
        }),
        map(_set => {
          this.setLoading(false);
          let users = [].concat.apply([], _set);
          users = users.map(_user => new UserAuth(_user));
          this.setTables(users);
          console.log(users);
          return users;
        }),
        catchError(_error => {
          this.setLoading(false);
          this.setStatusError(this.setError(_error));
          return throwError(this.getError());
        })
      );
  }

  findByEmail(email: string): Observable<UserAuth> {
    this.setLoading(true);
    return this.getTokenV2()
    .pipe(
      mergeMap(_token => {
        return forkJoin(
          this.http.get(`https://ngx-ems.auth0.com/api/v2/users-by-email?email=${encodeURIComponent(email)}`, {
            headers: new HttpHeaders().set(
              "Authorization",
              `Bearer ${_token.getAccessToken()}`
            )
          }),
          this.user.findRolesByEmail(email)
        );
      }),
      map((_set: any) => {
        this.setLoading(false);
        const _user = _set[0];
        const _roles = _set[1];
        const user = _user.length === 0 ? new UserAuth() : new UserAuth(_user[0]);
        user.setUserRoles(_roles);
        console.log(user);
        return user;
      }),
      catchError(_error => {
        this.setLoading(false);
        this.setStatusError(this.setError(_error));
        return throwError(this.getError());
      })
    );
  }

  createUser(value: UserAuth): Observable<UserAuth> {
    this.setLoading(true);
    return this.getTokenV2().pipe(
      mergeMap(_token => {
        return this.http.post(
          "https://ngx-ems.auth0.com/api/v2/users",
          {
            connection: "SpmsDB",
            email: value.getEmail(),
            password: value.getPassword(),
            username: value.getUsername(),
            user_metadata: value.getUserMetadata()
          },
          {
            headers: new HttpHeaders().set(
              "Authorization",
              `Bearer ${_token.getAccessToken()}`
            )
          }
        );
      }),
      map((_user: UserAuth) => {
        this.setLoading(false);
        const user = new UserAuth(_user);
        const users = this.getTables();
        users.unshift(user);
        this.setTables(users);
        console.log(user);
        return user;
      }),
      catchError(_error => {
        this.setLoading(false);
        this.setStatusError(this.setError(_error));
        return throwError(this.getError());
      })
    );
  }

  updateUser(value: UserAuth): Observable<UserAuth> {
    this.setLoading(true);
    return this.getTokenV2().pipe(
      mergeMap(_token => {
        return this.http.patch(
          `https://ngx-ems.auth0.com/api/v2/users/${value.getId()}`,
          {
            connection: "SpmsDB",
            user_metadata: value.getUserMetadata()
          },
          {
            headers: new HttpHeaders().set(
              "Authorization",
              `Bearer ${_token.getAccessToken()}`
            )
          }
        );
      }),
      map((_user: UserAuth) => {
        this.setLoading(false);
        const user = new UserAuth(_user);
        let users = this.getTables();
        users = users.map(_set =>
          _set.getId() === user.getId() ? user : _set
        );
        this.setTables(users);
        console.log(user);
        return user;
      }),
      catchError(_error => {
        this.setLoading(false);
        this.setStatusError(this.setError(_error));
        return throwError(this.getError());
      })
    );
  }

  addRoles(value: UserAuth, oldRoles: string[], newRoles: string[], token: string) {
    let adds = newRoles.filter(_new => {
      const index = oldRoles.findIndex(_old => _old === _new);
      return index === -1;
    });
    let removes = oldRoles.filter(_old => {
      const index = newRoles.findIndex(_new => _new === _old);
      return index === -1;
    });
    const req = new HttpRequest("DELETE", `${environment.authUrl}/users/${value.getId()}/roles`, {
      headers : new HttpHeaders().set("Authorization", `Bearer ${token}`)
    });
    const newReq = req.clone({body: removes});
    let services = [];
    if (adds.length !== 0) {
      services.push(
        this.http.patch(`${environment.authUrl}/users/${value.getId()}/roles`, adds, {
          headers: new HttpHeaders().set("Authorization", `Bearer ${token}`)
        })
      );
    }
    if (removes.length !== 0) {
      services.push(this.http.request(newReq));
    }
    this.setLoading(true);
    if (services.length !== 0) {
      return forkJoin(...services).pipe(
        mergeMap(_set => this.getUserById(value.getId(), token)),
        map(_user => {
          this.setLoading(false);
          const user = new UserAuth(_user);
          let users = this.getTables();
          users = users.map(_set => _set.getId() === user.getId() ? user : _set);
          this.setTables(users);
          return user;
        }),
        catchError(_error => {
          this.setLoading(false);
          this.setStatusError(this.setError(_error));
          return throwError(this.getError());
        })
      );
    } else {
      this.setLoading(false);
      return of(null);
    }
  }

  deleteUser(id: string): Observable<string> {
    this.setLoading(true);
    return this.getTokenV2()
    .pipe(
      mergeMap(_token => {
        return this.http.delete(`https://ngx-ems.auth0.com/api/v2/users/${id}`, {
          headers: new HttpHeaders().set("Authorization", `Bearer ${_token.getAccessToken()}`)
        });
      }),
      map(_user => {
        this.setLoading(false);
        let users = this.getTables();
        users = users.filter(_set => _set.getId() !== id);
        this.setTables(users);
        return id;
      }),
      catchError(_error => {
        this.setLoading(false);
        this.setStatusError(this.setError(_error));
        return throwError(this.getError());
      })
    );
  }

  loginBySocialMedia(type: ConnectionType) {
    // tslint:disable-next-line:max-line-length
    const headers = new HttpHeaders();
    headers.set( "Access-Control-Allow-Credentials" , "true");
    headers.set("Access-Control-Allow-Origin", "*");
    // tslint:disable-next-line:max-line-length
    const url = `https://ngx-ems.auth0.com/authorize?response_type=code&client_id=TPbF5PKI20F1yTLMle0eqy4ck2UzFGej&connection=${type}&redirect_uri=http://localhost:4200/#/public/map`;
    return this.http.get(url, {headers: headers});
  }


}
