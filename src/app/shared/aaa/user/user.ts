import { Routes } from '@angular/router';
import { Role, RoleType } from '../../models/role/role';
import { Region } from '../../models/region/region';
import { User } from '../../models/user/user';

export class UserMetadata {
  name: string;
  roles?: Role[];
}

export class Authorization  {
  constructor(input?: Authorization) {
    Object.assign(this, input);
  }

  groups: string[];
  roles: string[];
  permissions: string[];
}

export class AppMetadata {
  authorization: Authorization;
}

export class UserAuth {
  constructor(input?: UserAuth) {
    Object.assign(this, input);
    if (input && !input.app_metadata) {
      this.app_metadata = new AppMetadata();
    }
  }

  private email: string;
  private username: string;
  private password: string;
  private connection: string;
  private email_verified: boolean;
  private update_at: string;
  private picture: string;
  private user_id: string;
  private nickname: string;
  private identities: Array<any>;
  private created_at: string;
  private user_metadata: UserMetadata;
  private app_metadata:  AppMetadata;
  private last_ip: string;
  private last_login: string;
  private logins_count: number;

  private findPermissions(permission: string): boolean {
    return this.getPermissions().findIndex(_permission => _permission === permission) !== -1;
  }

  getRedirect(): string {
    let redirect = '';
    if (this.findPermissions('read:reference') && !this.findPermissions('read:site:{province:$}')) {
      redirect = "/home/person";
    } else if (this.findPermissions('read:dashboard-admin')) {
      redirect = '/home/dashboard-admin';
    } else if (this.findPermissions('read:dashboard-pm')) {
      redirect = '/home/dashboard-pm';
    } else if (this.findPermissions('read:dashboard-pic')) {
      redirect = '/home/dashboard-pic';
    } else if (this.findPermissions('read:dashboard-finance')) {
      redirect = '/home/dashboard-finance';
    } else if (this.findPermissions('read:dashboard-admin-center')) {
      redirect = '/home/dashboard-admin-center';
    } else if (this.findPermissions('read:dashboard-gm')) {
      redirect = '/home/dashboard-gm';
    }
    return redirect;
  }

  setRouterRedirect(config: Routes): Routes {
    if (config.findIndex(_set => _set.path === "") === -1) {
      config.unshift({path: '', redirectTo: "home", pathMatch: 'full'});
    }
    const link = this.getRedirect();
    const links = link.split("/");
    const indexHome = config.findIndex(_set => _set.path === "home");
    if (config[indexHome].children.findIndex(_set => _set.path === "") === -1) {
      config[indexHome].children.unshift({
        path : "", redirectTo: links[2], pathMatch: "full"
      });
    }
    return config;
  }


  getPermissions(): string[] {
    return this.app_metadata && this.app_metadata.authorization ? this.app_metadata.authorization.permissions : [];
  }

  getRoles(): string[] {
    return this.app_metadata && this.app_metadata.authorization ? this.app_metadata.authorization.roles : [];
  }

  setRoles(roles: string[]) {
    if (this.app_metadata) {
      if (!this.app_metadata.authorization) {
        this.app_metadata = new AppMetadata();
        this.app_metadata.authorization = new Authorization({groups: [], permissions: [], roles: []});
      }
      this.app_metadata.authorization.roles = roles;
    }
  }

  getUserRoles(): Role[] {
    return this.user_metadata && this.user_metadata.roles ? this.user_metadata.roles.map(_role => new Role(_role)) : [];
  }

  getRegion(roleType: RoleType): Region {
    const roles = Object.assign([], this.user_metadata.roles);
    const index = roles
    .map(_role => new Role(_role))
    .findIndex(_role => _role.getRoleType() === roleType);
    return index !== -1 ? new Region(roles[index]['region']) : null;
  }

  setUserRoles(roles: Role[]) {
    if (!this.user_metadata) {
      this.user_metadata = new UserMetadata();
    }
    this.user_metadata.roles = roles;
  }

  getUserDetail(): User {
    const roles = this.getUserRoles();
    const index = roles.findIndex(_set => _set.getRoleType() === RoleType.User);
    return index !== -1 ? new User(JSON.parse(JSON.stringify(roles[index]))) : null;
  }

  getEmail(): string {
    return this.email;
  }

  setEmail(email: string ) {
    this.email = email;
  }

  getUsername(): string {
    return this.username;
  }

  setUsername(username: string) {
    this.username = username;
  }

  getPicture(): string {
    return this.picture;
  }

  getId(): string {
    return this.user_id;
  }

  getName(): string {
    return this.user_metadata ? this.user_metadata.name : null;
  }

  setName(name: string) {
    if (!this.user_metadata) {
      this.user_metadata = new UserMetadata();
    }
    this.user_metadata.name = name;
  }

  getPassword(): string {
    return this.password;
  }

  setPassword(password: string) {
    this.password = password;
  }

  setConnection(connection: string) {
    this.connection = connection;
  }

  getUserMetadata(): UserMetadata {
    return this.user_metadata;
  }



}
