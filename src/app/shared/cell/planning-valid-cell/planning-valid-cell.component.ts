import { Component, OnInit, Input } from '@angular/core';
import { ValidType } from '../../models/weekly-planning/weekly-planning';



@Component({
  selector: 'app-planning-valid-cell',
  template: `
    <div *ngIf="status">
      <img src="/assets/img/collection/{{url[status]}}.png"/>&nbsp;
      <span [style.color]="colors[status]">{{ status }}</span>
    </div>
  `,
  styles: []
})
export class PlanningValidCellComponent implements OnInit {

  constructor() { }
  @Input() status: ValidType;
  url = {
    'Belum Berlaku' : 'information', 'Masih Berlaku' : 'NORMAL', 'Tidak Berlaku' : 'CANCEL'
  };
  colors = {
     'Masih Berlaku' : '#5cb85c', 'Tidak Berlaku': '#d9534f', 'Belum Berlaku' : '#5bc0de'
  };

  ngOnInit() {
  }

}
