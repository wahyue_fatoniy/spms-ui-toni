import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-progress-cell',
  template: `
    <div style="padding-right:20px;">
      <mat-grid-list cols="3" rowHeight="65px;">
          <mat-grid-tile colspan="2">
              <mat-progress-bar mode="determinate" [value]="progress"></mat-progress-bar>
              &nbsp;&nbsp;<span style="font-size: 12px;">{{progress}}%</span>
              </mat-grid-tile>
      </mat-grid-list>
    </div>
  `,
  styles: []
})
export class ProgressCellComponent implements OnInit {
  constructor() {}
  @Input()
  progress: number;

  ngOnInit() {}
}


// <mat-grid-tile colspan="1">
// <img src="/assets/img/collection/progress.png"/>&nbsp;{{progress}}%&nbsp;&nbsp;
// </mat-grid-tile>