import { Component, OnInit, Input } from '@angular/core';
import { RoleType } from '../../models/role/role';

@Component({
  selector: 'app-role-cell',
  template: `
    <img src="/assets/img/collection/{{role}}.png"/>&nbsp;
    {{ label[role] ? label[role] : '' }}
  `,
  styles: []
})
export class RoleCellComponent implements OnInit {

  constructor() { }
  @Input() role;
  label = {
    DOCUMENTATION: 'Documentation', ENGINEER: 'Engineer', TECHNICIAN: 'Technician', CORDINATOR: 'Cordinator'
  };
  ngOnInit() {
  }

}
