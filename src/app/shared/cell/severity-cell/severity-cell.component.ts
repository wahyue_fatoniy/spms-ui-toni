import { Component, OnInit, Input } from '@angular/core';
import { SeverityType } from '../../models/task/task';

@Component({
  selector: 'app-severity-cell',
  template: `
    <div [style.color]="colors[severity]">
      <img matTooltip="{{ label[severity] ? label[severity] : '' }}" src="/assets/img/collection/{{severity}}.png"/>&nbsp;
    </div>
  `,
  styles: []
})
export class SeverityCellComponent implements OnInit {

  constructor() { }
  @Input() severity: SeverityType;
  colors = {
    NONE : '#ababab',  NORMAL: '#5cb85c', CRITICAL_I: '#d9534f', CRITICAL_E: '#d9534f',
    MAJOR_I: '#f0ad4e', MAJOR_E: '#f0ad4e'
  };
  label = {
    NONE : 'None',  NORMAL: 'Normal', CRITICAL_I: 'Critical I', CRITICAL_E: 'Critical E',
    MAJOR_I: 'Major I', MAJOR_E: 'Major E'
  };

  ngOnInit() {
  }

}
