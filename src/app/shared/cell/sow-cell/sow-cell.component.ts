import { Component, OnInit, Input } from '@angular/core';
import { SowType } from '../../models/sow-type/sow-type';

@Component({
  selector: 'app-sow-cell',
  template: `
    <button mat-button [matMenuTriggerFor]="menu">
        <img src="/assets/img/collection/sow.png"/>&nbsp;
        {{ sow.getSowCode() }}
    </button>
    <mat-menu #menu="matMenu">
      <button mat-menu-item disabled>
        <img src="/assets/img/collection/description.png"/>&nbsp;
        {{ sow.getDescription() }}
      </button>
      <button mat-menu-item disabled>
        <img src="/assets/img/collection/price.png"/>&nbsp;
        {{ sow.getPrice() | localCurrency }}
      </button>
      <button mat-menu-item disabled>
        <img src="/assets/img/collection/area.png"/>&nbsp;
        {{ sow.getRegion().getName() }}
      </button>
    </mat-menu>
  `,
  styles: []
})
export class SowCellComponent implements OnInit {

  constructor() { }
  @Input() sow: SowType;

  ngOnInit() {
  }

}
