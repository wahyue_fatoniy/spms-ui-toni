import { Component, OnInit, Input } from '@angular/core';
import { StatusType } from '../../models/site-work/site-work';

@Component({
  selector: 'app-status-cell',
  template: `
    <div *ngIf="status" [style.color]="colors[status]">
      <img matTooltip="{{label[status] ? label[status] : ''}}" src="/assets/img/collection/{{status}}.png"/>&nbsp;
    </div>
  `,
  styles: []
})
// {{ label[status] ? label[status] : '' }}
export class StatusCellComponent implements OnInit {

  constructor() { }
  @Input() status: StatusType;
  colors = {
    NOT_STARTED : '#ababab', ON_PROGRESS : '#5bc0de', DONE: '#5cb85c', CANCEL: '#d9534f'
  };
  label = {
    NOT_STARTED : 'Not Started', ON_PROGRESS : 'On Progress', DONE: 'Done', CANCEL: 'Cancel', NOTSTARTED: 'N/A', 
  };

  ngOnInit() { }


}
