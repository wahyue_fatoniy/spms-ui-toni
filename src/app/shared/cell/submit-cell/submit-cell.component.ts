import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-submit-cell',
  template: `
    <img src="/assets/img/collection/DONE.png" *ngIf="submit"/>
    <img src="/assets/img/collection/CANCEL.png" *ngIf="!submit"/>
  `,
  styles: ['']
})
export class SubmitCellComponent implements OnInit {

  constructor() { }
  @Input() submit: boolean;
  ngOnInit() {
  }

}
