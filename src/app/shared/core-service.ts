import { Observable, throwError, of, forkJoin } from 'rxjs';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { map, catchError } from 'rxjs/operators';
import { DatePipe } from '@angular/common';

export interface CoreServiceConfig {
  url?: string;
  getOptions?: string;
  setRow?: (input?: any) => any;
}

interface StatusError {
  name: string;
  message: string;
}

export class CoreService<T> {

  constructor(private config: CoreServiceConfig, public http: HttpClient) {}

  private tables: T[] = [];
  private loading: boolean;
  private statusError: StatusError;
  format = new DatePipe('id');
  utcFormat = 'yyyy-MM-ddTHH:mm:ss';


  setDate(date: string | Date): string {
    return this.format.transform(date, 'yyyy-MM-ddTHH:mm:ssZ', '+0000');
  }

  clearError() {
    this.statusError = undefined;
  }

  getLoading(): boolean {
    return this.loading;
  }

  setLoading(loading: boolean) {
    this.loading = loading;
  }

  getError(): StatusError {
    return this.statusError;
  }

  setStatusError(statusError: StatusError) {
    this.statusError = statusError;
  }

  setTables(data: T[]) {
    this.tables = Object.assign([], data);
  }

  getTables(): T[] {
    return Object.assign([], this.tables);
  }

  pushTables(value: T) {
    const index = this.tables.findIndex(result => result['id'] === value['id']);
    if (index === -1) {
      this.tables = [value, ...this.tables];
    }
  }

  deleteTables(id) {
    this.tables = this.tables.filter(_value => _value['id'] !== id);
  }

  editTables(id, value: T) {
    this.tables = this.tables.map(_value => _value['id'] === id ? value : _value);
  }

  getById(id: number): T {
    const index = this.tables.findIndex(row => row['id'] === id);
    return this.tables[index];
  }


  findAll(): Observable<T[]> {
    this.loading = true;
    const options = this.config.getOptions ? this.config.getOptions : '';
    return this.http.get(this.config.url + options).pipe(
      map((result: any) => {
        this.loading = false;
        this.tables = result.map(obj => this.config.setRow(obj));
        console.log(`find all URL= ${this.config.url}${options}`);
        console.log(this.tables);
        return result;
      }),
      catchError(error => {
        this.loading = false;
        this.statusError = this.setError(error);
        return throwError(this.getError());
      })
    );
  }

  findById(id: number): Observable<T> {
    const options = this.config.getOptions ? this.config.getOptions : '';
    return this.http.get(this.config.url + `/${id}` + options)
    .pipe(
      map((result: any) => result ? this.config.setRow(result) : null),
      catchError(error => {
        this.statusError = this.setError(error);
        return throwError(this.getError());
      })
    );
  }

  findByIds(ids: number[]): Observable<T[]> {
    this.setLoading(true);
    if (ids.length === 0) {
      this.setLoading(false);
      console.log('list of id undefined');
      return of([]);
    } else {
      let services = [];
      services = ids.map(id => this.findById(id));
// tslint:disable-next-line: deprecation
      return forkJoin(...services).pipe(
        map((result: any) => {
          this.loading = false;
          console.log(`find all list if ${ids}`);
          // tslint:disable-next-line:prefer-const
          let data = [].concat.apply(result);
          this.tables = data.filter(obj => obj !== null).map(obj => this.config.setRow(obj));
          console.log(this.tables);
          return this.tables;
        }),
        catchError(error => {
          this.loading = false;
          this.statusError = this.setError(error);
          return throwError(this.getError());
        })
      );
    }
  }

  create(value: T): Observable<T> {
    this.loading = false;
    return this.http.post(this.config.url, value)
    .pipe(
      map((result: any) => {
        console.log('ON_CREATE', result);
        this.loading = false;
        const temp = this.config.setRow(result);
        this.pushTables(temp);
        console.log(`create URL= ${this.config.url}, value= ${value}`, value);
        console.log(this.tables);
        return temp;
      }),
      catchError(error => {
        this.loading = false;
        this.statusError = this.setError(error);
        return throwError(this.getError());
      })
    );
  }

  update(id: number, value: T): Observable<T> {
    this.loading = false;
    return this.http.put(`${this.config.url}/${id}`, value)
    .pipe(
      map((result: any) => {
        this.loading = false;
        console.log(result);
        this.tables = this.tables.map(obj => obj['id'] === id ? this.config.setRow(result) : obj);
        console.log(`update URL= ${this.config.url}/${id}, Value= ${value}, ID=${id}`, result);
        console.log(this.tables);
        return value;
      }),
      catchError(error => {
        this.loading = false;
        this.statusError = this.setError(error);
        return throwError(this.getError());
      })
    );
  }

  updateSpesific(id: number, value: any): Observable<T> {
    this.loading = false;
    return this.http.post(`${this.config.url}/update?where=${encodeURIComponent(JSON.stringify({id: id}))}`, value)
    .pipe(
      map((result: any) => {
        this.loading = false;
        this.tables = this.tables.map(obj => obj['id'] === id ? value : obj);
        console.log(`update URL= ${this.config.url}/${id}, Value= ${value}, ID=${id}`);
        console.log(this.tables);
        return value;
      }),
      catchError(error => {
        this.loading = false;
        this.statusError = this.setError(error);
        return throwError(this.getError());
      })
    );
  }



  delete(id: number): Observable<number> {
    this.loading = false;
    return this.http.delete(`${this.config.url}/${id}`)
    .pipe(
      map((result: any) => {
        this.loading = false;
        this.deleteTables(id);
        console.log(`delete URL= ${this.config.url}/${id}, ID=${id}`);
        console.log(this.tables);
        return id;
      }),
      catchError(error => {
        this.loading = false;
        this.statusError = this.setError(error);
        return throwError(this.getError());
      })
    );
  }

  createAll(channels: T[]) {
    if (channels.length === 0) {
      console.log('channels not found');
      return of([]);
    } else {
      const services = channels.map(result => this.create(result));
      return forkJoin(...services);
    }
  }

  deleteAll(channels: T[]) {
    if (channels.length === 0) {
      console.log('channels not found');
      return of([]);
    } else {
      const services = channels.map(result => this.delete(result['id']));
      return forkJoin(...services);
    }
  }

  setError(statusError: HttpErrorResponse): StatusError {
    let name: string, message: string;
    name = statusError.name;
    message = statusError.message;
    if (statusError.error) {
      if (statusError.error.error) {
        name = statusError.error.error.name
          ? statusError.error.error.name
          : name;
        message = statusError.error.error.message
          ? statusError.error.error.message
          : message;
      }
    }
    return {
      name: name ? name : 'Error',
      message: message ? message : 'Internal server error'
    };
  }

}
