import { MatTableDataSource } from '@angular/material';
import { NormalTextPipe } from './normal-text.pipe';
import { FormControl } from '@angular/forms';
import { Subject } from 'rxjs';

interface TypeFilter {
  value: string;
  label: string;
}

export interface TableConfig {
  title: string;
  columns: string[];
  typeFilters: TypeFilter[];
}

export class CoreTable<T> {
  constructor(config: TableConfig) {
      this.title = config.title;
      this.columns = config.columns;
      this.typeFilters = config.typeFilters;
      const PAGE = localStorage.getItem('pageSize');
      // tslint:disable-next-line:radix
      this.pageSize = PAGE ? parseInt(PAGE) : 10;
  }

  destroy$: Subject<boolean> = new Subject<boolean>();
  search: FormControl = new FormControl();
  title: string;
  pageSize: number;
  pageIndex = 0;
  action: boolean;
  pageSizes = [10, 25, 50, 100];
  selectedId: number;
  columns: string[] = [];
  typeFilters: TypeFilter[] = [];
  typeFilter = 'all';
  datasource: MatTableDataSource<T> = new MatTableDataSource([]);
  text: NormalTextPipe = new NormalTextPipe();
  searchDate: FormControl = new FormControl();
  maxDate: Date = new Date();

  onSetPage(event) {
    this.pageSize = event.pageSize;
    this.pageIndex = event.pageIndex;
    localStorage.setItem('pageSize', event.pageSize);
  }

  pushColumn(config: TypeFilter) {
    if (this.columns.findIndex(_column => _column === config.value) === -1) {
      this.columns.push(config.value);
      this.typeFilters.push(config);
    }
  }

  enabledAction() {
    const index = this.columns.findIndex(result => result === 'action');
    if (this.action === true && index === -1) {
      this.columns = [...this.columns, 'action'];
    }
  }

  filterPredicate(data: any, keyword: string, typeFilter: string): boolean {
    let result;
    if (typeFilter === 'all') {
      result = JSON.stringify(data);
    } else {
      result = data[ typeFilter ];
    }
    result = this.text.transform(result);
    return result.indexOf(this.text.transform(keyword)) !== -1;
  }

  compare(a, b, isAsc) {
    return (this.text.transform(a) < this.text.transform(b) ? -1 : 1) * (isAsc ? 1 : -1);
  }

  exist(data: any[]): any[] {
    return data.filter((item, pos, self) => item ? self.indexOf(item) === pos : false);
  }
}
