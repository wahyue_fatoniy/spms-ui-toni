
export enum ActivityType {
    Mos = 'MOS',
    Installation = 'INSTALLATION',
    Integration = 'INTEGRATION',
    TakeData = 'TAKE_DATA',
    Atp = 'ATP',
    CreateDocument = 'CREATE_DOCUMENT',
    Create = 'CREATE_BAUT'
}

export enum TaskType {
  Documentaion = 'DOCUMENTATION',
  Engineer = 'ENGINEER',
  Technician = 'TECHNICIAN'
}

export class Activity {
  constructor(input?: Activity) {
    Object.assign(this, input);
  }

  private id: number;
  private type: TaskType;
  private number: number;
  private name: ActivityType;

  getId(): number {
    return this.id;
  }

  setId(id: number) {
    this.id = id;
  }

  getType(): TaskType {
    return this.type;
  }

  setType(type: TaskType) {
    this.type = type;
  }

  getNumber(): number {
    return this.number;
  }

  setNumber(number: number) {
    this.number = number;
  }

  getName(): ActivityType {
    return this.name;
  }

  setName(name: ActivityType) {
    this.name = name;
  }

}
