import { Injectable } from '@angular/core';
import { CoreServiceConfig, CoreService } from '../../core-service';
import { environment } from '../../../../environments/environment';
import { AdminCenter } from './adminCenter.model';
import { HttpClient } from '@angular/common/http';

// tslint:disable-next-line:prefer-const
let CONFIG: CoreServiceConfig = {
  url: `${environment.apiUrl}/api/admin_centers`,
  setRow: input => new AdminCenter(input)
};


@Injectable({
  providedIn: 'root'
})
export class AdminCenterService extends CoreService<AdminCenter> {

  constructor(http: HttpClient) {
    super(CONFIG, http);
  }

}
