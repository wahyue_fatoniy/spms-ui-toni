import { Role } from '../role/role';

export class AdminCenter extends Role {
  private nip: number;

  constructor(input?: AdminCenter) {
    super(input);
  }

  getNip() {
    return this.nip;
  }
}
