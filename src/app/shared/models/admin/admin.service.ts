import { Injectable } from '@angular/core';
import { CoreServiceConfig, CoreService } from '../../core-service';
import { environment } from '../../../../environments/environment';
import { Admin } from './admin';
import { HttpClient } from '@angular/common/http';

// tslint:disable-next-line:prefer-const
let CONFIG: CoreServiceConfig = {
  url: `${environment.apiUrl}/api/admins`,
  setRow: input => new Admin(input)
};


@Injectable({
  providedIn: 'root'
})
export class AdminService extends CoreService<Admin> {

  constructor(http: HttpClient) {
    super(CONFIG, http);
  }

}
