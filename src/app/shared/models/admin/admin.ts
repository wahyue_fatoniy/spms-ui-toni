import { Role } from '../role/role';
import { Region } from '../region/region';

export class Admin extends Role {
  private nip: number;
  private region: Region;

  constructor(input?: Admin) {
    super(input);
    if (input && input.region) {
      this.region = new Region(input.region);
    }
  }

  getNip() {
    return this.nip;
  }

  getRegion() {
    return this.region;
  }
}
