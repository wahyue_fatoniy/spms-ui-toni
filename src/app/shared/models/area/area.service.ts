import { Injectable } from '@angular/core';
import { CoreServiceConfig, CoreService } from '../../core-service';
import { environment } from 'src/environments/environment';
import { HttpClient } from '@angular/common/http';
import { Region } from '../region/region';
import { Observable, throwError } from 'rxjs';
import { map, catchError } from 'rxjs/operators';
import { Area } from './area';

// tslint:disable-next-line:prefer-const
let CONFIG: CoreServiceConfig = {
  url: `${environment.apiUrl}/api/areas`,
  setRow: input => new Area(input)
};


@Injectable({
  providedIn: 'root'
})
export class AreaService extends CoreService<Area> {

  constructor(http: HttpClient) {
    super(CONFIG, http);
  }

  findByRegions(regions: Region[]): Observable<Area[]> {
    this.setLoading(true);
    return this.http.post(`${CONFIG.url}/regions`, regions).pipe(
      map((result: any) => {
        this.setLoading(false);
        const tables = result.map(_project => new Area(_project));
        this.setTables(tables);
        console.log('get by regions', tables);
        return result;
      }),
      catchError(error => {
        this.setLoading(false);
        this.setStatusError(this.setError(error));
        return throwError(this.getError());
      })
    );
  }

  getByRegionId(id: number): Area[] {
    return this.getTables().filter(_set => {
      return _set.getRegion().getId() === id;
    });
  }

}
