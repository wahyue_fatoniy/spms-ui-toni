import { Region } from '../region/region';

export class Area {
  constructor(input?: Area) {
    Object.assign(this, input);
    if (input && input.region) {
      this.region = new Region(input.region);
    }
  }

  private id: number;
  private name: string;
  private latitude: number;
  private longitude: number;
  private region: Region;

  getId(): number {
    return this.id;
  }

  setId(id: number) {
    this.id = id;
  }

  getName(): string {
    return this.name;
  }

  setName(name: string) {
    this.name = name;
  }

  getLatitude(): number {
    return this.latitude;
  }

  setLatitude(latitude: number) {
    this.latitude = latitude;
  }

  getLongitude(): number {
    return this.longitude;
  }

  setLongitude(longitude: number) {
    this.longitude = longitude;
  }

  getRegion(): Region {
    return this.region;
  }

  setRegion(region: Region) {
    this.region = region;
  }

}
