import { Injectable } from '@angular/core';
import { CoreService, CoreServiceConfig } from '../../core-service';
import { Cordinator } from './cordinator';
import { environment } from 'src/environments/environment';
import { HttpClient } from '@angular/common/http';

// tslint:disable-next-line:prefer-const
let CONFIG: CoreServiceConfig = {
  url: `${environment.apiUrl}/api/cordinators`,
  setRow: input => new Cordinator(input)
};


@Injectable({
  providedIn: 'root'
})
export class CordinatorService extends CoreService<Cordinator> {

  constructor(http: HttpClient) {
    super(CONFIG, http);
  }

}
