import { Role } from '../role/role';
import { Region } from '../region/region';

export class Cordinator extends Role {

  private region: Region;
  private nip: number;

  constructor(input?: Cordinator) {
    super(input);
    if (input && input.region) {
      this.region = new Region(input.region);
    }
  }

  getRegion(): Region {
    return this.region;
  }

  setRegion(region: Region) {
    this.region = region;
  }

  getNip(): number {
    return this.nip;
  }

  setNip(nip: number) {
    this.nip = nip;
  }

}
