import { Injectable } from '@angular/core';
import { CoreServiceConfig, CoreService } from '../../core-service';
import { environment } from '../../../../environments/environment';
import { Customer } from './customer';
import { HttpClient } from '@angular/common/http';

// tslint:disable-next-line:prefer-const
let CONFIG: CoreServiceConfig = {
  url: `${environment.apiUrl}/api/customers`,
  setRow: input => new Customer(input)
};

@Injectable({
  providedIn: 'root'
})
export class CustomerService extends CoreService<Customer> {

  constructor(http: HttpClient) {
    super(CONFIG, http);
   }
}
