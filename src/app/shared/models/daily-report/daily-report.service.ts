import { Injectable } from '@angular/core';
import { CoreServiceConfig, CoreService } from '../../core-service';
import { DailyReport } from './daily-report';
import { environment } from '../../../../environments/environment';
import { HttpClient } from '@angular/common/http';
import { throwError } from 'rxjs';
import { catchError, map } from 'rxjs/operators';


// tslint:disable-next-line:prefer-const
let CONFIG: CoreServiceConfig = {
  url: `${environment.apiUrl}/api/daily_reports`,
  setRow: input => new DailyReport(input)
};


@Injectable({
  providedIn: 'root'
})
export class DailyReportService extends CoreService<DailyReport> {

  constructor(http: HttpClient) {
    super(CONFIG, http);
  }

  submit(id: number) {
    this.setLoading(true);
    return this.http.post(`${CONFIG.url}/submit/${id}`, {})
    .pipe(
      map((result: DailyReport) => {
        this.setLoading(false);
        this.editTables(id, new DailyReport(result));
        console.log('daily report', new DailyReport(result));
        return result;
      }),
      catchError(error => {
        this.setLoading(false);
        this.setStatusError(this.setError(error));
        return throwError(this.getError());
      })
    );
  }

  findBySiteWorkId(id: number) {
    this.setLoading(true);
    return this.http.get(`${CONFIG.url}/site_work/${id}`)
    .pipe(
      map((result: any) => {
        this.setLoading(false);
        const tables = result.map(_project => new DailyReport(_project));
        this.setTables(tables);
        console.log('daily report', tables);
        return result;
      }),
      catchError(error => {
        this.setLoading(false);
        this.setStatusError(this.setError(error));
        return throwError(this.getError());
      })
    );
  }

  getPriority(): DailyReport[] {
    return this.getTables().sort((a, b) => b.getId() - a.getId());
  }


}
