import { SiteWork } from '../site-work/site-work';
import { TaskExecution } from '../task-execution/task-execution';
import { Task } from '../task/task';
import { Resource } from '../resource/resource';

export class DailyReport {
  constructor(input?: DailyReport) {
    Object.assign(this, input);
    if (input && input.siteWork) {
      this.siteWork = new SiteWork(input.siteWork);
    }
    if (input && input.taskExecutions) {
      this.taskExecutions = input.taskExecutions ?
      input.taskExecutions.map(
        task => new TaskExecution(task)
      ) : [];
    }
  }

  private id: number;
  private date: string;
  private submit: boolean;
  private siteWork: SiteWork;
  private taskExecutions: TaskExecution[] = [];
  private opex: number;

  getId(): number {
    return this.id;
  }

  setId(id: number) {
    this.id = id;
  }

  getDate(): Date {
    return new Date(this.date);
  }

  setDate(date: string) {
    this.date = date;
  }

  getSubmit(): boolean {
    return this.submit;
  }

  setSubmit(submit: boolean) {
    this.submit = submit;
  }

  getSiteWork(): SiteWork {
    return this.siteWork;
  }

  setSiteWork(siteWork: SiteWork) {
    this.siteWork = siteWork;
  }

  getTaskExecutions(): TaskExecution[] {
    return this.taskExecutions ? this.taskExecutions : [];
  }

  setTaskExecutions(taskExecutions: TaskExecution[]) {
    this.taskExecutions = taskExecutions ? taskExecutions : [];
  }

  changeFinish(index: number, value: boolean) {
    if (this.taskExecutions[index]) {
      this.taskExecutions[index].setFinish(value);
    }
  }

  changeManHours(index: number, value: number) {
    if (this.taskExecutions[index]) {
      this.taskExecutions[index].setManHour(value);
    }
  }

  changeResources(index: number, value: Resource) {
    if (this.taskExecutions[index]) {
      this.taskExecutions[index].setResource(value);
    }
  }

  getTaskIds(): number[] {
    return this.taskExecutions ? this.taskExecutions.map(_set => _set.getTask().getId()) : [];
  }

  setTaskExecutionsByTasks(tasks: Task[]) {
    let executions: TaskExecution[] = [];
    for (const task of tasks) {
      const index = this.taskExecutions ? this.taskExecutions.findIndex(_set => _set.getTask().getId() === task.getId()) : -1;
      if (index === -1) {
        const execution = new TaskExecution();
        execution.setFinish(false);
        execution.setManHour(0);
        execution.setTask(task);
        execution.setResource(new Resource());
        execution.setId(null);
        executions.push(execution);
      } else {
        executions.push(this.taskExecutions[index]);
      }
    }
    this.setTaskExecutions(executions);
  }


  setOpex(opex: number) {
    this.opex = opex;
  }

  getOpex() {
    return this.opex;
  }
}
