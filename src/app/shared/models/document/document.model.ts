export enum Status {
  create = 'CREATE',
  submit = 'SUBMIT',
  revision = 'REVISION',
  reject = 'REJECT',
  approve = 'APPROVE'
}

export class Document {
  private id: number;
  private no: string;
  private title: string;
  private status: Status;
  private author: string;
  private nik: number;
  private reviewer: string;

  constructor(input?: Object) {
    if (input) {
      this.id = input['id'];
      this.no = input['no'];
      this.title = input['title'];
      this.status = input['status'];
      this.author = input['author'];
      this.nik = input['nik'];
      this.reviewer = input['reviewer'];
    }
  }

  public getId() {
    return this.id;
  }

  public getNo() {
    return this.no;
  }

  public getTitle() {
    return this.title;
  }

  public getStatus() {
    return this.status;
  }

  public getAuthor() {
    return this.author;
  }

  public getNIK() {
    return this.nik;
  }

  public getReviewer() {
    return this.reviewer;
  }

  public setId(id: number) {
    this.id = id;
  }

  public setNo(no: string) {
    this.no = no;
  }

  public setTitle(title: string) {
    this.title = title;
  }

  public setStatus(status: Status) {
    this.status = status;
  }

  public setNik(nik: number) {
    this.nik = nik;
  }

  public setReviewer(reviewer: string) {
    this.reviewer = reviewer;
  }

  public setAuthor(author: string) {
    this.author = author;
  }

}

export class DocumentLifecycle {
  private id: number;
  private document: Document;
  private date: Date;
  private description: string;
  private status: Status;

  constructor(input?: Object) {
    if (input) {
      this.id = input['id'];
      this.document = new Document(input['document']);
      this.date = input['date'];
      this.description = input['description'];
    }
  }

  public getId() {
    return this.id;
  }

  public getDocument() {
    return this.document;
  }

  public getDate() {
    return this.date;
  }

  public getDescription() {
    return this.description;
  }

  public setId(id: number) {
    this.id = id;
  }

  public setDocument(document: Document) {
    this.document = document;
  }

  public setDate(date: Date) {
    this.date = date;
  }

  public setDescription(description: string) {
    this.description = description;
  }
}
