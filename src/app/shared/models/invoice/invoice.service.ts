import { Injectable } from '@angular/core';
import { CoreServiceConfig, CoreService } from '../../core-service';
import { Invoice } from './invoice';
import { environment } from '../../../../environments/environment';
import { HttpClient } from '@angular/common/http';
import { map, catchError } from 'rxjs/operators';
import { throwError } from 'rxjs';
import { PurchaseOrder } from '../../../../models/purchaseOrder-service/purchase-order.model';

// tslint:disable-next-line:prefer-const
let CONFIG: CoreServiceConfig = {
  url: `${environment.apiUrl}/api/invoices`,
  setRow: input => new Invoice(input)
};


@Injectable({
  providedIn: 'root'
})
export class InvoiceService extends CoreService<Invoice> {

  constructor(http: HttpClient) {
    super(CONFIG, http);
  }

  findByPOId(id: number) {
    this.setLoading(true);
    return this.http.get(`${CONFIG.url}/purchase_order/${id}`).pipe(
      map((result: any) => {
        this.setLoading(false);
        const tables = result.map(_project => new Invoice(_project));
        this.setTables(tables);
        console.log('get invoices by po id', tables);
        return result;
      }),
      catchError(error => {
        this.setLoading(false);
        this.setStatusError(this.setError(error));
        return throwError(this.getError());
      })
    );
  }

  submit(id: number) {
    this.setLoading(true);
    return this.http.post(`${CONFIG.url}/submit/${id}`, {})
    .pipe(
      map((result: Invoice) => {
        this.setLoading(false);
        this.editTables(id, new Invoice(result));
        console.log('invoices', new Invoice(result));
        return result;
      }),
      catchError(error => {
        this.setLoading(false);
        this.setStatusError(this.setError(error));
        return throwError(this.getError());
      })
    );
  }

  setPaymentDate(id: number) {
    this.setLoading(true);
    return this.http.post(`${CONFIG.url}/payment/${id}`, {}).pipe(
      map((result: Invoice) => {
        this.setLoading(false);
        this.editTables(id, new Invoice(result));
        console.log('invoices', new Invoice(result));
        return result;
      }),
      catchError(error => {
        this.setLoading(false);
        this.setStatusError(this.setError(error));
        return throwError(this.getError());
      })
    );
  }
}
