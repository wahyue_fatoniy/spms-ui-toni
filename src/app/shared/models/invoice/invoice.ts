import { PurchaseOrder } from '../purchase-order/purchase-order';
import { SiteWork } from '../site-work/site-work';
import { DatePipe } from '@angular/common';

export class Invoice {
  constructor(input?: Invoice) {
    Object.assign(this, input);
    if (input && input.purchaseOrder) {
      this.purchaseOrder = new PurchaseOrder(input.purchaseOrder);
    }
    if (input && input.siteWorks) {
      this.siteWorks = input.siteWorks.map(_set => new SiteWork(_set));
    }
  }

  private id: number;
  private invoiceNumber: string;
  private invoiceDate: string;
  private dueDate: string;
  private invoiceValue: number;
  private referenceBast: string;
  private paymentDate: string;
  private purchaseOrder: PurchaseOrder;
  private siteWorks: SiteWork[];
  private submit: boolean;

  getId(): number {
    return this.id;
  }

  setId(id: number) {
    this.id = id;
  }

  getInvoiceNumber(): string {
    return this.invoiceNumber;
  }

  setInvoiceNumber(invoiceNumber: string) {
    this.invoiceNumber = invoiceNumber;
  }

  getInvoiceDate(): Date {
    return new Date(this.invoiceDate);
  }

  setInvoiceDate(invoiceDate: Date) {
    const date = new DatePipe('id');
    this.invoiceDate = date.transform(invoiceDate, 'yyyy-MM-ddTHH:mm:ssZ', '+0000');
  }

  getDueDate(): Date {
    return new Date(this.dueDate);
  }

  setDueDate(dueDate: Date) {
    const date = new DatePipe('id');
    this.dueDate = date.transform(dueDate, 'yyyy-MM-ddTHH:mm:ssZ', '+0000');
  }

  getInvoiceValue(): number {
    return this.invoiceValue;
  }

  setInvoiceValue(invoiceValue: number) {
    this.invoiceValue = invoiceValue;
  }

  getReferenceBast(): string {
    return this.referenceBast;
  }

  setReferenceBast(referenceBast: string) {
    this.referenceBast = referenceBast;
  }

  getPaymentDate(): Date {
    return this.paymentDate ? new Date(this.paymentDate) : null;
  }

  setPaymentDate(paymentDate: Date) {
    const date = new DatePipe('id');
    this.paymentDate = paymentDate ? date.transform(paymentDate, 'yyyy-MM-ddTHH:mm:ssZ', '+0000') : null;
  }

  getPurchaseOrder(): PurchaseOrder {
    return this.purchaseOrder;
  }

  setPurchaseOrder(po: PurchaseOrder) {
    this.purchaseOrder = po;
  }

  getSiteWorks(): SiteWork[] {
    return this.siteWorks;
  }

  setSiteWorks(siteWorks: SiteWork[]) {
    this.siteWorks = siteWorks;
  }

  getSubmit(): boolean {
    return this.submit;
  }

  setSubmit(submit: boolean) {
    this.submit = submit;
  }

}
