import { Injectable } from '@angular/core';
import { CoreServiceConfig, CoreService } from '../../core-service';
import { environment } from '../../../../environments/environment';
import { Partner } from './partner';
import { HttpClient } from '@angular/common/http';

// tslint:disable-next-line:prefer-const
let CONFIG: CoreServiceConfig = {
  url: `${environment.apiUrl}/api/partners`,
  setRow: input => new Partner(input)
};


@Injectable({
  providedIn: 'root'
})
export class PartnerService extends CoreService<Partner> {

  constructor(http: HttpClient) {
    super(CONFIG, http);
   }
}
