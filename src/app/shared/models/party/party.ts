export class Party {
  constructor(input?: Party) {
    Object.assign(this, input);
  }

  private id: number;
  private name: string;
  private email: string;
  private address: string;
  private phone: string;

  getId(): number {
    return this.id;
  }

  setId(id: number) {
    this.id = id;
  }

  getName(): string {
    return this.name;
  }

  setName(name: string) {
    this.name = name;
  }

  getEmail(): string {
    return this.email;
  }

  setEmail(email: string) {
    this.email = email;
  }

  getAddress(): string {
    return this.address;
  }

  setAddress(address: string) {
    this.address = address;
  }

  getPhone(): string {
    return this.phone;
  }

  setPhone(phone: string) {
    return this.phone;
  }

}
