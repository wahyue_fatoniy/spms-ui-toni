import { Injectable } from '@angular/core';
import { CoreServiceConfig, CoreService } from '../../core-service';
import { ProjectManager } from './project-manager';
import { environment } from '../../../../environments/environment';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map, filter } from 'rxjs/operators';
// tslint:disable-next-line:prefer-const
let CONFIG: CoreServiceConfig = {
  url: `${environment.apiUrl}/api/project_managers`,
  setRow: input => new ProjectManager(input)
};


@Injectable({
  providedIn: 'root'
})
export class ProjectManagerService extends CoreService<ProjectManager> {

  constructor(http: HttpClient) {
    super(CONFIG, http);
  }

  public findByRegionId(id: number): Observable<ProjectManager[]> {
    return this.http.get(CONFIG.url).pipe(
      map((pmMap: ProjectManager[]) => {
        const pm = pmMap.map(valMap => new ProjectManager(valMap))
        .filter(pmFilter => pmFilter.getRegion().getId() === id);
        this.setTables(pm);
        return pm;
      })
    );
  }

}
