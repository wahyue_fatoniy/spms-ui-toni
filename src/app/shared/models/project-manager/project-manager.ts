import { Role } from '../role/role';
import { Region } from '../region/region';

export class ProjectManager extends Role {
  private region: Region;
  private nip: number;

  constructor(input?: ProjectManager) {
    super(input);
    if (input && input.region) {
      this.region = new Region(input.region);
    }
  }

  public getRegion() {
    return this.region;
  }

  public setRegion(region: Region) {
    this.region = new Region();
  }

  public getNip() {
    return this.nip;
  }

  public setNip(nip: number) {
    this.nip = nip;
  }

}
