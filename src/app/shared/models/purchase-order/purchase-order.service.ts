import { Injectable } from '@angular/core';
import { CoreServiceConfig, CoreService } from '../../core-service';
import { environment } from '../../../../environments/environment';
import { PurchaseOrder, PoStatus } from './purchase-order';
import { HttpClient } from '@angular/common/http';
import { map, catchError } from 'rxjs/operators';
import { throwError, Observable } from 'rxjs';
import { StatusType, SiteWork } from '../site-work/site-work';

// tslint:disable-next-line:prefer-const
let CONFIG: CoreServiceConfig = {
  url: `${environment.apiUrl}/api/purchase_orders`,
  setRow: input => new PurchaseOrder(input)
};

@Injectable({
  providedIn: 'root'
})
export class PurchaseOrderService extends CoreService<PurchaseOrder> {
  constructor(http: HttpClient) {
    super(CONFIG, http);
  }

  cancel(id: number) {
    this.setLoading(true);
    return this.http.post(`${CONFIG.url}/cancel/${id}`, {}).pipe(
      map((result: PurchaseOrder) => {
        this.setLoading(false);
        this.editTables(id, new PurchaseOrder(result));
        console.log('cancel', new PurchaseOrder(result));
        return result;
      }),
      catchError(error => {
        this.setLoading(false);
        this.setStatusError(this.setError(error));
        return throwError(this.getError());
      })
    );
  }

  getPriority(settings: PoStatus[]): PurchaseOrder[] {
    const projects = this.getTables().sort((a, b) => b.getId() - a.getId());
    const progress = [],
      cancel = [],
      done = [],
      notStarted = [];
    for (const project of projects) {
      if (project.getStatus() === PoStatus.Cancel) {
        cancel.push(project);
      } else if (project.getStatus() === PoStatus.Done) {
        done.push(project);
      } else if (project.getStatus() === PoStatus.Progress) {
        progress.push(project);
      } else {
        notStarted.push(project);
      }
    }
    let result = [];
    for (const setting of settings) {
      if (setting === PoStatus.Progress) {
        result = [...result, ...progress];
      } else if (setting === PoStatus.Done) {
        result = [...result, ...done];
      } else if (setting === PoStatus.Cancel) {
        result = [...result, ...cancel];
      } else if (setting === PoStatus.NotStarted) {
        result = [...result, ...notStarted];
      }
    }
    console.log(result);
    return result;
  }

  public addProjectPo(poId: number, projects: SiteWork[]): Promise<PurchaseOrder> {
    this.setLoading(true);
    return new Promise((resolve, reject) => {
        this.http
          .post(`${CONFIG.url}/site_work/add/${poId}`, {
            siteWorks: projects
          }).subscribe((val: PurchaseOrder) => {
            this.setLoading(false);
            resolve(val);
          }, err => {
            this.setLoading(false);
            reject(err);
          });
    });
  }

  public removeProjectPo(poId: number, projects: SiteWork[]): Promise<PurchaseOrder> {
    this.setLoading(true);
    return new Promise((resolve, reject) => {
        this.http
          .post(`${CONFIG.url}/site_work/remove/${poId}`, {
            siteWorks: projects
          }).subscribe((val: PurchaseOrder) => {
            this.setLoading(false);
            resolve(val);
          }, err => {
            this.setLoading(false);
            reject(err);
          });
    });
  }
}
