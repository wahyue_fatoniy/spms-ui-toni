import { Customer } from "../customer/customer";
import { Partner } from "../partner/partner";
import { Sales } from "../sales/sales";
import { SiteWork } from "../site-work/site-work";
import { DatePipe } from "@angular/common";

export enum PoStatus {
  Progress = "ON_PROGRESS",
  Done = "DONE",
  NotStarted = "NOTSTARTED",
  Cancel = "CANCEL"
}

export class PurchaseOrder {
  constructor(input?: PurchaseOrder) {
    Object.assign(this, input);
    if (input && input.customer) {
      this.customer = new Customer(input.customer);
    }
    if (input && input.sales) {
      this.sales = new Sales(input.sales);
    }
    if (input && input.partner) {
      this.partner = new Partner(input.partner);
    }
    if (input && input.siteWorks) {
      this.siteWorks = input.siteWorks.map(_set => new SiteWork(_set));
    }
  }

  private id: number;
  private poNumber: string;
  // private poValue = 0;
  private publishDate: string;
  private expiredDate: string;
  private customer: Customer;
  private partner: Partner;
  private sales: Sales;
  private siteWorks: any[];
  private totalInvoice: number;
  private cancel: boolean;
  private submit: boolean;
  private totalProjectValue: number;

  getId(): number {
    return this.id;
  }

  setId(id: number) {
    this.id = id;
  }

  getPoNumber(): string {
    return this.poNumber;
  }

  setPoNumber(poNumber: string) {
    this.poNumber = poNumber;
  }

  getPoValue(): number {
    return this.totalProjectValue !== null ? this.totalProjectValue : 0;
  }

  setPoValue(poValue: number) {
    this.totalProjectValue = poValue;
  }

  getPublishDate(): Date {
    return new Date(this.publishDate);
  }

  setPublishDate(publishDate: Date) {
    const date = new DatePipe("id");
    this.publishDate = date.transform(
      publishDate,
      "yyyy-MM-ddTHH:mm:ssZ",
      "+0000"
    );
  }

  getExpiredDate(): Date {
    return new Date(this.expiredDate);
  }

  setExpiredDate(expiredDate: Date) {
    const date = new DatePipe("id");
    this.expiredDate = date.transform(
      expiredDate,
      "yyyy-MM-ddTHH:mm:ssZ",
      "+0000"
    );
  }

  getCustomer(): Customer {
    return this.customer;
  }

  setCustomer(customer: Customer) {
    this.customer = customer;
  }

  getSales(): Sales {
    return this.sales;
  }

  setSales(sales: Sales) {
    this.sales = sales;
  }

  getPartner(): Partner {
    return this.partner;
  }

  setPartner(partner: Partner) {
    this.partner = partner;
  }

  getSiteWorks(): SiteWork[] {
    return this.siteWorks;
  }

  setSiteWorks(siteWorks: any[]) {
    this.siteWorks = siteWorks;
  }

  getTotalInvoice(): number {
    return this.totalInvoice;
  }

  setTotalInvoice(totalInvoice: number) {
    this.totalInvoice = totalInvoice;
  }

  getCancel(): boolean {
    return this.cancel;
  }

  setCancel(cancel: boolean) {
    this.cancel = cancel;
  }

  getStatus(): PoStatus {
    let status: PoStatus;
    if (this.cancel) {
      status = PoStatus.Cancel;
    } else if (
      (this.totalInvoice === this.totalProjectValue ||
        this.totalInvoice > this.totalProjectValue) &&
      this.totalInvoice !== 0
    ) {
      status = PoStatus.Done;
    } else if (
      this.totalInvoice !== 0 &&
      this.totalInvoice < this.totalProjectValue
    ) {
      status = PoStatus.Progress;
    } else {
      status = PoStatus.NotStarted;
    }
    return status;
  }

  getTotalProjectValue() {
    return this.totalProjectValue;
  }

  setTotalProjectValue(value: number) {
    this.totalProjectValue = value;
  }

  getSubmit(): boolean {
    return this.submit;
  }

  setSubmit(submit: boolean) {
    this.submit = submit;
  }

  getSiteWorkIds(): number[] {
    const siteWorks = this.siteWorks ? this.siteWorks : [];
    return siteWorks.map(_set => _set.getId());
  }

  // mengupdte nilai po value dari site Work
  changeValueSiteWork(index: number, value: number) {
    if (this.siteWorks[index]) {
      this.siteWorks[index].setPoValue(value);
    }
  }

  getRemainingInvoice(): number {
    const totalInvoice = this.totalInvoice ? this.totalInvoice : 0;
    return this.totalProjectValue - totalInvoice;
  }

  getTotalPoValue(): number {
    let total = 0;
    const siteWorks: SiteWork[] = this.siteWorks ? this.siteWorks : [];
    for (const project of siteWorks) {
      total += project.getPoValue() ? project.getPoValue() : 0;
    }
    return total;
  }
}
