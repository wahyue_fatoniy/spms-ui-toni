export enum referenceType {
  assignmentId = 'ASSIGNMENT_ID',
  activityId = 'ACTIVITY_ID',
  reference = 'REFERENCE',
}

export class Reference {
  private id: number;
  private referenceType: referenceType;
  private description: string;

  constructor(input?: Reference) {
    Object.assign(this, input);
  }

  getId() {
    return this.id;
  }

  setId(id: number) {
    this.id = id;
  }

  getReferenceType() {
    return this.referenceType;
  }

  setReferenceType(reference: referenceType) {
    this.referenceType = reference;
  }

  getDescription() {
    return this.description;
  }

  setDescription(desc: string) {
    this.description = desc;
  }
}
