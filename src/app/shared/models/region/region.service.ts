import { Injectable } from '@angular/core';
import { CoreServiceConfig, CoreService } from '../../core-service';
import { environment } from '../../../../environments/environment';
import { Region } from './region';
import { HttpClient } from '@angular/common/http';

// tslint:disable-next-line:prefer-const
let CONFIG: CoreServiceConfig = {
  url: `${environment.apiUrl}/api/regions`,
  setRow: input => new Region(input)
};


@Injectable({
  providedIn: 'root'
})
export class RegionService extends CoreService<Region> {

  constructor(http: HttpClient) {
    super(CONFIG, http);
  }

}
