export class Region {
  constructor(input?: Region) {
    Object.assign(this, input);
  }

  private id: number;
  private name: string;
  private latitude: number;
  private longitude: number;

  getId(): number {
    return this.id;
  }

  setId(id: number) {
    this.id = id;
  }

  getName(): string {
    return this.name;
  }

  setName(name: string) {
    this.name = name;
  }

  getLatitude(): number {
    return this.latitude;
  }

  setLatitude(latitude: number) {
    this.latitude = latitude;
  }

  getLongitude(): number {
    return this.longitude;
  }

  setLongitude(longitude: number) {
    this.longitude = longitude;
  }

}
