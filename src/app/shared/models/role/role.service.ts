import { Injectable } from '@angular/core';
import { CoreServiceConfig, CoreService } from '../../core-service';
import { environment } from '../../../../environments/environment';
import { Role } from './role';
import { HttpClient } from '@angular/common/http';

// tslint:disable-next-line:prefer-const
let CONFIG: CoreServiceConfig = {
  url: `${environment.apiUrl}/api/roles`,
  setRow: input => new Role(input)
};

@Injectable({
  providedIn: 'root'
})
export class RoleService extends CoreService<Role>  {

  constructor(http: HttpClient) {
    super(CONFIG, http);
  }

  getByEmail(email: string) {
    return this.getTables().filter(_set => _set.getParty().getEmail() === email);
  }

}

