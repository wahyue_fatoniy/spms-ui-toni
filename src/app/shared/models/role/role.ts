import { Party } from '../party/party';
import { Region } from '../region/region';

export enum RoleType {
    Customer = 'CUSTOMER',
    Sales = 'SALES',
    Admin = 'ADMIN',
    Partner = 'PARTNER',
    Engineer = 'ENGINEER',
    Technician = 'TECHNICIAN',
    Documentation = 'DOCUMENTATION',
    Cordinator = 'CORDINATOR',
    User = 'USER',
    ProjectManager = 'PROJECT_MANAGER'
}

export class Role {

  constructor(input?: Role) {
    Object.assign(this, input);
    if (input && input.party) {
      this.party = new Party(input.party);
    }
  }

  public id: number;
  public roleType: RoleType;
  public party: Party;

  getId(): number {
    return this.id;
  }

  setId(id: number) {
    this.id = id;
  }

  getRoleType(): RoleType {
    return this.roleType;
  }

  setRoleType(roleType: RoleType) {
    this.roleType = roleType;
  }

  getParty(): Party {
    return this.party;
  }

  setParty(party: Party) {
    this.party = party;
  }

}
