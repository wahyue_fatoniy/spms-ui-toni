import { Injectable } from '@angular/core';
import { CoreServiceConfig, CoreService } from '../../core-service';
import { environment } from '../../../../environments/environment';
import { Sales } from './sales';
import { HttpClient } from '@angular/common/http';

// tslint:disable-next-line:prefer-const
let CONFIG: CoreServiceConfig = {
  url: `${environment.apiUrl}/api/sales`,
  setRow: input => new Sales(input)
};


@Injectable({
  providedIn: 'root'
})
export class SalesService extends CoreService<Sales> {

  constructor(http: HttpClient) {
    super(CONFIG, http);
  }
}
