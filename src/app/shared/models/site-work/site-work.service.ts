import { Injectable } from '@angular/core';
import { CoreService, CoreServiceConfig } from '../../core-service';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../../../environments/environment';
import { catchError, map } from 'rxjs/operators';
import { throwError, of, Observable } from 'rxjs';
import { SiteWork, StatusType } from './site-work';
import { Status } from '../../../../models/enum/enum';
import { StatusCellComponent } from '../../cell/status-cell/status-cell.component';
import { Admin } from '../admin/admin';
import { ProjectManager } from '../project-manager/project-manager';
import { Cordinator } from '../cordinator/cordinator';

// tslint:disable-next-line:prefer-const
let CONFIG: CoreServiceConfig = {
  url: `${environment.apiUrl}/api/site_works`,
  setRow: input => new SiteWork(input)
};

@Injectable({
  providedIn: 'root'
})
export class SiteWorkService extends CoreService<SiteWork> {
  constructor(http: HttpClient) {
    super(CONFIG, http);
  }

  findProgress() {
    this.setLoading(true);
    return this.http.get(`${CONFIG.url}/progress`).pipe(
      map((result: any) => {
        this.setLoading(false);
        const tables = result.map(_project => new SiteWork(_project));
        this.setTables(tables);
        console.log('Project progress', tables);
        return result;
      }),
      catchError(error => {
        this.setLoading(false);
        this.setStatusError(this.setError(error));
        return throwError(this.getError());
      })
    );
  }

  findDone() {
    this.setLoading(true);
    return this.http.get(`${CONFIG.url}/done`).pipe(
      map((result: any) => {
        this.setLoading(false);
        const tables = result.map(_project => new SiteWork(_project));
        this.setTables(tables);
        console.log('Project done', tables);
        return result;
      }),
      catchError(error => {
        this.setLoading(false);
        this.setStatusError(this.setError(error));
        return throwError(this.getError());
      })
    );
  }

  getPriority(settings: StatusType[]): SiteWork[] {
    const projects = this.getTables().sort((a, b) => b.getId() - a.getId());
    const progress = [], cancel = [], done = [], notStarted = [];
    for (const project of projects) {
      if (project.getStatus() === StatusType.Cancel) {
         cancel.push(project);
      } else if (project.getStatus() === StatusType.Done) {
         done.push(project);
      } else if (project.getStatus() === StatusType.NotStarted) {
         notStarted.push(project);
      } else if (project.getStatus() === StatusType.Progress) {
         progress.push(project);
      }
    }
    let result = [];
    for (const setting of settings) {
      if (setting === StatusType.NotStarted) {
        result = [...result, ...notStarted];
      } else if (setting === StatusType.Progress) {
        result = [...result, ...progress];
      } else if (setting === StatusType.Done) {
        result = [...result, ...done];
      } else if (setting === StatusType.Cancel) {
        result = [...result, ...cancel];
      }
    }
    return result;
  }

  findByAdminId(id: number) {
    this.setLoading(true);
    return this.http.get(`${CONFIG.url}/admin/party/${id}`).pipe(
      map((result: any) => {
        this.setLoading(false);
        const tables = result.map(_project => new SiteWork(_project));
        this.setTables(tables);
        console.log('get by admin', tables);
        return result;
      }),
      catchError(error => {
        this.setLoading(false);
        this.setStatusError(this.setError(error));
        return throwError(this.getError());
      })
    );
  }

  findByAdmins(admins: Admin[]): Observable<SiteWork[]> {
    this.setLoading(true);
    return this.http.post(`${CONFIG.url}/admins`, admins).pipe(
      map((result: any) => {
        console.log(result);
        this.setLoading(false);
        const tables = result.map(_project => new SiteWork(_project));
        this.setTables(tables);
        console.log('get by admins', tables);
        return result;
      }),
      catchError(error => {
        this.setLoading(false);
        this.setStatusError(this.setError(error));
        return throwError(this.getError());
      })
    );
  }

  findByPMs(pms: ProjectManager[]): Observable<SiteWork[]> {
    this.setLoading(true);
    return this.http.post(`${CONFIG.url}/project_managers`, pms).pipe(
      map((result: any) => {
        this.setLoading(false);
        const tables = result.map(_project => new SiteWork(_project));
        this.setTables(tables);
        console.log('get by PMs', tables);
        return result;
      }),
      catchError(error => {
        this.setLoading(false);
        this.setStatusError(this.setError(error));
        return throwError(this.getError());
      })
    );
  }

  findByCordinators(cordinators: Cordinator[]): Observable<SiteWork[]> {
    this.setLoading(true);
    return this.http.post(`${CONFIG.url}/cordinators`, cordinators).pipe(
      map((result: any) => {
        this.setLoading(false);
        const tables = result.map(_project => new SiteWork(_project));
        this.setTables(tables);
        console.log('get by Cordinators', tables);
        return result;
      }),
      catchError(error => {
        this.setLoading(false);
        this.setStatusError(this.setError(error));
        return throwError(this.getError());
      })
    );
  }

  findByCoordinatorId(id: number) {
    this.setLoading(true);
    return this.http.get(`${CONFIG.url}/cordinator/party/${id}`).pipe(
      map((result: any) => {
        this.setLoading(false);
        const tables = result.map(_project => new SiteWork(_project));
        this.setTables(tables);
        console.log('get by admin', tables);
        return result;
      }),
      catchError(error => {
        this.setLoading(false);
        this.setStatusError(this.setError(error));
        return throwError(this.getError());
      })
    );
  }


  cancel(id: number) {
    this.setLoading(true);
    return this.http.post(`${CONFIG.url}/cancel/${id}`, {})
    .pipe(
      map((result: SiteWork) => {
        console.log('result', result);
        this.setLoading(false);
        this.editTables(id, new SiteWork(result));
        console.log('cancel', new SiteWork(result));
        return result;
      }),
      catchError(error => {
        this.setLoading(false);
        this.setStatusError(this.setError(error));
        return throwError(this.getError());
      })
    );
  }

}
