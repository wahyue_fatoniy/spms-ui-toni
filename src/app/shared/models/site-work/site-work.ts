import { Site } from '../site/site';
import { Team } from '../team/team';
import { Admin } from '../admin/admin';
import { ProjectManager } from '../project-manager/project-manager';
import { Task } from '../task/task';
import { DatePipe } from '@angular/common';
import { AdminCenter } from '../admin-center/adminCenter.model';
import { Document } from '../document/document.model';
import { Reference } from '../references/references';

export enum StatusType {
    Progress = 'ON_PROGRESS',
    Done = 'DONE',
    NotStarted = 'NOT_STARTED',
    Cancel = 'CANCEL'
}

export class SiteWork {
  constructor(input?: SiteWork) {
    Object.assign(this, input);
    if (input && input.site) {
      this.site = new Site(input.site);
    }
    if (input && input.team) {
      this.team = new Team(input.team);
    }
    if (input && input.admin) {
      this.admin = new Admin(input.admin);
    }
    if (input && input.projectManager) {
      this.projectManager = new ProjectManager(input.projectManager);
    }
    if (input && input.tasks) {
      this.tasks = input.tasks.map(_set => new Task(_set));
    }
    if (input && input.documents) {
      this.documents = input.documents.map(_set => new Document(_set));
    }
    if (input && input.adminCenter) {
      this.adminCenter = new AdminCenter(input.adminCenter);
    }


    if (input && input.references) {
      this.references = input.references.map(_set => new Reference(_set));
    }
  }

  private id: number;
  private projectCode: string;
  private status: StatusType;
  private startDate: string;
  private endDate: string;
  private site: Site;
  private poValue = 0;
  private team: Team;
  private budget = 0;
  private admin: Admin;
  private projectManager: ProjectManager;
  private tasks: Task[];
  private adminCenter: AdminCenter;
  private documents: Document[];
  private alarmCount: number;
  private estimatedProjectValue: number;
  private progress: number;
  private references: Reference[];

  getId(): number {
    return this.id;
  }

  setId(id: number) {
    this.id = id;
  }

  getProjectCode(): string {
    return this.projectCode;
  }

  setProjectCode(projectCode: string) {
    this.projectCode = projectCode;
  }

  getStatus(): StatusType {
    return this.status;
  }

  setStatus(status: StatusType) {
    this.status = status;
  }

  getStartDate(): Date {
    return this.startDate ? new Date(this.startDate) : null ;
  }

  setStartDate(startDate: string) {
    const date = new DatePipe('id');
    this.startDate = date.transform(startDate, 'yyyy-MM-ddTHH:mm:ssZ', '+0000');
  }

  getEndDate(): Date {
    return this.endDate ? new Date(this.endDate) : null;
  }

  setEndDate(endDate: Date) {
    const date = new DatePipe('id');
    this.endDate = date.transform(endDate, 'yyyy-MM-ddTHH:mm:ssZ', '+0000');
  }

  getSite(): Site {
    return this.site;
  }

  setSite(site: Site) {
    this.site = site;
  }

  getBudget(): number {
    return this.budget;
  }

  setBudget(budget: number) {
    this.budget = budget;
  }

  getPoValue(): number {
    return this.poValue !== null ? this.poValue : 0;
  }

  setPoValue(poValue: number) {
    this.poValue = poValue;
  }

  getTeam(): Team {
    return this.team;
  }

  setTeam(team: Team) {
    this.team = team;
  }

  getAdmin(): Admin {
    return this.admin;
  }

  setAdmin(admin: Admin) {
    this.admin = admin;
  }

  getProjectManager(): ProjectManager {
    return this.projectManager;
  }

  setProjectManager(projectManager: ProjectManager) {
    this.projectManager = projectManager;
  }

  getTasks(): Task[] {
    return this.tasks;
  }

  setTasks(tasks: Task[]) {
    this.tasks = tasks;
  }

  getProgress(): number {
    return parseFloat(this.progress.toFixed(1));
  }

  getAdminCenter(): AdminCenter {
    return this.adminCenter;
  }

  setAdminCenter(adminCenter: AdminCenter) {
    this.adminCenter = adminCenter;
  }

  getDocuments() {
    return this.documents;
  }

  getAlarmCount() {
    return this.alarmCount;
  }

  getEstimatedValue() {
    return this.estimatedProjectValue;
  }

  getReferences() {
    return this.references ? this.references : [];
  }

  setReference(references: Reference[]) {
    this.references = references;
  }
}
