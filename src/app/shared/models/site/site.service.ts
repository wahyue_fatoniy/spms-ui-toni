import { Injectable } from '@angular/core';
import { Site } from './site';
import { CoreServiceConfig, CoreService } from '../../core-service';
import { environment } from '../../../../environments/environment';
import { HttpClient } from '@angular/common/http';
import { map, catchError } from 'rxjs/operators';
import { throwError, Observable } from 'rxjs';
import { Region } from '../region/region';

// tslint:disable-next-line:prefer-const
let CONFIG: CoreServiceConfig = {
  url: `${environment.apiUrl}/api/sites`,
  setRow: input => new Site(input)
};


@Injectable({
  providedIn: 'root'
})
export class SiteService extends CoreService<Site> {

  constructor(http: HttpClient) {
    super(CONFIG, http);
  }

  findByRegions(regions: Region[]): Observable<Site[]> {
    this.setLoading(true);
    return this.http.post(`${CONFIG.url}/regions`, regions).pipe(
      map((result: any) => {
        this.setLoading(false);
        const tables = result.map(_project => new Site(_project));
        this.setTables(tables);
        console.log('get by regions', tables);
        return result;
      }),
      catchError(error => {
        this.setLoading(false);
        this.setStatusError(this.setError(error));
        return throwError(this.getError());
      })
    );
  }

}
