import { Region } from '../region/region';
import { Area } from '../area/area';

export class Site {
  constructor(input?: Site) {
    Object.assign(this, input);
    if (input && input.area) {
      this.area = new Area(input.area);
    }
    if (input && input.region) {
      this.region = new Region(input.region);
    }
  }

  private id: number;
  private name: string;
  private siteCode: string;
  private latitude: number;
  private longitude: number;
  private region: Region;
  private area: Area;

  getId(): number {
    return this.id;
  }

  setId(id: number) {
    this.id = id;
  }

  getName(): string {
    return this.name;
  }

  setName(name: string) {
    this.name = name;
  }

  getSiteCode(): string {
    return this.siteCode;
  }

  setSiteCode(siteCode: string) {
    this.siteCode = siteCode;
  }

  getLatitude(): number {
    return this.latitude;
  }

  setLatitude(latitude: number) {
    this.latitude = latitude;
  }

  getLongitude(): number {
    return this.longitude;
  }

  setLongitude(longitude: number) {
    this.longitude = longitude;
  }

  getRegion(): Region {
    return this.region;
  }

  setRegion(region: Region) {
    this.region = region;
  }

  getArea(): Area {
    return this.area;
  }

  setArea(area: Area) {
    this.area = area;
  }

}
