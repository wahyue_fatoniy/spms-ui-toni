import { Injectable } from '@angular/core';
import { CoreServiceConfig, CoreService } from '../../core-service';
import { environment } from '../../../../environments/environment';
import { SowType } from './sow-type';
import { HttpClient } from '@angular/common/http';
import { of, forkJoin, throwError, Observable } from 'rxjs';
import { catchError, map } from 'rxjs/operators';
import { Region } from '../region/region';
let CONFIG: CoreServiceConfig = {
  url: `${environment.apiUrl}/api/sow_types`,
  setRow: input => new SowType(input)
};


@Injectable({
  providedIn: 'root'
})
export class SowTypeService extends CoreService<SowType> {

  constructor(http: HttpClient) {
      super(CONFIG, http);
  }

  findByRegionIds(ids: number[]): Observable<SowType[]> {
    this.setLoading(true);
    if (ids.length === 0) {
      this.setLoading(false);
      return of([]);
    } else {
      let services = [];
      for (const id of ids) {
        services.push(this.http.get(`${CONFIG.url}/region/${id}`));
      }
      return forkJoin(...services).pipe(
        map((result: any) => {
          this.setLoading(false);
          const tables = result.map(_project => new SowType(_project));
          this.setTables(tables);
          console.log('Project progress', tables);
          return result;
        }),
        catchError(error => {
          this.setLoading(false);
          this.setStatusError(this.setError(error));
          return throwError(this.getError());
        })
      );
    }
  }

  findByRegions(regions: Region[]): Observable<SowType[]> {
    this.setLoading(true);
    return this.http.post(`${CONFIG.url}/regions`, regions).pipe(
      map((result: any) => {
        this.setLoading(false);
        const tables = result.map(_project => new SowType(_project));
        this.setTables(tables);
        console.log('get by regions', tables);
        return result;
      }),
      catchError(error => {
        this.setLoading(false);
        this.setStatusError(this.setError(error));
        return throwError(this.getError());
      })
    );
  }

}
