import { Region } from '../region/region';

export class SowType {
  constructor(input?: SowType) {
    Object.assign(this, input);
    if (input && input.region) {
      this.region = new Region(input.region);
    }
  }

  private id: number;
  private sowCode: string;
  private price: number;
  private description: string;
  private documentationDay: number;
  private engineerDay: number;
  private technicianDay: number;
  private atpDay: number;
  private region: Region;

  getId(): number {
    return this.id;
  }

  setId(id: number) {
    this.id = id;
  }

  getSowCode(): string {
    return this.sowCode;
  }

  setSowCode(sowCode: string) {
    this.sowCode = sowCode;
  }

  getPrice(): number {
    return this.price;
  }

  setPrice(price: number) {
    this.price = price;
  }

  getDescription(): string {
    return this.description;
  }

  setDescription(description: string) {
    this.description = description;
  }

  getDocumentationDay(): number {
    return this.documentationDay;
  }

  setDocumentationDay(documentationDay: number) {
    this.documentationDay = documentationDay;
  }

  getEngineerDay(): number {
    return this.engineerDay;
  }

  setEngineerDay(engineerDay: number) {
    this.engineerDay = engineerDay;
  }

  getTechnicianDay(): number {
    return this.technicianDay;
  }

  setTechnicianDay(technicianDay: number) {
    this.technicianDay = technicianDay;
  }

  getRegion(): Region {
    return this.region;
  }

  setRegion(region: Region) {
    this.region = region;
  }

  getAtpDay() {
    return this.atpDay;
  }

  setAtpDay(atpDay: number) {
    this.atpDay = atpDay;
  }

}
