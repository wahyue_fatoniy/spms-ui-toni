import { SowType } from '../sow-type/sow-type';

export class Sow {
  constructor(input?: Sow) {
    Object.assign(this, input);
    if (input.sowType) {
      this.sowType = new SowType(input.sowType);
    }
  }

  private id: number;
  private sowType: SowType;

  getId(): number {
    return this.id;
  }

  setId(id: number) {
    this.id = id;
  }

  getSowType(): SowType {
    return this.sowType;
  }

  setSowType(sowType: SowType) {
    this.sowType = sowType;
  }

}
