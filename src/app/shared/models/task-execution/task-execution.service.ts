import { Injectable } from '@angular/core';
import { CoreServiceConfig, CoreService } from '../../core-service';
import { environment } from '../../../../environments/environment';
import { TaskExecution } from './task-execution';
import { HttpClient } from '@angular/common/http';

// tslint:disable-next-line:prefer-const
let CONFIG: CoreServiceConfig = {
  url: `${environment.apiUrl}/api/site_works`,
  setRow: input => new TaskExecution(input)
};


@Injectable({
  providedIn: 'root'
})
export class TaskExecutionService extends CoreService<TaskExecution> {

  constructor(http: HttpClient) {
    super(CONFIG, http);
  }

}
