import { Task } from '../task/task';
import { Resource } from '../resource/resource';

export class TaskExecution {
  constructor(input?: TaskExecution) {
    Object.assign(this, input);
    if (input && input.task) {
      this.task = new Task(input.task);
    }
    if (input) {
      this.resource = new Resource(input['resource']);
    }
  }

  private id: number;
  private manHour: number;
  private finish: boolean;
  private note: string;
  private date: string;
  private task: Task;
  private resource: Resource;

  getId(): number {
    return this.id;
  }

  setId(id: number) {
    this.id = id;
  }

  getManHour(): number {
    return this.manHour;
  }

  setManHour(manHour: number) {
    this.manHour = manHour;
  }

  getFinish(): boolean {
    return this.finish;
  }

  setFinish(finish: boolean) {
    this.finish = finish;
  }

  getNote(): string {
    return this.note;
  }

  setNote(note: string) {
    this.note = note;
  }


  getDate(): Date {
    return new Date(this.date);
  }

  setDate(date: string) {
    this.date = date;
  }

  getTask(): Task {
    return this.task;
  }

  setTask(task: Task) {
    this.task = task;
  }

  getResource(): Resource {
    return this.resource ? this.resource : new Resource();
  }

  setResource(resource: Resource) {
    this.resource = resource;
  }

  getResourcesIds(): number {
    return this.resource ? this.resource.getId() : null;
  }

}
