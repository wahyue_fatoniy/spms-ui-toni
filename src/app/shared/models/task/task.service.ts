import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { CoreServiceConfig, CoreService } from '../../core-service';
import { Task, SeverityType } from './task';
import { environment } from '../../../../environments/environment';
import { StatusType } from '../site-work/site-work';
import { throwError } from 'rxjs';
import { map, catchError } from 'rxjs/operators';

// tslint:disable-next-line:prefer-const
let CONFIG: CoreServiceConfig = {
  url: `${environment.apiUrl}/api/tasks`,
  setRow: input => new Task(input)
};


@Injectable({
  providedIn: 'root'
})
export class TaskService extends CoreService<Task> {

  constructor(http: HttpClient) {
    super(CONFIG, http);
  }

  getPriority(): Task[] {
    let done: Task[] = [], undone: Task[] = [];
    let alarm = [], normal = [];
    for (const task of  this.getTables()) {
        if (task.getStatus() === StatusType.Cancel || task.getStatus() === StatusType.Done) {
          done.push(task);
        } else {
          undone.push(task);
        }
    }
    for (const task of undone) {
       if (task.getSeverity() === SeverityType.None || task.getSeverity() === SeverityType.Normal) {
         normal.push(task);
       } else {
         alarm.push(task);
       }
    }
    return [...alarm, ...normal, ...done];
  }

  findBySiteWorkId(id: number) {
    console.log(`${CONFIG.url}/site_work/${id}`);
    this.setLoading(true);
    return this.http.get(`${CONFIG.url}/site_work/${id}`)
    .pipe(
      map((result: any) => {
        this.setLoading(false);
        const tables = result.map(_project => new Task(_project));
        this.setTables(tables);
        console.log('task', tables);
        return result;
      }),
      catchError(error => {
        this.setLoading(false);
        this.setStatusError(this.setError(error));
        return throwError(this.getError());
      })
    );
  }
}
