import { Sow } from '../sow/sow';
import { Activity } from '../activity/activity';

export enum StatusType {
  Progress = 'ON_PROGRESS',
  Done = 'DONE',
  NotStarted = 'NOT_STARTED',
  Cancel = 'CANCEL'
}

export enum SeverityType {
    CriticalI = 'CRITICAL_I',
    CriticalE = 'CRITICAL_E',
    MajorI = 'MAJOR_I',
    MajorE = 'MAJOR_E',
    Normal = 'NORMAL',
    None = 'NONE'
}

export class Task {
  constructor(input?: Task) {
    Object.assign(this, input);
    if (input && input.activity) {
      this.activity = new Activity(input.activity);
    }
    // if (input && input.siteWork) {
    //   this.siteWork = new SiteWork(input.siteWork);
    // }
    if (input && input.sow) {
      this.sow = new Sow(input.sow);
    }
    if (input && input.requirements) {
      this.requirements = input.requirements.map(requirement => new Task(requirement));
    }
  }

  private id: number;
  private status: StatusType;
  private startDate: string;
  private endDate: string;
  private severity: SeverityType;
  private sow: Sow;
  // private siteWork: SiteWork;
  private activity: Activity;
  private note: string;
  private description: string;
  private requirements: Task[];

  getId(): number {
    return this.id;
  }

  setId(id: number) {
    this.id = id;
  }

  getStatus(): StatusType {
    return this.status;
  }

  setStatus(status: StatusType) {
    this.status = status;
  }

  getStartDate(): Date {
    return new Date(this.startDate);
  }

  setStartDate(startDate: string) {
    this.startDate = startDate;
  }

  getEndDate(): Date {
    return new Date(this.endDate);
  }

  setEndDate(endDate: string) {
    this.endDate = endDate;
  }

  getSeverity(): SeverityType {
    return this.severity;
  }

  setSeverity(severity: SeverityType) {
    this.severity = severity;
  }

  getSow(): Sow {
    return this.sow;
  }

  setSow(sow: Sow) {
    this.sow = sow;
  }

  // getSiteWork(): SiteWork {
  //   return this.siteWork;
  // }

  // setSiteWork(siteWork: SiteWork) {
  //   this.siteWork = siteWork;
  // }

  getActivity(): Activity {
    return this.activity;
  }

  setActivity(activity: Activity) {
    this.activity = activity;
  }

  getNote(): string {
    return this.note;
  }

  setNote(note: string) {
    this.note = note;
  }

  getDescription() {
    return this.description;
  }

  setDescription(desc: string) {
    this.description = desc;
  }


  public getRequirements() {
    return this.requirements;
  }

  public setRequirements(tasks: Task[]) {
    this.requirements = tasks;
  }


  public isPreceededBy(prevTask: Task): boolean {
    let retVal = false;
    if (this.activity.getType() === prevTask.getActivity().getType() &&
      this.sow.getId() === prevTask.getSow().getId()
      ) {
      if (this.activity.getNumber() === prevTask.getActivity().getNumber() + 1) {
        retVal = true;
      }
    }
    return retVal;
  }


  isPredecessor(nextTask: Task): boolean {
    let isVal = false;
    if (
      this.sow.getId() === nextTask.getSow().getId() &&
      this.activity.getType() === nextTask.getActivity().getType()) {
        if ( this.activity.getNumber() === (nextTask.getActivity().getNumber() - 1)) {
          isVal = true;
        }
      }
      return isVal;
  }

  public isFinished(): boolean {
    let retVal = false;
    if (this.status === "DONE") {
      retVal = true;
    }
    return retVal;
  }

  public isInitial(): boolean {
    let retVal = false;
    if (this.getRequirements().length === 0 && !this.isFinished()) {
      retVal = true;
    }
    return retVal;
  }

  public isReady(): boolean {
    let retVal = false;
    let i = 0;
    if (!this.isInitial() && !this.isFinished()) {
      this.requirements.forEach((requirement: Task) => {
        if (requirement.getStatus() === StatusType.Done) {
          i = i + 1;
        }
      });
      if (i === this.requirements.length) {
        retVal = true;
      }
    }
    return retVal;
  }

  public isRequirement(taskRequirement: Task) {
    let retVal = false;
    if (
      this.requirements.findIndex(
        task => task.getId() === taskRequirement.getId()
      ) !== -1
    ) {
      retVal = true;
    }
    return retVal;
  }
}

export class TaskPlan extends Task {
  private planStatus: StatusType;
  constructor(task?: Task) {
    if (task) {
      super(task);
      this.planStatus = task.getStatus();
    }
  }

  public getPlanStatus() {
    return this.planStatus;
  }

  public setPlanStatus(status: StatusType) {
    this.planStatus = status;
  }

}
