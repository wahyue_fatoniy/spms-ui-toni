import { Engineer } from '../engineer/engineer';
import { Documentation } from '../documentation/documentation';
import { Technician } from '../technician/technician';
import { Cordinator } from '../cordinator/cordinator';
import { Resource } from '../resource/resource';

export class Team {
  constructor(input?: Team) {
    Object.assign(this, input);
    if (input && input.resources) {
      this.resources = input.resources.map(_set => new Resource(_set));
    }
    if (input && input.cordinator) {
      this.cordinator = new Cordinator(input.cordinator);
    }
  }

  private id;
  private cordinator: Cordinator;
  private resources: Resource[];

  getId(): number {
    return this.id;
  }

  setId(id: number) {
    this.id = id;
  }

  getCordinator(): Cordinator {
    return this.cordinator;
  }

  setCordinator(cordinator: Cordinator) {
    this.cordinator = cordinator;
  }

  getResources(): Resource[] {
    return this.resources;
  }

  setResources(resources: Resource[]) {
    this.resources = resources;
  }

}
