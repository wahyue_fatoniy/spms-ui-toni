import { Injectable } from '@angular/core';
import { CoreService, CoreServiceConfig } from '../../core-service';
import { User } from './user';
import { environment } from '../../../../environments/environment';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { Role } from '../role/role';

// tslint:disable-next-line:prefer-const
let CONFIG: CoreServiceConfig = {
  url: `${environment.apiUrl}/api/users`,
  setRow: input => new User(input)
};

@Injectable({
  providedIn: 'root'
})
export class UserService extends CoreService<User> {

  constructor(http: HttpClient) {
    super(CONFIG, http);
  }

  findRolesByEmail(email: string): Observable<Role[]> {
    return this.http.post(`${CONFIG.url}/roles`, {email: email})
    .pipe(
      map((_roles: Role[]) => {
        return _roles.map(_set => new Role(_set));
      })
    );
  }

}
