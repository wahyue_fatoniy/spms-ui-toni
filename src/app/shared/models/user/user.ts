import { Role } from '../role/role';

export class User extends Role {

  private email: string;
  private username: string;
  private password: string;

  constructor(input?: User) {
    if (input) {
      super(input);
      this.email = input['email'];
      this.username = input['username'];
      this.password = input['password'];
    }
  }

  getEmail(): string {
    return this.email;
  }

  setEmail(email: string) {
    this.email = email;
  }

  getUsername(): string {
    return this.username;
  }

  setUsername(username: string) {
    this.username = username;
  }

  getPassword(): string {
    return this.password;
  }

  setPassword(password: string) {
    this.password = password;
  }

}
