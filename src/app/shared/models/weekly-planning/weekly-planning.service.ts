import { Injectable } from '@angular/core';
import { CoreServiceConfig, CoreService } from '../../core-service';
import { WeeklyPlanning, ValidType } from './weekly-planning';
import { environment } from '../../../../environments/environment';
import { HttpClient } from '@angular/common/http';
import { map, catchError } from 'rxjs/operators';
import { throwError } from 'rxjs';
import { Task } from '../task/task';
import { StatusType } from '../site-work/site-work';

// tslint:disable-next-line:prefer-const
let CONFIG: CoreServiceConfig = {
  url: `${environment.apiUrl}/api/weekly_plannings`,
  setRow: input => new WeeklyPlanning(input)
};

@Injectable({
  providedIn: 'root'
})
export class WeeklyPlanningService extends CoreService<WeeklyPlanning> {

  constructor(http: HttpClient) {
    super(CONFIG, http);
  }

  submit(id: number) {
    this.setLoading(true);
    return this.http.post(`${CONFIG.url}/submit/${id}`, {})
    .pipe(
      map((result: WeeklyPlanning) => {
        this.setLoading(false);
        this.editTables(id, new WeeklyPlanning(result));
        console.log('weekly planning', new WeeklyPlanning(result));
        return result;
      }),
      catchError(error => {
        this.setLoading(false);
        this.setStatusError(this.setError(error));
        return throwError(this.getError());
      })
    );
  }

  findBySiteWorkId(id: number) {
    this.setLoading(true);
    return this.http.get(`${CONFIG.url}/site_work/${id}`)
    .pipe(
      map((result: any) => {
        this.setLoading(false);
        const tables = result.map(_project => new WeeklyPlanning(_project));
        this.setTables(tables);
        console.log('weekly planning', tables);
        return result;
      }),
      catchError(error => {
        this.setLoading(false);
        this.setStatusError(this.setError(error));
        return throwError(this.getError());
      })
    );
  }

  getPriority(): WeeklyPlanning[] {
    return this.getTables().sort((a, b) => b.getStartDate().getTime() - a.getStartDate().getTime());
  }

  getValidTasks(): Task[] {
    let tasks = [];
    // mencari semua tasks yang ada di weekly planning yang masih berlaku
    for (const planning of this.getTables()) {
      if (planning.getStatus() === ValidType.Valid && planning.getSubmit() === true) {
        tasks = [...tasks, ...planning.getTasks()
          .filter(_task => _task.getStatus() === StatusType.NotStarted || _task.getStatus() === StatusType.Progress)
        ];
      }
    }
    return tasks;
  }

  getTasksByIds(ids: number[]): Task[] {
    let tasks = [];
    if (ids) {
      for (const id  of ids) {
        const index = this.getValidTasks().findIndex(_set => _set.getId() === id);
        if (index !== -1) {
          tasks.push(this.getValidTasks()[index]);
        }
      }
    }
    return tasks;
  }
}
