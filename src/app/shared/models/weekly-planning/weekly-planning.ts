import { Task } from '../task/task';
import { Documentation } from '../documentation/documentation';
import { Engineer } from '../engineer/engineer';
import { Technician } from '../technician/technician';
import { SiteWork } from '../site-work/site-work';
import { Cordinator } from '../cordinator/cordinator';

export enum ValidType {
  Idle = 'Belum Berlaku',
  Valid = 'Masih Berlaku',
  Invalid = 'Tidak Berlaku'
}

export class WeeklyPlanning {
  constructor(input?: WeeklyPlanning) {
    Object.assign(this, input);
    if (input.tasks) {
      this.tasks = input.tasks.map(task => new Task(task));
    }
    if (input.siteWork) {
      this.siteWork = new SiteWork(input.siteWork);
    }
  }

  private id: number;
  private startDate: string;
  private endDate: string;
  private tasks: Task[];
  private siteWork: SiteWork;
  private operational: number;
  private submit: boolean;


  getId(): number {
    return this.id;
  }

  setId(id: number) {
    this.id = id;
  }

  getStartDate(): Date {
    return new Date(this.startDate);
  }

  setStartDate(startDate: string) {
    this.startDate = startDate;
  }

  getEndDate(): Date {
    return new Date(this.endDate);
  }

  setEndDate(endDate: string) {
    this.endDate = endDate;
  }

  getTasks(): Task[] {
    return this.tasks;
  }

  setTasks(tasks: Task[]) {
    this.tasks = tasks;
  }

  getSiteWork(): SiteWork {
    return this.siteWork;
  }

  setSiteWork(siteWork: SiteWork) {
    this.siteWork = siteWork;
  }

  getOperational(): number {
    return this.operational;
  }

  setOperational(operational: number) {
    this.operational = operational;
  }

  getSubmit(): boolean {
    return this.submit;
  }

  setSubmit(submit: boolean) {
    this.submit = submit;
  }

  getStatus(): ValidType {
    // const start = this.getStartDate();
    // const end = this.getEndDate();
    // const now = new Date();
    // let valid: ValidType;
    // if (now.getTime() > start.getTime() && now.getTime() > end.getTime()) {
    //     valid = ValidType.Invalid;
    // } else if (now.getTime() > start.getTime() && now.getTime() < end.getTime()) {
    //     valid = ValidType.Valid;
    // } else if (now.getTime() < start.getTime() && now.getTime() < end.getTime()) {
    //     valid = ValidType.Idle;
    // }
    return ValidType.Valid;
  }

}
