import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'normalText'
})
export class NormalTextPipe implements PipeTransform {

  transform(keyword?: string): string {
    const result = keyword ? keyword.replace(/\s/g, '').toLowerCase() : '';
    return result;
  }

}
