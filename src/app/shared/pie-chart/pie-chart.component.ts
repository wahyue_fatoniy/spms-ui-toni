import { Component, OnInit, Input, ChangeDetectionStrategy, OnChanges, NgZone, SimpleChanges } from '@angular/core';

interface PieChartData {
  name: string;
  value: number;
}

export interface PieChartOptions {
  view: number[];
  data: PieChartData[];
  animation: boolean;
  labels: boolean;
  legend: boolean;
  legendTitle: string;
  legendPosition: string;
}


@Component({
  selector: 'app-pie-chart',
  templateUrl: './pie-chart.component.html',
  styleUrls: ['./pie-chart.component.css'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class PieChartComponent implements OnInit, OnChanges {
  @Input() view: number[];
  @Input() data: PieChartData[];
  @Input() animation: boolean;
  @Input() labels: boolean;
  @Input() legend: boolean;
  @Input() legendTitle: string;
  @Input() legendPosition: string;
  @Input() doughnut = false;
  @Input() gradient = true;
  @Input() explodeSlices = false;

  constructor(
    public ngZone: NgZone
  ) { }

  ngOnInit() {
  }

  ngOnChanges(changes: SimpleChanges) {
    this.ngZone.run(() => {
      this.data = this.data.slice();
    });
  }

}
