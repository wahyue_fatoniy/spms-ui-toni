import { Component, OnInit, Output, EventEmitter, Input, ChangeDetectionStrategy } from '@angular/core';
import { NgxDrpOptions, PresetItem } from 'ngx-mat-daterange-picker';

export interface RangeDate {
  fromDate: Date;
  toDate: Date;
}

@Component({
  selector: 'app-range-date',
  templateUrl: './range-date.component.html',
  styleUrls: ['./range-date.component.css'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class RangeDateComponent implements OnInit {
  @Input() range: RangeDate = {
    fromDate: new Date(),
    toDate: new Date(
      new Date().setDate(new Date().getDate() - new Date().getDay() + 30)
    )
  };
  options: NgxDrpOptions;
  @Output() OnSelectDate = new EventEmitter();

  constructor() { }

  ngOnInit() {
    this.dateRangeOption();
  }

  public dateRangeOption(): void {
    const backDate = numOfDays => {
      const today = new Date();
      return new Date(today.setDate(today.getDate() - numOfDays));
    };

    const presets: Array<PresetItem> = [
      {
        presetLabel: 'Last 7 Days',
        range: { fromDate: backDate(7), toDate: new Date() }
      },
      {
        presetLabel: 'Last 30 Days',
        range: { fromDate: backDate(30), toDate: new Date() }
      }
    ];

    this.options = {
      presets: presets,
      format: 'longDate',
      range: this.range,
      applyLabel: 'Submit',
      calendarOverlayConfig: {
        shouldCloseOnBackdropClick: true
      },
      cancelLabel: 'Cancel',
      placeholder: 'Choose Date'
    };
  }

  public updateRange(val: Range): void {
    this.OnSelectDate.emit(val);
  }

}
