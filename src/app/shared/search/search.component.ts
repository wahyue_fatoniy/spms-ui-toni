import {
  Component,
  OnInit,
  OnDestroy,
  Input,
  OnChanges,
  forwardRef
} from '@angular/core';
import {
  FormControl,
  ControlValueAccessor,
  NG_VALUE_ACCESSOR
} from '@angular/forms';
import { Subject } from 'rxjs';
import { takeUntil, filter, debounceTime } from 'rxjs/operators';

@Component({
  selector: 'app-search',
  template: `
    <mat-form-field>
      <mat-label>Search</mat-label>
      <input matInput [formControl]='model' [matAutocomplete]='auto' />
      <mat-autocomplete #auto='matAutocomplete'>
        <mat-option *ngFor='let value of autocompletes' [value]='value'>
          {{ value }}
        </mat-option>
      </mat-autocomplete>
      <span matPrefix>
      </span>
    </mat-form-field>
  `,
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => SearchComponent),
      multi: true
    },
  ]
})
export class SearchComponent
  implements OnInit, OnChanges, OnDestroy, ControlValueAccessor {
  constructor() {}
  model: FormControl = new FormControl();
  subscriber: Subject<boolean> = new Subject<boolean>();
  autocompletes: string[] = [];
  @Input() typeFilter: string;
  @Input() data: any[] = [];
  onChanges(key: string) {}

  ngOnDestroy() {
    this.subscriber.next(true);
    this.subscriber.complete();
  }

  ngOnChanges() {
    if (this.typeFilter) {
      this.model.setValue(null);
    }
  }

  ngOnInit() {
    this.model.valueChanges
      .pipe(
        takeUntil(this.subscriber),
        filter(
          _key => this.typeFilter !== null || this.typeFilter !== undefined
        ),
        debounceTime(300)
      )
      .subscribe(_key => {
        this.autocompletes = [];
        if (_key && this.data.length !== 0 && this.typeFilter !== 'all') {
          this.autocompletes = this.data
            .map(_value => _value[this.typeFilter])
            .filter(_value => {
              const key = _key
                ? _key
                    .split(' ')
                    .join('')
                    .toLowerCase()
                : '';
              const value = _value
                ? _value
                    .toString()
                    .split(' ')
                    .join('')
                    .toLowerCase()
                : '';
              return value.indexOf(key) !== -1;
            });
        }
        this.onChanges(_key);
      });
  }

  writeValue(key: string): void {
    if (key) {
      this.model.setValue(key);
    }
  }

  registerOnChange(fn: any): void {
    this.onChanges = fn;
  }

  registerOnTouched(fn: any): void {}

  setDisabledState(isDisabled: boolean): void {
    this.model.reset({ value: this.model.value, isDisabled });
  }
}
