import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'selectSearch'
})
export class SelectSearchPipe implements PipeTransform {

  private normalText(word: string): string {
    return word ? word.toLowerCase().split(' ').join('') : '';
  }

  transform(data: Array<any>, keyword: string): Array<any> {
    return data.filter(_set => {
      const value  = JSON.stringify(_set);
      return keyword && keyword !== '' ? this.normalText(value).indexOf(this.normalText(keyword)) !== -1 : true;
    });
  }

}
