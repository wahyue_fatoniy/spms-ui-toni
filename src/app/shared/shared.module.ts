import { NgModule } from '@angular/core';
import { SearchComponent } from './search/search.component';
import { TypeFilterComponent } from './type-filter/type-filter.component';
import { MaterialModule } from '../material.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { StatusCellComponent } from './cell/status-cell/status-cell.component';
import { ProgressCellComponent } from './cell/progress-cell/progress-cell.component';
import { FormlyModule } from '@ngx-formly/core';
import { FormlyMaterialModule } from '@ngx-formly/material';
import { FormlyMatDatepickerModule } from '@ngx-formly/material/datepicker';
import { MatNativeDateModule } from '@angular/material/core';
import { CommonModule } from '@angular/common';
import { PipesModule } from '../pipes/pipes.module';
import { WidgetComponentComponent } from './widget-component/widget-component.component';
import { PieChartComponent } from './pie-chart/pie-chart.component';
import { RangeDateComponent } from './range-date/range-date.component';
import { NgxChartsModule } from '@swimlane/ngx-charts';
import { NgxMatDrpModule } from 'ngx-mat-daterange-picker';
import { SubmitCellComponent } from './cell/submit-cell/submit-cell.component';
import { SeverityCellComponent } from './cell/severity-cell/severity-cell.component';
import { PlanningValidCellComponent } from './cell/planning-valid-cell/planning-valid-cell.component';
import { MatSelectSearchComponent } from '../template/mat-select-search/mat-select-search.component';
import { NgxMatSelectSearchModule } from 'ngx-mat-select-search';
import { ProjectTableComponent } from '../components/project-table/project-table.component';
import { FormListComponent } from '../components/form-list/form-list.component';
import { TableDocumentComponent } from '../components/form-list/table-document/table-document.component';
import { TableViewComponentComponent } from '../components/table-view-component/table-view-component.component';
import { FormRangeDateComponent } from '../components/form-range-date/form-range-date.component';
import { CellTaskTypeComponent } from '../components/cell-task-type/cell-task-type.component';
import { CardDescriptionComponent } from '../components/card-description/card-description.component';
import { ToastrService, ToastrModule } from 'ngx-toastr';

@NgModule({
  imports: [
    CommonModule,
    MaterialModule,
    FormsModule,
    ReactiveFormsModule,
    FormlyModule.forRoot(
      {
        validationMessages: [
          { name: 'required', message: 'This field is required' },
        ],
      }
    ),
    FormlyMaterialModule,
    FormlyMatDatepickerModule,
    MatNativeDateModule,
    PipesModule,
    NgxChartsModule,
    NgxMatDrpModule,
    NgxMatSelectSearchModule,
    ToastrModule.forRoot({
      timeOut: 8000,
    }),
  ],
  declarations: [
    SearchComponent,
    TypeFilterComponent,
    StatusCellComponent,
    ProgressCellComponent,
    WidgetComponentComponent,
    PieChartComponent,
    RangeDateComponent,
    SeverityCellComponent,
    SubmitCellComponent,
    PlanningValidCellComponent,
    MatSelectSearchComponent,
    ProjectTableComponent,
    FormListComponent,
    TableDocumentComponent,
    TableViewComponentComponent,
    FormRangeDateComponent,
    CellTaskTypeComponent,
    CardDescriptionComponent
  ],
  exports: [
    MaterialModule,
    FormsModule,
    ReactiveFormsModule,
    PipesModule,
    NgxChartsModule,
    NgxMatDrpModule,
    NgxMatSelectSearchModule,

    SearchComponent,
    TypeFilterComponent,
    StatusCellComponent,
    ProgressCellComponent,
    WidgetComponentComponent,
    PieChartComponent,
    RangeDateComponent,
    SeverityCellComponent,
    ProgressCellComponent,
    SubmitCellComponent,
    MatSelectSearchComponent,
    FormlyModule,
    FormlyMaterialModule,
    FormlyMatDatepickerModule,
    MatNativeDateModule,
    PlanningValidCellComponent,
    SeverityCellComponent,


    FormListComponent,
    TableDocumentComponent,
    TableViewComponentComponent,
    FormRangeDateComponent,
    CellTaskTypeComponent,
    CardDescriptionComponent,
    ToastrModule,
  ],
  providers: [
    ToastrService
  ]
})
export class SharedModule { }
