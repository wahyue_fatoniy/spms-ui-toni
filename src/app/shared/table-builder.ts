import { MatTableDataSource, MatSort, MatPaginator } from "@angular/material";
import { FormControl } from "@angular/forms";
import { Subject } from "rxjs";
import { debounceTime, takeUntil } from "rxjs/operators";
import { EventEmitter } from '@angular/core';

export interface TypeFilter {
    key: string;
    label: string;
}

interface Config {
    key?: string;
    label: string;
}

interface ArrayObjectType extends Config {
    columnType: ColumnType.arrayObject;
    config: { key: string };
}

interface ArrayStringType extends Config {
    columnType: ColumnType.arrayString;
    config: string[];
}

interface ObjectType extends Config {
    columnType: ColumnType.object;
    objectKey: string;
}

interface StringType extends Config {
    columnType: ColumnType.string;
}

interface ProgressBarType extends Config {
    columnType: ColumnType.progressBar;
}

interface DateType extends Config {
    columnType: ColumnType.date;
    format:
    | "longDate"
    | "shortDate"
    | "mediumDate"
    | "long"
    | "short"
    | "medium"
    | "full"
    | "fullDate"
    | "shortTime"
    | "mediumTIme"
    | "longTime"
    | "fullTime";
}

interface CurrencyType extends Config {
    columnType: ColumnType.currency;
    local: "Rp. " | "$ ";
}

interface HoverType extends Config {
    columnType: ColumnType.hover;
    icon?: string;
}

interface StatusType extends Config {
    columnType: ColumnType.status;
}


interface SeverityType extends Config {
    columnType: ColumnType.severity;
}

interface ActionType extends Config {
    columnType: ColumnType.action;
}

export type actionType = "ADD" | "DELETE" | "UPDATE" | "ACKNOWLEDGE" | "SUBMIT" | 'HOVER' | 'ACCEPT' | 'REJECT' | 'REQUEST' | 'VIEW';

export interface OnEvent {
    value: any;
    type: actionType;
}

export enum ColumnType {
    arrayObject = "arrayObject",
    arrayString = "arrayString",
    object = "object",
    string = "string",
    date = "date",
    currency = "currency",
    hover = 'hover',
    progressBar = 'progressBar',
    action = "action",
    status = "status",
    severity = "severity",
}

export type ColumnConfig =
    | ArrayObjectType
    | ArrayStringType
    | ObjectType
    | StringType
    | DateType
    | CurrencyType
    | ActionType
    | HoverType
    | ProgressBarType
    | SeverityType
    | StatusType;

export class TableBuilder {
    private _filterControlTypes: FormControl = new FormControl("all");
    private _searchControl: FormControl = new FormControl();
    private _datasource: MatTableDataSource<any>;
    private _onEvent: EventEmitter<OnEvent>;
    private _subscription: Subject<boolean>;
    private _columnConfigs: ColumnConfig[];
    private _displayedColumns: string[];
    private _filterTypes: TypeFilter[];
    private _pageSizeOptions: number[];
    private _actionTypes: actionType[];
    private _pageIndex: number;
    private _pageSize: number;
    private _title: string;

    constructor() {
        const pageSize = localStorage.getItem("pageSize");
        this._pageSize = pageSize ? parseInt(pageSize) : 10;
        this._datasource = new MatTableDataSource([]);
        this._pageSizeOptions = [5, 10, 25, 100];
        this._subscription = new Subject();
        this._columnConfigs = [];
        this._filterTypes = [];
        this._actionTypes = [];
        this._pageIndex = 0;
        this.setFilter();
    }

    public get typeFilters(): TypeFilter[] {
        return this._filterTypes;
    }

    public get filterControlTypes(): FormControl {
        return this._filterControlTypes;
    }

    public get datasource() {
        return this._datasource;
    }

    public get displayedColumns(): string[] {
        return this._displayedColumns;
    }

    public get pageSize() {
        return this._pageSize;
    }

    public get pageSizeOptions() {
        return this._pageSizeOptions;
    }

    public get pageIndex() {
        return this._pageIndex;
    }

    public get title() {
        return this._title;
    }

    public get searchControl() {
        return this._searchControl;
    }

    public get subscription() {
        return this._subscription;
    }

    public get columnConfigs() {
        return this._columnConfigs;
    }

    public get actionTypes() {
        return this._actionTypes;
    }

    public get onEvent() {
        return this._onEvent;
    }

    public set datasource(datasource: MatTableDataSource<any>) {
        this.datasource = datasource;
    }

    public set columnConfigs(columnConfigs: ColumnConfig[]) {
        this._columnConfigs = columnConfigs;
    }

    public set sort(sort: MatSort) {
        this._datasource.sort = sort;
    }

    public set paginator(paginator: MatPaginator) {
        this._datasource.paginator = paginator;
    }

    public set subscription(subscription: Subject<boolean>) {
        this._subscription = subscription;
    }

    public set title(title: string) {
        this._title = title;
    }

    public set pageIndex(index: number) {
        this._pageIndex = index;
    }

    public set pageSize(size: number) {
        localStorage.setItem("pageSize", size.toString());
        this._pageSize = size;
    }

    public set pageSizeOptions(pageSize: number[]) {
        this._pageSizeOptions = pageSize;
    }

    public set actionTypes(types: actionType[]) {
        this._actionTypes = types;
    }

    public set typeFilters(value: TypeFilter[]) {
        this._filterTypes = value;
    }

    public set onEvent(emitter: EventEmitter<OnEvent>) {
        this._onEvent = emitter;
    }

    public setDisplayedColumns() {
        this._displayedColumns = this.columnConfigs.map(column => {
            return column.key;
        });
    }

    public setTypeFilters() {
        this.typeFilters = this.columnConfigs
            .filter(type => {
                if (type.key !== 'no' && type.key !== 'action') {
                    return type;
                }
            })
            .map(column => {
                return { key: column.key, label: column.label };
            });
    }

    public setColumnActions() {
        this.actionTypes.forEach(type => {
            if (type !== 'ADD') {
                this.columnConfigs = [
                    ...this.columnConfigs,
                    { label: 'Action', key: 'action', columnType: ColumnType.action }
                ]
            }
        });
        this.columnConfigs = this.columnConfigs.filter((config, i, arr) => {
            return i === arr.findIndex(val => val.key === config.key)
        });
    }

    private filterPredicate(
        data: any,
        keyword: string,
        typeFilter: string,
    ): boolean {
        let result;
        if (typeFilter === "all") {
            result = JSON.stringify(data);
        } else {
            result = data[typeFilter];
        }
        result = result
            ? result
                .toString()
                .replace(/\s/g, "")
                .toLowerCase()
            : "";
        keyword = keyword
            ? keyword
                .toString()
                .replace(/\s/g, "")
                .toLowerCase()
            : "";
        return result.indexOf(keyword) !== -1;
    }

    private setFilter(): void {
        this.searchControl.valueChanges
            .pipe(
                takeUntil(this.subscription),
                debounceTime(300)
            )
            .subscribe(keyword => {
                this._datasource.filter = keyword;
            });
        this._datasource.filterPredicate = (data, keyword) => {
            return this.filterPredicate(data, keyword, this._filterControlTypes.value);
        };
    }

}
