import { Component, OnInit, Input, OnDestroy, forwardRef } from '@angular/core';
import {
  FormControl,
  NG_VALUE_ACCESSOR,
  ControlValueAccessor,
} from '@angular/forms';
import { Subject } from 'rxjs';
import { takeUntil, filter, distinctUntilChanged } from 'rxjs/operators';

export interface TypeFilter {
  key: string;
  label: string;
}

@Component({
  selector: 'app-type-filter',
  template: `
    <mat-form-field *ngIf='typeFilters'>
      <mat-label>Type Filter</mat-label>
      <mat-select [formControl]="model">
        <mat-option value="all">All</mat-option>
        <mat-option
          *ngFor="let filter of typeFilters; let i = index"
          [value]="filter.key"
        >
          {{ filter.label }}
        </mat-option>
      </mat-select>
      <div matPrefix>
      </div>
    </mat-form-field>
  `,
  styles: [],
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => TypeFilterComponent),
      multi: true
    }
  ]
})
export class TypeFilterComponent
  implements OnInit, OnDestroy, ControlValueAccessor {
  constructor() {}

  model: FormControl = new FormControl();
  subsriber: Subject<boolean> = new Subject<boolean>();
  @Input() typeFilters: TypeFilter[];
  onChanges(key: string) {}

  ngOnInit() {
    this.model.valueChanges
      .pipe(
        takeUntil(this.subsriber),
        filter(_key => _key !== null)
      )
      .pipe(distinctUntilChanged())
      .subscribe(_key => {
        this.onChanges(_key);
      });
  }

  ngOnDestroy() {
    this.subsriber.next(true);
    this.subsriber.complete();
  }

  writeValue(key: string): void {
    if (key) {
      this.model.setValue(key);
    }
  }

  registerOnChange(fn: any): void {
    this.onChanges = fn;
  }

  registerOnTouched(fn: any): void {}

  setDisabledState(isDisabled: boolean): void {
    this.model.reset({ value: this.model.value, isDisabled });
  }
}
