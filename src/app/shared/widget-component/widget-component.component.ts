import { Component, OnInit, Input, ChangeDetectionStrategy, EventEmitter, Output, OnChanges } from '@angular/core';

export interface WidgetData {
  title: string;
  value: any;
  iconColor: string;
  icon: string;
}

@Component({
  selector: 'app-widget-component',
  templateUrl: './widget-component.component.html',
  styleUrls: ['./widget-component.component.css'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class WidgetComponentComponent implements OnInit, OnChanges {
  @Input() data: any
  @Input() title: string;
  @Input() iconColor: string;
  @Input() icon: string;

  date = new Date();
  constructor() { }
  @Output() OnClickIcon = new EventEmitter<string>();

  ngOnInit() { }

  ngOnChanges() {
  }

  onClick(title: string) {
    this.OnClickIcon.emit(title);
  }

}
