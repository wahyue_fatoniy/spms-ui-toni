import { Component, OnInit, Inject } from '@angular/core';
import { Site } from 'src/app/shared/models/site/site';
import { AreaService } from 'src/app/shared/models/area/area.service';
import { RegionService } from 'src/app/shared/models/region/region.service';
import { FormControl, Validators } from '@angular/forms';
import { MAT_DIALOG_DATA } from '@angular/material';
import { Region } from 'src/app/shared/models/region/region';
import { Area } from 'src/app/shared/models/area/area';
import { AAAService } from 'src/app/shared/aaa/aaa.service';

interface SiteFormV2 {
  value: Site;
  area: AreaService;
  region: RegionService;
}

@Component({
  selector: 'app-site-form-v2',
  templateUrl: './site-form-v2.component.html',
  styles: []
})
export class SiteFormV2Component implements OnInit {

  constructor(@Inject(MAT_DIALOG_DATA) public data: SiteFormV2, private aaa: AAAService) { }

  siteCode: FormControl = new FormControl(this.data.value.getSiteCode(), [Validators.required]);
  name: FormControl = new FormControl(this.data.value.getName(), [Validators.required]);
  latitude: FormControl = new FormControl(this.data.value.getLatitude(), [Validators.required]);
  longitude: FormControl = new FormControl(this.data.value.getLongitude(), [Validators.required]);
  area: FormControl = new FormControl(this.data.value.getArea().getId(), [Validators.required]);
  region: FormControl = new FormControl({
    value : this.data.value.getRegion().getId(),
    disabled : this.aaa.isAuthorized('read:region:{province:$}')
  }, [Validators.required]);

  ngOnInit() {
    this.siteCode.valueChanges.subscribe(_set => this.data.value.setSiteCode(_set));
    this.name.valueChanges.subscribe(_set => this.data.value.setName(_set));
    this.latitude.valueChanges.subscribe(_set => this.data.value.setLatitude(_set));
    this.longitude.valueChanges.subscribe(_set => this.data.value.setLongitude(_set));
    this.area.valueChanges.subscribe(_set => {
      if (_set) {
        this.data.value.setArea(this.data.area.getById(_set));
      }
    });
    this.region.valueChanges.subscribe(_set => {
      if (_set) {
        this.area.setValue(null);
        this.data.value.setRegion(this.data.region.getById(_set));
      }
    });
  }

  invalid(): boolean {
    return this.name.invalid || this.latitude.invalid || this.longitude.invalid || this.area.invalid ||
    this.area.invalid || this.siteCode.invalid;
  }

  getAreas(): Area[] {
    return this.region.value ? this.data.area.getByRegionId(this.region.value) : this.data.area.getTables();
  }

  getRegions(): Region[] {
    return this.data.region.getTables();
  }

}
