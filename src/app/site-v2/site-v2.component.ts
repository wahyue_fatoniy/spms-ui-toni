import { Component, OnInit, ViewChild } from "@angular/core";
import { TableConfig, CoreTable } from "../shared/core-table";
import { Site } from "../shared/models/site/site";
import { AreaService } from "../shared/models/area/area.service";
import { RegionService } from "../shared/models/region/region.service";
import { AAAService } from "../shared/aaa/aaa.service";
import { SiteService } from "../shared/models/site/site.service";
import { forkJoin } from "rxjs";
import { RoleType } from "../shared/models/role/role";
import { Region } from "../shared/models/region/region";
import { MatSort, MatPaginator, Sort, MatDialog } from "@angular/material";
import { SiteFormV2Component } from "./site-form-v2/site-form-v2.component";
import { Area } from '../shared/models/area/area';
import { DialogComponent } from '../shared/dialog/dialog.component';

const CONFIG: TableConfig = {
  title: `Site`,
  columns: [
    "no",
    "siteCode",
    "name",
    "latitude",
    "longitude",
    "area",
    "region"
  ],
  typeFilters: [
    { value: "all", label: "All" },
    { value: "siteCode", label: "Site Code" },
    { value: "name", label: "Name" },
    { value: "latitude", label: "Latitude" },
    { value: "longitude", label: "Longitude" },
    { value: "area", label: "Area" },
    { value: "region", label: "Region" }
  ]
};

@Component({
  selector: "app-site-v2",
  templateUrl: "./site-v2.component.html",
  styles: []
})
export class SiteV2Component extends CoreTable<Site> implements OnInit {
  constructor(
    private area: AreaService,
    private region: RegionService,
    public site: SiteService,
    private aaa: AAAService,
    private dialog: MatDialog
  ) {
    super(CONFIG);
  }

  @ViewChild(MatPaginator)
  paginator: MatPaginator;
  @ViewChild(MatSort)
  sort: MatSort;

  getRegion(row: Site): string {
    return row.getRegion().getName();
  }

  getArea(row: Site): string {
    return row.getArea().getName();
  }

  getAuthRegions(): Region[] {
    const regions = this.region.getTables().filter(_set => {
      return (
        !this.aaa.isAuthorized("read:region:{province:$}") ||
        this.aaa.isAuthorized(`read:region:{province:${_set.getId()}}`)
      );
    });
    console.log("Regions Authority", regions);
    return regions;
  }

  getLoading(): boolean {
    return (
      this.site.getLoading() ||
      this.area.getLoading() ||
      this.region.getLoading()
    );
  }

  ngOnInit() {
    this.initialize();
    this.region.findAll().subscribe(_success => {
      forkJoin(
        this.area.findByRegions(this.getAuthRegions()),
        this.site.findByRegions(this.getAuthRegions())
      ).subscribe(_set => (this.datasource.data = this.site.getTables()));
    });
  }

  initialize() {
    this.action = this.aaa.isAuthorized("write:site");
    this.enabledAction();
    this.datasource.paginator = this.paginator;
    this.datasource.sort = this.sort;
    this.datasource.filterPredicate = (set: Site, keyword: string) => {
      const value = {
        siteCode: set.getSiteCode(),
        name: set.getName(),
        latitude: set.getLatitude(),
        longitude: set.getLongitude(),
        area: this.getArea(set),
        region: this.getRegion(set)
      };
      return this.filterPredicate(value, keyword, this.typeFilter);
    };
    this.search.valueChanges.subscribe(
      _search => (this.datasource.filter = _search)
    );
  }

  getAutocomplete(): string[] {
    // tslint:disable-next-line:prefer-const
    let data = {
      all: [],
      siteCode: [],
      name: [],
      latitude: [],
      longitude: [],
      area: [],
      region: []
    };
    this.site.getTables().forEach(set => {
      data.siteCode.push(set.getSiteCode());
      data.name.push(set.getName());
      data.latitude.push(set.getLatitude());
      data.longitude.push(set.getLongitude());
      data.area.push(this.getArea(set));
      data.region.push(this.getRegion(set));
    });
    return this.exist(data[this.typeFilter]).filter(_set => {
      const keyword = this.text.transform(this.search.value);
      const result = this.text.transform(_set);
      return keyword ? result.indexOf(keyword) !== -1 : true;
    });
  }

  sortData(sort: Sort) {
    if (!sort.active || sort.direction === "") {
      this.datasource.data = this.site.getTables();
      return;
    }
    this.datasource.data = this.datasource.data.sort((a, b) => {
      const isAsc = sort.direction === "asc";
      switch (sort.active) {
        case "siteCode":
          return this.compare(a.getSiteCode(), b.getSiteCode(), isAsc);
        case "name":
          return this.compare(a.getName(), b.getName(), isAsc);
        case "latitude":
          return this.compare(a.getLatitude(), b.getLatitude(), isAsc);
        case "longitude":
          return this.compare(a.getLongitude(), b.getLongitude(), isAsc);
        case "area":
          return this.compare(this.getArea(a), this.getArea(b), isAsc);
        case "region":
          return this.compare(this.getRegion(a), this.getRegion(b), isAsc);
        default:
          return 0;
      }
    });
  }

  onCreate() {
    const site = new Site();
    site.setRegion(
      this.aaa.isAuthorized("read:region:{province:$}")
        ? this.aaa.getCurrentUser().getRegion(RoleType.Admin)
        : new Region()
    );
    site.setArea(new Area());
    const dialog = this.dialog.open(SiteFormV2Component, {
      width: "800px",
      data: { value : site, region: this.region, area: this.area}
    });
    dialog.afterClosed().subscribe((_set: Site) => {
      if (_set) {
        this.site.create(_set)
        .subscribe(_success => this.datasource.data = this.site.getTables());
      }
    });
  }

  onUpdate(row: Site) {
    const dialog = this.dialog.open(SiteFormV2Component, {
      width: "800px",
      data: { value : new Site(row), region: this.region, area: this.area}
    });
    dialog.afterClosed().subscribe((_set: Site) => {
      if (_set) {
        this.site.update(row.getId(), _set)
        .subscribe(_success => this.datasource.data = this.site.getTables());
      }
    });
  }

  onDelete(row: Site) {
    const dialog = this.dialog.open(DialogComponent, {
      data : {id: row.getId(), message : `Are you sure to remove ${row.getName()} ?`}
    });
    dialog.afterClosed().subscribe(_set => {
      if (_set) {
        this.site.delete(row.getId())
        .subscribe(_success => this.datasource.data = this.site.getTables());
      }
    });
  }
}
