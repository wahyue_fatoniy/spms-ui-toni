import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FormDialogRemoveComponent } from './form-dialog-remove.component';

describe('FormDialogRemoveComponent', () => {
  let component: FormDialogRemoveComponent;
  let fixture: ComponentFixture<FormDialogRemoveComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FormDialogRemoveComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FormDialogRemoveComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
