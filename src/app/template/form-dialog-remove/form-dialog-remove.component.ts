import { Component, OnInit, Inject} from '@angular/core';
import { MAT_DIALOG_DATA } from '@angular/material';

interface DeleteAction {
  id: number;
  message: string;
}

@Component({
  selector: 'app-form-dialog-remove',
  templateUrl: './form-dialog-remove.component.html',
  styleUrls: ['./form-dialog-remove.component.css']
})
export class FormDialogRemoveComponent implements OnInit {

  constructor(@Inject(MAT_DIALOG_DATA) public data: DeleteAction) { }

  ngOnInit() { }

}
