import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { FormTableValue } from '../../../models/interface/interface';

@Component({
  selector: 'app-form-table',
  templateUrl: './form-table.component.html',
  styleUrls: ['./form-table.component.css']
})
export class FormTableComponent implements OnInit {
  tableData: Object[];
  title: string;
  tableHead: Array<string> = [];
  constructor(
    private matDialogRef: MatDialogRef<FormTableComponent>,
    @Inject(MAT_DIALOG_DATA) public data: FormTableValue
  ) { }

  ngOnInit() {
    this.tableData = this.data['data'];
    this.title = this.data['title'];
    this.tableHead = Object.keys(this.data['data'][0]);
  }

  capitalizeFirstLetter(string) {
    return string.charAt(0).toUpperCase() + string.slice(1);
  }

}
