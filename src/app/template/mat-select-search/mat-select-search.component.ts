import {
  Component,
  OnInit,
  Output,
  EventEmitter,
  Input,
  OnDestroy,
} from '@angular/core';
import { FormControl } from '@angular/forms';
import { ReplaySubject, Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';

@Component({
  selector: 'app-mat-select-search',
  template: `
    <ngx-mat-select-search [formControl]="textInput"></ngx-mat-select-search>
  `,
  styles: ['']
})
export class MatSelectSearchComponent implements OnInit, OnDestroy {
  public replayData: ReplaySubject<Object[]> = new ReplaySubject<Object[]>(1);
  public textInput: FormControl = new FormControl();
  public dataBinding: Object[] = [];
  private _onDestroy = new Subject<void>();
  private constData: Object[] = [];

  @Output() dataChange = new EventEmitter<Object[]>();
  @Input()
  get data() {
    return this.dataBinding;
  }
  set data(val) {
    this.dataBinding = val;
    this.dataChange.emit(this.dataBinding);
  }

  constructor() { }

  ngOnInit() {
    this.onInputChanges();
    this.replayData
    .pipe(takeUntil(this._onDestroy))
    .subscribe(val => {
      this.data = val;
    });
  }

  ngOnDestroy() {
    this._onDestroy.next();
    this._onDestroy.complete();
  }

  private onInputChanges(): void {
    this.textInput.valueChanges.pipe(
      takeUntil(this._onDestroy)
    ).
    subscribe(() => {
      this.constData = Object.assign(this.constData, this.dataBinding.filter(data => {
        return !this.constData.find(constData =>
          constData && data ?  constData['id'] === data['id'] : data === constData
        );
      }));
      this.filterData();
    });
  }

  private filterData() {
    // get the search keyword
    let search = this.textInput.value;
    if (!search) {
      this.replayData.next(this.constData.slice());
      return;
    } else {
      search = search.toLowerCase();
    }
    // filter the data
    this.replayData.next(
      this.constData.filter(data => JSON.stringify(data).toLowerCase().indexOf(search) > -1)
    );
  }
}
