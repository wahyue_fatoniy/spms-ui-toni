import { Component, OnInit } from "@angular/core";
import { Location } from "@angular/common";
import { Router, NavigationEnd, ActivatedRoute } from "@angular/router";
import { AAAService } from "src/app/shared/aaa/aaa.service";
import { filter, takeUntil } from "rxjs/operators";
import { Subject } from "rxjs";
// import * as $ from 'jquery';

interface RouterConfig {
  id?: number;
  title: string;
  icon: string;
  path: string;
  scope: string;
  class?: string;
  collapse?: string;
  child?: RouterConfig[];
}

@Component({
  selector: "app-sidebar",
  templateUrl: "./sidebar.component.html",
  styleUrls: ["./sidebar.component.scss"]
})
export class SidebarComponent implements OnInit {
  ROUTES: Array<RouterConfig> = [
    {
      id: 1,
      title: "Dashboard",
      icon: "web",
      path: "home/dashboard-admin",
      class: "",
      child: [],
      scope: "read:dashboard-admin"
    },
    {
      id: 2,
      title: "Dashboard",
      icon: "web",
      path: "home/dashboard-pm",
      class: "",
      child: [],
      scope: "read:dashboard-pm"
    },
    {
      id: 3,
      title: "Dashboard",
      icon: "web",
      path: "home/dashboard-pic",
      class: "",
      child: [],
      scope: "read:dashboard-pic"
    },
    {
      id: 4,
      title: "Dashboard",
      icon: "web",
      path: "home/dashboard-finance",
      class: "",
      child: [],
      scope: "read:dashboard-finance"
    },
    {
      id: 5,
      title: "Dashboard",
      icon: "web",
      path: "home/dashboard-admin-center",
      class: "",
      child: [],
      scope: "read:dashboard-admin-center"
    },
    {
      id: 6,
      title: "Dashboard",
      icon: "web",
      path: "home/dashboard-gm",
      class: "",
      child: [],
      scope: "read:dashboard-gm"
    },
    // {
    //   id: 7,
    //   title: "Reference",
    //   icon: "dashboard",
    //   collapse: "",
    //   path: "",
    //   scope: "read:reference",
    //   child: []
    // },
    {
      id: 7,
      title: "Person",
      path: "home/person",
      class: "",
      icon: "person",
      scope: "read:person",
      child: [],
    },
    {
      id: 8,
      title: "Organization",
      path: "home/organization",
      class: "",
      icon: "account_balance",
      scope: "read:organization",
      child: [],

    },
    {
      id: 9,
      title: "Region",
      path: "home/region",
      class: "",
      icon: "place",
      scope: "read:region",
      child: [],

    },
    {
      id: 10,
      title: "Area",
      path: "home/area",
      class: "",
      icon: "person_pin_circle",
      scope: "read:area",
      child: [],

    },
    {
      id: 11,
      title: "Site",
      path: "home/site",
      class: "",
      icon: "store",
      scope: "read:site",
      child: [],
    },
    {
      id: 12,
      title: "Role",
      icon: "assignment_ind",
      collapse: "",
      path: "home/role",
      scope: "read:role",
      child: []
    },
    {
      id: 13,
      title: "Report",
      icon: "assignment",
      path: "home/report-pic",
      class: "",
      child: [],
      scope: "read:report-pic"
    },
    {
      id: 14,
      title: "Report",
      icon: "assignment",
      path: "home/report-pm",
      class: "",
      child: [],
      scope: "read:report-pm"
    },
    {
      id: 15,
      title: "Report",
      icon: "assignment",
      path: "home/report-admin",
      class: "",
      child: [],
      scope: "read:report-admin"
    },
    {
      id: 16,
      title: "Report",
      icon: "assignment",
      path: "home/report-finance",
      class: "",
      child: [],
      scope: "read:report-finance"
    },
    {
      id: 17,
      title: "Report",
      icon: "assignment",
      path: "home/report-developer",
      class: "",
      child: [],
      scope: "read:report-developer"
    }
  ];
  destroy$: Subject<boolean> = new Subject();

  constructor(
    public location: Location,
    public router: Router,
    public aaa: AAAService,
    public activeRoute: ActivatedRoute
  ) {
    this.subscribeActiveRoute();
  }

  ngOnInit() {}

  public subscribeActiveRoute(): void {
    let path: string;
    this.router.events
      .pipe(
        filter(event => event instanceof NavigationEnd),
        takeUntil(this.destroy$)
      )
      .subscribe(({ urlAfterRedirects }: NavigationEnd) => {
        path = urlAfterRedirects.split("/")[2];
        this.setInitActiveRoute(path);
      });
  }

  public setInitActiveRoute(initPath: string): void {
    let path: string;
    this.ROUTES = this.ROUTES.map(route => {
      path = route["path"].split("/")[1];
      if (path === initPath) {
        route["class"] = "active";
      }
      return route;
    });
    this.destroy$.next(true);
    this.destroy$.complete();
  }

  setActiveRoute(routeId: number): void {
    this.ROUTES = this.ROUTES.map(route => {
      if (route.id === routeId) {
        if (route.child.length !== 0) {
          route.collapse = this.setCollapse(route);
        }
        route.class = "active";
      } else {
        route.class = "";
      }
      return route;
    });
  }

  setCollapse(route: RouterConfig): string {
    if (route.collapse === "show") {
      route.collapse = "";
    } else {
      route.collapse = "show";
    }
    return route.collapse;
  }

  routeNavigate(path: string): void {
    this.router
      .navigate([path])
      .then()
      .catch(err => console.log(err));
  }

  isMobileMenu() {
    // if ($(window).width() > 991) {
    //   return false;
    // }
    // return true;
  }
}
