export const environment = {
  production: true,
  apiUrl: 'http://spms-satunol.ddns.net:3011',
  host: 'http://spms-satunol.ddns.net:3011',
  authUrl : 'https://ngx-ems.us.webtask.io/adf6e2f2b84784b57522e3b19dfc9201/api',
  reportUrl: 'http://spms-satunol.ddns.net:3015/jasperserver'
};
