import { TaskType } from '../enum/enum';

export class Activities {
  private id: number;
  private number: number;
  private type: TaskType;
  private name: string;

  constructor(input: Object) {
    if (input !== undefined) {
        this.id = input['id'];
        this.number = input['number'];
        this.type = input['type'];
        this.name = input['name'];
    }
  }

  public get getNumber() {
    return this.number;
  }
  public get getType() {
    return this.type;
  }

  public get getName() {
    return this.name;
  }
}
