import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Activities } from './activites.model';
import { Observable, BehaviorSubject } from 'rxjs';
import { map } from 'rxjs/operators';
import { environment } from '../../environments/environment';

const URL = '/api/activities';

@Injectable({
  providedIn: 'root'
})
export class ActivitiesService {

  private activities: BehaviorSubject<Activities[]> = new BehaviorSubject<Activities[]>([]);

  constructor(private http: HttpClient) {
    this.getAll.catch(err => console.log(err));
  }

  public get getData(): Observable<Activities[]> {
    return this.activities.asObservable();
  }

  private get getAll(): Promise<Activities[]> {
    return this.http
      .get(environment.apiUrl + URL)
      .pipe(
        map((val: Activities[]) => {
          val = val.map(activity => new Activities(activity));
          this.activities.next(val);
          return val;
        })
      ).toPromise();
  }
}
