import { Injectable } from '@angular/core';
import { Observable, BehaviorSubject, Subscription } from 'rxjs';
import { map } from 'rxjs/operators';
import { Admin } from './admin.model';
import { HttpClient } from '@angular/common/http';
import { PersonService } from '../person-service/person.service';
import { Person } from '../person-service/person.model';
import { environment } from '../../environments/environment';

const URL = '/api/admins';

@Injectable({
  providedIn: 'root'
})
export class AdminService {
  private admin: BehaviorSubject<Admin[]> = new BehaviorSubject<Admin[]>([]);
  private data: Array<Admin> = [];

  constructor(private http: HttpClient, private personService: PersonService) {
    this.onGetData();
  }

  public get connect(): Observable<Admin[]> {
    return this.admin.asObservable();
  }

  public disconnect(subscriptions: Subscription): void {
    if (subscriptions !== undefined) {
      if (subscriptions['closed'] === false) {
        subscriptions.unsubscribe();
      }
    }
  }

  private onGetData(): void {
    this.http
      .get(environment.apiUrl + URL)
      .pipe(
        map((adminsMap: Array<Admin>) => {
          this.data = adminsMap.map(adminMap => {
            return new Admin(adminMap);
          });
          this.admin.next(this.data);
        })
      )
      .toPromise()
      .catch(err => console.log(err));
  }


  public findByPartyId(id: number): Observable<Admin> {
    return this.http.get(environment.apiUrl + URL + `/party/${id}`)
    .pipe(
      map((admin: Object) => {
        return new Admin(admin);
      })
    );
  }

  public create(value: Admin): Promise<Admin> {
    return new Promise((resolve, reject) => {
      this.personService
        .create(new Person(value.getParty))
        .then((person: Person) => {
          value['party'] = person;

          this.http
            .post(environment.apiUrl + URL, value)
            .pipe(
              map((adminMap: Admin) => {
                value['id'] = adminMap['id'];
                this.data = [...this.data, new Admin(value)];
                this.admin.next(this.data);
                resolve(new Admin(value));
              })
            )
            .toPromise()
            .catch(err => reject(err));
        })
        .catch(err => reject(err));
    });
  }

  public update(value: Admin): Promise<Admin> {
    return new Promise((resolve, reject) => {
      this.personService
        .update(new Person(value.getParty))
        .then(() => {
          this.http
            .put(`${environment.apiUrl + URL}/${value['id']}`, value)
            .pipe(
              map((adminMap: Admin) => {
                this.data = this.data.map(
                  dataMap =>
                    dataMap['id'] === adminMap['id'] ? value : dataMap
                );
                this.admin.next(this.data);
                resolve(value);
              })
            )
            .toPromise()
            .catch(err => reject(err));
        })
        .catch(err => reject(err));
    });
  }

  public delete(value: Admin): Promise<number> {
    return this.http
      .delete(`${environment.apiUrl + URL}/${value['id']}`)
      .pipe(
        map((adminId: number) => {
          this.data = this.data.filter(result => result['id'] !== adminId);
          this.admin.next(this.data);
          return adminId;
        })
      )
      .toPromise();
  }
}

