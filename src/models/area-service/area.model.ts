import { Region } from '../region-service/region.model';

export class Area {
  private id: number;
  private name: string;
  private region: Region;
  private latitude: number;
  private longitude: number;

  constructor(input: Object) {
    if (input !== undefined) {
      this.id = input['id'];
      this.name = input['name'];
      this.region = new Region(input['region']);
      this.latitude = input['latitude'];
      this.longitude = input['longitude'];
    }
  }

  public get getId() {
    return this.id;
  }

  public get getName() {
    return this.name;
  }

  public get getRegion() {
    return this.region;
  }

  public get getLatitude() {
    return this.latitude;
  }

  public get getLongitude() {
    return this.longitude;
  }
}
