import { Injectable } from '@angular/core';
import { Observable, BehaviorSubject } from 'rxjs';
import { map } from 'rxjs/operators';
import { HttpClient } from '@angular/common/http';
import { Area } from './area.model';
import { environment } from '../../environments/environment';
const URL = '/api/areas';

@Injectable({
  providedIn: 'root'
})
export class AreaService {
  private area: BehaviorSubject<Area[]> = new BehaviorSubject<Area[]>([]);
  private data: Array<Area> = [];

  constructor(private http: HttpClient) {}

  public get getArea(): Observable<Area[]> {
    return this.area.asObservable();
  }

  public get getAll(): Promise<Area[]> {
    return this.http.get(environment.apiUrl + URL).pipe(
      map((areasMap: Array<Area>) => {
        this.data = areasMap.map(areaMap => new Area(areaMap));
        this.area.next(this.data);
        return this.data;
      })
    ).toPromise();
  }

  public create(value: Area): Promise<Area> {
    return this.http
      .post(environment.apiUrl + URL, value)
      .pipe(
        map((areaMap: Area) => {
          const area = new Area(areaMap);
          this.data = [...this.data, area];
          this.area.next(this.data);
          return area;
        })
      )
      .toPromise();
  }

  public update(value: Area): Promise<Area> {
    return this.http
      .put(`${environment.apiUrl + URL}/${value.getId}`, value)
      .pipe(
        map((areaMap: Area) => {
          const area = new Area(areaMap);
          this.data = this.data.map(
            mapData =>
              mapData['id'] === areaMap['id']
                ? area
                : mapData
          );
          this.area.next(this.data);
          return area;
        })
      )
      .toPromise();
  }

  public delete(id: number): Promise<number> {
    return this.http
      .delete(`${environment.apiUrl + URL}/${id}`)
      .pipe(
        map((areaId: number) => {
          this.data = this.data.filter(
            (result: Area) => (result.getId !== areaId)
          );
          this.area.next(this.data);
          return areaId;
        })
      )
      .toPromise();
  }
}
