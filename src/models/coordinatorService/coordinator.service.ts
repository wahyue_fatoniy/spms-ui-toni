import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable, Subscription } from 'rxjs';
import { Coordinator } from './coordinator.model';
import { HttpClient } from '@angular/common/http';
import { PersonService } from '../person-service/person.service';
import { map } from 'rxjs/operators';
import { Person } from '../person-service/person.model';
import { environment } from '../../environments/environment';

const URL = '/api/internal_cordinators';

@Injectable({
  providedIn: 'root'
})
export class CoordinatorService {
  private coordinator: BehaviorSubject<Coordinator[]> = new BehaviorSubject<Coordinator[]>([]);
  private data: Array<Coordinator> = [];

  constructor(private http: HttpClient, private personService: PersonService) {
    this.onGetData();
  }

  public get connect(): Observable<Coordinator[]> {
    return this.coordinator.asObservable();
  }

  public disconnect(subscriptions: Subscription): void {
    if (subscriptions !== undefined) {
      if (subscriptions['closed'] === false) {
        subscriptions.unsubscribe();
      }
    }
  }

  public findByPartyId(id: number): Observable<Coordinator> {
    return this.http.get(environment.apiUrl + '/api/cordinators' + `/party/${id}`)
    .pipe(
      map((pic: Object) => {
        return new Coordinator(pic);
      })
    );
  }

  private onGetData(): void {
    this.http
      .get(environment.apiUrl + URL)
      .pipe(
        map((coordinatorsMap: Array<Coordinator>) => {
          this.data = coordinatorsMap.map(coordinatorMap => {
            return new Coordinator(coordinatorMap);
          });
          this.coordinator.next(this.data);
        })
      )
      .toPromise()
      .catch(err => console.log(err));
  }

  public create(value: Coordinator): Promise<Coordinator> {
    return new Promise((resolve, reject) => {
      this.personService
        .create(new Person(value.getParty))
        .then((person: Person) => {
          value['party'] = person;
          this.http
            .post(environment.apiUrl + URL, value)
            .pipe(
              map((coordinatorMap: Coordinator) => {
                value['id'] = coordinatorMap['id'];
                this.data = [...this.data, new Coordinator(value)];
                this.coordinator.next(this.data);
                resolve(new Coordinator(value));
              })
            )
            .toPromise()
            .catch(err => reject(err));
        })
        .catch(err => reject(err));
    });
  }

  public update(value: Coordinator): Promise<Coordinator> {
    return new Promise((resolve, reject) => {
      this.personService
        .update(new Person(value['party']))
        .then(() => {
          this.http
            .put(`${environment.apiUrl + URL}/${value['id']}`, value)
            .pipe(
              map((coordinatorMap: Coordinator) => {
                this.data = this.data.map(
                  dataMap =>
                    dataMap['id'] === coordinatorMap['id'] ? value : dataMap
                );
                this.coordinator.next(this.data);
                resolve(value);
              })
            )
            .toPromise()
            .catch(err => reject(err));
        })
        .catch(err => reject(err));
    });
  }

  public delete(value: Coordinator): Promise<number> {
    return new Promise((resolve, reject) => {
      this.http
        .delete(`${environment.apiUrl + URL}/${value['id']}`)
        .toPromise()
        .then((coordinatorId: number) => {
          this.data = this.data.filter((result: Coordinator) => result.getId !== coordinatorId);
          this.coordinator.next(this.data);
          resolve(coordinatorId);
        })
        .catch(err => reject(err));
    });
  }
}
