import { Role } from '../role-service/role.model';

export class Customer extends Role {
  protected id: number;

  constructor(input: Object) {
    if (input) {
      super('CUSTOMER', input);
      this.id = input['id'];
    }
  }

  public get getId() {
    return this.id;
  }
}
