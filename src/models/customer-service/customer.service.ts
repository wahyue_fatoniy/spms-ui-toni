import { Injectable, OnInit } from '@angular/core';
import { Observable, BehaviorSubject, Subscription } from 'rxjs';
import { map } from 'rxjs/operators';
import { Customer } from './customer.model';
import { HttpClient } from '@angular/common/http';
import { setPartyType } from '../party-service/party.service';
import { OrganizationService } from '../organization-service/organization.service';
import { PersonService } from '../person-service/person.service';
import { Organization } from '../organization-service/organization.model';
import { Person } from '../person-service/person.model';
import { Party } from '../party-service/party.model';
import { environment } from '../../environments/environment';

const URL = '/api/customers';

@Injectable({
  providedIn: 'root'
})
export class CustomerService {
  private customer: BehaviorSubject<Customer[]> = new BehaviorSubject<
    Customer[]
  >([]);
  private data: Array<Customer> = [];

  constructor(
    private http: HttpClient,
    private organizationService: OrganizationService,
    private personService: PersonService
  ) {
    this.onGetData();
  }

  public get connect(): Observable<Customer[]> {
    return this.customer.asObservable();
  }

  public disconnect(subscriptions: Subscription): void {
    if (subscriptions !== undefined) {
      if (subscriptions.closed === false) {
        subscriptions.unsubscribe();
      }
    }
  }

  private onGetData(): void {
    this.http
      .get(environment.apiUrl + URL)
      .pipe(
        map((customersMap: Array<Customer>) => {
          this.data = customersMap.map((customerMap: Customer) => {
            customerMap['party']['partyType'] = setPartyType(customerMap);
            return new Customer(customerMap);
          });
          this.customer.next(this.data);
        })
      )
      .toPromise()
      .catch(err => console.log(err));
  }

  public create(value: Customer): Promise<Customer> {
    return new Promise((resolve, reject) => {
      this.referenceService(value['party'], 'create').then(
        (party: Person | Organization) => {
          value['party'] = party;
          this.http
            .post(environment.apiUrl + URL, value)
            .toPromise()
            .then((customer: Customer) => {
              customer['party']['partyType'] = setPartyType(value);
              this.data = [...this.data, new Customer(customer)];
              this.customer.next(this.data);
              resolve(customer);
            })
            .catch(err => reject(err));
        }
      );
    });
  }

  public update(value: Customer): Promise<Customer> {
    return new Promise((resolve, reject) => {
      value['party']['partyType'] = setPartyType(value);
      this.referenceService(value['party'], 'update')
        .then((party: Person | Organization) => {
          this.http
            .put(`${environment.apiUrl + URL}/${value['id']}`, value)
            .pipe(
              map((customerMap: Customer) => {
                this.data = this.data.map(
                  dataMap =>
                    dataMap['id'] === customerMap['id'] ? value : dataMap
                );
                this.customer.next(this.data);
                resolve(customerMap);
              })
            )
            .toPromise()
            .catch(err => reject(err));
        })
        .catch(err => reject(err));
    });
  }

  public delete(value: Customer): Promise<number> {
    return new Promise((resolve, reject) => {
      this.http
        .delete(`${environment.apiUrl + URL}/${value['id']}`)
        .pipe(
          map((customerId: number) => {
            this.referenceService(value['party'], 'delete').catch(err =>
              reject(err)
            );
            this.data = this.data.filter(
              dataMap => dataMap['id'] !== customerId
            );
            this.customer.next(this.data);
            resolve(customerId);
          })
        )
        .toPromise()
        .catch(err => reject(err));
    });
  }

  private referenceService(
    party: Party,
    action: string
  ): Promise<Person | Organization> {
    if (party !== undefined) {
      let referenceService: Promise<Person | Organization | number>;
      switch (party['partyType']) {
        case 'ORGANIZATION':
          return (referenceService = this.organizationService[
            `${action}`
          ](new Organization(party)));

        case 'PERSON':
          return (referenceService = this.personService[`${action}`](
            new Person(party)
          ));
      }
    }
  }
}
