import { Subcont } from '../subcont-service/subcont.model';
import { Coordinator } from '../coordinatorService/coordinator.model';
import { SiteWork } from '../siteWork-service/siteWork.model';
import { TaskExecution } from '../taskExecution-service/task-execution.model';

export class DailyReport {
  private id: number;
  private siteWork: SiteWork;
  private taskExecutions: TaskExecution[];
  private submit: boolean;

  constructor(input: Object) {
    if (input !== undefined) {
      this.id = input['id'];
      this.siteWork = new SiteWork(input['siteWork']);
      this.taskExecutions = input['taskExecutions'] ? input['taskExecutions'].map(task => new TaskExecution(task)) : [];
      this.submit = input['submit'];
    }
  }

  public get getId() {
    return this.id;
  }

  public get getSiteWork() {
    return this.siteWork;
  }

  public get getTaskExecutions() {
    return this.taskExecutions;
  }

  public get getSubmit() {
    return this.submit;
  }
}
