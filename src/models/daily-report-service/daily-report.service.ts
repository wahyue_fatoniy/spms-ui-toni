import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { DailyReport } from '../../models/daily-report-service/daily-report.model';
import { BehaviorSubject, Observable } from 'rxjs';
import { environment } from '../../environments/environment';
import { map } from 'rxjs/operators';

const URL = '/api/daily_reports';

@Injectable({
  providedIn: 'root'
})
export class DailyReportService {

  private dailyReport: BehaviorSubject<DailyReport[]> = new BehaviorSubject<DailyReport[]>([]);
  private data: DailyReport[] = [];

  constructor(private http: HttpClient) { }

  public get getDailyReport(): Observable<DailyReport[]> {
    return this.dailyReport.asObservable();
  }

  public get getAll(): Promise<DailyReport[]> {
    return this.http.get(environment.apiUrl + URL).pipe(
      map((val: DailyReport[]) => {
        this.data = val.map(report => new DailyReport(report));
        this.dailyReport.next(this.data);
        return this.data;
      })
    ).toPromise();
  }
}
