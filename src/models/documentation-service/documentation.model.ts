import { Resources } from '../resources-service/resources.model';
import { Region } from '../region-service/region.model';
import { Person } from '../person-service/person.model';

export class Documentation extends Resources {
  private documentationType: string;

  constructor(input: Object) {
    if (input) {
      input = { ...input, party: new Person(input['party'])};
      input['party']['partyType'] = 'PERSON';
      super('DOCUMENTATION', input);
    }
  }

  public get getDocumentationType() {
    if (this.getNip === undefined || this.getNip === null) {
        this.documentationType = 'SUBCONT';
    } else { this.documentationType = 'INTERNAL'; }
    return this.documentationType;
  }
}
