import { Injectable } from '@angular/core';
import { Observable, BehaviorSubject, Subscription } from 'rxjs';
import { map } from 'rxjs/operators';
import { HttpClient } from '@angular/common/http';
import { PersonService } from '../person-service/person.service';
import { Person } from '../person-service/person.model';
import { Documentation } from './documentation.model';
import { environment } from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class DocumentationService {
  private documentation: BehaviorSubject<Documentation[]> = new BehaviorSubject<Documentation[]>([]);
  private data: Array<Documentation> = [];
  private URL = '/api/documentations';

  constructor(private http: HttpClient, private personService: PersonService) {
    this.onGetData();
  }

  public get connect(): Observable<Documentation[]> {
    return this.documentation.asObservable();
  }

  public disconnect(subscriptions: Subscription): void {
    if (subscriptions !== undefined) {
      if (subscriptions['closed'] === false) {
        subscriptions.unsubscribe();
      }
    }
  }

  private onGetData(): void {
    this.http
      .get(environment.apiUrl + this.URL)
      .pipe(
        map((documentsMap: Array<Documentation>) => {
          this.data = documentsMap.map(documentMap => {
            return new Documentation(documentMap);
          });
          this.documentation.next(this.data);
        })
      )
      .toPromise()
      .catch(err => console.log(err));
  }

  public create(value: Documentation): Promise<Documentation> {
    return new Promise((resolve, reject) => {
      this.personService
        .personExist(new Person(value['party']))
        .then((person: Person) => {
          value['party'] = person;
          this.http
            .post(environment.apiUrl + this.setUrl(value), value)
            .pipe(
              map((documentMap: Documentation) => {
                value['id'] = documentMap['id'];
                this.data = [...this.data, new Documentation(value)];
                this.documentation.next(this.data);
                resolve(new Documentation(value));
              })
            )
            .toPromise()
            .catch(err => reject(err));
        })
        .catch(err => reject(err));
    });
  }

  public update(value: Documentation): Promise<Documentation> {
    return new Promise((resolve, reject) => {
      this.personService
        .update(new Person(value['party']))
        .then(() => {
          this.http
            .put(`${environment.apiUrl + this.setUrl(value)}/${value['id']}`, value)
            .pipe(
              map((documentMap: Documentation) => {
                this.data = this.data.map(
                  dataMap =>
                    dataMap['id'] === documentMap['id'] ? value : dataMap
                );
                this.documentation.next(this.data);
                resolve(value);
              })
            )
            .toPromise()
            .catch(err => reject(err));
        })
        .catch(err => reject(err));
    });
  }

  public delete(value: Documentation): Promise<number> {
    return new Promise((resolve, reject) => {
      this.http
        .delete(`${environment.apiUrl + this.setUrl(value)}/${value['id']}`)
        .toPromise()
        .then((documentId: number) => {
          this.personService
            .delete(new Person(value['party']))
            .catch(err => reject(err));
          this.data = this.data.filter(result => result['id'] !== documentId);
          this.documentation.next(this.data);
          resolve(documentId);
        })
        .catch(err => reject(err));
    });
  }

  private setUrl(value: Documentation): string {
    let url = '/api/internal_documentations';
    if (value['nip'] === undefined || value['nip'] === null) {
      url = this.URL;
    }
    return url;
  }
}
