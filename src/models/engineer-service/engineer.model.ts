import { Resources } from '../resources-service/resources.model';
import { Region } from '../region-service/region.model';
import { Person } from '../person-service/person.model';

export class Engineer extends Resources {
  private engineerType: string;

  constructor(input: Object) {
    if (input) {
      input = { ...input, party: new Person(input['party'])};
      input['party']['partyType'] = 'PERSON';
      super('ENGINEER', input);
    }
  }

  public get getId() {
    return this.id;
  }

  public get getEngineerType() {
    if (this.getNip === null || this.getNip === undefined) {
      this.engineerType = 'SUBCONT';
    } else {
      this.engineerType = 'INTERNAL';
    }
    return this.engineerType;
  }
}
