import { Injectable } from '@angular/core';
import { Observable, BehaviorSubject, Subscription } from 'rxjs';
import { map } from 'rxjs/operators';
import { HttpClient } from '@angular/common/http';
import { PersonService } from '../person-service/person.service';
import { Person } from '../person-service/person.model';
import { Engineer } from './engineer.model';
import { environment } from '../../environments/environment';
@Injectable({
  providedIn: 'root'
})
export class EngineerService {
  private engineer: BehaviorSubject<Engineer[]> = new BehaviorSubject<Engineer[]>([]);
  private data: Array<Engineer> = [];
  private URL = '/api/engineers';

  constructor(private http: HttpClient, private personService: PersonService) {
    this.onGetData();
  }

  public get connect(): Observable<Engineer[]> {
    return this.engineer.asObservable();
  }

  public disconnect(subscriptions: Subscription): void {
    if (subscriptions !== undefined) {
      if (subscriptions['closed'] === false) {
        subscriptions.unsubscribe();
      }
    }
  }

  private onGetData(): void {
    this.http
      .get(environment.apiUrl + this.URL)
      .pipe(
        map((engineersMap: Array<Engineer>) => {
          this.data = engineersMap.map(engineerMap => {
            return new Engineer(engineerMap);
          });
          this.engineer.next(this.data);
        })
      )
      .toPromise()
      .catch(err => console.log(err));
  }

  public create(value: Engineer): Promise<Engineer> {
    return new Promise((resolve, reject) => {
      this.personService
        .personExist(new Person(value['party']))
        .then((person: Person) => {
          value['party'] = person;
          this.http
            .post(environment.apiUrl + this.setUrl(value), value)
            .pipe(
              map((engineerMap: Engineer) => {
                value['id'] = engineerMap['id'];
                this.data = [...this.data, new Engineer(value)];
                this.engineer.next(this.data);
                resolve(new Engineer(value));
              })
            )
            .toPromise()
            .catch(err => reject(err));
        })
        .catch(err => reject(err));
    });
  }

  public update(value: Engineer): Promise<Engineer> {
    return new Promise((resolve, reject) => {
      this.personService
        .update(new Person(value['party']))
        .then(() => {
          this.http
            .put(`${environment.apiUrl + this.setUrl(value)}/${value['id']}`, value)
            .pipe(
              map((engineerMap: Engineer) => {
                this.data = this.data.map(
                  dataMap =>
                    dataMap['id'] === engineerMap['id'] ? value : dataMap
                );
                this.engineer.next(this.data);
                resolve(value);
              })
            )
            .toPromise()
            .catch(err => reject(err));
        })
        .catch(err => reject(err));
    });
  }

  public delete(value: Engineer): Promise<number> {
    return new Promise((resolve, reject) => {
      this.http
        .delete(`${environment.apiUrl + this.setUrl(value)}/${value['id']}`)
        .toPromise()
        .then((engineerId: number) => {
          this.personService
            .delete(new Person(value['party']))
            .catch(err => reject(err));
          this.data = this.data.filter((result: Engineer) => result.getId !== engineerId);
          this.engineer.next(this.data);
          resolve(engineerId);
        })
        .catch(err => reject(err));
    });
  }

  private setUrl(value: Engineer): string {
    let url = '/api/internal_engineers';
    if (value['nip'] === undefined || value['nip'] === null) {
      url = this.URL;
    }
    return url;
  }
}
