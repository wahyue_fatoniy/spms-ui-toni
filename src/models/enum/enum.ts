export enum Status {
  ON_PROGRESS = 'ON_PROGRESS',
  DONE = 'DONE',
  CANCEL = 'CANCEL',
  NOT_STARTED = 'NOT_STARTED'
}

export enum UIstatus {
  ON_PROGRESS = 'ON PROGRESS',
  DONE = 'DONE',
  CANCEL = 'CANCEL',
  NOT_STARTED = 'NOT STARTED'
}

export enum TaskStatus {
  false = 'NOT DONE',
  true = 'DONE'
}

export enum TaskType {
  ENGINEER = 'ENGINEER',
  TECHNICIAN = 'TECHNICIAN',
  DOCUMENTATION = 'DOCUMENTATION'
}

export enum Activity {
  MOS = 'MOS',
  INSTALLATION = 'INSTALLATION',
  INTEGRATION = 'INTEGRATION',
  TAKE_DATA = 'TAKE DATA',
  ATP = 'ATP'
}

export enum ActivityDoc {
  CREATE_DOCUMENT = 'CREATE DOCUMENT',
  CREATE_BAUT = 'CREATE BAUT'
}

export enum Severity {
  'NONE' = 'NONE',
  'NORMAL' = 'NORMAL',
  'MAJOR_I' = 'MAJOR-I',
  'MAJOR_E' = 'MAJOR-E',
  'CRITICAL_I' = 'CRITICAL-I',
  'CRITICAL_E' = 'CRITICAL-E'
}

export enum MandaysType {
  engineerdays = 'engineerdays',
  techniciandays = 'techniciandays',
  documentationdays = 'documentationdays'
}

export enum ActivityDays {
  MOS = '10%',
  INSTALLATION = '30%',
  INTEGRATION = '30%',
  TAKE_DATA = '10%',
  ATP = '20%',
  CREATE_DOCUMENT = '50%',
  CREATE_BAUT = '50%'
}

export enum UIActivity {
  CREATE_DOCUMENT = 'CREATE DOC',
  CREATE_BAUT = 'CREATE BAUT',
  MOS = 'MOS',
  INSTALLATION = 'INSTALLATION',
  INTEGRATION = 'INTEGRATION',
  TAKE_DATA = 'TAKE DATA',
  ATP = 'ATP'
}

export enum Roles {
  Admin = 'admin',
  Finance = 'finance',
  Sales = 'sales',
  'Project Manager' = 'projectManager',
  Coordinator = 'coordinator',
  Subcont = 'Subcont',
  Customer = 'customer',
  Partner = 'partner',
  Engineer = 'engineer',
  Technician = 'technician',
  Documentation = 'documentation'
}
