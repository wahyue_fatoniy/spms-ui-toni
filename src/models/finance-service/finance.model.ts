import { Role } from '../role-service/role.model';
import { Person } from '../person-service/person.model';

export class Finance extends Role {
  private nip: number;
  constructor(input: Object) {
    if (input) {
      input = {...input, party: new Person(input['party'])};
      input['party']['partyType'] = 'PERSON';
      super('FINANCE', input);
      this.nip = input['nip'];
    }
  }

  public get getNip() {
    return this.nip;
  }
}
