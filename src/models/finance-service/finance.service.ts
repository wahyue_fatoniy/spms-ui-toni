import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import { Finance } from './finance.model';
import { environment } from '../../environments/environment';
import { BehaviorSubject, Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { PersonService } from '../person-service/person.service';
import { Person } from '../person-service/person.model';

const URL = `${environment.apiUrl}/api/finances`;

@Injectable({
  providedIn: 'root'
})
export class FinanceService {
  private finance: BehaviorSubject<Finance[]> = new BehaviorSubject<Finance[]>([]);
  private data: Finance[] = [];
  constructor(private http: HttpClient, private personService: PersonService) { }

  public get getFinance(): Observable<Finance[]> {
    return this.finance.asObservable();
  }

  public get getAll(): Promise<Finance[]> {
    return this.http
      .get(URL)
      .pipe(
        map((finances: Finance[]) => {
          this.data = finances.map(adminMap => {
            return new Finance(adminMap);
          });
          this.finance.next(this.data);
          return this.data;
        })
      )
      .toPromise();
  }

  public create(value: Finance): Promise<Finance> {
    return new Promise((resolve, reject) => {
      this.personService
        .create(new Person(value.getParty))
        .then((person: Person) => {
          value['party'] = person;

          this.http
            .post(URL, value)
            .pipe(
              map((finance: Finance) => {
                value['id'] = finance['id'];
                this.data = [...this.data, new Finance(value)];
                this.finance.next(this.data);
                resolve(new Finance(value));
              })
            )
            .toPromise()
            .catch(err => reject(err));
        })
        .catch(err => reject(err));
    });
  }

  public update(value: Finance): Promise<Finance> {
    return new Promise((resolve, reject) => {
      this.personService
        .update(new Person(value.getParty))
        .then(() => {
          this.http
            .put(`${URL}/${value['id']}`, value)
            .pipe(
              map((financeMap: Finance) => {
                this.data = this.data.map(
                  dataMap =>
                    dataMap['id'] === financeMap['id'] ? value : dataMap
                );
                this.finance.next(this.data);
                resolve(value);
              })
            )
            .toPromise()
            .catch(err => reject(err));
        })
        .catch(err => reject(err));
    });
  }


  public delete(value: Finance): Promise<number> {
    return this.http
      .delete(`${URL}/${value['id']}`)
      .pipe(
        map((financeId: number) => {
          this.data = this.data.filter(result => result['id'] !== financeId);
          this.finance.next(this.data);
          return financeId;
        })
      )
      .toPromise();
  }
}
