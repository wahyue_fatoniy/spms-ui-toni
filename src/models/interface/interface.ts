import { Admin } from '../admin-service/admin.model';
import { Area } from '../area-service/area.model';
import { Coordinator } from '../coordinatorService/coordinator.model';
import { Customer } from '../customer-service/customer.model';
import { Documentation } from '../documentation-service/documentation.model';
import { Engineer } from '../engineer-service/engineer.model';
import { Organization } from '../organization-service/organization.model';
import { Partner } from '../partner-service/partner.model';
import { Person } from '../person-service/person.model';
import { PurchaseOrder } from '../purchaseOrder-service/purchase-order.model';
import { Region } from '../region-service/region.model';
import { Sales } from '../sales-service/sales.model';
import { Site } from '../site-service/site.model';
import { SiteWork } from '../siteWork-service/siteWork.model';
import { SowType } from '../sowType-service/sow-type.model';
import { Subcont } from '../subcont-service/subcont.model';
import { Task } from '../task-service/task.model';
import { TaskExecution } from '../taskExecution-service/task-execution.model';
import { TaskExecutor } from '../taskExecutor-service/task-executor.model';
import { TaskOwner } from '../taskOwner-service/task-owner.model';
import { Technician } from '../technician-service/technician.model';
import { WeeklyPlanning } from '../weeklyPlanning-Service/weekly-planning.model';
import { Invoice } from '../invoice-service/invoice.model';
import { Finance } from '../finance-service/finance.model';

export interface TaskTableEvent {
  action: string;
  value: Task | Object;
}

export interface PersonTableEvent {
  action: string;
  value: Person | Object;
}

export interface OrganizationTableEvent {
  action: string;
  value: Organization | Object;
}

export interface SiteTableEvent {
  action: string;
  value: Site | Object;
}

export interface AreaTableEvent {
  action: string;
  value: Area | Object;
}

export interface RegionTableEvent {
  action: string;
  value: Region | Object;
}

export interface SowTableEvent {
  action: string;
  value: SowType | Object;
}

export interface AdminTableEvent {
  action: string;
  value: Admin | Object;
}

export interface SalesTableEvent {
  action: string;
  value: Sales | Object;
}

export interface PartnerTableEvent {
  action: string;
  value: Partner | Object;
}

export interface CustomerTableEvent {
  action: string;
  value: Customer | Object;
}

export interface ExecutorTableEvent {
  action: string;
  value: TaskExecutor | Object;
}

export interface OwnerTableEvent {
  action: string;
  value: TaskOwner | Object;
}

export interface TaskExecutionTableEvent {
  action: string;
  value: TaskExecution | Object;
}

export interface ProjectTableEvent {
  action: string;
  value: SiteWork | Object;
}

export interface PoTableEvent {
  action: string;
  value: PurchaseOrder | Object;
}

export interface TaskExecutorTableEvent {
  action: string;
  value: TaskExecutor | Object;
}

export interface WeeklyPlanningTableEvent {
  action: string;
  value: WeeklyPlanning | Object;
}

export interface EngineerTableEvent {
  action: string;
  value: Engineer | Object;
}

export interface TechnicianTableEvent {
  action: string;
  value: Technician | Object;
}

export interface DocumentationTableEvent {
  action: string;
  value: Documentation | Object;
}

export interface CoordinatorTableEvent {
  action: string;
  value: Finance | Object;
}

export interface SubcontTableEvent {
  action: string;
  value: Subcont | Object;
}

export interface InvoiceTableEvent {
  action: string;
  value: Invoice | Object;
}

export interface FinanceTableEvent {
  action: string;
  value: Finance | Object;
}

export type ExecutorType = 'ENGINEER' | 'TECHNICIAN' | 'DOCUMENTATION';
export type MandaysType = 'engineerDay' | 'technicianDay' | 'documentationDay';
export type Gender = 'MALE' | 'FEMALE';
export type PartyType = 'ORGANIZATION' | 'PERSON';
export type picType = 'INTERNAL' | 'SUBCONT';

export interface ExecutorDaysType  {
  type: ExecutorType;
  mandaysType: MandaysType;
}

export interface Activity<T> {
  MOS: T;
  INSTALLATION: T;
  INTEGRATION: T;
  TAKE_DATA: T;
  ATP: T;
}

export interface ActivityDoc<T> {
  CREATE_DOCUMENT: T;
  SUBMIT_DOCUMENT: T;
  APPROVE_DOCUMENT: T;
  SUBMIT_BAUT: T;
  APPROVE_BAUT: T;
}

export interface Filter {
  value: string | { value: string, key: string };
  label: string;
}

export interface FormTableValue {
  data: SiteWork[];
  title: string;
}

export interface ParamCoordinatorId {
  coordinatorId: number;
  siteWorkId: number;
  startDate: string;
  endDate: string;
}

export interface WeeklyPlanningDate {
  startWeek: string;
  endWeek: string;
}
