import { SiteWork } from '../siteWork-service/siteWork.model';
import { PurchaseOrder } from '../purchaseOrder-service/purchase-order.model';
import { DatePipe } from '@angular/common';
import { NumberInStringPipe } from '../../app/pipes/numberInString/number-in-string.pipe';

const datePipe: DatePipe = new DatePipe('id');
const setNumber: NumberInStringPipe = new NumberInStringPipe();

export class Invoice {
  private id: number;
  private invoiceNumber: number;
  private po: PurchaseOrder;
  private siteWorks: SiteWork[];
  private invoiceDate: string;
  private dueDate: string;
  private invoiceValue: number;
  private referenceBast: string;
  private paymentDate: string;
  private totalInvoice: number;

  constructor(input?: Object) {
    if (input !== undefined) {
      this.id = input['id'];
      this.invoiceNumber = input['invoiceNumber'];
      this.invoiceDate = input['invoiceDate'] ? this.setDate(input['invoiceDate']) : null;
      this.po = new PurchaseOrder(input['po']);
      this.siteWorks = input['siteWorks']
        ? input['siteWorks'].map(val => new SiteWork(val))
        : [];
      this.dueDate = this.setDate(input['dueDate']);
      this.paymentDate = input['paymentDate'] ? this.setDate(input['paymentDate']) : null;
      this.invoiceValue = input['invoiceValue'] ? setNumber.transform(input['invoiceValue']) : 0;
      this.referenceBast = input['referenceBast'];
      this.totalInvoice = this.po.getTotalInvoice ? setNumber.transform(this.po.getTotalInvoice) : 0;
    }
  }

  public get getId() {
    return this.id;
  }

  public get getInvoiceNumber() {
    return this.invoiceNumber;
  }

  public get getInvoiceDate() {
    return this.invoiceDate;
  }

  public get getPo() {
    return this.po;
  }

  public get getSiteWorks() {
    return this.siteWorks;
  }

  public get getDueDate() {
    return this.dueDate;
  }

  public get getPaymentDate() {
    return this.paymentDate;
  }

  public get getInvoiceValue() {
    return this.invoiceValue;
  }

  public get getRefBast() {
    return this.referenceBast;
  }

  public get getTotalInvoice() {
    return this.totalInvoice;
  }

  private setDate(date: string | Date): string {
    return datePipe.transform(date, 'yyyy-MM-ddTHH:mm:ssZ', '+0000');
  }
}
