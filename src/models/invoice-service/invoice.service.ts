import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { BehaviorSubject, Observable } from 'rxjs';
import { Invoice } from './invoice.model';
import { environment } from '../../environments/environment';
import { map } from 'rxjs/operators';

const URL = '/api/invoices';

@Injectable({
  providedIn: 'root'
})
export class InvoiceService {

  public invoice: BehaviorSubject<Invoice[]> =  new BehaviorSubject<Invoice[]>([]);
  private data: Invoice[] = [];

  constructor(private http: HttpClient) { }

  public get getInvoice(): Observable<Invoice[]> {
    return this.invoice.asObservable();
  }

  public get getAll(): Promise<Invoice[]> {
    return this.http.get(environment.apiUrl + URL).pipe(
      map((val: Invoice[]) => {
        this.data = val.map(invoice => new Invoice(invoice));
        this.invoice.next(this.data);
        return this.data;
      })
    ).toPromise();
  }

  public create(value: Invoice): Promise<Invoice[]> {
    return this.http.post(environment.apiUrl + URL, value).pipe(
      map((val: Invoice[]) => {
        this.data = [...this.data, new Invoice(val)];
        this.invoice.next(this.data);
        return this.data;
      })
    ).toPromise();
  }

  public update(value: Invoice): Promise<Invoice> {
    return this.http.put(`${environment.apiUrl + URL }/${value.getId}`, value).pipe(
      map((val: Invoice) => {
        val = new Invoice(val);
        this.data = this.data.map(invoice => invoice.getId === val.getId ? val : invoice);
        this.invoice.next(this.data);
        return val;
      })
    ).toPromise();
  }
}
