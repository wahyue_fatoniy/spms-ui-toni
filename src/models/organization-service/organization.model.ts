import { Party } from '../party-service/party.model';
import { Role } from '../role-service/role.model';

export class Organization extends Party {
  protected id: number;
  private roles: Role[];
  constructor(input: Object) {
    if (input) {
      const party = {
        name: input['name'],
        address: input['address'],
        email: input['email'],
        phone: input['phone']
      };
      super('ORGANIZATION', party);
      this.roles = input['roles'] ? input['roles'] : [];
      this.id = input['id'];
    }
  }

  public get getRoles() {
    return this.roles;
  }

  public set getRoles(roles: any) {
    this.roles = roles;
  }

  public get getId() {
    return this.id;
  }
}
