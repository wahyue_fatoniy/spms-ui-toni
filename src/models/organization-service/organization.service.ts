import { Injectable } from '@angular/core';
import { Observable, BehaviorSubject } from 'rxjs';
import { map } from 'rxjs/operators';
import { Organization } from './organization.model';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../environments/environment';

const URL = '/api/organizations';

@Injectable({
  providedIn: 'root'
})
export class OrganizationService {
  private organization: BehaviorSubject<Organization[]> = new BehaviorSubject<
    Organization[]
  >([]);
  private data: Array<Organization> = [];

  constructor(private http: HttpClient) {}

  public get getOrganization(): Observable<Organization[]> {
    return this.organization.asObservable();
  }

  public get getAll(): Promise<Array<Organization>> {
    return this.http
      .get(environment.apiUrl + URL)
      .pipe(
        map((organizationsMap: Array<Organization>) => {
          this.data = organizationsMap.map(
            organizationMap => new Organization(organizationMap)
          );
          this.organization.next(this.data);
          return this.data;
        })
      )
      .toPromise();
  }

  public create(value: Organization): Promise<Organization> {
    return this.http
      .post(environment.apiUrl + URL, value)
      .pipe(
        map((organizationMap: Organization) => {
          const organization = new Organization(organizationMap);
          this.data = [...this.data, organization];
          this.organization.next(this.data);
          return organization;
        })
      )
      .toPromise();
  }

  public update(value: Organization): Promise<Organization> {
    return this.http
      .put(`${environment.apiUrl + URL}/${value['id']}`, value)
      .pipe(
        map((organizationMap: Organization) => {
          const organization = new Organization(organizationMap);
          this.data = this.data.map(
            dataMap => (dataMap['id'] === value['id'] ? organization : dataMap)
          );
          this.organization.next(this.data);
          return organization;
        })
      )
      .toPromise();
  }

  public delete(value: Organization): Promise<number> {
    return this.http
      .delete(`${environment.apiUrl + URL}/${value['id']}`)
      .pipe(
        map((organizationId: number) => {
          this.data = this.data.filter(
            result => result['id'] !== organizationId
          );
          this.organization.next(this.data);
          return organizationId;
        })
      )
      .toPromise();
  }
}
