import { Role } from '../role-service/role.model';

export class Partner extends Role {
  protected id: number;

  constructor(input: Object) {
    if (input !== undefined && input !== null) {
      super('PARTNER', input);
      this.id = input['id'];
    }
  }

  public get getId() {
    return this.id;
  }
}
