import { Injectable } from '@angular/core';
import { Observable, BehaviorSubject, Subscription } from 'rxjs';
import { map } from 'rxjs/operators';
import { Partner } from './partner.model';
import { HttpClient } from '@angular/common/http';
import { OrganizationService } from '../organization-service/organization.service';
import { Organization } from '../organization-service/organization.model';
import { environment } from '../../environments/environment';

const URL = '/api/partners';

@Injectable({
  providedIn: 'root'
})
export class PartnerService {
  private partner: BehaviorSubject<Partner[]> = new BehaviorSubject<Partner[]>(
    []
  );
  private data: Array<Partner> = [];

  constructor(
    private http: HttpClient,
    private organizationService: OrganizationService
  ) {
    this.onGetData();
  }

  public get connect(): Observable<Partner[]> {
    return this.partner.asObservable();
  }

  public disconnect(subscriptions: Subscription): void {
    if (subscriptions !== undefined) {
      if (subscriptions['closed'] === false) {
        subscriptions.unsubscribe();
      }
    }
  }

  private onGetData(): void {
    this.http
      .get(environment.apiUrl + URL)
      .pipe(
        map((orgMap: Array<Partner>) => {
          this.data = orgMap.map(org => {
            return new Partner(org);
          });
          this.partner.next(this.data);
        })
      )
      .toPromise()
      .catch(err => console.log(err));
  }

  public create(value: Partner): Promise<Partner> {
    return new Promise((resolve, reject) => {
      this.organizationService
        .create(new Organization(value['party']))
        .then((org: Organization) => {
          value['party'] = org;
          this.http
            .post(environment.apiUrl + URL, value)
            .pipe(
              map((partnerMap: Partner) => {
                value['id'] = partnerMap['id'];
                this.data = [...this.data, new Partner(value)];
                this.partner.next(this.data);
                resolve(new Partner(value));
              })
            )
            .toPromise()
            .catch(err => reject(err));
        })
        .catch(err => reject(err));
    });
  }

  public update(value: Partner): Promise<Partner> {
    return new Promise((resolve, reject) => {
      this.organizationService
        .update(new Organization(value['party']))
        .then(() => {
          this.http
            .put(`${environment.apiUrl + URL}/${value['id']}`, value)
            .pipe(
              map((partnerMap: Partner) => {
                this.data = this.data.map(
                  dataMap =>
                    dataMap['id'] === partnerMap['id'] ? value : dataMap
                );
                this.partner.next(this.data);
                resolve(value);
              })
            )
            .toPromise()
            .catch(err => reject(err));
        })
        .catch(err => reject(err));
    });
  }

  public delete(value: Partner): Promise<number> {
    return new Promise((resolve, reject) => {
      this.http
        .delete(`${environment.apiUrl + URL}/${value['id']}`)
        .toPromise()
        .then((orgId: number) => {
          this.organizationService
            .delete(new Organization(value['party']))
            .catch(err => reject(err));
          this.data = this.data.filter((result: Partner) => result.getId !== orgId);
          this.partner.next(this.data);
          resolve(orgId);
        })
        .catch(err => reject(err));
    });
  }
}
