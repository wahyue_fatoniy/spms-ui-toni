export class Party {
  protected id: number;
  private name: string;
  private phone: string;
  private email: string;
  private address: string;
  private partyType: string;

  constructor(partyType: string, input: Object) {
    this.partyType = partyType;
    if (input) {
        this.id = input['id'];
        this.name = input['name'];
        this.phone = input['phone'];
        this.email = input['email'];
        this.address = input['address'];
    }
  }

  public get getId() {
    return this.id;
  }

  public get getName() {
    return this.name;
  }

  public get getPhone() {
    return this.phone;
  }

  public get getEmail() {
    return this.email;
  }

  public get getAddress() {
    return this.address;
  }

  public get getPartyType() {
    return this.partyType;
  }
}
