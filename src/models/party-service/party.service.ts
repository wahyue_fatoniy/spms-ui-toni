import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class PartyService {

  constructor() { }
}

export function setPartyType(value: Object): string {
  if (value['party'] !== null) {
    if (
      value['party']['birthDate'] === undefined ||
      value['party']['gender'] === undefined
    ) {
      return 'ORGANIZATION';
    } else { return 'PERSON'; }
  } else { return 'ORGANIZATION'; }
}
