import { Party } from '../party-service/party.model';
import { DatePipe } from '@angular/common';
import { Role } from '../role-service/role.model';

const datePipe: DatePipe = new DatePipe('id');

export class Person extends Party {
  protected id: number;
  private firstName: string;
  private midName: string;
  private lastName: string;
  private gender: string;
  private birthDate: string;
  private roles: any[];

  constructor(input: Object) {
    if (input) {
      const party = input ? new Party('PERSON', input) : {};
      super('PERSON', party);
      this.id = input['id'];
      this.gender = input['gender'];
      this.birthDate = this.setDate(input['birthDate']);
      this.setName = input['name'];
      this.roles = input['roles'] ? input['roles'] : [];
    }
  }

  public get getId() {
    return this.id;
  }

  public get getFirstName() {
    return this.firstName;
  }

  public get getLastName() {
    return this.lastName;
  }

  public get getGender() {
    return this.gender;
  }

  public get getBirthDate() {
    return this.birthDate;
  }

  public get getRoles() {
    return this.roles ? this.roles : [];
  }

  public set setRoles(roles: any[]) {
    this.roles = roles;
  }

  private setDate(date: string | Date): string {
    return datePipe.transform(date, 'yyyy-MM-ddTHH:mm:ssZ', '+0000');
  }

  private set setName(value: string) {
    if (value) {
      const names = value.split(' ');
      const set = name => (name ? name : ' ');
      (this.firstName = set(names.shift())),
        (this.lastName = set(names.pop())),
        (this.midName = set(names.join(' ')));
    }
  }
}
