import { Injectable } from '@angular/core';
import { Person } from './person.model';
import { Observable, BehaviorSubject } from 'rxjs';
import { map } from 'rxjs/operators';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../environments/environment';

const URL = '/api/persons';

@Injectable({
  providedIn: 'root'
})
export class PersonService {
  private person: BehaviorSubject<Person[]> = new BehaviorSubject<Person[]>([]);
  private data: Array<Person> = [];

  constructor(public http: HttpClient) {}

  public get getPerson(): Observable<Person[]> {
    return this.person.asObservable();
  }

  public get getAll(): Promise<Person[]> {
    return this.http
      .get(environment.apiUrl + URL)
      .pipe(
        map((personsMap: Array<Person>) => {
          this.data = personsMap.map(personMap => {
            return new Person(personMap);
          });
          this.person.next(this.data);
          return this.data;
        })
      )
      .toPromise();
  }

  public create(value: Person): Promise<Person> {
    return this.http
      .post(environment.apiUrl + URL, value)
      .pipe(
        map((personMap: Person) => {
          const person = new Person(personMap);
          this.data = [...this.data, person];
          this.person.next(this.data);
          return person;
        })
      )
      .toPromise();
  }

  public update(value: Person): Promise<Person> {
    return this.http
      .put(`${environment.apiUrl + URL}/${value['id']}`, value)
      .pipe(
        map((personMap: Person) => {
          const person = new Person(personMap);
          this.data = this.data.map(
            mapData => (mapData['id'] === personMap['id'] ? person : mapData)
          );
          this.person.next(this.data);
          return person;
        })
      )
      .toPromise();
  }

  public delete(value: Person): Promise<number> {
    return this.http
      .delete(`${environment.apiUrl + URL}/${value['id']}`)
      .pipe(
        map((personId: number) => {
          this.data = this.data.filter(result => result['id'] !== personId);
          this.person.next(this.data);
          return personId;
        })
      )
      .toPromise();
  }

  public personExist<T>(person: Person): Promise<Person> {
    return new Promise((resolve, reject) => {
      this.getAll.then(value => {
        const identity = person.getBirthDate + person.getName;
        let personData: Person[] = [];
        personData = value.filter((val: Person) => {
          val['identity'] = val.getBirthDate + val.getName;
          return val['identity'] === identity;
        });
        if (personData.length === 0) {
          this.create(person).then(val => {
            resolve(val);
          });
        } else {
          resolve(personData[0]);
        }
      });
    });
  }
}
