import { Role } from '../role-service/role.model';

export class PIC extends Role {
  protected id: number;
  private nip: number;
  constructor(input: Object) {
    super('CORDINATOR', input);
    this.id = input['id'];
    this.nip = input['nip'];
  }
}
