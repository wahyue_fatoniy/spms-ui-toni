import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { BehaviorSubject } from 'rxjs';
import { PIC } from './pic.model';

@Injectable({
  providedIn: 'root'
})
export class PicService {
  constructor(private http: HttpClient) { }
  private pic: BehaviorSubject<PIC[]> = new BehaviorSubject<PIC[]>([]);
  private data: PIC[] = [];
}
