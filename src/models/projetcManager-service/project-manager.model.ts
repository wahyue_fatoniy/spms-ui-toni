import { Role } from '../role-service/role.model';
import { Region } from '../region-service/region.model';
import { Person } from '../person-service/person.model';

export class ProjectManager extends Role {
  private nip: number;
  private region: Region;

  constructor(input: Object) {
    if (input) {
      input = { ...input, party: new Person(input['party'])};
      input['party']['partyType'] = 'PERSON';
      super('PROJECT_MANAGER', input);
      this.nip = input['nip'];
      this.region = new Region(input['region']);
    }
  }

  public get getNip() {
    return this.nip;
  }

  public get getRegion() {
    return this.region;
  }
}
