import { Injectable } from '@angular/core';
import { environment } from '../../environments/environment';
import { BehaviorSubject, Observable, Subscription } from 'rxjs';
import { ProjectManager } from './project-manager.model';
import { HttpClient } from '@angular/common/http';
import { PersonService } from '../person-service/person.service';
import { map } from 'rxjs/operators';
import { Person } from '../person-service/person.model';

const URL = `${environment.apiUrl}/api/project_managers`;

@Injectable({
  providedIn: 'root'
})
export class ProjectManagerService {
  private projectManager: BehaviorSubject<
    ProjectManager[]
  > = new BehaviorSubject<ProjectManager[]>([]);
  private data: Array<ProjectManager> = [];

  constructor(private http: HttpClient, private personService: PersonService) {
    this.onGetData();
  }

  public get connect(): Observable<ProjectManager[]> {
    return this.projectManager.asObservable();
  }

  public disconnect(subscriptions: Subscription): void {
    if (subscriptions !== undefined) {
      if (subscriptions['closed'] === false) {
        subscriptions.unsubscribe();
      }
    }
  }

  private onGetData(): void {
    this.http
      .get(URL)
      .pipe(
        map((coordinatorsMap: Array<ProjectManager>) => {
          this.data = coordinatorsMap.map(coordinatorMap => {
            return new ProjectManager(coordinatorMap);
          });
          this.projectManager.next(this.data);
        })
      )
      .toPromise()
      .catch(val => console.log(val));
  }

  public findByPartyId(id: number): Observable<ProjectManager> {
    return this.http.get(URL + `/party/${id}`)
    .pipe(
      map((pm: Object) => {
        return new ProjectManager(pm);
      })
    );
  }

  public create(value: ProjectManager): Promise<ProjectManager> {
    return new Promise((resolve, reject) => {
      this.personService
        .create(new Person(value['party']))
        .then((person: Person) => {
          value['party'] = person;
          this.http
            .post(URL, value)
            .pipe(
              map((coordinatorMap: ProjectManager) => {
                value['id'] = coordinatorMap['id'];
                this.data = [...this.data, new ProjectManager(value)];
                this.projectManager.next(this.data);
                resolve(new ProjectManager(value));
              })
            )
            .toPromise()
            .catch(err => reject(err));
        })
        .catch(err => reject(err));
    });
  }

  public update(value: ProjectManager): Promise<ProjectManager> {
    return new Promise((resolve, reject) => {
      this.personService
        .update(new Person(value['party']))
        .then(() => {
          this.http
            .put(`${URL}/${value['id']}`, value)
            .pipe(
              map((coordinatorMap: ProjectManager) => {
                this.data = this.data.map(
                  dataMap =>
                    dataMap['id'] === coordinatorMap['id'] ? value : dataMap
                );
                this.projectManager.next(this.data);
                resolve(value);
              })
            )
            .toPromise()
            .catch(err => reject(err));
        })
        .catch(err => reject(err));
    });
  }

  public delete(value: ProjectManager): Promise<number> {
    return this.http
      .delete(`${URL}/${value['id']}`)
      .pipe(
        map((pmId: number) => {
          this.data = this.data.filter(
            (result: ProjectManager) => result.getId !== pmId
          );
          this.projectManager.next(this.data);
          return pmId;
        })
      )
      .toPromise();
  }
}
