import { Customer } from '../customer-service/customer.model';
import { Sales } from '../sales-service/sales.model';
import { SiteWork } from '../siteWork-service/siteWork.model';
import { Partner } from '../partner-service/partner.model';
import { DatePipe } from '@angular/common';

const datePipe: DatePipe = new DatePipe('id');

export class PurchaseOrder {
  private id: number;
  private poNumber: number;
  private poValue: number;
  private publishDate: string;
  private expiredDate: string;
  private siteWorks: Array<SiteWork>;
  private customer: Customer;
  private sales: Sales;
  private partner: Partner;
  private totalInvoice: number;

  constructor(input: Object) {
    if (input !== undefined) {
      (this.id = input['id']), (this.poNumber = input['poNumber']);
      this.poValue = input['poValue'];
      this.publishDate = this.setDate(input['publishDate']);
      this.expiredDate = this.setDate(input['expiredDate']);
      this.siteWorks =
        input['siteWorks'] instanceof Array
          ? input['siteWorks'].map(project => new SiteWork(project))
          : [];
      this.customer = new Customer(input['customer']);
      this.sales = new Sales(input['sales']);
      this.partner = new Partner(input['partner']);
      this.totalInvoice = input['totalInvoice'] ? input['totalInvoice'] : 0;
    }
  }

  public get getId() {
    return this.id;
  }

  public get getPoNumber() {
    return this.poNumber;
  }

  public get getPoValue() {
    return this.poValue;
  }

  public get getPublishDate() {
    return this.publishDate;
  }

  public get getExpiredDate() {
    return this.expiredDate;
  }

  public get getSiteWorks() {
    return this.siteWorks;
  }

  public get getCustomer() {
    return this.customer;
  }

  public get getSales() {
    return this.sales;
  }

  public get getPartner() {
    return this.partner;
  }

  public get getTotalInvoice() {
    return this.totalInvoice;
  }

  private setDate(date: string | Date): string {
    return datePipe.transform(date, 'yyyy-MM-ddTHH:mm:ssZ', '+0000');
  }
}
