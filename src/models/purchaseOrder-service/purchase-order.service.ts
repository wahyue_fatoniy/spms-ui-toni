import { Injectable } from '@angular/core';
import { Observable, of, BehaviorSubject } from 'rxjs';
import { map } from 'rxjs/operators';
import { PurchaseOrder } from './purchase-order.model';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../environments/environment';
const URL = '/api/purchase_orders';

@Injectable({
  providedIn: 'root'
})
export class PurchaseOrderService {
  private purchaseOrder: BehaviorSubject<PurchaseOrder[]> = new BehaviorSubject<
    PurchaseOrder[]
  >([]);
  private data: Array<PurchaseOrder> = [];

  constructor(private http: HttpClient) {}

  public get getPurchaseOrder(): Observable<PurchaseOrder[]> {
    return this.purchaseOrder.asObservable();
  }

  public get getAll(): Promise<PurchaseOrder[]> {
    return this.http.get(environment.apiUrl + URL).pipe(
      map((purchaseOrdersMap: Array<PurchaseOrder>) => {
        console.log(purchaseOrdersMap);
        this.data = purchaseOrdersMap.map(arrMap => {
          return new PurchaseOrder(arrMap);
        });
        console.log(this.data);
        this.purchaseOrder.next(this.data);
        return this.data;
      })
    )
    .toPromise();
  }

  public create(value: Object): Promise<PurchaseOrder> {
    return this.http
      .post(environment.apiUrl + URL, value)
      .pipe(
        map((purchaseOrderMap: PurchaseOrder) => {
          const PO = new PurchaseOrder(purchaseOrderMap);
          this.data = [...this.data, PO];
          this.purchaseOrder.next(this.data);
          return PO;
        })
      )
      .toPromise();
  }

  public update(value: PurchaseOrder): Promise<PurchaseOrder> {
    return this.http
      .put(`${environment.apiUrl + URL}/${value['id']}`, value)
      .pipe(
        map((purchaseOrderMap: PurchaseOrder) => {
          const PO = new PurchaseOrder(purchaseOrderMap);
          this.data = this.data.map(
            dataMap => dataMap['id'] === PO.getId ? PO : dataMap
          );
          this.purchaseOrder.next(this.data);
          return PO;
        })
      )
      .toPromise();
  }

  delete(id: number): Promise<number> {
    return this.http
      .delete(`${environment.apiUrl + URL}/${id}`)
      .pipe(
        map((PoId: number) => {
          this.data = this.data.filter(
            result => (result.getId !== PoId)
          );
          this.purchaseOrder.next(this.data);
          return PoId;
        })
      )
      .toPromise();
  }
}
