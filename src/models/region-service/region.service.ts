import { Injectable } from '@angular/core';
import { Observable, BehaviorSubject } from 'rxjs';
import { map } from 'rxjs/operators';
import { HttpClient } from '@angular/common/http';
import { Region } from './region.model';
import { environment } from '../../environments/environment';

const URL = '/api/regions';

@Injectable({
  providedIn: 'root'
})
export class RegionService {
  private region: BehaviorSubject<Region[]> = new BehaviorSubject<Region[]>([]);
  private data: Array<Region> = [];

  constructor(private http: HttpClient) {
    this.getAll.then();
  }

  public get getRegion(): Observable<Region[]> {
    return this.region.asObservable();
  }

  public get getAll(): Promise<Region[]> {
    return this.http.get(environment.apiUrl + URL).pipe(
      map((regionsMap: Array<Region>) => {
        this.data = regionsMap.map(regionMap => new Region(regionMap));
        this.region.next(this.data);
        return this.data;
      })
    ).toPromise();
  }

  public create(value: Region): Promise<Region> {
    return this.http
      .post(environment.apiUrl + URL, value)
      .pipe(
        map((regionMap: Region) => {
          const region = new Region(regionMap);
          this.data = [...this.data, region];
          this.region.next(this.data);
          return region;
        })
      )
      .toPromise();
  }

  public update(value: Region): Promise<Region> {
    return this.http
      .put(`${environment.apiUrl + URL}/${value['id']}`, value)
      .pipe(
        map((regionMap: Region) => {
          const region = new Region(regionMap);
          this.data = this.data.map(
            mapData =>
              mapData['id'] === regionMap['id']
                ? region
                : mapData
          );
          this.region.next(this.data);
          return region;
        })
      )
      .toPromise();
  }

  public delete(id: number): Promise<number> {
    return this.http
      .delete(`${environment.apiUrl + URL}/${id}`)
      .pipe(
        map((regionId: number) => {
          this.data = this.data.filter(
            result => (result['id'] !== regionId)
          );
          this.region.next(this.data);
          return regionId;
        })
      )
      .toPromise();
  }
}
