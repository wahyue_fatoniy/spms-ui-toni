import { Role } from '../role-service/role.model';
import { Region } from '../region-service/region.model';

export class Resources extends Role {
  protected id: number;
  private resourceType: string;
  private nip: number;
  private region: Region;

  constructor(resourceType: string, input: Object) {
    super(resourceType, input);
    if (input !== undefined) {
        this.id = input['id'];
        this.nip = input['nip'];
        this.region = new Region(input['region']);
    }
    this.resourceType = resourceType;
  }

  public get getId() {
    return this.id;
  }

  public get getNip() {
    return this.nip;
  }

  public get getRegion() {
    return this.region;
  }

  public get getResourceType() {
    return this.resourceType;
  }
}
