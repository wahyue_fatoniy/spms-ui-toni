import { Party } from '../party-service/party.model';
import { Organization } from '../organization-service/organization.model';
import { Person } from '../person-service/person.model';
import { DatePipe } from '@angular/common';

const datePipe: DatePipe = new DatePipe('id');

export class Role {
  protected id: number;
  private roleType: string;
  private party: Party;
  private startDate: string;
  private endDate: string;

  constructor(roleType: string, input: Object) {
    this.roleType = roleType;
    if (input) {
      this.id = input['id'];
      this.party = this.setParty(input['party']);
      this.startDate = this.setDate(new Date());
      this.endDate = input['endDate'];
    }
  }


  private setDate(date: string | Date): string {
    return datePipe.transform(date, 'yyyy-MM-ddTHH:mm:ssZ', '+0000');
  }

  public get getId() {
    return this.id;
  }

  public get getRoleType() {
    return this.roleType;
  }

  public get getParty() {
    return this.party;
  }

  public set getParty(party: Party) {
    this.party = this.setParty(party);
  }

  public get getStartDate() {
    return this.startDate;
  }

  public get getEndDate() {
    return this.endDate;
  }

  private setParty(party: Party): Party {
    if (party) {
      if (party['partyType'] === 'PERSON') {
        return new Person(party);
      } else if (party['partyType'] === 'ORGANIZATION') {
        return new Organization(party);
      } else {
        return party;
      }
    } else { return party; }
  }
}
