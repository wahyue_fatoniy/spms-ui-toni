import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Admin } from '../admin-service/admin.model';
import { Coordinator } from '../coordinatorService/coordinator.model';
import { environment } from '../../environments/environment';
import { map } from 'rxjs/operators';
import { ProjectManager } from '../projetcManager-service/project-manager.model';
import { Sales } from '../sales-service/sales.model';
import { Finance } from '../finance-service/finance.model';
import { Customer } from '../customer-service/customer.model';
import { Role } from './role.model';
import { Partner } from '../partner-service/partner.model';
import { Engineer } from '../engineer-service/engineer.model';
import { Technician } from '../technician-service/technician.model';
import { Documentation } from '../documentation-service/documentation.model';
import { GeneralManager } from 'src/app/models/generalManager.model';
import { AdminCenter } from 'src/app/models/adminCenter.model';

interface ConfigRole {
  url: string;
  model: any;
}

interface ConfigUrl {
  ADMIN: ConfigRole;
  CORDINATOR: ConfigRole;
  PROJECT_MANAGER: ConfigRole;
  SALES: ConfigRole;
  FINANCE: ConfigRole;
  CUSTOMER: ConfigRole;
  PARTNER: ConfigRole;
  ENGINEER: ConfigRole;
  TECHNICIAN: ConfigRole;
  DOCUMENTATION: ConfigRole;
  SUBCONT: ConfigRole;
  SUBCONT_ENGINEER: ConfigRole;
  SUBCONT_TECHNICIAN: ConfigRole;
  SUBCONT_DOCUMENTATION: ConfigRole;
  GENERAL_MANAGER: ConfigRole;
  ADMIN_CENTER: ConfigRole;
}

const host = environment.apiUrl;

@Injectable({
  providedIn: 'root'
})
export class RoleService {
  private configUrl: ConfigUrl = {
    ADMIN: { url: host + '/api/admins', model: Admin },
    CORDINATOR: { url: host + '/api/internal_cordinators', model: Coordinator },
    PROJECT_MANAGER: {
      url: host + '/api/project_managers',
      model: ProjectManager
    },
    SALES: {
      url: host + '/api/sales',
      model: Sales
    },
    FINANCE: {
      url: host + '/api/finances',
      model: Finance
    },
    CUSTOMER: {
      url: host + '/api/customers',
      model: Customer
    },
    PARTNER: {
      url: host + '/api/partners',
      model: Partner
    },
    ENGINEER: {
      url: host + '/api/internal_engineers',
      model: Engineer
    },
    TECHNICIAN: {
      url: host + '/api/internal_technicians',
      model: Technician
    },
    DOCUMENTATION: {
      url: host + '/api/internal_documentations',
      model: Documentation
    },
    SUBCONT: {
      url: host + '/api/subcont_cordinators',
      model: Coordinator
    },
    SUBCONT_ENGINEER: {
      url: host + '/api/subcont_engineers',
      model: Engineer
    },
    SUBCONT_TECHNICIAN: {
      url: host + '/api/subcont_technicians',
      model: Technician
    },
    SUBCONT_DOCUMENTATION: {
      url: host + '/api/subcont_documentations',
      model: Documentation
    },
    GENERAL_MANAGER: {
      url: host + '/api/general_managers',
      model: GeneralManager
    },
    ADMIN_CENTER: {
      url: host + '/api/admin_centers',
      model: AdminCenter
    }
  };

  constructor(private http: HttpClient) { }

  public create(data: Role[]): Promise<Role[]> {
    return new Promise((resolve, reject) => {
      for (let role of data) {
        console.log(role);
        const URL = this.setUrl(role);
        this.http
          .post(URL, role)
          .pipe(
            map(roleVal => {
              resolve(new (this.configUrl[role['roleType']].model)(roleVal));
            })
          )
          .toPromise()
          .catch(err => reject(err));
      }
    });
  }

  public update(data: Role[]): Promise<Role[]> {
    return new Promise((resolve, reject) => {
      for (let role of data) {
        const URL = this.setUrl(role);
        this.http
          .put(URL + `/${role['id']}`, role)
          .pipe(
            map(roleVal => {
              resolve(new (this.configUrl[role['roleType']].model)(roleVal));
            })
          )
          .toPromise()
          .catch(err => reject(err));
      }
    });
  }

  private setUrl(role: Role): string {
    let url: string;
    let valNIP = isNIP();
    switch (role['roleType']) {
      case 'ENGINEER':
        url = this.configUrl.SUBCONT_ENGINEER.url;
        if (valNIP) {
          url = this.configUrl.ENGINEER.url;
        }
        break;
      case 'TECHNICIAN':
        url = this.configUrl.SUBCONT_TECHNICIAN.url;
        if (valNIP) {
          url = this.configUrl.TECHNICIAN.url;
        }
        break;
      case 'DOCUMENTATION':
        url = this.configUrl.SUBCONT_DOCUMENTATION.url;
        if (valNIP) {
          url = this.configUrl.DOCUMENTATION.url;
        }
        break;
      case 'CORDINATOR':
        url = this.configUrl.SUBCONT.url;
        if (valNIP) {
          url = this.configUrl.CORDINATOR.url;
        }
        break;
      default:
        url = this.configUrl[role['roleType']].url;
        break;
    }

    function isNIP(): boolean {
      if (role['nip'] !== undefined && role['nip'] !== null) {
        return true;
      }
      return false;
    }
    return url;
  }
}
