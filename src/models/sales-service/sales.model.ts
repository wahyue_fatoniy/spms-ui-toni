import { Role } from '../role-service/role.model';
import { Person } from '../person-service/person.model';

export class Sales extends Role {
  protected id: number;
  private nip: number;

  constructor(input: Object) {
    if (input) {
      input = { ...input, party: new Person(input['party'])};
      input['party']['partyType'] = 'PERSON';
      super('SALES', input);
      this.id = input['id'];
      this.nip = input['nip'];
    }
  }

  public get getId() {
    return this.id;
  }

  public get getNip() {
    return this.nip;
  }
}
