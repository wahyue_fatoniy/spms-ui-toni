import { Injectable } from '@angular/core';
import { Observable, BehaviorSubject, Subscription } from 'rxjs';
import { map } from 'rxjs/operators';
import { Sales } from './sales.model';
import { HttpClient } from '@angular/common/http';
import { PersonService } from '../person-service/person.service';
import { Person } from '../person-service/person.model';
import { environment } from '../../environments/environment';

const URL = '/api/sales';

@Injectable({
  providedIn: 'root'
})
export class SalesService {
  private sales: BehaviorSubject<Sales[]> = new BehaviorSubject<Sales[]>([]);
  private data: Array<Sales> = [];

  constructor(
    private http: HttpClient,
    private personService: PersonService,
  ) {
    this.onGetData();
  }

  public get connect(): Observable<Sales[]> {
    return this.sales.asObservable();
  }

  public disconnect(subscriptions: Subscription): void {
    if (subscriptions !== undefined) {
      if (subscriptions['closed'] === false) {
        subscriptions.unsubscribe();
      }
    }
  }

  private onGetData(): void {
    this.http
      .get(environment.apiUrl + URL)
      .pipe(
        map((salesMap: Array<Sales>) => {
          this.data = salesMap.map(sales => {
            return new Sales(sales);
          });
          console.log(this.data);
          this.sales.next(this.data);
        })
      )
      .toPromise()
      .catch(err => console.log(err));
  }

  public create(value: Sales): Promise<Sales> {
    return new Promise((resolve, reject) => {
      this.personService
        .create(new Person(value['party']))
        .then((person: Person) => {
          value['party'] = person;
          this.http
            .post(environment.apiUrl + URL, value)
            .pipe(
              map((salesMap: Sales) => {
                value['id'] = salesMap['id'];
                this.data = [...this.data, new Sales(value)];
                this.sales.next(this.data);
                resolve(new Sales(value));
              })
            )
            .toPromise()
            .catch(err => reject(err));
        })
        .catch(err => reject(err));
    });
  }

  public update(value: Sales): Promise<Sales> {
    return new Promise((resolve, reject) => {
      this.personService
        .update(new Person(value['party']))
        .then(() => {

          this.http
            .put(`${environment.apiUrl + URL}/${value['id']}`, value)
            .pipe(
              map((salesMap: Sales) => {
                this.data = this.data.map(
                  dataMap =>
                    dataMap['id'] === salesMap['id'] ? value : dataMap
                );
                this.sales.next(this.data);
                resolve(value);
              })
            )
            .toPromise()
            .catch(err => reject(err));

          })
        .catch(err => reject(err));
    });
  }

  public delete(value: Sales): Promise<number> {
    return new Promise((resolve, reject) => {
      this.http
        .delete(`${environment.apiUrl + URL}/${value['id']}`)
        .toPromise()
        .then((salesId: number) => {
          this.personService
            .delete(new Person(value['party']))
            .catch(err => reject(err));
          this.data = this.data.filter((result: Sales) => result.getId !== salesId);
          this.sales.next(this.data);
          resolve(salesId);
        })
        .catch(err => reject(err));
    });
  }
}
