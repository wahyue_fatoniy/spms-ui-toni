import { Injectable } from '@angular/core';
import { Observable, BehaviorSubject } from 'rxjs';
import { map } from 'rxjs/operators';
import { Site } from './site.model';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../environments/environment';

const URL = '/api/sites';

@Injectable({
  providedIn: 'root'
})
export class SiteService {
  private site: BehaviorSubject<Site[]> = new BehaviorSubject<Site[]>([]);
  private data: Array<Site> = [];

  constructor(private http: HttpClient) {}

  public get getSite(): Observable<Site[]> {
    return this.site.asObservable();
  }

  public get getAll(): Promise<Site[]> {
    return this.http.get(environment.apiUrl + URL).pipe(
      map((sitesMap: Array<Site>) => {
        console.log(sitesMap);
        this.data = sitesMap.map(siteMap => {
          return new Site(siteMap);
        });
        this.site.next(this.data);
        return this.data;
      })
    ).toPromise();
  }

  public create(value: Site): Promise<Site> {
    return this.http.post(environment.apiUrl + URL, value).pipe(
      map((siteMap: Site) => {
        const site = new Site(siteMap);
        this.data = [...this.data, site];
        this.site.next(this.data);
        return site;
      })
    ).toPromise();

  }

  public update(value: Site): Promise<Site> {
    return this.http
      .put(`${environment.apiUrl + URL}/${value['id']}`, value)
      .pipe(
        map((siteMap: Site) => {
          const site = new Site(siteMap);
          this.data = this.data.map(
            mapData =>
              mapData['id'] === siteMap['id']
                ? site
                : mapData
          );
          this.site.next(this.data);
          return site;
        })
      )
      .toPromise();
  }

  public delete(id: number): Promise<number> {
    return this.http
      .delete(`${environment.apiUrl + URL}/${id}`)
      .pipe(
        map((siteId: number) => {
          this.data = this.data.filter(
            result => (result['id'] !== siteId)
          );
          this.site.next(this.data);
          return siteId;
        })
      )
      .toPromise();
  }
}
