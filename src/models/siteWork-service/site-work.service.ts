import { Injectable } from '@angular/core';
import { Observable, BehaviorSubject, forkJoin } from 'rxjs';
import { map } from 'rxjs/operators';
import { SiteWork } from './siteWork.model';
import { HttpClient } from '@angular/common/http';
import { TaskService } from '../task-service/task.service';
import { Task } from '../task-service/task.model';
import { environment } from '../../environments/environment';
import { CoreService } from '../../app/shared/core-service';
import { Admin } from '../admin-service/admin.model';
import { ProjectManager } from 'src/app/shared/models/project-manager/project-manager';

const URL = '/api/site_works';

@Injectable({
  providedIn: 'root'
})
export class SiteWorkService {
  private siteWork: BehaviorSubject<SiteWork[]> = new BehaviorSubject<
    SiteWork[]
  >([]);
  private data: Array<SiteWork> = [];

  constructor(private http: HttpClient, private serviceTask: TaskService) { }

  public get getSiteWork(): Observable<SiteWork[]> {
    return this.siteWork.asObservable();
  }

  public get getAll(): Promise<SiteWork[]> {
    return this.http.get(environment.apiUrl + URL)
      .pipe(
        map((resultMap: SiteWork[]) => {
          this.data = resultMap.map(siteWork => new SiteWork(siteWork));
          this.siteWork.next(this.data);
          return this.data;
        })
      )
      .toPromise();
  }

  public findByPicParty(id: number): Observable<SiteWork[]> {
    return this.http.get(`${environment.apiUrl + URL}/cordinator/party/${id}`)
    .pipe(
      map((siteWork: Object[]) => {
        return siteWork.map(siteWorkVal => new SiteWork(siteWorkVal));
      })
    );
  }

  public findByAdminParty(id: number): Observable<SiteWork[]> {
    return this.http.get(`${environment.apiUrl + URL}/admin/party/${id}`)
    .pipe(
      map((siteWork: Object[]) => {
        return siteWork.map(siteWorkVal => new SiteWork(siteWorkVal));
      })
    );
  }

  public findById(id: number): Observable<SiteWork[]> {
    return this.http.get(environment.apiUrl + URL)
      .pipe(
        map((resultMap: SiteWork[]) => {
          this.data = resultMap.map(siteWork => new SiteWork(siteWork)).
          filter(val => val.getId === id);
          return this.data;
        })
      );
  }

  public findByPmParty(partyId: number): Promise<SiteWork[]> {
    return this.http.get(`${environment.apiUrl + URL}/project_manager/party/${partyId}`).pipe(
      map((val: SiteWork[]) => {
        this.data = val.map(siteWork => new SiteWork(siteWork));
        this.siteWork.next(this.data);
        return this.data;
      })
    ).toPromise();
  }

  public create(value: SiteWork): Promise<SiteWork> {
    return this.http
      .post(environment.apiUrl + URL, value)
      .pipe(
        map((siteWorkMap: SiteWork) => {
          const siteWork = new SiteWork(siteWorkMap);
          this.data = [...this.data, siteWork];
          this.siteWork.next(this.data);
          return siteWork;
        })
      )
      .toPromise();
  }

  public cancel(siteWorkId: number): Promise<SiteWork> {
    return this.http
      .post(`${environment.apiUrl + URL}/${siteWorkId}/cancel`, {})
      .pipe(
        map((siteWorkMap: SiteWork) => {
          const siteWork = new SiteWork(siteWorkMap);
          this.data = this.data.map(
            mapData =>
              mapData.getId === siteWorkMap['id'] ? siteWork : mapData
          );
          this.siteWork.next(this.data);
          return siteWork;
        })
      )
      .toPromise();
  }

  public update(value: SiteWork): Promise<SiteWork[]> {
    return this.http.put(`${environment.apiUrl + URL}/${value.getId}`, value).pipe(
      map((siteWork: SiteWork) => {
        this.data = this.data.map(val => val.getId === SiteWork['id'] ? siteWork : val);
        this.siteWork.next(this.data);
        return this.data;
      })
    ).toPromise();
  }

  public delete(id: number): Promise<number> {
    return this.http
      .delete(`${environment.apiUrl + URL}/${id}`)
      .pipe(
        map((siteWorkId: number) => {
          this.data = this.data.filter(
            result => (result.getId !== siteWorkId)
          );
          this.siteWork.next(this.data);
          return siteWorkId;
        })
      )
      .toPromise();
  }

  public findByPMs(pms: ProjectManager[]): Observable<SiteWork[]> {
    return this.http.post(`${environment.apiUrl + URL}/project_managers`, pms).pipe(
      map((result: any) => {
        return result.map(_project => new SiteWork(_project));
      })
    );
  }

}
