import { Site } from '../site-service/site.model';
import { Status, Activity, ActivityDoc } from '../enum/enum';
import { DatePipe } from '@angular/common';
import { Task } from '../task-service/task.model';
import { SowType } from '../sowType-service/sow-type.model';
import { Coordinator } from '../coordinatorService/coordinator.model';
import { Subcont } from '../subcont-service/subcont.model';
import { map } from 'rxjs/operators';
import { setPartyType } from '../party-service/party.service';
import { Engineer } from '../engineer-service/engineer.model';
import { Technician } from '../technician-service/technician.model';
import { Documentation } from '../documentation-service/documentation.model';
import { Team } from '../team-service/team.model';
import { Admin } from '../admin-service/admin.model';
import { ProjectManager } from '../projetcManager-service/project-manager.model';

const datePipe: DatePipe = new DatePipe('id');

export class SiteWork {
  private id: number;
  private cordinator: Coordinator | Subcont;
  private projectCode: string;
  private site: Site;
  private status: string;
  private startDate: string;
  private endDate: string;
  private tasks: Array<Task>;
  private total: number;
  private poValue: number;
  private projectManager: ProjectManager;
  private team: Team;
  private admin: Admin;

  constructor(input: Object) {
    if (input) {
      this.id = input['id'];
      this.cordinator = this.SetCordinatorParty(input['cordinator']);
      this.projectCode = input['projectCode'];
      this.site = input['site'] ? new Site(input['site']) : new Site();
      this.status = Status[input['status']];
      this.startDate = this.setDate(input['startDate']);
      this.endDate = this.setDate(input['endDate']);
      this.tasks = input['tasks']
        ? input['tasks'].map(task => new Task(task))
        : [];
      this.total = input['budget'] ? input['budget'] : 0;
      this.poValue = input['poValue'] ? input['poValue'] : 0;
      this.projectManager = new ProjectManager(input['projectManager']);
      this.team = new Team(input['team']);
      this.admin = new Admin(input['admin']);
    }
  }

  public get isFinished() {
    let retVal = false;
    if (this.status === 'DONE') {
      retVal = true;
    }
    return retVal;
  }

  public get isCancel() {
    let retVal = false;
    if (this.status === 'CANCEL') {
      retVal = true;
    }
    return retVal;
  }

  public get getId() {
    return this.id;
  }

  public get getCoordinator() {
    return this.cordinator;
  }

  public get getProjectCode() {
    return this.projectCode;
  }

  public get getSite() {
    return this.site;
  }

  public get getStatus() {
    return this.status;
  }

  public get getStartDate() {
    return this.startDate;
  }

  public get getEndDate() {
    return this.endDate;
  }

  public get getTasks() {
    return this.tasks;
  }

  public get getTotal() {
    return this.total;
  }

  public get getPoValue() {
    return this.poValue;
  }

  public get getPM() {
    return this.projectManager;
  }

  public get getTeam() {
    return this.team;
  }

  public get getAdmin() {
    return this.admin;
  }

  private setDate(date: string | Date): string {
    return datePipe.transform(date, 'yyyy-MM-ddTHH:mm:ssZ', '+0000');
  }

  private SetCordinatorParty<T>(
    val: Coordinator | Subcont
  ): Coordinator | Subcont {
    if (val !== undefined) {
      val['party']['partyType'] = setPartyType(val);
      if (val['party']['partyType'] === 'PERSON') {
        val = new Coordinator(val);
      } else {
        val = new Subcont(val);
      }
      return val;
    }
  }
}
