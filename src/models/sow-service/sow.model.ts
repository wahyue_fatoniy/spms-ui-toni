import { Region } from '../region-service/region.model';
import { SowType } from '../sowType-service/sow-type.model';

export class SoW {
  private id: number;
  private sowType: SowType;

  constructor(input: Object) {
    if (input !== undefined) {
      this.id = input['id'];
      this.sowType = new SowType(input['sowType']);
    }
  }

  get getId() {
    return this.id;
  }

  get getSowType() {
    return this.sowType;
  }
}
