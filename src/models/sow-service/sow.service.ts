import { Injectable } from '@angular/core';
import { Observable, BehaviorSubject, of } from 'rxjs';
import { map } from 'rxjs/operators';
import { HttpClient } from '@angular/common/http';
import { SoW } from './sow.model';
import { environment } from '../../environments/environment';
const URL = '/api/sows';

@Injectable({
  providedIn: 'root'
})
export class SowService {

  private sow: BehaviorSubject<SoW[]> = new BehaviorSubject<SoW[]>([]);
  private data: Array<SoW> = [];

  constructor(private http: HttpClient) {}

  public get getSow(): Observable<SoW[]> {
    return this.sow.asObservable();
  }

  public get getAll(): Promise<SoW[]> {
    return this.http
      .get(environment.apiUrl + URL)
      .pipe(
        map((sowsMap: Array<SoW>) => {
          this.data = sowsMap.map(sowMap => {
            return new SoW(sowMap);
          });
          this.sow.next(this.data);
          return this.data;
        })
      )
      .toPromise();
  }

  create(value: SoW): Promise<SoW> {
    return this.http
      .post(environment.apiUrl + URL, value)
      .pipe(
        map((sowMap: SoW) => {
          const sow = new SoW(sowMap);
          this.data = [...this.data, sow];
          this.sow.next(this.data);
          return sow;
        })
      )
      .toPromise();
  }
}
