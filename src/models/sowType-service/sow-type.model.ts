import { Region } from '../region-service/region.model';

export class SowType {
  private id: number;
  private sowCode: string;
  private description: string;
  private price: number;
  private engineerDay: number;
  private technicianDay: number;
  private documentationDay: number;
  private region: Region;

  constructor(input: Object) {
    if (input !== undefined) {
      this.id = input['id'];
      this.sowCode = input['sowCode'];
      this.description = input['description'];
      this.price = input['price'];
      this.engineerDay = input['engineerDay'];
      this.technicianDay = input['technicianDay'];
      this.documentationDay = input['documentationDay'];
      this.region = new Region(input['region']);
    }
  }

  public get getId() {
    return this.id;
  }

  public get getSowCode() {
    return this.sowCode;
  }

  public get getDescription() {
    return this.description;
  }

  public get getPrice() {
    return this.price;
  }

  public get getEngineerday() {
    return this.engineerDay;
  }

  public get getTechnicianday() {
    return this.technicianDay;
  }

  public get getDocumentationday() {
    return this.documentationDay;
  }

  public get getRegion() {
    return this.region;
  }
}
