import { TestBed, inject } from '@angular/core/testing';

import { SowTypeService } from './sow-type.service';

describe('SowTypeService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [SowTypeService]
    });
  });

  it('should be created', inject([SowTypeService], (service: SowTypeService) => {
    expect(service).toBeTruthy();
  }));
});
