import { Injectable } from '@angular/core';
import { Observable, BehaviorSubject, of } from 'rxjs';
import { map } from 'rxjs/operators';
import { SowType } from '../sowType-service/sow-type.model';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../environments/environment';
const URL = '/api/sow_types';

@Injectable({
  providedIn: 'root'
})
export class SowTypeService {
  private sow: BehaviorSubject<SowType[]> = new BehaviorSubject<SowType[]>([]);
  private data: Array<SowType> = [];

  constructor(private http: HttpClient) {
    this.getAll.catch(err => console.log(err));
  }

  public get getData(): Observable<SowType[]> {
    return this.sow.asObservable();
  }

  public get getAll(): Promise<SowType[]> {
    return this.http
      .get(environment.apiUrl + URL)
      .pipe(
        map((sowsMap: Array<SowType>) => {
          this.data = sowsMap.map(sowMap => {
            return new SowType(sowMap);
          });
          this.sow.next(this.data);
          return this.data;
        })
      )
      .toPromise();
  }

  public create(value: SowType): Promise<SowType> {
    return this.http
      .post(environment.apiUrl + URL, value)
      .pipe(
        map((sowMap: SowType) => {
          const sow = new SowType(sowMap);
          this.data = [...this.data, sow];
          this.sow.next(this.data);
          return sow;
        })
      )
      .toPromise();
  }

  public update(value: SowType): Promise<SowType> {
    return this.http
      .put(`${environment.apiUrl + URL}/${value['id']}`, value)
      .pipe(
        map((sowMap: SowType) => {
          const sow = new SowType(sowMap);
          this.data = this.data.map(
            mapData => (mapData['id'] === sowMap['id'] ? sow : mapData)
          );
          this.sow.next(this.data);
          return sow;
        })
      )
      .toPromise();
  }

  public delete(id: number): Promise<number> {
    return this.http
      .delete(`${environment.apiUrl + URL}/${id}`)
      .pipe(
        map((sowId: number) => {
          this.data = this.data.filter(
            result => (result['id'] !== sowId)
          );
          this.sow.next(this.data);
          return sowId;
        })
      )
      .toPromise();
  }
}
