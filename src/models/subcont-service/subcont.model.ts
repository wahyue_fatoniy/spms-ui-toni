import { Role } from '../role-service/role.model';
import { Region } from '../region-service/region.model';

export class Subcont extends Role {
  private region: Region;
  constructor(input: Object) {
    if (input) {
        super('CORDINATOR', input);
        this.region = new Region(input['region']);
      }
  }

  public get getRegion() {
    return this.region;
  }
}
