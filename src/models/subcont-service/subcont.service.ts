import { Injectable } from '@angular/core';
import { Observable, BehaviorSubject, Subscription } from 'rxjs';
import { map } from 'rxjs/operators';
import { HttpClient } from '@angular/common/http';
import { OrganizationService } from '../organization-service/organization.service';
import { Organization } from '../organization-service/organization.model';
import { Subcont } from './subcont.model';
import { setPartyType } from '../party-service/party.service';
import { environment } from '../../environments/environment';

const URL = '/api/cordinators';

@Injectable({
  providedIn: 'root'
})
export class SubcontService {
  private subcont: BehaviorSubject<Subcont[]> = new BehaviorSubject<Subcont[]>(
    []
  );
  private data: Array<Subcont> = [];

  constructor(
    private http: HttpClient,
    private organizationService: OrganizationService
  ) {
    this.onGetData();
  }

  public get connect(): Observable<Subcont[]> {
    return this.subcont.asObservable();
  }

  public disconnect(subscriptions: Subscription): void {
    if (subscriptions !== undefined) {
      if (subscriptions['closed'] === false) {
        subscriptions.unsubscribe();
      }
    }
  }

  private onGetData(): void {
    this.http
      .get(environment.apiUrl + URL)
      .pipe(
        map((picMap: Subcont[]) => {
          picMap = picMap.filter(pic => pic['party'] !== null);
          this.data = picMap.map(pic => {
            pic['party']['partyType'] = setPartyType(pic);
            return new Subcont(pic);
          });
          this.data = this.data.filter(pic => pic['party']['partyType'] === 'ORGANIZATION');
          this.subcont.next(this.data);
        })
      )
      .toPromise()
      .catch(err => console.log(err));
  }

  public create(value: Subcont): Promise<Subcont> {
    return new Promise((resolve, reject) => {
      this.organizationService
        .create(new Organization(value['party']))
        .then((org: Organization) => {
          value['party'] = org;
          this.http
            .post(environment.apiUrl + URL, value)
            .pipe(
              map((subcontMap: Subcont) => {
                value['id'] = subcontMap['id'];
                this.data = [...this.data, new Subcont(value)];
                this.subcont.next(this.data);
                resolve(new Subcont(value));
              })
            )
            .toPromise()
            .catch(err => reject(err));
        })
        .catch(err => reject(err));
    });
  }

  public update(value: Subcont): Promise<Subcont> {
    return new Promise((resolve, reject) => {
      this.organizationService
        .update(new Organization(value['party']))
        .then(() => {
          this.http
            .put(`${environment.apiUrl + URL}/${value['id']}`, value)
            .pipe(
              map((subcontMap: Subcont) => {
                this.data = this.data.map(
                  dataMap =>
                    dataMap['id'] === subcontMap['id'] ? value : dataMap
                );
                this.subcont.next(this.data);
                resolve(value);
              })
            )
            .toPromise()
            .catch(err => reject(err));
        })
        .catch(err => reject(err));
    });
  }

  public delete(value: Subcont): Promise<number> {
    return new Promise((resolve, reject) => {
      this.http
        .delete(`${environment.apiUrl + URL}/${value['id']}`)
        .toPromise()
        .then((orgId: number) => {
          this.organizationService
            .delete(new Organization(value['party']))
            .catch(err => reject(err));
          this.data = this.data.filter((result: Subcont) => result.getId !== orgId);
          this.subcont.next(this.data);
          resolve(orgId);
        })
        .catch(err => reject(err));
    });
  }
}
