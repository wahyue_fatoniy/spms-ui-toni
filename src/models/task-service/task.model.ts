import { SoW } from '../sow-service/sow.model';
import { TaskType, Status, ActivityDays, Severity } from '../enum/enum';
import { SiteWork } from '../siteWork-service/siteWork.model';
import { DatePipe } from '@angular/common';
import { ExecutorDaysType } from '../interface/interface';
import { Activities } from '../activities-service/activites.model';

const datePipe: DatePipe = new DatePipe('id');

export class Task {
  private id: number;
  private siteWork: SiteWork;
  private sow: SoW;
  private taskType: TaskType;
  private activity: Activities;
  private manDays: number;
  private status: string;
  private severity: Severity;
  private startDate: string;
  private endDate: string;

  constructor(input: Object) {
    if (input !== undefined) {
      this.id = input['id'];
      this.siteWork = input['siteWork'];
      this.sow = new SoW(input['sow']);
      this.activity = new Activities(input['activity']);
      this.taskType = input['activity']['type'];
      this.manDays = this.setMandays();
      this.status = Status[input['status']];
      this.severity = input['severity'];
      this.startDate = this.setDate(input['startDate']);
      this.endDate = this.setDate(input['endDate']);
    }
  }

  public get getId() {
    return this.id;
  }

  public get getSiteWork() {
    return this.siteWork;
  }

  public get getSow() {
    return this.sow;
  }

  public set getSow(sow: Object) {
    this.sow = new SoW(sow);
  }

  public get getTaskType() {
    return this.taskType;
  }

  public get getManDays() {
    return this.manDays;
  }

  public get getStatus() {
    return this.status;
  }

  public get getSeverity() {
    return this.severity;
  }

  public get getStartDate() {
    return this.startDate;
  }

  public get getEndDate() {
    return this.endDate;
  }

  public get getActivity() {
    return this.activity;
  }

  private setDate(date: string | Date): string {
    return datePipe.transform(date, 'yyyy-MM-ddTHH:mm:ssZ', '+0000');
  }

  private setMandays(): number {
    let retVal: number;
    const executorDaysType: Array<ExecutorDaysType> = [
      { type: 'ENGINEER', mandaysType: 'engineerDay' },
      { type: 'TECHNICIAN', mandaysType: 'technicianDay' },
      { type: 'DOCUMENTATION', mandaysType: 'documentationDay' }
    ];
    executorDaysType.forEach(val => {
      if (val.type === this.taskType) {
        retVal =
          (parseInt(ActivityDays[this.activity.getName], 0) / 100) *
          this.sow.getSowType[val.mandaysType];
          retVal = parseFloat(retVal.toFixed(1));
      }
    });
    return retVal;
  }

  public isInitial(): boolean {
    let retVal = false;
    if (this.activity.getNumber === 0) {
      retVal = true;
    }
    return retVal;
  }

  public isNotStarted(): boolean {
    let retVal = false;
    if (this.status === 'NOT_STARTED') {
      retVal = true;
    }
    return retVal;
  }

  public isOnProgress(): boolean {
    let retVal = false;
    if (this.status === 'ON_PROGRESS') {
      retVal = true;
    }
    return retVal;
  }

  public isFinished(): boolean {
    let retVal = false;
    if (this.status === 'DONE') {
      retVal = true;
    }
    return retVal;
  }

  public isPredecessor(nextTask: Task): boolean {
    let retVal = false;
    if (
      this.activity.getType === nextTask.activity.getType &&
      this.sow['id'] === nextTask['sow']['id']
    ) {
      if (this.activity.getNumber === nextTask.activity.getNumber - 1) {
        retVal = true;
      }
    }
    return retVal;
  }

  public isPreceededBy(prevTask: Task): boolean {
    let retVal = false;
    if (this.activity.getType === prevTask.activity.getType &&
      this.sow['id'] === prevTask['sow']['id']
      ) {
      if (this.activity.getNumber === prevTask.activity.getNumber + 1) {
        retVal = true;
      }
    }
    return retVal;
  }
}
