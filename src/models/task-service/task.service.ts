import { Injectable } from '@angular/core';
import { Observable, BehaviorSubject } from 'rxjs';
import { map } from 'rxjs/operators';
import { Task } from './task.model';
import { HttpClient } from '@angular/common/http';
import { SowService } from '../sow-service/sow.service';
import { environment } from '../../environments/environment';

const URL = '/api/tasks';

@Injectable({
  providedIn: 'root'
})
export class TaskService {
  private task: BehaviorSubject<Task[]> = new BehaviorSubject<Task[]>([]);
  private data: Array<Task> = [];

  constructor(private http: HttpClient, private sowService: SowService) {
    this.getAll.catch(err => console.log(err));
  }

  public get getTask(): Observable<Task[]> {
    return this.task.asObservable();
  }

  public get getAll(): Promise<Task[]> {
    return this.http
      .get(environment.apiUrl + URL)
      .pipe(
        map((tasksMap: Array<Task>) => {
          this.data = tasksMap.map(taskMap => {
            return new Task(taskMap);
          });
          this.task.next(this.data);
          return this.data;
        })
      )
      .toPromise();
  }

  public create(value: Task): Promise<Task> {
    return this.http
      .post(environment.apiUrl + URL, value)
      .pipe(
        map((taskMap: Task) => {
          const task = new Task(taskMap);
          this.data = [...this.data, task];
          this.task.next(this.data);
          return task;
        })
      )
      .toPromise();
  }

  public createTask(siteWorkId: number, sowTypeId: number): Promise<Task[]> {
    return this.http.post(`${environment.apiUrl + URL}/generate/${siteWorkId}/${sowTypeId}`, []).pipe(
      map((val: Task[]) => {
        this.data = [...val.map(task => new Task(task))];
        this.task.next(this.data);
        return this.data;
      })
    ).toPromise();
  }

  public update(value: Task): Promise<Task> {
    return this.http
      .put(`${environment.apiUrl + URL}/${value['id']}`, value)
      .pipe(
        map((taskMap: Task) => {
          const task = new Task(taskMap);
          this.data = [...this.data, task];
          this.task.next(this.data);
          return task;
        })
      )
      .toPromise();
  }
}
