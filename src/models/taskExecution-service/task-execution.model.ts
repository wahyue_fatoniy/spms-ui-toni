import { Task } from '../task-service/task.model';
import { Severity } from '../enum/enum';

export class TaskExecution {
  private id: number;
  private task: Task;
  private manHours: string;
  private finish: boolean;
  private note: string;
  private date: Date;

  constructor(input: Object) {
    if (input !== undefined) {
        this.id = input['id'];
        this.task = new Task(input['task']);
        this.manHours = input['manHours'];
        this.finish = input['finish'];
        this.note = input['note'];
        this.date = input['date'] ? new Date(input['date']) : null;
    }
  }

  public get getId() {
    return this.id;
  }

  public get getTask() {
    return this.task;
  }

  public get getManhours() {
    return this.manHours;
  }

  public get getFinish() {
    return this.finish;
  }

  public get getNote() {
    return this.note;
  }

  public get getDate() {
    return this.date;
  }
}
