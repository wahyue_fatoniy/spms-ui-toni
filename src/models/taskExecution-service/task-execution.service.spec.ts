import { TestBed, inject } from '@angular/core/testing';

import { TaskExecutionService } from './task-execution.service';

describe('TaskExecutionService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [TaskExecutionService]
    });
  });

  it('should be created', inject([TaskExecutionService], (service: TaskExecutionService) => {
    expect(service).toBeTruthy();
  }));
});
