import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { BehaviorSubject, Observable } from 'rxjs';
import { TaskExecution } from './task-execution.model';
import { map } from 'rxjs/operators';
import { TaskService } from '../task-service/task.service';
import { environment } from '../../environments/environment';
import * as moment from 'moment';
import { DatePipe } from '@angular/common';

const URL = '/api/task_executions';

@Injectable({
  providedIn: 'root'
})
export class TaskExecutionService {
  private taskExecution: BehaviorSubject<TaskExecution[]> = new BehaviorSubject<
    TaskExecution[]
  >([]);
  private data: Array<TaskExecution> = [];
  private datePipe: DatePipe = new DatePipe('id');

  constructor(private http: HttpClient, private taskService: TaskService) {}

  public get getTaskExecution(): Observable<TaskExecution[]> {
    return this.taskExecution.asObservable();
  }

  public get getAll(): Promise<TaskExecution[]> {
    return this.http
      .get(environment.apiUrl + URL)
      .pipe(
        map((taskExecutionsMap: Array<TaskExecution>) => {
          this.data = taskExecutionsMap.map(
            taskExecutionMap => new TaskExecution(taskExecutionMap)
          );
          this.taskExecution.next(this.data);
          return this.data;
        })
      )
      .toPromise();
  }

  public getThisDay(date: Date): Promise<TaskExecution[]> {
    const start = this.getStartOfDay(date);
    const end = this.getEndOfDay(start);
    return this.http.get(`${environment.apiUrl + URL}/${start}/${end}`)
    .pipe(
      map((val: TaskExecution[]) => {
        this.data = val.map(taskExecution => new TaskExecution(taskExecution));
        this.taskExecution.next(this.data);
        return this.data;
      })
    ).toPromise();

  }

  public create(value: TaskExecution): Promise<TaskExecution> {
    this.taskService.update(value.getTask).then(val => console.log(value));

    return this.http
      .post(environment.apiUrl + URL, value)
      .pipe(
        map((taskMap: TaskExecution) => {
          const taskExecution = new TaskExecution(taskMap);
          this.data = [...this.data, taskExecution];
          this.taskExecution.next(this.data);
          return taskExecution;
        })
      )
      .toPromise();
  }

  public update(value: TaskExecution): Promise<TaskExecution> {
    return this.http
      .put(`${environment.apiUrl + URL}/${value['id']}`, value)
      .pipe(
        map((taskExecutionMap: TaskExecution) => {
          const taskExecution = new TaskExecution(taskExecutionMap);
          this.data = this.data.map(
            mapData =>
              mapData.getId === taskExecution['id'] ? taskExecution : mapData
          );
          this.taskExecution.next(this.data);
          return taskExecution;
        })
      )
      .toPromise();
  }

  public delete(id: number): Promise<number> {
    return this.http
      .delete(`${environment.apiUrl + URL}/${id}`)
      .pipe(
        map((taskExecutionId: number) => {
          this.data = this.data.filter(
            result => (result.getId !== taskExecutionId)
          );
          this.taskExecution.next(this.data);
          return taskExecutionId;
        })
      )
      .toPromise();
  }

  private getStartOfDay(date: Date): string {
    const startOfDay = moment(date)
      .startOf('day');
    /**set start date to UTC format */
    const startDate: string = this.datePipe.transform(
      startOfDay,
      'yyyy-MM-ddTHH:mm:ssZ',
      '+0000'
    );
    return startDate;
  }

  private getEndOfDay(startDay: string) {
    const endOfDay = moment(startDay)
      .endOf('day');
    /**set end date to UTC format */
    const date: string = this.datePipe.transform(
      endOfDay,
      'yyyy-MM-ddTHH:mm:ssZ',
      '+0000'
    );
    return date;
  }
}
