import { Role } from '../role-service/role.model';

export class TaskExecutor extends Role {
  protected id: number;
  constructor(input: Object) {
    if (input !== undefined) {
      super('TASK_EXECUTOR', input);
      this.id = input['id'];
    }
  }
}
