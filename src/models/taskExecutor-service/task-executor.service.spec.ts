import { TestBed, inject } from '@angular/core/testing';

import { TaskExecutorService } from './task-executor.service';

describe('TaskExecutorService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [TaskExecutorService]
    });
  });

  it('should be created', inject([TaskExecutorService], (service: TaskExecutorService) => {
    expect(service).toBeTruthy();
  }));
});
