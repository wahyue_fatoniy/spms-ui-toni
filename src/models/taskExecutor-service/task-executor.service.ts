import { Injectable } from '@angular/core';
import { Observable, BehaviorSubject } from 'rxjs';
import { map } from 'rxjs/operators';
import { TaskExecutor } from './task-executor.model';
import { HttpClient } from '@angular/common/http';
import { setPartyType } from '../party-service/party.service';
import { HOST } from '../../app/publicIP';

const URL = '/api/task_executors';

@Injectable({
  providedIn: 'root'
})
export class TaskExecutorService {
  private executorData: Array<Object> = [
    {
      id: 1,
      roleType: 'EXECUTOR',
      party: {
        id: 1,
        firstName: 'Dayat',
        midName: '',
        lastName: '',
        gender: 'MALE',
        birthDate: new Date(),
        name: 'Dayat',
        address: 'Jl. Buana 3',
        email: 'dyt@gmail.com',
        phone: '085755537987',
        partyType: 'PERSON'
      }
    }
  ];

  private taskExecutor: BehaviorSubject<TaskExecutor[]> = new BehaviorSubject<
    TaskExecutor[]
  >([]);
  private data: Array<TaskExecutor> = [];

  constructor(private http: HttpClient) {}

  getExecutor(): Observable<TaskExecutor[]> {
    return this.taskExecutor.asObservable();
  }

  getAll(): Promise<TaskExecutor[]> {
    return this.http
      .get(HOST + URL)
      .pipe(
        map((executorsMap: Array<TaskExecutor>) => {
          this.data = executorsMap.map(
            (executorMap: TaskExecutor) => {
              executorMap['party']['partyType'] = setPartyType(executorMap);
              return new TaskExecutor(executorMap);
            }
          );
          this.taskExecutor.next(this.data);
          return this.data;
        })
      )
      .toPromise();
  }

  createExecutor(value: TaskExecutor): Promise<TaskExecutor> {
    return this.http
      .post(HOST + URL, value)
      .pipe(
        map((executorMap: TaskExecutor) => {
          const executor = new TaskExecutor(value);
          this.data = [...this.data, executor];
          this.taskExecutor.next(this.data);
          return executor;
        })
      )
      .toPromise();
  }

  updateExecutor(value: TaskExecutor): Promise<TaskExecutor> {
    return this.http
      .put(`${HOST + URL}/${value['id']}`, value)
      .pipe(
        map((executorMap: TaskExecutor) => {
          this.data = this.data.map(
            dataMap => (dataMap['id'] === executorMap['id'] ? value : dataMap)
          );
          this.taskExecutor.next(this.data);
          return value;
        })
      )
      .toPromise();
  }

  deleteExecutor(id: number): Promise<number> {
    return this.http
      .delete(`${HOST + URL}/${id}`)
      .pipe(
        map((executorId: number) => {
          this.data = this.data.filter(
            dataMap => (dataMap['id'] === executorId ? '' : dataMap)
          );
          this.taskExecutor.next(this.data);
          return executorId;
        })
      )
      .toPromise();
  }
}
