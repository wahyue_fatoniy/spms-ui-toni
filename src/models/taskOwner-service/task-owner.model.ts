import { Role } from '../role-service/role.model';

export class TaskOwner extends Role {
  protected id: number;
  constructor(input: Object) {
    if (input !== undefined) {
      super('TASK_OWNER', input);
      this.id = input['id'];
    }
  }
}
