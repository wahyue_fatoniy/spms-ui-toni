import { TestBed, inject } from '@angular/core/testing';

import { TaskOwnerService } from './task-owner.service';

describe('TaskOwnerService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [TaskOwnerService]
    });
  });

  it('should be created', inject([TaskOwnerService], (service: TaskOwnerService) => {
    expect(service).toBeTruthy();
  }));
});
