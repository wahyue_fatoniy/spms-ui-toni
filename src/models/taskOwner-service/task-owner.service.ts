import { Injectable } from '@angular/core';
import { Observable, BehaviorSubject, Subscription } from 'rxjs';
import { map } from 'rxjs/operators';
import { TaskOwner } from './task-owner.model';
import { HttpClient } from '@angular/common/http';
import { setPartyType } from '../party-service/party.service';
import { HOST } from '../../app/publicIP';
import { OrganizationService } from '../organization-service/organization.service';
import { PersonService } from '../person-service/person.service';
import { Person } from '../person-service/person.model';
import { Organization } from '../organization-service/organization.model';
import { Party } from '../party-service/party.model';

const URL = '/api/task_owners';

@Injectable({
  providedIn: 'root'
})
export class TaskOwnerService {
  private ownerData: Array<Object> = [
    {
      id: 1,
      roleType: 'OWNER',
      party: {
        id: 1,
        name: 'Satunol Microsystem',
        address: 'Jl. Radio Dalam Raya',
        email: 'satunol@gmail.com',
        phone: '08579997987',
        partyType: 'ORGANIZATION'
      }
    }
  ];

  private taskOwner: BehaviorSubject<TaskOwner[]> = new BehaviorSubject<
    TaskOwner[]
  >([]);
  private data: Array<TaskOwner> = [];

  constructor(
    private http: HttpClient,
    private organizationService: OrganizationService,
    private personService: PersonService
  ) {
    this.onGetData();
  }

  connect(): Observable<TaskOwner[]> {
    return this.taskOwner.asObservable();
  }

  disconnect(subscriptions: Subscription): void {
    if (subscriptions !== undefined) {
      if (subscriptions.closed === false) {
        subscriptions.unsubscribe();
      }
    }
  }

  private onGetData(): void {
    this.http
      .get(HOST + URL)
      .pipe(
        map((customersMap: Array<TaskOwner>) => {
          this.data = customersMap.map((taskOwnerMap: TaskOwner) => {
            taskOwnerMap['party']['partyType'] = setPartyType(taskOwnerMap);
            return new TaskOwner(taskOwnerMap);
          });
          this.taskOwner.next(this.data);
        })
      )
      .toPromise()
      .catch(err => console.log(err));
  }

  createTaskOwner(value: TaskOwner): Promise<TaskOwner> {
    return new Promise((resolve, reject) => {
      this.referenceService(value['party'], 'create').then(
        (party: Person | Organization) => {
          value['party'] = party;
          this.http
            .post(HOST + URL, value)
            .toPromise()
            .then((taskOwner: TaskOwner) => {
              taskOwner['party']['partyType'] = setPartyType(value);
              this.data = [...this.data, new TaskOwner(taskOwner)];
              this.taskOwner.next(this.data);
              resolve(taskOwner);
            })
            .catch(err => reject(err));
        }
      ).catch(err => reject(err));
    });
  }

  updateTaskOwner(value: TaskOwner): Promise<TaskOwner> {
    return new Promise((resolve, reject) => {
      value['party']['partyType'] = setPartyType(value);
      this.referenceService(value['party'], 'update')
        .then((party: Person | Organization) => {
          this.http
            .put(`${HOST + URL}/${value['id']}`, value)
            .pipe(
              map((taskOwnerMap: TaskOwner) => {
                this.data = this.data.map(
                  dataMap =>
                    dataMap['id'] === taskOwnerMap['id'] ? value : dataMap
                );
                this.taskOwner.next(this.data);
                resolve(taskOwnerMap);
              })
            )
            .toPromise()
            .catch(err => reject(err));
        })
        .catch(err => reject(err));
    });
  }

  deleteTaskOwner(value: TaskOwner): Promise<number> {
    return new Promise((resolve, reject) => {
      this.http
        .delete(`${HOST + URL}/${value['id']}`)
        .pipe(
          map((taskOwnerId: number) => {
            this.referenceService(value['party'], 'delete').catch(err =>
              reject(err)
            );
            this.data = this.data.filter(
              dataMap => dataMap['id'] !== taskOwnerId
            );
            this.taskOwner.next(this.data);
            resolve(taskOwnerId);
          })
        )
        .toPromise()
        .catch(err => reject(err));
    });
  }

  private referenceService(
    party: Party,
    action: string
  ): Promise<Person | Organization> {
    if (party !== undefined) {
      let referenceService: Promise<Person | Organization | number>;
      switch (party['partyType']) {
        case 'ORGANIZATION':
          return (referenceService = this.organizationService[
            `${action}Organization`
          ](new Organization(party)));
        case 'PERSON':
          return (referenceService = this.personService[`${action}Person`](
            new Person(party)
          ));
      }
    }
  }
}
