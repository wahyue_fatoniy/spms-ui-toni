import { Resources } from '../resources-service/resources.model';
import { Coordinator } from '../coordinatorService/coordinator.model';
import { Subcont } from '../subcont-service/subcont.model';

export class Team {
  private id: number;
  private resources: Resources[];
  private cordinator: Coordinator | Subcont;

  constructor(input: Object) {
    if (input !== undefined && input !== null) {
      this.id = input['id'];
      this.resources = input['resources'] ? input['resources'].map(val => new Resources(val['roleType'], val)) : [];
      this.cordinator = input['cordinator'];
    }
  }

  public get getId() {
    return this.id;
  }

  public get getResources() {
    return this.resources ? this.resources : [];
  }

  public  get getCoordinator() {
    return this.cordinator;
  }
}
