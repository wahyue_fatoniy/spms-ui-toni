import { Resources } from '../resources-service/resources.model';
import { Region } from '../region-service/region.model';
import { Person } from '../person-service/person.model';

export class Technician extends Resources {
  private technicianType: string;

  constructor(input: Object) {
    if (input) {
      input = { ...input, party: new Person(input['party'])};
      input['party']['partyType'] = 'PERSON';
      super('TECHNICIAN', input);
    }
  }

  public get getId() {
    return this.id;
  }

  public get getTechnicianType() {
    if (this.getNip === null || this.getNip === undefined) {
      this.technicianType = 'SUBCONT';
    } else {
      this.technicianType = 'INTERNAL';
    }
    return this.technicianType;
  }
}
