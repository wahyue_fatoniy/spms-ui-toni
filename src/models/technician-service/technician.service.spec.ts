import { TestBed, inject } from '@angular/core/testing';

import { TechnicianService } from './technician.service';

describe('TechnicianService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [TechnicianService]
    });
  });

  it('should be created', inject([TechnicianService], (service: TechnicianService) => {
    expect(service).toBeTruthy();
  }));
});
