import { Injectable } from '@angular/core';
import { Observable, BehaviorSubject, Subscription } from 'rxjs';
import { map } from 'rxjs/operators';
import { HttpClient } from '@angular/common/http';
import { PersonService } from '../person-service/person.service';
import { Person } from '../person-service/person.model';
import { Technician } from './technician.model';
import { environment } from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class TechnicianService {
  private technician: BehaviorSubject<Technician[]> = new BehaviorSubject<
    Technician[]
  >([]);
  private data: Array<Technician> = [];
  private URL = '/api/technicians';

  constructor(private http: HttpClient, private personService: PersonService) {
    this.onGetData();
  }

  public get connect(): Observable<Technician[]> {
    return this.technician.asObservable();
  }

  public disconnect(subscriptions: Subscription): void {
    if (subscriptions !== undefined) {
      if (subscriptions['closed'] === false) {
        subscriptions.unsubscribe();
      }
    }
  }

  private onGetData(): void {
    this.http
      .get(environment.apiUrl + this.URL)
      .pipe(
        map((techniciansMap: Array<Technician>) => {
          this.data = techniciansMap.map(technicianMap => {
            return new Technician(technicianMap);
          });
          this.technician.next(this.data);
        })
      )
      .toPromise()
      .catch(err => console.log(err));
  }

  public create(value: Technician): Promise<Technician> {
    return new Promise((resolve, reject) => {
    this.personService.personExist(new Person(value['party'])).then(val => {
      value['party'] = val;

      this.http
        .post(environment.apiUrl + this.setUrl(value), value)
        .pipe(
          map((technicianMap: Technician) => {
            value['id'] = technicianMap['id'];
            this.data = [...this.data, new Technician(value)];
            this.technician.next(this.data);
            resolve(new Technician(value));
          })
        )
        .toPromise()
        .catch(err => reject(err));

      });
    });
  }

  public update(value: Technician): Promise<Technician> {
    return new Promise((resolve, reject) => {
      this.personService
        .update(new Person(value['party']))
        .then(() => {

          this.http
            .put(`${environment.apiUrl + this.setUrl(value)}/${value['id']}`, value)
            .pipe(
              map((technicianMap: Technician) => {
                this.data = this.data.map(
                  dataMap =>
                    dataMap['id'] === technicianMap['id'] ? value : dataMap
                );
                this.technician.next(this.data);
                resolve(value);
              })
            )
            .toPromise()
            .catch(err => reject(err));

          })
        .catch(err => reject(err));
    });
  }

  public delete(value: Technician): Promise<number> {
    return new Promise((resolve, reject) => {
      this.http
        .delete(`${environment.apiUrl + this.setUrl(value)}/${value['id']}`)
        .toPromise()
        .then((technicianId: number) => {

          this.personService
            .delete(new Person(value['party']))
            .catch(err => reject(err));

          this.data = this.data.filter((result: Technician) => result.getId !== technicianId);

          this.technician.next(this.data);
          resolve(technicianId);
        })
        .catch(err => reject(err));
    });
  }

  private setUrl(value: Technician): string {
    let url = '/api/internal_technicians';
    if (value['nip'] === undefined || value['nip'] === null) {
      url = this.URL;
    }
    return url;
  }
}
