import { Role } from '../role-service/role.model';
import { Person } from '../person-service/person.model';
export class User extends Role {
  private username: string;
  private password: string;
  private email: string;

  constructor(input: Object) {
    if (input !== undefined || null) {
      input = {...input, party: new Person(input['party']) };
      input['party']['partyType'] = 'PERSON';
      super('USER', input);
      this.username = input['username'];
      this.password = input['password'];
      this.email = input['email'];
    }
  }

  public get getUsername() {
    return this.username;
  }

  public get getpassword() {
    return this.password;
  }

  public get getEmail() {
    return this.email;
  }
}
