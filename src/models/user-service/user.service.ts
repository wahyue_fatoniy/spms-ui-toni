import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';
import { User } from './user.model';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../environments/environment';
import { map } from 'rxjs/operators';
import { PersonService } from '../person-service/person.service';
import { Person } from '../person-service/person.model';

const URL = `${environment.apiUrl}/api/users`;

@Injectable({
  providedIn: 'root'
})
export class UserService {
  public user: BehaviorSubject<User[]> =  new BehaviorSubject<User[]>([]);
  private data: User[] = [];
  constructor(private http: HttpClient, private personService: PersonService) { }

  public get getUser(): Observable<User[]> {
    return this.user.asObservable();
  }

  public get getAll(): Promise<User[]> {
    return this.http.get(URL).pipe(
      map((val: User[]) => {
        this.data = val.map(user => new User(user));
        this.user.next(this.data);
        return this.data;
      })
    ).toPromise();
  }

  public getByParty(id: number): Observable<User> {
    return this.http.get(URL).pipe(
      map((users: User[]) => users.find(user => new User(user).getParty.getId === id))
    );
  }

  public create(val: User): Promise<User[]> {
    return this.http.post(URL, val).pipe(
      map((user: User) => {
        this.data = [...this.data, new User(user)];
        this.user.next(this.data);
        return this.data;
      })
    ).toPromise();
  }

  public delete(user: User): Promise<number> {
    return this.http.delete(`${URL}/${user['id']}`).pipe(
      map((val: number) => {
        this.personService.delete(new Person(user.getParty)).catch(err => console.log(err));
        return val;
      })
    ).toPromise();
  }

  public update(user: User): Promise<User> {
    return this.http.put(`${URL}.${user.getId}`, user).pipe(
      map(val => {
        return new User(val);
      })
    ).toPromise();
  }
}
