import { Task } from '../task-service/task.model';
import { SiteWork } from '../siteWork-service/siteWork.model';
import { DatePipe } from '@angular/common';
import { Technician } from '../technician-service/technician.model';
import { Engineer } from '../engineer-service/engineer.model';
import { Documentation } from '../documentation-service/documentation.model';
import { Coordinator } from '../coordinatorService/coordinator.model';
import { Subcont } from '../subcont-service/subcont.model';

const datePipe: DatePipe = new DatePipe('id');

export class WeeklyPlanning {
  private id: number;
  private siteWork: SiteWork;
  private tasks: Task[];
  private cordinator: Coordinator | Subcont;
  private engineers: Array<Engineer>;
  private technicians: Array<Technician>;
  private documentations: Array<Documentation>;
  private startDate: string | Date ;
  private endDate: string | Date;
  private operational: number;
  private budgetUsed: number;
  private budget: number;
  private submit: boolean;

  constructor(input: Object) {
    if (input !== undefined) {
      this.id = input['id'];
      this.startDate = this.setDate(input['startDate']);
      this.endDate = this.setDate(input['endDate']);
      this.operational = input['operational'] ? input['operational'] : 0;
      this.cordinator = input['cordinator'];
      this.engineers =
        input['engineers'] instanceof Array
          ? input['engineers'].map(engineer => new Engineer(engineer))
          : [];
      this.technicians =
        input['technicians'] instanceof Array
          ? input['technicians'].map(technician => new Technician(technician))
          : [];
      this.documentations =
        input['documentations'] instanceof Array
          ? input['documentations'].map(document => new Documentation(document))
          : [];
      this.tasks =
        input['tasks'] instanceof Array
          ? input['tasks'].map(task => new Task(task))
          : [];
      this.siteWork = new SiteWork(input['siteWork']);
      this.budgetUsed = input['total'] ? input['total'] : 0;
      this.budget = input['budget'] ? input['budget'] : 0;
      this.submit = input['submit'] ? input['submit'] : false;
    }
  }

  public get getId() {
    return this.id;
  }

  public get getStartDate() {
    return this.startDate;
  }

  public set getStartDate(date: Date | string) {
    this.startDate = date;
  }

  public get getEndDate() {
    return this.endDate;
  }

  public set getEndDate(date: Date | string) {
    this.endDate = date;
  }

  public get getOperational() {
    return this.operational;
  }

  public set getOperational(operational: any) {
    this.operational = operational;
  }

  public get getCoordinator() {
    return this.cordinator;
  }

  public get getEngineer() {
    return this.engineers;
  }

  public get getTechnician() {
    return this.technicians;
  }

  public get getDocumentation() {
    return this.documentations;
  }

  public get getTasks() {
    return this.tasks;
  }

  public set getTasks(tasks: Task[]) {
    this.tasks = tasks;
  }

  public get getSiteWork() {
    return this.siteWork;
  }

  public set getSiteWork(siteWork: SiteWork) {
    this.siteWork = siteWork;
  }

  public get getBudget() {
    return this.budget;
  }

  public get getBudgetUsed() {
    return this.budgetUsed;
  }

  public get getSubmit() {
    return this.submit;
  }

  private setDate(date: string | Date): string {
    return datePipe.transform(date, 'yyyy-MM-ddTHH:mm:ssZ', '+0000');
  }
}
