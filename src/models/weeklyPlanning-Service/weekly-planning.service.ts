import { Injectable, OnDestroy } from '@angular/core';
import { BehaviorSubject, Observable, Subscription } from 'rxjs';
import { WeeklyPlanning } from './weekly-planning.model';
import { HttpClient } from '@angular/common/http';
import { SiteWorkService } from '../siteWork-service/site-work.service';
import { map, filter } from 'rxjs/operators';
import * as moment from 'moment';
import { DatePipe } from '@angular/common';
import {
  ParamCoordinatorId,
  WeeklyPlanningDate
} from '../../models/interface/interface';
import { environment } from '../../environments/environment';
const URL = `${environment.apiUrl}/api/weekly_plannings`;

@Injectable({
  providedIn: 'root'
})
export class WeeklyPlanningService {
  private datePipe: DatePipe = new DatePipe('id');
  private weeklyPlanning: BehaviorSubject<
    WeeklyPlanning[]
  > = new BehaviorSubject<WeeklyPlanning[]>([]);
  private data: Array<WeeklyPlanning> = [];

  constructor(
    private http: HttpClient,
    private siteWorkService: SiteWorkService
  ) {}

  public get connect(): Observable<WeeklyPlanning[]> {
    return this.weeklyPlanning.asObservable();
  }

  public submit(id: number): Promise<WeeklyPlanning[]> {
    return this.http
      .post(`${URL}/submit/${id}`, {})
      .pipe(
        map((val: WeeklyPlanning) => {
          this.data = this.data.map((planning: WeeklyPlanning) => {
            return planning.getId === val['id'] ? new WeeklyPlanning(val) : planning;
          });
          this.weeklyPlanning.next(this.data);
          return this.data;
        })
      )
      .toPromise();
  }

  public disconnect(subscriptions: Subscription): void {
    if (subscriptions !== undefined) {
      if (subscriptions['closed'] === false) {
        subscriptions.unsubscribe();
      }
    }
  }

  public getAll(): Promise<WeeklyPlanning[]> {
    return this.http
      .get(`${URL}`)
      .pipe(
        map((val: WeeklyPlanning[]) => {
          this.data = val.map(
            weeklyPlanning => new WeeklyPlanning(weeklyPlanning)
          );
          this.weeklyPlanning.next(this.data);
          return this.data;
        })
      )
      .toPromise();
  }

  public getByWeek(initialDate: Date): Promise<WeeklyPlanning[]> {
    const startWeek = this.getStartOfWeek(initialDate);
    const endWeek = this.getEndOfWeek(startWeek);
    return this.http
      .get(`${URL}/${startWeek}/${endWeek}`)
      .pipe(
        map((val: WeeklyPlanning[]) => {
          this.data = val.map(
            weeklyPlanning => new WeeklyPlanning(weeklyPlanning)
          );
          this.weeklyPlanning.next(this.data);
          return this.data;
        })
      )
      .toPromise();
  }

  public getByRangeDate(fromDate: Date, toDate: Date): Promise<WeeklyPlanning[]> {
    const startWeek = this.setUTCDate(fromDate);
    const endWeek = this.setUTCDate(toDate);
    return this.http
      .get(`${URL}/${startWeek}/${endWeek}`)
      .pipe(
        map((val: WeeklyPlanning[]) => {
          this.data = val.map(
            weeklyPlanning => new WeeklyPlanning(weeklyPlanning)
          );
          this.weeklyPlanning.next(this.data);
          return this.data;
        })
      )
      .toPromise();
  }

  public getBySiteWorkId(siteWorId: number): Observable<WeeklyPlanning[]> {
    return this.http
      .get(`${URL}`)
      .pipe(
        map((val: WeeklyPlanning[]) =>
          val
            .map(weeklyPlanning => new WeeklyPlanning(weeklyPlanning))
            .filter(
              (filterWeekly: WeeklyPlanning) =>
                filterWeekly.getSiteWork.getId === siteWorId
            )
        )
      );
  }

  public getByCoordinatorId(
    urlParam: ParamCoordinatorId
  ): Observable<WeeklyPlanning[]> {
    return this.http
      .get(
          `${URL}/${urlParam.siteWorkId}/${urlParam.coordinatorId}/${
            urlParam.startDate
          }/${urlParam.endDate}`
      )
      .pipe(
        map((planningMap: WeeklyPlanning[]) =>
          planningMap.map(val => new WeeklyPlanning(val))
        )
      );
  }

  public create(value: WeeklyPlanning): void {
    this.http
      .post(URL, value)
      .toPromise()
      .then((weeklyPlanning: WeeklyPlanning) => {
        value['id'] = weeklyPlanning['id'];
        this.data = [...this.data, new WeeklyPlanning(value)];
        this.weeklyPlanning.next(this.data);
      })
      .catch(err => console.error(err));
  }

  public update(value: WeeklyPlanning): void {
    this.http
      .put(`${URL}/${value['id']}`, value)
      .toPromise()
      .then((weeklyPlanning: WeeklyPlanning) => {
        console.log(weeklyPlanning);
        this.data = this.data.map((fltrWeeklyPlanning: WeeklyPlanning) => {
          return fltrWeeklyPlanning.getId === value['id']
            ? value
            : fltrWeeklyPlanning;
        });
        this.weeklyPlanning.next(this.data);
      })
      .catch(err => console.log(err));
  }

  public delete(valueId: number): void {
    this.http
      .delete(`${URL}/${valueId}`)
      .toPromise()
      .then((weeklyId: number) => {
        this.data = this.data.filter(
          (value: WeeklyPlanning) => value.getId !== valueId
        );
        this.weeklyPlanning.next(this.data);
      })
      .catch(err => console.log(err));
  }

  private getStartOfWeek(startWeek: Date): string {
    const startOfWeek = moment(startWeek)
      .startOf('week')
      .day(+1);
    /**set start initialDate to UTC format */
    const initialDate: string = this.datePipe.transform(
      startOfWeek,
      'yyyy-MM-ddTHH:mm:ssZ',
      '+0000'
    );
    return initialDate;
  }

  private getEndOfWeek(startWeek: string) {
    const endOfWeek = moment(new Date(startWeek))
      .day(+7)
      .endOf('day');
    /**set end initialDate to UTC format */
    const initialDate: string = this.datePipe.transform(
      endOfWeek,
      'yyyy-MM-ddTHH:mm:ssZ',
      '+0000'
    );
    return initialDate;
  }

  private setUTCDate(date: Date): string {
    const utcDate: string = this.datePipe.transform(
      date,
      'yyyy-MM-ddTHH:mm:ssZ',
      '+0000'
    );
    return utcDate;
  }
}
