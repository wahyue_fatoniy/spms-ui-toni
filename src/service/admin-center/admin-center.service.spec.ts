import { TestBed, inject } from '@angular/core/testing';

import { AdminCenterService } from './admin-center.service';

describe('AdminCenterService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [AdminCenterService]
    });
  });

  it('should be created', inject([AdminCenterService], (service: AdminCenterService) => {
    expect(service).toBeTruthy();
  }));
});
