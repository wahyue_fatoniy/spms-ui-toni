import { Injectable } from '@angular/core';
import { MainService } from '../main-service/main.service';
import { AdminCenter } from 'src/app/models/adminCenter.model';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';

const URL = `${environment.apiUrl}/api/admin_centers`;

@Injectable({
  providedIn: 'root'
})
export class AdminCenterService extends MainService<AdminCenter> {

  constructor(protected http: HttpClient) {
    super(AdminCenter, URL, http);
  }
}
