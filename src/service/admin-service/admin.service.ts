import { Injectable } from '@angular/core';
import { MainService } from '../main-service/main.service';
import { Admin } from 'src/app/models/admin.model';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';

const URL = `${environment.apiUrl}/api/admins`;


@Injectable({
  providedIn: 'root'
})
export class AdminService extends MainService<Admin> {
  constructor(protected http: HttpClient) {
    super(Admin, URL, http);
  }
}
