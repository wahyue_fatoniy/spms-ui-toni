import { Injectable } from '@angular/core';
import { MainService } from '../main-service/main.service';
import { Assignment, AssignmentStatus } from 'src/app/models/assignment.model';
import { environment } from 'src/environments/environment';
import { HttpClient } from '@angular/common/http';
import { map } from 'rxjs/operators';

const URL = `${environment.apiUrl}/api/assignments`;

@Injectable({
  providedIn: 'root'
})
export class AssignmentService extends MainService<Assignment> {
  constructor(protected http: HttpClient) {
    super(Assignment, URL, http);
  }


  getByGmDate(id: number, start: Date, end: Date) {
    return this.http.get(`${URL}/general_manager/${id}/start_date/${start}/${end}`);
  }

  getByPmDate(id: number, start: Date, end: Date) {
    return this.http.get(`${URL}/project_manager/${id}/start_date/${start}/${end}`);
  }

  getByPmStatus(pmId: number, status: AssignmentStatus) {
    return this.http.get(`${URL}/project_manager/${pmId}/status/${status}`)
    .pipe(
      map((assignments: any[]) => {
        const data = assignments.map(val => {
          return new Assignment(val);
        })
        this.subject.next(data);
        return data;
      })
    );
  }

  getByGmStatus(gmId: number, status: AssignmentStatus) {
    return this.http.get(`${URL}/general_manager/${gmId}/status/${status}`)
    .pipe(
      map((assignments: any[]) => {
        const data = assignments.map(val => {
          return new Assignment(val);
        })
        this.subject.next(data);
        return data;
      })
    );
  }

  getByStatus(status: AssignmentStatus) {
    return this.http.get(`${URL}/status/${status}`)
    .pipe(
      map((assignments: any[]) => {
        const data = assignments.map(val => {
          return new Assignment(val);
        })
        this.subject.next(data);
        return data;
      })
    );
  }
}
