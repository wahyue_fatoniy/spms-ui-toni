import { Injectable } from '@angular/core';
import { MainService } from '../main-service/main.service';
import { Contract, ContractStatus } from 'src/app/models/contract.model';
import { environment } from 'src/environments/environment';
import { map } from 'rxjs/operators';
import { HttpClient } from '@angular/common/http';

const URL = `${environment.apiUrl}/api/contracts`;

@Injectable({
  providedIn: 'root'
})
export class ContractService extends MainService<Contract> {

  constructor(protected http: HttpClient) {
    super(Contract, URL, http);
  }

  findByGmDate(id: number, start: Date, end: Date) {
    return this.http.get(`${URL}/general_manager/${id}/start_date/${start}/${end}`);
  }

  findByPmDate(id: number, start: Date, end: Date) {
    return this.http.get(`${URL}/project_manager/${id}/start_date/${start}/${end}`);
  }

  findByPmStatus(pmId: number, status: ContractStatus) {
    return this.http.get(`${URL}/project_manager/${pmId}/status/${status}`)
      .pipe(
        map((contracts: any[]) => {
          const data = contracts.map(val => {
            return new Contract(val);
          })
          this.subject.next(data);
          return data;
        })
      );
  }

  findByGmStatus(gmId: number, status: ContractStatus) {
    return this.http.get(`${URL}/general_manager/${gmId}/status/${status}`)
      .pipe(
        map((contracts: any[]) => {
          const data = contracts.map(val => {
            return new Contract(val);
          })
          this.subject.next(data);
          return data;
        })
      );
  }

  findByStatus(status: ContractStatus) {
    return this.http.get(`${URL}/status/${status}`)
      .pipe(
        map((contracts: any[]) => {
          const data = contracts.map(val => {
            return new Contract(val);
          })
          this.subject.next(data);
          return data;
        })
      );
  }

  findById(id: number) {
    return this.http.get(`${URL}/${id}`)
      .pipe(
        map((contract: any) => {
          console.log(contract);
          this.data = [new Contract(contract)]
          this.subject.next(this.data);
          return contract;
        })
      );
  }

}
