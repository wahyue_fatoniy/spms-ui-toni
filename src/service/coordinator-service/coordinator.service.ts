import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { MainService } from '../main-service/main.service';
import { Coordinator } from 'src/app/models/coordinator.model';
import { HttpClient } from '@angular/common/http';
import { map } from 'rxjs/operators';


const URL = `${environment.apiUrl}/api/internal_cordinators`;

@Injectable({
  providedIn: 'root'
})

export class CoordinatorService extends MainService<Coordinator> {

  constructor(protected http: HttpClient) {
    super(Coordinator, URL, http);
  }

  findByRegionId(id: number) {
    return this.http.get(`${environment.apiUrl}/api/cordinators/region/${id}`)
    .pipe(
      map((coordinator: Object[]) => {
        this.data = coordinator.map(val => {
          return new Coordinator(val);
        });
        this.subject.next(this.data);
        return this.data;
      })
    );
  }
}
