import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { MainService } from '../main-service/main.service';
import { Customer } from 'src/app/models/customer.model';
import { HttpClient } from '@angular/common/http';

const URL = `${environment.apiUrl}/api/customers`;


@Injectable({
  providedIn: 'root'
})
export class CustomerService extends MainService<Customer> {

  constructor(protected http: HttpClient) {
    super(Customer, URL, http);
   }
}
