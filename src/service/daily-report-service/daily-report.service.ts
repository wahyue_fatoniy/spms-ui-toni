import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { MainService } from '../main-service/main.service';
import { DailyReport } from 'src/app/models/daliyReport.model';
import { environment } from 'src/environments/environment';
import { map, catchError } from 'rxjs/operators';
import { Observable } from 'rxjs';

const URL = environment.apiUrl + '/api/daily_reports';

@Injectable({
  providedIn: 'root'
})
export class DailyReportService extends MainService<DailyReport>  {

  constructor(protected http: HttpClient) {
    super(DailyReport, URL, http);
  }

  async getBySiteWorkId(siteWorkId: number) {
    this.setLoading = true;
    return this.http.get(URL + `/site_work/${siteWorkId}`).pipe(
      map((dailyReport: DailyReport[]) => {
        this.data = dailyReport.map(val => new DailyReport(val));
        this.subject.next(this.data);
        this.setLoading = false;
        return this.data;
      },
      catchError(err => {
        this.setLoading = false;
        return err;
      }))
    ).toPromise();
  }

  findBySiteWorkId(id: number): Observable<DailyReport[]> {
    this.setLoading = true;
    return this.http.get(`${URL}/site_work/${id}`)
    .pipe(
      map((result: any) => {
        this.setLoading = false;
        this.data = result.map(report => new DailyReport(report));
        this.subject.next(this.data);
        return result;
      }),
      catchError(error => {
        this.setLoading = false;
        return error;
      })
    );
  }
}
