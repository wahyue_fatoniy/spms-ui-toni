import { TestBed, inject } from '@angular/core/testing';

import { DailyPlanService } from './daily-plan.service';

describe('DailyPlanService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [DailyPlanService]
    });
  });

  it('should be created', inject([DailyPlanService], (service: DailyPlanService) => {
    expect(service).toBeTruthy();
  }));
});
