import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { DailyPlan } from 'src/app/models/dailyPlan.model';
import { HttpClient } from '@angular/common/http';
import { MainService } from '../main-service/main.service';
import { map } from 'rxjs/operators';

const URL = environment.apiUrl + '/api/daily_plannings';

@Injectable({
  providedIn: 'root'
})
export class DailyPlanService extends MainService<DailyPlan> {
  constructor(protected http: HttpClient) {
    super(DailyPlan, URL, http);
  }

  findByProjectId(id: number) {
    return this.http.get(`${URL}/site_work/${id}`)
    .pipe(
      map((result: any[]) => {
        const dailyPlans = result.map(val => new DailyPlan(val));
        this.data = dailyPlans;
        this.subject.next(dailyPlans);
        return dailyPlans;
      })
    )
  }
}
