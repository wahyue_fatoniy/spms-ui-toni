import { TestBed, inject } from '@angular/core/testing';

import { DocumentHistoryService } from './document-history.service';

describe('DocumentHistoryService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [DocumentHistoryService]
    });
  });

  it('should be created', inject([DocumentHistoryService], (service: DocumentHistoryService) => {
    expect(service).toBeTruthy();
  }));
});
