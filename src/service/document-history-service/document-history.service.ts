import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { MainService } from '../main-service/main.service';
import { DocumentLifecycle } from 'src/app/models/document.model';
import { HttpClient } from '@angular/common/http';
import { map } from 'rxjs/operators';

const URL =  `${environment.apiUrl}/api/document_life_cycles`;

@Injectable({
  providedIn: 'root'
})
export class DocumentHistoryService extends MainService<DocumentLifecycle> {
  constructor(protected http: HttpClient) {
    super(DocumentLifecycle, URL, http);
  }

  getHistoryDoc(docId: number) {
    return this.http.get(`${URL}/document/${docId}`).pipe(
      map((doc: Object[]) => {
        this.data = doc.map(document => new DocumentLifecycle(document));
        this.subject.next(this.data);
        return this.data;
      })
    );
  }

  createHistoryDoc(lifeCycleDoc: DocumentLifecycle) {
    return this.http.post(`${URL}`, lifeCycleDoc).pipe(
      map((result: Object) => {
        const doc = new DocumentLifecycle(result);
        this.data = [...this.data, doc]
        this.subject.next(this.data);
        return doc;
      })
    );
  }

}
