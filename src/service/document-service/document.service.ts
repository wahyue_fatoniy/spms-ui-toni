import { Injectable } from '@angular/core';
import { MainService } from '../main-service/main.service';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { Document, DocumentLifecycle } from 'src/app/models/document.model';
import { map } from 'rxjs/operators';

const URL = environment.apiUrl + '/api/documents';
const URLhistoryDoc =  `${environment.apiUrl}/api/document_life_cycles`;
@Injectable({
  providedIn: 'root'
})
export class DocumentService extends MainService<Document> {

  constructor(protected http: HttpClient) {
    super(Document, URL, http);
  }

  getHistoryDoc(docId: number) {
    return this.http.get(`${URLhistoryDoc}/document/${docId}`).pipe(
      map((doc: Object[]) => {
        doc = doc.map(document => new DocumentLifecycle(document));
        return doc;
      })
    );
  }

  createHistoryDoc(lifeCycleDoc: DocumentLifecycle) {
    return this.http.post(`${URLhistoryDoc}`, lifeCycleDoc);
  }

  createByPid(projectId: number, data: Document) {
    return this.http.post(`${URL}/site_work/${projectId}`, data).pipe(
      map((document: any) => {
        const doc = new Document(document); 
        this.data = [...this.data, doc];
        this.subject.next(this.data);
        return doc;
      })
    )
  }

  findByPid(projectId: number) {
    return this.http.get(`${URL}/site_work/${projectId}`).pipe(
      map((documents: any[]) => {
        this.data = documents.map(doc => new Document(doc));
        this.subject.next(this.data);
        return this.data;
      })
    )
  }

  updateById(id: number, projectId: number, data: Document) {
    return this.http.put(`${URL}/${id}/site_work/${projectId}`, data).pipe(
      map(document => {
        const doc = new Document(document);
        this.data = this.data.map(val => {
          return val.getId === doc.getId ? doc : val;
        });
        this.subject.next(this.data);
        return this.data;
      })
    )
  }

  deleteById(id: number) {
    return this.http.delete(`${URL}/${id}`)
    .pipe(
      map((id: number) => {
        this.data = this.data.filter(val => {
          return val.getId !== id;
        });
        this.subject.next(this.data);
        return this.data;
      })
    )
  }
}
