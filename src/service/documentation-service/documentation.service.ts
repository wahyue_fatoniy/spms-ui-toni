import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { MainService } from '../main-service/main.service';
import { Documentation } from 'src/app/models/documentation.model';
import { HttpClient } from '@angular/common/http';
import { map } from 'rxjs/operators';

const URL = `${environment.apiUrl}/api/documentations`;

@Injectable({
  providedIn: 'root'
})
export class DocumentationService extends MainService<Documentation> {

  constructor(protected http: HttpClient) {
    super(Documentation, URL, http);
  }

  findByRegionId(id: number) {
    return this.http.get(`${environment.apiUrl}/api/documentations/region/${id}`)
    .pipe(
      map((doc: Object[]) => {
        this.data = doc.map(val => {
          return new Documentation(val);
        });
        this.subject.next(this.data);
        return this.data;
      })
    );
  }
}
