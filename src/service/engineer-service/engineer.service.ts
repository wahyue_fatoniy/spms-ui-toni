import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { MainService } from '../main-service/main.service';
import { Engineer } from 'src/app/models/engineer.model';
import { HttpClient } from '@angular/common/http';
import { map } from 'rxjs/operators';

const URL = `${environment.apiUrl}/api/engineers`;

@Injectable({
  providedIn: 'root'
})
export class EngineerService extends MainService<Engineer> {

  constructor(protected http: HttpClient) {
    super(Engineer, URL, http);
  }

  findByRegionId(id: number) {
    return this.http.get(`${environment.apiUrl}/api/engineers/region/${id}`)
    .pipe(
      map((engineer: Object[]) => {
        this.data = engineer.map(val => {
          return new Engineer(val);
        });
        this.subject.next(this.data);
        return this.data;
      })
    );
  }
}
