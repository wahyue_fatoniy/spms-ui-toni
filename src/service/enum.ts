export enum projectStatus {
  ON_PROGRESS = 'ON_PROGRESS',
  NOT_STARTED = 'NOT_STARTED',
  DONE = 'DONE',
  CANCEL = 'CANCEL'
}

export enum Severity {
  CRITICAL_I = 'CRITICAL_I',
  CRITICAL_E = 'CRITICAL_E',
  MAJOR_I = 'MAJOR_I',
  MAJOR_E = 'MAJOR_E',
  NORMAL = 'NORMAL'
}

