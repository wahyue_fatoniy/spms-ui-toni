import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { DatePipe } from '@angular/common';
import { HttpClient } from '@angular/common/http';
const URL = `${environment.apiUrl}/api/site_works/generate_excel`;
@Injectable({
  providedIn: 'root'
})
export class ExportExcelService {
  private datePipe: DatePipe = new DatePipe('id');
  constructor(
    private http: HttpClient
  ) {}

  generate(startDate: Date, endDate: Date) {
    const start = this.setDate(startDate);
    const end = this.setDate(endDate);
    return this.http.get(`${URL}/${start}/${end}`);
  }

  generateByPm(pmId: number, startDate: Date, endDate: Date) {
    const start = this.setDate(startDate);
    const end = this.setDate(endDate);
    return this.http.get(`${URL}/${pmId}/${start}/${end}`);
  }

  private setDate(date: string | Date): string {
    return this.datePipe.transform(date, 'yyyy-MM-ddTHH:mm:ssZ', '+0000');
  }
}
