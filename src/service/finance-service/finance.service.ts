import { Injectable } from '@angular/core';
import { MainService } from '../main-service/main.service';
import { Finance } from 'src/app/models/finance.model';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';

const URL = `${environment.apiUrl}/api/finances`;

@Injectable({
  providedIn: 'root'
})
export class FinanceService extends MainService<Finance> {
  constructor(protected http: HttpClient) {
    super(Finance, URL, http);
  }
}
