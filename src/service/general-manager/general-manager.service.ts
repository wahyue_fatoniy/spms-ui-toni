import { Injectable } from '@angular/core';
import { MainService } from '../main-service/main.service';
import { GeneralManager } from 'src/app/models/generalManager.model';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';

const URL = `${environment.apiUrl}/api/general_managers`;

@Injectable({
  providedIn: 'root'
})
export class GeneralManagerService extends MainService<GeneralManager> {

  constructor(protected http: HttpClient) {
    super(GeneralManager, URL, http);
  }

}
