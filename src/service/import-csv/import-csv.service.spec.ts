import { TestBed, inject } from '@angular/core/testing';

import { ImportCsvService } from './import-csv.service';

describe('ImportCsvService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [ImportCsvService]
    });
  });

  it('should be created', inject([ImportCsvService], (service: ImportCsvService) => {
    expect(service).toBeTruthy();
  }));
});
