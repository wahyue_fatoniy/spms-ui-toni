import { Injectable } from "@angular/core";
import { HttpClient, HttpHeaders } from "@angular/common/http";

const URL = "http://localhost:3001/api";

@Injectable({
  providedIn: "root"
})
export class ImportCsvService {
  constructor(private http: HttpClient) {}

  public importCsvProject(file: File) {
    let formData: FormData = new FormData();

    formData.append("file", file, file.name);

    let headers = new HttpHeaders();
    // headers.delete("Content-Type");
    // headers.set("Accept", "multipart/form-data");
    return this.http.post(`${URL}/import-project`, formData, {
      headers: headers,
      responseType: "text",
      reportProgress: true
    });
  }
}
