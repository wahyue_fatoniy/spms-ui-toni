import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { MainService } from '../main-service/main.service';
import { HttpClient } from '@angular/common/http';
import { map } from 'rxjs/operators';
import { Invoice } from 'src/app/models/invoice.model';

const URL = environment.apiUrl + '/api/invoices';

@Injectable({
  providedIn: 'root'
})
export class InvoiceService extends MainService<Invoice> {

  constructor(protected http: HttpClient) {
    super(Invoice, URL, http);
  }

  findByPOId(id: number) {
    return this.http.get(`${URL}/purchase_order/${id}`).pipe(
      map((result: any[]) => {
        this.data = result.map(val => new Invoice(val));
        this.subject.next(this.data);
        return this.data;
      })
    );
  }

  submit(id: number) {
    return this.http.post(`${URL}/submit/${id}`, {})
    .pipe(
      map((result: Invoice) => {
        const invoice = new Invoice(result);
        this.data = this.data.map(val => {
          return val.getId === invoice.getId ? invoice : val;
        });
        this.subject.next(this.data);
        return this.data;
      }),
    );
  }

}
