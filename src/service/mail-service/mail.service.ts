import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { Mail } from 'src/app/models/mail.model';

const URL = `${environment.apiUrl}/api/emails`;


@Injectable({
  providedIn: 'root'
})
export class MailService {
  constructor(private http: HttpClient) { }

  send(email: Mail) {
    return this.http.post(URL, email);
  }
}
