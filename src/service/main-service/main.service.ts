import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { BehaviorSubject, Observable } from 'rxjs';
import { map } from 'rxjs/operators';

export interface ServiceMessage {
  status: string;
  message: string;
}

export class MainService<T> {
  protected subject: BehaviorSubject<T[]> = new BehaviorSubject<T[]>([]);
  protected data: T[] = [];
  private loading = false;

  constructor(
    private classModel: any,
    private Url: string,
    protected http: HttpClient
  ) { }

  public get getLoading() {
    return this.loading;
  }

  protected set setLoading(loading: boolean) {
    this.loading = loading;
  }

  public subscriptionData(): Observable<T[]> {
    return this.subject.asObservable();
  }

  public getData(): Observable<ServiceMessage> {
    this.setLoading = true;
    return new Observable((observer) => {
      this.http
        .get(this.Url)
        .pipe(map((data: T[]) => data.map(mapData => new (this.classModel)(mapData))))
        .toPromise()
        .then((data: T[]) => {
          this.data = data;
          this.subject.next(this.data);
          observer.next({ status: 'success', message: 'Success Getting Data' });
          this.setLoading = false;
        })
        .catch((err: Error) => {
          observer.error({ status: 'error', message: err.message });
          this.setLoading = false;
        });
    });
  }

  public create(data: T): Promise<ServiceMessage> {
    this.setLoading = true;
    return new Promise((resolve, reject) => {
      this.http
        .post(this.Url, data)
        .pipe(
          map(dataCreated => {
            return new this.classModel(dataCreated);
          })
        )
        .toPromise()
        .then((dataCreated: T) => {
          console.log(dataCreated);
          this.data = [...this.data, dataCreated];
          this.subject.next(this.data);
          resolve({ status: 'success', message: 'Success Created Data' });
          this.setLoading = false;
        })
        .catch((err: Error) => {
          console.log(err);
          reject({ status: 'error', message: err.message });
          this.setLoading = false;
        });
    });
  }

  public update(data: T): Promise<T> {
    this.setLoading = true;
    return new Promise((resolve, reject) => {
      this.http
        .put(`${this.Url}/${data['id']}`, data)
        .pipe(map((updateData: T) => new this.classModel(updateData)))
        .toPromise()
        .then((dataUpdate: T) => {
          this.data = this.subject.getValue().map(dataMap =>
            dataMap['id'] === dataUpdate['id'] ? dataUpdate : dataMap
          );
          this.subject.next(this.data);
          resolve(dataUpdate);
          this.setLoading = false;
        })
        .catch((err: Error) => {
          reject({ status: 'error', message: err.message });
          this.setLoading = false;
        });
    });
  }

  public delete(id: number): Promise<ServiceMessage> {
    this.setLoading = true;
    return new Promise((resolve, reject) => {
      this.http
        .delete(`${this.Url}/${id}`)
        .toPromise()
        .then((dataId: number) => {
          this.data = this.data.filter(
            dataFilter => (dataFilter['id'] !== dataId)
          );
          this.subject.next(this.data);
          resolve({ status: 'success', message: 'Success Deleted Data' });
          this.setLoading = false;
        })
        .catch((err: Error) => {
          reject({ status: 'error', message: err.message });
          this.setLoading = false;
        });
    });
  }
}
