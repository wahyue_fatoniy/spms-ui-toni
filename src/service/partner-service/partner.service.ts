import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { MainService } from '../main-service/main.service';
import { Partner } from 'src/app/models/partner.model';
import { HttpClient } from '@angular/common/http';

const URL = `${environment.apiUrl}/api/partners`;

@Injectable({
  providedIn: 'root'
})
export class PartnerService extends MainService<Partner> {

  constructor(protected http: HttpClient) {
    super(Partner, URL, http);
  }
}
