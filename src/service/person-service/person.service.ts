import { Injectable } from '@angular/core';
import { MainService } from '../main-service/main.service';
import { Person } from '../../app/models/person.model';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../environments/environment';
import { pipe } from '@angular/core/src/render3/pipe';
import { map } from 'rxjs/operators';

const URL = `${environment.apiUrl}/api/persons`;

@Injectable({
  providedIn: 'root'
})
export class PersonService extends MainService<Person> {

  constructor(protected http: HttpClient) {
    super(Person, URL, http);
  }

  createWithReturn(data: Person): Promise<Person> {
    return this.http.post(URL, data)
    .pipe(
      map((val: Person) => {
      this.subject.next([...this.data, new Person(val)]);
      return new Person(val);
    }))
    .toPromise();
  }
}
