import { Injectable } from '@angular/core';
import { ProjectManager } from 'src/app/models/projectManager.model';
import { HttpClient } from '@angular/common/http';
import { MainService } from '../main-service/main.service';
import { environment } from 'src/environments/environment';

const URL = `${environment.apiUrl}/api/project_managers`;

@Injectable({
  providedIn: 'root'
})
export class ProjectManagerService extends MainService<ProjectManager> {

  constructor(protected http: HttpClient) {
    super(ProjectManager, URL, http);
   }
}
