import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { MainService } from '../main-service/main.service';
import { PurchaseOrder } from 'src/app/models/purchaseOrder.model';
import { environment } from 'src/environments/environment';

const URL = `${environment.apiUrl}/api/purchase_orders`;

@Injectable({
  providedIn: 'root'
})
export class PurchaseOrderService extends MainService<PurchaseOrder> {
  constructor(protected http: HttpClient) {
    super(PurchaseOrder, URL, http);
  }
}
