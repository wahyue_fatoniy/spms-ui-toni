import { Injectable } from '@angular/core';
import { MainService } from '../main-service/main.service';
import { Region } from '../../app/models/region.model';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../environments/environment';

const URL = `${environment.apiUrl}/api/regions`;

@Injectable({
  providedIn: 'root'
})
export class RegionService extends MainService<Region> {

  constructor(protected http: HttpClient) {
    super(Region, URL, http);
  }
}
