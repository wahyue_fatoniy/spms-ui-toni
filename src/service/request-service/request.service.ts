import { Injectable } from '@angular/core';
import { MainService } from '../main-service/main.service';
import { Request, RequestStatus, RequestType } from 'src/app/models/request.model';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../environments/environment';
import { map } from 'rxjs/operators';

const URL = `${environment.apiUrl}/api/requests`;

@Injectable({
  providedIn: 'root'
})
export class RequestService extends MainService<Request> {
  constructor(protected http: HttpClient) {
    super(Request, URL, http);
  }

  findByStatus(status: RequestStatus, type: RequestType) {
    return this.http.get(`${URL}/status/${status}/type/${type}`)
    .pipe(
      map((requests: any[]) => {
        const data = requests.map(val => {
          return new Request(val);
        })
        this.subject.next(data);
        return data;
      })
    );
  }

  findByType(type: RequestType) {
    return this.http.get(`${URL}/type/${type}`)
    .pipe(
      map((requests: any[]) => {
        const data = requests.map(val => {
          return new Request(val);
        })
        this.subject.next(data);
        return data;
      })
    );
  }
}
