import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';

export interface RouteState {
  path: string;
  param?: any;
}

@Injectable({
  providedIn: 'root'
})
export class RouteService {

  private pathParamState = new BehaviorSubject<RouteState>(null);

  pathParam: Observable<RouteState>;

  constructor() {
    this.pathParam = this.pathParamState.asObservable();
  }

  updatePathParamState(routeState: RouteState) {
    this.pathParamState.next(routeState);
  }
}
