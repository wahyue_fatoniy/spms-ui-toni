import { Injectable } from '@angular/core';
import { MainService } from '../main-service/main.service';
import { Sales } from 'src/app/models/sales.model';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';

const URL = `${environment.apiUrl}/api/sales`;

@Injectable({
  providedIn: 'root'
})
export class SalesService extends MainService<Sales> {
  constructor(protected http: HttpClient) {
    super(Sales, URL, http);
  }
}
