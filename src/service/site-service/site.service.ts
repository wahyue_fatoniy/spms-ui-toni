import { Injectable } from '@angular/core';
import { Site } from 'src/app/models/site.model';
import { MainService } from '../main-service/main.service';
import { environment } from 'src/environments/environment';
import { HttpClient } from '@angular/common/http';

const URL = `${environment.apiUrl}/api/sites`;

@Injectable({
  providedIn: 'root'
})
export class SiteService extends MainService<Site> {

  constructor(protected http: HttpClient) {
    super(Site, URL, http);
  }
}
