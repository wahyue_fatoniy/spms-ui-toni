import { TestBed, inject } from '@angular/core/testing';

import { SiteWorkService } from './site-work.service';

describe('SiteWorkService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [SiteWorkService]
    });
  });

  it('should be created', inject([SiteWorkService], (service: SiteWorkService) => {
    expect(service).toBeTruthy();
  }));
});
