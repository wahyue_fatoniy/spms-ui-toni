import { Injectable  } from '@angular/core';
import { MainService } from '../main-service/main.service';
import { environment } from '../../environments/environment';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { SiteWork } from '../../app/models/siteWork.model';
import { map, catchError } from 'rxjs/operators';
import { Observable } from 'rxjs';
import { Admin } from 'src/app/models/admin.model';
import { Coordinator } from '../../app/models/coordinator.model';
import { projectStatus } from '../enum';
import { DatePipe } from '@angular/common';
import { ProjectManager } from 'src/app/models/projectManager.model';
import { AssignmentStatus } from 'src/app/models/assignment.model';

const URL = `${environment.apiUrl}/api/site_works`;

export enum StatusType {
  Progress = 'ON_PROGRESS',
  Done = 'DONE',
  NotStarted = 'NOT_STARTED',
  Cancel = 'CANCEL'
}

@Injectable({
  providedIn: 'root'
})
export class SiteWorkService extends MainService<SiteWork> {
  datePipe = new DatePipe('id');

  constructor(protected http: HttpClient) {
    super(SiteWork, URL, http);
  }

  public findById(id: number): Observable<SiteWork> {
    this.setLoading = true;
      return this.http.get(`${URL}/${id}`).pipe(
        map((siteWork: SiteWork) => {
          this.setLoading = false;
          this.data = this.data.map(
            val => val.getId === new SiteWork(siteWork).getId ? new SiteWork(siteWork) : val
          );
          this.subject.next(this.data);
          return new SiteWork(siteWork);
        })
      );
  }

  public findByStatus(status: projectStatus[]): Observable<SiteWork[]> {
      this.setLoading = true;
      return this.http.post(`${URL}/status`, status).pipe(
        map((siteWorks: SiteWork[]) => {
          this.setLoading = false;
          siteWorks = siteWorks.map(val => new SiteWork(val));
          siteWorks.forEach(val => {
            this.data = this.data.map(data => data.getId === val.getId ? val : data);
          });
          this.subject.next(siteWorks);
          return this.data;
        })
      );
  }

  public findByPMs(pms: ProjectManager[]): Observable<SiteWork[]> {
    this.setLoading = true;
    return this.http.post(`${URL}/project_managers`, pms).pipe(
      map((result: any) => {
        this.setLoading = false;
        const siteWorks = result.map(_project => new SiteWork(_project));
        this.data = siteWorks;
        this.subject.next(siteWorks);
        return siteWorks;
      }),
      catchError(err => {
        this.setLoading = false;
        return err;
      })
    );
  }

  public findByPMRange(pmId: number, start: Date, end: Date): Observable<SiteWork[]> {
    this.setLoading = true;
    return this.http.get(`${URL}/project_manager/${pmId}/${this.setDate(start)}/${this.setDate(end)}`).pipe(
      map((result: any) => {
        this.setLoading = false;
        const siteWorks = result.map(_project => new SiteWork(_project));
        this.data = siteWorks;
        this.subject.next(siteWorks);
        return siteWorks;
      }),
      catchError(err => {
        this.setLoading = false;
        return err;
      })
    );
  }

  public findByRange(startDate: Date, endDate: Date): Observable<SiteWork[]> {
    this.setLoading = true;
    return this.http.get(`${URL}/start_date/${this.setDate(startDate)}/${this.setDate(endDate)}`).pipe(
      map((result: any) => {
        this.setLoading = false;
        const siteWorks = result.map(_project => new SiteWork(_project));
        this.data = siteWorks;
        this.subject.next(siteWorks);
        return siteWorks;
      }),
      catchError(err => {
        this.setLoading = false;
        return err;
      })
    );
  }

  private setDate(date: string | Date): string {
    return this.datePipe.transform(date, 'yyyy-MM-ddTHH:mm:ssZ', '+0000');
  }

  public findByAdmins(admins: Admin[]): Observable<SiteWork[]> {
    this.setLoading = true;
    return this.http.post(`${URL}/admins`, admins).pipe(
      map((result: any) => {
        this.setLoading = false;
        const siteWorks = result.map(_project => new SiteWork(_project));
        this.data = siteWorks;
        this.subject.next(siteWorks);
        return siteWorks;
      }),
      catchError(err => {
        this.setLoading = false;
        return err;
      })
    );
  }

  public findByPics(pics: Coordinator[]): Observable<SiteWork[]> {
    this.setLoading = true;
    return this.http.post(`${URL}/cordinators`, pics).pipe(
      map((result: any) => {
        this.setLoading = false;
        const siteWorks = result.map(_project => new SiteWork(_project));
        this.data = siteWorks;
        this.subject.next(siteWorks);
        return siteWorks;
      }),
      catchError(err => {
        this.setLoading = false;
        return err;
      })
    );
  }

  public findByAssignmentStatus(status: AssignmentStatus) {
    return this.http.get(`${URL}/assignment/status/${status}`).pipe(
      map((result: any) => {
        this.setLoading = false;
        const siteWorks = result.map(_project => new SiteWork(_project));
        this.data = siteWorks;
        this.subject.next(siteWorks);
        return siteWorks;
      }),
      catchError(err => {
        this.setLoading = false;
        return err;
      })
    );
  }

  public findByAssignmentPm(pmId: number, status: AssignmentStatus) {
    return this.http.get(`${URL}/assignment/project_manager/${pmId}/status/${status}`).pipe(
      map((result: any) => {
        this.setLoading = false;
        const siteWorks = result.map(_project => new SiteWork(_project));
        this.data = siteWorks;
        this.subject.next(this.data);
        return siteWorks;
      }),
      catchError(err => {
        this.setLoading = false;
        return err;
      })
    );
  }

  findNotPaid() {
    return this.http.get(`${URL}/not_paid`).pipe(
      map((results: any[]) => {
          this.data = results.map(result => new SiteWork(result));
          this.subject.next(this.data);
          return this.data;
      })
    )
  }

  getPriority(settings: projectStatus[], siteWorks: SiteWork[]): SiteWork[] {
    const projects = siteWorks.sort((a, b) => b.getId - a.getId);
    const progress = [], cancel = [], done = [], notStarted = [];
    for (const project of projects) {
      if (project.getStatus === projectStatus.CANCEL) {
         cancel.push(project);
      } else if (project.getStatus === projectStatus.DONE) {
         done.push(project);
      } else if (project.getStatus === projectStatus.NOT_STARTED) {
         notStarted.push(project);
      } else if (project.getStatus === projectStatus.ON_PROGRESS) {
         progress.push(project);
      }
    }
    let result = [];
    for (const setting of settings) {
      if (setting === projectStatus.NOT_STARTED) {
        result = [...result, ...notStarted];
      } else if (setting === projectStatus.ON_PROGRESS) {
        result = [...result, ...progress];
      } else if (setting === projectStatus.DONE) {
        result = [...result, ...done];
      } else if (setting === projectStatus.CANCEL) {
        result = [...result, ...cancel];
      }
    }
    return result;
  }


  public getReport(siteworkIds: number[]) {
    let headers = new HttpHeaders();
    headers.append('Content-Type', 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
    return this.http.post('http://spms-satunol.ddns.net:3001/api/customer-report', siteworkIds,
    {responseType: 'arraybuffer'})
    .pipe(
      map(res => new Blob([res], { type: 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet' }))
    );
  }
}
