import { TestBed, inject } from '@angular/core/testing';

import { SowService } from './sow.service';

describe('SowService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [SowService]
    });
  });

  it('should be created', inject([SowService], (service: SowService) => {
    expect(service).toBeTruthy();
  }));
});
