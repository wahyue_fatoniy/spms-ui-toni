import { Injectable } from '@angular/core';
import { MainService } from '../main-service/main.service';
import { environment } from '../../environments/environment';
import { HttpClient } from '@angular/common/http';
import { SoW } from '../../app/models/sow.model';
import { map } from 'rxjs/operators';

const URL = `${environment.apiUrl}/api/sows`;

@Injectable({
  providedIn: 'root'
})
export class SowService extends MainService<SoW> {
  constructor(protected http: HttpClient) {
    super(SoW, URL, http);
  }

  public getSowHistories(siteworkId: number) {
    return this.http.get(`${environment.apiUrl}/api/sow_histories/site_work/${siteworkId}`).pipe(
      map((sows: any[]) => {
          sows = sows.map(sow => {
            return new SoW(sow)
          })
          this.subject.next(sows);
          return sows;
      })
    );
  }
}
