import { Injectable } from "@angular/core";
import { environment } from "../../environments/environment";
import { MainService } from "../main-service/main.service";
import { HttpClient } from "@angular/common/http";
import { SowType } from "../../app/models/sow-type.model";
import { Observable } from "rxjs";
import { Region } from "src/app/models/region.model";
import { map, catchError } from "rxjs/operators";

const URL = `${environment.apiUrl}/api/sow_types`;

@Injectable({
  providedIn: "root"
})
export class SowTypeService extends MainService<SowType> {
  constructor(protected http: HttpClient) {
    super(SowType, URL, http);
  }

  public findByRegions(regions: Region[]): Observable<SowType[]> {
    this.setLoading = true;
    return this.http.post(`${URL}/regions`, regions).pipe(
      map((result: any) => {
        this.setLoading = false;
        const sowType = result.map(_sowType => new SowType(_sowType));
        this.subject.next(sowType);
        return sowType;
      }),
      catchError(err => {
        this.setLoading = false;
        return err;
      })
    );
  }
}
