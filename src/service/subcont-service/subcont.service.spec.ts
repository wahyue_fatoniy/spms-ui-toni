import { TestBed, inject } from '@angular/core/testing';

import { SubcontService } from './subcont.service';

describe('SubcontService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [SubcontService]
    });
  });

  it('should be created', inject([SubcontService], (service: SubcontService) => {
    expect(service).toBeTruthy();
  }));
});
