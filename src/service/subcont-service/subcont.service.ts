import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { MainService } from '../main-service/main.service';
import { Coordinator } from 'src/app/models/coordinator.model';
import { HttpClient } from '@angular/common/http';

const URL = `${environment.apiUrl}/api/cordinators`;

@Injectable({
  providedIn: 'root'
})
export class SubcontService extends MainService<Coordinator> {

  constructor(protected http: HttpClient) {
    super(Coordinator, URL, http);
   }
}
