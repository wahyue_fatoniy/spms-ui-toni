import { Injectable } from "@angular/core";
import { environment } from "../../environments/environment";
import { MainService, ServiceMessage } from "../main-service/main.service";
import { Task } from "../../app/models/task.model";
import { HttpClient } from "@angular/common/http";
import { map, catchError } from "rxjs/operators";
import { Observable } from "rxjs";

const URL = `${environment.apiUrl}/api/tasks`;

@Injectable({
  providedIn: "root"
})
export class TaskService extends MainService<Task> {
  constructor(protected http: HttpClient) {
    super(Task, URL, http);
  }

  public createTask(
    siteWorkId: number,
    sowTypeId: number
  ): Promise<ServiceMessage> {
    this.setLoading = true;
    return new Promise((resolve, reject) => {
      this.http
        .post(`${URL}/generate_task/${siteWorkId}/${sowTypeId}`, [])
        .pipe(
          map((task: Task[]) => {
            console.log(task);
            return task.map(taskMap => new Task(taskMap));
          })
        )
        .toPromise()
        .then((val: Task[]) => {
          this.createRequirement(siteWorkId, val)
            .toPromise()
            .then((tasks: Task[]) => {
              this.setLoading = false;
              this.data = tasks.map(task => new Task(task));
              this.subject.next(this.data);
            });
          resolve({ status: "status", message: "Success Created Data" });
        })
        .catch((err: Error) => {
          this.setLoading = false;
          reject({ status: "error", message: err.message });
        });
    });
  }

  private createRequirement(siteworkId: number, tasks: Task[]) {
    return this.http.post(`${URL}/generate_requirement/${siteworkId}`, tasks);
  }

  findBySiteWorkId(id: number): Observable<Task[]> {
    this.setLoading = true;
    return this.http.get(`${URL}/site_work/${id}`).pipe(
      map((result: any) => {
        this.setLoading = false;
        this.data = result.map(_project => new Task(_project));
        this.subject.next(this.data);
        return result.map(_project => new Task(_project));
      }),
      catchError(error => {
        this.setLoading = false;
        return error;
      })
    );
  }
}
