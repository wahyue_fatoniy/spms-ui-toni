import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { Team } from 'src/app/models/team.model';
import { MainService } from '../main-service/main.service';
import { HttpClient } from '@angular/common/http';


const URL = `${environment.apiUrl}/api/teams`;

@Injectable({
  providedIn: 'root'
})
export class TeamService extends MainService<Team> {

  constructor(protected http: HttpClient) {
    super(Team, URL, http);
  }
}
