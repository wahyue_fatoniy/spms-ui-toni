import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { MainService } from '../main-service/main.service';
import { Technician } from 'src/app/models/technician.model';
import { HttpClient } from '@angular/common/http';
import { map } from 'rxjs/operators';


const URL = `${environment.apiUrl}/api/technicians`;

@Injectable({
  providedIn: 'root'
})
export class TechnicianService extends MainService<Technician> {

  constructor(protected http: HttpClient) {
    super(Technician, URL, http);
  }

  findByRegionId(id: number) {
    return this.http.get(`${environment.apiUrl}/api/technicians/region/${id}`)
    .pipe(
      map((technician: Object[]) => {
        this.data = technician.map(val => {
          return new Technician(val);
        });
        this.subject.next(this.data);
        return this.data;
      })
    );
  }
}
