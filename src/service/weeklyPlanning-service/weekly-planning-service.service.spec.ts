import { TestBed, inject } from '@angular/core/testing';

import { WeeklyPlanningServiceService } from './weekly-planning-service.service';

describe('WeeklyPlanningServiceService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [WeeklyPlanningServiceService]
    });
  });

  it('should be created', inject([WeeklyPlanningServiceService], (service: WeeklyPlanningServiceService) => {
    expect(service).toBeTruthy();
  }));
});
