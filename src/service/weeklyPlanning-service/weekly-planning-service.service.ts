import { Injectable } from '@angular/core';
import { environment } from '../../environments/environment';
import { MainService, ServiceMessage } from '../main-service/main.service';
import { WeeklyPlanning } from '../../app/models/weeklyPlanning.model';
import { HttpClient } from '@angular/common/http';
import { map, catchError } from 'rxjs/operators';
import { DatePipe } from '@angular/common';
import * as moment from 'moment';
import { Observable } from 'rxjs';

const URL = `${environment.apiUrl}/api/weekly_plannings`;

@Injectable({
  providedIn: 'root'
})
export class WeeklyPlanningServiceService extends MainService<WeeklyPlanning> {
  private datePipe: DatePipe = new DatePipe('id');

  constructor(protected http: HttpClient) {
    super(WeeklyPlanning, URL, http);
  }

  public submit(id: number): Promise<{}> {
    this.setLoading = true;
    return this.http
      .post(`${URL}/submit/${id}`, {})
      .pipe(
        map((planning: WeeklyPlanning) => {
          this.setLoading = false;
          this.data = this.data.map(_planning => {
            return planning['id'] === _planning.getId ? new WeeklyPlanning(planning) : _planning;
          });
          this.subject.next(this.data);
          return { status: 'success', message: 'Success to submit data' };
        }),
        catchError((err) => {
          this.setLoading = false;
          return err;
        })
      )
      .toPromise();
  }

  public getByWeek(siteworkId: number, initialDate: Date): Observable<{}> {
    const startWeek = this.getStartOfWeek(initialDate);
    const endWeek = this.getEndOfWeek(startWeek);
    this.setLoading = true;
    return this.http
      .get(`${URL}/${siteworkId}/${startWeek}/${endWeek}`)
      .pipe(
        map((val: WeeklyPlanning[]) => {
          this.setLoading = false;
          this.data = val.map(
            weeklyPlanning => new WeeklyPlanning(weeklyPlanning)
          );
          this.subject.next(this.data);
          return { status: 'success', message: 'Success to get data' };
        }),
        catchError(err => {
          this.setLoading = false;
          return err;
        })
      );
  }

  public getByRangeDate(siteworkId: number, fromDate: Date, toDate: Date): Observable<{}> {
    const startWeek = this.setUTCDate(fromDate);
    const endWeek = this.setUTCDate(toDate);
    this.setLoading = true;
    return this.http
      .get(`${URL}/${siteworkId}/${startWeek}/${endWeek}`)
      .pipe(
        map((val: WeeklyPlanning[]) => {
          this.setLoading = false;
          this.data = val.map(
            weeklyPlanning => new WeeklyPlanning(weeklyPlanning)
          );
          this.subject.next(this.data);
          return { status: 'success', message: 'Success to get data' };
        }),
        catchError(err => {
          this.setLoading = false;
          return err;
        })
      );
  }

  private getStartOfWeek(startWeek: Date): string {
    const startOfWeek = moment(startWeek)
      .startOf('week')
      .day(+1);
    /**set start initialDate to UTC format */
    const initialDate: string = this.datePipe.transform(
      startOfWeek,
      'yyyy-MM-ddTHH:mm:ssZ',
      '+0000'
    );
    return initialDate;
  }

  private getEndOfWeek(startWeek: string) {
    const endOfWeek = moment(new Date(startWeek))
      .day(+7)
      .endOf('day');
    /**set end initialDate to UTC format */
    const initialDate: string = this.datePipe.transform(
      endOfWeek,
      'yyyy-MM-ddTHH:mm:ssZ',
      '+0000'
    );
    return initialDate;
  }

  private setUTCDate(date: Date): string {
    const utcDate: string = this.datePipe.transform(
      date,
      'yyyy-MM-ddTHH:mm:ssZ',
      '+0000'
    );
    return utcDate;
  }
}
